/**
 */
package ca.mcgill.sel.usecases;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Actor</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.usecases.Actor#getLowerBound <em>Lower Bound</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.Actor#getUpperBound <em>Upper Bound</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.usecases.UcPackage#getActor()
 * @model
 * @generated
 */
public interface Actor extends NamedElement {
    /**
     * Returns the value of the '<em><b>Lower Bound</b></em>' attribute.
     * The default value is <code>"1"</code>.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Lower Bound</em>' attribute.
     * @see #setLowerBound(int)
     * @see ca.mcgill.sel.usecases.UcPackage#getActor_LowerBound()
     * @model default="1"
     * @generated
     */
    int getLowerBound();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.usecases.Actor#getLowerBound <em>Lower Bound</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Lower Bound</em>' attribute.
     * @see #getLowerBound()
     * @generated
     */
    void setLowerBound(int value);

    /**
     * Returns the value of the '<em><b>Upper Bound</b></em>' attribute.
     * The default value is <code>"-1"</code>.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Upper Bound</em>' attribute.
     * @see #setUpperBound(int)
     * @see ca.mcgill.sel.usecases.UcPackage#getActor_UpperBound()
     * @model default="-1"
     * @generated
     */
    int getUpperBound();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.usecases.Actor#getUpperBound <em>Upper Bound</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Upper Bound</em>' attribute.
     * @see #getUpperBound()
     * @generated
     */
    void setUpperBound(int value);

} // Actor
