package ca.mcgill.sel.usecases.util;

import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.usecases.Layout;
import ca.mcgill.sel.usecases.LayoutElement;
import ca.mcgill.sel.usecases.UcFactory;
import ca.mcgill.sel.usecases.UseCaseModel;

public final class UcModelUtil {
    
    /**
     * Creates a new instance of {@link UcdmModelUtil}.
     */
    private UcModelUtil() {
        // suppress default constructor
    }    

    /**
     * Creates a new use case diagram and associated entities
     * @param name The name of the diagram
     * @return The new diagram
     */
    public static UseCaseModel createUseCaseDiagram(String name) {
        UseCaseModel ucd = UcFactory.eINSTANCE.createUseCaseModel();       

        ucd.setName(name);        

        // create an empty layout
        createLayout(ucd);       

        return ucd;
    }    

    

    /**

     * Creates a new layout for a given {@link UseCaseDiagram}.
     * The layout is the {@link ca.mcgill.sel.ram.impl.ContainerMapImpl} specifically
     * that holds all {@link LayoutElement} for children of the given {@link UseCaseDiagram}.
     *
     * @param cd the {@link UseCaseDiagram} holding the {@link LayoutElement} for its children
     */

    private static void createLayout(UseCaseModel ucd) {
        Layout layout = UcFactory.eINSTANCE.createLayout();

        // workaround used here since creating the map, adding the values and then putting it doesn't work
        // EMF somehow does some magic with the passed map instance
        layout.getContainers().put(ucd, new BasicEMap<EObject, LayoutElement>());

        ucd.setLayout(layout);
    }
}
