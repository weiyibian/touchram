/**
 */
package ca.mcgill.sel.usecases.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see ca.mcgill.sel.usecases.util.UcResourceFactoryImpl
 * @generated
 */
public class UcResourceImpl extends XMIResourceImpl {
    /**
     * Creates an instance of the resource.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param uri the URI of the new resource.
     * @generated
     */
    public UcResourceImpl(URI uri) {
        super(uri);
    }

} //UcResourceImpl
