/**
 */
package ca.mcgill.sel.usecases;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Use Case</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.usecases.UseCase#getPrimaryActor <em>Primary Actor</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.UseCase#getSecondaryActors <em>Secondary Actors</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.usecases.UcPackage#getUseCase()
 * @model
 * @generated
 */
public interface UseCase extends NamedElement {
    /**
     * Returns the value of the '<em><b>Primary Actor</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Primary Actor</em>' reference.
     * @see #setPrimaryActor(Actor)
     * @see ca.mcgill.sel.usecases.UcPackage#getUseCase_PrimaryActor()
     * @model
     * @generated
     */
    Actor getPrimaryActor();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.usecases.UseCase#getPrimaryActor <em>Primary Actor</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Primary Actor</em>' reference.
     * @see #getPrimaryActor()
     * @generated
     */
    void setPrimaryActor(Actor value);

    /**
     * Returns the value of the '<em><b>Secondary Actors</b></em>' reference list.
     * The list contents are of type {@link ca.mcgill.sel.usecases.Actor}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Secondary Actors</em>' reference list.
     * @see ca.mcgill.sel.usecases.UcPackage#getUseCase_SecondaryActors()
     * @model
     * @generated
     */
    EList<Actor> getSecondaryActors();

} // UseCase
