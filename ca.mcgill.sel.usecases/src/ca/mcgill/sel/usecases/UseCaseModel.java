/**
 */
package ca.mcgill.sel.usecases;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Use Case Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.usecases.UseCaseModel#getLayout <em>Layout</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.UseCaseModel#getActors <em>Actors</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.UseCaseModel#getUseCases <em>Use Cases</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.UseCaseModel#getNotes <em>Notes</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.usecases.UcPackage#getUseCaseModel()
 * @model
 * @generated
 */
public interface UseCaseModel extends NamedElement {
    /**
     * Returns the value of the '<em><b>Layout</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Layout</em>' containment reference.
     * @see #setLayout(Layout)
     * @see ca.mcgill.sel.usecases.UcPackage#getUseCaseModel_Layout()
     * @model containment="true"
     * @generated
     */
    Layout getLayout();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.usecases.UseCaseModel#getLayout <em>Layout</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Layout</em>' containment reference.
     * @see #getLayout()
     * @generated
     */
    void setLayout(Layout value);

    /**
     * Returns the value of the '<em><b>Actors</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.usecases.Actor}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Actors</em>' containment reference list.
     * @see ca.mcgill.sel.usecases.UcPackage#getUseCaseModel_Actors()
     * @model containment="true"
     * @generated
     */
    EList<Actor> getActors();

    /**
     * Returns the value of the '<em><b>Use Cases</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.usecases.UseCase}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Use Cases</em>' containment reference list.
     * @see ca.mcgill.sel.usecases.UcPackage#getUseCaseModel_UseCases()
     * @model containment="true"
     * @generated
     */
    EList<UseCase> getUseCases();

    /**
     * Returns the value of the '<em><b>Notes</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.usecases.Note}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Notes</em>' containment reference list.
     * @see ca.mcgill.sel.usecases.UcPackage#getUseCaseModel_Notes()
     * @model containment="true"
     * @generated
     */
    EList<Note> getNotes();

} // UseCaseModel
