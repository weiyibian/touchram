/**
 */
package ca.mcgill.sel.usecases.impl;

import ca.mcgill.sel.usecases.Actor;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.UseCase;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Use Case</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.usecases.impl.UseCaseImpl#getPrimaryActor <em>Primary Actor</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.impl.UseCaseImpl#getSecondaryActors <em>Secondary Actors</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UseCaseImpl extends NamedElementImpl implements UseCase {
    /**
     * The cached value of the '{@link #getPrimaryActor() <em>Primary Actor</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getPrimaryActor()
     * @generated
     * @ordered
     */
    protected Actor primaryActor;

    /**
     * The cached value of the '{@link #getSecondaryActors() <em>Secondary Actors</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getSecondaryActors()
     * @generated
     * @ordered
     */
    protected EList<Actor> secondaryActors;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected UseCaseImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return UcPackage.Literals.USE_CASE;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Actor getPrimaryActor() {
        if (primaryActor != null && primaryActor.eIsProxy()) {
            InternalEObject oldPrimaryActor = (InternalEObject)primaryActor;
            primaryActor = (Actor)eResolveProxy(oldPrimaryActor);
            if (primaryActor != oldPrimaryActor) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, UcPackage.USE_CASE__PRIMARY_ACTOR, oldPrimaryActor, primaryActor));
            }
        }
        return primaryActor;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public Actor basicGetPrimaryActor() {
        return primaryActor;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setPrimaryActor(Actor newPrimaryActor) {
        Actor oldPrimaryActor = primaryActor;
        primaryActor = newPrimaryActor;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, UcPackage.USE_CASE__PRIMARY_ACTOR, oldPrimaryActor, primaryActor));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList<Actor> getSecondaryActors() {
        if (secondaryActors == null) {
            secondaryActors = new EObjectResolvingEList<Actor>(Actor.class, this, UcPackage.USE_CASE__SECONDARY_ACTORS);
        }
        return secondaryActors;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case UcPackage.USE_CASE__PRIMARY_ACTOR:
                if (resolve) return getPrimaryActor();
                return basicGetPrimaryActor();
            case UcPackage.USE_CASE__SECONDARY_ACTORS:
                return getSecondaryActors();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case UcPackage.USE_CASE__PRIMARY_ACTOR:
                setPrimaryActor((Actor)newValue);
                return;
            case UcPackage.USE_CASE__SECONDARY_ACTORS:
                getSecondaryActors().clear();
                getSecondaryActors().addAll((Collection<? extends Actor>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case UcPackage.USE_CASE__PRIMARY_ACTOR:
                setPrimaryActor((Actor)null);
                return;
            case UcPackage.USE_CASE__SECONDARY_ACTORS:
                getSecondaryActors().clear();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case UcPackage.USE_CASE__PRIMARY_ACTOR:
                return primaryActor != null;
            case UcPackage.USE_CASE__SECONDARY_ACTORS:
                return secondaryActors != null && !secondaryActors.isEmpty();
        }
        return super.eIsSet(featureID);
    }

} //UseCaseImpl
