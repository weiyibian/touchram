package ca.mcgill.sel.ram.expressions.tests

import static extension org.junit.Assert.*

import com.google.inject.Inject
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.XtextRunner
import org.eclipse.xtext.testing.util.ParseHelper
import org.junit.Test
import org.junit.runner.RunWith
import ca.mcgill.sel.ram.ValueSpecification
import org.xtext.example.mydsl.tests.ExpressionsDslInjectorProvider
import ca.mcgill.sel.ram.expressions.typechecking.TypeChecker

/**
 * To compile we need to add reference project 
 * and disable compile project restriction erros for XTend
 */
@RunWith(XtextRunner)
@InjectWith(ExpressionsDslInjectorProvider)
class TypeCheckerTest {
    @Inject extension ParseHelper<ValueSpecification> parseHelper
    extension TestUtils utils = new TestUtils

    @Test
    def testMulType() {
        "2*2".typeCheckWithNoIssues(TypeChecker.Types.intType)
        "2.0f*2.0f".typeCheckWithNoIssues(TypeChecker.Types.floatType)
    }

    @Test
    def testNotType() {
        "!true".typeCheckWithNoIssues(TypeChecker.Types.booleanType)
    }

    @Test
    def testDivType() {
        "2/2".typeCheckWithNoIssues(TypeChecker.Types.intType)
    }

    @Test
    def testPlusType() {
        "2+2".typeCheckWithNoIssues(TypeChecker.Types.intType)
    }

    @Test
    def testMinusType() {
        "2-2".typeCheckWithNoIssues(TypeChecker.Types.intType)
    }

    @Test
    def testBitshiftType() {
        #["2>>2", "2<<2", "2>>>2"].forEach([e|e.typeCheckWithNoIssues(TypeChecker.Types.intType)])
    }

    @Test
    def testComparisonType() {
        #["2>2", "2>=2", "2<2", "2<=2"].forEach([e|e.typeCheckWithNoIssues(TypeChecker.Types.booleanType)])
    }

    @Test
    def testEqualityType() {
        #["2!=2", "2==2"].forEach([e|e.typeCheckWithNoIssues(TypeChecker.Types.booleanType)])
    }

    @Test
    def testAndType() {
        #["false&&true"].forEach([e|e.typeCheckWithNoIssues(TypeChecker.Types.booleanType)])
    }

    @Test
    def testOrType() {
        #["false||true"].forEach([e|e.typeCheckWithNoIssues(TypeChecker.Types.booleanType)])
    }

    @Test
    def testTernaryType() {
        #["false?3:4"].forEach([e|e.typeCheckWithNoIssues(TypeChecker.Types.intType)])
    }

    def typeCheckWithNoIssues(String input, TypeChecker.Types expectedType) {
        TypeChecker.typeCheckNode(input.parse, expectedType).length().assertEquals(0)
    }

    def typeCheckWithIssues(String input, TypeChecker.Types expectedType) {
        TypeChecker.typeCheckNode(input.parse, expectedType).printObject
    }
}
