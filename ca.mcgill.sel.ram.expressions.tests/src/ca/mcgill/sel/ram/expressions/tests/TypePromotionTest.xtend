package ca.mcgill.sel.ram.expressions.tests

import static extension org.junit.Assert.*

import com.google.inject.Inject
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.XtextRunner
import org.eclipse.xtext.testing.util.ParseHelper
import org.junit.Test
import org.junit.runner.RunWith
import ca.mcgill.sel.ram.ValueSpecification
import org.xtext.example.mydsl.tests.ExpressionsDslInjectorProvider
import ca.mcgill.sel.ram.expressions.typechecking.TypeChecker
import ca.mcgill.sel.ram.expressions.typechecking.TypeCheckingUtil

/**
 * To compile we need to add reference project 
 * and disable compile project restriction erros for XTend
 */
@RunWith(XtextRunner)
@InjectWith(ExpressionsDslInjectorProvider)
class TypePromotionTest {
    @Inject extension ParseHelper<ValueSpecification> parseHelper
    extension TestUtils utils = new TestUtils

    @Test
    def testPromotionLogic() {
        // 2nd argument is also used as a type that the whole expression should promote to
        #[
            #[TypeChecker.Types.byteType, TypeChecker.Types.intType],
            #[TypeChecker.Types.intType, TypeChecker.Types.floatType],
            #[TypeChecker.Types.longType, TypeChecker.Types.floatType],
            #[TypeChecker.Types.charType, TypeChecker.Types.floatType],
            #[TypeChecker.Types.longType, TypeChecker.Types.doubleType]
        ].forEach([e|TypeCheckingUtil.promoteToType(e.get(0), e.get(1)).assertEquals(e.get(1))])
    }

    @Test
    def testTypePromotionMultDivMod() {
        "2*2.0f".typeCheckWithNoIssues(TypeChecker.Types.floatType)
        "2.0f*2".typeCheckWithNoIssues(TypeChecker.Types.floatType)
        "'a'/2.0f".typeCheckWithNoIssues(TypeChecker.Types.floatType)
        "'a'%2.0d".typeCheckWithNoIssues(TypeChecker.Types.doubleType)
    }

    @Test
    def testTypePromotionMinus() {
        "'a'-2L".typeCheckWithNoIssues(TypeChecker.Types.longType)
        "2.0f-2D".typeCheckWithNoIssues(TypeChecker.Types.doubleType)
    }

    @Test
    def testTypePromotionPlus() {
        "\"string\"+2".typeCheckWithNoIssues(TypeChecker.Types.stringType)
        "\"string\"+2d".typeCheckWithNoIssues(TypeChecker.Types.stringType)
    }

    def typeCheckWithNoIssues(String input, TypeChecker.Types expectedType) {
        TypeChecker.typeCheckNode(input.parse, expectedType).length().assertEquals(0)
    }

    def typeCheckWithIssues(String input, TypeChecker.Types expectedType) {
        TypeChecker.typeCheckNode(input.parse, expectedType).printObject
    }
}
