[comment encoding = UTF-8 /]
[module generateExpression('http://cs.mcgill.ca/sel/ram/3.0')]
[import ca::mcgill::sel::ram::generator::common::commonHelpers /]
[import ca::mcgill::sel::ram::generator::java::javaHelpers /]

[**
 * Generates the given value specification.
 * @param v The value specification
 * @param m The message
 */]
[template public generateValueSpecification(v : Ternary, m : Message) post(trim())]
[generateValueSpecification(v.condition, m)/] ? [generateValueSpecification(v.expressionT, m)/] : [generateValueSpecification(v.expressionF, m)/] 
[/template]

[**
 * Generates the given value specification.
 * @param v The value specification
 * @param m The message
 */]
[template public generateValueSpecification(v : ValueSpecification, m : Message) /]

[**
 * Generates the given value specification.
 * @param v The value specification
 * @param m The message
 */]
[template public generateValueSpecification(v : MulDivMod, m : Message) post(trim())]
[generateValueSpecification(v.left, m)/] [v.op/] [generateValueSpecification(v.right, m)/] 
[/template]

[**
 * Generates the given value specification.
 * @param v The value specification
 * @param m The message
 */]
[template public generateValueSpecification(v : Plus, m : Message) post(trim())]
[generateValueSpecification(v.left, m)/] + [generateValueSpecification(v.right, m)/] 
[/template]

[**
 * Generates the given value specification.
 * @param v The value specification
 * @param m The message
 */]
[template public generateValueSpecification(v : Minus, m : Message) post(trim())]
[generateValueSpecification(v.left, m)/] - [generateValueSpecification(v.right, m)/] 
[/template]

[**
 * Generates the given value specification.
 * @param v The value specification
 * @param m The message
 */]
[template public generateValueSpecification(v : Comparison, m : Message) post(trim())]
[generateValueSpecification(v.left, m)/] [v.op/] [generateValueSpecification(v.right, m)/] 
[/template]


[**
 * Generates the given value specification.
 * @param v The value specification
 * @param m The message
 */]
[template public generateValueSpecification(v : Equality, m : Message) post(trim())]
[generateValueSpecification(v.left, m)/] [v.op/] [generateValueSpecification(v.right, m)/] 
[/template]

[**
 * Generates the given value specification.
 * @param v The value specification
 * @param m The message
 */]
[template public generateValueSpecification(v : And, m : Message) post(trim())]
[generateValueSpecification(v.left, m)/] && [generateValueSpecification(v.right, m)/] 
[/template]

[**
 * Generates the given value specification.
 * @param v The value specification
 * @param m The message
 */]
[template public generateValueSpecification(v : Or, m : Message) post(trim())]
[generateValueSpecification(v.left, m)/] || [generateValueSpecification(v.right, m)/] 
[/template]


[**
 * Generates the given value specification.
 * @param v The value specification
 * @param m The message
 */]
[template public generateValueSpecification(v : Not, m : Message) post(trim())]
   !([generateValueSpecification(v.expression, m)/]) 
[/template]

[**
 * Generates the given value specification.
 * @param v The value specification
 * @param m The message
 */]
[template public generateValueSpecification(v : UnaryMinus, m : Message) post(trim())]
    -[generateValueSpecification(v.expression, m)/] 
[/template]

[**
 * Generates the given value specification.
 * @param v The value specification
 * @param m The message
 */]
[template public generateValueSpecification(v : ParameterValue, m : Message) post(trim())]
    [parameter.name/]
[/template]


[**
 * Generates the given value specification.
 * @param v The value specification
 * @param m The message
 */]
[template public generateValueSpecification(v : StructuralFeatureValue, m : Message) post(trim())]
[if (m.getTargetLifeline().represents = value)]
    this
[else]
    [getStructuralFeatureName(value, m)/]
[/if]
[/template]

[template public generateValueSpecification(v : EnumLiteralValue, m : Message) post(trim())]
    [value.enum.getTypeName()/].[value.name/]
[/template]


[**
 * Generates the given value specification.
 * @param v The value specification
 * @param m The message
 */]
[template public generateValueSpecification(v : LiteralString, m : Message) post(trim())]
    [value/]
[/template]


[**
 * Generates the given value specification.
 * @param v The value specification
 * @param m The message
 */]
[template public generateValueSpecification(v : LiteralBoolean, m : Message) post(trim())]
    [value/]
[/template]


[**
 * Generates the given value specification.
 * @param v The value specification
 * @param m The message
 */]
[template public generateValueSpecification(v : LiteralInteger, m : Message) post(trim())]
    [value/]
[/template]

[**
 * Generates the given value specification.
 * @param v The value specification
 * @param m The message
 */]
[template public generateValueSpecification(v : LiteralChar, m : Message) post(trim())]
    '[value/]'
[/template]


[**
 * Generates the given value specification.
 * @param v The value specification
 * @param m The message
 */]
[template public generateValueSpecification(v : LiteralFloat, m : Message) post(trim())]
    [value/]f
[/template]


[**
 * Generates the given value specification.
 * @param v The value specification
 * @param m The message
 */]
[template public generateValueSpecification(v : LiteralDouble, m : Message) post(trim())]
    [value/]d
[/template]


[**
 * Generates the given value specification.
 * @param v The value specification
 * @param m The message
 */]
[template public generateValueSpecification(v : LiteralLong, m : Message) post(trim())]
    [value/]L
[/template]

[**
 * Generates the given value specification.
 * @param v The value specification
 * @param m The message
*/]
[template public generateValueSpecification(v : LiteralByte, m : Message) post(trim())]
    [value/]
[/template]

[**
 * Generates the given value specification.
 * @param v The value specification
 * @param m The message
 */]
[template public generateValueSpecification(v : LiteralNull, m : Message) post(trim())]
 null
[/template]

[**
 * Generates the given value specification.
 * @param v The value specification
 * @param m The message
 */]
[template public generateValueSpecification(v : OpaqueExpression, m : Message)
        post(removeMarkedNewLines('#').trim())]
[_body/]
[/template]


[**
 * Returns the correct local name of a structural feature object.
 * @param sf The structural feature
 * @param m The message
 */]
[query public getStructuralFeatureName(sf : StructuralFeature, m : Message) : String =
    if m.localProperties->includes(sf) then
        sf.getLocalPropertyName(name)
    else
        if (not sf._static) then
            'this.' + sf.name
        else
            m.signature.eContainer(Classifier).name + '.' + sf.name
        endif
    endif
/]


[**
 * Returns the name of a local property.
 * Here you can customize local variable names, like 'local_' + name for example.
 * @param name The original name
 */]
[query public getLocalPropertyName(name : String) : String =
    name
/]