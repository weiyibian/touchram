[comment encoding = UTF-8 /]
[module javaHelpers('http://cs.mcgill.ca/sel/ram/3.0')]

[import ca::mcgill::sel::ram::generator::common::commonHelpers /]


[**
 * Get the Java type name depending on the given type.
 */]
[template public getTypeName(t : Type)     /]
[template public getTypeName(t : RVoid)     ]void[/template]
[template public getTypeName(t : RAny)      ]Object[/template]
[template public getTypeName(t : ObjectType)][t.name/][/template]
[template public getTypeName(t : Classifier)][t.name/][t.generateClassifierGenericTypes()/][/template]
[template public getTypeName(t : REnum)     ][t.name/][/template]
[template public getTypeName(t : RArray)    ][t.oclAsType(RArray).type.getTypeName()/]['[]'/][/template]
[template public getTypeName(t : RLong)     ]long[/template]
[template public getTypeName(t : RBoolean)  ]boolean[/template]
[template public getTypeName(t : RInt)      ]int[/template]
[template public getTypeName(t : RChar)     ]char[/template]
[template public getTypeName(t : RString)   ]String[/template]
[template public getTypeName(t : RDouble)   ]double[/template]
[template public getTypeName(t : RFloat)    ]float[/template]
[template public getTypeName(t : TypeParameter)][if not t.genericType.oclIsUndefined()][t.genericType.getTypeName()/][else][t.name/][/if][/template]


[**
 * Get the default value of a Java type.
 */]
[template public getDefaultTypeValue(t : Type)     /]
[template public getDefaultTypeValue(t : RVoid)     ][/template]
[template public getDefaultTypeValue(t : RAny)      ]null[/template]
[template public getDefaultTypeValue(t : ObjectType)]null[/template]
[template public getDefaultTypeValue(t : RLong)     ]0[/template]
[template public getDefaultTypeValue(t : RBoolean)  ]false[/template]
[template public getDefaultTypeValue(t : RInt)      ]0[/template]
[template public getDefaultTypeValue(t : RChar)     ]0[/template]
[template public getDefaultTypeValue(t : RString)   ]""[/template]
[template public getDefaultTypeValue(t : RDouble)   ]0[/template]
[template public getDefaultTypeValue(t : RFloat)    ]0[/template]


[**
 * Get the generic types of a classifier.
 * @param cf The classifier
 */]
[template private generateClassifierGenericTypes(cf : Classifier) post(trim())]
[if (not cf.typeParameters->isEmpty())]
    <[for (tp : TypeParameter | cf.typeParameters) separator(', ')
    ][tp.getTypeName()/][/for]>
[/if]
[/template]


[**
 * Get the type name of a typed element.
 * @param te The typed element
 */]
[template public getTypeNameOfTypedElement(te : TypedElement) post(trim())]
[te.getType().getTypeName()/]
[/template]


[**
 * Get the type name of an association end.
 * @param ae The association end
 */]
[template public getTypeNameOfTypedElement(ae : AssociationEnd) post(trim())]
[let name : String = ae.getType().getTypeName()]
    [if (ae.isMultipleAssociation())]
        List<[name/]>
    [else]
        [name/]
    [/if]
[/let]
[/template]


[**
 * Get the package name (dot notation) of the given element (Class, Enum...).
 * This information is extracted from the parent aspect name.
 * @param e The element
 */]
[query public getPackageName(e : NamedElement) : String =
    e.eContainer(Aspect).name.toLowerCaseUnderscoreIdentifier()
/]

[**
 * Get the package name (dot notation) of the given element (Class, Enum...).
 * This information is extracted from the model's signature.
 * @param sig
 */]
[query public getSignaturePackageName(sig : ArtifactSignature) : String =
sig.groupId+'.'+sig.artifactId
/]


[**
 * Get the full file name of the given element (Class, Enum...).
 * @param e The element
 * @param flags Generator flags. Relevant here because we do not want to create an extra encompassing folder in case of a maven project.
 */]
[query public getFileName(e : NamedElement, flags : String) : String =
if flags.contains('MAVENSIGNATURE') then
    e.name + '.java'
else
	'./' + e.getPackageName().substituteAll('.', '/') + '/' + e.name + '.java'
endif    
/]


[**
 * Get the Java visibility name of the given operation.
 * @param o The operation
 */]
[query public getVisibilityName(op : Operation) : String =
    if op.visibility.oclIsUndefined() then
        'private'
    else if op.visibility = RAMVisibilityType::private then
        'private'
    else if op.visibility = RAMVisibilityType::protected then
        'protected'
    else if op.visibility = RAMVisibilityType::public then
        'public'
    else
        ''
    endif endif endif endif
/]


[**
 * Get the Java visibility name of the given operation with a trailing space if necessary.
 * @param o The operation
 */]
[query public getVisibilityNameWithTrailingSpace(op : Operation) : String =
    let name : String = op.getVisibilityName() in
    if name <> '' then
        name + ' '
    else
        ''
    endif
/]

[**
 * Returns the Java visibility name of the given classifier.
 * If the classifier is not public, an empty string is returned.
 * Otherwise, public with a trailing space is returned.
 * 
 * @param classifier the classifier to get the visibility for
 */]
[query public getVisibilityName(classifier : Classifier) : String =
    if (classifier.visibility.oclIsUndefined()
        or classifier.visibility = core::COREVisibilityType::concern) then
        ''
    else
        'public '
    endif
/]

[**
 * @Author Max
 * Get the Java visibility name of the given operation with a trailing space if necessary.
 * @param o The operation
 */]
[query public getAnnotationBody(a : Annotation) : String =
	a.matter
/]