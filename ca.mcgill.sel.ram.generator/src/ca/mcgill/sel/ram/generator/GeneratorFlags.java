package ca.mcgill.sel.ram.generator;

/**
 * Special flags to modify the generators behavior at runtime. At least one flag
 * must be provided when the java generator is called. The template signature
 * stipulates the presence of exactly one argument. Unfortunately acceleo does
 * not allow to pass the arguments as an array of variable length, so they have
 * to be passed as concatenated strings. (This happens in "DisplayHandler").
 * 
 * @author Maximilian Schiedermeier
 *
 */
public enum GeneratorFlags {

	/**
	 * Advises the generator to extract package information from the maven signature
	 * instead of using the enclosing aspect name.
	 */
	MAVENSIGNATURE,

	/**
	 * Default flag that should be used if no other flags are present. Acceleo fails
	 * to resolve the template if no arguments are provided, so we need a dummy flag
	 * to signal that there are no flags.
	 */
	VOID;

	/**
	 * Although implemented as Generics, acceleo crashes when fed with non-string
	 * arguments. We therefore convert all Enums to their string representation,
	 * before handing them on to acceleo.
	 */
	@Override
	public String toString() {
		return this.name();
	}
}
