package ca.mcgill.sel.ram.weaver;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREModelExtension;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.core.util.COREModelUtil;
import ca.mcgill.sel.core.weaver.COREModelWeaver;
import ca.mcgill.sel.core.weaver.util.WeavingInformation;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.PrimitiveType;
import ca.mcgill.sel.ram.RAny;
import ca.mcgill.sel.ram.REnum;
import ca.mcgill.sel.ram.RVoid;
import ca.mcgill.sel.ram.RamFactory;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.Traceable;
import ca.mcgill.sel.ram.Type;
import ca.mcgill.sel.ram.WovenAspect;
import ca.mcgill.sel.ram.util.RAMModelUtil;
import ca.mcgill.sel.ram.weaver.structuralview.StructuralViewWeaver;

/**
 * The RAMWeaver is capable of weaving two or more aspects together within a hierarchy.
 * This class is a singleton.
 * 
 * The weaver has two operations:
 * <ul>
 * <li><b>single weave:</b> weaving two directly dependent aspects together</li>
 * <li><b>complete weave (weave all):</b> weaves together all aspects within a hierarchy to receive an independent
 * aspect</li>
 * </ul>
 * 
 * For each two aspects that are woven together, the {@link ca.mcgill.sel.ram.StructuralView} is woven.
 * The {@link ca.mcgill.sel.ram.MessageView}s and {@link ca.mcgill.sel.ram.StateView}s are copied
 * from the lower-level to the higher-level aspect and are only woven at the very end,
 * when there are no dependencies left.
 *
 * @author mschoettle
 * @author walabe
 * @author abirayed
 * @author oalam
 */
// TODO: mschoettle: Use proper logger instead of System.out.
public final class RAMWeaver extends COREModelWeaver<Aspect> {

    /**
     * File to save an aspect temporarily.
     */
    private static final File TEMPORARY_ASPECT = new File("temp.ram");

    // private MessageViewWeaver messageViewWeaver;
    // private StateViewWeaver stateViewWeaver;

    /**
     * Creates a new instance of the weaver and initializes all required objects.
     */
    public RAMWeaver() {
        // messageViewWeaver = new MessageViewWeaver();
        // stateViewWeaver = new StateViewWeaver();
    }

//    /**
//     * Weaves an aspect hierarchy using the top down approach.
//     * Starts at the top of the hierarchy (with the aspect that should be woven)
//     * and subsequently weaves depending aspects into this aspect until no dependencies are left anymore.
//     * 
//     * Consider the following example:
//     * 
//     * <pre>
//     *        A
//     *      / | \
//     *     B  C  D
//     *    /  / \  \
//     *   E  F   G  H
//     *  /           \
//     * I             J
//     * </pre>
//     * 
//     * The algorithm will first weave B, C and D into A (= A')
//     * Then it will weave E, F, G and H into A' (= A'').
//     * Finally, it will weave I and J into A''.
//     * 
//     * @param aspect the aspect of which all dependent aspects should be woven into
//     * @return a woven aspect (copy) with no dependencies (all dependent aspects are woven into it)
//     */
//    public Aspect weaveAllTopDown(Aspect aspect) {
//        weavingInformation.clear();
//
//        Aspect result = weaveAllSingle(aspect);
//        weaveAll(result);
//
//        return result;
//    }
//
//    /**
//     * Weaves the complete hierarchy in a top-down manner with the exception that no
//     * state and message views are woven.
//     * This means, state and message views are only copied.
//     * 
//     * @param aspect the aspect of which all dependent aspects should be woven into
//     * @return a woven aspect (copy) with no dependencies (all dependent aspects are woven into it)
//     * @see #weaveAllTopDown(Aspect)
//     * @see #weaveModelExtension(Aspect, Instantiation)
//     */
//    public Aspect weaveAllSingle(Aspect aspect) {
//        weavingInformation.clear();
//
//        Aspect result = loadNewInstance(aspect);
//
//        while (!result.getModelExtensions().isEmpty() || !result.getModelReuses().isEmpty()) {
//            // Get the first level of compositions for this aspect ...
//            List<COREModelComposition> currentLevel = 
//                    new ArrayList<COREModelComposition>(result.getModelExtensions());
//            currentLevel.addAll(result.getModelReuses());
//            
//            // Weave only the first level ...
//            for (COREModelComposition composition : currentLevel) {
//                doWeaveSingle(result, composition);
//            }
//
//        }
//
//        return result;
//    }

//    /**
//     * Weaves the state machines of the state views.
//     * Doesn't take into account the dependencies and instantiations of the aspect.
//     * 
//     * @param aspect the aspect to weave state machines for
//     * @param listener to notify when events occur during weaving
//     */
//    public void weaveStateMachines(Aspect aspect, WeaverListener<Aspect> listener) {
//        try {
//            listener.weavingStarted();
//
//            Aspect result = loadNewInstance(aspect);
//            stateViewWeaver.weaveStateViews(result);
//
//            listener.weavingFinished(result);
//            // CHECKSTYLE:IGNORE IllegalCatch: There could be many different exceptions during initialization.
//        } catch (Exception e) {
//            // TODO: catch in lower level weaver operations, and throw custom exception.
//            listener.weavingFailed(e);
//        }
//
//    }
//
//    /**
//     * Weaves the state machines of the state views.
//     * Doesn't apply the CSP composition to the copied state machines.
//     * 
//     * @param aspect the aspect to weave state machines for
//     * @param listener to notify when events occur during weaving
//     */
//    public void weaveAllNoCSPWeavingForStateViews(Aspect aspect, WeaverListener<Aspect> listener) {
//        try {
//            listener.weavingStarted();
//
//            Aspect result = loadNewInstance(aspect);
//            weavingInformation.clear();
//            // Do phase 1: Weave all structural views and copy the message and state views
//            // from the lower- to the higher-level aspect.
//            resolveDependenciesBottomUp(result);
//
//            messageViewWeaver.weaveMessageViews(aspect, weavingInformation);
//
//            listener.weavingFinished(result);
//            // CHECKSTYLE:IGNORE IllegalCatch: There could be many different exceptions during initialization.
//        } catch (Exception e) {
//            // TODO: catch in lower level weaver operations, and throw custom exception.
//            listener.weavingFailed(e);
//        }
//    }

//    /**
//     * Performs the final step of weaving.
//     * I.e., message views and state views are woven.
//     * The actions are performed on the given aspect directly.
//     * 
//     * @param aspect the aspect for which views should be woven
//     */
//    public void weaveAll(Aspect aspect) {
//        messageViewWeaver.weaveMessageViews(aspect, weavingInformation);
//        stateViewWeaver.weaveStateViews(aspect);
//    }
//

    /**
     * Weaves two directly dependent aspects together (single weave).
     * Weaves the structural view and copies the message and states view
     * of the source model of the composition to the base model.
     * 
     * @param externalBase the COREExternalArtefact of the base
     * @param target the aspect to weave into
     * @param composition the COREModelComposition for the aspect that is to be woven
     * @param weavingInfo the weaving information which is going to remember which elements
     *        from the source model are mapped to which model elements in the base model
     *        and vice versa.
     * @param topdown a boolean that indicates whether we are doing top down or bottom up weaving.
     *        when weaving topdown, names of mapped structural elements have to be copied.
     * 
     * @return the augmented base model containing the model elements from the source model as well
     */
    public Aspect weaveModel(COREExternalArtefact externalBase, Aspect target, COREModelComposition composition,
            WeavingInformation weavingInfo, boolean topdown) {
        
        Aspect sourceAspect = (Aspect) ((COREExternalArtefact) composition.getSource()).getRootModelElement();

        // add primitive type mappings to weaving information so that the primitive types are updated correctly 
        for (final Type typeFromSource : sourceAspect.getStructuralView().getTypes()) {
            // we only create a mapping if the base Type is a primitive type
            if (typeFromSource instanceof PrimitiveType) {
                final PrimitiveType primitiveTypeFromBase;
                primitiveTypeFromBase = getPrimitiveTypeFromAspect(target,
                        typeFromSource.getName());
                if (primitiveTypeFromBase != null) {
                    // TODO if class was mapped to primitive type by user (e.g., Map: Key -> String),
                    // the mapping in mappings will be overridden by the regular primitive type mapping
                    weavingInfo.add(typeFromSource, primitiveTypeFromBase);
                }
            } else if (typeFromSource instanceof RVoid) {
                final Type voidTypeFromBase;
                voidTypeFromBase = getVoidTypeFromAspect(target);
                if (voidTypeFromBase != null) {
                    // TODO if class was mapped to primitive type by user (e.g., Map: Key -> String),
                    // the mapping in mappings will be overriden by the regular primitive type mapping
                    weavingInfo.add(typeFromSource, voidTypeFromBase);
                }
            } else if (typeFromSource instanceof RAny) {
                final Type anyTypeFromBase;
                anyTypeFromBase = getAnyTypeFromAspect(target);
                if (anyTypeFromBase != null) {
                    // TODO if class was mapped to primitive type by user (e.g., Map: Key -> String),
                    // the mapping in mappings will be overriden by the regular primitive type mapping
                    weavingInfo.add(typeFromSource, anyTypeFromBase);
                }
            }
        }
        
        StructuralViewWeaver structuralViewWeaver = StructuralViewWeaver.getInstance();

        // CHECKSTYLE:IGNORE ParameterAssignment FOR 2 LINES: Needed, because the operation returns the result,
        // however it returns base.
        target = structuralViewWeaver.weaveModel(externalBase, target, composition, weavingInfo, topdown);


        createTrace(target, (Aspect) ((COREExternalArtefact) composition.getSource()).getRootModelElement(),
                weavingInfo, composition instanceof COREModelExtension);

        // Merge weaving information, to be able to look at woven elements later.
        // this.weavingInformation.add(currentWeavingInformation); (not needed anymore, I think)

        //messageViewWeaver.copyMessageViews(base, (Aspect) composition.getSource(), currentWeavingInformation);
        //StateViewWeaverUtils.copyStateViews(base, (Aspect) composition.getSource(), currentWeavingInformation);

//        if (composition instanceof COREModelReuse) {
//            base.getModelReuses().remove(composition);
//        } else {
//            base.getModelExtensions().remove(composition);
//        }

        // RAMReferenceUpdater updater = RAMReferenceUpdater.getInstance();

        // Update the structural view.
        // updater.update(base.getStructuralView(), weavingInfo);
        
        // rename the aspect
        target.setName(target.getName().concat("_").concat(sourceAspect.getName()));
                
        return target;
    }
    
//    /**
//     * Performs the step of a single weave.
//     * The source aspect of the composition is woven into the base aspect.
//     * 
//     * @param base the aspect that is woven into
//     * @param composition the composition referring to the aspect to weave into the base aspect
//     */
//    public void doWeaveSingle(Aspect base, COREModelComposition composition) {
//        System.out.println("Weaving " + composition.getSource().getName() + " into " + base.getName());
//
//        StructuralViewWeaver structuralViewWeaver = StructuralViewWeaver.getInstance();
//        for (COREModelReuse modelReuse : composition.getSource().getModelReuses()) {
//            COREModelReuse copy = EcoreUtil.copy(modelReuse);
//            base.getModelReuses().add(copy);
//        }
//
//        // CHECKSTYLE:IGNORE ParameterAssignment FOR 2 LINES: Needed, because the operation returns the result,
//        // however it returns base.
//        base = structuralViewWeaver.weave(base, composition);
//
//        WeavingInformation currentWeavingInformation = structuralViewWeaver.getWeavingInformation();
//
//        // Update the model reuses.
//        //RAMReferenceUpdater.getInstance().update(base.getModelReuses(), currentWeavingInformation);
//
////        createTrace(base, (Aspect) composition.getSource(), currentWeavingInformation,
////                composition instanceof COREModelExtension);
////
//        // Merge weaving information, to be able to look at woven elements later.
//        this.weavingInformation.add(currentWeavingInformation);
//
//        //messageViewWeaver.copyMessageViews(base, (Aspect) composition.getSource(), currentWeavingInformation);
//        //StateViewWeaverUtils.copyStateViews(base, (Aspect) composition.getSource(), currentWeavingInformation);
//    }
//
    /**
     * Create a trace of each traceable element in the woven aspect.
     * 
     * @param base the aspect we are adding tracing information to
     * @param wovenAspect the aspect woven into the base
     * @param currentWeavingInformation contains all the weaving information of the current weaving
     * @param extend whether the aspect comes from an extending aspect (true) or a reuse (false)
     */
    private void createTrace(Aspect base, Aspect wovenAspect, WeavingInformation currentWeavingInformation,
            boolean extend) {
        WovenAspect wovenAspectTracing = RamFactory.eINSTANCE.createWovenAspect();
        // TODO: Might have to be the textual representation of the aspect name...
        wovenAspectTracing.setName(wovenAspect.getName());
        wovenAspectTracing.setComesFrom(wovenAspect);
        // Create tracing hierarchy
        createTracingHierarchy(wovenAspect, wovenAspectTracing);
        int index = extend ? 0 : base.getWovenAspects().size();

        base.getWovenAspects().add(index, wovenAspectTracing);
        // associate the wovenAspects to the Traceable elements
        createTrace(currentWeavingInformation, wovenAspectTracing);
    }

    /**
     * Creates the hierarchy of {@link WovenAspect} associated for wovenAspect.
     * This hierarchy will contain the tracing information.
     * Realizes a copy of the WovenAspects so that they don't directly own references to the tracing elements
     * 
     * @param wovenAspect the {@link Aspect} associated to the tracing hierarchy
     * @param wovenAspectTracing the {@link WovenAspect} to add the hierarchy to
     */
    private void createTracingHierarchy(Aspect wovenAspect, WovenAspect wovenAspectTracing) {
        for (WovenAspect child : wovenAspect.getWovenAspects()) {
            WovenAspect childCopy = EcoreUtil.copy(child);
            wovenAspectTracing.getChildren().add(childCopy);
        }
    }

    /**
     * Creates tracing information. For each WovenAspect in the hierarchy,
     * associate it with the correct {@link Traceable} for tracing
     * The WovenAspect will update tracing information recursively for its children in the hierarchy
     * 
     * @param currentWeavingInformation the current {@link WeavingInformation}
     * @param wovenAspectTracing the {@link WovenAspect} to add tracing information to
     */
    public void createTrace(WeavingInformation currentWeavingInformation, WovenAspect wovenAspectTracing) {
        for (EObject toElement : currentWeavingInformation.getToElements()) {
            if (toElement instanceof Traceable 
                    && ((toElement instanceof REnum) || !(toElement instanceof PrimitiveType))) {
                // add the element to the parent WovenAspect
                Traceable to = (Traceable) toElement;
                Traceable from = resolveFromElement(to, currentWeavingInformation);
                wovenAspectTracing.getWovenElements().put(to, from);
                // update the children
                for (WovenAspect wa : wovenAspectTracing.getChildren()) {
                    updateChildren(wa, (Traceable) toElement, currentWeavingInformation);
                }
            }
        }
    }

    /**
     * Get the highest level 'from' element for the given element, if it exists.
     * Looks through the {@link WeavingInformation} for mapped elements. For each of them, will make them refer to
     * the base model according on previous tracing information. For instance if we have in weavingInfo
     * op() -> woven_op(). In the woven aspect where woven_op() is defined, there should be tracing information
     * with woven_op() -> base_op(). op() is then resolved to base_op().
     * If several elements come out of this operation, the highest level one will be chosen.
     * If no element comes out it means the toElement does not come from a different aspect.
     *
     * @param toElement - The element we want the original element for.
     * @param currentWeavingInformation - Current mappings made by the weaver
     * @return The highest level element, if it exists, null otherwise.
     */
    private Traceable resolveFromElement(Traceable toElement, WeavingInformation currentWeavingInformation) {
        List<Traceable> froms = currentWeavingInformation.getFromElements(toElement);
        List<Traceable> resolved = new LinkedList<Traceable>();
        for (Traceable from : froms) {
            // Get the first fromElement for this Traceable.
            Traceable resolvedFrom = RAMModelUtil.getOriginElement(from);
            if (resolvedFrom != null) {
                resolved.add(resolvedFrom);
            } else {
                resolved.add(from);
            }
        }
        return getHighestLevelElement(resolved);
    }

    /**
     * Goes through a list of elements and return the one coming from the highest level in the hierarchy of aspects.
     *
     * @param elements - the elements to examine
     * @return the one element that is higher or same level as all the others. null if the list was empty.
     */
    private Traceable getHighestLevelElement(List<Traceable> elements) {
        Traceable highestLevel = null;
        Aspect highestAspect = null;
        for (Traceable traceable : elements) {
            Aspect asp = EMFModelUtil.getRootContainerOfType(traceable, RamPackage.Literals.ASPECT);
            if (highestLevel == null || highestAspect == null || (asp != null && asp != highestAspect
                    && COREModelUtil.collectExtendedModels(highestAspect).contains(
                    COREArtefactUtil.getReferencingExternalArtefact(asp)))) {
                highestLevel = traceable;
                highestAspect = asp;
            }
        }
        return highestLevel;
    }

    /**
     * This is a helper method that searches all the primitive types in the
     * aspect and tries to find one that matches by name.
     * @param aspect The aspect to search in
     * @param name The name of the primitive type to search for
     * @return The primitive type in the aspect if found otherwise null if not found.
     */
    private static PrimitiveType getPrimitiveTypeFromAspect(final Aspect aspect, final String name) {
        for (final Type primitiveTypeFromAspect : aspect.getStructuralView().getTypes()) {
            if (primitiveTypeFromAspect instanceof PrimitiveType
                    &&
                    primitiveTypeFromAspect.getName().equals(name)) {
                return (PrimitiveType) primitiveTypeFromAspect;
            }
        }
        return null;
    }
    

    /**
     * This is a helper method that searches all the primitive types in the
     * lower level aspect and tries to find RAny type.
     * @param aspect The aspect to search in
     * @return The RAny type in the aspect if found otherwise null if not found.
     */
    private static RAny getAnyTypeFromAspect(final Aspect aspect) {
        for (final Type anyTypeFromAspect : aspect.getStructuralView().getTypes()) {
            if (anyTypeFromAspect instanceof RAny) {
                return (RAny) anyTypeFromAspect;
            }
        }
        return null;
    }
    
    /**
     * This is a helper method that searches all the primitive types in the
     * lower level aspect and tries to find one that matches name.
     * @param aspect The aspect to search in
     * @return The RVoid type in the aspect if found otherwise null if not found.
     */
    private static RVoid getVoidTypeFromAspect(final Aspect aspect) {
        for (final Type voidTypeFromAspect : aspect.getStructuralView().getTypes()) {
            if (voidTypeFromAspect instanceof RVoid) {
                return (RVoid) voidTypeFromAspect;
            }
        }
        return null;
    }
    
    /**
     * Creates tracing information for the given WovenAspect.
     * Looks inside the {@link WovenAspect#getWovenElements()} for the given {@link Traceable} T in the
     * {@link WeavingInformation} as a key for mapping and update the aspect accordingly.
     * For example if A is in the aspect wovens elements and we find a mapping A->toElement,
     * we have to add toElement to the wovenAspect wovenElements too.
     * Tracing information will be update like so recursively for all children in the hierarchy.
     * 
     * @param wovenAspect the {@link WovenAspect} to update
     * @param toElement the {@link Traceable} to look for
     * @param currentweavingInfo the {@link WeavingInformation} to search the mappings in.
     */
    private void updateChildren(WovenAspect wovenAspect, Traceable toElement, WeavingInformation currentweavingInfo) {
        List<Traceable> fromElements = new LinkedList<Traceable>();
        EMap<Traceable, Traceable> wovenElements = wovenAspect.getWovenElements();

        // toElement is the new element from the woven aspect we want to map.
        // We need to save it as a key and make it refer to froms from base models.
        for (Traceable from : currentweavingInfo.getFromElements(toElement)) {
            if (wovenElements.containsKey(from)) {
                fromElements.add(from);
            }
        }

        if (!fromElements.isEmpty() && !wovenElements.containsKey(toElement)) {
            // Add the element. If everything was constructed correctly there should be only one.
            for (Traceable from : fromElements) {
                Traceable newFrom = wovenElements.get(from);
                wovenElements.put(toElement, newFrom);
            }
            // Clear list of woven elements of non useful items.
            for (Traceable fromElt : fromElements) {
                boolean toRemove = true;
                for (Traceable toElt : currentweavingInfo.getToElements(fromElt)) {
                    if (!wovenElements.containsKey(toElt)) {
                        toRemove = false;
                        break;
                    }
                }
                if (toRemove) {
                    wovenElements.removeKey(fromElt);
                }
            }
        }

        // update the tracing information recursively for all children
        for (WovenAspect wa : wovenAspect.getChildren()) {
            updateChildren(wa, toElement, currentweavingInfo);
        }
    }

    /**
     * Loads a new instance of an aspect. This is required, because lower-level aspects
     * that contain dependencies will be modified, i.e., it's dependencies are
     * woven into it. If this (lower-level) aspect is loaded somewhere else, e.g., a GUI,
     * the modifications are reflected there, which is unwanted behavior.
     * If the aspect were cloned, it would require the mappings of the instantiation
     * for this lower-level aspect to be updated to the new elements, which would
     * be more time-consuming to do.
     * Therefore, to circumvent this, the aspect is saved in a temporary file
     * and loaded again using a separate resource set, which forces to load
     * other aspects to be loaded as new instances. Otherwise the existing
     * resource set would get the resources for the lower-level aspects
     * from its cache.
     * 
     * @param aspect the aspect to load a new instance for
     * @return a new instance of the given aspect
     */
    public Aspect loadNewInstance(Aspect aspect) {
        // Given aspect has to be cloned. Otherwise, when adding the aspect to the new resource
        // it gets removed from its current resource. This will mean that the given aspect
        // is directly modified.
        Aspect result = EcoreUtil.copy(aspect);

        String pathName = TEMPORARY_ASPECT.getAbsolutePath();

        // Use our own resource set for saving and loading to workaround the issue.
        ResourceSet resourceSet = new ResourceSetImpl();

        // Create a resource to temporarily save the aspect.
        Resource resource = resourceSet.createResource(URI.createFileURI(pathName));
        resource.getContents().add(result);

        try {
            resource.save(Collections.EMPTY_MAP);

            // Load the temporary aspect ...
            resource = resourceSet.getResource(URI.createFileURI(pathName), true);
            result = (Aspect) resource.getContents().get(0);

            // Copy the aspect to loose the reference to the temporary file...
            result = EcoreUtil.copy(result);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Delete the temporary file ...
        TEMPORARY_ASPECT.delete();

        return result;
    }

    /** Creates an empty aspect.
     * @return the new, empty aspect
     */
    public Aspect createEmptyModel() {
        Aspect newAspect = RAMModelUtil.createAspect("newAspect");
        return newAspect;
    }
}
