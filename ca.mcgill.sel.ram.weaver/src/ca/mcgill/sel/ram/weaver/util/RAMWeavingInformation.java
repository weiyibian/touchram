package ca.mcgill.sel.ram.weaver.util;

import java.util.HashMap;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.core.weaver.util.WeavingInformation;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.NamedElement;
import ca.mcgill.sel.ram.StructuralView;

/**
 * Stores information on what element from the lower-level aspect (from element)
 * is woven to which element in the higher-level aspect (to element).
 * The weaving information can be considered a map, that maps from lower-level
 * to higher-level and higher-level to lower-level elements.
 * The information can be retrieved either using the from or to element.
 * Since a lower-level element can be mapped to more than one element
 * in the higher-level, getting the corresponding to element using the from element
 * retrieves a list of elements.
 * 
 * @author mschoettle
 */
public class RAMWeavingInformation extends WeavingInformation {

    @Override
    public void add(EObject fromElement, EObject toElement) {
        if (!toElement.eClass().isInstance(fromElement)) {
            // Mapping a class to an implementation class is fine, though,
            // e.g. Key (Class) is mapped to RString (ImplementationClass).
            if (!(toElement instanceof Classifier && fromElement instanceof Classifier)) {
                throw new IllegalArgumentException("from and to element have to be of the same type");
            }
        }
        super.add(fromElement, toElement);
    }
    
    @Override
    public WeavingInformation clone() {
        WeavingInformation clone = new RAMWeavingInformation();

        clone.setFromTosMap(new HashMap<EObject, List<EObject>>(fromTosMap));

        return clone;
    }

    @Override
    protected void print(EObject object) {
        if (object instanceof NamedElement) {
            System.out.print(((NamedElement) object).getName());
        } else {
            System.out.print(object);
        }

        System.out.print(" [" + object.eClass().getName() + "]");

        EObject container = object.eContainer();

        if (!(container instanceof StructuralView) && container instanceof NamedElement) {
            System.out.print(" (");
            print(container);
            System.out.print(")");
        }
    }

}
