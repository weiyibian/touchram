package ca.mcgill.sel.ram.weaver.structuralview;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;

import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREModelExtension;
import ca.mcgill.sel.core.util.COREInterfaceUtil;
import ca.mcgill.sel.core.weaver.COREWeaver;
import ca.mcgill.sel.core.weaver.util.COREModelElementReferenceUpdater;
import ca.mcgill.sel.core.weaver.util.COREReferenceUpdater;
import ca.mcgill.sel.core.weaver.util.WeavingInformation;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.Association;
import ca.mcgill.sel.ram.AssociationEnd;
import ca.mcgill.sel.ram.Attribute;
import ca.mcgill.sel.ram.Class;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.ImplementationClass;
import ca.mcgill.sel.ram.Operation;
import ca.mcgill.sel.ram.Parameter;
import ca.mcgill.sel.ram.RAMVisibilityType;
import ca.mcgill.sel.ram.REnum;
import ca.mcgill.sel.ram.REnumLiteral;
import ca.mcgill.sel.ram.Type;

/**
 * This is the class that represents our structural view weaver.
 * @author walabe
 *
 */
public final class StructuralViewWeaver {
    
    /**
     * The singleton instance.
     */
    private static StructuralViewWeaver instance;
            
    /**
     * Our Constructor it initializes our attributes.
     */
    private StructuralViewWeaver() {
    }
    
    /**
     * Returns the singleton instance of the {@link StructuralViewWeaver}.
     * 
     * @return the singleton instance
     */
    public static StructuralViewWeaver getInstance() {
        if (instance == null) {
            instance = new StructuralViewWeaver();
        }
        
        return instance;
    }
    
    /**
     * The main method of our class structural view weaver
     * that is called to perform weaving for a given instantiation.
     * @param baseArtefact the artefact that refers to the base aspect
     * @param base The aspect we want to weave into
     * @param composition The COREModelComposition to the aspect we want to weave into base
     * @param weavingInfo the weaving information that needs to be updated
     * @param topdown boolean that indicates whether we are doing top down or bottom up weaving
     * @return The newly woven aspect
     */
    public Aspect weaveModel(COREExternalArtefact baseArtefact, Aspect base, COREModelComposition composition,
            WeavingInformation weavingInfo, boolean topdown) {
                
        Aspect sourceAspect = (Aspect) ((COREExternalArtefact) composition.getSource()).getRootModelElement();

        // First go through all classifiers in the source model, and either merge or copy them into the
        // base model
        for (Iterator<Classifier> iterator = sourceAspect.getStructuralView().getClasses().iterator(); iterator
                .hasNext(); ) {
            Classifier classFromSource = iterator.next();
            
            System.out.print("Handling " + classFromSource.getName() + ", which is ");
            
            // check if the class has to be woven
            if (weavingInfo.wasWoven(classFromSource)) {
                List<Classifier> mappedToBaseClasses = weavingInfo.getToElements(classFromSource);
                ArrayList<Classifier> replacedMappedToClasses = new ArrayList<Classifier>();
                ArrayList<Classifier> newImplentationClasses = new ArrayList<Classifier>();
                System.out.println(" mapped to: " + mappedToBaseClasses);
                // merge class
                for (Classifier baseTargetClass : mappedToBaseClasses) {
                    if (topdown) {
                        if (classFromSource instanceof ImplementationClass) {
                            // if we are weaving topdown and the source class is an implementation class,
                            // then we need to make the target class in the base an implementation class.
                            // The only way to do that is to create a new implementation class in the base,
                            // and redirect all the references.
                            ImplementationClass newImplementationClass = EcoreUtil.copy(
                                    (ImplementationClass) classFromSource);
                            base.getStructuralView().getClasses().add(newImplementationClass);  
                            // Now copy all properties to the implementation class
                            newImplementationClass.getAssociationEnds().addAll(baseTargetClass.getAssociationEnds());
                            // Now redirect all references in the woven aspect to the new implementation class
                            for (EStructuralFeature.Setting s 
                                : EcoreUtil.UsageCrossReferencer.find(baseTargetClass, base)) {
                                s.set(newImplementationClass);
                            }
                            // Now remove the non-implementation class in the base
                            base.getStructuralView().getClasses().remove(baseTargetClass);
                            // finally, update the weaving info
                            replacedMappedToClasses.add(baseTargetClass);
                            newImplentationClasses.add(newImplementationClass);
                        } else {
                            // if we are weaving topdown we need to copy the name
                            baseTargetClass.setName(classFromSource.getName());
                        }
                    }
                    if (baseTargetClass instanceof Class && classFromSource instanceof Class) {
                        weaveAttributes((Class) baseTargetClass, (Class) classFromSource, weavingInfo, topdown);
                        weaveOperations(baseTargetClass, classFromSource,
                                base, baseArtefact, composition instanceof COREModelExtension, weavingInfo, topdown);
                        
                        // Weave inheritance
                        // Although this code will set the supertype reference to point to the super class in
                        // the source model, the reference updater will take care of changing the reference to
                        // the class in the base model eventually
                        for (Classifier parent : classFromSource.getSuperTypes()) {                    
                            if (!baseTargetClass.getSuperTypes().contains(parent)) {
                                baseTargetClass.getSuperTypes().add(parent);
                            }
                        }
                    }
                }
                // update the weaving information for all classes that were mapped to
                // implementation classes
                if (topdown && classFromSource instanceof ImplementationClass) {
                    for (int i = 0; i < replacedMappedToClasses.size(); i++) {
                        mappedToBaseClasses.remove(replacedMappedToClasses.get(i));
                        mappedToBaseClasses.add(newImplentationClasses.get(i));
                    }
                }

            } else {
                // there is no user-defined mapping, therefore we simply copy the model element from
                // the source model into the base
                System.out.println("not mapped");
                Classifier modelElementCopy = COREWeaver.copyModelElement(classFromSource, baseArtefact,
                        weavingInfo, composition);
                base.getStructuralView().getClasses().add(modelElementCopy);               
            }
        }
        
        // now look for enums and do the same
        for (Iterator<Type> iterator = sourceAspect.getStructuralView().getTypes().iterator();
                iterator.hasNext(); ) {
            Type typeFromSource = iterator.next();
            
            if (typeFromSource instanceof REnum) {
                REnum enumFromSource = (REnum) typeFromSource;
                System.out.print("Handling " + enumFromSource.getName() + ", which is ");

                if (weavingInfo.wasWoven(enumFromSource)) {
                    List<REnum> mappedToBaseEnums = weavingInfo.getToElements(enumFromSource);                
                    System.out.println(" mapped to: " + mappedToBaseEnums);
                    for (REnum destEnum : mappedToBaseEnums) {
                        weaveEnumLiterals(destEnum, enumFromSource, weavingInfo);
                    }
                } else {
                    System.out.println("not mapped");
                    REnum modelElementCopy = COREWeaver.copyModelElement(enumFromSource, baseArtefact,
                        weavingInfo, composition);
                    base.getStructuralView().getTypes().add(modelElementCopy); 
                }
            }
        }
        
        // now copy new associations and association ends
        if (sourceAspect.getStructuralView().getAssociations() != null) {
            for (Iterator<Association> iterator = sourceAspect.getStructuralView().getAssociations().iterator();
                    iterator.hasNext(); ) {
                Association associationFromSource = iterator.next();
                if (!weavingInfo.wasWoven(associationFromSource)) {
                    // there is no user-defined mapping, therefore we simply copy the association from
                    // the source model into the base
                    System.out.println(associationFromSource.getName() + " not mapped");                
                    Association modelElementCopy = COREWeaver.copyModelElement(associationFromSource, baseArtefact,
                            weavingInfo, composition);
                    base.getStructuralView().getAssociations().add(modelElementCopy);
                    
                    // find where the association ends go and add them to the appropriate classes
                    for (AssociationEnd ae : associationFromSource.getEnds()) {
                        Classifier sourceClass = ae.getClassifier();
                        Classifier baseClass = weavingInfo.getFirstToElement(sourceClass);
                        AssociationEnd newAe = COREWeaver.copyModelElement(ae, baseArtefact,
                                weavingInfo, composition);
                        baseClass.getAssociationEnds().add(newAe);
                        modelElementCopy.getEnds().add(newAe);
                    }               
                }
            }
        }
        
        // handle associations
//        if (composition instanceof COREModelReuse) {
//            String concernName = ((COREModelReuse) composition).getReuse().getReusedConcern().getName();
//            if (Constants.ASSOCIATION_CONCERN_NAME.equals(concernName)) {
//                
//            }
//            
//        }
        
        // now update any references that still refer to the external source model
        COREReferenceUpdater updater = COREModelElementReferenceUpdater.getInstance();
        updater.update(base.getStructuralView(), weavingInfo);
        return base;
    }   

    /**
     * This method performs weaving of attributes.
     * @param baseClass The class we are merging classFromLowerLevel into
     * @param sourceClass The class that will be merged into classFromHigherLevel
     * @param weavingInformation The data structure containing all mapping information
     * @param topdown boolean that indicates whether we are doing top down or bottom up weaving
     */
    public static void weaveAttributes(final Class baseClass, final Class sourceClass,
            final WeavingInformation weavingInformation, boolean topdown) {
        
        for (final Attribute sourceAttribute : sourceClass.getAttributes()) {
            // check to see if the attribute was not mapped
            // if it was mapped it means we are renaming hence we do not need to modify
            // anything in the higher level class
            if (!weavingInformation.wasWoven(sourceAttribute)) {
                // create a copy of the attribute that we will add to classFromHigherLevel
                final Attribute baseAttribute = EcoreUtil.copy(sourceAttribute);
                
                // create a mapping between attributes indicating they have been woven
                weavingInformation.add(sourceAttribute, baseAttribute);
                // and add the attribute to the class from the base
                baseClass.getAttributes().add(baseAttribute);
            } else if (topdown) {
                for (Attribute destinationAttribute : weavingInformation.getToElements(sourceAttribute)) {
                    destinationAttribute.setName(sourceAttribute.getName());
                }
            }
        }
    }    

    /**
     * This is the main method that performs weaving of operations.
     * 
     * @param baseClass The class that classFromLowerLevel is being woven into
     * @param sourceClass The class that will contain the operations we want to weave into classFromHigherLevel
     * @param baseAspect The aspect that classFromHigherLevel belongs to
     * @param isExtends Represents the type of weaving extends or depends
     * @param weavingInformation The data structure containing all mapping information
     * @param baseArtefact the artefact we're weaving into
     * @param topdown boolean that indicates whether we are doing top down or bottom up weaving
     */
    public static void weaveOperations(final Classifier baseClass, final Classifier sourceClass,
            final Aspect baseAspect, final COREExternalArtefact baseArtefact, 
            final boolean isExtends, final WeavingInformation weavingInformation, boolean topdown) {

        for (final Operation opFromLowerLevel : sourceClass.getOperations()) {
            boolean opMapped = false;
            // First check to see if this operation was mapped.
            // See issue #112.
            List<Operation> toElements = weavingInformation.getToElements(opFromLowerLevel);
            if (toElements != null) {
                for (final Operation key : toElements) {
                    if (key.eContainer().equals(baseClass)) {
                        // Operation was mapped by user, operation already exists in weaving information,
                        // but not it's parameters, therefore, create weaving information for parameters
                        createParameterWeavingInformation(opFromLowerLevel, key, weavingInformation);
                        opMapped = true;
                        break;
                    }
                }
            }

            if (!opMapped) {
                final Operation opFromHigherLevel = getExistingOperation(baseClass, opFromLowerLevel,
                        weavingInformation);
                if (opFromHigherLevel != null) {
                    weavingInformation.add(opFromLowerLevel, opFromHigherLevel);
                    createParameterWeavingInformation(opFromLowerLevel, opFromHigherLevel, weavingInformation);
                } else {
                    // TODO check parameters
                    Operation opCopy = EcoreUtil.copy(opFromLowerLevel);

                    if (!isExtends && !(baseClass instanceof ImplementationClass)
                            && COREInterfaceUtil.isPublic(baseArtefact, opFromLowerLevel)) {
                        //TODO: set the visibilty of the operation to CONCERN!
                        //COREInterfaceUtil.(COREVisibilityType.CONCERN);
                        opCopy.setVisibility(RAMVisibilityType.PACKAGE);
                    } else {
                        opCopy.setVisibility(opFromLowerLevel.getVisibility());
                        //TODO: copy the visibilty of the operation
                        //opCopy.setExtendedVisibility(opFromLowerLevel.getExtendedVisibility());
                    }

                    baseClass.getOperations().add(opCopy);
                    weavingInformation.add(opFromLowerLevel, opCopy);

                    createParameterWeavingInformation(opFromLowerLevel, opCopy, weavingInformation);
                }
            }
        }
    }

    /**
     * Operations are sometimes just created to be checked
     * in createOpCopyInHigherLevel, but never added. Therefore, parameters
     * are "mapped" only when in certain circumstances (e.g., operation is
     * actually added to a class).
     * 
     * @param original The original operation
     * @param copy The copy of original
     * @param weavingInformation The data structure containing all mapping information
     */
    public static void createParameterWeavingInformation(Operation original, Operation copy,
            WeavingInformation weavingInformation) {
        for (int index = 0; index < original.getParameters().size(); index++) {
            Parameter oldParameter = original.getParameters().get(index);
            Parameter newParameter = copy.getParameters().get(index);
            weavingInformation.add(oldParameter, newParameter);
        }
    }

    /**
     * This method will search in parentClass for an operation that matches
     * opToLookFor by signature.
     * 
     * @param baseClass The class to search in.
     * @param sourceClass The operation from lower level class to look for in classFromHigherLevel.
     * @param weavingInformation The data structure containing all mapping information.
     * @return the operation in the higher level class matching operation from lower level class
     *         or null if there is no match.
     */
    public static Operation getExistingOperation(Classifier baseClass, Operation sourceClass,
            WeavingInformation weavingInformation) {
        for (final Operation baseOperation : baseClass.getOperations()) {
            if (baseOperation.getName().equals(sourceClass.getName())) {
                boolean match = false;
                if (weavingInformation.wasWoven(sourceClass.getReturnType())) {
                    match = weavingInformation.getFirstToElement(sourceClass.getReturnType()).equals(
                            baseOperation.getReturnType());
                }
                match = match
                        || sourceClass.getReturnType().equals(baseOperation.getReturnType());

                if (match
                        && baseOperation.getParameters().size() == sourceClass.getParameters()
                                .size()
                        && sameParameters(baseOperation, sourceClass, weavingInformation)) {
                    return baseOperation;
                }
            }
        }
        return null;
    }

    /**
     * This method will compare the parameters of two operations to
     * see if they match. For a match to occur the two operations must
     * have the same number of parameters in the same order and for
     * each parameter must have the same name and same type.
     * 
     * @param baseOperation The operation from higher level class
     * @param sourceOperation The operation from lower level class
     * @param weavingInformation The data structure containing all mapping information
     * @return true if the parameters match or false otherwise
     */
    private static boolean sameParameters(final Operation baseOperation,
            final Operation sourceOperation, WeavingInformation weavingInformation) {
        boolean same = false;
        if (baseOperation.getParameters().size() == sourceOperation.getParameters().size()) {
            same = true;
            for (int index = 0; index < baseOperation.getParameters().size(); index++) {
                final Parameter baseParameter = baseOperation.getParameters().get(index);
                final Parameter sourceParameter = sourceOperation.getParameters().get(index);
                if (weavingInformation.wasWoven(sourceParameter)) {
                    same = weavingInformation.getFirstToElement(sourceParameter).equals(baseParameter);
                }

                if (weavingInformation.wasWoven(sourceParameter.getType())) {
                    same = same
                            && (weavingInformation.getFirstToElement(sourceParameter.getType()).equals(
                                    baseParameter.getType()) || sourceParameter.getType().equals(
                                    baseParameter.getType()));
                } else {
                    same = same && sourceParameter.getType().equals(baseParameter.getType());
                }

                // TODO check if there is a mapping either names have to be the same or there is a mapping
            }
        }
        return same;
    }

    /**
     * This method performs weaving of Enum Literals.
     * @param destinationEnum The enum we are merging sourceEnum into
     * @param sourceEnum The enum that will be merged into destinationEnum
     * @param weavingInformation The data structure containing all mapping information
     */
    public static void weaveEnumLiterals(final REnum destinationEnum, final REnum sourceEnum,
            final WeavingInformation weavingInformation) {
        
        for (final REnumLiteral sourceLiteral : sourceEnum.getLiterals()) {
            // if it was mapped it means we are renaming hence we do not need to modify
            // anything in the destination, otherwise we copy
            if (!weavingInformation.wasWoven(sourceLiteral)) {
                // create a copy of the literal that we will add to destinationEnum
                final REnumLiteral destinationLiteral = EcoreUtil.copy(sourceLiteral);
                
                // create a mapping between attributes indicating they have been woven
                weavingInformation.add(sourceLiteral, destinationLiteral);
                // and add the attribute to the class from the base
                destinationEnum.getLiterals().add(destinationLiteral);
            }
        }
    }

//    /**
//     * Processes associations.
//     *
//     * If association has upper bound of 1:
//     * - Delete the association from the concern that does not have a feature selection.
//     * - Map association ends.
//     *
//     * If the upper bound is greater than 1:
//     * - Navigable in one direction: Delete the association from Data --> Associated
//     * - Navigable in both directions: Set navigability of the association end to false
//     * - Rename the association going to the Collection with the name of the association that was going
//     * to Associated.
//     *
//     * @param aspect The aspect that contains the associations to be processed.
//     * @param modelReuse the model reuse woven into the aspect
//     * @param weavingInformation The weaving information to map association ends
//     */
//    private static void processAssociations(Aspect aspect, COREModelReuse modelReuse,
//            WeavingInformation weavingInformation) {
//
//        ClassifierMapping sourceClassifierMapping = null;
//        Association associationToRemove = null;
//
//        // Find the class mapped to 'Source' and get the association end with the matching feature selection
//        for (COREModelElementComposition<?> mec : modelReuse.getCompositions()) {
//            ClassifierMapping classifierMapping = (ClassifierMapping) mec;
//            
//            if (Constants.ASSOCIATION_SOURCE_CLASS_NAME.equals(classifierMapping.getFrom().getName())) {
//                sourceClassifierMapping = classifierMapping;
//            }
//        }
//
//        for (AssociationEnd associationEnd : sourceClassifierMapping.getTo().getAssociationEnds()) {
//            if (associationEnd.getFeatureSelection() == null) {
//                if (Constants.ASSOCIATION_END_NAME_ASSOCIATED.equals(associationEnd.getName())) {
//                    // Remove Data --> (myAssociated) Associated
//                    associationToRemove = associationEnd.getAssoc();
//                }
//            } else if (associationEnd.getFeatureSelection().getReuse() == modelReuse.getReuse()) {
//                if (associationEnd.getUpperBound() == 1) {
//                    // Map association end in the concern to the one in the aspect
//                    AssociationEnd toMap = sourceClassifierMapping.getFrom().getAssociationEnds().get(0);
//                    weavingInformation.getToElements(toMap).remove(0);
//                    weavingInformation.add(toMap, associationEnd);
//                } else {
//                    if (associationEnd.getOppositeEnd().isNavigable()) {
//                        associationEnd.setFeatureSelection(null);
//                        associationEnd.setNavigable(false);
//                    } else {
//                        // Remove Data --> (endName) Associated
//                        associationToRemove = associationEnd.getAssoc();
//                        
//                        AssociationEnd toMap = sourceClassifierMapping.getFrom().getAssociationEnds().get(0);
//                        weavingInformation.add(associationEnd, toMap);
//                    }
//
//                    // Rename Data --> (collection) Collection to the endName.
//                    for (Association association : aspect.getStructuralView().getAssociations()) {
//                        for (AssociationEnd end : association.getEnds()) {
//                            if (Constants.ASSOCIATION_END_NAME_COLLECTION.equals(end.getName())
//                                    && Constants.ASSOCIATION_NAME_DATA_COLLECTION.equals(end.getAssoc().getName())) {
//                                // Find Collection --> (elements) Associated end to update the multiplicity
//                                end.setName(associationEnd.getName());
//                            } else if (Constants.ASSOCIATION_END_NAME_ELEMENTS.equals(end.getName())) {
//                                // Find Collection --> (elements) Associated end to update the multiplicity
//                                end.setUpperBound(associationEnd.getUpperBound());
//                                end.setLowerBound(associationEnd.getLowerBound());
//                            }
//                        }
//                    }
//                }
//
//            }
//        }
//
//        // Delete the association between Data and Associated
//        if (associationToRemove != null) {
//            for (AssociationEnd associationEnd : associationToRemove.getEnds()) {
//                associationEnd.getClassifier().getAssociationEnds().remove(associationEnd);
//            }
//            aspect.getStructuralView().getAssociations().remove(associationToRemove);
//        }
//    }
    
}
