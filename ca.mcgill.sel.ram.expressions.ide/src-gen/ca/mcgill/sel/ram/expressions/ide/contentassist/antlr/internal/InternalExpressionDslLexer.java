package ca.mcgill.sel.ram.expressions.ide.contentassist.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.Lexer;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalExpressionDslLexer extends Lexer {
    public static final int RULE_E_BYTE=12;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__37=37;
    public static final int T__16=16;
    public static final int T__38=38;
    public static final int T__17=17;
    public static final int T__39=39;
    public static final int T__18=18;
    public static final int RULE_E_NULL=6;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__14=14;
    public static final int T__36=36;
    public static final int RULE_E_DOUBLE=11;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int RULE_E_CHAR=5;
    public static final int RULE_E_BOOLEAN=8;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=13;
    public static final int RULE_E_LONG=10;
    public static final int RULE_NUMBER=4;
    public static final int RULE_E_FLOAT=9;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int RULE_E_STRING=7;
    public static final int T__20=20;
    public static final int T__42=42;
    public static final int T__21=21;

    // delegates
    // delegators

    public InternalExpressionDslLexer() {;} 
    public InternalExpressionDslLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public InternalExpressionDslLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);

    }
    public String getGrammarFileName() { return "InternalExpressionDsl.g"; }

    // $ANTLR start "T__14"
    public final void mT__14() throws RecognitionException {
        try {
            int _type = T__14;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalExpressionDsl.g:11:7: ( '==' )
            // InternalExpressionDsl.g:11:9: '=='
            {
            match("=="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__14"

    // $ANTLR start "T__15"
    public final void mT__15() throws RecognitionException {
        try {
            int _type = T__15;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalExpressionDsl.g:12:7: ( '!=' )
            // InternalExpressionDsl.g:12:9: '!='
            {
            match("!="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__15"

    // $ANTLR start "T__16"
    public final void mT__16() throws RecognitionException {
        try {
            int _type = T__16;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalExpressionDsl.g:13:7: ( '>=' )
            // InternalExpressionDsl.g:13:9: '>='
            {
            match(">="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__16"

    // $ANTLR start "T__17"
    public final void mT__17() throws RecognitionException {
        try {
            int _type = T__17;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalExpressionDsl.g:14:7: ( '<=' )
            // InternalExpressionDsl.g:14:9: '<='
            {
            match("<="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__17"

    // $ANTLR start "T__18"
    public final void mT__18() throws RecognitionException {
        try {
            int _type = T__18;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalExpressionDsl.g:15:7: ( '>' )
            // InternalExpressionDsl.g:15:9: '>'
            {
            match('>'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__18"

    // $ANTLR start "T__19"
    public final void mT__19() throws RecognitionException {
        try {
            int _type = T__19;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalExpressionDsl.g:16:7: ( '<' )
            // InternalExpressionDsl.g:16:9: '<'
            {
            match('<'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__19"

    // $ANTLR start "T__20"
    public final void mT__20() throws RecognitionException {
        try {
            int _type = T__20;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalExpressionDsl.g:17:7: ( '>>' )
            // InternalExpressionDsl.g:17:9: '>>'
            {
            match(">>"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__20"

    // $ANTLR start "T__21"
    public final void mT__21() throws RecognitionException {
        try {
            int _type = T__21;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalExpressionDsl.g:18:7: ( '<<' )
            // InternalExpressionDsl.g:18:9: '<<'
            {
            match("<<"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__21"

    // $ANTLR start "T__22"
    public final void mT__22() throws RecognitionException {
        try {
            int _type = T__22;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalExpressionDsl.g:19:7: ( '>>>' )
            // InternalExpressionDsl.g:19:9: '>>>'
            {
            match(">>>"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__22"

    // $ANTLR start "T__23"
    public final void mT__23() throws RecognitionException {
        try {
            int _type = T__23;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalExpressionDsl.g:20:7: ( '*' )
            // InternalExpressionDsl.g:20:9: '*'
            {
            match('*'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__23"

    // $ANTLR start "T__24"
    public final void mT__24() throws RecognitionException {
        try {
            int _type = T__24;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalExpressionDsl.g:21:7: ( '/' )
            // InternalExpressionDsl.g:21:9: '/'
            {
            match('/'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__24"

    // $ANTLR start "T__25"
    public final void mT__25() throws RecognitionException {
        try {
            int _type = T__25;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalExpressionDsl.g:22:7: ( '%' )
            // InternalExpressionDsl.g:22:9: '%'
            {
            match('%'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__25"

    // $ANTLR start "T__26"
    public final void mT__26() throws RecognitionException {
        try {
            int _type = T__26;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalExpressionDsl.g:23:7: ( '++' )
            // InternalExpressionDsl.g:23:9: '++'
            {
            match("++"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__26"

    // $ANTLR start "T__27"
    public final void mT__27() throws RecognitionException {
        try {
            int _type = T__27;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalExpressionDsl.g:24:7: ( '--' )
            // InternalExpressionDsl.g:24:9: '--'
            {
            match("--"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__27"

    // $ANTLR start "T__28"
    public final void mT__28() throws RecognitionException {
        try {
            int _type = T__28;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalExpressionDsl.g:25:7: ( '_' )
            // InternalExpressionDsl.g:25:9: '_'
            {
            match('_'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__28"

    // $ANTLR start "T__29"
    public final void mT__29() throws RecognitionException {
        try {
            int _type = T__29;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalExpressionDsl.g:26:7: ( '?' )
            // InternalExpressionDsl.g:26:9: '?'
            {
            match('?'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__29"

    // $ANTLR start "T__30"
    public final void mT__30() throws RecognitionException {
        try {
            int _type = T__30;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalExpressionDsl.g:27:7: ( ':' )
            // InternalExpressionDsl.g:27:9: ':'
            {
            match(':'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__30"

    // $ANTLR start "T__31"
    public final void mT__31() throws RecognitionException {
        try {
            int _type = T__31;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalExpressionDsl.g:28:7: ( '||' )
            // InternalExpressionDsl.g:28:9: '||'
            {
            match("||"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__31"

    // $ANTLR start "T__32"
    public final void mT__32() throws RecognitionException {
        try {
            int _type = T__32;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalExpressionDsl.g:29:7: ( '&&' )
            // InternalExpressionDsl.g:29:9: '&&'
            {
            match("&&"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__32"

    // $ANTLR start "T__33"
    public final void mT__33() throws RecognitionException {
        try {
            int _type = T__33;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalExpressionDsl.g:30:7: ( '+' )
            // InternalExpressionDsl.g:30:9: '+'
            {
            match('+'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__33"

    // $ANTLR start "T__34"
    public final void mT__34() throws RecognitionException {
        try {
            int _type = T__34;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalExpressionDsl.g:31:7: ( '-' )
            // InternalExpressionDsl.g:31:9: '-'
            {
            match('-'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__34"

    // $ANTLR start "T__35"
    public final void mT__35() throws RecognitionException {
        try {
            int _type = T__35;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalExpressionDsl.g:32:7: ( '!' )
            // InternalExpressionDsl.g:32:9: '!'
            {
            match('!'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__35"

    // $ANTLR start "T__36"
    public final void mT__36() throws RecognitionException {
        try {
            int _type = T__36;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalExpressionDsl.g:33:7: ( '(' )
            // InternalExpressionDsl.g:33:9: '('
            {
            match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__36"

    // $ANTLR start "T__37"
    public final void mT__37() throws RecognitionException {
        try {
            int _type = T__37;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalExpressionDsl.g:34:7: ( ')' )
            // InternalExpressionDsl.g:34:9: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__37"

    // $ANTLR start "T__38"
    public final void mT__38() throws RecognitionException {
        try {
            int _type = T__38;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalExpressionDsl.g:35:7: ( '\\'' )
            // InternalExpressionDsl.g:35:9: '\\''
            {
            match('\''); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__38"

    // $ANTLR start "T__39"
    public final void mT__39() throws RecognitionException {
        try {
            int _type = T__39;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalExpressionDsl.g:36:7: ( '.' )
            // InternalExpressionDsl.g:36:9: '.'
            {
            match('.'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__39"

    // $ANTLR start "T__40"
    public final void mT__40() throws RecognitionException {
        try {
            int _type = T__40;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalExpressionDsl.g:37:7: ( '|' )
            // InternalExpressionDsl.g:37:9: '|'
            {
            match('|'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__40"

    // $ANTLR start "T__41"
    public final void mT__41() throws RecognitionException {
        try {
            int _type = T__41;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalExpressionDsl.g:38:7: ( '^' )
            // InternalExpressionDsl.g:38:9: '^'
            {
            match('^'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__41"

    // $ANTLR start "T__42"
    public final void mT__42() throws RecognitionException {
        try {
            int _type = T__42;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalExpressionDsl.g:39:7: ( '&' )
            // InternalExpressionDsl.g:39:9: '&'
            {
            match('&'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__42"

    // $ANTLR start "RULE_E_FLOAT"
    public final void mRULE_E_FLOAT() throws RecognitionException {
        try {
            int _type = RULE_E_FLOAT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalExpressionDsl.g:4107:14: ( RULE_NUMBER ( '.' RULE_NUMBER )? ( 'f' | 'F' ) )
            // InternalExpressionDsl.g:4107:16: RULE_NUMBER ( '.' RULE_NUMBER )? ( 'f' | 'F' )
            {
            mRULE_NUMBER(); 
            // InternalExpressionDsl.g:4107:28: ( '.' RULE_NUMBER )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0=='.') ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // InternalExpressionDsl.g:4107:29: '.' RULE_NUMBER
                    {
                    match('.'); 
                    mRULE_NUMBER(); 

                    }
                    break;

            }

            if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_E_FLOAT"

    // $ANTLR start "RULE_E_DOUBLE"
    public final void mRULE_E_DOUBLE() throws RecognitionException {
        try {
            int _type = RULE_E_DOUBLE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalExpressionDsl.g:4109:15: ( RULE_NUMBER ( '.' RULE_NUMBER )? ( 'd' | 'D' ) )
            // InternalExpressionDsl.g:4109:17: RULE_NUMBER ( '.' RULE_NUMBER )? ( 'd' | 'D' )
            {
            mRULE_NUMBER(); 
            // InternalExpressionDsl.g:4109:29: ( '.' RULE_NUMBER )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0=='.') ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // InternalExpressionDsl.g:4109:30: '.' RULE_NUMBER
                    {
                    match('.'); 
                    mRULE_NUMBER(); 

                    }
                    break;

            }

            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_E_DOUBLE"

    // $ANTLR start "RULE_E_BYTE"
    public final void mRULE_E_BYTE() throws RecognitionException {
        try {
            int _type = RULE_E_BYTE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalExpressionDsl.g:4111:13: ( RULE_NUMBER 'b' )
            // InternalExpressionDsl.g:4111:15: RULE_NUMBER 'b'
            {
            mRULE_NUMBER(); 
            match('b'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_E_BYTE"

    // $ANTLR start "RULE_E_LONG"
    public final void mRULE_E_LONG() throws RecognitionException {
        try {
            int _type = RULE_E_LONG;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalExpressionDsl.g:4113:13: ( RULE_NUMBER 'L' )
            // InternalExpressionDsl.g:4113:15: RULE_NUMBER 'L'
            {
            mRULE_NUMBER(); 
            match('L'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_E_LONG"

    // $ANTLR start "RULE_E_BOOLEAN"
    public final void mRULE_E_BOOLEAN() throws RecognitionException {
        try {
            int _type = RULE_E_BOOLEAN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalExpressionDsl.g:4115:16: ( ( 'true' | 'false' ) )
            // InternalExpressionDsl.g:4115:18: ( 'true' | 'false' )
            {
            // InternalExpressionDsl.g:4115:18: ( 'true' | 'false' )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0=='t') ) {
                alt3=1;
            }
            else if ( (LA3_0=='f') ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalExpressionDsl.g:4115:19: 'true'
                    {
                    match("true"); 


                    }
                    break;
                case 2 :
                    // InternalExpressionDsl.g:4115:26: 'false'
                    {
                    match("false"); 


                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_E_BOOLEAN"

    // $ANTLR start "RULE_E_CHAR"
    public final void mRULE_E_CHAR() throws RecognitionException {
        try {
            int _type = RULE_E_CHAR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalExpressionDsl.g:4117:13: ( ( 'a' .. 'z' | 'A' .. 'Z' ) )
            // InternalExpressionDsl.g:4117:15: ( 'a' .. 'z' | 'A' .. 'Z' )
            {
            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_E_CHAR"

    // $ANTLR start "RULE_E_STRING"
    public final void mRULE_E_STRING() throws RecognitionException {
        try {
            int _type = RULE_E_STRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalExpressionDsl.g:4119:15: ( '\"' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\"' ) ) )* '\"' )
            // InternalExpressionDsl.g:4119:17: '\"' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\"' ) ) )* '\"'
            {
            match('\"'); 
            // InternalExpressionDsl.g:4119:21: ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\"' ) ) )*
            loop4:
            do {
                int alt4=3;
                int LA4_0 = input.LA(1);

                if ( (LA4_0=='\\') ) {
                    alt4=1;
                }
                else if ( ((LA4_0>='\u0000' && LA4_0<='!')||(LA4_0>='#' && LA4_0<='[')||(LA4_0>=']' && LA4_0<='\uFFFF')) ) {
                    alt4=2;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalExpressionDsl.g:4119:22: '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' )
            	    {
            	    match('\\'); 
            	    if ( input.LA(1)=='\"'||input.LA(1)=='\''||input.LA(1)=='\\'||input.LA(1)=='b'||input.LA(1)=='f'||input.LA(1)=='n'||input.LA(1)=='r'||(input.LA(1)>='t' && input.LA(1)<='u') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;
            	case 2 :
            	    // InternalExpressionDsl.g:4119:67: ~ ( ( '\\\\' | '\"' ) )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='!')||(input.LA(1)>='#' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

            match('\"'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_E_STRING"

    // $ANTLR start "RULE_E_NULL"
    public final void mRULE_E_NULL() throws RecognitionException {
        try {
            int _type = RULE_E_NULL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalExpressionDsl.g:4121:13: ( 'null' )
            // InternalExpressionDsl.g:4121:15: 'null'
            {
            match("null"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_E_NULL"

    // $ANTLR start "RULE_WS"
    public final void mRULE_WS() throws RecognitionException {
        try {
            int _type = RULE_WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalExpressionDsl.g:4123:9: ( ( ' ' | '\\t' )+ )
            // InternalExpressionDsl.g:4123:11: ( ' ' | '\\t' )+
            {
            // InternalExpressionDsl.g:4123:11: ( ' ' | '\\t' )+
            int cnt5=0;
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0=='\t'||LA5_0==' ') ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalExpressionDsl.g:
            	    {
            	    if ( input.LA(1)=='\t'||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    if ( cnt5 >= 1 ) break loop5;
                        EarlyExitException eee =
                            new EarlyExitException(5, input);
                        throw eee;
                }
                cnt5++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_WS"

    // $ANTLR start "RULE_NUMBER"
    public final void mRULE_NUMBER() throws RecognitionException {
        try {
            int _type = RULE_NUMBER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalExpressionDsl.g:4125:13: ( ( '0' | '1' .. '9' ( '0' .. '9' )* ) )
            // InternalExpressionDsl.g:4125:15: ( '0' | '1' .. '9' ( '0' .. '9' )* )
            {
            // InternalExpressionDsl.g:4125:15: ( '0' | '1' .. '9' ( '0' .. '9' )* )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0=='0') ) {
                alt7=1;
            }
            else if ( ((LA7_0>='1' && LA7_0<='9')) ) {
                alt7=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // InternalExpressionDsl.g:4125:16: '0'
                    {
                    match('0'); 

                    }
                    break;
                case 2 :
                    // InternalExpressionDsl.g:4125:20: '1' .. '9' ( '0' .. '9' )*
                    {
                    matchRange('1','9'); 
                    // InternalExpressionDsl.g:4125:29: ( '0' .. '9' )*
                    loop6:
                    do {
                        int alt6=2;
                        int LA6_0 = input.LA(1);

                        if ( ((LA6_0>='0' && LA6_0<='9')) ) {
                            alt6=1;
                        }


                        switch (alt6) {
                    	case 1 :
                    	    // InternalExpressionDsl.g:4125:30: '0' .. '9'
                    	    {
                    	    matchRange('0','9'); 

                    	    }
                    	    break;

                    	default :
                    	    break loop6;
                        }
                    } while (true);


                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_NUMBER"

    public void mTokens() throws RecognitionException {
        // InternalExpressionDsl.g:1:8: ( T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | T__41 | T__42 | RULE_E_FLOAT | RULE_E_DOUBLE | RULE_E_BYTE | RULE_E_LONG | RULE_E_BOOLEAN | RULE_E_CHAR | RULE_E_STRING | RULE_E_NULL | RULE_WS | RULE_NUMBER )
        int alt8=39;
        alt8 = dfa8.predict(input);
        switch (alt8) {
            case 1 :
                // InternalExpressionDsl.g:1:10: T__14
                {
                mT__14(); 

                }
                break;
            case 2 :
                // InternalExpressionDsl.g:1:16: T__15
                {
                mT__15(); 

                }
                break;
            case 3 :
                // InternalExpressionDsl.g:1:22: T__16
                {
                mT__16(); 

                }
                break;
            case 4 :
                // InternalExpressionDsl.g:1:28: T__17
                {
                mT__17(); 

                }
                break;
            case 5 :
                // InternalExpressionDsl.g:1:34: T__18
                {
                mT__18(); 

                }
                break;
            case 6 :
                // InternalExpressionDsl.g:1:40: T__19
                {
                mT__19(); 

                }
                break;
            case 7 :
                // InternalExpressionDsl.g:1:46: T__20
                {
                mT__20(); 

                }
                break;
            case 8 :
                // InternalExpressionDsl.g:1:52: T__21
                {
                mT__21(); 

                }
                break;
            case 9 :
                // InternalExpressionDsl.g:1:58: T__22
                {
                mT__22(); 

                }
                break;
            case 10 :
                // InternalExpressionDsl.g:1:64: T__23
                {
                mT__23(); 

                }
                break;
            case 11 :
                // InternalExpressionDsl.g:1:70: T__24
                {
                mT__24(); 

                }
                break;
            case 12 :
                // InternalExpressionDsl.g:1:76: T__25
                {
                mT__25(); 

                }
                break;
            case 13 :
                // InternalExpressionDsl.g:1:82: T__26
                {
                mT__26(); 

                }
                break;
            case 14 :
                // InternalExpressionDsl.g:1:88: T__27
                {
                mT__27(); 

                }
                break;
            case 15 :
                // InternalExpressionDsl.g:1:94: T__28
                {
                mT__28(); 

                }
                break;
            case 16 :
                // InternalExpressionDsl.g:1:100: T__29
                {
                mT__29(); 

                }
                break;
            case 17 :
                // InternalExpressionDsl.g:1:106: T__30
                {
                mT__30(); 

                }
                break;
            case 18 :
                // InternalExpressionDsl.g:1:112: T__31
                {
                mT__31(); 

                }
                break;
            case 19 :
                // InternalExpressionDsl.g:1:118: T__32
                {
                mT__32(); 

                }
                break;
            case 20 :
                // InternalExpressionDsl.g:1:124: T__33
                {
                mT__33(); 

                }
                break;
            case 21 :
                // InternalExpressionDsl.g:1:130: T__34
                {
                mT__34(); 

                }
                break;
            case 22 :
                // InternalExpressionDsl.g:1:136: T__35
                {
                mT__35(); 

                }
                break;
            case 23 :
                // InternalExpressionDsl.g:1:142: T__36
                {
                mT__36(); 

                }
                break;
            case 24 :
                // InternalExpressionDsl.g:1:148: T__37
                {
                mT__37(); 

                }
                break;
            case 25 :
                // InternalExpressionDsl.g:1:154: T__38
                {
                mT__38(); 

                }
                break;
            case 26 :
                // InternalExpressionDsl.g:1:160: T__39
                {
                mT__39(); 

                }
                break;
            case 27 :
                // InternalExpressionDsl.g:1:166: T__40
                {
                mT__40(); 

                }
                break;
            case 28 :
                // InternalExpressionDsl.g:1:172: T__41
                {
                mT__41(); 

                }
                break;
            case 29 :
                // InternalExpressionDsl.g:1:178: T__42
                {
                mT__42(); 

                }
                break;
            case 30 :
                // InternalExpressionDsl.g:1:184: RULE_E_FLOAT
                {
                mRULE_E_FLOAT(); 

                }
                break;
            case 31 :
                // InternalExpressionDsl.g:1:197: RULE_E_DOUBLE
                {
                mRULE_E_DOUBLE(); 

                }
                break;
            case 32 :
                // InternalExpressionDsl.g:1:211: RULE_E_BYTE
                {
                mRULE_E_BYTE(); 

                }
                break;
            case 33 :
                // InternalExpressionDsl.g:1:223: RULE_E_LONG
                {
                mRULE_E_LONG(); 

                }
                break;
            case 34 :
                // InternalExpressionDsl.g:1:235: RULE_E_BOOLEAN
                {
                mRULE_E_BOOLEAN(); 

                }
                break;
            case 35 :
                // InternalExpressionDsl.g:1:250: RULE_E_CHAR
                {
                mRULE_E_CHAR(); 

                }
                break;
            case 36 :
                // InternalExpressionDsl.g:1:262: RULE_E_STRING
                {
                mRULE_E_STRING(); 

                }
                break;
            case 37 :
                // InternalExpressionDsl.g:1:276: RULE_E_NULL
                {
                mRULE_E_NULL(); 

                }
                break;
            case 38 :
                // InternalExpressionDsl.g:1:288: RULE_WS
                {
                mRULE_WS(); 

                }
                break;
            case 39 :
                // InternalExpressionDsl.g:1:296: RULE_NUMBER
                {
                mRULE_NUMBER(); 

                }
                break;

        }

    }


    protected DFA8 dfa8 = new DFA8(this);
    static final String DFA8_eotS =
        "\2\uffff\1\35\1\40\1\43\3\uffff\1\45\1\47\3\uffff\1\51\1\53\5\uffff\2\54\3\32\6\uffff\1\66\22\uffff\1\54\7\uffff";
    static final String DFA8_eofS =
        "\72\uffff";
    static final String DFA8_minS =
        "\1\11\1\uffff\2\75\1\74\3\uffff\1\53\1\55\3\uffff\1\174\1\46\5\uffff\2\56\1\162\1\141\1\165\6\uffff\1\76\17\uffff\1\60\2\uffff\1\56\4\uffff\1\104\2\60";
    static final String DFA8_maxS =
        "\1\174\1\uffff\1\75\1\76\1\75\3\uffff\1\53\1\55\3\uffff\1\174\1\46\5\uffff\2\146\1\162\1\141\1\165\6\uffff\1\76\17\uffff\1\71\2\uffff\1\146\4\uffff\3\146";
    static final String DFA8_acceptS =
        "\1\uffff\1\1\3\uffff\1\12\1\13\1\14\2\uffff\1\17\1\20\1\21\2\uffff\1\27\1\30\1\31\1\32\1\34\5\uffff\1\44\1\43\1\46\1\2\1\26\1\3\1\uffff\1\5\1\4\1\10\1\6\1\15\1\24\1\16\1\25\1\22\1\33\1\23\1\35\1\47\1\41\1\40\1\uffff\1\37\1\36\1\uffff\1\42\1\45\1\11\1\7\3\uffff";
    static final String DFA8_specialS =
        "\72\uffff}>";
    static final String[] DFA8_transitionS = {
            "\1\33\26\uffff\1\33\1\2\1\31\2\uffff\1\7\1\16\1\21\1\17\1\20\1\5\1\10\1\uffff\1\11\1\22\1\6\1\24\11\25\1\14\1\uffff\1\4\1\1\1\3\1\13\1\uffff\32\32\3\uffff\1\23\1\12\1\uffff\5\32\1\27\7\32\1\30\5\32\1\26\6\32\1\uffff\1\15",
            "",
            "\1\34",
            "\1\36\1\37",
            "\1\42\1\41",
            "",
            "",
            "",
            "\1\44",
            "\1\46",
            "",
            "",
            "",
            "\1\50",
            "\1\52",
            "",
            "",
            "",
            "",
            "",
            "\1\57\25\uffff\1\60\1\uffff\1\61\5\uffff\1\55\25\uffff\1\56\1\uffff\1\60\1\uffff\1\61",
            "\1\57\1\uffff\12\62\12\uffff\1\60\1\uffff\1\61\5\uffff\1\55\25\uffff\1\56\1\uffff\1\60\1\uffff\1\61",
            "\1\63",
            "\1\63",
            "\1\64",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\65",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\67\11\70",
            "",
            "",
            "\1\57\1\uffff\12\62\12\uffff\1\60\1\uffff\1\61\5\uffff\1\55\25\uffff\1\56\1\uffff\1\60\1\uffff\1\61",
            "",
            "",
            "",
            "",
            "\1\60\1\uffff\1\61\35\uffff\1\60\1\uffff\1\61",
            "\12\71\12\uffff\1\60\1\uffff\1\61\35\uffff\1\60\1\uffff\1\61",
            "\12\71\12\uffff\1\60\1\uffff\1\61\35\uffff\1\60\1\uffff\1\61"
    };

    static final short[] DFA8_eot = DFA.unpackEncodedString(DFA8_eotS);
    static final short[] DFA8_eof = DFA.unpackEncodedString(DFA8_eofS);
    static final char[] DFA8_min = DFA.unpackEncodedStringToUnsignedChars(DFA8_minS);
    static final char[] DFA8_max = DFA.unpackEncodedStringToUnsignedChars(DFA8_maxS);
    static final short[] DFA8_accept = DFA.unpackEncodedString(DFA8_acceptS);
    static final short[] DFA8_special = DFA.unpackEncodedString(DFA8_specialS);
    static final short[][] DFA8_transition;

    static {
        int numStates = DFA8_transitionS.length;
        DFA8_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA8_transition[i] = DFA.unpackEncodedString(DFA8_transitionS[i]);
        }
    }

    class DFA8 extends DFA {

        public DFA8(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 8;
            this.eot = DFA8_eot;
            this.eof = DFA8_eof;
            this.min = DFA8_min;
            this.max = DFA8_max;
            this.accept = DFA8_accept;
            this.special = DFA8_special;
            this.transition = DFA8_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | T__41 | T__42 | RULE_E_FLOAT | RULE_E_DOUBLE | RULE_E_BYTE | RULE_E_LONG | RULE_E_BOOLEAN | RULE_E_CHAR | RULE_E_STRING | RULE_E_NULL | RULE_WS | RULE_NUMBER );";
        }
    }
 

}