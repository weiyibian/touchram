package ca.mcgill.sel.ram.expressions.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import ca.mcgill.sel.ram.expressions.services.ExpressionDslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalExpressionDslParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_E_STRING", "RULE_E_BOOLEAN", "RULE_E_FLOAT", "RULE_E_LONG", "RULE_E_DOUBLE", "RULE_E_BYTE", "RULE_E_CHAR", "RULE_E_NULL", "RULE_NUMBER", "RULE_WS", "'?'", "':'", "'||'", "'&&'", "'|'", "'^'", "'&'", "'=='", "'!='", "'>='", "'<='", "'>'", "'<'", "'>>'", "'<<'", "'>>>'", "'+'", "'-'", "'*'", "'/'", "'%'", "'!'", "'('", "')'", "'++'", "'--'", "'\\''", "'.'", "'_'"
    };
    public static final int RULE_E_BYTE=9;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__37=37;
    public static final int T__16=16;
    public static final int T__38=38;
    public static final int T__17=17;
    public static final int T__39=39;
    public static final int T__18=18;
    public static final int RULE_E_NULL=11;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__14=14;
    public static final int T__36=36;
    public static final int RULE_E_DOUBLE=8;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int RULE_E_BOOLEAN=5;
    public static final int RULE_E_CHAR=10;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=13;
    public static final int RULE_E_LONG=7;
    public static final int RULE_NUMBER=12;
    public static final int RULE_E_FLOAT=6;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int RULE_E_STRING=4;
    public static final int T__20=20;
    public static final int T__42=42;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalExpressionDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalExpressionDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalExpressionDslParser.tokenNames; }
    public String getGrammarFileName() { return "InternalExpressionDsl.g"; }



     	private ExpressionDslGrammarAccess grammarAccess;

        public InternalExpressionDslParser(TokenStream input, ExpressionDslGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "ValueSpecification";
       	}

       	@Override
       	protected ExpressionDslGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleValueSpecification"
    // InternalExpressionDsl.g:64:1: entryRuleValueSpecification returns [EObject current=null] : iv_ruleValueSpecification= ruleValueSpecification EOF ;
    public final EObject entryRuleValueSpecification() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleValueSpecification = null;


        try {
            // InternalExpressionDsl.g:64:59: (iv_ruleValueSpecification= ruleValueSpecification EOF )
            // InternalExpressionDsl.g:65:2: iv_ruleValueSpecification= ruleValueSpecification EOF
            {
             newCompositeNode(grammarAccess.getValueSpecificationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleValueSpecification=ruleValueSpecification();

            state._fsp--;

             current =iv_ruleValueSpecification; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleValueSpecification"


    // $ANTLR start "ruleValueSpecification"
    // InternalExpressionDsl.g:71:1: ruleValueSpecification returns [EObject current=null] : this_Conditional_0= ruleConditional ;
    public final EObject ruleValueSpecification() throws RecognitionException {
        EObject current = null;

        EObject this_Conditional_0 = null;



        	enterRule();

        try {
            // InternalExpressionDsl.g:77:2: (this_Conditional_0= ruleConditional )
            // InternalExpressionDsl.g:78:2: this_Conditional_0= ruleConditional
            {

            		newCompositeNode(grammarAccess.getValueSpecificationAccess().getConditionalParserRuleCall());
            	
            pushFollow(FOLLOW_2);
            this_Conditional_0=ruleConditional();

            state._fsp--;


            		current = this_Conditional_0;
            		afterParserOrEnumRuleCall();
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleValueSpecification"


    // $ANTLR start "entryRuleConditional"
    // InternalExpressionDsl.g:89:1: entryRuleConditional returns [EObject current=null] : iv_ruleConditional= ruleConditional EOF ;
    public final EObject entryRuleConditional() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConditional = null;


        try {
            // InternalExpressionDsl.g:89:52: (iv_ruleConditional= ruleConditional EOF )
            // InternalExpressionDsl.g:90:2: iv_ruleConditional= ruleConditional EOF
            {
             newCompositeNode(grammarAccess.getConditionalRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleConditional=ruleConditional();

            state._fsp--;

             current =iv_ruleConditional; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConditional"


    // $ANTLR start "ruleConditional"
    // InternalExpressionDsl.g:96:1: ruleConditional returns [EObject current=null] : (this_Or_0= ruleOr ( () otherlv_2= '?' ( (lv_expressionT_3_0= ruleConditional ) ) otherlv_4= ':' ( (lv_expressionF_5_0= ruleConditional ) ) )? ) ;
    public final EObject ruleConditional() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject this_Or_0 = null;

        EObject lv_expressionT_3_0 = null;

        EObject lv_expressionF_5_0 = null;



        	enterRule();

        try {
            // InternalExpressionDsl.g:102:2: ( (this_Or_0= ruleOr ( () otherlv_2= '?' ( (lv_expressionT_3_0= ruleConditional ) ) otherlv_4= ':' ( (lv_expressionF_5_0= ruleConditional ) ) )? ) )
            // InternalExpressionDsl.g:103:2: (this_Or_0= ruleOr ( () otherlv_2= '?' ( (lv_expressionT_3_0= ruleConditional ) ) otherlv_4= ':' ( (lv_expressionF_5_0= ruleConditional ) ) )? )
            {
            // InternalExpressionDsl.g:103:2: (this_Or_0= ruleOr ( () otherlv_2= '?' ( (lv_expressionT_3_0= ruleConditional ) ) otherlv_4= ':' ( (lv_expressionF_5_0= ruleConditional ) ) )? )
            // InternalExpressionDsl.g:104:3: this_Or_0= ruleOr ( () otherlv_2= '?' ( (lv_expressionT_3_0= ruleConditional ) ) otherlv_4= ':' ( (lv_expressionF_5_0= ruleConditional ) ) )?
            {

            			newCompositeNode(grammarAccess.getConditionalAccess().getOrParserRuleCall_0());
            		
            pushFollow(FOLLOW_3);
            this_Or_0=ruleOr();

            state._fsp--;


            			current = this_Or_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalExpressionDsl.g:112:3: ( () otherlv_2= '?' ( (lv_expressionT_3_0= ruleConditional ) ) otherlv_4= ':' ( (lv_expressionF_5_0= ruleConditional ) ) )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==14) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // InternalExpressionDsl.g:113:4: () otherlv_2= '?' ( (lv_expressionT_3_0= ruleConditional ) ) otherlv_4= ':' ( (lv_expressionF_5_0= ruleConditional ) )
                    {
                    // InternalExpressionDsl.g:113:4: ()
                    // InternalExpressionDsl.g:114:5: 
                    {

                    					current = forceCreateModelElementAndSet(
                    						grammarAccess.getConditionalAccess().getConditionalConditionAction_1_0(),
                    						current);
                    				

                    }

                    otherlv_2=(Token)match(input,14,FOLLOW_4); 

                    				newLeafNode(otherlv_2, grammarAccess.getConditionalAccess().getQuestionMarkKeyword_1_1());
                    			
                    // InternalExpressionDsl.g:124:4: ( (lv_expressionT_3_0= ruleConditional ) )
                    // InternalExpressionDsl.g:125:5: (lv_expressionT_3_0= ruleConditional )
                    {
                    // InternalExpressionDsl.g:125:5: (lv_expressionT_3_0= ruleConditional )
                    // InternalExpressionDsl.g:126:6: lv_expressionT_3_0= ruleConditional
                    {

                    						newCompositeNode(grammarAccess.getConditionalAccess().getExpressionTConditionalParserRuleCall_1_2_0());
                    					
                    pushFollow(FOLLOW_5);
                    lv_expressionT_3_0=ruleConditional();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getConditionalRule());
                    						}
                    						set(
                    							current,
                    							"expressionT",
                    							lv_expressionT_3_0,
                    							"ca.mcgill.sel.ram.expressions.ExpressionDsl.Conditional");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_4=(Token)match(input,15,FOLLOW_4); 

                    				newLeafNode(otherlv_4, grammarAccess.getConditionalAccess().getColonKeyword_1_3());
                    			
                    // InternalExpressionDsl.g:147:4: ( (lv_expressionF_5_0= ruleConditional ) )
                    // InternalExpressionDsl.g:148:5: (lv_expressionF_5_0= ruleConditional )
                    {
                    // InternalExpressionDsl.g:148:5: (lv_expressionF_5_0= ruleConditional )
                    // InternalExpressionDsl.g:149:6: lv_expressionF_5_0= ruleConditional
                    {

                    						newCompositeNode(grammarAccess.getConditionalAccess().getExpressionFConditionalParserRuleCall_1_4_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_expressionF_5_0=ruleConditional();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getConditionalRule());
                    						}
                    						set(
                    							current,
                    							"expressionF",
                    							lv_expressionF_5_0,
                    							"ca.mcgill.sel.ram.expressions.ExpressionDsl.Conditional");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConditional"


    // $ANTLR start "entryRuleOr"
    // InternalExpressionDsl.g:171:1: entryRuleOr returns [EObject current=null] : iv_ruleOr= ruleOr EOF ;
    public final EObject entryRuleOr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOr = null;


        try {
            // InternalExpressionDsl.g:171:43: (iv_ruleOr= ruleOr EOF )
            // InternalExpressionDsl.g:172:2: iv_ruleOr= ruleOr EOF
            {
             newCompositeNode(grammarAccess.getOrRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOr=ruleOr();

            state._fsp--;

             current =iv_ruleOr; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOr"


    // $ANTLR start "ruleOr"
    // InternalExpressionDsl.g:178:1: ruleOr returns [EObject current=null] : (this_And_0= ruleAnd ( () otherlv_2= '||' ( (lv_right_3_0= ruleAnd ) ) )* ) ;
    public final EObject ruleOr() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_And_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalExpressionDsl.g:184:2: ( (this_And_0= ruleAnd ( () otherlv_2= '||' ( (lv_right_3_0= ruleAnd ) ) )* ) )
            // InternalExpressionDsl.g:185:2: (this_And_0= ruleAnd ( () otherlv_2= '||' ( (lv_right_3_0= ruleAnd ) ) )* )
            {
            // InternalExpressionDsl.g:185:2: (this_And_0= ruleAnd ( () otherlv_2= '||' ( (lv_right_3_0= ruleAnd ) ) )* )
            // InternalExpressionDsl.g:186:3: this_And_0= ruleAnd ( () otherlv_2= '||' ( (lv_right_3_0= ruleAnd ) ) )*
            {

            			newCompositeNode(grammarAccess.getOrAccess().getAndParserRuleCall_0());
            		
            pushFollow(FOLLOW_6);
            this_And_0=ruleAnd();

            state._fsp--;


            			current = this_And_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalExpressionDsl.g:194:3: ( () otherlv_2= '||' ( (lv_right_3_0= ruleAnd ) ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==16) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalExpressionDsl.g:195:4: () otherlv_2= '||' ( (lv_right_3_0= ruleAnd ) )
            	    {
            	    // InternalExpressionDsl.g:195:4: ()
            	    // InternalExpressionDsl.g:196:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getOrAccess().getOrLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    otherlv_2=(Token)match(input,16,FOLLOW_4); 

            	    				newLeafNode(otherlv_2, grammarAccess.getOrAccess().getVerticalLineVerticalLineKeyword_1_1());
            	    			
            	    // InternalExpressionDsl.g:206:4: ( (lv_right_3_0= ruleAnd ) )
            	    // InternalExpressionDsl.g:207:5: (lv_right_3_0= ruleAnd )
            	    {
            	    // InternalExpressionDsl.g:207:5: (lv_right_3_0= ruleAnd )
            	    // InternalExpressionDsl.g:208:6: lv_right_3_0= ruleAnd
            	    {

            	    						newCompositeNode(grammarAccess.getOrAccess().getRightAndParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_6);
            	    lv_right_3_0=ruleAnd();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getOrRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"ca.mcgill.sel.ram.expressions.ExpressionDsl.And");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOr"


    // $ANTLR start "entryRuleAnd"
    // InternalExpressionDsl.g:230:1: entryRuleAnd returns [EObject current=null] : iv_ruleAnd= ruleAnd EOF ;
    public final EObject entryRuleAnd() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAnd = null;


        try {
            // InternalExpressionDsl.g:230:44: (iv_ruleAnd= ruleAnd EOF )
            // InternalExpressionDsl.g:231:2: iv_ruleAnd= ruleAnd EOF
            {
             newCompositeNode(grammarAccess.getAndRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAnd=ruleAnd();

            state._fsp--;

             current =iv_ruleAnd; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAnd"


    // $ANTLR start "ruleAnd"
    // InternalExpressionDsl.g:237:1: ruleAnd returns [EObject current=null] : (this_BitwiseOR_0= ruleBitwiseOR ( () otherlv_2= '&&' ( (lv_right_3_0= ruleBitwiseOR ) ) )* ) ;
    public final EObject ruleAnd() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_BitwiseOR_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalExpressionDsl.g:243:2: ( (this_BitwiseOR_0= ruleBitwiseOR ( () otherlv_2= '&&' ( (lv_right_3_0= ruleBitwiseOR ) ) )* ) )
            // InternalExpressionDsl.g:244:2: (this_BitwiseOR_0= ruleBitwiseOR ( () otherlv_2= '&&' ( (lv_right_3_0= ruleBitwiseOR ) ) )* )
            {
            // InternalExpressionDsl.g:244:2: (this_BitwiseOR_0= ruleBitwiseOR ( () otherlv_2= '&&' ( (lv_right_3_0= ruleBitwiseOR ) ) )* )
            // InternalExpressionDsl.g:245:3: this_BitwiseOR_0= ruleBitwiseOR ( () otherlv_2= '&&' ( (lv_right_3_0= ruleBitwiseOR ) ) )*
            {

            			newCompositeNode(grammarAccess.getAndAccess().getBitwiseORParserRuleCall_0());
            		
            pushFollow(FOLLOW_7);
            this_BitwiseOR_0=ruleBitwiseOR();

            state._fsp--;


            			current = this_BitwiseOR_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalExpressionDsl.g:253:3: ( () otherlv_2= '&&' ( (lv_right_3_0= ruleBitwiseOR ) ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==17) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalExpressionDsl.g:254:4: () otherlv_2= '&&' ( (lv_right_3_0= ruleBitwiseOR ) )
            	    {
            	    // InternalExpressionDsl.g:254:4: ()
            	    // InternalExpressionDsl.g:255:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getAndAccess().getAndLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    otherlv_2=(Token)match(input,17,FOLLOW_4); 

            	    				newLeafNode(otherlv_2, grammarAccess.getAndAccess().getAmpersandAmpersandKeyword_1_1());
            	    			
            	    // InternalExpressionDsl.g:265:4: ( (lv_right_3_0= ruleBitwiseOR ) )
            	    // InternalExpressionDsl.g:266:5: (lv_right_3_0= ruleBitwiseOR )
            	    {
            	    // InternalExpressionDsl.g:266:5: (lv_right_3_0= ruleBitwiseOR )
            	    // InternalExpressionDsl.g:267:6: lv_right_3_0= ruleBitwiseOR
            	    {

            	    						newCompositeNode(grammarAccess.getAndAccess().getRightBitwiseORParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_7);
            	    lv_right_3_0=ruleBitwiseOR();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getAndRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"ca.mcgill.sel.ram.expressions.ExpressionDsl.BitwiseOR");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAnd"


    // $ANTLR start "entryRuleBitwiseOR"
    // InternalExpressionDsl.g:289:1: entryRuleBitwiseOR returns [EObject current=null] : iv_ruleBitwiseOR= ruleBitwiseOR EOF ;
    public final EObject entryRuleBitwiseOR() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBitwiseOR = null;


        try {
            // InternalExpressionDsl.g:289:50: (iv_ruleBitwiseOR= ruleBitwiseOR EOF )
            // InternalExpressionDsl.g:290:2: iv_ruleBitwiseOR= ruleBitwiseOR EOF
            {
             newCompositeNode(grammarAccess.getBitwiseORRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBitwiseOR=ruleBitwiseOR();

            state._fsp--;

             current =iv_ruleBitwiseOR; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBitwiseOR"


    // $ANTLR start "ruleBitwiseOR"
    // InternalExpressionDsl.g:296:1: ruleBitwiseOR returns [EObject current=null] : (this_BitwiseXOR_0= ruleBitwiseXOR ( () ( (lv_op_2_0= '|' ) ) ( (lv_right_3_0= ruleBitwiseXOR ) ) )* ) ;
    public final EObject ruleBitwiseOR() throws RecognitionException {
        EObject current = null;

        Token lv_op_2_0=null;
        EObject this_BitwiseXOR_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalExpressionDsl.g:302:2: ( (this_BitwiseXOR_0= ruleBitwiseXOR ( () ( (lv_op_2_0= '|' ) ) ( (lv_right_3_0= ruleBitwiseXOR ) ) )* ) )
            // InternalExpressionDsl.g:303:2: (this_BitwiseXOR_0= ruleBitwiseXOR ( () ( (lv_op_2_0= '|' ) ) ( (lv_right_3_0= ruleBitwiseXOR ) ) )* )
            {
            // InternalExpressionDsl.g:303:2: (this_BitwiseXOR_0= ruleBitwiseXOR ( () ( (lv_op_2_0= '|' ) ) ( (lv_right_3_0= ruleBitwiseXOR ) ) )* )
            // InternalExpressionDsl.g:304:3: this_BitwiseXOR_0= ruleBitwiseXOR ( () ( (lv_op_2_0= '|' ) ) ( (lv_right_3_0= ruleBitwiseXOR ) ) )*
            {

            			newCompositeNode(grammarAccess.getBitwiseORAccess().getBitwiseXORParserRuleCall_0());
            		
            pushFollow(FOLLOW_8);
            this_BitwiseXOR_0=ruleBitwiseXOR();

            state._fsp--;


            			current = this_BitwiseXOR_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalExpressionDsl.g:312:3: ( () ( (lv_op_2_0= '|' ) ) ( (lv_right_3_0= ruleBitwiseXOR ) ) )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==18) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalExpressionDsl.g:313:4: () ( (lv_op_2_0= '|' ) ) ( (lv_right_3_0= ruleBitwiseXOR ) )
            	    {
            	    // InternalExpressionDsl.g:313:4: ()
            	    // InternalExpressionDsl.g:314:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getBitwiseORAccess().getLogicalOperatorLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    // InternalExpressionDsl.g:320:4: ( (lv_op_2_0= '|' ) )
            	    // InternalExpressionDsl.g:321:5: (lv_op_2_0= '|' )
            	    {
            	    // InternalExpressionDsl.g:321:5: (lv_op_2_0= '|' )
            	    // InternalExpressionDsl.g:322:6: lv_op_2_0= '|'
            	    {
            	    lv_op_2_0=(Token)match(input,18,FOLLOW_4); 

            	    						newLeafNode(lv_op_2_0, grammarAccess.getBitwiseORAccess().getOpVerticalLineKeyword_1_1_0());
            	    					

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getBitwiseORRule());
            	    						}
            	    						setWithLastConsumed(current, "op", lv_op_2_0, "|");
            	    					

            	    }


            	    }

            	    // InternalExpressionDsl.g:334:4: ( (lv_right_3_0= ruleBitwiseXOR ) )
            	    // InternalExpressionDsl.g:335:5: (lv_right_3_0= ruleBitwiseXOR )
            	    {
            	    // InternalExpressionDsl.g:335:5: (lv_right_3_0= ruleBitwiseXOR )
            	    // InternalExpressionDsl.g:336:6: lv_right_3_0= ruleBitwiseXOR
            	    {

            	    						newCompositeNode(grammarAccess.getBitwiseORAccess().getRightBitwiseXORParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_8);
            	    lv_right_3_0=ruleBitwiseXOR();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getBitwiseORRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"ca.mcgill.sel.ram.expressions.ExpressionDsl.BitwiseXOR");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBitwiseOR"


    // $ANTLR start "entryRuleBitwiseXOR"
    // InternalExpressionDsl.g:358:1: entryRuleBitwiseXOR returns [EObject current=null] : iv_ruleBitwiseXOR= ruleBitwiseXOR EOF ;
    public final EObject entryRuleBitwiseXOR() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBitwiseXOR = null;


        try {
            // InternalExpressionDsl.g:358:51: (iv_ruleBitwiseXOR= ruleBitwiseXOR EOF )
            // InternalExpressionDsl.g:359:2: iv_ruleBitwiseXOR= ruleBitwiseXOR EOF
            {
             newCompositeNode(grammarAccess.getBitwiseXORRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBitwiseXOR=ruleBitwiseXOR();

            state._fsp--;

             current =iv_ruleBitwiseXOR; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBitwiseXOR"


    // $ANTLR start "ruleBitwiseXOR"
    // InternalExpressionDsl.g:365:1: ruleBitwiseXOR returns [EObject current=null] : (this_BitwiseAnd_0= ruleBitwiseAnd ( () ( (lv_op_2_0= '^' ) ) ( (lv_right_3_0= ruleBitwiseAnd ) ) )* ) ;
    public final EObject ruleBitwiseXOR() throws RecognitionException {
        EObject current = null;

        Token lv_op_2_0=null;
        EObject this_BitwiseAnd_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalExpressionDsl.g:371:2: ( (this_BitwiseAnd_0= ruleBitwiseAnd ( () ( (lv_op_2_0= '^' ) ) ( (lv_right_3_0= ruleBitwiseAnd ) ) )* ) )
            // InternalExpressionDsl.g:372:2: (this_BitwiseAnd_0= ruleBitwiseAnd ( () ( (lv_op_2_0= '^' ) ) ( (lv_right_3_0= ruleBitwiseAnd ) ) )* )
            {
            // InternalExpressionDsl.g:372:2: (this_BitwiseAnd_0= ruleBitwiseAnd ( () ( (lv_op_2_0= '^' ) ) ( (lv_right_3_0= ruleBitwiseAnd ) ) )* )
            // InternalExpressionDsl.g:373:3: this_BitwiseAnd_0= ruleBitwiseAnd ( () ( (lv_op_2_0= '^' ) ) ( (lv_right_3_0= ruleBitwiseAnd ) ) )*
            {

            			newCompositeNode(grammarAccess.getBitwiseXORAccess().getBitwiseAndParserRuleCall_0());
            		
            pushFollow(FOLLOW_9);
            this_BitwiseAnd_0=ruleBitwiseAnd();

            state._fsp--;


            			current = this_BitwiseAnd_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalExpressionDsl.g:381:3: ( () ( (lv_op_2_0= '^' ) ) ( (lv_right_3_0= ruleBitwiseAnd ) ) )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==19) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalExpressionDsl.g:382:4: () ( (lv_op_2_0= '^' ) ) ( (lv_right_3_0= ruleBitwiseAnd ) )
            	    {
            	    // InternalExpressionDsl.g:382:4: ()
            	    // InternalExpressionDsl.g:383:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getBitwiseXORAccess().getLogicalOperatorLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    // InternalExpressionDsl.g:389:4: ( (lv_op_2_0= '^' ) )
            	    // InternalExpressionDsl.g:390:5: (lv_op_2_0= '^' )
            	    {
            	    // InternalExpressionDsl.g:390:5: (lv_op_2_0= '^' )
            	    // InternalExpressionDsl.g:391:6: lv_op_2_0= '^'
            	    {
            	    lv_op_2_0=(Token)match(input,19,FOLLOW_4); 

            	    						newLeafNode(lv_op_2_0, grammarAccess.getBitwiseXORAccess().getOpCircumflexAccentKeyword_1_1_0());
            	    					

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getBitwiseXORRule());
            	    						}
            	    						setWithLastConsumed(current, "op", lv_op_2_0, "^");
            	    					

            	    }


            	    }

            	    // InternalExpressionDsl.g:403:4: ( (lv_right_3_0= ruleBitwiseAnd ) )
            	    // InternalExpressionDsl.g:404:5: (lv_right_3_0= ruleBitwiseAnd )
            	    {
            	    // InternalExpressionDsl.g:404:5: (lv_right_3_0= ruleBitwiseAnd )
            	    // InternalExpressionDsl.g:405:6: lv_right_3_0= ruleBitwiseAnd
            	    {

            	    						newCompositeNode(grammarAccess.getBitwiseXORAccess().getRightBitwiseAndParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_9);
            	    lv_right_3_0=ruleBitwiseAnd();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getBitwiseXORRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"ca.mcgill.sel.ram.expressions.ExpressionDsl.BitwiseAnd");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBitwiseXOR"


    // $ANTLR start "entryRuleBitwiseAnd"
    // InternalExpressionDsl.g:427:1: entryRuleBitwiseAnd returns [EObject current=null] : iv_ruleBitwiseAnd= ruleBitwiseAnd EOF ;
    public final EObject entryRuleBitwiseAnd() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBitwiseAnd = null;


        try {
            // InternalExpressionDsl.g:427:51: (iv_ruleBitwiseAnd= ruleBitwiseAnd EOF )
            // InternalExpressionDsl.g:428:2: iv_ruleBitwiseAnd= ruleBitwiseAnd EOF
            {
             newCompositeNode(grammarAccess.getBitwiseAndRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBitwiseAnd=ruleBitwiseAnd();

            state._fsp--;

             current =iv_ruleBitwiseAnd; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBitwiseAnd"


    // $ANTLR start "ruleBitwiseAnd"
    // InternalExpressionDsl.g:434:1: ruleBitwiseAnd returns [EObject current=null] : (this_Equality_0= ruleEquality ( () ( (lv_op_2_0= '&' ) ) ( (lv_right_3_0= ruleEquality ) ) )* ) ;
    public final EObject ruleBitwiseAnd() throws RecognitionException {
        EObject current = null;

        Token lv_op_2_0=null;
        EObject this_Equality_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalExpressionDsl.g:440:2: ( (this_Equality_0= ruleEquality ( () ( (lv_op_2_0= '&' ) ) ( (lv_right_3_0= ruleEquality ) ) )* ) )
            // InternalExpressionDsl.g:441:2: (this_Equality_0= ruleEquality ( () ( (lv_op_2_0= '&' ) ) ( (lv_right_3_0= ruleEquality ) ) )* )
            {
            // InternalExpressionDsl.g:441:2: (this_Equality_0= ruleEquality ( () ( (lv_op_2_0= '&' ) ) ( (lv_right_3_0= ruleEquality ) ) )* )
            // InternalExpressionDsl.g:442:3: this_Equality_0= ruleEquality ( () ( (lv_op_2_0= '&' ) ) ( (lv_right_3_0= ruleEquality ) ) )*
            {

            			newCompositeNode(grammarAccess.getBitwiseAndAccess().getEqualityParserRuleCall_0());
            		
            pushFollow(FOLLOW_10);
            this_Equality_0=ruleEquality();

            state._fsp--;


            			current = this_Equality_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalExpressionDsl.g:450:3: ( () ( (lv_op_2_0= '&' ) ) ( (lv_right_3_0= ruleEquality ) ) )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==20) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalExpressionDsl.g:451:4: () ( (lv_op_2_0= '&' ) ) ( (lv_right_3_0= ruleEquality ) )
            	    {
            	    // InternalExpressionDsl.g:451:4: ()
            	    // InternalExpressionDsl.g:452:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getBitwiseAndAccess().getLogicalOperatorLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    // InternalExpressionDsl.g:458:4: ( (lv_op_2_0= '&' ) )
            	    // InternalExpressionDsl.g:459:5: (lv_op_2_0= '&' )
            	    {
            	    // InternalExpressionDsl.g:459:5: (lv_op_2_0= '&' )
            	    // InternalExpressionDsl.g:460:6: lv_op_2_0= '&'
            	    {
            	    lv_op_2_0=(Token)match(input,20,FOLLOW_4); 

            	    						newLeafNode(lv_op_2_0, grammarAccess.getBitwiseAndAccess().getOpAmpersandKeyword_1_1_0());
            	    					

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getBitwiseAndRule());
            	    						}
            	    						setWithLastConsumed(current, "op", lv_op_2_0, "&");
            	    					

            	    }


            	    }

            	    // InternalExpressionDsl.g:472:4: ( (lv_right_3_0= ruleEquality ) )
            	    // InternalExpressionDsl.g:473:5: (lv_right_3_0= ruleEquality )
            	    {
            	    // InternalExpressionDsl.g:473:5: (lv_right_3_0= ruleEquality )
            	    // InternalExpressionDsl.g:474:6: lv_right_3_0= ruleEquality
            	    {

            	    						newCompositeNode(grammarAccess.getBitwiseAndAccess().getRightEqualityParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_10);
            	    lv_right_3_0=ruleEquality();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getBitwiseAndRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"ca.mcgill.sel.ram.expressions.ExpressionDsl.Equality");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBitwiseAnd"


    // $ANTLR start "entryRuleEquality"
    // InternalExpressionDsl.g:496:1: entryRuleEquality returns [EObject current=null] : iv_ruleEquality= ruleEquality EOF ;
    public final EObject entryRuleEquality() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEquality = null;


        try {
            // InternalExpressionDsl.g:496:49: (iv_ruleEquality= ruleEquality EOF )
            // InternalExpressionDsl.g:497:2: iv_ruleEquality= ruleEquality EOF
            {
             newCompositeNode(grammarAccess.getEqualityRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEquality=ruleEquality();

            state._fsp--;

             current =iv_ruleEquality; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEquality"


    // $ANTLR start "ruleEquality"
    // InternalExpressionDsl.g:503:1: ruleEquality returns [EObject current=null] : (this_Comparison_0= ruleComparison ( () ( ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) ) ) ( (lv_right_3_0= ruleComparison ) ) )* ) ;
    public final EObject ruleEquality() throws RecognitionException {
        EObject current = null;

        Token lv_op_2_1=null;
        Token lv_op_2_2=null;
        EObject this_Comparison_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalExpressionDsl.g:509:2: ( (this_Comparison_0= ruleComparison ( () ( ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) ) ) ( (lv_right_3_0= ruleComparison ) ) )* ) )
            // InternalExpressionDsl.g:510:2: (this_Comparison_0= ruleComparison ( () ( ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) ) ) ( (lv_right_3_0= ruleComparison ) ) )* )
            {
            // InternalExpressionDsl.g:510:2: (this_Comparison_0= ruleComparison ( () ( ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) ) ) ( (lv_right_3_0= ruleComparison ) ) )* )
            // InternalExpressionDsl.g:511:3: this_Comparison_0= ruleComparison ( () ( ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) ) ) ( (lv_right_3_0= ruleComparison ) ) )*
            {

            			newCompositeNode(grammarAccess.getEqualityAccess().getComparisonParserRuleCall_0());
            		
            pushFollow(FOLLOW_11);
            this_Comparison_0=ruleComparison();

            state._fsp--;


            			current = this_Comparison_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalExpressionDsl.g:519:3: ( () ( ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) ) ) ( (lv_right_3_0= ruleComparison ) ) )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( ((LA8_0>=21 && LA8_0<=22)) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalExpressionDsl.g:520:4: () ( ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) ) ) ( (lv_right_3_0= ruleComparison ) )
            	    {
            	    // InternalExpressionDsl.g:520:4: ()
            	    // InternalExpressionDsl.g:521:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getEqualityAccess().getEqualityLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    // InternalExpressionDsl.g:527:4: ( ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) ) )
            	    // InternalExpressionDsl.g:528:5: ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) )
            	    {
            	    // InternalExpressionDsl.g:528:5: ( (lv_op_2_1= '==' | lv_op_2_2= '!=' ) )
            	    // InternalExpressionDsl.g:529:6: (lv_op_2_1= '==' | lv_op_2_2= '!=' )
            	    {
            	    // InternalExpressionDsl.g:529:6: (lv_op_2_1= '==' | lv_op_2_2= '!=' )
            	    int alt7=2;
            	    int LA7_0 = input.LA(1);

            	    if ( (LA7_0==21) ) {
            	        alt7=1;
            	    }
            	    else if ( (LA7_0==22) ) {
            	        alt7=2;
            	    }
            	    else {
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 7, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt7) {
            	        case 1 :
            	            // InternalExpressionDsl.g:530:7: lv_op_2_1= '=='
            	            {
            	            lv_op_2_1=(Token)match(input,21,FOLLOW_4); 

            	            							newLeafNode(lv_op_2_1, grammarAccess.getEqualityAccess().getOpEqualsSignEqualsSignKeyword_1_1_0_0());
            	            						

            	            							if (current==null) {
            	            								current = createModelElement(grammarAccess.getEqualityRule());
            	            							}
            	            							setWithLastConsumed(current, "op", lv_op_2_1, null);
            	            						

            	            }
            	            break;
            	        case 2 :
            	            // InternalExpressionDsl.g:541:7: lv_op_2_2= '!='
            	            {
            	            lv_op_2_2=(Token)match(input,22,FOLLOW_4); 

            	            							newLeafNode(lv_op_2_2, grammarAccess.getEqualityAccess().getOpExclamationMarkEqualsSignKeyword_1_1_0_1());
            	            						

            	            							if (current==null) {
            	            								current = createModelElement(grammarAccess.getEqualityRule());
            	            							}
            	            							setWithLastConsumed(current, "op", lv_op_2_2, null);
            	            						

            	            }
            	            break;

            	    }


            	    }


            	    }

            	    // InternalExpressionDsl.g:554:4: ( (lv_right_3_0= ruleComparison ) )
            	    // InternalExpressionDsl.g:555:5: (lv_right_3_0= ruleComparison )
            	    {
            	    // InternalExpressionDsl.g:555:5: (lv_right_3_0= ruleComparison )
            	    // InternalExpressionDsl.g:556:6: lv_right_3_0= ruleComparison
            	    {

            	    						newCompositeNode(grammarAccess.getEqualityAccess().getRightComparisonParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_11);
            	    lv_right_3_0=ruleComparison();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getEqualityRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"ca.mcgill.sel.ram.expressions.ExpressionDsl.Comparison");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEquality"


    // $ANTLR start "entryRuleComparison"
    // InternalExpressionDsl.g:578:1: entryRuleComparison returns [EObject current=null] : iv_ruleComparison= ruleComparison EOF ;
    public final EObject entryRuleComparison() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleComparison = null;


        try {
            // InternalExpressionDsl.g:578:51: (iv_ruleComparison= ruleComparison EOF )
            // InternalExpressionDsl.g:579:2: iv_ruleComparison= ruleComparison EOF
            {
             newCompositeNode(grammarAccess.getComparisonRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleComparison=ruleComparison();

            state._fsp--;

             current =iv_ruleComparison; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleComparison"


    // $ANTLR start "ruleComparison"
    // InternalExpressionDsl.g:585:1: ruleComparison returns [EObject current=null] : (this_BitwiseShift_0= ruleBitwiseShift ( () ( ( (lv_op_2_1= '>=' | lv_op_2_2= '<=' | lv_op_2_3= '>' | lv_op_2_4= '<' ) ) ) ( (lv_right_3_0= ruleBitwiseShift ) ) )* ) ;
    public final EObject ruleComparison() throws RecognitionException {
        EObject current = null;

        Token lv_op_2_1=null;
        Token lv_op_2_2=null;
        Token lv_op_2_3=null;
        Token lv_op_2_4=null;
        EObject this_BitwiseShift_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalExpressionDsl.g:591:2: ( (this_BitwiseShift_0= ruleBitwiseShift ( () ( ( (lv_op_2_1= '>=' | lv_op_2_2= '<=' | lv_op_2_3= '>' | lv_op_2_4= '<' ) ) ) ( (lv_right_3_0= ruleBitwiseShift ) ) )* ) )
            // InternalExpressionDsl.g:592:2: (this_BitwiseShift_0= ruleBitwiseShift ( () ( ( (lv_op_2_1= '>=' | lv_op_2_2= '<=' | lv_op_2_3= '>' | lv_op_2_4= '<' ) ) ) ( (lv_right_3_0= ruleBitwiseShift ) ) )* )
            {
            // InternalExpressionDsl.g:592:2: (this_BitwiseShift_0= ruleBitwiseShift ( () ( ( (lv_op_2_1= '>=' | lv_op_2_2= '<=' | lv_op_2_3= '>' | lv_op_2_4= '<' ) ) ) ( (lv_right_3_0= ruleBitwiseShift ) ) )* )
            // InternalExpressionDsl.g:593:3: this_BitwiseShift_0= ruleBitwiseShift ( () ( ( (lv_op_2_1= '>=' | lv_op_2_2= '<=' | lv_op_2_3= '>' | lv_op_2_4= '<' ) ) ) ( (lv_right_3_0= ruleBitwiseShift ) ) )*
            {

            			newCompositeNode(grammarAccess.getComparisonAccess().getBitwiseShiftParserRuleCall_0());
            		
            pushFollow(FOLLOW_12);
            this_BitwiseShift_0=ruleBitwiseShift();

            state._fsp--;


            			current = this_BitwiseShift_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalExpressionDsl.g:601:3: ( () ( ( (lv_op_2_1= '>=' | lv_op_2_2= '<=' | lv_op_2_3= '>' | lv_op_2_4= '<' ) ) ) ( (lv_right_3_0= ruleBitwiseShift ) ) )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( ((LA10_0>=23 && LA10_0<=26)) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalExpressionDsl.g:602:4: () ( ( (lv_op_2_1= '>=' | lv_op_2_2= '<=' | lv_op_2_3= '>' | lv_op_2_4= '<' ) ) ) ( (lv_right_3_0= ruleBitwiseShift ) )
            	    {
            	    // InternalExpressionDsl.g:602:4: ()
            	    // InternalExpressionDsl.g:603:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getComparisonAccess().getComparisonLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    // InternalExpressionDsl.g:609:4: ( ( (lv_op_2_1= '>=' | lv_op_2_2= '<=' | lv_op_2_3= '>' | lv_op_2_4= '<' ) ) )
            	    // InternalExpressionDsl.g:610:5: ( (lv_op_2_1= '>=' | lv_op_2_2= '<=' | lv_op_2_3= '>' | lv_op_2_4= '<' ) )
            	    {
            	    // InternalExpressionDsl.g:610:5: ( (lv_op_2_1= '>=' | lv_op_2_2= '<=' | lv_op_2_3= '>' | lv_op_2_4= '<' ) )
            	    // InternalExpressionDsl.g:611:6: (lv_op_2_1= '>=' | lv_op_2_2= '<=' | lv_op_2_3= '>' | lv_op_2_4= '<' )
            	    {
            	    // InternalExpressionDsl.g:611:6: (lv_op_2_1= '>=' | lv_op_2_2= '<=' | lv_op_2_3= '>' | lv_op_2_4= '<' )
            	    int alt9=4;
            	    switch ( input.LA(1) ) {
            	    case 23:
            	        {
            	        alt9=1;
            	        }
            	        break;
            	    case 24:
            	        {
            	        alt9=2;
            	        }
            	        break;
            	    case 25:
            	        {
            	        alt9=3;
            	        }
            	        break;
            	    case 26:
            	        {
            	        alt9=4;
            	        }
            	        break;
            	    default:
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 9, 0, input);

            	        throw nvae;
            	    }

            	    switch (alt9) {
            	        case 1 :
            	            // InternalExpressionDsl.g:612:7: lv_op_2_1= '>='
            	            {
            	            lv_op_2_1=(Token)match(input,23,FOLLOW_4); 

            	            							newLeafNode(lv_op_2_1, grammarAccess.getComparisonAccess().getOpGreaterThanSignEqualsSignKeyword_1_1_0_0());
            	            						

            	            							if (current==null) {
            	            								current = createModelElement(grammarAccess.getComparisonRule());
            	            							}
            	            							setWithLastConsumed(current, "op", lv_op_2_1, null);
            	            						

            	            }
            	            break;
            	        case 2 :
            	            // InternalExpressionDsl.g:623:7: lv_op_2_2= '<='
            	            {
            	            lv_op_2_2=(Token)match(input,24,FOLLOW_4); 

            	            							newLeafNode(lv_op_2_2, grammarAccess.getComparisonAccess().getOpLessThanSignEqualsSignKeyword_1_1_0_1());
            	            						

            	            							if (current==null) {
            	            								current = createModelElement(grammarAccess.getComparisonRule());
            	            							}
            	            							setWithLastConsumed(current, "op", lv_op_2_2, null);
            	            						

            	            }
            	            break;
            	        case 3 :
            	            // InternalExpressionDsl.g:634:7: lv_op_2_3= '>'
            	            {
            	            lv_op_2_3=(Token)match(input,25,FOLLOW_4); 

            	            							newLeafNode(lv_op_2_3, grammarAccess.getComparisonAccess().getOpGreaterThanSignKeyword_1_1_0_2());
            	            						

            	            							if (current==null) {
            	            								current = createModelElement(grammarAccess.getComparisonRule());
            	            							}
            	            							setWithLastConsumed(current, "op", lv_op_2_3, null);
            	            						

            	            }
            	            break;
            	        case 4 :
            	            // InternalExpressionDsl.g:645:7: lv_op_2_4= '<'
            	            {
            	            lv_op_2_4=(Token)match(input,26,FOLLOW_4); 

            	            							newLeafNode(lv_op_2_4, grammarAccess.getComparisonAccess().getOpLessThanSignKeyword_1_1_0_3());
            	            						

            	            							if (current==null) {
            	            								current = createModelElement(grammarAccess.getComparisonRule());
            	            							}
            	            							setWithLastConsumed(current, "op", lv_op_2_4, null);
            	            						

            	            }
            	            break;

            	    }


            	    }


            	    }

            	    // InternalExpressionDsl.g:658:4: ( (lv_right_3_0= ruleBitwiseShift ) )
            	    // InternalExpressionDsl.g:659:5: (lv_right_3_0= ruleBitwiseShift )
            	    {
            	    // InternalExpressionDsl.g:659:5: (lv_right_3_0= ruleBitwiseShift )
            	    // InternalExpressionDsl.g:660:6: lv_right_3_0= ruleBitwiseShift
            	    {

            	    						newCompositeNode(grammarAccess.getComparisonAccess().getRightBitwiseShiftParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_12);
            	    lv_right_3_0=ruleBitwiseShift();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getComparisonRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"ca.mcgill.sel.ram.expressions.ExpressionDsl.BitwiseShift");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleComparison"


    // $ANTLR start "entryRuleBitwiseShift"
    // InternalExpressionDsl.g:682:1: entryRuleBitwiseShift returns [EObject current=null] : iv_ruleBitwiseShift= ruleBitwiseShift EOF ;
    public final EObject entryRuleBitwiseShift() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBitwiseShift = null;


        try {
            // InternalExpressionDsl.g:682:53: (iv_ruleBitwiseShift= ruleBitwiseShift EOF )
            // InternalExpressionDsl.g:683:2: iv_ruleBitwiseShift= ruleBitwiseShift EOF
            {
             newCompositeNode(grammarAccess.getBitwiseShiftRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBitwiseShift=ruleBitwiseShift();

            state._fsp--;

             current =iv_ruleBitwiseShift; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBitwiseShift"


    // $ANTLR start "ruleBitwiseShift"
    // InternalExpressionDsl.g:689:1: ruleBitwiseShift returns [EObject current=null] : (this_PlusOrMinus_0= rulePlusOrMinus ( () ( ( (lv_op_2_1= '>>' | lv_op_2_2= '<<' | lv_op_2_3= '>>>' ) ) ) ( (lv_right_3_0= rulePlusOrMinus ) ) )* ) ;
    public final EObject ruleBitwiseShift() throws RecognitionException {
        EObject current = null;

        Token lv_op_2_1=null;
        Token lv_op_2_2=null;
        Token lv_op_2_3=null;
        EObject this_PlusOrMinus_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalExpressionDsl.g:695:2: ( (this_PlusOrMinus_0= rulePlusOrMinus ( () ( ( (lv_op_2_1= '>>' | lv_op_2_2= '<<' | lv_op_2_3= '>>>' ) ) ) ( (lv_right_3_0= rulePlusOrMinus ) ) )* ) )
            // InternalExpressionDsl.g:696:2: (this_PlusOrMinus_0= rulePlusOrMinus ( () ( ( (lv_op_2_1= '>>' | lv_op_2_2= '<<' | lv_op_2_3= '>>>' ) ) ) ( (lv_right_3_0= rulePlusOrMinus ) ) )* )
            {
            // InternalExpressionDsl.g:696:2: (this_PlusOrMinus_0= rulePlusOrMinus ( () ( ( (lv_op_2_1= '>>' | lv_op_2_2= '<<' | lv_op_2_3= '>>>' ) ) ) ( (lv_right_3_0= rulePlusOrMinus ) ) )* )
            // InternalExpressionDsl.g:697:3: this_PlusOrMinus_0= rulePlusOrMinus ( () ( ( (lv_op_2_1= '>>' | lv_op_2_2= '<<' | lv_op_2_3= '>>>' ) ) ) ( (lv_right_3_0= rulePlusOrMinus ) ) )*
            {

            			newCompositeNode(grammarAccess.getBitwiseShiftAccess().getPlusOrMinusParserRuleCall_0());
            		
            pushFollow(FOLLOW_13);
            this_PlusOrMinus_0=rulePlusOrMinus();

            state._fsp--;


            			current = this_PlusOrMinus_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalExpressionDsl.g:705:3: ( () ( ( (lv_op_2_1= '>>' | lv_op_2_2= '<<' | lv_op_2_3= '>>>' ) ) ) ( (lv_right_3_0= rulePlusOrMinus ) ) )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( ((LA12_0>=27 && LA12_0<=29)) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // InternalExpressionDsl.g:706:4: () ( ( (lv_op_2_1= '>>' | lv_op_2_2= '<<' | lv_op_2_3= '>>>' ) ) ) ( (lv_right_3_0= rulePlusOrMinus ) )
            	    {
            	    // InternalExpressionDsl.g:706:4: ()
            	    // InternalExpressionDsl.g:707:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getBitwiseShiftAccess().getShiftLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    // InternalExpressionDsl.g:713:4: ( ( (lv_op_2_1= '>>' | lv_op_2_2= '<<' | lv_op_2_3= '>>>' ) ) )
            	    // InternalExpressionDsl.g:714:5: ( (lv_op_2_1= '>>' | lv_op_2_2= '<<' | lv_op_2_3= '>>>' ) )
            	    {
            	    // InternalExpressionDsl.g:714:5: ( (lv_op_2_1= '>>' | lv_op_2_2= '<<' | lv_op_2_3= '>>>' ) )
            	    // InternalExpressionDsl.g:715:6: (lv_op_2_1= '>>' | lv_op_2_2= '<<' | lv_op_2_3= '>>>' )
            	    {
            	    // InternalExpressionDsl.g:715:6: (lv_op_2_1= '>>' | lv_op_2_2= '<<' | lv_op_2_3= '>>>' )
            	    int alt11=3;
            	    switch ( input.LA(1) ) {
            	    case 27:
            	        {
            	        alt11=1;
            	        }
            	        break;
            	    case 28:
            	        {
            	        alt11=2;
            	        }
            	        break;
            	    case 29:
            	        {
            	        alt11=3;
            	        }
            	        break;
            	    default:
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 11, 0, input);

            	        throw nvae;
            	    }

            	    switch (alt11) {
            	        case 1 :
            	            // InternalExpressionDsl.g:716:7: lv_op_2_1= '>>'
            	            {
            	            lv_op_2_1=(Token)match(input,27,FOLLOW_4); 

            	            							newLeafNode(lv_op_2_1, grammarAccess.getBitwiseShiftAccess().getOpGreaterThanSignGreaterThanSignKeyword_1_1_0_0());
            	            						

            	            							if (current==null) {
            	            								current = createModelElement(grammarAccess.getBitwiseShiftRule());
            	            							}
            	            							setWithLastConsumed(current, "op", lv_op_2_1, null);
            	            						

            	            }
            	            break;
            	        case 2 :
            	            // InternalExpressionDsl.g:727:7: lv_op_2_2= '<<'
            	            {
            	            lv_op_2_2=(Token)match(input,28,FOLLOW_4); 

            	            							newLeafNode(lv_op_2_2, grammarAccess.getBitwiseShiftAccess().getOpLessThanSignLessThanSignKeyword_1_1_0_1());
            	            						

            	            							if (current==null) {
            	            								current = createModelElement(grammarAccess.getBitwiseShiftRule());
            	            							}
            	            							setWithLastConsumed(current, "op", lv_op_2_2, null);
            	            						

            	            }
            	            break;
            	        case 3 :
            	            // InternalExpressionDsl.g:738:7: lv_op_2_3= '>>>'
            	            {
            	            lv_op_2_3=(Token)match(input,29,FOLLOW_4); 

            	            							newLeafNode(lv_op_2_3, grammarAccess.getBitwiseShiftAccess().getOpGreaterThanSignGreaterThanSignGreaterThanSignKeyword_1_1_0_2());
            	            						

            	            							if (current==null) {
            	            								current = createModelElement(grammarAccess.getBitwiseShiftRule());
            	            							}
            	            							setWithLastConsumed(current, "op", lv_op_2_3, null);
            	            						

            	            }
            	            break;

            	    }


            	    }


            	    }

            	    // InternalExpressionDsl.g:751:4: ( (lv_right_3_0= rulePlusOrMinus ) )
            	    // InternalExpressionDsl.g:752:5: (lv_right_3_0= rulePlusOrMinus )
            	    {
            	    // InternalExpressionDsl.g:752:5: (lv_right_3_0= rulePlusOrMinus )
            	    // InternalExpressionDsl.g:753:6: lv_right_3_0= rulePlusOrMinus
            	    {

            	    						newCompositeNode(grammarAccess.getBitwiseShiftAccess().getRightPlusOrMinusParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_13);
            	    lv_right_3_0=rulePlusOrMinus();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getBitwiseShiftRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"ca.mcgill.sel.ram.expressions.ExpressionDsl.PlusOrMinus");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBitwiseShift"


    // $ANTLR start "entryRulePlusOrMinus"
    // InternalExpressionDsl.g:775:1: entryRulePlusOrMinus returns [EObject current=null] : iv_rulePlusOrMinus= rulePlusOrMinus EOF ;
    public final EObject entryRulePlusOrMinus() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePlusOrMinus = null;


        try {
            // InternalExpressionDsl.g:775:52: (iv_rulePlusOrMinus= rulePlusOrMinus EOF )
            // InternalExpressionDsl.g:776:2: iv_rulePlusOrMinus= rulePlusOrMinus EOF
            {
             newCompositeNode(grammarAccess.getPlusOrMinusRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePlusOrMinus=rulePlusOrMinus();

            state._fsp--;

             current =iv_rulePlusOrMinus; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePlusOrMinus"


    // $ANTLR start "rulePlusOrMinus"
    // InternalExpressionDsl.g:782:1: rulePlusOrMinus returns [EObject current=null] : (this_MulOrDiv_0= ruleMulOrDiv ( ( ( () otherlv_2= '+' ) | ( () otherlv_4= '-' ) ) ( (lv_right_5_0= ruleMulOrDiv ) ) )* ) ;
    public final EObject rulePlusOrMinus() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject this_MulOrDiv_0 = null;

        EObject lv_right_5_0 = null;



        	enterRule();

        try {
            // InternalExpressionDsl.g:788:2: ( (this_MulOrDiv_0= ruleMulOrDiv ( ( ( () otherlv_2= '+' ) | ( () otherlv_4= '-' ) ) ( (lv_right_5_0= ruleMulOrDiv ) ) )* ) )
            // InternalExpressionDsl.g:789:2: (this_MulOrDiv_0= ruleMulOrDiv ( ( ( () otherlv_2= '+' ) | ( () otherlv_4= '-' ) ) ( (lv_right_5_0= ruleMulOrDiv ) ) )* )
            {
            // InternalExpressionDsl.g:789:2: (this_MulOrDiv_0= ruleMulOrDiv ( ( ( () otherlv_2= '+' ) | ( () otherlv_4= '-' ) ) ( (lv_right_5_0= ruleMulOrDiv ) ) )* )
            // InternalExpressionDsl.g:790:3: this_MulOrDiv_0= ruleMulOrDiv ( ( ( () otherlv_2= '+' ) | ( () otherlv_4= '-' ) ) ( (lv_right_5_0= ruleMulOrDiv ) ) )*
            {

            			newCompositeNode(grammarAccess.getPlusOrMinusAccess().getMulOrDivParserRuleCall_0());
            		
            pushFollow(FOLLOW_14);
            this_MulOrDiv_0=ruleMulOrDiv();

            state._fsp--;


            			current = this_MulOrDiv_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalExpressionDsl.g:798:3: ( ( ( () otherlv_2= '+' ) | ( () otherlv_4= '-' ) ) ( (lv_right_5_0= ruleMulOrDiv ) ) )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( ((LA14_0>=30 && LA14_0<=31)) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // InternalExpressionDsl.g:799:4: ( ( () otherlv_2= '+' ) | ( () otherlv_4= '-' ) ) ( (lv_right_5_0= ruleMulOrDiv ) )
            	    {
            	    // InternalExpressionDsl.g:799:4: ( ( () otherlv_2= '+' ) | ( () otherlv_4= '-' ) )
            	    int alt13=2;
            	    int LA13_0 = input.LA(1);

            	    if ( (LA13_0==30) ) {
            	        alt13=1;
            	    }
            	    else if ( (LA13_0==31) ) {
            	        alt13=2;
            	    }
            	    else {
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 13, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt13) {
            	        case 1 :
            	            // InternalExpressionDsl.g:800:5: ( () otherlv_2= '+' )
            	            {
            	            // InternalExpressionDsl.g:800:5: ( () otherlv_2= '+' )
            	            // InternalExpressionDsl.g:801:6: () otherlv_2= '+'
            	            {
            	            // InternalExpressionDsl.g:801:6: ()
            	            // InternalExpressionDsl.g:802:7: 
            	            {

            	            							current = forceCreateModelElementAndSet(
            	            								grammarAccess.getPlusOrMinusAccess().getPlusLeftAction_1_0_0_0(),
            	            								current);
            	            						

            	            }

            	            otherlv_2=(Token)match(input,30,FOLLOW_4); 

            	            						newLeafNode(otherlv_2, grammarAccess.getPlusOrMinusAccess().getPlusSignKeyword_1_0_0_1());
            	            					

            	            }


            	            }
            	            break;
            	        case 2 :
            	            // InternalExpressionDsl.g:814:5: ( () otherlv_4= '-' )
            	            {
            	            // InternalExpressionDsl.g:814:5: ( () otherlv_4= '-' )
            	            // InternalExpressionDsl.g:815:6: () otherlv_4= '-'
            	            {
            	            // InternalExpressionDsl.g:815:6: ()
            	            // InternalExpressionDsl.g:816:7: 
            	            {

            	            							current = forceCreateModelElementAndSet(
            	            								grammarAccess.getPlusOrMinusAccess().getMinusLeftAction_1_0_1_0(),
            	            								current);
            	            						

            	            }

            	            otherlv_4=(Token)match(input,31,FOLLOW_4); 

            	            						newLeafNode(otherlv_4, grammarAccess.getPlusOrMinusAccess().getHyphenMinusKeyword_1_0_1_1());
            	            					

            	            }


            	            }
            	            break;

            	    }

            	    // InternalExpressionDsl.g:828:4: ( (lv_right_5_0= ruleMulOrDiv ) )
            	    // InternalExpressionDsl.g:829:5: (lv_right_5_0= ruleMulOrDiv )
            	    {
            	    // InternalExpressionDsl.g:829:5: (lv_right_5_0= ruleMulOrDiv )
            	    // InternalExpressionDsl.g:830:6: lv_right_5_0= ruleMulOrDiv
            	    {

            	    						newCompositeNode(grammarAccess.getPlusOrMinusAccess().getRightMulOrDivParserRuleCall_1_1_0());
            	    					
            	    pushFollow(FOLLOW_14);
            	    lv_right_5_0=ruleMulOrDiv();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getPlusOrMinusRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_5_0,
            	    							"ca.mcgill.sel.ram.expressions.ExpressionDsl.MulOrDiv");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePlusOrMinus"


    // $ANTLR start "entryRuleMulOrDiv"
    // InternalExpressionDsl.g:852:1: entryRuleMulOrDiv returns [EObject current=null] : iv_ruleMulOrDiv= ruleMulOrDiv EOF ;
    public final EObject entryRuleMulOrDiv() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMulOrDiv = null;


        try {
            // InternalExpressionDsl.g:852:49: (iv_ruleMulOrDiv= ruleMulOrDiv EOF )
            // InternalExpressionDsl.g:853:2: iv_ruleMulOrDiv= ruleMulOrDiv EOF
            {
             newCompositeNode(grammarAccess.getMulOrDivRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleMulOrDiv=ruleMulOrDiv();

            state._fsp--;

             current =iv_ruleMulOrDiv; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMulOrDiv"


    // $ANTLR start "ruleMulOrDiv"
    // InternalExpressionDsl.g:859:1: ruleMulOrDiv returns [EObject current=null] : (this_Primary_0= rulePrimary ( () ( ( (lv_op_2_1= '*' | lv_op_2_2= '/' | lv_op_2_3= '%' ) ) ) ( (lv_right_3_0= rulePrimary ) ) )* ) ;
    public final EObject ruleMulOrDiv() throws RecognitionException {
        EObject current = null;

        Token lv_op_2_1=null;
        Token lv_op_2_2=null;
        Token lv_op_2_3=null;
        EObject this_Primary_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalExpressionDsl.g:865:2: ( (this_Primary_0= rulePrimary ( () ( ( (lv_op_2_1= '*' | lv_op_2_2= '/' | lv_op_2_3= '%' ) ) ) ( (lv_right_3_0= rulePrimary ) ) )* ) )
            // InternalExpressionDsl.g:866:2: (this_Primary_0= rulePrimary ( () ( ( (lv_op_2_1= '*' | lv_op_2_2= '/' | lv_op_2_3= '%' ) ) ) ( (lv_right_3_0= rulePrimary ) ) )* )
            {
            // InternalExpressionDsl.g:866:2: (this_Primary_0= rulePrimary ( () ( ( (lv_op_2_1= '*' | lv_op_2_2= '/' | lv_op_2_3= '%' ) ) ) ( (lv_right_3_0= rulePrimary ) ) )* )
            // InternalExpressionDsl.g:867:3: this_Primary_0= rulePrimary ( () ( ( (lv_op_2_1= '*' | lv_op_2_2= '/' | lv_op_2_3= '%' ) ) ) ( (lv_right_3_0= rulePrimary ) ) )*
            {

            			newCompositeNode(grammarAccess.getMulOrDivAccess().getPrimaryParserRuleCall_0());
            		
            pushFollow(FOLLOW_15);
            this_Primary_0=rulePrimary();

            state._fsp--;


            			current = this_Primary_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalExpressionDsl.g:875:3: ( () ( ( (lv_op_2_1= '*' | lv_op_2_2= '/' | lv_op_2_3= '%' ) ) ) ( (lv_right_3_0= rulePrimary ) ) )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( ((LA16_0>=32 && LA16_0<=34)) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // InternalExpressionDsl.g:876:4: () ( ( (lv_op_2_1= '*' | lv_op_2_2= '/' | lv_op_2_3= '%' ) ) ) ( (lv_right_3_0= rulePrimary ) )
            	    {
            	    // InternalExpressionDsl.g:876:4: ()
            	    // InternalExpressionDsl.g:877:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getMulOrDivAccess().getMulDivModLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    // InternalExpressionDsl.g:883:4: ( ( (lv_op_2_1= '*' | lv_op_2_2= '/' | lv_op_2_3= '%' ) ) )
            	    // InternalExpressionDsl.g:884:5: ( (lv_op_2_1= '*' | lv_op_2_2= '/' | lv_op_2_3= '%' ) )
            	    {
            	    // InternalExpressionDsl.g:884:5: ( (lv_op_2_1= '*' | lv_op_2_2= '/' | lv_op_2_3= '%' ) )
            	    // InternalExpressionDsl.g:885:6: (lv_op_2_1= '*' | lv_op_2_2= '/' | lv_op_2_3= '%' )
            	    {
            	    // InternalExpressionDsl.g:885:6: (lv_op_2_1= '*' | lv_op_2_2= '/' | lv_op_2_3= '%' )
            	    int alt15=3;
            	    switch ( input.LA(1) ) {
            	    case 32:
            	        {
            	        alt15=1;
            	        }
            	        break;
            	    case 33:
            	        {
            	        alt15=2;
            	        }
            	        break;
            	    case 34:
            	        {
            	        alt15=3;
            	        }
            	        break;
            	    default:
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 15, 0, input);

            	        throw nvae;
            	    }

            	    switch (alt15) {
            	        case 1 :
            	            // InternalExpressionDsl.g:886:7: lv_op_2_1= '*'
            	            {
            	            lv_op_2_1=(Token)match(input,32,FOLLOW_4); 

            	            							newLeafNode(lv_op_2_1, grammarAccess.getMulOrDivAccess().getOpAsteriskKeyword_1_1_0_0());
            	            						

            	            							if (current==null) {
            	            								current = createModelElement(grammarAccess.getMulOrDivRule());
            	            							}
            	            							setWithLastConsumed(current, "op", lv_op_2_1, null);
            	            						

            	            }
            	            break;
            	        case 2 :
            	            // InternalExpressionDsl.g:897:7: lv_op_2_2= '/'
            	            {
            	            lv_op_2_2=(Token)match(input,33,FOLLOW_4); 

            	            							newLeafNode(lv_op_2_2, grammarAccess.getMulOrDivAccess().getOpSolidusKeyword_1_1_0_1());
            	            						

            	            							if (current==null) {
            	            								current = createModelElement(grammarAccess.getMulOrDivRule());
            	            							}
            	            							setWithLastConsumed(current, "op", lv_op_2_2, null);
            	            						

            	            }
            	            break;
            	        case 3 :
            	            // InternalExpressionDsl.g:908:7: lv_op_2_3= '%'
            	            {
            	            lv_op_2_3=(Token)match(input,34,FOLLOW_4); 

            	            							newLeafNode(lv_op_2_3, grammarAccess.getMulOrDivAccess().getOpPercentSignKeyword_1_1_0_2());
            	            						

            	            							if (current==null) {
            	            								current = createModelElement(grammarAccess.getMulOrDivRule());
            	            							}
            	            							setWithLastConsumed(current, "op", lv_op_2_3, null);
            	            						

            	            }
            	            break;

            	    }


            	    }


            	    }

            	    // InternalExpressionDsl.g:921:4: ( (lv_right_3_0= rulePrimary ) )
            	    // InternalExpressionDsl.g:922:5: (lv_right_3_0= rulePrimary )
            	    {
            	    // InternalExpressionDsl.g:922:5: (lv_right_3_0= rulePrimary )
            	    // InternalExpressionDsl.g:923:6: lv_right_3_0= rulePrimary
            	    {

            	    						newCompositeNode(grammarAccess.getMulOrDivAccess().getRightPrimaryParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_15);
            	    lv_right_3_0=rulePrimary();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getMulOrDivRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"ca.mcgill.sel.ram.expressions.ExpressionDsl.Primary");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMulOrDiv"


    // $ANTLR start "entryRulePrimary"
    // InternalExpressionDsl.g:945:1: entryRulePrimary returns [EObject current=null] : iv_rulePrimary= rulePrimary EOF ;
    public final EObject entryRulePrimary() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePrimary = null;


        try {
            // InternalExpressionDsl.g:945:48: (iv_rulePrimary= rulePrimary EOF )
            // InternalExpressionDsl.g:946:2: iv_rulePrimary= rulePrimary EOF
            {
             newCompositeNode(grammarAccess.getPrimaryRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePrimary=rulePrimary();

            state._fsp--;

             current =iv_rulePrimary; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePrimary"


    // $ANTLR start "rulePrimary"
    // InternalExpressionDsl.g:952:1: rulePrimary returns [EObject current=null] : ( ( () otherlv_1= '!' ( (lv_expression_2_0= rulePrimary ) ) ) | ( () otherlv_4= '-' ( (lv_expression_5_0= rulePrimary ) ) ) | (otherlv_6= '(' this_ValueSpecification_7= ruleValueSpecification otherlv_8= ')' ) | ( () ( ( (lv_op_10_1= '++' | lv_op_10_2= '--' ) ) ) ( (lv_expression_11_0= ruleAtomic ) ) ) | ( () ( (lv_expression_13_0= ruleAtomic ) ) ( ( (lv_op_14_1= '++' | lv_op_14_2= '--' ) ) ) ) | this_Atomic_15= ruleAtomic ) ;
    public final EObject rulePrimary() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token lv_op_10_1=null;
        Token lv_op_10_2=null;
        Token lv_op_14_1=null;
        Token lv_op_14_2=null;
        EObject lv_expression_2_0 = null;

        EObject lv_expression_5_0 = null;

        EObject this_ValueSpecification_7 = null;

        EObject lv_expression_11_0 = null;

        EObject lv_expression_13_0 = null;

        EObject this_Atomic_15 = null;



        	enterRule();

        try {
            // InternalExpressionDsl.g:958:2: ( ( ( () otherlv_1= '!' ( (lv_expression_2_0= rulePrimary ) ) ) | ( () otherlv_4= '-' ( (lv_expression_5_0= rulePrimary ) ) ) | (otherlv_6= '(' this_ValueSpecification_7= ruleValueSpecification otherlv_8= ')' ) | ( () ( ( (lv_op_10_1= '++' | lv_op_10_2= '--' ) ) ) ( (lv_expression_11_0= ruleAtomic ) ) ) | ( () ( (lv_expression_13_0= ruleAtomic ) ) ( ( (lv_op_14_1= '++' | lv_op_14_2= '--' ) ) ) ) | this_Atomic_15= ruleAtomic ) )
            // InternalExpressionDsl.g:959:2: ( ( () otherlv_1= '!' ( (lv_expression_2_0= rulePrimary ) ) ) | ( () otherlv_4= '-' ( (lv_expression_5_0= rulePrimary ) ) ) | (otherlv_6= '(' this_ValueSpecification_7= ruleValueSpecification otherlv_8= ')' ) | ( () ( ( (lv_op_10_1= '++' | lv_op_10_2= '--' ) ) ) ( (lv_expression_11_0= ruleAtomic ) ) ) | ( () ( (lv_expression_13_0= ruleAtomic ) ) ( ( (lv_op_14_1= '++' | lv_op_14_2= '--' ) ) ) ) | this_Atomic_15= ruleAtomic )
            {
            // InternalExpressionDsl.g:959:2: ( ( () otherlv_1= '!' ( (lv_expression_2_0= rulePrimary ) ) ) | ( () otherlv_4= '-' ( (lv_expression_5_0= rulePrimary ) ) ) | (otherlv_6= '(' this_ValueSpecification_7= ruleValueSpecification otherlv_8= ')' ) | ( () ( ( (lv_op_10_1= '++' | lv_op_10_2= '--' ) ) ) ( (lv_expression_11_0= ruleAtomic ) ) ) | ( () ( (lv_expression_13_0= ruleAtomic ) ) ( ( (lv_op_14_1= '++' | lv_op_14_2= '--' ) ) ) ) | this_Atomic_15= ruleAtomic )
            int alt19=6;
            alt19 = dfa19.predict(input);
            switch (alt19) {
                case 1 :
                    // InternalExpressionDsl.g:960:3: ( () otherlv_1= '!' ( (lv_expression_2_0= rulePrimary ) ) )
                    {
                    // InternalExpressionDsl.g:960:3: ( () otherlv_1= '!' ( (lv_expression_2_0= rulePrimary ) ) )
                    // InternalExpressionDsl.g:961:4: () otherlv_1= '!' ( (lv_expression_2_0= rulePrimary ) )
                    {
                    // InternalExpressionDsl.g:961:4: ()
                    // InternalExpressionDsl.g:962:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getPrimaryAccess().getNotAction_0_0(),
                    						current);
                    				

                    }

                    otherlv_1=(Token)match(input,35,FOLLOW_4); 

                    				newLeafNode(otherlv_1, grammarAccess.getPrimaryAccess().getExclamationMarkKeyword_0_1());
                    			
                    // InternalExpressionDsl.g:972:4: ( (lv_expression_2_0= rulePrimary ) )
                    // InternalExpressionDsl.g:973:5: (lv_expression_2_0= rulePrimary )
                    {
                    // InternalExpressionDsl.g:973:5: (lv_expression_2_0= rulePrimary )
                    // InternalExpressionDsl.g:974:6: lv_expression_2_0= rulePrimary
                    {

                    						newCompositeNode(grammarAccess.getPrimaryAccess().getExpressionPrimaryParserRuleCall_0_2_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_expression_2_0=rulePrimary();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPrimaryRule());
                    						}
                    						set(
                    							current,
                    							"expression",
                    							lv_expression_2_0,
                    							"ca.mcgill.sel.ram.expressions.ExpressionDsl.Primary");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalExpressionDsl.g:993:3: ( () otherlv_4= '-' ( (lv_expression_5_0= rulePrimary ) ) )
                    {
                    // InternalExpressionDsl.g:993:3: ( () otherlv_4= '-' ( (lv_expression_5_0= rulePrimary ) ) )
                    // InternalExpressionDsl.g:994:4: () otherlv_4= '-' ( (lv_expression_5_0= rulePrimary ) )
                    {
                    // InternalExpressionDsl.g:994:4: ()
                    // InternalExpressionDsl.g:995:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getPrimaryAccess().getUnaryMinusAction_1_0(),
                    						current);
                    				

                    }

                    otherlv_4=(Token)match(input,31,FOLLOW_4); 

                    				newLeafNode(otherlv_4, grammarAccess.getPrimaryAccess().getHyphenMinusKeyword_1_1());
                    			
                    // InternalExpressionDsl.g:1005:4: ( (lv_expression_5_0= rulePrimary ) )
                    // InternalExpressionDsl.g:1006:5: (lv_expression_5_0= rulePrimary )
                    {
                    // InternalExpressionDsl.g:1006:5: (lv_expression_5_0= rulePrimary )
                    // InternalExpressionDsl.g:1007:6: lv_expression_5_0= rulePrimary
                    {

                    						newCompositeNode(grammarAccess.getPrimaryAccess().getExpressionPrimaryParserRuleCall_1_2_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_expression_5_0=rulePrimary();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPrimaryRule());
                    						}
                    						set(
                    							current,
                    							"expression",
                    							lv_expression_5_0,
                    							"ca.mcgill.sel.ram.expressions.ExpressionDsl.Primary");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalExpressionDsl.g:1026:3: (otherlv_6= '(' this_ValueSpecification_7= ruleValueSpecification otherlv_8= ')' )
                    {
                    // InternalExpressionDsl.g:1026:3: (otherlv_6= '(' this_ValueSpecification_7= ruleValueSpecification otherlv_8= ')' )
                    // InternalExpressionDsl.g:1027:4: otherlv_6= '(' this_ValueSpecification_7= ruleValueSpecification otherlv_8= ')'
                    {
                    otherlv_6=(Token)match(input,36,FOLLOW_4); 

                    				newLeafNode(otherlv_6, grammarAccess.getPrimaryAccess().getLeftParenthesisKeyword_2_0());
                    			

                    				newCompositeNode(grammarAccess.getPrimaryAccess().getValueSpecificationParserRuleCall_2_1());
                    			
                    pushFollow(FOLLOW_16);
                    this_ValueSpecification_7=ruleValueSpecification();

                    state._fsp--;


                    				current = this_ValueSpecification_7;
                    				afterParserOrEnumRuleCall();
                    			
                    otherlv_8=(Token)match(input,37,FOLLOW_2); 

                    				newLeafNode(otherlv_8, grammarAccess.getPrimaryAccess().getRightParenthesisKeyword_2_2());
                    			

                    }


                    }
                    break;
                case 4 :
                    // InternalExpressionDsl.g:1045:3: ( () ( ( (lv_op_10_1= '++' | lv_op_10_2= '--' ) ) ) ( (lv_expression_11_0= ruleAtomic ) ) )
                    {
                    // InternalExpressionDsl.g:1045:3: ( () ( ( (lv_op_10_1= '++' | lv_op_10_2= '--' ) ) ) ( (lv_expression_11_0= ruleAtomic ) ) )
                    // InternalExpressionDsl.g:1046:4: () ( ( (lv_op_10_1= '++' | lv_op_10_2= '--' ) ) ) ( (lv_expression_11_0= ruleAtomic ) )
                    {
                    // InternalExpressionDsl.g:1046:4: ()
                    // InternalExpressionDsl.g:1047:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getPrimaryAccess().getPreIncrementOrDecrementAction_3_0(),
                    						current);
                    				

                    }

                    // InternalExpressionDsl.g:1053:4: ( ( (lv_op_10_1= '++' | lv_op_10_2= '--' ) ) )
                    // InternalExpressionDsl.g:1054:5: ( (lv_op_10_1= '++' | lv_op_10_2= '--' ) )
                    {
                    // InternalExpressionDsl.g:1054:5: ( (lv_op_10_1= '++' | lv_op_10_2= '--' ) )
                    // InternalExpressionDsl.g:1055:6: (lv_op_10_1= '++' | lv_op_10_2= '--' )
                    {
                    // InternalExpressionDsl.g:1055:6: (lv_op_10_1= '++' | lv_op_10_2= '--' )
                    int alt17=2;
                    int LA17_0 = input.LA(1);

                    if ( (LA17_0==38) ) {
                        alt17=1;
                    }
                    else if ( (LA17_0==39) ) {
                        alt17=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 17, 0, input);

                        throw nvae;
                    }
                    switch (alt17) {
                        case 1 :
                            // InternalExpressionDsl.g:1056:7: lv_op_10_1= '++'
                            {
                            lv_op_10_1=(Token)match(input,38,FOLLOW_17); 

                            							newLeafNode(lv_op_10_1, grammarAccess.getPrimaryAccess().getOpPlusSignPlusSignKeyword_3_1_0_0());
                            						

                            							if (current==null) {
                            								current = createModelElement(grammarAccess.getPrimaryRule());
                            							}
                            							setWithLastConsumed(current, "op", lv_op_10_1, null);
                            						

                            }
                            break;
                        case 2 :
                            // InternalExpressionDsl.g:1067:7: lv_op_10_2= '--'
                            {
                            lv_op_10_2=(Token)match(input,39,FOLLOW_17); 

                            							newLeafNode(lv_op_10_2, grammarAccess.getPrimaryAccess().getOpHyphenMinusHyphenMinusKeyword_3_1_0_1());
                            						

                            							if (current==null) {
                            								current = createModelElement(grammarAccess.getPrimaryRule());
                            							}
                            							setWithLastConsumed(current, "op", lv_op_10_2, null);
                            						

                            }
                            break;

                    }


                    }


                    }

                    // InternalExpressionDsl.g:1080:4: ( (lv_expression_11_0= ruleAtomic ) )
                    // InternalExpressionDsl.g:1081:5: (lv_expression_11_0= ruleAtomic )
                    {
                    // InternalExpressionDsl.g:1081:5: (lv_expression_11_0= ruleAtomic )
                    // InternalExpressionDsl.g:1082:6: lv_expression_11_0= ruleAtomic
                    {

                    						newCompositeNode(grammarAccess.getPrimaryAccess().getExpressionAtomicParserRuleCall_3_2_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_expression_11_0=ruleAtomic();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPrimaryRule());
                    						}
                    						set(
                    							current,
                    							"expression",
                    							lv_expression_11_0,
                    							"ca.mcgill.sel.ram.expressions.ExpressionDsl.Atomic");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;
                case 5 :
                    // InternalExpressionDsl.g:1101:3: ( () ( (lv_expression_13_0= ruleAtomic ) ) ( ( (lv_op_14_1= '++' | lv_op_14_2= '--' ) ) ) )
                    {
                    // InternalExpressionDsl.g:1101:3: ( () ( (lv_expression_13_0= ruleAtomic ) ) ( ( (lv_op_14_1= '++' | lv_op_14_2= '--' ) ) ) )
                    // InternalExpressionDsl.g:1102:4: () ( (lv_expression_13_0= ruleAtomic ) ) ( ( (lv_op_14_1= '++' | lv_op_14_2= '--' ) ) )
                    {
                    // InternalExpressionDsl.g:1102:4: ()
                    // InternalExpressionDsl.g:1103:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getPrimaryAccess().getPostIncrementOrDecrementAction_4_0(),
                    						current);
                    				

                    }

                    // InternalExpressionDsl.g:1109:4: ( (lv_expression_13_0= ruleAtomic ) )
                    // InternalExpressionDsl.g:1110:5: (lv_expression_13_0= ruleAtomic )
                    {
                    // InternalExpressionDsl.g:1110:5: (lv_expression_13_0= ruleAtomic )
                    // InternalExpressionDsl.g:1111:6: lv_expression_13_0= ruleAtomic
                    {

                    						newCompositeNode(grammarAccess.getPrimaryAccess().getExpressionAtomicParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_18);
                    lv_expression_13_0=ruleAtomic();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPrimaryRule());
                    						}
                    						set(
                    							current,
                    							"expression",
                    							lv_expression_13_0,
                    							"ca.mcgill.sel.ram.expressions.ExpressionDsl.Atomic");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalExpressionDsl.g:1128:4: ( ( (lv_op_14_1= '++' | lv_op_14_2= '--' ) ) )
                    // InternalExpressionDsl.g:1129:5: ( (lv_op_14_1= '++' | lv_op_14_2= '--' ) )
                    {
                    // InternalExpressionDsl.g:1129:5: ( (lv_op_14_1= '++' | lv_op_14_2= '--' ) )
                    // InternalExpressionDsl.g:1130:6: (lv_op_14_1= '++' | lv_op_14_2= '--' )
                    {
                    // InternalExpressionDsl.g:1130:6: (lv_op_14_1= '++' | lv_op_14_2= '--' )
                    int alt18=2;
                    int LA18_0 = input.LA(1);

                    if ( (LA18_0==38) ) {
                        alt18=1;
                    }
                    else if ( (LA18_0==39) ) {
                        alt18=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 18, 0, input);

                        throw nvae;
                    }
                    switch (alt18) {
                        case 1 :
                            // InternalExpressionDsl.g:1131:7: lv_op_14_1= '++'
                            {
                            lv_op_14_1=(Token)match(input,38,FOLLOW_2); 

                            							newLeafNode(lv_op_14_1, grammarAccess.getPrimaryAccess().getOpPlusSignPlusSignKeyword_4_2_0_0());
                            						

                            							if (current==null) {
                            								current = createModelElement(grammarAccess.getPrimaryRule());
                            							}
                            							setWithLastConsumed(current, "op", lv_op_14_1, null);
                            						

                            }
                            break;
                        case 2 :
                            // InternalExpressionDsl.g:1142:7: lv_op_14_2= '--'
                            {
                            lv_op_14_2=(Token)match(input,39,FOLLOW_2); 

                            							newLeafNode(lv_op_14_2, grammarAccess.getPrimaryAccess().getOpHyphenMinusHyphenMinusKeyword_4_2_0_1());
                            						

                            							if (current==null) {
                            								current = createModelElement(grammarAccess.getPrimaryRule());
                            							}
                            							setWithLastConsumed(current, "op", lv_op_14_2, null);
                            						

                            }
                            break;

                    }


                    }


                    }


                    }


                    }
                    break;
                case 6 :
                    // InternalExpressionDsl.g:1157:3: this_Atomic_15= ruleAtomic
                    {

                    			newCompositeNode(grammarAccess.getPrimaryAccess().getAtomicParserRuleCall_5());
                    		
                    pushFollow(FOLLOW_2);
                    this_Atomic_15=ruleAtomic();

                    state._fsp--;


                    			current = this_Atomic_15;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePrimary"


    // $ANTLR start "entryRuleAtomic"
    // InternalExpressionDsl.g:1169:1: entryRuleAtomic returns [EObject current=null] : iv_ruleAtomic= ruleAtomic EOF ;
    public final EObject entryRuleAtomic() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAtomic = null;


        try {
            // InternalExpressionDsl.g:1169:47: (iv_ruleAtomic= ruleAtomic EOF )
            // InternalExpressionDsl.g:1170:2: iv_ruleAtomic= ruleAtomic EOF
            {
             newCompositeNode(grammarAccess.getAtomicRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAtomic=ruleAtomic();

            state._fsp--;

             current =iv_ruleAtomic; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAtomic"


    // $ANTLR start "ruleAtomic"
    // InternalExpressionDsl.g:1176:1: ruleAtomic returns [EObject current=null] : ( ( () ( (lv_value_1_0= ruleE_INT ) ) ) | ( () ( (lv_value_3_0= RULE_E_STRING ) ) ) | ( () ( (lv_body_5_0= ruleFeature ) ) ) | ( () ( (lv_value_7_0= RULE_E_BOOLEAN ) ) ) | ( () ( (lv_value_9_0= RULE_E_FLOAT ) ) ) | ( () ( (lv_value_11_0= RULE_E_LONG ) ) ) | ( () ( (lv_value_13_0= RULE_E_DOUBLE ) ) ) | ( () ( (lv_value_15_0= RULE_E_BYTE ) ) ) | ( () otherlv_17= '\\'' ( (lv_value_18_0= RULE_E_CHAR ) ) otherlv_19= '\\'' ) | ( () this_E_NULL_21= RULE_E_NULL ) ) ;
    public final EObject ruleAtomic() throws RecognitionException {
        EObject current = null;

        Token lv_value_3_0=null;
        Token lv_value_7_0=null;
        Token lv_value_9_0=null;
        Token lv_value_11_0=null;
        Token lv_value_13_0=null;
        Token lv_value_15_0=null;
        Token otherlv_17=null;
        Token lv_value_18_0=null;
        Token otherlv_19=null;
        Token this_E_NULL_21=null;
        AntlrDatatypeRuleToken lv_value_1_0 = null;

        AntlrDatatypeRuleToken lv_body_5_0 = null;



        	enterRule();

        try {
            // InternalExpressionDsl.g:1182:2: ( ( ( () ( (lv_value_1_0= ruleE_INT ) ) ) | ( () ( (lv_value_3_0= RULE_E_STRING ) ) ) | ( () ( (lv_body_5_0= ruleFeature ) ) ) | ( () ( (lv_value_7_0= RULE_E_BOOLEAN ) ) ) | ( () ( (lv_value_9_0= RULE_E_FLOAT ) ) ) | ( () ( (lv_value_11_0= RULE_E_LONG ) ) ) | ( () ( (lv_value_13_0= RULE_E_DOUBLE ) ) ) | ( () ( (lv_value_15_0= RULE_E_BYTE ) ) ) | ( () otherlv_17= '\\'' ( (lv_value_18_0= RULE_E_CHAR ) ) otherlv_19= '\\'' ) | ( () this_E_NULL_21= RULE_E_NULL ) ) )
            // InternalExpressionDsl.g:1183:2: ( ( () ( (lv_value_1_0= ruleE_INT ) ) ) | ( () ( (lv_value_3_0= RULE_E_STRING ) ) ) | ( () ( (lv_body_5_0= ruleFeature ) ) ) | ( () ( (lv_value_7_0= RULE_E_BOOLEAN ) ) ) | ( () ( (lv_value_9_0= RULE_E_FLOAT ) ) ) | ( () ( (lv_value_11_0= RULE_E_LONG ) ) ) | ( () ( (lv_value_13_0= RULE_E_DOUBLE ) ) ) | ( () ( (lv_value_15_0= RULE_E_BYTE ) ) ) | ( () otherlv_17= '\\'' ( (lv_value_18_0= RULE_E_CHAR ) ) otherlv_19= '\\'' ) | ( () this_E_NULL_21= RULE_E_NULL ) )
            {
            // InternalExpressionDsl.g:1183:2: ( ( () ( (lv_value_1_0= ruleE_INT ) ) ) | ( () ( (lv_value_3_0= RULE_E_STRING ) ) ) | ( () ( (lv_body_5_0= ruleFeature ) ) ) | ( () ( (lv_value_7_0= RULE_E_BOOLEAN ) ) ) | ( () ( (lv_value_9_0= RULE_E_FLOAT ) ) ) | ( () ( (lv_value_11_0= RULE_E_LONG ) ) ) | ( () ( (lv_value_13_0= RULE_E_DOUBLE ) ) ) | ( () ( (lv_value_15_0= RULE_E_BYTE ) ) ) | ( () otherlv_17= '\\'' ( (lv_value_18_0= RULE_E_CHAR ) ) otherlv_19= '\\'' ) | ( () this_E_NULL_21= RULE_E_NULL ) )
            int alt20=10;
            switch ( input.LA(1) ) {
            case RULE_NUMBER:
                {
                alt20=1;
                }
                break;
            case RULE_E_STRING:
                {
                alt20=2;
                }
                break;
            case RULE_E_CHAR:
            case 42:
                {
                alt20=3;
                }
                break;
            case RULE_E_BOOLEAN:
                {
                alt20=4;
                }
                break;
            case RULE_E_FLOAT:
                {
                alt20=5;
                }
                break;
            case RULE_E_LONG:
                {
                alt20=6;
                }
                break;
            case RULE_E_DOUBLE:
                {
                alt20=7;
                }
                break;
            case RULE_E_BYTE:
                {
                alt20=8;
                }
                break;
            case 40:
                {
                alt20=9;
                }
                break;
            case RULE_E_NULL:
                {
                alt20=10;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 20, 0, input);

                throw nvae;
            }

            switch (alt20) {
                case 1 :
                    // InternalExpressionDsl.g:1184:3: ( () ( (lv_value_1_0= ruleE_INT ) ) )
                    {
                    // InternalExpressionDsl.g:1184:3: ( () ( (lv_value_1_0= ruleE_INT ) ) )
                    // InternalExpressionDsl.g:1185:4: () ( (lv_value_1_0= ruleE_INT ) )
                    {
                    // InternalExpressionDsl.g:1185:4: ()
                    // InternalExpressionDsl.g:1186:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getAtomicAccess().getLiteralIntegerAction_0_0(),
                    						current);
                    				

                    }

                    // InternalExpressionDsl.g:1192:4: ( (lv_value_1_0= ruleE_INT ) )
                    // InternalExpressionDsl.g:1193:5: (lv_value_1_0= ruleE_INT )
                    {
                    // InternalExpressionDsl.g:1193:5: (lv_value_1_0= ruleE_INT )
                    // InternalExpressionDsl.g:1194:6: lv_value_1_0= ruleE_INT
                    {

                    						newCompositeNode(grammarAccess.getAtomicAccess().getValueE_INTParserRuleCall_0_1_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_value_1_0=ruleE_INT();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAtomicRule());
                    						}
                    						set(
                    							current,
                    							"value",
                    							lv_value_1_0,
                    							"ca.mcgill.sel.ram.expressions.ExpressionDsl.E_INT");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalExpressionDsl.g:1213:3: ( () ( (lv_value_3_0= RULE_E_STRING ) ) )
                    {
                    // InternalExpressionDsl.g:1213:3: ( () ( (lv_value_3_0= RULE_E_STRING ) ) )
                    // InternalExpressionDsl.g:1214:4: () ( (lv_value_3_0= RULE_E_STRING ) )
                    {
                    // InternalExpressionDsl.g:1214:4: ()
                    // InternalExpressionDsl.g:1215:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getAtomicAccess().getLiteralStringAction_1_0(),
                    						current);
                    				

                    }

                    // InternalExpressionDsl.g:1221:4: ( (lv_value_3_0= RULE_E_STRING ) )
                    // InternalExpressionDsl.g:1222:5: (lv_value_3_0= RULE_E_STRING )
                    {
                    // InternalExpressionDsl.g:1222:5: (lv_value_3_0= RULE_E_STRING )
                    // InternalExpressionDsl.g:1223:6: lv_value_3_0= RULE_E_STRING
                    {
                    lv_value_3_0=(Token)match(input,RULE_E_STRING,FOLLOW_2); 

                    						newLeafNode(lv_value_3_0, grammarAccess.getAtomicAccess().getValueE_STRINGTerminalRuleCall_1_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAtomicRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_3_0,
                    							"ca.mcgill.sel.ram.expressions.ExpressionDsl.E_STRING");
                    					

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalExpressionDsl.g:1241:3: ( () ( (lv_body_5_0= ruleFeature ) ) )
                    {
                    // InternalExpressionDsl.g:1241:3: ( () ( (lv_body_5_0= ruleFeature ) ) )
                    // InternalExpressionDsl.g:1242:4: () ( (lv_body_5_0= ruleFeature ) )
                    {
                    // InternalExpressionDsl.g:1242:4: ()
                    // InternalExpressionDsl.g:1243:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getAtomicAccess().getOpaqueExpressionAction_2_0(),
                    						current);
                    				

                    }

                    // InternalExpressionDsl.g:1249:4: ( (lv_body_5_0= ruleFeature ) )
                    // InternalExpressionDsl.g:1250:5: (lv_body_5_0= ruleFeature )
                    {
                    // InternalExpressionDsl.g:1250:5: (lv_body_5_0= ruleFeature )
                    // InternalExpressionDsl.g:1251:6: lv_body_5_0= ruleFeature
                    {

                    						newCompositeNode(grammarAccess.getAtomicAccess().getBodyFeatureParserRuleCall_2_1_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_body_5_0=ruleFeature();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAtomicRule());
                    						}
                    						set(
                    							current,
                    							"body",
                    							lv_body_5_0,
                    							"ca.mcgill.sel.ram.expressions.ExpressionDsl.Feature");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;
                case 4 :
                    // InternalExpressionDsl.g:1270:3: ( () ( (lv_value_7_0= RULE_E_BOOLEAN ) ) )
                    {
                    // InternalExpressionDsl.g:1270:3: ( () ( (lv_value_7_0= RULE_E_BOOLEAN ) ) )
                    // InternalExpressionDsl.g:1271:4: () ( (lv_value_7_0= RULE_E_BOOLEAN ) )
                    {
                    // InternalExpressionDsl.g:1271:4: ()
                    // InternalExpressionDsl.g:1272:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getAtomicAccess().getLiteralBooleanAction_3_0(),
                    						current);
                    				

                    }

                    // InternalExpressionDsl.g:1278:4: ( (lv_value_7_0= RULE_E_BOOLEAN ) )
                    // InternalExpressionDsl.g:1279:5: (lv_value_7_0= RULE_E_BOOLEAN )
                    {
                    // InternalExpressionDsl.g:1279:5: (lv_value_7_0= RULE_E_BOOLEAN )
                    // InternalExpressionDsl.g:1280:6: lv_value_7_0= RULE_E_BOOLEAN
                    {
                    lv_value_7_0=(Token)match(input,RULE_E_BOOLEAN,FOLLOW_2); 

                    						newLeafNode(lv_value_7_0, grammarAccess.getAtomicAccess().getValueE_BOOLEANTerminalRuleCall_3_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAtomicRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_7_0,
                    							"ca.mcgill.sel.ram.expressions.ExpressionDsl.E_BOOLEAN");
                    					

                    }


                    }


                    }


                    }
                    break;
                case 5 :
                    // InternalExpressionDsl.g:1298:3: ( () ( (lv_value_9_0= RULE_E_FLOAT ) ) )
                    {
                    // InternalExpressionDsl.g:1298:3: ( () ( (lv_value_9_0= RULE_E_FLOAT ) ) )
                    // InternalExpressionDsl.g:1299:4: () ( (lv_value_9_0= RULE_E_FLOAT ) )
                    {
                    // InternalExpressionDsl.g:1299:4: ()
                    // InternalExpressionDsl.g:1300:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getAtomicAccess().getLiteralFloatAction_4_0(),
                    						current);
                    				

                    }

                    // InternalExpressionDsl.g:1306:4: ( (lv_value_9_0= RULE_E_FLOAT ) )
                    // InternalExpressionDsl.g:1307:5: (lv_value_9_0= RULE_E_FLOAT )
                    {
                    // InternalExpressionDsl.g:1307:5: (lv_value_9_0= RULE_E_FLOAT )
                    // InternalExpressionDsl.g:1308:6: lv_value_9_0= RULE_E_FLOAT
                    {
                    lv_value_9_0=(Token)match(input,RULE_E_FLOAT,FOLLOW_2); 

                    						newLeafNode(lv_value_9_0, grammarAccess.getAtomicAccess().getValueE_FLOATTerminalRuleCall_4_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAtomicRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_9_0,
                    							"ca.mcgill.sel.ram.expressions.ExpressionDsl.E_FLOAT");
                    					

                    }


                    }


                    }


                    }
                    break;
                case 6 :
                    // InternalExpressionDsl.g:1326:3: ( () ( (lv_value_11_0= RULE_E_LONG ) ) )
                    {
                    // InternalExpressionDsl.g:1326:3: ( () ( (lv_value_11_0= RULE_E_LONG ) ) )
                    // InternalExpressionDsl.g:1327:4: () ( (lv_value_11_0= RULE_E_LONG ) )
                    {
                    // InternalExpressionDsl.g:1327:4: ()
                    // InternalExpressionDsl.g:1328:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getAtomicAccess().getLiteralLongAction_5_0(),
                    						current);
                    				

                    }

                    // InternalExpressionDsl.g:1334:4: ( (lv_value_11_0= RULE_E_LONG ) )
                    // InternalExpressionDsl.g:1335:5: (lv_value_11_0= RULE_E_LONG )
                    {
                    // InternalExpressionDsl.g:1335:5: (lv_value_11_0= RULE_E_LONG )
                    // InternalExpressionDsl.g:1336:6: lv_value_11_0= RULE_E_LONG
                    {
                    lv_value_11_0=(Token)match(input,RULE_E_LONG,FOLLOW_2); 

                    						newLeafNode(lv_value_11_0, grammarAccess.getAtomicAccess().getValueE_LONGTerminalRuleCall_5_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAtomicRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_11_0,
                    							"ca.mcgill.sel.ram.expressions.ExpressionDsl.E_LONG");
                    					

                    }


                    }


                    }


                    }
                    break;
                case 7 :
                    // InternalExpressionDsl.g:1354:3: ( () ( (lv_value_13_0= RULE_E_DOUBLE ) ) )
                    {
                    // InternalExpressionDsl.g:1354:3: ( () ( (lv_value_13_0= RULE_E_DOUBLE ) ) )
                    // InternalExpressionDsl.g:1355:4: () ( (lv_value_13_0= RULE_E_DOUBLE ) )
                    {
                    // InternalExpressionDsl.g:1355:4: ()
                    // InternalExpressionDsl.g:1356:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getAtomicAccess().getLiteralDoubleAction_6_0(),
                    						current);
                    				

                    }

                    // InternalExpressionDsl.g:1362:4: ( (lv_value_13_0= RULE_E_DOUBLE ) )
                    // InternalExpressionDsl.g:1363:5: (lv_value_13_0= RULE_E_DOUBLE )
                    {
                    // InternalExpressionDsl.g:1363:5: (lv_value_13_0= RULE_E_DOUBLE )
                    // InternalExpressionDsl.g:1364:6: lv_value_13_0= RULE_E_DOUBLE
                    {
                    lv_value_13_0=(Token)match(input,RULE_E_DOUBLE,FOLLOW_2); 

                    						newLeafNode(lv_value_13_0, grammarAccess.getAtomicAccess().getValueE_DOUBLETerminalRuleCall_6_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAtomicRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_13_0,
                    							"ca.mcgill.sel.ram.expressions.ExpressionDsl.E_DOUBLE");
                    					

                    }


                    }


                    }


                    }
                    break;
                case 8 :
                    // InternalExpressionDsl.g:1382:3: ( () ( (lv_value_15_0= RULE_E_BYTE ) ) )
                    {
                    // InternalExpressionDsl.g:1382:3: ( () ( (lv_value_15_0= RULE_E_BYTE ) ) )
                    // InternalExpressionDsl.g:1383:4: () ( (lv_value_15_0= RULE_E_BYTE ) )
                    {
                    // InternalExpressionDsl.g:1383:4: ()
                    // InternalExpressionDsl.g:1384:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getAtomicAccess().getLiteralByteAction_7_0(),
                    						current);
                    				

                    }

                    // InternalExpressionDsl.g:1390:4: ( (lv_value_15_0= RULE_E_BYTE ) )
                    // InternalExpressionDsl.g:1391:5: (lv_value_15_0= RULE_E_BYTE )
                    {
                    // InternalExpressionDsl.g:1391:5: (lv_value_15_0= RULE_E_BYTE )
                    // InternalExpressionDsl.g:1392:6: lv_value_15_0= RULE_E_BYTE
                    {
                    lv_value_15_0=(Token)match(input,RULE_E_BYTE,FOLLOW_2); 

                    						newLeafNode(lv_value_15_0, grammarAccess.getAtomicAccess().getValueE_BYTETerminalRuleCall_7_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAtomicRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_15_0,
                    							"ca.mcgill.sel.ram.expressions.ExpressionDsl.E_BYTE");
                    					

                    }


                    }


                    }


                    }
                    break;
                case 9 :
                    // InternalExpressionDsl.g:1410:3: ( () otherlv_17= '\\'' ( (lv_value_18_0= RULE_E_CHAR ) ) otherlv_19= '\\'' )
                    {
                    // InternalExpressionDsl.g:1410:3: ( () otherlv_17= '\\'' ( (lv_value_18_0= RULE_E_CHAR ) ) otherlv_19= '\\'' )
                    // InternalExpressionDsl.g:1411:4: () otherlv_17= '\\'' ( (lv_value_18_0= RULE_E_CHAR ) ) otherlv_19= '\\''
                    {
                    // InternalExpressionDsl.g:1411:4: ()
                    // InternalExpressionDsl.g:1412:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getAtomicAccess().getLiteralCharAction_8_0(),
                    						current);
                    				

                    }

                    otherlv_17=(Token)match(input,40,FOLLOW_19); 

                    				newLeafNode(otherlv_17, grammarAccess.getAtomicAccess().getApostropheKeyword_8_1());
                    			
                    // InternalExpressionDsl.g:1422:4: ( (lv_value_18_0= RULE_E_CHAR ) )
                    // InternalExpressionDsl.g:1423:5: (lv_value_18_0= RULE_E_CHAR )
                    {
                    // InternalExpressionDsl.g:1423:5: (lv_value_18_0= RULE_E_CHAR )
                    // InternalExpressionDsl.g:1424:6: lv_value_18_0= RULE_E_CHAR
                    {
                    lv_value_18_0=(Token)match(input,RULE_E_CHAR,FOLLOW_20); 

                    						newLeafNode(lv_value_18_0, grammarAccess.getAtomicAccess().getValueE_CHARTerminalRuleCall_8_2_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAtomicRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_18_0,
                    							"ca.mcgill.sel.ram.expressions.ExpressionDsl.E_CHAR");
                    					

                    }


                    }

                    otherlv_19=(Token)match(input,40,FOLLOW_2); 

                    				newLeafNode(otherlv_19, grammarAccess.getAtomicAccess().getApostropheKeyword_8_3());
                    			

                    }


                    }
                    break;
                case 10 :
                    // InternalExpressionDsl.g:1446:3: ( () this_E_NULL_21= RULE_E_NULL )
                    {
                    // InternalExpressionDsl.g:1446:3: ( () this_E_NULL_21= RULE_E_NULL )
                    // InternalExpressionDsl.g:1447:4: () this_E_NULL_21= RULE_E_NULL
                    {
                    // InternalExpressionDsl.g:1447:4: ()
                    // InternalExpressionDsl.g:1448:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getAtomicAccess().getLiteralNullAction_9_0(),
                    						current);
                    				

                    }

                    this_E_NULL_21=(Token)match(input,RULE_E_NULL,FOLLOW_2); 

                    				newLeafNode(this_E_NULL_21, grammarAccess.getAtomicAccess().getE_NULLTerminalRuleCall_9_1());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAtomic"


    // $ANTLR start "entryRuleFeature"
    // InternalExpressionDsl.g:1463:1: entryRuleFeature returns [String current=null] : iv_ruleFeature= ruleFeature EOF ;
    public final String entryRuleFeature() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleFeature = null;


        try {
            // InternalExpressionDsl.g:1463:47: (iv_ruleFeature= ruleFeature EOF )
            // InternalExpressionDsl.g:1464:2: iv_ruleFeature= ruleFeature EOF
            {
             newCompositeNode(grammarAccess.getFeatureRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFeature=ruleFeature();

            state._fsp--;

             current =iv_ruleFeature.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFeature"


    // $ANTLR start "ruleFeature"
    // InternalExpressionDsl.g:1470:1: ruleFeature returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_IDENTIFIER_0= ruleIDENTIFIER | (this_IDENTIFIER_1= ruleIDENTIFIER kw= '.' this_IDENTIFIER_3= ruleIDENTIFIER ) ) ;
    public final AntlrDatatypeRuleToken ruleFeature() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        AntlrDatatypeRuleToken this_IDENTIFIER_0 = null;

        AntlrDatatypeRuleToken this_IDENTIFIER_1 = null;

        AntlrDatatypeRuleToken this_IDENTIFIER_3 = null;



        	enterRule();

        try {
            // InternalExpressionDsl.g:1476:2: ( (this_IDENTIFIER_0= ruleIDENTIFIER | (this_IDENTIFIER_1= ruleIDENTIFIER kw= '.' this_IDENTIFIER_3= ruleIDENTIFIER ) ) )
            // InternalExpressionDsl.g:1477:2: (this_IDENTIFIER_0= ruleIDENTIFIER | (this_IDENTIFIER_1= ruleIDENTIFIER kw= '.' this_IDENTIFIER_3= ruleIDENTIFIER ) )
            {
            // InternalExpressionDsl.g:1477:2: (this_IDENTIFIER_0= ruleIDENTIFIER | (this_IDENTIFIER_1= ruleIDENTIFIER kw= '.' this_IDENTIFIER_3= ruleIDENTIFIER ) )
            int alt21=2;
            alt21 = dfa21.predict(input);
            switch (alt21) {
                case 1 :
                    // InternalExpressionDsl.g:1478:3: this_IDENTIFIER_0= ruleIDENTIFIER
                    {

                    			newCompositeNode(grammarAccess.getFeatureAccess().getIDENTIFIERParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_IDENTIFIER_0=ruleIDENTIFIER();

                    state._fsp--;


                    			current.merge(this_IDENTIFIER_0);
                    		

                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalExpressionDsl.g:1489:3: (this_IDENTIFIER_1= ruleIDENTIFIER kw= '.' this_IDENTIFIER_3= ruleIDENTIFIER )
                    {
                    // InternalExpressionDsl.g:1489:3: (this_IDENTIFIER_1= ruleIDENTIFIER kw= '.' this_IDENTIFIER_3= ruleIDENTIFIER )
                    // InternalExpressionDsl.g:1490:4: this_IDENTIFIER_1= ruleIDENTIFIER kw= '.' this_IDENTIFIER_3= ruleIDENTIFIER
                    {

                    				newCompositeNode(grammarAccess.getFeatureAccess().getIDENTIFIERParserRuleCall_1_0());
                    			
                    pushFollow(FOLLOW_21);
                    this_IDENTIFIER_1=ruleIDENTIFIER();

                    state._fsp--;


                    				current.merge(this_IDENTIFIER_1);
                    			

                    				afterParserOrEnumRuleCall();
                    			
                    kw=(Token)match(input,41,FOLLOW_22); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getFeatureAccess().getFullStopKeyword_1_1());
                    			

                    				newCompositeNode(grammarAccess.getFeatureAccess().getIDENTIFIERParserRuleCall_1_2());
                    			
                    pushFollow(FOLLOW_2);
                    this_IDENTIFIER_3=ruleIDENTIFIER();

                    state._fsp--;


                    				current.merge(this_IDENTIFIER_3);
                    			

                    				afterParserOrEnumRuleCall();
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFeature"


    // $ANTLR start "entryRuleIDENTIFIER"
    // InternalExpressionDsl.g:1520:1: entryRuleIDENTIFIER returns [String current=null] : iv_ruleIDENTIFIER= ruleIDENTIFIER EOF ;
    public final String entryRuleIDENTIFIER() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleIDENTIFIER = null;


        try {
            // InternalExpressionDsl.g:1520:50: (iv_ruleIDENTIFIER= ruleIDENTIFIER EOF )
            // InternalExpressionDsl.g:1521:2: iv_ruleIDENTIFIER= ruleIDENTIFIER EOF
            {
             newCompositeNode(grammarAccess.getIDENTIFIERRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleIDENTIFIER=ruleIDENTIFIER();

            state._fsp--;

             current =iv_ruleIDENTIFIER.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIDENTIFIER"


    // $ANTLR start "ruleIDENTIFIER"
    // InternalExpressionDsl.g:1527:1: ruleIDENTIFIER returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (this_E_CHAR_0= RULE_E_CHAR | kw= '_' ) (this_E_CHAR_2= RULE_E_CHAR | kw= '_' | this_NUMBER_4= RULE_NUMBER )* ) ;
    public final AntlrDatatypeRuleToken ruleIDENTIFIER() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_E_CHAR_0=null;
        Token kw=null;
        Token this_E_CHAR_2=null;
        Token this_NUMBER_4=null;


        	enterRule();

        try {
            // InternalExpressionDsl.g:1533:2: ( ( (this_E_CHAR_0= RULE_E_CHAR | kw= '_' ) (this_E_CHAR_2= RULE_E_CHAR | kw= '_' | this_NUMBER_4= RULE_NUMBER )* ) )
            // InternalExpressionDsl.g:1534:2: ( (this_E_CHAR_0= RULE_E_CHAR | kw= '_' ) (this_E_CHAR_2= RULE_E_CHAR | kw= '_' | this_NUMBER_4= RULE_NUMBER )* )
            {
            // InternalExpressionDsl.g:1534:2: ( (this_E_CHAR_0= RULE_E_CHAR | kw= '_' ) (this_E_CHAR_2= RULE_E_CHAR | kw= '_' | this_NUMBER_4= RULE_NUMBER )* )
            // InternalExpressionDsl.g:1535:3: (this_E_CHAR_0= RULE_E_CHAR | kw= '_' ) (this_E_CHAR_2= RULE_E_CHAR | kw= '_' | this_NUMBER_4= RULE_NUMBER )*
            {
            // InternalExpressionDsl.g:1535:3: (this_E_CHAR_0= RULE_E_CHAR | kw= '_' )
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==RULE_E_CHAR) ) {
                alt22=1;
            }
            else if ( (LA22_0==42) ) {
                alt22=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 22, 0, input);

                throw nvae;
            }
            switch (alt22) {
                case 1 :
                    // InternalExpressionDsl.g:1536:4: this_E_CHAR_0= RULE_E_CHAR
                    {
                    this_E_CHAR_0=(Token)match(input,RULE_E_CHAR,FOLLOW_23); 

                    				current.merge(this_E_CHAR_0);
                    			

                    				newLeafNode(this_E_CHAR_0, grammarAccess.getIDENTIFIERAccess().getE_CHARTerminalRuleCall_0_0());
                    			

                    }
                    break;
                case 2 :
                    // InternalExpressionDsl.g:1544:4: kw= '_'
                    {
                    kw=(Token)match(input,42,FOLLOW_23); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getIDENTIFIERAccess().get_Keyword_0_1());
                    			

                    }
                    break;

            }

            // InternalExpressionDsl.g:1550:3: (this_E_CHAR_2= RULE_E_CHAR | kw= '_' | this_NUMBER_4= RULE_NUMBER )*
            loop23:
            do {
                int alt23=4;
                switch ( input.LA(1) ) {
                case RULE_E_CHAR:
                    {
                    alt23=1;
                    }
                    break;
                case 42:
                    {
                    alt23=2;
                    }
                    break;
                case RULE_NUMBER:
                    {
                    alt23=3;
                    }
                    break;

                }

                switch (alt23) {
            	case 1 :
            	    // InternalExpressionDsl.g:1551:4: this_E_CHAR_2= RULE_E_CHAR
            	    {
            	    this_E_CHAR_2=(Token)match(input,RULE_E_CHAR,FOLLOW_23); 

            	    				current.merge(this_E_CHAR_2);
            	    			

            	    				newLeafNode(this_E_CHAR_2, grammarAccess.getIDENTIFIERAccess().getE_CHARTerminalRuleCall_1_0());
            	    			

            	    }
            	    break;
            	case 2 :
            	    // InternalExpressionDsl.g:1559:4: kw= '_'
            	    {
            	    kw=(Token)match(input,42,FOLLOW_23); 

            	    				current.merge(kw);
            	    				newLeafNode(kw, grammarAccess.getIDENTIFIERAccess().get_Keyword_1_1());
            	    			

            	    }
            	    break;
            	case 3 :
            	    // InternalExpressionDsl.g:1565:4: this_NUMBER_4= RULE_NUMBER
            	    {
            	    this_NUMBER_4=(Token)match(input,RULE_NUMBER,FOLLOW_23); 

            	    				current.merge(this_NUMBER_4);
            	    			

            	    				newLeafNode(this_NUMBER_4, grammarAccess.getIDENTIFIERAccess().getNUMBERTerminalRuleCall_1_2());
            	    			

            	    }
            	    break;

            	default :
            	    break loop23;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIDENTIFIER"


    // $ANTLR start "entryRuleE_INT"
    // InternalExpressionDsl.g:1577:1: entryRuleE_INT returns [String current=null] : iv_ruleE_INT= ruleE_INT EOF ;
    public final String entryRuleE_INT() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleE_INT = null;


        try {
            // InternalExpressionDsl.g:1577:45: (iv_ruleE_INT= ruleE_INT EOF )
            // InternalExpressionDsl.g:1578:2: iv_ruleE_INT= ruleE_INT EOF
            {
             newCompositeNode(grammarAccess.getE_INTRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleE_INT=ruleE_INT();

            state._fsp--;

             current =iv_ruleE_INT.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleE_INT"


    // $ANTLR start "ruleE_INT"
    // InternalExpressionDsl.g:1584:1: ruleE_INT returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : this_NUMBER_0= RULE_NUMBER ;
    public final AntlrDatatypeRuleToken ruleE_INT() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_NUMBER_0=null;


        	enterRule();

        try {
            // InternalExpressionDsl.g:1590:2: (this_NUMBER_0= RULE_NUMBER )
            // InternalExpressionDsl.g:1591:2: this_NUMBER_0= RULE_NUMBER
            {
            this_NUMBER_0=(Token)match(input,RULE_NUMBER,FOLLOW_2); 

            		current.merge(this_NUMBER_0);
            	

            		newLeafNode(this_NUMBER_0, grammarAccess.getE_INTAccess().getNUMBERTerminalRuleCall());
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleE_INT"

    // Delegated rules


    protected DFA19 dfa19 = new DFA19(this);
    protected DFA21 dfa21 = new DFA21(this);
    static final String dfa_1s = "\35\uffff";
    static final String dfa_2s = "\5\uffff\11\20\1\uffff\1\20\2\uffff\3\20\2\uffff\6\20";
    static final String dfa_3s = "\1\4\4\uffff\2\16\2\12\5\16\1\12\1\16\2\uffff\4\12\1\50\2\12\1\16\3\12";
    static final String dfa_4s = "\1\52\4\uffff\2\47\2\52\5\47\1\12\1\47\2\uffff\4\52\1\50\2\52\1\47\3\52";
    static final String dfa_5s = "\1\uffff\1\1\1\2\1\3\1\4\13\uffff\1\6\1\5\13\uffff";
    static final String dfa_6s = "\35\uffff}>";
    static final String[] dfa_7s = {
            "\1\6\1\11\1\12\1\13\1\14\1\15\1\7\1\17\1\5\22\uffff\1\2\3\uffff\1\1\1\3\1\uffff\2\4\1\16\1\uffff\1\10",
            "",
            "",
            "",
            "",
            "\25\20\2\uffff\1\20\2\21",
            "\25\20\2\uffff\1\20\2\21",
            "\1\22\1\uffff\1\24\1\uffff\25\20\2\uffff\1\20\2\21\1\uffff\1\25\1\23",
            "\1\22\1\uffff\1\24\1\uffff\25\20\2\uffff\1\20\2\21\1\uffff\1\25\1\23",
            "\25\20\2\uffff\1\20\2\21",
            "\25\20\2\uffff\1\20\2\21",
            "\25\20\2\uffff\1\20\2\21",
            "\25\20\2\uffff\1\20\2\21",
            "\25\20\2\uffff\1\20\2\21",
            "\1\26",
            "\25\20\2\uffff\1\20\2\21",
            "",
            "",
            "\1\22\1\uffff\1\24\1\uffff\25\20\2\uffff\1\20\2\21\1\uffff\1\25\1\23",
            "\1\22\1\uffff\1\24\1\uffff\25\20\2\uffff\1\20\2\21\1\uffff\1\25\1\23",
            "\1\22\1\uffff\1\24\1\uffff\25\20\2\uffff\1\20\2\21\1\uffff\1\25\1\23",
            "\1\27\37\uffff\1\30",
            "\1\31",
            "\1\32\1\uffff\1\34\1\uffff\25\20\2\uffff\1\20\2\21\2\uffff\1\33",
            "\1\32\1\uffff\1\34\1\uffff\25\20\2\uffff\1\20\2\21\2\uffff\1\33",
            "\25\20\2\uffff\1\20\2\21",
            "\1\32\1\uffff\1\34\1\uffff\25\20\2\uffff\1\20\2\21\2\uffff\1\33",
            "\1\32\1\uffff\1\34\1\uffff\25\20\2\uffff\1\20\2\21\2\uffff\1\33",
            "\1\32\1\uffff\1\34\1\uffff\25\20\2\uffff\1\20\2\21\2\uffff\1\33"
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final short[] dfa_2 = DFA.unpackEncodedString(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final char[] dfa_4 = DFA.unpackEncodedStringToUnsignedChars(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[] dfa_6 = DFA.unpackEncodedString(dfa_6s);
    static final short[][] dfa_7 = unpackEncodedStringArray(dfa_7s);

    class DFA19 extends DFA {

        public DFA19(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 19;
            this.eot = dfa_1;
            this.eof = dfa_2;
            this.min = dfa_3;
            this.max = dfa_4;
            this.accept = dfa_5;
            this.special = dfa_6;
            this.transition = dfa_7;
        }
        public String getDescription() {
            return "959:2: ( ( () otherlv_1= '!' ( (lv_expression_2_0= rulePrimary ) ) ) | ( () otherlv_4= '-' ( (lv_expression_5_0= rulePrimary ) ) ) | (otherlv_6= '(' this_ValueSpecification_7= ruleValueSpecification otherlv_8= ')' ) | ( () ( ( (lv_op_10_1= '++' | lv_op_10_2= '--' ) ) ) ( (lv_expression_11_0= ruleAtomic ) ) ) | ( () ( (lv_expression_13_0= ruleAtomic ) ) ( ( (lv_op_14_1= '++' | lv_op_14_2= '--' ) ) ) ) | this_Atomic_15= ruleAtomic )";
        }
    }
    static final String dfa_8s = "\10\uffff";
    static final String dfa_9s = "\1\uffff\5\7\2\uffff";
    static final String dfa_10s = "\6\12\2\uffff";
    static final String dfa_11s = "\6\52\2\uffff";
    static final String dfa_12s = "\6\uffff\1\2\1\1";
    static final String dfa_13s = "\10\uffff}>";
    static final String[] dfa_14s = {
            "\1\1\37\uffff\1\2",
            "\1\3\1\uffff\1\5\1\uffff\25\7\2\uffff\3\7\1\uffff\1\6\1\4",
            "\1\3\1\uffff\1\5\1\uffff\25\7\2\uffff\3\7\1\uffff\1\6\1\4",
            "\1\3\1\uffff\1\5\1\uffff\25\7\2\uffff\3\7\1\uffff\1\6\1\4",
            "\1\3\1\uffff\1\5\1\uffff\25\7\2\uffff\3\7\1\uffff\1\6\1\4",
            "\1\3\1\uffff\1\5\1\uffff\25\7\2\uffff\3\7\1\uffff\1\6\1\4",
            "",
            ""
    };

    static final short[] dfa_8 = DFA.unpackEncodedString(dfa_8s);
    static final short[] dfa_9 = DFA.unpackEncodedString(dfa_9s);
    static final char[] dfa_10 = DFA.unpackEncodedStringToUnsignedChars(dfa_10s);
    static final char[] dfa_11 = DFA.unpackEncodedStringToUnsignedChars(dfa_11s);
    static final short[] dfa_12 = DFA.unpackEncodedString(dfa_12s);
    static final short[] dfa_13 = DFA.unpackEncodedString(dfa_13s);
    static final short[][] dfa_14 = unpackEncodedStringArray(dfa_14s);

    class DFA21 extends DFA {

        public DFA21(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 21;
            this.eot = dfa_8;
            this.eof = dfa_9;
            this.min = dfa_10;
            this.max = dfa_11;
            this.accept = dfa_12;
            this.special = dfa_13;
            this.transition = dfa_14;
        }
        public String getDescription() {
            return "1477:2: (this_IDENTIFIER_0= ruleIDENTIFIER | (this_IDENTIFIER_1= ruleIDENTIFIER kw= '.' this_IDENTIFIER_3= ruleIDENTIFIER ) )";
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000004002L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x000005D880001FF0L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000010002L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000020002L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000040002L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000080002L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000100002L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000600002L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000007800002L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000038000002L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x00000000C0000002L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000700000002L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000050000001FF0L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x000000C000000000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000000400L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000010000000000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000040000000400L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000040000001402L});

}