package ca.mcgill.sel.ram.expressions.conversion;

import org.eclipse.xtext.common.services.DefaultTerminalConverters;
import org.eclipse.xtext.conversion.ValueConverter;

import com.google.inject.Inject;

/**
 * Customized ValueConverter for some Literals.
 * 
 * @author Rohit Verma
 */
public class CustomValueConverter extends DefaultTerminalConverters {
    @Inject
    private LONGValueConverter longValueConverter;

    @Inject
    private BYTEValueConverter byteValueConverter;

    /**
     * Customized ValueConverter for rule "LONG" in the Xtext grammar.
     * @return instance of LONGValueConverter
     */
    @ValueConverter(rule = "E_LONG")
    // CHECKSTYLE:IGNORE MethodName: Following Xtext naming convention.
    public LONGValueConverter E_LONG() {
        return longValueConverter;
    }

    /**
     * Customized ValueConverter for rule "BYTE" in the Xtext grammar.
     * @return instance of BYTEValueConverter
     */
    @ValueConverter(rule = "E_BYTE")
    // CHECKSTYLE:IGNORE MethodName: Following Xtext naming convention.
    public BYTEValueConverter E_BYTE() {
        return byteValueConverter;
    }

}
