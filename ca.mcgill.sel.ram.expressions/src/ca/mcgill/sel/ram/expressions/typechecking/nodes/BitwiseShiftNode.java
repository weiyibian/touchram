package ca.mcgill.sel.ram.expressions.typechecking.nodes;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.ram.ValueSpecification;
import ca.mcgill.sel.ram.expressions.typechecking.TypeChecker;
import ca.mcgill.sel.ram.expressions.typechecking.TypeChecker.Types;
import ca.mcgill.sel.ram.expressions.typechecking.TypeCheckingUtil;

/**
 * This class is used for derivation of type for BitwiseShiftNode.
 * 
 * @author maksim
 *
 */
public final class BitwiseShiftNode {

    /**
     * Private constructor to prevent unintended instantiation.
     */
    private BitwiseShiftNode() {
    }

    /**
     * Method that implements the typechecking logic for the node representing "BitwiseShift" Operator.
     * 
     * @param left : left of ComparisonNode
     * @param right : right of ComparisonNode
     * 
     * @return The type that ComparisonNode should return
     */
    public static Types typeCheck(ValueSpecification left, ValueSpecification right) {
        Types shouldReturnType = null;

        Types lType = TypeChecker.resolveType(left);
        Types rType = TypeChecker.resolveType(right);

        if (lType != null && rType != null) {
            if (TypeCheckingUtil.BITWISESHIFT_OPERANDS.contains(lType)
                    && Types.intType == rType) {
                shouldReturnType = Types.intType;
            }
        } else {
            if (TypeCheckingUtil.COMPARISON_OPERANDS.contains(lType)) {
                TypeChecker.addIssue(EMFEditUtil.getText(right)
                        + " is not a valid type for addition"
                        + EMFEditUtil.getText(left));
            } else {
                TypeChecker.addIssue(EMFEditUtil.getText(left)
                        + " is not a valid type for addition"
                        + EMFEditUtil.getText(right));
            }

        }

        return shouldReturnType;
    }
}