package ca.mcgill.sel.ram.expressions.typechecking.nodes;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.ram.ValueSpecification;
import ca.mcgill.sel.ram.expressions.typechecking.TypeChecker;
import ca.mcgill.sel.ram.expressions.typechecking.TypeChecker.Types;

/**
 * Used to infer type of the conditionalExpression.
 * 
 * @author maksim
 */
public final class ConditionalNode {

    /**
     * Private constructor to prevent unintended instantiation.
     */
    private ConditionalNode() {
    }

    /**
     * Checks type of the conditional expression.
     * 
     * @param conditional
     *            Boolean part in ternary operator.
     * @param expressionT
     *            expressionT to evaluate
     * @param expressionF
     *            expressionF to evaluate
     * @return
     *         return type of the ternary operator
     */
    public static Types typeCheck(ValueSpecification conditional, ValueSpecification expressionT,
            ValueSpecification expressionF) {

        Types shouldReturnType = null;
        Types conditionalType = TypeChecker.resolveType(conditional);
        Types expressionTType = TypeChecker.resolveType(expressionT);
        Types expressionFType = TypeChecker.resolveType(expressionF);

        if (conditionalType != null && expressionTType != null && expressionFType != null) {
            if (Types.booleanType.equals(conditionalType) && (expressionTType.equals(expressionFType))) {
                shouldReturnType = expressionTType;
            }
        } else {
            if (!Types.booleanType.equals(conditionalType)) {
                TypeChecker.addIssue(EMFEditUtil.getText(conditional) + " is not a boolean");
            } else if (!expressionTType.equals(expressionFType)) {
                TypeChecker.addIssue(EMFEditUtil.getText(expressionT) + " does not have same type as "
                        + EMFEditUtil.getText(expressionF));
            }
        }
        return shouldReturnType;
    }
}