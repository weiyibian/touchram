package ca.mcgill.sel.ram.expressions.typechecking.nodes;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.ram.ValueSpecification;
import ca.mcgill.sel.ram.expressions.typechecking.TypeChecker;
import ca.mcgill.sel.ram.expressions.typechecking.TypeCheckingUtil;
import ca.mcgill.sel.ram.expressions.typechecking.TypeChecker.Types;

/**
 * A class that contains the typechecking logic for a node representing "AND" Operator..
 * 
 * @author Rohit Verma
 */
public final class AndNode {

    /**
     * Private constructor to prevent unintended instantiation.
     */
    private AndNode() {
    }

    /**
     * Method that implements the typechecking logic for the node representing "AND" Operator.
     * 
     * @param left : left of AndNode
     * @param right : right of AndNode
     * 
     * @return The type that AndNode should return
     */
    public static Types typeCheck(ValueSpecification left, ValueSpecification right) {

        Types shouldReturnType = null;

        // Each operand of the conditional-and operator must be of type boolean.

        Types lType = TypeChecker.resolveType(left);
        Types rType = TypeChecker.resolveType(right);

        if (lType != null && rType != null) {
            if (Types.booleanType.equals(lType) && Types.booleanType.equals(rType)) {
                shouldReturnType = Types.booleanType;
            } else {
                if (TypeCheckingUtil.EQUALITY_OPERANDS.contains(lType)) {
                    TypeChecker.addIssue(EMFEditUtil.getText(right)
                            + " is not a valid type for ANDing"
                            + EMFEditUtil.getText(left));
                } else {
                    TypeChecker.addIssue(EMFEditUtil.getText(left)
                            + " is not a valid type for ANDing"
                            + EMFEditUtil.getText(right));
                }
            }
        }
        return shouldReturnType;
    }
}