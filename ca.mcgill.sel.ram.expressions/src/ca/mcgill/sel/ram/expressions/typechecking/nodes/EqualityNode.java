package ca.mcgill.sel.ram.expressions.typechecking.nodes;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.ram.ValueSpecification;
import ca.mcgill.sel.ram.expressions.typechecking.TypeChecker;
import ca.mcgill.sel.ram.expressions.typechecking.TypeCheckingUtil;
import ca.mcgill.sel.ram.expressions.typechecking.TypeChecker.Types;

/**
 * A class that contains the typechecking logic for a node representing "Equality" Operator..
 * 
 * @author Rohit Verma
 */
public final class EqualityNode {

    /**
     * Private constructor to prevent unintended instantiation.
     */
    private EqualityNode() {
    }

    /**
     * Method that implements the typechecking logic for the node representing "Equality" Operator.
     * 
     * @param left : left of EqualityNode
     * @param right : right of EqualityNode
     * 
     * @return The type that EqualityNode should return
     */
    public static Types typeCheck(ValueSpecification left, ValueSpecification right) {

        Types shouldReturnType = null;

        // The equality operators may be used to compare two operands that
        // are convertible to numeric type, or two operands of type boolean or Boolean, or two operands
        // that are each of either reference type or the null type. All other cases result in a compile-time error.

        Types lType = TypeChecker.resolveType(left);
        Types rType = TypeChecker.resolveType(right);

        if (lType != null && rType != null) {
            if (lType.equals(rType)
                    || (TypeCheckingUtil.EQUALITY_OPERANDS.contains(lType)
                            && TypeCheckingUtil.EQUALITY_OPERANDS.contains(rType))
                    || (lType.equals(Types.booleanType) && Types.booleanType.equals(rType))
                    || !(TypeCheckingUtil.EQUALITY_OPERANDS.contains(lType)) && (rType.equals(Types.nullType))
                    || !(TypeCheckingUtil.EQUALITY_OPERANDS.contains(rType)) && (lType.equals(Types.nullType))) {
                shouldReturnType = Types.booleanType;
            }
        } else {
            if (TypeCheckingUtil.EQUALITY_OPERANDS.contains(lType)) {
                TypeChecker.addIssue(EMFEditUtil.getText(right)
                        + " is not a valid type for reference equality check with "
                        + EMFEditUtil.getText(left));
            } else {
                TypeChecker.addIssue(EMFEditUtil.getText(left)
                        + " is not a valid type for reference equality check with "
                        + EMFEditUtil.getText(right));
            }

        }
        return shouldReturnType;
    }
}