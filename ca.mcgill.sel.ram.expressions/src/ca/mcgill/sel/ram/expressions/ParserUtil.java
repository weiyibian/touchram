package ca.mcgill.sel.ram.expressions;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.util.CancelIndicator;
import org.eclipse.xtext.validation.CheckMode;
import org.eclipse.xtext.validation.IResourceValidator;
import org.eclipse.xtext.validation.Issue;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.MessageView;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.Type;
import ca.mcgill.sel.ram.ValueSpecification;
import ca.mcgill.sel.ram.OpaqueExpression;
import ca.mcgill.sel.ram.expressions.typechecking.TypeChecker;
import ca.mcgill.sel.ram.expressions.typechecking.TypeChecker.Types;

/**
 * This class is used to access the Parser generated through Xtext.
 *
 * @author Rohit Verma
 */
public class ParserUtil {
    
    /**
     * Stores the two types used representing the same type.
     * 
     * @author maksim
     */
    public static class TypeContainer {
        /**
         * Used to distinguish fields when accessing a field.
         */
        private boolean isModelType;

        /**
         * Uses type definition from the model.
         */
        private Type ramType;

        /**
         * Allows user to specify types directly.
         */
        private Types typecheckerType;

        /**
         * TypeContainer is needed to handle two different types used by the ParserUtil.
         * 
         * @param ramType This is used to set type that has been defined in Ram Model.
         */
        public TypeContainer(Type ramType) {
            this.isModelType = true;
            this.ramType = ramType;
        }

        /**
         * TypeContainer is needed to handle two different types used by the ParserUtil.
         * 
         * @param typeCheckerType This is used to set type that has been defined for TypeChecker.
         */
        public TypeContainer(Types typeCheckerType) {
            this.isModelType = false;
            this.typecheckerType = typeCheckerType;
        }
    }

    /**
     * Error message if the identifier doesn't exist in the graphical model.
     */
    public static final String ERROR_MESSAGE = " is not a valid identifier";

    private List<String> issuesValList;
    private List<String> issuesList;

    /**
     * Method to do a Text-to-Model transformation (userString-to-model).
     * 
     * @param resource Persistent document that contains parsed model
     * @return ParserResult
     */
    public ParserResult parse(Resource resource) {

        // Root of the AST that the parser will return
        ValueSpecification nodevs = null;

        // Contains issues encountered while parsing contained in parserReturnedList
        issuesList = new ArrayList<String>();

        // The result (AST root + issues) returned by the parser and typechecker
        ParserResult result = new ParserResult();

        // Create resource and validate resource
        issuesList = validate(resource);

        if ((resource.getContents().size() > 0) && (issuesList.isEmpty())) {
            nodevs = (ValueSpecification) resource.getContents().get(0);
        }

        result.setModel(nodevs);
        result.setErrors(issuesList);
        return result;
    }

    /**
     * Method to replace nodes of type OpaqueExpression in textual model (AST)
     * with their corresponding types in graphical model.
     * 
     * @param nodeToTransform
     *            The root node (containing nodes of type OpaqueExpression) that is to be replaced.
     * 
     * @param resource
     *            resource
     * @param rootContainer
     *            rootContainer
     * @param currentTextView
     *            currentTextView
     * @param rootMessageView
     *            rootMessageView
     * @return
     *         The root node (containing nodes of (another) type that replaced OpaqueExpression nodes).
     */
    public ValueSpecification replaceNodesWithActualTypes(ValueSpecification nodeToTransform, Resource resource,
            Aspect rootContainer, EObject currentTextView, MessageView rootMessageView) {

        ValueSpecification transformedNode = null;

        if (resource != null) {
            if ((resource.getContents().size() > 0) && (issuesList.isEmpty())) {
                transformedNode = OpaqueExpressionUtil.replaceOpaqueExpressionsInCST(nodeToTransform, rootContainer,
                        currentTextView,
                        rootMessageView, this);
            }
        }
        return transformedNode;
    }

    /**
     * Method to do a Text-to-Model transformation (userString-to-model)
     * and do associated checks and balances required in the journey for an Expression.
     * 
     * @param userString
     *            String that will be parsed to model
     * @param currentTextView
     *            Object containing the string to be parsed
     * @param typeContainer
     *            Two different types are used in the system. This is a container to reduce code duplication.
     * @return
     *         Object of type ParserResult that contains parsed model and list of errors encountered during parsing
     */
    public ParserResult parseAndTypeCheckExpression(String userString, EObject currentTextView,
            TypeContainer typeContainer) {

        Aspect rootContainer = EMFModelUtil.getRootContainerOfType(currentTextView, RamPackage.Literals.ASPECT);
        MessageView rootMessageView =
                EMFModelUtil.getRootContainerOfType(currentTextView, RamPackage.Literals.MESSAGE_VIEW);

        ResourceSet resourceSet = rootContainer.eResource().getResourceSet();

        Resource resource = createModel(userString, resourceSet);

        // 1.Parse user string to Expression AST
        ParserResult result = parse(resource);

        ValueSpecification rootNodeExpression = result.getModel();
        issuesValList = result.getErrors();

        // Force the user to fix syntactic errors before moving further
        if (!issuesValList.isEmpty()) {
            return result;
        }

        // 2.Replace Opaque nodes in Expression AST with actual types
        rootNodeExpression = replaceNodesWithActualTypes(rootNodeExpression, resource, rootContainer, currentTextView,
                rootMessageView);
        if (!issuesValList.isEmpty()) {
            for (String error : result.getErrors()) {
                System.out.println(error);
            }
            return result;
        }

        // 3.TypeCheck Expression AST
        if (typeContainer.isModelType) {
            TypeChecker.typeCheckNode(rootNodeExpression, typeContainer.ramType);
        } else {
            TypeChecker.typeCheckNode(rootNodeExpression, typeContainer.typecheckerType);
        }
        result.setErrors(TypeChecker.getIssueList());

        if (!result.getErrors().isEmpty()) {
            for (String error : result.getErrors()) {
                System.out.println(error);
            }
        }
        return result;
    }
    
    public ValueSpecification checkOpaqueExpression(OpaqueExpression exp, EObject currentTextView) {
        ValueSpecification result = null;
        Aspect rootContainer = EMFModelUtil.getRootContainerOfType(currentTextView, RamPackage.Literals.ASPECT);
        MessageView rootMessageView =
                EMFModelUtil.getRootContainerOfType(currentTextView, RamPackage.Literals.MESSAGE_VIEW);
        ResourceSet resourceSet = rootContainer.eResource().getResourceSet();
        Resource resource = createModel(exp.getBody(), resourceSet);
        this.issuesValList = new ArrayList<String>();
        
        
        if (resource != null) {
            if (resource.getContents().size() > 0) {
                result = OpaqueExpressionUtil.checkOpaqueExpression(exp, rootContainer, currentTextView, 
                        rootMessageView, this);
            }
        }

        if (!issuesValList.isEmpty()) {
            for (String error : issuesValList) {
                System.out.println(error);
            }
            return null;
        }
        return result;
    }

    /**
     * Method to do a Text-to-Model transformation (userString-to-model).
     * 
     * @param userString
     *            String that will be parsed to model
     * @param resourceSet
     *            resourceSet
     * @return
     *         resource containing parsed model
     */
    private Resource createModel(String userString, ResourceSet resourceSet) {
        Resource resource = resourceSet.createResource(URI.createURI("synthetic:/inmemory.expression"));

        InputStream in = new ByteArrayInputStream(userString.getBytes());
        try {
            resource.load(in, resourceSet.getLoadOptions());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return resource;
    }

    /**
     * Method to validate the parsed model.
     * 
     * @param resourceLocal
     *            The resource that contains the model.
     * @return issuesValList
     *         The list of issues found during validation.
     */
    private List<String> validate(Resource resourceLocal) {
        issuesValList = new ArrayList<String>();
        // System.out.format("Validating: %s\n", resourceLocal.getURI());

        // Validation
        IResourceValidator validator = ((XtextResource) resourceLocal)
                .getResourceServiceProvider().getResourceValidator();
        List<Issue> issues = validator.validate(resourceLocal, CheckMode.ALL, CancelIndicator.NullImpl);
        for (Issue issue : issues) {
            addIssue(issue.getMessage());
        }
        return issuesValList;
    }

    /**
     * Method to add issue to issue list.
     * 
     * @param message
     *            Error description.
     */
    public void addIssue(String message) {
        issuesValList.add(message);
    }
}