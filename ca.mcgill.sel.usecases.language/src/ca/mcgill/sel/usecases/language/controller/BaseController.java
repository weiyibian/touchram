package ca.mcgill.sel.usecases.language.controller;

import java.util.Collections;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.command.CommandParameter;
import org.eclipse.emf.edit.command.CreateChildCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.controller.CoreBaseController;
import ca.mcgill.sel.usecases.Actor;
import ca.mcgill.sel.usecases.LayoutElement;
import ca.mcgill.sel.usecases.UseCaseModel;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.UseCase;
import ca.mcgill.sel.usecases.impl.ContainerMapImpl;
import ca.mcgill.sel.usecases.impl.ElementMapImpl;

/**
 * The controller with basic functionality that can be used by any sub-class.
 *
 * @author rlanguay
 */
public abstract class BaseController extends CoreBaseController {

    /**
     * Creates a new instance of {@link BaseController}.
     */
    protected BaseController() {
        // prevent anyone outside this package to instantiate
    }
    
    /**
     * Creates a new command which adds a new entry to the layout map for a given container.
     *
     * @param editingDomain the {@link EditingDomain}
     * @param container the container of all layouted elements
     * @param object the object that should be layouted
     * @param layoutElement the {@link LayoutElement} for the object
     * @return the command which adds a new entry to the layout map, null if no layout or container map exists
     */
    protected static Command createAddLayoutElementCommand(EditingDomain editingDomain, EObject container, 
            EObject object, LayoutElement layoutElement) {
        
        UseCaseModel ucd = (UseCaseModel) EcoreUtil.getRootContainer(container);

        if (ucd.getLayout() != null) {
            // Get the ContainerMap.
            ContainerMapImpl containerMap = EMFModelUtil.getEntryFromMap(ucd.getLayout().getContainers(), container);

            if (containerMap != null) {
                // Get the new child descriptor for the entry of the map.
                CommandParameter newChildDescriptor =
                        EMFEditUtil.getNewChildDescriptor(containerMap, UcPackage.Literals.CONTAINER_MAP__VALUE);

                // Set the key and value of the ElementMap.
                // We evade using commands here because it is not necessary.
                ElementMapImpl elementMap = (ElementMapImpl) newChildDescriptor.getValue();

                elementMap.setKey(object);
                elementMap.setValue(layoutElement);

                // The newChildDescriptor references the element map we modified, so we can just use
                // the CreateChildCommand.
                return CreateChildCommand.create(editingDomain, containerMap, newChildDescriptor,
                        Collections.EMPTY_LIST);
            }
        }

        return null;
    }
    
    /**
     * Creates a new command which moves the given object to the given position.
     *
     * @param editingDomain the {@link EditingDomain}
     * @param container the container the object belongs to
     * @param object the object that should be moved
     * @param x the new x position
     * @param y the new y position
     * @return the command which moves the object to the new position
     */
    protected Command createMoveCommand(EditingDomain editingDomain, EObject container,
            EObject object, float x, float y) {
        UseCaseModel ucd = (UseCaseModel) EcoreUtil.getRootContainer(container);
    
        EMap<EObject, LayoutElement> myMap = ucd.getLayout().getContainers().get(container);
        LayoutElement layoutElement = myMap.get(object);
    
        // Use CompoundCommand.
        CompoundCommand moveCompoundCommand = new CompoundCommand();
    
        // Create SetCommand for x.
        moveCompoundCommand.append(SetCommand.create(editingDomain, layoutElement,
               UcPackage.Literals.LAYOUT_ELEMENT__X, x));
        // Create SetCommand for y.
        moveCompoundCommand.append(SetCommand.create(editingDomain, layoutElement,
                UcPackage.Literals.LAYOUT_ELEMENT__Y, y));
    
        return moveCompoundCommand;
    }
    
    /**
     * Creates a new command which removes the entry of the layout map for a given container.
     *
     * @param editingDomain the {@link EditingDomain}
     * @param container the container of all layouted elements
     * @param object the object whose layout should be removed
     * @return the command which adds a new entry to the layout map
     */
    protected static Command createRemoveLayoutElementCommand(EditingDomain editingDomain, EObject container,
            EObject object) {
        UseCaseModel ucd = (UseCaseModel) EcoreUtil.getRootContainer(container);

        ContainerMapImpl containerMap = EMFModelUtil.getEntryFromMap(ucd.getLayout().getContainers(), container);
        ElementMapImpl elementMap = EMFModelUtil.getEntryFromMap(containerMap.getValue(), object);

        // Create command for removing ElementMap (includes the layout element).
        return RemoveCommand.create(editingDomain, elementMap);
    }
    
    /**
     * Creates a command to clear the primary actor on a use case.
     * @param editingDomain The {@link EditingDomain}
     * @param useCase The use case
     * @return The command which clears the primary actor
     */
    protected static Command createClearPrimaryActorCommand(EditingDomain editingDomain, UseCase useCase) {
        return SetCommand.create(
                editingDomain, useCase, UcPackage.Literals.USE_CASE__PRIMARY_ACTOR, SetCommand.UNSET_VALUE);
    }
    
    /**
     * Creates a new command which removes a secondary actor from a use case.
     * @param editingDomain The {@link EditingDomain}
     * @param useCase The use case
     * @param actorToRemove The actor to remove
     * @return The command to remove the secondary actor
     */
    protected static Command createRemoveSecondaryActorCommand(EditingDomain editingDomain,
            UseCase useCase, Actor actorToRemove) {
        return RemoveCommand.create(
                editingDomain, useCase, UcPackage.Literals.USE_CASE__SECONDARY_ACTORS, actorToRemove);
    }
}
