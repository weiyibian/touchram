package ca.mcgill.sel.usecases.language.controller;

/**
 * A factory to obtain controllers for use case diagrams
 * @author rlanguay
 *
 */
public class ControllerFactory {
    /**
     * The singleton instance of this factory.
     */
    public static final ControllerFactory INSTANCE = new ControllerFactory();
    
    private ActorController actorController;
    private UseCaseController useCaseController;
    private UseCaseModelController useCaseDiagramController;
        
    /**
     * Creates a new instance of {@link ControllerFactory}.
     */
    private ControllerFactory() {
        
    }
    
    /**
     * Returns the controller for {@link ca.mcgill.sel.usecases.UseCaseDiagram}s.
     * 
     * @return the controller for {@link ca.mcgill.sel.usecases.UseCaseDiagram}s
     */
    public UseCaseModelController getUseCaseDiagramController() {
        if (useCaseDiagramController == null) {
            useCaseDiagramController = new UseCaseModelController();
        }
        
        return useCaseDiagramController;
    }
    
    /**
     * Returns the controller for {@link ca.mcgill.sel.usecases.Actor}s.
     * 
     * @return the controller for {@link ca.mcgill.sel.usecases.Actor}s.
     */
    public ActorController getActorController() {
        if (actorController == null) {
            actorController = new ActorController();
        }
        
        return actorController;
    }
    
    /**
     * Returns the controller for {@link ca.mcgill.sel.usecases.UseCase}s.
     * 
     * @return the controller for {@link ca.mcgill.sel.usecases.UseCase}s.
     */
    public UseCaseController getUseCaseController() {
        if (useCaseController == null) {
            useCaseController = new UseCaseController();
        }
        
        return useCaseController;
    }
}
