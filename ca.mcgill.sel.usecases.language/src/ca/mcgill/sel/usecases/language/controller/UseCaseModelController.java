package ca.mcgill.sel.usecases.language.controller;

import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.usecases.*;

/**
 * Controller defining actions on use case diagrams.
 * @author rlanguay
 *
 */
public class UseCaseModelController extends BaseController {

    /**
     * Creates a new instance of {@link UseCaseModelController}.
     */
    protected UseCaseModelController() {
        // Prevent anyone outside this package to instantiate.
    }
    
    /**
     * Creates a new actor as well as its layout element.
     *
     * @param owner the {@link UseCaseDiagram} the actor should be added to
     * @param name the name of the actor
     * @param x the x position of the class
     * @param y the y position of the class 
     */
    public void createNewActor(UseCaseModel owner, String name, float x, float y) {
        Actor newActor = UcFactory.eINSTANCE.createActor();
        newActor.setName(name);

        LayoutElement layoutElement = UcFactory.eINSTANCE.createLayoutElement();
        layoutElement.setX(x);
        layoutElement.setY(y);

        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);

        // Create commands.
        Command addClassCommand = AddCommand.create(editingDomain, owner, UcPackage.Literals.USE_CASE_MODEL__ACTORS,
                newActor);
        Command createElementMapCommand =
                createAddLayoutElementCommand(editingDomain, owner, newActor, layoutElement);

        // Create compound command.
        CompoundCommand compoundCommand = new CompoundCommand();
        // If the layout element is added first, when the structural view gets notified (about a new class)
        // access to the layout element is already possible.
        compoundCommand.append(addClassCommand);
        compoundCommand.append(createElementMapCommand);

        doExecute(editingDomain, compoundCommand);
    }
    
    /**
     * Creates a new use case as well as its layout element.
     *
     * @param owner the {@link UseCaseDiagram} the actor should be added to
     * @param name the name of the use case
     * @param x the x position of the class
     * @param y the y position of the class
     */
    public void createNewUseCase(UseCaseModel owner, String name, float x, float y) {
        UseCase newUseCase = UcFactory.eINSTANCE.createUseCase();
        newUseCase.setName(name);

        LayoutElement layoutElement = UcFactory.eINSTANCE.createLayoutElement();
        layoutElement.setX(x);
        layoutElement.setY(y);

        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);

        // Create commands.
        Command addClassCommand = AddCommand.create(editingDomain, owner, UcPackage.Literals.USE_CASE_MODEL__USE_CASES,
                newUseCase);
        Command createElementMapCommand =
                createAddLayoutElementCommand(editingDomain, owner, newUseCase, layoutElement);

        // Create compound command.
        CompoundCommand compoundCommand = new CompoundCommand();
        // If the layout element is added first, when the structural view gets notified (about a new class)
        // access to the layout element is already possible.
        compoundCommand.append(addClassCommand);
        compoundCommand.append(createElementMapCommand);

        doExecute(editingDomain, compoundCommand);
    }
    
    /**
     * Creates a new note.
     *
     * @param owner the {@link UseCaseDiagram} the note should be added to
     * @param x the x position of the note
     * @param y the y position of the note
     */
    public void createNewNote(UseCaseModel cd, float x, float y) {
        //Create note
        Note newNote = UcFactory.eINSTANCE.createNote();

        //Create layout element
        LayoutElement layoutElement = UcFactory.eINSTANCE.createLayoutElement();
        layoutElement.setX(x);
        layoutElement.setY(y);
        
        //Get editing domain
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(cd);
        
        //Create Commands
        Command addNoteCommand = AddCommand.create(editingDomain, cd, UcPackage.Literals.USE_CASE_MODEL__NOTES,
                newNote);
        Command createElementMapCommand =
                createAddLayoutElementCommand(editingDomain, cd, newNote, layoutElement);

        // Create compound command.
        CompoundCommand compoundCommand = new CompoundCommand();
        compoundCommand.append(addNoteCommand);
        compoundCommand.append(createElementMapCommand);

        doExecute(editingDomain, compoundCommand);
        cd.getNotes().add(newNote);       
    }
    
    /**
     * Creates a note link between the note and the given element.
     *
     * @param owner the {@link ClassDiagram} the association should be added to
     * @param note the {@link Note} linked from
     * @param annotatedElement the annotated {@link NamedElement}
     */
    public void createAnnotation(UseCaseModel owner, Note note, NamedElement annotatedElement) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);

        if(!note.getNotedElement().contains(annotatedElement)) {
            Command addNotedElementCommand = AddCommand.create(editingDomain, note,
                    UcPackage.Literals.NOTE__NOTED_ELEMENT, annotatedElement);
            // execute command
            doExecute(editingDomain, addNotedElementCommand);
        }
    }

    /**
     * Moves a set of named elements in a diagram.
     * @param ucd The use case diagram.
     * @param positionMap The position map to use.
     */
    public void moveNamedElements(
            UseCaseModel ucd, Map<NamedElement, LayoutElement> positionMap) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(ucd);

        if (positionMap.size() > 0) {
            // Create compound command.
            CompoundCommand moveClassifiersCommand = new CompoundCommand();
    
            for (Entry<NamedElement, LayoutElement> entry : positionMap.entrySet()) {
                // get all required values
                NamedElement namedElement = entry.getKey();
                float x = entry.getValue().getX();
                float y = entry.getValue().getY();
    
                Command classifierMoveCommand = createMoveCommand(editingDomain, ucd, namedElement, x, y);
                moveClassifiersCommand.append(classifierMoveCommand);
            }
    
            doExecute(editingDomain, moveClassifiersCommand);
        }
        
    }

    /**
     * Moves a set of non-named elements in a diagram.
     * @param ucd The use case diagram.
     * @param nonClassifierPositionMap The position map to use.
     */
    public void moveNonNamedElements(
            UseCaseModel ucd, Map<EObject, LayoutElement> nonClassifierPositionMap) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(ucd);

        if (nonClassifierPositionMap.size() > 0) {
            // Create compound command.
            CompoundCommand moveClassifiersCommand = new CompoundCommand();
    
            for (Entry<EObject, LayoutElement> entry : nonClassifierPositionMap.entrySet()) {
                // get all required values
                EObject object = entry.getKey();
                float x = entry.getValue().getX();
                float y = entry.getValue().getY();
    
                Command classifierMoveCommand = createMoveCommand(editingDomain, ucd, object, x, y);
                moveClassifiersCommand.append(classifierMoveCommand);
            }
    
            doExecute(editingDomain, moveClassifiersCommand);
        }
        
    }
    
    /**
     * Removes the given actor and its associated layout element.
     *
     * @param actor the actor that should be removed
     */
    public void removeActor(Actor actor) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(actor);
        UseCaseModel ucd = (UseCaseModel) actor.eContainer();
        
        CompoundCommand compoundCommand = new CompoundCommand();
        
        // Manage the use cases associated to this actor
        for (UseCase uc : ucd.getUseCases()) {
            if (uc.getPrimaryActor() == actor) {
                compoundCommand.append(createClearPrimaryActorCommand(editingDomain, uc));
            } else if (uc.getSecondaryActors().contains(actor)) {
                compoundCommand.append(createRemoveSecondaryActorCommand(editingDomain, uc, actor));
            }
        }
        
        // Create command for removing ElementMap (includes the layout element).
        Command removeElementMapCommand = createRemoveLayoutElementCommand(editingDomain, ucd, actor);
        compoundCommand.append(removeElementMapCommand);

        // Create command for removing note.
        Command removeUseCaseCommand = RemoveCommand.create(editingDomain, actor);
        compoundCommand.append(removeUseCaseCommand);

        doExecute(editingDomain, compoundCommand);
    }
    
    /**
     * Removes the given use case and its associated layout element.
     *
     * @param useCase the use case that should be removed
     */
    public void removeUseCase(UseCase useCase) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(useCase);
        UseCaseModel ucd = (UseCaseModel) useCase.eContainer();
        
        CompoundCommand compoundCommand = new CompoundCommand();
        
        // Create command for removing ElementMap (includes the layout element).
        Command removeElementMapCommand = createRemoveLayoutElementCommand(editingDomain, ucd, useCase);
        compoundCommand.append(removeElementMapCommand);

        // Create command for removing note.
        Command removeUseCaseCommand = RemoveCommand.create(editingDomain, useCase);
        compoundCommand.append(removeUseCaseCommand);

        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Removes the given note and its associated layout element
     *
     * @param note the note that should be removed
     */
    public void removeNote(Note note) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(note);
        UseCaseModel ucd = (UseCaseModel) note.eContainer();
        
        CompoundCommand compoundCommand = new CompoundCommand();
        
        // Create command for removing ElementMap (includes the layout element).
        Command removeElementMapCommand = createRemoveLayoutElementCommand(editingDomain, ucd, note);
        compoundCommand.append(removeElementMapCommand);

        // Create command for removing note.
        Command removeNoteCommand = RemoveCommand.create(editingDomain, note);
        compoundCommand.append(removeNoteCommand);

        doExecute(editingDomain, compoundCommand);
    }
    
    /**
     * Remove a note link between the note and the given class.
     *
     * @param note the {@link Note} linked from
     * @param annotatedElement the annotated {@link NamedElement}
     */
    public void removeAnnotation(Note note, NamedElement annotatedElement) {
        UseCaseModel ucd = (UseCaseModel) note.eContainer();
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(ucd);
        
        Command removeNotedElementCommand = RemoveCommand.create(editingDomain, note,
                UcPackage.Literals.NOTE__NOTED_ELEMENT, annotatedElement);
        
        // execute command
        doExecute(editingDomain, removeNotedElementCommand);
    }
    
    /**
     * Remove all the annotations of the given note
     *
     * @param note the {@link Note} linked from
     */
    public void removeAnnotations(Note note) {
        UseCaseModel ucd = (UseCaseModel) note.eContainer();
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(ucd);
        
        CompoundCommand compoundCommand = new CompoundCommand();
        
        if (note.getNotedElement().size() > 0) {
            for (NamedElement element : note.getNotedElement()) {
                Command removeNotedElementCommand = RemoveCommand.create(editingDomain, note,
                        UcPackage.Literals.NOTE__NOTED_ELEMENT, element);
                compoundCommand.append(removeNotedElementCommand);
            }
                
            // execute command
            doExecute(editingDomain, compoundCommand);
        }
    }
}
