package ca.mcgill.sel.usecases.language.controller;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.usecases.Actor;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.UseCase;
import ca.mcgill.sel.usecases.UseCaseModel;

/**
 * A controller for actions on a use case.
 * @author rlanguay
 *
 */
public class UseCaseController extends BaseController {
    
    /**
     * Sets the primary actor on a use case.
     * @param actor The actor.
     * @param useCase The use case.
     */
    public void setPrimaryActor(Actor actor, UseCase useCase) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(useCase);
        CompoundCommand command = new CompoundCommand();
        
        // If the actor is already a secondary actor, remove it from that set first.
        if (useCase.getSecondaryActors().contains(actor)) {
            command.append(createRemoveSecondaryActorCommand(editingDomain, useCase, actor));
        }
        
        Command setCommand = SetCommand.create(editingDomain, useCase, UcPackage.Literals.USE_CASE__PRIMARY_ACTOR, actor);
        command.append (setCommand);
        
        doExecute(editingDomain, command);
    }
    
    /**
     * Adds a secondary actor to a use case.
     * @param actor The actor.
     * @param useCase The use case.
     */
    public void addSecondaryActor(Actor actor, UseCase useCase) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(useCase);
        CompoundCommand command = new CompoundCommand();
        
        // If the actor is already the primary actor, remove it first.
        if (useCase.getPrimaryActor() != null && useCase.getPrimaryActor().equals(actor)) {
            command.append(createClearPrimaryActorCommand(editingDomain, useCase));
        }
        
        Command addCommand = AddCommand.create(
                editingDomain, useCase, UcPackage.Literals.USE_CASE__SECONDARY_ACTORS, actor, useCase.getSecondaryActors().size());
        command.append (addCommand);
        
        doExecute(editingDomain, command);
    }
    
    /**
     * Clears the primary actor from a use case.
     * @param useCase The use case.
     */
    public void clearPrimaryActor(UseCase useCase) {
        UseCaseModel ucd = (UseCaseModel) useCase.eContainer();
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(ucd);
        
        Command command = createClearPrimaryActorCommand(editingDomain, useCase);
        
        doExecute(editingDomain, command);
    }
    
    /**
     * Removes a secondary actor from a use case.
     * @param actor The actor.
     * @param useCase The use case.
     */
    public void removeSecondaryActor(Actor actor, UseCase useCase) {
        UseCaseModel ucd = (UseCaseModel) useCase.eContainer();
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(ucd);
        
        Command command = createRemoveSecondaryActorCommand(editingDomain, useCase, actor);
        
        doExecute(editingDomain, command);
    }
}
