/**
 */
package ca.mcgill.sel.classdiagram.provider;


import ca.mcgill.sel.classdiagram.Attribute;
import ca.mcgill.sel.classdiagram.CdmPackage;
import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.classdiagram.ObjectType;
import ca.mcgill.sel.classdiagram.util.CdmInterfaceUtil;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;

/**
 * This is the item provider adapter for a {@link ca.mcgill.sel.classdiagram.Attribute} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class AttributeItemProvider extends StructuralFeatureItemProvider {
    /**
     * This constructs an instance from a factory and a notifier.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public AttributeItemProvider(AdapterFactory adapterFactory) {
        super(adapterFactory);
    }

    /**
     * This returns the property descriptors for the adapted class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
        if (itemPropertyDescriptors == null) {
            super.getPropertyDescriptors(object);

        }
        return itemPropertyDescriptors;
    }

    /**
     * This returns Attribute.gif.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object getImage(Object object) {
        return overlayImage(object, getResourceLocator().getImage("full/obj16/Attribute"));
    }

    /**
     * This returns the label text for the adapted class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String getText(Object object) {
        String label = ((Attribute)object).getName();
        return label == null || label.length() == 0 ?
            getString("_UI_Attribute_type") :
            getString("_UI_Attribute_type") + " " + label;
    }
    

    /**
     * This handles model notifications by calling {@link #updateChildren} to update any cached
     * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void notifyChanged(Notification notification) {
        updateChildren(notification);
        super.notifyChanged(notification);
    }

    /**
     * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
     * that can be created under this object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
        super.collectNewChildDescriptors(newChildDescriptors, object);
    }

    /**
     * This adds a property descriptor for the Type feature, which makes sure that it is impossible to 
     * define Object reference attributes (in this case, the user should use an association instead).
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated NOT
     */
    protected void addTypePropertyDescriptor(Object object) {
        itemPropertyDescriptors.add(
                // CHECKSTYLE:IGNORE AnonInnerLength: Okay here.
                new ItemPropertyDescriptor(
                	((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
                    getResourceLocator(),
                        getString("_UI_TypedElement_type_feature"),
                        getString("_UI_PropertyDescriptor_description", "_UI_TypedElement_type_feature", "_UI_TypedElement_type"),
                        CdmPackage.Literals.TYPED_ELEMENT__TYPE,
                        true,
                        false,
                        true,
                        null,
                        null,
                        null) {

                    @Override
                    public Collection<?> getChoiceOfValues(Object object) {
                        Attribute attribute = (Attribute) object;
                        // Set the current object as the target in order for the label provider to use.
                        // Since the item providers are state-less,
                        // the target could be any object of the loaded models.
                        setTarget(attribute);
                        ClassDiagram cd = EMFModelUtil.getRootContainerOfType(attribute, CdmPackage.Literals.CLASS_DIAGRAM);

                        Collection<ObjectType> result = CdmInterfaceUtil.getAvailableAttributeTypes(cd);
                        return result;
                    }
                });

     }

    
    
}
