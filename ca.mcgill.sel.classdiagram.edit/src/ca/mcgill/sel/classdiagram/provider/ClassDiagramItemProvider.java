/**
 */
package ca.mcgill.sel.classdiagram.provider;


import ca.mcgill.sel.classdiagram.CdmFactory;
import ca.mcgill.sel.classdiagram.CdmPackage;
import ca.mcgill.sel.classdiagram.ClassDiagram;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link ca.mcgill.sel.classdiagram.ClassDiagram} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ClassDiagramItemProvider extends NamedElementItemProvider {
    /**
     * This constructs an instance from a factory and a notifier.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public ClassDiagramItemProvider(AdapterFactory adapterFactory) {
        super(adapterFactory);
    }

    /**
     * This returns the property descriptors for the adapted class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
        if (itemPropertyDescriptors == null) {
            super.getPropertyDescriptors(object);

        }
        return itemPropertyDescriptors;
    }

    /**
     * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
     * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
     * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
        if (childrenFeatures == null) {
            super.getChildrenFeatures(object);
            childrenFeatures.add(CdmPackage.Literals.CLASS_DIAGRAM__CLASSES);
            childrenFeatures.add(CdmPackage.Literals.CLASS_DIAGRAM__TYPES);
            childrenFeatures.add(CdmPackage.Literals.CLASS_DIAGRAM__ASSOCIATIONS);
            childrenFeatures.add(CdmPackage.Literals.CLASS_DIAGRAM__NOTES);
            childrenFeatures.add(CdmPackage.Literals.CLASS_DIAGRAM__LAYOUT);
        }
        return childrenFeatures;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EStructuralFeature getChildFeature(Object object, Object child) {
        // Check the type of the specified child object and return the proper feature to use for
        // adding (see {@link AddCommand}) it as a child.

        return super.getChildFeature(object, child);
    }

    /**
     * This returns ClassDiagram.gif.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object getImage(Object object) {
        return overlayImage(object, getResourceLocator().getImage("full/obj16/ClassDiagram"));
    }

    /**
     * This returns the label text for the adapted class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String getText(Object object) {
        String label = ((ClassDiagram)object).getName();
        return label == null || label.length() == 0 ?
            getString("_UI_ClassDiagram_type") :
            getString("_UI_ClassDiagram_type") + " " + label;
    }
    

    /**
     * This handles model notifications by calling {@link #updateChildren} to update any cached
     * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void notifyChanged(Notification notification) {
        updateChildren(notification);

        switch (notification.getFeatureID(ClassDiagram.class)) {
            case CdmPackage.CLASS_DIAGRAM__CLASSES:
            case CdmPackage.CLASS_DIAGRAM__TYPES:
            case CdmPackage.CLASS_DIAGRAM__ASSOCIATIONS:
            case CdmPackage.CLASS_DIAGRAM__NOTES:
            case CdmPackage.CLASS_DIAGRAM__LAYOUT:
                fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
                return;
        }
        super.notifyChanged(notification);
    }

    /**
     * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
     * that can be created under this object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
        super.collectNewChildDescriptors(newChildDescriptors, object);

        newChildDescriptors.add
            (createChildParameter
                (CdmPackage.Literals.CLASS_DIAGRAM__CLASSES,
                 CdmFactory.eINSTANCE.createClass()));

        newChildDescriptors.add
            (createChildParameter
                (CdmPackage.Literals.CLASS_DIAGRAM__CLASSES,
                 CdmFactory.eINSTANCE.createImplementationClass()));

        newChildDescriptors.add
            (createChildParameter
                (CdmPackage.Literals.CLASS_DIAGRAM__CLASSES,
                 CdmFactory.eINSTANCE.createCDBoolean()));

        newChildDescriptors.add
            (createChildParameter
                (CdmPackage.Literals.CLASS_DIAGRAM__CLASSES,
                 CdmFactory.eINSTANCE.createCDDouble()));

        newChildDescriptors.add
            (createChildParameter
                (CdmPackage.Literals.CLASS_DIAGRAM__CLASSES,
                 CdmFactory.eINSTANCE.createCDInt()));

        newChildDescriptors.add
            (createChildParameter
                (CdmPackage.Literals.CLASS_DIAGRAM__CLASSES,
                 CdmFactory.eINSTANCE.createCDLong()));

        newChildDescriptors.add
            (createChildParameter
                (CdmPackage.Literals.CLASS_DIAGRAM__CLASSES,
                 CdmFactory.eINSTANCE.createCDString()));

        newChildDescriptors.add
            (createChildParameter
                (CdmPackage.Literals.CLASS_DIAGRAM__CLASSES,
                 CdmFactory.eINSTANCE.createCDByte()));

        newChildDescriptors.add
            (createChildParameter
                (CdmPackage.Literals.CLASS_DIAGRAM__CLASSES,
                 CdmFactory.eINSTANCE.createCDFloat()));

        newChildDescriptors.add
            (createChildParameter
                (CdmPackage.Literals.CLASS_DIAGRAM__CLASSES,
                 CdmFactory.eINSTANCE.createCDArray()));

        newChildDescriptors.add
            (createChildParameter
                (CdmPackage.Literals.CLASS_DIAGRAM__CLASSES,
                 CdmFactory.eINSTANCE.createCDChar()));

        newChildDescriptors.add
            (createChildParameter
                (CdmPackage.Literals.CLASS_DIAGRAM__CLASSES,
                 CdmFactory.eINSTANCE.createCDEnum()));

        newChildDescriptors.add
            (createChildParameter
                (CdmPackage.Literals.CLASS_DIAGRAM__CLASSES,
                 CdmFactory.eINSTANCE.createCDSet()));

        newChildDescriptors.add
            (createChildParameter
                (CdmPackage.Literals.CLASS_DIAGRAM__CLASSES,
                 CdmFactory.eINSTANCE.createCDSequence()));

        newChildDescriptors.add
            (createChildParameter
                (CdmPackage.Literals.CLASS_DIAGRAM__TYPES,
                 CdmFactory.eINSTANCE.createClass()));

        newChildDescriptors.add
            (createChildParameter
                (CdmPackage.Literals.CLASS_DIAGRAM__TYPES,
                 CdmFactory.eINSTANCE.createTypeParameter()));

        newChildDescriptors.add
            (createChildParameter
                (CdmPackage.Literals.CLASS_DIAGRAM__TYPES,
                 CdmFactory.eINSTANCE.createImplementationClass()));

        newChildDescriptors.add
            (createChildParameter
                (CdmPackage.Literals.CLASS_DIAGRAM__TYPES,
                 CdmFactory.eINSTANCE.createCDBoolean()));

        newChildDescriptors.add
            (createChildParameter
                (CdmPackage.Literals.CLASS_DIAGRAM__TYPES,
                 CdmFactory.eINSTANCE.createCDDouble()));

        newChildDescriptors.add
            (createChildParameter
                (CdmPackage.Literals.CLASS_DIAGRAM__TYPES,
                 CdmFactory.eINSTANCE.createCDInt()));

        newChildDescriptors.add
            (createChildParameter
                (CdmPackage.Literals.CLASS_DIAGRAM__TYPES,
                 CdmFactory.eINSTANCE.createCDLong()));

        newChildDescriptors.add
            (createChildParameter
                (CdmPackage.Literals.CLASS_DIAGRAM__TYPES,
                 CdmFactory.eINSTANCE.createCDString()));

        newChildDescriptors.add
            (createChildParameter
                (CdmPackage.Literals.CLASS_DIAGRAM__TYPES,
                 CdmFactory.eINSTANCE.createCDByte()));

        newChildDescriptors.add
            (createChildParameter
                (CdmPackage.Literals.CLASS_DIAGRAM__TYPES,
                 CdmFactory.eINSTANCE.createCDFloat()));

        newChildDescriptors.add
            (createChildParameter
                (CdmPackage.Literals.CLASS_DIAGRAM__TYPES,
                 CdmFactory.eINSTANCE.createCDArray()));

        newChildDescriptors.add
            (createChildParameter
                (CdmPackage.Literals.CLASS_DIAGRAM__TYPES,
                 CdmFactory.eINSTANCE.createCDChar()));

        newChildDescriptors.add
            (createChildParameter
                (CdmPackage.Literals.CLASS_DIAGRAM__TYPES,
                 CdmFactory.eINSTANCE.createCDEnum()));

        newChildDescriptors.add
            (createChildParameter
                (CdmPackage.Literals.CLASS_DIAGRAM__TYPES,
                 CdmFactory.eINSTANCE.createCDAny()));

        newChildDescriptors.add
            (createChildParameter
                (CdmPackage.Literals.CLASS_DIAGRAM__TYPES,
                 CdmFactory.eINSTANCE.createCDVoid()));

        newChildDescriptors.add
            (createChildParameter
                (CdmPackage.Literals.CLASS_DIAGRAM__TYPES,
                 CdmFactory.eINSTANCE.createCDSet()));

        newChildDescriptors.add
            (createChildParameter
                (CdmPackage.Literals.CLASS_DIAGRAM__TYPES,
                 CdmFactory.eINSTANCE.createCDSequence()));

        newChildDescriptors.add
            (createChildParameter
                (CdmPackage.Literals.CLASS_DIAGRAM__ASSOCIATIONS,
                 CdmFactory.eINSTANCE.createAssociation()));

        newChildDescriptors.add
            (createChildParameter
                (CdmPackage.Literals.CLASS_DIAGRAM__NOTES,
                 CdmFactory.eINSTANCE.createNote()));

        newChildDescriptors.add
            (createChildParameter
                (CdmPackage.Literals.CLASS_DIAGRAM__LAYOUT,
                 CdmFactory.eINSTANCE.createLayout()));
    }

    /**
     * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
        Object childFeature = feature;
        Object childObject = child;

        boolean qualify =
            childFeature == CdmPackage.Literals.CLASS_DIAGRAM__CLASSES ||
            childFeature == CdmPackage.Literals.CLASS_DIAGRAM__TYPES;

        if (qualify) {
            return getString
                ("_UI_CreateChild_text2",
                 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
        }
        return super.getCreateChildText(owner, feature, child, selection);
    }

}
