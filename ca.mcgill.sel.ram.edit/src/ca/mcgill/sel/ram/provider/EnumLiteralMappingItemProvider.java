/**
 */
package ca.mcgill.sel.ram.provider;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;

import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.provider.COREMappingItemProvider;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.core.util.COREModelUtil;
import ca.mcgill.sel.ram.EnumLiteralMapping;
import ca.mcgill.sel.ram.EnumMapping;
import ca.mcgill.sel.ram.REnum;
import ca.mcgill.sel.ram.REnumLiteral;
import ca.mcgill.sel.ram.RamFactory;
import ca.mcgill.sel.ram.impl.REnumImpl;

/**
 * This is the item provider adapter for a {@link ca.mcgill.sel.ram.EnumLiteralMapping} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class EnumLiteralMappingItemProvider extends COREMappingItemProvider {
    /**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public EnumLiteralMappingItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

    /**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

    /**
	 * This returns EnumLiteralMapping.gif.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/EnumLiteralMapping"));
	}

    /**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public String getText(Object object) {
		return getString("_UI_EnumLiteralMapping_type");
	}
    
    /**
     * This adds a property descriptor for the From feature.
     * <!-- begin-user-doc -->
     * 
     * @param object the object to add a property descriptor for
     *            <!-- end-user-doc -->
     * @generated NOT
     */
    @Override
    protected void addFromPropertyDescriptor(Object object) {
        itemPropertyDescriptors.add(
            // CHECKSTYLE:IGNORE AnonInnerLength: Okay here.
            new MappingFromItemPropertyDescriptor(
                    ((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
                    getResourceLocator(),
                    getString("_UI_CORELink_from_feature"),
                    getString("_UI_PropertyDescriptor_description", 
                                "_UI_CORELink_from_feature", "_UI_CORELink_type"),
                    CorePackage.Literals.CORE_LINK__FROM,
                    true,
                    false,
                    true,
                    null,
                    null,
                    null) {
        
                @Override
                public Collection<?> getChoiceOfValues(Object object) {
                    COREMapping<REnumLiteral> enumLMapping = (EnumLiteralMapping) object;
                    
                    // Gather the EnumMapping associated
                    COREMapping<REnum> enumMapping = (EnumMapping) enumLMapping.eContainer();
                   
                    // Gather all the enums literals from all mapped elements
                    Collection<REnumLiteral> enumLResult = new ArrayList<>();
                    for (EObject enumImpl : COREArtefactUtil.getAllMappedElements(enumMapping)) {
                        enumLResult.addAll(((REnumImpl) enumImpl).getLiterals());
                    }
                    
                    // Filter out all elements from enumLResult that are mapped to another element.
                    COREModelUtil.filterMappedElements(enumLResult);
                    
                    return enumLResult;
                }
            });
    }
    
    /**
     * This adds a property descriptor for the To feature.
     * <!-- begin-user-doc -->
     * 
     * @param object the object to add a property descriptor for
     *            <!-- end-user-doc -->
     * @generated NOT
     */
    protected void addToPropertyDescriptor(Object object) {
        itemPropertyDescriptors.add(
            new ItemPropertyDescriptor(
                    ((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
                    getResourceLocator(),
                    getString("_UI_CORELink_to_feature"),
                    getString("_UI_PropertyDescriptor_description", "_UI_CORELink_to_feature", 
                                "_UI_CORELink_type"),
                    CorePackage.Literals.CORE_LINK__TO,
                    true,
                    false,
                    true,
                    null,
                    null,
                    null) {
                
                @Override
                public Collection<?> getChoiceOfValues(Object object) {
                    COREMapping<REnumLiteral> enumLMapping = (EnumLiteralMapping) object;
                    
                    // Gather the EnumMapping associated
                    COREMapping<REnum> enumMapping = (EnumMapping) enumLMapping.eContainer();
                   
                    // Gather all the enum literals from all mapped elements
                    Collection<REnumLiteral> enumLResult = new ArrayList<>();
                    for (EObject enumImpl : COREArtefactUtil.getAllMappedElements(enumMapping)) {
                        enumLResult.addAll(((REnumImpl) enumImpl).getLiterals());
                    }
                    
                    // Gather the REnum from the "to" relation
                    REnumImpl rEnum = (REnumImpl) enumMapping.getTo();
                    enumLResult.addAll(rEnum.getLiterals());
                    
                    // Filter out all elements from enumLResult that are mapped to another element.
                    COREModelUtil.filterMappedElements(enumLResult);
                    
                    // Remove the previously chosen attribute in the enumLMapping from
                    enumLResult.remove(enumLMapping.getFrom());
                    
                    return enumLResult;
                }
            });
    }
    

    /**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public void notifyChanged(Notification notification) {
		updateChildren(notification);
		super.notifyChanged(notification);
	}

    /**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.CORE_MAPPING__MAPPINGS,
				 RamFactory.eINSTANCE.createClassifierMapping()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.CORE_MAPPING__MAPPINGS,
				 RamFactory.eINSTANCE.createAttributeMapping()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.CORE_MAPPING__MAPPINGS,
				 RamFactory.eINSTANCE.createOperationMapping()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.CORE_MAPPING__MAPPINGS,
				 RamFactory.eINSTANCE.createParameterMapping()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.CORE_MAPPING__MAPPINGS,
				 RamFactory.eINSTANCE.createEnumMapping()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.CORE_MAPPING__MAPPINGS,
				 RamFactory.eINSTANCE.createEnumLiteralMapping()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.CORE_MAPPING__MAPPINGS,
				 RamFactory.eINSTANCE.createAssociationEndMapping()));
	}

    /**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public ResourceLocator getResourceLocator() {
		return RAMEditPlugin.INSTANCE;
	}

}
