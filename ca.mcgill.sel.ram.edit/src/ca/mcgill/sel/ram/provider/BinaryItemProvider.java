/**
 */
package ca.mcgill.sel.ram.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.ram.Binary;
import ca.mcgill.sel.ram.Shift;
import ca.mcgill.sel.ram.LogicalOperator;
import ca.mcgill.sel.ram.MulDivMod;
import ca.mcgill.sel.ram.Comparison;
import ca.mcgill.sel.ram.Equality;
import ca.mcgill.sel.ram.RamFactory;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.ValueSpecification;

/**
 * This is the item provider adapter for a {@link ca.mcgill.sel.ram.Binary} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class BinaryItemProvider extends OperatorItemProvider {

    /**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public BinaryItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

    /**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

    /**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(RamPackage.Literals.BINARY__LEFT);
			childrenFeatures.add(RamPackage.Literals.BINARY__RIGHT);
		}
		return childrenFeatures;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

    /**
	 * This returns Binary.gif.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Binary"));
	}

    /**
     * This returns textual representation of the operator.
     * 
     * @param valueSpec
     *            The root node representing the Operator
     * @return opVal
     *         Textual representation of the Operator
     * @generated NOT
     */
    private String getOperatorRepresentation(ValueSpecification valueSpec) {
        String opVal = "";
        switch (valueSpec.eClass().getClassifierID()) {

            case RamPackage.EQUALITY:
                opVal = " " + ((Equality) valueSpec).getOp() + " ";
                break;
            case RamPackage.MUL_DIV_MOD:
                opVal = " " + ((MulDivMod) valueSpec).getOp() + " ";
                break;
            case RamPackage.PLUS:
                opVal = " + ";
                break;
            case RamPackage.AND:
                opVal = " && ";
                break;
            case RamPackage.OR:
                opVal = " || ";
                break;
            case RamPackage.MINUS:
                opVal = " - ";
                break;
            case RamPackage.COMPARISON:
                opVal = " " + ((Comparison) valueSpec).getOp() + " ";
                break;
            case RamPackage.SHIFT:
                opVal = " " + ((Shift) valueSpec).getOp() + " ";
                break;
            case RamPackage.LOGICAL_OPERATOR:
                opVal = " " + ((LogicalOperator) valueSpec).getOp() + " ";
                break;
            default:
                opVal = "";
                break;
        }
        return opVal;
    }

    /**
     * This returns the label text for the adapted class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public String getText(Object object) {

        String label = "";

        Binary parent = (Binary) object;
        ValueSpecification left = parent.getLeft();
        ValueSpecification right = parent.getRight();

        label = processNode(parent, left, false);
        label = label.concat(getOperatorRepresentation((ValueSpecification) object));
        label = label.concat(processNode(parent, right, true));

        return label;
    }

    /**
     * This returns the label text for the corresponding node.
     * 
     * @param parent
     *            Parent of the node for which textual representation needs to be determined
     * @param node
     *            Node for which textual representation needs to be determined
     * @param putParanthesis
     *            Determines if there is a need to put parenthesis in the expression
     * @return Textual representation of the node
     * @generated NOT
     */
    private String processNode(ValueSpecification parent, ValueSpecification node, boolean putParanthesis) {
        String label = "";

        // If we have equal priority operators, we only put parenthesis on the right node (if specified by the user)
        if (node instanceof Binary) {
            if (getPrecedence(parent) > getPrecedence(node)
                    || (getPrecedence(parent) == getPrecedence(node) && putParanthesis)) {
                label = label.concat("(" + EMFEditUtil.getText(node)) + ")";
            } else {
                label = label.concat(EMFEditUtil.getText(node));
            }
        } else {
            label = label.concat(EMFEditUtil.getText(node));
        }

        return label;
    }

    /**
     * This returns operator precedence.
     * 
     * @param valueSpec
     *            The Node/Model
     * @return precedence
     *         The numeric value representing precedence
     * @generated NOT
     */
    public int getPrecedence(ValueSpecification valueSpec) {
        int precedence = 0;
        switch (valueSpec.eClass().getClassifierID()) {
            case RamPackage.MUL_DIV_MOD:
                precedence = 10;
                break;
            case RamPackage.PLUS:
                precedence = 9;
                break;
            case RamPackage.MINUS:
                precedence = 9;
                break;
            case RamPackage.BITWISE:
                precedence = 8;
                break;
            case RamPackage.COMPARISON:
                precedence = 7;
                break;
            case RamPackage.EQUALITY:
                precedence = 6;
                break;
            case RamPackage.AND:
                precedence = 5;
                break;
            case RamPackage.OR:
                precedence = 4;
                break;
            case RamPackage.CONDITIONAL:
                precedence = 5;
                break;
            default:
                precedence = 0;
                break;
        }
        return precedence;
    }

    /**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Binary.class)) {
			case RamPackage.BINARY__LEFT:
			case RamPackage.BINARY__RIGHT:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

    /**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__LEFT,
				 RamFactory.eINSTANCE.createStructuralFeatureValue()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__LEFT,
				 RamFactory.eINSTANCE.createParameterValue()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__LEFT,
				 RamFactory.eINSTANCE.createOpaqueExpression()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__LEFT,
				 RamFactory.eINSTANCE.createLiteralString()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__LEFT,
				 RamFactory.eINSTANCE.createLiteralInteger()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__LEFT,
				 RamFactory.eINSTANCE.createLiteralBoolean()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__LEFT,
				 RamFactory.eINSTANCE.createLiteralNull()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__LEFT,
				 RamFactory.eINSTANCE.createEnumLiteralValue()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__LEFT,
				 RamFactory.eINSTANCE.createLiteralLong()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__LEFT,
				 RamFactory.eINSTANCE.createLiteralFloat()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__LEFT,
				 RamFactory.eINSTANCE.createLiteralChar()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__LEFT,
				 RamFactory.eINSTANCE.createLiteralDouble()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__LEFT,
				 RamFactory.eINSTANCE.createEquality()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__LEFT,
				 RamFactory.eINSTANCE.createPlus()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__LEFT,
				 RamFactory.eINSTANCE.createMulDivMod()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__LEFT,
				 RamFactory.eINSTANCE.createNot()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__LEFT,
				 RamFactory.eINSTANCE.createAnd()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__LEFT,
				 RamFactory.eINSTANCE.createOr()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__LEFT,
				 RamFactory.eINSTANCE.createMinus()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__LEFT,
				 RamFactory.eINSTANCE.createComparison()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__LEFT,
				 RamFactory.eINSTANCE.createUnaryMinus()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__LEFT,
				 RamFactory.eINSTANCE.createShift()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__LEFT,
				 RamFactory.eINSTANCE.createConditional()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__LEFT,
				 RamFactory.eINSTANCE.createPreIncrementOrDecrement()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__LEFT,
				 RamFactory.eINSTANCE.createPostIncrementOrDecrement()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__LEFT,
				 RamFactory.eINSTANCE.createLogicalOperator()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__LEFT,
				 RamFactory.eINSTANCE.createLiteralByte()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__RIGHT,
				 RamFactory.eINSTANCE.createStructuralFeatureValue()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__RIGHT,
				 RamFactory.eINSTANCE.createParameterValue()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__RIGHT,
				 RamFactory.eINSTANCE.createOpaqueExpression()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__RIGHT,
				 RamFactory.eINSTANCE.createLiteralString()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__RIGHT,
				 RamFactory.eINSTANCE.createLiteralInteger()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__RIGHT,
				 RamFactory.eINSTANCE.createLiteralBoolean()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__RIGHT,
				 RamFactory.eINSTANCE.createLiteralNull()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__RIGHT,
				 RamFactory.eINSTANCE.createEnumLiteralValue()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__RIGHT,
				 RamFactory.eINSTANCE.createLiteralLong()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__RIGHT,
				 RamFactory.eINSTANCE.createLiteralFloat()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__RIGHT,
				 RamFactory.eINSTANCE.createLiteralChar()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__RIGHT,
				 RamFactory.eINSTANCE.createLiteralDouble()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__RIGHT,
				 RamFactory.eINSTANCE.createEquality()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__RIGHT,
				 RamFactory.eINSTANCE.createPlus()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__RIGHT,
				 RamFactory.eINSTANCE.createMulDivMod()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__RIGHT,
				 RamFactory.eINSTANCE.createNot()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__RIGHT,
				 RamFactory.eINSTANCE.createAnd()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__RIGHT,
				 RamFactory.eINSTANCE.createOr()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__RIGHT,
				 RamFactory.eINSTANCE.createMinus()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__RIGHT,
				 RamFactory.eINSTANCE.createComparison()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__RIGHT,
				 RamFactory.eINSTANCE.createUnaryMinus()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__RIGHT,
				 RamFactory.eINSTANCE.createShift()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__RIGHT,
				 RamFactory.eINSTANCE.createConditional()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__RIGHT,
				 RamFactory.eINSTANCE.createPreIncrementOrDecrement()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__RIGHT,
				 RamFactory.eINSTANCE.createPostIncrementOrDecrement()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__RIGHT,
				 RamFactory.eINSTANCE.createLogicalOperator()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.BINARY__RIGHT,
				 RamFactory.eINSTANCE.createLiteralByte()));
	}

    /**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == RamPackage.Literals.BINARY__LEFT ||
			childFeature == RamPackage.Literals.BINARY__RIGHT;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
