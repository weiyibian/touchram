/**
 */
package ca.mcgill.sel.ram.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.ram.RamFactory;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.Ternary;
import ca.mcgill.sel.ram.ValueSpecification;

/**
 * This is the item provider adapter for a {@link ca.mcgill.sel.ram.Ternary} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class TernaryItemProvider extends OperatorItemProvider {
    /**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public TernaryItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

    /**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

    /**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(RamPackage.Literals.TERNARY__CONDITION);
			childrenFeatures.add(RamPackage.Literals.TERNARY__EXPRESSION_T);
			childrenFeatures.add(RamPackage.Literals.TERNARY__EXPRESSION_F);
		}
		return childrenFeatures;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

    /**
	 * This returns Ternary.gif.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Ternary"));
	}

    /**
     * This returns the label text for the adapted class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public String getText(Object object) {
        Ternary parent = (Ternary) object;
        String condition = EMFEditUtil.getText(parent.getCondition());
        String expressionT = EMFEditUtil.getText(parent.getExpressionT());
        String expressionF = EMFEditUtil.getText(parent.getExpressionF());

        String result = String.format(getOperatorRepresentation(parent), condition, expressionT, expressionF);
        return result;
    }

    /**
     * This returns textual representation of the operator.
     * 
     * @param valueSpec
     *            The root node representing the Operator
     * @return opVal
     *         Textual representation of the Operator
     * @generated NOT
     */
    private String getOperatorRepresentation(ValueSpecification valueSpec) {
        switch (valueSpec.eClass().getClassifierID()) {
            case RamPackage.CONDITIONAL:
                return "%s ? %s : %s";
            default:
                return "%s";
        }
    }

    /**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Ternary.class)) {
			case RamPackage.TERNARY__CONDITION:
			case RamPackage.TERNARY__EXPRESSION_T:
			case RamPackage.TERNARY__EXPRESSION_F:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

    /**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__CONDITION,
				 RamFactory.eINSTANCE.createStructuralFeatureValue()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__CONDITION,
				 RamFactory.eINSTANCE.createParameterValue()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__CONDITION,
				 RamFactory.eINSTANCE.createOpaqueExpression()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__CONDITION,
				 RamFactory.eINSTANCE.createLiteralString()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__CONDITION,
				 RamFactory.eINSTANCE.createLiteralInteger()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__CONDITION,
				 RamFactory.eINSTANCE.createLiteralBoolean()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__CONDITION,
				 RamFactory.eINSTANCE.createLiteralNull()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__CONDITION,
				 RamFactory.eINSTANCE.createEnumLiteralValue()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__CONDITION,
				 RamFactory.eINSTANCE.createLiteralLong()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__CONDITION,
				 RamFactory.eINSTANCE.createLiteralFloat()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__CONDITION,
				 RamFactory.eINSTANCE.createLiteralChar()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__CONDITION,
				 RamFactory.eINSTANCE.createLiteralDouble()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__CONDITION,
				 RamFactory.eINSTANCE.createEquality()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__CONDITION,
				 RamFactory.eINSTANCE.createPlus()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__CONDITION,
				 RamFactory.eINSTANCE.createMulDivMod()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__CONDITION,
				 RamFactory.eINSTANCE.createNot()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__CONDITION,
				 RamFactory.eINSTANCE.createAnd()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__CONDITION,
				 RamFactory.eINSTANCE.createOr()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__CONDITION,
				 RamFactory.eINSTANCE.createMinus()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__CONDITION,
				 RamFactory.eINSTANCE.createComparison()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__CONDITION,
				 RamFactory.eINSTANCE.createUnaryMinus()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__CONDITION,
				 RamFactory.eINSTANCE.createShift()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__CONDITION,
				 RamFactory.eINSTANCE.createConditional()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__CONDITION,
				 RamFactory.eINSTANCE.createPreIncrementOrDecrement()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__CONDITION,
				 RamFactory.eINSTANCE.createPostIncrementOrDecrement()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__CONDITION,
				 RamFactory.eINSTANCE.createLogicalOperator()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__CONDITION,
				 RamFactory.eINSTANCE.createLiteralByte()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_T,
				 RamFactory.eINSTANCE.createStructuralFeatureValue()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_T,
				 RamFactory.eINSTANCE.createParameterValue()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_T,
				 RamFactory.eINSTANCE.createOpaqueExpression()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_T,
				 RamFactory.eINSTANCE.createLiteralString()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_T,
				 RamFactory.eINSTANCE.createLiteralInteger()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_T,
				 RamFactory.eINSTANCE.createLiteralBoolean()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_T,
				 RamFactory.eINSTANCE.createLiteralNull()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_T,
				 RamFactory.eINSTANCE.createEnumLiteralValue()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_T,
				 RamFactory.eINSTANCE.createLiteralLong()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_T,
				 RamFactory.eINSTANCE.createLiteralFloat()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_T,
				 RamFactory.eINSTANCE.createLiteralChar()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_T,
				 RamFactory.eINSTANCE.createLiteralDouble()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_T,
				 RamFactory.eINSTANCE.createEquality()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_T,
				 RamFactory.eINSTANCE.createPlus()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_T,
				 RamFactory.eINSTANCE.createMulDivMod()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_T,
				 RamFactory.eINSTANCE.createNot()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_T,
				 RamFactory.eINSTANCE.createAnd()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_T,
				 RamFactory.eINSTANCE.createOr()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_T,
				 RamFactory.eINSTANCE.createMinus()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_T,
				 RamFactory.eINSTANCE.createComparison()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_T,
				 RamFactory.eINSTANCE.createUnaryMinus()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_T,
				 RamFactory.eINSTANCE.createShift()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_T,
				 RamFactory.eINSTANCE.createConditional()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_T,
				 RamFactory.eINSTANCE.createPreIncrementOrDecrement()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_T,
				 RamFactory.eINSTANCE.createPostIncrementOrDecrement()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_T,
				 RamFactory.eINSTANCE.createLogicalOperator()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_T,
				 RamFactory.eINSTANCE.createLiteralByte()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_F,
				 RamFactory.eINSTANCE.createStructuralFeatureValue()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_F,
				 RamFactory.eINSTANCE.createParameterValue()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_F,
				 RamFactory.eINSTANCE.createOpaqueExpression()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_F,
				 RamFactory.eINSTANCE.createLiteralString()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_F,
				 RamFactory.eINSTANCE.createLiteralInteger()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_F,
				 RamFactory.eINSTANCE.createLiteralBoolean()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_F,
				 RamFactory.eINSTANCE.createLiteralNull()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_F,
				 RamFactory.eINSTANCE.createEnumLiteralValue()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_F,
				 RamFactory.eINSTANCE.createLiteralLong()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_F,
				 RamFactory.eINSTANCE.createLiteralFloat()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_F,
				 RamFactory.eINSTANCE.createLiteralChar()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_F,
				 RamFactory.eINSTANCE.createLiteralDouble()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_F,
				 RamFactory.eINSTANCE.createEquality()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_F,
				 RamFactory.eINSTANCE.createPlus()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_F,
				 RamFactory.eINSTANCE.createMulDivMod()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_F,
				 RamFactory.eINSTANCE.createNot()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_F,
				 RamFactory.eINSTANCE.createAnd()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_F,
				 RamFactory.eINSTANCE.createOr()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_F,
				 RamFactory.eINSTANCE.createMinus()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_F,
				 RamFactory.eINSTANCE.createComparison()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_F,
				 RamFactory.eINSTANCE.createUnaryMinus()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_F,
				 RamFactory.eINSTANCE.createShift()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_F,
				 RamFactory.eINSTANCE.createConditional()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_F,
				 RamFactory.eINSTANCE.createPreIncrementOrDecrement()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_F,
				 RamFactory.eINSTANCE.createPostIncrementOrDecrement()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_F,
				 RamFactory.eINSTANCE.createLogicalOperator()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.TERNARY__EXPRESSION_F,
				 RamFactory.eINSTANCE.createLiteralByte()));
	}

    /**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == RamPackage.Literals.TERNARY__CONDITION ||
			childFeature == RamPackage.Literals.TERNARY__EXPRESSION_T ||
			childFeature == RamPackage.Literals.TERNARY__EXPRESSION_F;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
