/**
 */
package ca.mcgill.sel.ram.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.provider.IChangeNotifier;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.INotifyChangedListener;
import org.eclipse.emf.edit.provider.IViewerNotification;
import org.eclipse.emf.edit.provider.ViewerNotification;

import ca.mcgill.sel.ram.Binary;
import ca.mcgill.sel.ram.EnumLiteralValue;
import ca.mcgill.sel.ram.ParameterValue;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.StructuralFeatureValue;
import ca.mcgill.sel.ram.Ternary;
import ca.mcgill.sel.ram.ValueSpecification;

/**
 * This is the item provider adapter for a {@link ca.mcgill.sel.ram.Operator} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class OperatorItemProvider extends ValueSpecificationItemProvider {

    class ChangeListener implements INotifyChangedListener {
        public void notifyChanged(Notification notification) {

            if (notification.getNotifier() != null && getTarget() != null) {
                Object element = ((IViewerNotification) notification).getElement();
                EObject target = (EObject) getTarget();

                // Restrict to the first operator of the expression,
                // and only if the updated element is one that references another object.
                if (!(target.eContainer() instanceof ValueSpecification)
                        && target.eContainer() != null
                        && (target instanceof Binary || target instanceof Ternary)
                        && (element instanceof ParameterValue || element instanceof StructuralFeatureValue
                                || element instanceof EnumLiteralValue)) {

                    // Since there can be a nested hierarchy of operators, we need to go through the hierarchy
                    // and find out whether the updated element is contained within.
                    // I.e., the item provider of the elements checked for above trigger notifications
                    // when their referenced object changes
                    // (hence, we don't need to check for the specific reference in each of the elements).
                    boolean contained = false;

                    for (TreeIterator<EObject> iterator = target.eAllContents(); iterator.hasNext();) {
                        if (iterator.next() == element) {
                            contained = true;
                            break;
                        }
                    }

                    if (contained) {
                        ((IChangeNotifier) getAdapterFactory()).removeListener(this);
                        fireNotifyChanged(new ViewerNotification(notification, target.eContainer(), false, true));
                        ((IChangeNotifier) getAdapterFactory()).addListener(this);
                    }
                }
            }
        }
    }

    private ChangeListener changeListener;

    /**
     * This constructs an instance from a factory and a notifier.
     * <!-- begin-user-doc -->
     * 
     * @param adapterFactory the adapter factory for this item provider
     *            <!-- end-user-doc -->
     * @generated NOT
     */
    public OperatorItemProvider(AdapterFactory adapterFactory) {
        super(adapterFactory);

        /**
         * The change listener is required to get notifications about referenced elements
         * and pass the notifications to its own listener.
         * I.e., The values name could change.
         */
        if (adapterFactory instanceof IChangeNotifier) {
            IChangeNotifier changeNotifier = (IChangeNotifier) adapterFactory;
            changeListener = new ChangeListener();
            changeNotifier.addListener(changeListener);
        }
    }

    @Override
    public void dispose() {
        super.dispose();

        if (changeListener != null) {
            ((IChangeNotifier) getAdapterFactory()).removeListener(changeListener);
        }
    }

    /**
     * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

    /**
	 * This returns Operator.gif.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Operator"));
	}

    /**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public String getText(Object object) {
		return getString("_UI_Operator_type");
	}

    /**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public void notifyChanged(Notification notification) {
		updateChildren(notification);
		super.notifyChanged(notification);
	}

    /**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

    /**
     * This returns operator precedence.
     * 
     * @param valueSpec
     *            The Node/Model
     * @return precedence
     *         The numeric value representing precedence
     * @generated NOT
     */
    public int getPrecedence(ValueSpecification valueSpec) {
        int precedence = 0;
        switch (valueSpec.eClass().getClassifierID()) {
            case RamPackage.NOT:
                precedence = 10;
                break;
            case RamPackage.MUL_DIV_MOD:
                precedence = 9;
                break;
            case RamPackage.PLUS:
                precedence = 8;
                break;
            case RamPackage.MINUS:
                precedence = 8;
                break;
            case RamPackage.COMPARISON:
                precedence = 7;
                break;
            case RamPackage.EQUALITY:
                precedence = 6;
                break;
            case RamPackage.AND:
                precedence = 5;
                break;
            case RamPackage.OR:
                precedence = 4;
                break;
            default:
                precedence = 0;
                break;
        }
        return precedence;
    }
}
