/**
 */
package ca.mcgill.sel.ram.provider;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.ram.Not;
import ca.mcgill.sel.ram.Operator;
import ca.mcgill.sel.ram.RamFactory;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.Unary;
import ca.mcgill.sel.ram.PostIncrementOrDecrement;
import ca.mcgill.sel.ram.PreIncrementOrDecrement;
import ca.mcgill.sel.ram.ValueSpecification;
import java.util.Collection;
import java.util.List;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link ca.mcgill.sel.ram.Unary} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class UnaryItemProvider extends OperatorItemProvider {
    /**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public UnaryItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

    /**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

    /**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(RamPackage.Literals.UNARY__EXPRESSION);
		}
		return childrenFeatures;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

    /**
	 * This returns Unary.gif.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Unary"));
	}

    /**
     * This returns textual representation of the operator.
     * 
     * @param valueSpec
     *            The root node representing the Operator
     * @return opVal
     *         Textual representation of the Operator
     * @generated NOT
     */
    private String getNodeRepresentation(ValueSpecification valueSpec) {
        String result = "";
        
        switch (valueSpec.eClass().getClassifierID()) {
            case RamPackage.NOT:
                result = " !%s";
                break;
            case RamPackage.UNARY_MINUS:
                result = "-%s";
                break;
            case RamPackage.POST_INCREMENT_OR_DECREMENT:
                result = "%s" + (((PostIncrementOrDecrement) valueSpec).getOp());
                break;
            case RamPackage.PRE_INCREMENT_OR_DECREMENT:
                result = (((PreIncrementOrDecrement) valueSpec).getOp()) + "%s";
                break;
        }
        
        return result;
    }

    /**
     * This returns the label text for the adapted class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public String getText(Object object) {
        // TODO: Refactor later and remove label
        String label = "";

        Unary parent = (Unary) object;
        ValueSpecification expression = parent.getExpression();

        if (object instanceof Not) {
            if (expression instanceof Operator) {
                if (getPrecedence(parent) > getPrecedence(expression)
                        || getPrecedence(parent) == getPrecedence(expression)) {
                    label = String.format(getNodeRepresentation((ValueSpecification) object),
                            "(" + EMFEditUtil.getText(expression)) + ")";
                }
            } else {
                label = String.format(getNodeRepresentation((ValueSpecification) object),
                        EMFEditUtil.getText(expression));
            }
        } else if (object instanceof PostIncrementOrDecrement || object instanceof PreIncrementOrDecrement) {
            label = String.format(getNodeRepresentation((ValueSpecification) object),
                    EMFEditUtil.getText(expression));
        }
        return label;
    }

    /**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Unary.class)) {
			case RamPackage.UNARY__EXPRESSION:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

    /**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.UNARY__EXPRESSION,
				 RamFactory.eINSTANCE.createStructuralFeatureValue()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.UNARY__EXPRESSION,
				 RamFactory.eINSTANCE.createParameterValue()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.UNARY__EXPRESSION,
				 RamFactory.eINSTANCE.createOpaqueExpression()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.UNARY__EXPRESSION,
				 RamFactory.eINSTANCE.createLiteralString()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.UNARY__EXPRESSION,
				 RamFactory.eINSTANCE.createLiteralInteger()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.UNARY__EXPRESSION,
				 RamFactory.eINSTANCE.createLiteralBoolean()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.UNARY__EXPRESSION,
				 RamFactory.eINSTANCE.createLiteralNull()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.UNARY__EXPRESSION,
				 RamFactory.eINSTANCE.createEnumLiteralValue()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.UNARY__EXPRESSION,
				 RamFactory.eINSTANCE.createLiteralLong()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.UNARY__EXPRESSION,
				 RamFactory.eINSTANCE.createLiteralFloat()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.UNARY__EXPRESSION,
				 RamFactory.eINSTANCE.createLiteralChar()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.UNARY__EXPRESSION,
				 RamFactory.eINSTANCE.createLiteralDouble()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.UNARY__EXPRESSION,
				 RamFactory.eINSTANCE.createEquality()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.UNARY__EXPRESSION,
				 RamFactory.eINSTANCE.createPlus()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.UNARY__EXPRESSION,
				 RamFactory.eINSTANCE.createMulDivMod()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.UNARY__EXPRESSION,
				 RamFactory.eINSTANCE.createNot()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.UNARY__EXPRESSION,
				 RamFactory.eINSTANCE.createAnd()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.UNARY__EXPRESSION,
				 RamFactory.eINSTANCE.createOr()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.UNARY__EXPRESSION,
				 RamFactory.eINSTANCE.createMinus()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.UNARY__EXPRESSION,
				 RamFactory.eINSTANCE.createComparison()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.UNARY__EXPRESSION,
				 RamFactory.eINSTANCE.createUnaryMinus()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.UNARY__EXPRESSION,
				 RamFactory.eINSTANCE.createShift()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.UNARY__EXPRESSION,
				 RamFactory.eINSTANCE.createConditional()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.UNARY__EXPRESSION,
				 RamFactory.eINSTANCE.createPreIncrementOrDecrement()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.UNARY__EXPRESSION,
				 RamFactory.eINSTANCE.createPostIncrementOrDecrement()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.UNARY__EXPRESSION,
				 RamFactory.eINSTANCE.createLogicalOperator()));

		newChildDescriptors.add
			(createChildParameter
				(RamPackage.Literals.UNARY__EXPRESSION,
				 RamFactory.eINSTANCE.createLiteralByte()));
	}

}
