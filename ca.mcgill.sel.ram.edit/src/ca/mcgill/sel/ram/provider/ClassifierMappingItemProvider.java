/**
 */
package ca.mcgill.sel.ram.provider;


import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;

import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.COREModelExtension;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.controller.COREControllerFactory;
import ca.mcgill.sel.core.controller.MappingController;
import ca.mcgill.sel.core.provider.COREMappingItemProvider;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.ClassifierMapping;
import ca.mcgill.sel.ram.ImplementationClass;
import ca.mcgill.sel.ram.RamFactory;
import ca.mcgill.sel.ram.controller.util.RAMReferenceUtil;
import ca.mcgill.sel.ram.provider.util.RAMEditUtil;
import ca.mcgill.sel.ram.util.RAMInterfaceUtil;

/**
 * This is the item provider adapter for a {@link ca.mcgill.sel.ram.ClassifierMapping} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ClassifierMappingItemProvider
    extends COREMappingItemProvider {
    /**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public ClassifierMappingItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

    /**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

    /**
     * This adds a property descriptor for the From Element feature.
     * <!-- begin-user-doc -->
     * @param object the object to add a property descriptor for
     * <!-- end-user-doc -->
     * @generated NOT
     */
    protected void addFromPropertyDescriptor(Object object) {
        itemPropertyDescriptors.add(
            // CHECKSTYLE:IGNORE AnonInnerLength: Okay here.
            new ItemPropertyDescriptor(
                ((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
                 getResourceLocator(),
                 // CHECKSTYLE:IGNORE MultipleStringLiterals: Outcome of code generator.
                 getString("_UI_CORELink_from_feature"),
                 getString("_UI_PropertyDescriptor_description", 
                             "_UI_CORELink_from_feature", "_UI_CORELink_type"),
                 CorePackage.Literals.CORE_LINK__FROM,
                 true,
                 false,
                 true,
                 null,
                 null,
                 null) {
                
                @Override
                public Collection<?> getChoiceOfValues(Object object) {
                    COREMapping<Classifier> classifierMapping = (ClassifierMapping) object;
                    
                    // Gather the COREModelExtension of the classifierMapping to gather its COREExternalArtefact
                    COREModelExtension modelExtension = (COREModelExtension) classifierMapping.eContainer();
                    COREExternalArtefact artefact = (COREExternalArtefact) modelExtension.getSource();
                    
                    // Gather all external & internal Classifiers
                    Collection<Classifier> classifiersResult = RAMInterfaceUtil.getAvailableClasses(artefact);
                    classifiersResult.remove(classifierMapping.getFrom());
                    
                    // Check for ImplementationClass and remove them
                    Iterator<Classifier> iterator = classifiersResult.iterator();
                    while (iterator.hasNext()) {
                        Classifier classifier = iterator.next();
                        if (classifier instanceof ImplementationClass) {
                            iterator.remove();
                        }
                    }
                    
                    // Check for cardinalities restrictions
                    Collection<EObject> classifiersFiltered = COREArtefactUtil.filterPossibleMapping(
                            classifiersResult, artefact, classifierMapping);
                    
                    return classifiersFiltered;
                }
                
                @Override
                public IItemLabelProvider getLabelProvider(Object object) {
                    // CHECKSTYLE:IGNORE AnonInnerLength: Okay here.
                    return new IItemLabelProvider() {
                        
                        @Override
                        public String getText(Object object) {
                            StringBuilder result = new StringBuilder();
                            result.append(itemDelegator.getText(object));
                            
                            if (object != null) {
                                @SuppressWarnings("unchecked")
                                COREMapping<Classifier> target = (COREMapping<Classifier>) getTarget();
                                result.append(RAMEditUtil.getReferencedMappingsText(target, (Classifier) object));
                            }
                            
                            return result.toString();
                        }

                        @Override
                        public Object getImage(Object object) {
                            return itemDelegator.getImage(object);
                        }
                    };
                }
                
                @Override
                public void setPropertyValue(Object object, Object value) {
                    MappingController controller = COREControllerFactory.INSTANCE.getMappingController();
                    
                    @SuppressWarnings("unchecked")
                    COREMapping<Classifier> mapping = (COREMapping<Classifier>) object;
                    controller.setFromMapping(mapping, (Classifier) value);
                }
                
            });
    }

    /**
     * This adds a property descriptor for the To Element feature.
     * <!-- begin-user-doc -->
     * @param object the object to add a property descriptor for
     * <!-- end-user-doc -->
     * @generated NOT
     */
    protected void addToPropertyDescriptor(Object object) {
        itemPropertyDescriptors.add(
            // CHECKSTYLE:IGNORE AnonInnerLength: Okay here.
            new ItemPropertyDescriptor(
                ((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
                 getResourceLocator(),
                 getString("_UI_CORELink_to_feature"),
                 getString("_UI_PropertyDescriptor_description", 
                             "_UI_CORELink_to_feature", "_UI_CORELink_type"),
                 CorePackage.Literals.CORE_LINK__TO,
                 true,
                 false,
                 true,
                 null,
                 null,
                 null) {
                @Override
                public Collection<?> getChoiceOfValues(Object object) {
                    COREMapping<Classifier> classifierMapping = (ClassifierMapping) object;
                    
                    // return nothing if the "from" mapping has not been established yet
                    if (classifierMapping.getFrom() == null) {
                        return null;
                    }
                    
                    // Gather the current COREExternalArtefact
                    COREExternalArtefact currentArtefact = 
                            (COREExternalArtefact) classifierMapping.eContainer().eContainer();
                    
                    // Gather all Classifiers
                    Collection<Classifier> classifiersResult = RAMInterfaceUtil.getAvailableClasses(currentArtefact);
                    
                    // Check for ImplementationClass and remove them, if "from" class has some properties
                    Classifier fromClass = classifierMapping.getFrom();
                    if (fromClass.getAssociationEnds().size() == 0
                            && fromClass.getAttributes().size() == 0
                            && fromClass.getOperations().size() == 0) {
                        return classifiersResult;
                    }
                    Iterator<Classifier> iterator = classifiersResult.iterator();
                    while (iterator.hasNext()) {
                        Classifier classifier = iterator.next();
                        if (classifier instanceof ImplementationClass) {
                            iterator.remove();
                        }
                    }
                    return classifiersResult;
                }
                
                @Override
                public void setPropertyValue(Object object, Object value) {
                    
                    EditingDomain editingDomain = getEditingDomain(object);
                    CompoundCommand compoundCommand = new CompoundCommand();
                    EObject eValue = (EObject) value;

                    // if the chosen class is from a different aspect, we need to localize it
                    EObject localValue = RAMReferenceUtil.localizeElement(editingDomain, compoundCommand,
                            ((COREExternalArtefact) (((ClassifierMapping) object).eContainer()
                                    .eContainer())).getRootModelElement(), eValue);
                    compoundCommand.append(SetCommand.create(editingDomain, object,
                            CorePackage.Literals.CORE_LINK__TO, localValue));
                    
                    editingDomain.getCommandStack().execute(compoundCommand);

                    
                }
            });
    }

    /**
	 * This returns ClassifierMapping.gif.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/ClassifierMapping"));
	}

    /**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public String getText(Object object) {
		return getString("_UI_ClassifierMapping_type");
	}

    /**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public void notifyChanged(Notification notification) {
		updateChildren(notification);
		super.notifyChanged(notification);
	}

    /**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.CORE_MAPPING__MAPPINGS,
				 RamFactory.eINSTANCE.createClassifierMapping()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.CORE_MAPPING__MAPPINGS,
				 RamFactory.eINSTANCE.createAttributeMapping()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.CORE_MAPPING__MAPPINGS,
				 RamFactory.eINSTANCE.createOperationMapping()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.CORE_MAPPING__MAPPINGS,
				 RamFactory.eINSTANCE.createParameterMapping()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.CORE_MAPPING__MAPPINGS,
				 RamFactory.eINSTANCE.createEnumMapping()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.CORE_MAPPING__MAPPINGS,
				 RamFactory.eINSTANCE.createEnumLiteralMapping()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.CORE_MAPPING__MAPPINGS,
				 RamFactory.eINSTANCE.createAssociationEndMapping()));
	}

    /**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public ResourceLocator getResourceLocator() {
		return RAMEditPlugin.INSTANCE;
	}

}
