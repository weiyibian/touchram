/**
 */
package ca.mcgill.sel.ram.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;

import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.COREModelExtension;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.controller.COREControllerFactory;
import ca.mcgill.sel.core.controller.MappingController;
import ca.mcgill.sel.core.provider.COREMappingItemProvider;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.core.util.COREModelUtil;
import ca.mcgill.sel.ram.Attribute;
import ca.mcgill.sel.ram.AttributeMapping;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.ClassifierMapping;
import ca.mcgill.sel.ram.RamFactory;
import ca.mcgill.sel.ram.impl.ClassImpl;
import ca.mcgill.sel.ram.provider.util.RAMEditUtil;

/**
 * This is the item provider adapter for a {@link ca.mcgill.sel.ram.AttributeMapping} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class AttributeMappingItemProvider
        extends COREMappingItemProvider {
    /**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public AttributeMappingItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

    /**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

    /**
     * This adds a property descriptor for the To feature.
     * <!-- begin-user-doc -->
     * @param object the object to add a property descriptor for
     * <!-- end-user-doc -->
     * @generated NOT
     */
    protected void addToPropertyDescriptor(Object object) {
        itemPropertyDescriptors.add(
                // CHECKSTYLE:IGNORE AnonInnerLength: Okay here.
                new ItemPropertyDescriptor(
                        ((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
                        getResourceLocator(),
                        getString("_UI_CORELink_to_feature"),
                        getString("_UI_PropertyDescriptor_description", "_UI_CORELink_to_feature",
                                "_UI_CORELink_type"),
                        CorePackage.Literals.CORE_LINK__TO,
                        true,
                        false,
                        true,
                        null,
                        null,
                        null) {

                    @Override
                    public Collection<?> getChoiceOfValues(Object object) {
                        COREMapping<Attribute> attributeMapping = (AttributeMapping) object;
                        
                        // Gather the ClassifierMapping associated
                        COREMapping<Classifier> classifierMapping = (ClassifierMapping) attributeMapping.eContainer();
                       
                        // Gather all the attributes from all mapped elements
                        Collection<Attribute> attributesResult = new ArrayList<>();
                        for (EObject classImpl : COREArtefactUtil.getAllMappedElements(classifierMapping)) {
                            attributesResult.addAll(((ClassImpl) classImpl).getAttributes());
                        }
                        attributesResult.remove(attributeMapping.getFrom());
                        
                        // Gather the Classifier from the "to" relation
                        ClassImpl classifier = (ClassImpl) classifierMapping.getTo();
                        attributesResult.addAll(classifier.getAttributes());
                        
                        // Filter out all elements from attributesResult that are mapped to another element.
                        COREModelUtil.filterMappedElements(attributesResult);
                        
                        // Remove the previously chosen attribute in the attributeMapping from
                        attributesResult.remove(attributeMapping.getFrom());
                        
                        // Return the attributes used by the Classifier
                        return attributesResult;
                    }
                    
                    //@Override
                    //public void setPropertyValue(Object object, Object value) {
                    //    RAMReferenceUtil.setLocalizedPropertyValue(getEditingDomain(object), object, feature, value);
                    //}
                });
    }
    
    /**
     * This adds a property descriptor for the To feature.
     * <!-- begin-user-doc -->
     * @param object the object to add a property descriptor for
     * <!-- end-user-doc -->
     * @generated NOT
     */
    @Override
    protected void addFromPropertyDescriptor(Object object) {
        itemPropertyDescriptors.add(
                // CHECKSTYLE:IGNORE AnonInnerLength: Okay here.
                new ItemPropertyDescriptor(
                        ((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
                        getResourceLocator(),
                        getString("_UI_CORELink_from_feature"),
                        getString("_UI_PropertyDescriptor_description", "_UI_CORELink_from_feature", 
                                    "_UI_CORELink_type"),
                        CorePackage.Literals.CORE_LINK__FROM,
                        true,
                        false,
                        true,
                        null,
                        null,
                        null) {
                    
                    @Override
                    public Collection<?> getChoiceOfValues(Object object) {
                        COREMapping<Attribute> attributeMapping = (AttributeMapping) object;
                        
                        // Gather the ClassifierMapping associated
                        COREMapping<Classifier> classifierMapping = (ClassifierMapping) attributeMapping.eContainer();
                        COREModelExtension modelExtension = (COREModelExtension) classifierMapping.eContainer();
                        COREExternalArtefact artefact = (COREExternalArtefact) modelExtension.getSource();
                       
                        // Gather all the attributes from all mapped elements
                        Collection<Attribute> attributesResult = new ArrayList<>();
                        for (EObject classImpl : COREArtefactUtil.getAllMappedElements(classifierMapping)) {
                            attributesResult.addAll(((ClassImpl) classImpl).getAttributes());
                        }
                        
                        // Filter out all elements from attributesResult that are mapped to another element.
                        COREModelUtil.filterMappedElements(attributesResult);
                        
                        // Check for cardinalities restrictions
                        Collection<EObject> attributesFiltered = 
                                COREArtefactUtil.filterPossibleMapping(attributesResult, artefact, 
                                        attributeMapping);
                        
                        return attributesFiltered;
                    }
                    
                    @Override
                    public IItemLabelProvider getLabelProvider(Object object) {
                        // CHECKSTYLE:IGNORE AnonInnerLength: Okay here.
                        return new IItemLabelProvider() {

                            @Override
                            public String getText(Object object) {
                                
                                StringBuilder result = new StringBuilder();
                                result.append(itemDelegator.getText(object));
                                if (object != null) {
                                    @SuppressWarnings("unchecked")
                                    COREMapping<Attribute> target = (COREMapping<Attribute>) getTarget();
                                    result.append(RAMEditUtil.getReferencedMappingsText(target, (Attribute) object));
                                }
                                
                                return result.toString();
                            }

                            @Override
                            public Object getImage(Object object) {
                                return itemDelegator.getImage(object);
                            }
                        };
                    }
                    
                    @Override
                    public void setPropertyValue(Object object, Object value) {
                        MappingController controller = COREControllerFactory.INSTANCE.getMappingController();
                        
                        @SuppressWarnings("unchecked")
                        COREMapping<Attribute> mapping = (COREMapping<Attribute>) object;
                        controller.setFromMapping(mapping, (Attribute) value);
                    }
                });
    }

    /**
	 * This returns AttributeMapping.gif.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/AttributeMapping"));
	}

    /**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public String getText(Object object) {
		return getString("_UI_AttributeMapping_type");
	}

    /**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public void notifyChanged(Notification notification) {
		updateChildren(notification);
		super.notifyChanged(notification);
	}

    /**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.CORE_MAPPING__MAPPINGS,
				 RamFactory.eINSTANCE.createClassifierMapping()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.CORE_MAPPING__MAPPINGS,
				 RamFactory.eINSTANCE.createAttributeMapping()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.CORE_MAPPING__MAPPINGS,
				 RamFactory.eINSTANCE.createOperationMapping()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.CORE_MAPPING__MAPPINGS,
				 RamFactory.eINSTANCE.createParameterMapping()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.CORE_MAPPING__MAPPINGS,
				 RamFactory.eINSTANCE.createEnumMapping()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.CORE_MAPPING__MAPPINGS,
				 RamFactory.eINSTANCE.createEnumLiteralMapping()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.CORE_MAPPING__MAPPINGS,
				 RamFactory.eINSTANCE.createAssociationEndMapping()));
	}

    /**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public ResourceLocator getResourceLocator() {
		return RAMEditPlugin.INSTANCE;
	}

}
