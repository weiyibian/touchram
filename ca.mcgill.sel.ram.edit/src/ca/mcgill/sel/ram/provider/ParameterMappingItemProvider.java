/**
 */
package ca.mcgill.sel.ram.provider;


import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;

import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.provider.COREMappingItemProvider;
import ca.mcgill.sel.ram.Parameter;
import ca.mcgill.sel.ram.PrimitiveType;
import ca.mcgill.sel.ram.RamFactory;
import ca.mcgill.sel.ram.Type;
import ca.mcgill.sel.ram.impl.ParameterMappingImpl;
import ca.mcgill.sel.ram.controller.util.RAMReferenceUtil;

/**
 * This is the item provider adapter for a {@link ca.mcgill.sel.ram.ParameterMapping} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ParameterMappingItemProvider
    extends COREMappingItemProvider {
    /**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public ParameterMappingItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

    /**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

    /**
     * This adds a property descriptor for the To feature.
     * <!-- begin-user-doc -->
     * @param object the object to add a property descriptor for
     * <!-- end-user-doc -->
     * @generated NOT
     */
    protected void addToPropertyDescriptor(Object object) {
        itemPropertyDescriptors.add(
            new MappingToItemPropertyDescriptor(
                ((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
                 getResourceLocator(),
                 getString("_UI_CORELink_to_feature"),
                 getString("_UI_PropertyDescriptor_description", 
                             "_UI_CORELink_to_feature", "_UI_CORELink_type"),
                 CorePackage.Literals.CORE_LINK__TO,
                 true,
                 false,
                 true,
                 null,
                 null,
                 null) {
                
                @Override
                public void setPropertyValue(Object object, Object value) {
                    RAMReferenceUtil.setLocalizedPropertyValue(getEditingDomain(object), object, feature, value);
                }
                
                @Override
                public Collection<?> getChoiceOfValues(Object object) {
                    
                    Collection<?> choiceOfValues = super.getChoiceOfValues(object);
                    ParameterMappingImpl paramMapping =  (ParameterMappingImpl) object;
                    Parameter from = paramMapping.basicGetFrom();
                    Type typeFrom = from.getType();
                    // If typeFrom is a Primitive type, then check for a mismatched typeTo 
                    // If (typeFrom == typeTo) ==> (int == String), then you remove typeTo from the proposition list
                    if (typeFrom instanceof PrimitiveType) {
                        for (Iterator<?> iterator = choiceOfValues.iterator(); iterator.hasNext(); ) {
                            Parameter paramTo = (Parameter) iterator.next();                        
                            if (paramTo != null) {
                                Type typeTo = paramTo.getType();
                                if (!typeFrom.eClass().equals(typeTo.eClass())) {
                                    iterator.remove();
                                }
                            }
                        }
                    }
                    return choiceOfValues;
                }
            });
    }

    /**
	 * This returns ParameterMapping.gif.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/ParameterMapping"));
	}

    /**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public String getText(Object object) {
		return getString("_UI_ParameterMapping_type");
	}

    /**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public void notifyChanged(Notification notification) {
		updateChildren(notification);
		super.notifyChanged(notification);
	}

    /**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.CORE_MAPPING__MAPPINGS,
				 RamFactory.eINSTANCE.createClassifierMapping()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.CORE_MAPPING__MAPPINGS,
				 RamFactory.eINSTANCE.createAttributeMapping()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.CORE_MAPPING__MAPPINGS,
				 RamFactory.eINSTANCE.createOperationMapping()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.CORE_MAPPING__MAPPINGS,
				 RamFactory.eINSTANCE.createParameterMapping()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.CORE_MAPPING__MAPPINGS,
				 RamFactory.eINSTANCE.createEnumMapping()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.CORE_MAPPING__MAPPINGS,
				 RamFactory.eINSTANCE.createEnumLiteralMapping()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.CORE_MAPPING__MAPPINGS,
				 RamFactory.eINSTANCE.createAssociationEndMapping()));
	}

    /**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public ResourceLocator getResourceLocator() {
		return RAMEditPlugin.INSTANCE;
	}

}
