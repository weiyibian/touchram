/**
 */
package ca.mcgill.sel.classdiagram;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CD Enum</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.classdiagram.CDEnum#getLiterals <em>Literals</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.classdiagram.CdmPackage#getCDEnum()
 * @model
 * @generated
 */
public interface CDEnum extends PrimitiveType {
    /**
     * Returns the value of the '<em><b>Literals</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.classdiagram.CDEnumLiteral}.
     * It is bidirectional and its opposite is '{@link ca.mcgill.sel.classdiagram.CDEnumLiteral#getEnum <em>Enum</em>}'.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Literals</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Literals</em>' containment reference list.
     * @see ca.mcgill.sel.classdiagram.CdmPackage#getCDEnum_Literals()
     * @see ca.mcgill.sel.classdiagram.CDEnumLiteral#getEnum
     * @model opposite="enum" containment="true" required="true"
     * @generated
     */
    EList<CDEnumLiteral> getLiterals();

} // CDEnum
