/**
 */
package ca.mcgill.sel.classdiagram;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Association</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.classdiagram.Association#getEnds <em>Ends</em>}</li>
 *   <li>{@link ca.mcgill.sel.classdiagram.Association#getAssociationClass <em>Association Class</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.classdiagram.CdmPackage#getAssociation()
 * @model
 * @generated
 */
public interface Association extends NamedElement {
    /**
     * Returns the value of the '<em><b>Ends</b></em>' reference list.
     * The list contents are of type {@link ca.mcgill.sel.classdiagram.AssociationEnd}.
     * It is bidirectional and its opposite is '{@link ca.mcgill.sel.classdiagram.AssociationEnd#getAssoc <em>Assoc</em>}'.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Ends</em>' reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Ends</em>' reference list.
     * @see ca.mcgill.sel.classdiagram.CdmPackage#getAssociation_Ends()
     * @see ca.mcgill.sel.classdiagram.AssociationEnd#getAssoc
     * @model opposite="assoc" lower="2"
     * @generated
     */
    EList<AssociationEnd> getEnds();

    /**
     * Returns the value of the '<em><b>Association Class</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Association Class</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Association Class</em>' reference.
     * @see #setAssociationClass(ca.mcgill.sel.classdiagram.Class)
     * @see ca.mcgill.sel.classdiagram.CdmPackage#getAssociation_AssociationClass()
     * @model
     * @generated
     */
    ca.mcgill.sel.classdiagram.Class getAssociationClass();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.classdiagram.Association#getAssociationClass <em>Association Class</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Association Class</em>' reference.
     * @see #getAssociationClass()
     * @generated
     */
    void setAssociationClass(ca.mcgill.sel.classdiagram.Class value);

} // Association
