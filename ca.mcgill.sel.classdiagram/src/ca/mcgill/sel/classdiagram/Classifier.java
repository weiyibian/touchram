/**
 */
package ca.mcgill.sel.classdiagram;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Classifier</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.classdiagram.Classifier#getSuperTypes <em>Super Types</em>}</li>
 *   <li>{@link ca.mcgill.sel.classdiagram.Classifier#isDataType <em>Data Type</em>}</li>
 *   <li>{@link ca.mcgill.sel.classdiagram.Classifier#isAbstract <em>Abstract</em>}</li>
 *   <li>{@link ca.mcgill.sel.classdiagram.Classifier#getVisibility <em>Visibility</em>}</li>
 *   <li>{@link ca.mcgill.sel.classdiagram.Classifier#getOperations <em>Operations</em>}</li>
 *   <li>{@link ca.mcgill.sel.classdiagram.Classifier#getTypeParameters <em>Type Parameters</em>}</li>
 *   <li>{@link ca.mcgill.sel.classdiagram.Classifier#getAssociationEnds <em>Association Ends</em>}</li>
 *   <li>{@link ca.mcgill.sel.classdiagram.Classifier#getAttributes <em>Attributes</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.classdiagram.CdmPackage#getClassifier()
 * @model abstract="true"
 * @generated
 */
public interface Classifier extends ObjectType {
    /**
     * Returns the value of the '<em><b>Super Types</b></em>' reference list.
     * The list contents are of type {@link ca.mcgill.sel.classdiagram.Classifier}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Super Types</em>' reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Super Types</em>' reference list.
     * @see ca.mcgill.sel.classdiagram.CdmPackage#getClassifier_SuperTypes()
     * @model
     * @generated
     */
    EList<Classifier> getSuperTypes();

    /**
     * Returns the value of the '<em><b>Data Type</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Data Type</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Data Type</em>' attribute.
     * @see #setDataType(boolean)
     * @see ca.mcgill.sel.classdiagram.CdmPackage#getClassifier_DataType()
     * @model required="true"
     * @generated
     */
    boolean isDataType();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.classdiagram.Classifier#isDataType <em>Data Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Data Type</em>' attribute.
     * @see #isDataType()
     * @generated
     */
    void setDataType(boolean value);

    /**
     * Returns the value of the '<em><b>Abstract</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Abstract</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Abstract</em>' attribute.
     * @see #setAbstract(boolean)
     * @see ca.mcgill.sel.classdiagram.CdmPackage#getClassifier_Abstract()
     * @model required="true"
     * @generated
     */
    boolean isAbstract();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.classdiagram.Classifier#isAbstract <em>Abstract</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Abstract</em>' attribute.
     * @see #isAbstract()
     * @generated
     */
    void setAbstract(boolean value);

    /**
     * Returns the value of the '<em><b>Visibility</b></em>' attribute.
     * The default value is <code>"package"</code>.
     * The literals are from the enumeration {@link ca.mcgill.sel.classdiagram.VisibilityType}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Visibility</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Visibility</em>' attribute.
     * @see ca.mcgill.sel.classdiagram.VisibilityType
     * @see #setVisibility(VisibilityType)
     * @see ca.mcgill.sel.classdiagram.CdmPackage#getClassifier_Visibility()
     * @model default="package"
     * @generated
     */
    VisibilityType getVisibility();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.classdiagram.Classifier#getVisibility <em>Visibility</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Visibility</em>' attribute.
     * @see ca.mcgill.sel.classdiagram.VisibilityType
     * @see #getVisibility()
     * @generated
     */
    void setVisibility(VisibilityType value);

    /**
     * Returns the value of the '<em><b>Operations</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.classdiagram.Operation}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Operations</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Operations</em>' containment reference list.
     * @see ca.mcgill.sel.classdiagram.CdmPackage#getClassifier_Operations()
     * @model containment="true"
     * @generated
     */
    EList<Operation> getOperations();

    /**
     * Returns the value of the '<em><b>Type Parameters</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.classdiagram.TypeParameter}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Type Parameters</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Type Parameters</em>' containment reference list.
     * @see ca.mcgill.sel.classdiagram.CdmPackage#getClassifier_TypeParameters()
     * @model containment="true"
     * @generated
     */
    EList<TypeParameter> getTypeParameters();

    /**
     * Returns the value of the '<em><b>Association Ends</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.classdiagram.AssociationEnd}.
     * It is bidirectional and its opposite is '{@link ca.mcgill.sel.classdiagram.AssociationEnd#getClassifier <em>Classifier</em>}'.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Association Ends</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Association Ends</em>' containment reference list.
     * @see ca.mcgill.sel.classdiagram.CdmPackage#getClassifier_AssociationEnds()
     * @see ca.mcgill.sel.classdiagram.AssociationEnd#getClassifier
     * @model opposite="classifier" containment="true"
     * @generated
     */
    EList<AssociationEnd> getAssociationEnds();

    /**
     * Returns the value of the '<em><b>Attributes</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.classdiagram.Attribute}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Attributes</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Attributes</em>' containment reference list.
     * @see ca.mcgill.sel.classdiagram.CdmPackage#getClassifier_Attributes()
     * @model containment="true"
     * @generated
     */
    EList<Attribute> getAttributes();

} // Classifier
