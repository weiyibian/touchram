/**
 */
package ca.mcgill.sel.classdiagram;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.mcgill.sel.classdiagram.CdmPackage#getParameter()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='notVoid'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot notVoid='Tuple {\n\tmessage : String = \'The type of the parameter may not be void\',\n\tstatus : Boolean = not self.type.oclIsTypeOf(CDVoid)\n}.status'"
 * @generated
 */
public interface Parameter extends TypedElement {
} // Parameter
