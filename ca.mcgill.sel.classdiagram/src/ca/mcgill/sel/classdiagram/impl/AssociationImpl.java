/**
 */
package ca.mcgill.sel.classdiagram.impl;

import ca.mcgill.sel.classdiagram.Association;
import ca.mcgill.sel.classdiagram.AssociationEnd;
import ca.mcgill.sel.classdiagram.CdmPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Association</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.classdiagram.impl.AssociationImpl#getEnds <em>Ends</em>}</li>
 *   <li>{@link ca.mcgill.sel.classdiagram.impl.AssociationImpl#getAssociationClass <em>Association Class</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AssociationImpl extends NamedElementImpl implements Association {
    /**
     * The cached value of the '{@link #getEnds() <em>Ends</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getEnds()
     * @generated
     * @ordered
     */
    protected EList<AssociationEnd> ends;

    /**
     * The cached value of the '{@link #getAssociationClass() <em>Association Class</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getAssociationClass()
     * @generated
     * @ordered
     */
    protected ca.mcgill.sel.classdiagram.Class associationClass;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected AssociationImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return CdmPackage.Literals.ASSOCIATION;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList<AssociationEnd> getEnds() {
        if (ends == null) {
            ends = new EObjectWithInverseResolvingEList<AssociationEnd>(AssociationEnd.class, this, CdmPackage.ASSOCIATION__ENDS, CdmPackage.ASSOCIATION_END__ASSOC);
        }
        return ends;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ca.mcgill.sel.classdiagram.Class getAssociationClass() {
        if (associationClass != null && associationClass.eIsProxy()) {
            InternalEObject oldAssociationClass = (InternalEObject)associationClass;
            associationClass = (ca.mcgill.sel.classdiagram.Class)eResolveProxy(oldAssociationClass);
            if (associationClass != oldAssociationClass) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, CdmPackage.ASSOCIATION__ASSOCIATION_CLASS, oldAssociationClass, associationClass));
            }
        }
        return associationClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public ca.mcgill.sel.classdiagram.Class basicGetAssociationClass() {
        return associationClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setAssociationClass(ca.mcgill.sel.classdiagram.Class newAssociationClass) {
        ca.mcgill.sel.classdiagram.Class oldAssociationClass = associationClass;
        associationClass = newAssociationClass;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CdmPackage.ASSOCIATION__ASSOCIATION_CLASS, oldAssociationClass, associationClass));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case CdmPackage.ASSOCIATION__ENDS:
                return ((InternalEList<InternalEObject>)(InternalEList<?>)getEnds()).basicAdd(otherEnd, msgs);
        }
        return super.eInverseAdd(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case CdmPackage.ASSOCIATION__ENDS:
                return ((InternalEList<?>)getEnds()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case CdmPackage.ASSOCIATION__ENDS:
                return getEnds();
            case CdmPackage.ASSOCIATION__ASSOCIATION_CLASS:
                if (resolve) return getAssociationClass();
                return basicGetAssociationClass();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case CdmPackage.ASSOCIATION__ENDS:
                getEnds().clear();
                getEnds().addAll((Collection<? extends AssociationEnd>)newValue);
                return;
            case CdmPackage.ASSOCIATION__ASSOCIATION_CLASS:
                setAssociationClass((ca.mcgill.sel.classdiagram.Class)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case CdmPackage.ASSOCIATION__ENDS:
                getEnds().clear();
                return;
            case CdmPackage.ASSOCIATION__ASSOCIATION_CLASS:
                setAssociationClass((ca.mcgill.sel.classdiagram.Class)null);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case CdmPackage.ASSOCIATION__ENDS:
                return ends != null && !ends.isEmpty();
            case CdmPackage.ASSOCIATION__ASSOCIATION_CLASS:
                return associationClass != null;
        }
        return super.eIsSet(featureID);
    }

} //AssociationImpl
