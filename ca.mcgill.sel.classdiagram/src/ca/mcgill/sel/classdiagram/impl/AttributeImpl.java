/**
 */
package ca.mcgill.sel.classdiagram.impl;

import ca.mcgill.sel.classdiagram.Attribute;
import ca.mcgill.sel.classdiagram.CdmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AttributeImpl extends StructuralFeatureImpl implements Attribute {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected AttributeImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return CdmPackage.Literals.ATTRIBUTE;
    }

} //AttributeImpl
