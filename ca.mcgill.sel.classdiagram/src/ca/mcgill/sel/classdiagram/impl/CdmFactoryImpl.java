/**
 */
package ca.mcgill.sel.classdiagram.impl;

import ca.mcgill.sel.classdiagram.Association;
import ca.mcgill.sel.classdiagram.AssociationEnd;
import ca.mcgill.sel.classdiagram.Attribute;
import ca.mcgill.sel.classdiagram.CDAny;
import ca.mcgill.sel.classdiagram.CDArray;
import ca.mcgill.sel.classdiagram.CDBoolean;
import ca.mcgill.sel.classdiagram.CDByte;
import ca.mcgill.sel.classdiagram.CDChar;
import ca.mcgill.sel.classdiagram.CDDouble;
import ca.mcgill.sel.classdiagram.CDEnum;
import ca.mcgill.sel.classdiagram.CDEnumLiteral;
import ca.mcgill.sel.classdiagram.CDFloat;
import ca.mcgill.sel.classdiagram.CDInt;
import ca.mcgill.sel.classdiagram.CDLong;
import ca.mcgill.sel.classdiagram.CDSequence;
import ca.mcgill.sel.classdiagram.CDSet;
import ca.mcgill.sel.classdiagram.CDString;
import ca.mcgill.sel.classdiagram.CDVoid;
import ca.mcgill.sel.classdiagram.CdmFactory;
import ca.mcgill.sel.classdiagram.CdmPackage;
import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.classdiagram.ImplementationClass;
import ca.mcgill.sel.classdiagram.Layout;
import ca.mcgill.sel.classdiagram.LayoutElement;
import ca.mcgill.sel.classdiagram.Note;
import ca.mcgill.sel.classdiagram.Operation;
import ca.mcgill.sel.classdiagram.OperationType;
import ca.mcgill.sel.classdiagram.Parameter;
import ca.mcgill.sel.classdiagram.ReferenceType;
import ca.mcgill.sel.classdiagram.TypeParameter;
import ca.mcgill.sel.classdiagram.VisibilityType;

import java.util.Map;

import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CdmFactoryImpl extends EFactoryImpl implements CdmFactory {
    /**
     * Creates the default factory implementation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public static CdmFactory init() {
        try {
            CdmFactory theCdmFactory = (CdmFactory)EPackage.Registry.INSTANCE.getEFactory(CdmPackage.eNS_URI);
            if (theCdmFactory != null) {
                return theCdmFactory;
            }
        }
        catch (Exception exception) {
            EcorePlugin.INSTANCE.log(exception);
        }
        return new CdmFactoryImpl();
    }

    /**
     * Creates an instance of the factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public CdmFactoryImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EObject create(EClass eClass) {
        switch (eClass.getClassifierID()) {
            case CdmPackage.PARAMETER: return createParameter();
            case CdmPackage.ATTRIBUTE: return createAttribute();
            case CdmPackage.OPERATION: return createOperation();
            case CdmPackage.CLASS: return createClass();
            case CdmPackage.TYPE_PARAMETER: return createTypeParameter();
            case CdmPackage.ASSOCIATION: return createAssociation();
            case CdmPackage.ASSOCIATION_END: return createAssociationEnd();
            case CdmPackage.CLASS_DIAGRAM: return createClassDiagram();
            case CdmPackage.IMPLEMENTATION_CLASS: return createImplementationClass();
            case CdmPackage.NOTE: return createNote();
            case CdmPackage.ELEMENT_MAP: return (EObject)createElementMap();
            case CdmPackage.LAYOUT: return createLayout();
            case CdmPackage.LAYOUT_ELEMENT: return createLayoutElement();
            case CdmPackage.CONTAINER_MAP: return (EObject)createContainerMap();
            case CdmPackage.CD_BOOLEAN: return createCDBoolean();
            case CdmPackage.CD_DOUBLE: return createCDDouble();
            case CdmPackage.CD_INT: return createCDInt();
            case CdmPackage.CD_LONG: return createCDLong();
            case CdmPackage.CD_STRING: return createCDString();
            case CdmPackage.CD_BYTE: return createCDByte();
            case CdmPackage.CD_FLOAT: return createCDFloat();
            case CdmPackage.CD_ARRAY: return createCDArray();
            case CdmPackage.CD_CHAR: return createCDChar();
            case CdmPackage.CD_ENUM: return createCDEnum();
            case CdmPackage.CD_ENUM_LITERAL: return createCDEnumLiteral();
            case CdmPackage.CD_ANY: return createCDAny();
            case CdmPackage.CD_VOID: return createCDVoid();
            case CdmPackage.CD_SET: return createCDSet();
            case CdmPackage.CD_SEQUENCE: return createCDSequence();
            default:
                throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object createFromString(EDataType eDataType, String initialValue) {
        switch (eDataType.getClassifierID()) {
            case CdmPackage.VISIBILITY_TYPE:
                return createVisibilityTypeFromString(eDataType, initialValue);
            case CdmPackage.OPERATION_TYPE:
                return createOperationTypeFromString(eDataType, initialValue);
            case CdmPackage.REFERENCE_TYPE:
                return createReferenceTypeFromString(eDataType, initialValue);
            default:
                throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String convertToString(EDataType eDataType, Object instanceValue) {
        switch (eDataType.getClassifierID()) {
            case CdmPackage.VISIBILITY_TYPE:
                return convertVisibilityTypeToString(eDataType, instanceValue);
            case CdmPackage.OPERATION_TYPE:
                return convertOperationTypeToString(eDataType, instanceValue);
            case CdmPackage.REFERENCE_TYPE:
                return convertReferenceTypeToString(eDataType, instanceValue);
            default:
                throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Parameter createParameter() {
        ParameterImpl parameter = new ParameterImpl();
        return parameter;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Attribute createAttribute() {
        AttributeImpl attribute = new AttributeImpl();
        return attribute;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Operation createOperation() {
        OperationImpl operation = new OperationImpl();
        return operation;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ca.mcgill.sel.classdiagram.Class createClass() {
        ClassImpl class_ = new ClassImpl();
        return class_;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public TypeParameter createTypeParameter() {
        TypeParameterImpl typeParameter = new TypeParameterImpl();
        return typeParameter;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Association createAssociation() {
        AssociationImpl association = new AssociationImpl();
        return association;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public AssociationEnd createAssociationEnd() {
        AssociationEndImpl associationEnd = new AssociationEndImpl();
        return associationEnd;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ClassDiagram createClassDiagram() {
        ClassDiagramImpl classDiagram = new ClassDiagramImpl();
        return classDiagram;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ImplementationClass createImplementationClass() {
        ImplementationClassImpl implementationClass = new ImplementationClassImpl();
        return implementationClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Note createNote() {
        NoteImpl note = new NoteImpl();
        return note;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public Map.Entry<EObject, LayoutElement> createElementMap() {
        ElementMapImpl elementMap = new ElementMapImpl();
        return elementMap;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Layout createLayout() {
        LayoutImpl layout = new LayoutImpl();
        return layout;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public LayoutElement createLayoutElement() {
        LayoutElementImpl layoutElement = new LayoutElementImpl();
        return layoutElement;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public Map.Entry<EObject, EMap<EObject, LayoutElement>> createContainerMap() {
        ContainerMapImpl containerMap = new ContainerMapImpl();
        return containerMap;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public CDBoolean createCDBoolean() {
        CDBooleanImpl cdBoolean = new CDBooleanImpl();
        return cdBoolean;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public CDDouble createCDDouble() {
        CDDoubleImpl cdDouble = new CDDoubleImpl();
        return cdDouble;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public CDInt createCDInt() {
        CDIntImpl cdInt = new CDIntImpl();
        return cdInt;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public CDLong createCDLong() {
        CDLongImpl cdLong = new CDLongImpl();
        return cdLong;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public CDString createCDString() {
        CDStringImpl cdString = new CDStringImpl();
        return cdString;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public CDByte createCDByte() {
        CDByteImpl cdByte = new CDByteImpl();
        return cdByte;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public CDFloat createCDFloat() {
        CDFloatImpl cdFloat = new CDFloatImpl();
        return cdFloat;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public CDArray createCDArray() {
        CDArrayImpl cdArray = new CDArrayImpl();
        return cdArray;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public CDChar createCDChar() {
        CDCharImpl cdChar = new CDCharImpl();
        return cdChar;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public CDEnum createCDEnum() {
        CDEnumImpl cdEnum = new CDEnumImpl();
        return cdEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public CDEnumLiteral createCDEnumLiteral() {
        CDEnumLiteralImpl cdEnumLiteral = new CDEnumLiteralImpl();
        return cdEnumLiteral;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public CDAny createCDAny() {
        CDAnyImpl cdAny = new CDAnyImpl();
        return cdAny;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public CDVoid createCDVoid() {
        CDVoidImpl cdVoid = new CDVoidImpl();
        return cdVoid;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public CDSet createCDSet() {
        CDSetImpl cdSet = new CDSetImpl();
        return cdSet;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public CDSequence createCDSequence() {
        CDSequenceImpl cdSequence = new CDSequenceImpl();
        return cdSequence;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public VisibilityType createVisibilityTypeFromString(EDataType eDataType, String initialValue) {
        VisibilityType result = VisibilityType.get(initialValue);
        if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertVisibilityTypeToString(EDataType eDataType, Object instanceValue) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public OperationType createOperationTypeFromString(EDataType eDataType, String initialValue) {
        OperationType result = OperationType.get(initialValue);
        if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertOperationTypeToString(EDataType eDataType, Object instanceValue) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public ReferenceType createReferenceTypeFromString(EDataType eDataType, String initialValue) {
        ReferenceType result = ReferenceType.get(initialValue);
        if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertReferenceTypeToString(EDataType eDataType, Object instanceValue) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public CdmPackage getCdmPackage() {
        return (CdmPackage)getEPackage();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @deprecated
     * @generated
     */
    @Deprecated
    public static CdmPackage getPackage() {
        return CdmPackage.eINSTANCE;
    }

} //CdmFactoryImpl
