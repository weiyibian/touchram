/**
 */
package ca.mcgill.sel.classdiagram.impl;

import ca.mcgill.sel.classdiagram.CdmPackage;
import ca.mcgill.sel.classdiagram.Operation;
import ca.mcgill.sel.classdiagram.OperationType;
import ca.mcgill.sel.classdiagram.Parameter;
import ca.mcgill.sel.classdiagram.Type;
import ca.mcgill.sel.classdiagram.VisibilityType;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.classdiagram.impl.OperationImpl#isAbstract <em>Abstract</em>}</li>
 *   <li>{@link ca.mcgill.sel.classdiagram.impl.OperationImpl#getVisibility <em>Visibility</em>}</li>
 *   <li>{@link ca.mcgill.sel.classdiagram.impl.OperationImpl#isStatic <em>Static</em>}</li>
 *   <li>{@link ca.mcgill.sel.classdiagram.impl.OperationImpl#getOperationType <em>Operation Type</em>}</li>
 *   <li>{@link ca.mcgill.sel.classdiagram.impl.OperationImpl#getReturnType <em>Return Type</em>}</li>
 *   <li>{@link ca.mcgill.sel.classdiagram.impl.OperationImpl#getParameters <em>Parameters</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OperationImpl extends NamedElementImpl implements Operation {
    /**
     * The default value of the '{@link #isAbstract() <em>Abstract</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #isAbstract()
     * @generated
     * @ordered
     */
    protected static final boolean ABSTRACT_EDEFAULT = false;

    /**
     * The cached value of the '{@link #isAbstract() <em>Abstract</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #isAbstract()
     * @generated
     * @ordered
     */
    protected boolean abstract_ = ABSTRACT_EDEFAULT;

    /**
     * The default value of the '{@link #getVisibility() <em>Visibility</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getVisibility()
     * @generated
     * @ordered
     */
    protected static final VisibilityType VISIBILITY_EDEFAULT = VisibilityType.PUBLIC;

    /**
     * The cached value of the '{@link #getVisibility() <em>Visibility</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getVisibility()
     * @generated
     * @ordered
     */
    protected VisibilityType visibility = VISIBILITY_EDEFAULT;

    /**
     * The default value of the '{@link #isStatic() <em>Static</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #isStatic()
     * @generated
     * @ordered
     */
    protected static final boolean STATIC_EDEFAULT = false;

    /**
     * The cached value of the '{@link #isStatic() <em>Static</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #isStatic()
     * @generated
     * @ordered
     */
    protected boolean static_ = STATIC_EDEFAULT;

    /**
     * The default value of the '{@link #getOperationType() <em>Operation Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getOperationType()
     * @generated
     * @ordered
     */
    protected static final OperationType OPERATION_TYPE_EDEFAULT = OperationType.NORMAL;

    /**
     * The cached value of the '{@link #getOperationType() <em>Operation Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getOperationType()
     * @generated
     * @ordered
     */
    protected OperationType operationType = OPERATION_TYPE_EDEFAULT;

    /**
     * The cached value of the '{@link #getReturnType() <em>Return Type</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getReturnType()
     * @generated
     * @ordered
     */
    protected Type returnType;

    /**
     * The cached value of the '{@link #getParameters() <em>Parameters</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getParameters()
     * @generated
     * @ordered
     */
    protected EList<Parameter> parameters;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected OperationImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return CdmPackage.Literals.OPERATION;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean isAbstract() {
        return abstract_;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setAbstract(boolean newAbstract) {
        boolean oldAbstract = abstract_;
        abstract_ = newAbstract;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CdmPackage.OPERATION__ABSTRACT, oldAbstract, abstract_));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public VisibilityType getVisibility() {
        return visibility;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setVisibility(VisibilityType newVisibility) {
        VisibilityType oldVisibility = visibility;
        visibility = newVisibility == null ? VISIBILITY_EDEFAULT : newVisibility;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CdmPackage.OPERATION__VISIBILITY, oldVisibility, visibility));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean isStatic() {
        return static_;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setStatic(boolean newStatic) {
        boolean oldStatic = static_;
        static_ = newStatic;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CdmPackage.OPERATION__STATIC, oldStatic, static_));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public OperationType getOperationType() {
        return operationType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setOperationType(OperationType newOperationType) {
        OperationType oldOperationType = operationType;
        operationType = newOperationType == null ? OPERATION_TYPE_EDEFAULT : newOperationType;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CdmPackage.OPERATION__OPERATION_TYPE, oldOperationType, operationType));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Type getReturnType() {
        if (returnType != null && returnType.eIsProxy()) {
            InternalEObject oldReturnType = (InternalEObject)returnType;
            returnType = (Type)eResolveProxy(oldReturnType);
            if (returnType != oldReturnType) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, CdmPackage.OPERATION__RETURN_TYPE, oldReturnType, returnType));
            }
        }
        return returnType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public Type basicGetReturnType() {
        return returnType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setReturnType(Type newReturnType) {
        Type oldReturnType = returnType;
        returnType = newReturnType;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CdmPackage.OPERATION__RETURN_TYPE, oldReturnType, returnType));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList<Parameter> getParameters() {
        if (parameters == null) {
            parameters = new EObjectContainmentEList<Parameter>(Parameter.class, this, CdmPackage.OPERATION__PARAMETERS);
        }
        return parameters;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case CdmPackage.OPERATION__PARAMETERS:
                return ((InternalEList<?>)getParameters()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case CdmPackage.OPERATION__ABSTRACT:
                return isAbstract();
            case CdmPackage.OPERATION__VISIBILITY:
                return getVisibility();
            case CdmPackage.OPERATION__STATIC:
                return isStatic();
            case CdmPackage.OPERATION__OPERATION_TYPE:
                return getOperationType();
            case CdmPackage.OPERATION__RETURN_TYPE:
                if (resolve) return getReturnType();
                return basicGetReturnType();
            case CdmPackage.OPERATION__PARAMETERS:
                return getParameters();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case CdmPackage.OPERATION__ABSTRACT:
                setAbstract((Boolean)newValue);
                return;
            case CdmPackage.OPERATION__VISIBILITY:
                setVisibility((VisibilityType)newValue);
                return;
            case CdmPackage.OPERATION__STATIC:
                setStatic((Boolean)newValue);
                return;
            case CdmPackage.OPERATION__OPERATION_TYPE:
                setOperationType((OperationType)newValue);
                return;
            case CdmPackage.OPERATION__RETURN_TYPE:
                setReturnType((Type)newValue);
                return;
            case CdmPackage.OPERATION__PARAMETERS:
                getParameters().clear();
                getParameters().addAll((Collection<? extends Parameter>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case CdmPackage.OPERATION__ABSTRACT:
                setAbstract(ABSTRACT_EDEFAULT);
                return;
            case CdmPackage.OPERATION__VISIBILITY:
                setVisibility(VISIBILITY_EDEFAULT);
                return;
            case CdmPackage.OPERATION__STATIC:
                setStatic(STATIC_EDEFAULT);
                return;
            case CdmPackage.OPERATION__OPERATION_TYPE:
                setOperationType(OPERATION_TYPE_EDEFAULT);
                return;
            case CdmPackage.OPERATION__RETURN_TYPE:
                setReturnType((Type)null);
                return;
            case CdmPackage.OPERATION__PARAMETERS:
                getParameters().clear();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case CdmPackage.OPERATION__ABSTRACT:
                return abstract_ != ABSTRACT_EDEFAULT;
            case CdmPackage.OPERATION__VISIBILITY:
                return visibility != VISIBILITY_EDEFAULT;
            case CdmPackage.OPERATION__STATIC:
                return static_ != STATIC_EDEFAULT;
            case CdmPackage.OPERATION__OPERATION_TYPE:
                return operationType != OPERATION_TYPE_EDEFAULT;
            case CdmPackage.OPERATION__RETURN_TYPE:
                return returnType != null;
            case CdmPackage.OPERATION__PARAMETERS:
                return parameters != null && !parameters.isEmpty();
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (abstract: ");
        result.append(abstract_);
        result.append(", visibility: ");
        result.append(visibility);
        result.append(", static: ");
        result.append(static_);
        result.append(", operationType: ");
        result.append(operationType);
        result.append(')');
        return result.toString();
    }

} //OperationImpl
