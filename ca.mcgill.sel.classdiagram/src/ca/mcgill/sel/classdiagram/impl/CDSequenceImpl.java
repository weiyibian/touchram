/**
 */
package ca.mcgill.sel.classdiagram.impl;

import ca.mcgill.sel.classdiagram.CDSequence;
import ca.mcgill.sel.classdiagram.CdmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CD Sequence</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CDSequenceImpl extends CDCollectionImpl implements CDSequence {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected CDSequenceImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return CdmPackage.Literals.CD_SEQUENCE;
    }

} //CDSequenceImpl
