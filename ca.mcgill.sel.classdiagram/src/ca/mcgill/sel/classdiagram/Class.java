/**
 */
package ca.mcgill.sel.classdiagram;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.mcgill.sel.classdiagram.CdmPackage#getClass_()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='notSelfSuperType'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot notSelfSuperType='Tuple {\n\tmessage : String = \'A class may not be it\\\'s own supertype\',\n\tstatus : Boolean = not self.superTypes-&gt;includes(self)\n}.status'"
 * @generated
 */
public interface Class extends Classifier {
} // Class
