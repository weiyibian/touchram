package ca.mcgill.sel.classdiagram.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.core.util.COREInterfaceUtil;
import ca.mcgill.sel.core.util.COREModelUtil;

import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.classdiagram.Classifier;
import ca.mcgill.sel.classdiagram.ObjectType;
import ca.mcgill.sel.classdiagram.PrimitiveType;
import ca.mcgill.sel.classdiagram.CDArray;
import ca.mcgill.sel.classdiagram.CDEnum;
import ca.mcgill.sel.classdiagram.CDEnumLiteral;
import ca.mcgill.sel.classdiagram.CdmPackage;

/**
 * Utility class with helper methods to retrieve available properties of a given model.
 * This reflects the available properties as per usage and customization defined by CORE.
 * 
 * @author mschoettle
 * @author yhattab
 */
public final class CdmInterfaceUtil {

    /**
     * Creates a new instance.
     */
    private CdmInterfaceUtil() {

    }

    /**
     * Returns all primitive types of the model the object is contained in.
     * This includes enums and arrays of primitive types.
     * 
     * @param currentObject the object contained in a model
     * @return the collection of primitive types of the model
     */
    public static Collection<PrimitiveType> getAvailablePrimitiveTypes(EObject currentObject) {
        Collection<PrimitiveType> result = new ArrayList<>();
        ClassDiagram cd = getClassDiagram(currentObject);

        // Collect all primitive types, except arrays of non-primitive types.
        Collection<PrimitiveType> primitiveTypes =
                EcoreUtil.getObjectsByType(cd.getTypes(), CdmPackage.Literals.PRIMITIVE_TYPE);

        for (PrimitiveType type : primitiveTypes) {
            // Ignore arrays with a non-primitive type.
            // Add enums through respective method below to include those from extended models.
            if (type instanceof CDArray
                    && !(((CDArray) type).getType() instanceof PrimitiveType)
                    || type instanceof CDEnum) {
                continue;
            }

            result.add(type);
        }

        result.addAll(getAvailableEnums(cd));

        return result;
    }

    /**
     * Returns all enums of the model the object is contained in as well as extended models.
     * 
     * @param currentObject the object contained in a model
     * @return the collection of enums
     */
    public static Collection<CDEnum> getAvailableEnums(EObject currentObject) {
        Collection<CDEnum> result = getAvailableExternalEnums(currentObject);

        //add the enums of the current model
        COREExternalArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(currentObject);
        
        ClassDiagram cd = (ClassDiagram) artefact.getRootModelElement();
        result.addAll(EcoreUtil.getObjectsByType(cd.getTypes(), CdmPackage.Literals.CD_ENUM));

        return result;
    }

    /**
     * Returns all enums of extended and reused models.
     * 
     * @param currentObject the object contained in a model
     * @return the collection of enums
     */
    public static Collection<CDEnum> getAvailableExternalEnums(EObject currentObject) {

        // Get the COREExternalArtefact that potentially contains extensions and reuses, and then get
        // all the COREExternalArtefacts of the extended models
        COREExternalArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(currentObject);
        Collection<COREExternalArtefact> extendedArtefacts = COREModelUtil.getExtendedArtefacts(artefact);
        
        Collection<CDEnum> result = new ArrayList<>();
        
        // From every extended artefact, gather the Enums 
        for (COREExternalArtefact externalArtefact : extendedArtefacts) {
            ClassDiagram cd = (ClassDiagram) externalArtefact.getRootModelElement();
            Collection<CDEnum> enums =
                    EcoreUtil.getObjectsByType(cd.getTypes(), CdmPackage.Literals.CD_ENUM);
            result.addAll(enums);
        }

        // Get all the COREExternalArtefacts that are reused
        Collection<COREExternalArtefact> reusedArtefacts = COREModelUtil.getReusedArtefacts(artefact);

        // From every reused artefact, gather the Enums if they are public
        for (COREExternalArtefact externalArtefact : reusedArtefacts) {
            ClassDiagram cd = (ClassDiagram) externalArtefact.getRootModelElement();
            Collection<CDEnum> enums =
                    EcoreUtil.getObjectsByType(cd.getTypes(), CdmPackage.Literals.CD_ENUM);
            for (CDEnum e : enums) {
                if (COREInterfaceUtil.isPublic(externalArtefact, e)) {
                    result.add(e);
                }
            }
        }
        
        Set<COREModelComposition> allCompositions = COREModelUtil.collectAllModelCompositions(artefact);        
        COREModelUtil.filterMappedElements(result, allCompositions);
        
        return result;
    }
    
    /**
     * Returns all classes of the model the object is contained in as well as extended models.
     * 
     * @param currentObject the object contained in a model
     * @return the collection of classes
     */
    public static Collection<Classifier> getAvailableClasses(EObject currentObject) {
        Collection<Classifier> result = getAvailableExternalClasses(currentObject);

        //add the classes of the current model
        COREExternalArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(currentObject);
        
        ClassDiagram cd = (ClassDiagram) artefact.getRootModelElement();
        result.addAll(cd.getClasses());

        return result;
    }
        
    /**
     * Returns all classes of extended and reused models.
     * 
     * @param currentObject the object contained in a model
     * @return the collection of classes
     */
    public static Collection<Classifier> getAvailableExternalClasses(EObject currentObject) {
        return getAvailableExternalClassifiers(currentObject, false);
    }

    /**
     * Returns all datatypes of the model the object is contained in as well as extended models that 
     * are identified as data types.
     * 
     * @param currentObject the object contained in a model
     * @return the collection of classes
     */
    public static Collection<Classifier> getAvailableDataTypes(EObject currentObject) {
        Collection<Classifier> result = new HashSet<>();
        Set<ClassDiagram> extendedAspects = getExtendedAspects(currentObject);

        for (ClassDiagram cd : extendedAspects) {
            for (Classifier classifier : cd.getClasses()) {
                if (classifier.isDataType()) {
                    result.add(classifier);
                }
            }
        }

        return result;
    }
    
    /**
     * Returns all data types of extended and reused models.
     * 
     * @param currentObject the object contained in a model
     * @return the collection of classes
     */
    public static Collection<Classifier> getAvailableExternalDatatypes(EObject currentObject) {
        return getAvailableExternalClassifiers(currentObject, true);
    }

    /**
     * Returns all available classes or datatypes of extended and reused models.
     * 
     * @param currentObject an object contained in a class diagram model
     * @param dataType <code>true</code> for data types, <code>false</code> to get all classes
     * @return the collection of classes
     */
    private static Collection<Classifier> getAvailableExternalClassifiers(EObject currentObject, boolean dataType) {

        // Get the COREExternalArtefact that potentially contains extensions and reuses, and then get
        // all the COREExternalArtefacts of the extended models
        COREExternalArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(currentObject);
        Collection<COREExternalArtefact> extendedArtefacts = COREModelUtil.getExtendedArtefacts(artefact);
        
        // From every extended artefacts, gather the Classifiers
        Collection<Classifier> classifiersResult = new ArrayList<>();
        for (COREExternalArtefact externalArtefact : extendedArtefacts) {
            ClassDiagram cd = (ClassDiagram) externalArtefact.getRootModelElement();
            classifiersResult.addAll(cd.getClasses());
        }
        
        // Get all the COREExternalArtefacts that are reused
        Collection<COREExternalArtefact> reusedArtefacts = COREModelUtil.getReusedArtefacts(artefact);
        

        // From every reused artefact, gather the Classifiers if they are public
        for (COREExternalArtefact externalArtefact : reusedArtefacts) {
            ClassDiagram cd = (ClassDiagram) externalArtefact.getRootModelElement();
            for (Classifier c : cd.getClasses()) {
                if (COREInterfaceUtil.isPublic(externalArtefact, c)) {
                    classifiersResult.add(c);
                }
            }
        }
        
        Set<COREModelComposition> allCompositions = COREModelUtil.collectAllModelCompositions(artefact);        
        COREModelUtil.filterMappedElements(classifiersResult, allCompositions);

        return classifiersResult;
    }

    /**
     * Returns all types that are available to be used as a type for attributes.
     * This includes data types, enums and primitive types.
     * 
     * @param currentObject the object contained in a model
     * @return the collection of types
     */
    public static Collection<ObjectType> getAvailableAttributeTypes(EObject currentObject) {
        Collection<ObjectType> result = new HashSet<>();

        result.addAll(getAvailableDataTypes(currentObject));
        result.addAll(getAvailablePrimitiveTypes(currentObject));
        result.addAll(getAvailableExternalEnums(currentObject));

        return result;
    }
    
    /**
     * Returns all enum literals of the available enums.
     * If the given object is an enum, only the enum literals of the given enum are returned.
     * 
     * @param currentObject the object contained in a model, an {@link REnum} if only its literals should be retrieved
     * @return the collection of enum literals
     */
    public static Collection<CDEnumLiteral> getAvailableLiterals(EObject currentObject) {
        Collection<CDEnumLiteral> literals = new HashSet<>();
        
        Collection<CDEnum> enums = getAvailableEnums(currentObject);
        
        if (currentObject instanceof CDEnum) {
            CDEnum specificEnum = (CDEnum) currentObject;
            literals.addAll(specificEnum.getLiterals());
        } else {
            for (CDEnum currentEnum : enums) {
                literals.addAll(currentEnum.getLiterals());
            }            
        }
        
        return literals;
    }

    /**
     * Returns the class diagram the given object is contained in.
     * If the object is the class diagram itself, the object is returned.
     * 
     * @param currentObject the object contained in a model
     * @return the class diagram the given object is contained in, null if none found
     */
    private static ClassDiagram getClassDiagram(EObject currentObject) {
        ClassDiagram cd = (ClassDiagram) EcoreUtil.getRootContainer(currentObject);

        if (cd == null && currentObject instanceof ClassDiagram) {
            cd = (ClassDiagram) currentObject;
        }

        return cd;
    }

    /**
     * Returns the set of extended class diagrams within whose hierarchy the given object is contain in.
     * The set contains the aspect the given object is contained in.
     * 
     * @param currentObject the object contained in a model
     * @return the set of extended class diagrams, including the class diagrams of the object
     */
    private static Set<ClassDiagram> getExtendedAspects(EObject currentObject) {
        ClassDiagram cd = getClassDiagram(currentObject);
        COREArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(cd);

        // Create a set of aspects where elements are allowed from.
        // Besides the current aspect, add extended aspects
        Set<COREArtefact> includedCOREArtefact = COREModelUtil.collectExtendedModels(artefact);
        Set<ClassDiagram> includedAspects = new HashSet<>();
        
        for (COREArtefact includedArtefact : includedCOREArtefact) {
            COREExternalArtefact extArtefact = (COREExternalArtefact) includedArtefact;
            includedAspects.add((ClassDiagram) extArtefact.getRootModelElement());
        }
        
        includedAspects.add(cd);

        return includedAspects;
    }

}
