package ca.mcgill.sel.classdiagram.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;

import ca.mcgill.sel.classdiagram.AssociationEnd;
import ca.mcgill.sel.classdiagram.CDAny;
import ca.mcgill.sel.classdiagram.CDBoolean;
import ca.mcgill.sel.classdiagram.CDSequence;
import ca.mcgill.sel.classdiagram.CDVoid;
import ca.mcgill.sel.classdiagram.CdmFactory;
import ca.mcgill.sel.classdiagram.CdmPackage;
import ca.mcgill.sel.classdiagram.Class;
import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.classdiagram.Classifier;
import ca.mcgill.sel.classdiagram.ImplementationClass;
import ca.mcgill.sel.classdiagram.Layout;
import ca.mcgill.sel.classdiagram.LayoutElement;
import ca.mcgill.sel.classdiagram.Operation;
import ca.mcgill.sel.classdiagram.OperationType;
import ca.mcgill.sel.classdiagram.Parameter;
import ca.mcgill.sel.classdiagram.PrimitiveType;
import ca.mcgill.sel.classdiagram.StructuralFeature;
import ca.mcgill.sel.classdiagram.Type;
import ca.mcgill.sel.commons.StringUtil;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREModelElementComposition;
import ca.mcgill.sel.core.CORENamedElement;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.core.util.COREModelUtil;

/**
 * Helper class with convenient static methods for working with Class Diagram model objects.
 *
 * @author mschoettle
 */
public final class CdmModelUtil {

    /**
     * Default name for constructor operation.
     */
    public static final String CONSTRUCTOR_NAME = "create";

    /**
     * Default name for destructor operation.
     */
    public static final String DESTRUCTOR_NAME = "destroy";
    
    public static final Map<String, String> irregularPlurals = new HashMap<String, String>() {
        private static final long serialVersionUID = 1L;
    {
        put("addendum", "addenda");
        put("alga", "algae");
        put("alumna", "alumnae");
        put("alumnus", "alumni");
        put("analysis", "analyses");
        put("appendix", "appendices");
        put("axis", "axes");
        put("bacillus", "bacilli");
        put("bacterium", "bacteria");
        put("basis", "bases");
        put("beau", "beaux");
        put("bison", "bison");
        put("bus", "busses");
        put("calf", "calves");
        put("child", "children");
        put("corps", "corps");
        put("corpus", "corpora");
        put("crisis", "crises");
        put("criterion", "criteria");
        put("curriculum", "curricula");
        put("datum", "data");
        put("deer", "deer");
        put("die", "dice");
        put("diagnosis", "diagnoses");
        put("echo", "echoes");
        put("elf", "elves");
        put("ellipsis", "ellipses");
        put("embargo", "embargoes");
        put("emphasis", "emphases");
        put("erratum", "errata");
        put("fireman", "firemen");
        put("foot", "feet");
        put("fungus", "fungi");
        put("genus", "genera");
        put("goose", "geese");
        put("half", "halves");
        put("hero", "heroes");
        put("hypothesis", "hypotheses");
        put("knife", "knives");
        put("leaf", "leaves");
        put("life", "lives");
        put("loaf", "loaves");
        put("louse", "lice");
        put("man", "men");
        put("matrix", "matrices");
        put("means", "means");
        put("medium", "media");
        put("memorandum", "memoranda");
        put("moose", "moose");
        put("mosquito", "mosquitoes");
        put("mouse", "mice");
        put("neurosis", "neuroses");
        put("nucleus", "nuclei");
        put("oasis", "oases");
        put("ovum", "ova");
        put("ox", "oxen");
        put("paralysis", "paralyses");
        put("parenthesis", "parentheses");
        put("person", "people");
        put("phenomenon", "phenomena");
        put("potato", "potatoes");
        put("scarf", "scarves");
        put("self", "selves");
        put("series", "series");
        put("sheep", "sheep");
        put("shelf", "shelves");
        put("scissors", "scissors");
        put("species", "species");
        put("stimulus", "stimuli");
        put("stratum", "strata");
        put("synthesis", "syntheses");
        put("synopsis", "synopses");
        put("tableau", "tableaux");
        put("that", "those");
        put("thesis", "theses");
        put("thief", "thieves");
        put("this", "these");
        put("tomato", "tomatoes");
        put("tooth", "teeth");
        put("torpedo", "torpedoes");
        put("vertebra", "vertebrae");
        put("veto", "vetoes");
        put("vita", "vitae");
        put("wife", "wives");
        put("wolf", "wolves");
        put("woman", "women");
    }};
    
    /**
     * Creates a new instance of {@link CdmModelUtil}.
     */
    private CdmModelUtil() {
        // suppress default constructor
    }

    /**
     * Creates a new operation with the given properties if it not already exists. Otherwise it returns null.
     *
     * @param owner - owner of the operation (used to check if operation is unique)
     * @param name the name of the operation
     * @param returnType the return type of the operation
     * @param parameters a list of parameters for the operation
     * @return a new {@link Operation} with the given properties set or null if the operation already exists
     */
    public static Operation createOperation(EObject owner, String name,
            Type returnType, List<Parameter> parameters) {
        return createOperation(owner, name, returnType, parameters, true);
    }

    /**
     * Creates a new operation with the given properties. It can check if operation already exists and returns null in
     * this case
     *
     * @param owner - owner of the operation (used to check if operation is unique)
     * @param name the name of the operation
     * @param returnType the return type of the operation
     * @param parameters a list of parameters for the operation
     * @param checkUnicity - true if you want to check if operation already exists
     * @return a new {@link Operation} with the given properties set or null if the operation already exists
     */
    public static Operation createOperation(EObject owner, String name,
            Type returnType, List<Parameter> parameters, boolean checkUnicity) {

        if (checkUnicity && !CdmModelUtil.isUniqueOperation(owner, name, returnType, parameters)) {
            return null;
        }

        Operation operation = CdmFactory.eINSTANCE.createOperation();
        operation.setName(name);
        operation.setReturnType(returnType);

        if (parameters != null) {
            operation.getParameters().addAll(parameters);
        }

        return operation;
    }

    /**
     * Returns whether the given sub-class has the given super class as a super type.
     * I.e., the sub-class inherits from the super class.
     *
     * @param subType the class that is considered a sub-class
     * @param superType the classifier that is considered a super-type
     * @return true, if the sub-type class has the super-type classifier as a super type, false otherwise
     */
    public static boolean hasSuperClass(Class subType, Classifier superType) {
        for (Classifier classifier : subType.getSuperTypes()) {
            if (classifier == superType) {
                return true;
            }
        }

        return false;
    }
    
    /**
     * Returns the plural of an {@link AssociationEnd} rolename if it still is the default generated name.
     * If the rolename is not the default generated one based on the classifer, the rolename is unchanged.
     *
     * @param roleName the role name to pluralize
     * @param classifierName the name of the AssociationEnd Classifier to check if it is the default generated rolename
     * @return String with the plural version of the rolename
     */
    public static String pluralizeRoleName(String roleName, String classifierName) {
        String defaultName = "my" + classifierName;
        if (!roleName.equals(defaultName)) {
            //avoid changing names that are not the default template
            return roleName;
        }
        if (roleName.length() > 0) {
            List<String> endingInEs = Arrays.asList(new String[] {"s", "z", "ch", "sh", "x"});
            String roleNameWithoutPrefix = roleName.replaceFirst("my", "").trim();
            char lastLetter = roleName.charAt(roleName.length() - 1);
            
            if (Character.isDigit(lastLetter)) {
                //ignore names ending in a digit
                return roleName;
            }
            
            if (irregularPlurals.containsKey(roleNameWithoutPrefix.toLowerCase())) {
                String plural = irregularPlurals.get(roleNameWithoutPrefix.toLowerCase());
                
                plural = plural.substring(0, 1).toUpperCase() + plural.substring(1);
                return "my" + plural; 
            } else if (lastLetter == 'y') {
                if (roleName.charAt(roleName.length() - 2) == 'e') {
                    return roleName.substring(0, roleName.length() - 2) + "ies";
                } else {
                    return roleName.substring(0, roleName.length() - 1) + "ies";
                }
            } else if (endingInEs.contains(roleName.substring(roleName.length() - 2))
                    || endingInEs.contains(lastLetter + "")) {
                return roleName + "es";
            } else {
                return roleName + "s";
            }
        }
        return roleName;
    }
    
    /**
     * Returns the singular of an {@link AssociationEnd} rolename if it still is the default generated name.
     * If the rolename is not the default generated one based on the classifer, the rolename is unchanged.
     *
     * @param roleName the role name to singularize
     * @param classifierName the name of the AssociationEnd Classifier to check if it is the default generated rolename
     * @return String with the singular version of the rolename
     */
    public static String singularizeRoleName(String roleName, String classifierName) {
        String defaultName = "my" + classifierName;
        if (roleName.length() > 0 && roleName.equals(pluralizeRoleName(defaultName, classifierName))) {
            //only singularize default rolenames
            char lastLetter = roleName.charAt(roleName.length() - 1);
            if (Character.isDigit(lastLetter)) {
                //ignore names ending in a digit
                return roleName;
            }
            
            //return default singular roleName
            return defaultName;
        }
        return roleName;
    }

    /**
     * Checks whether the given name is unique inside the owners feature.
     *
     * @param owner the owner containing {@link CORENamedElement}s
     * @param feature the feature of the owner containing {@link CORENamedElement}s
     * @param name the name to be checked for uniqueness
     * @return true, if the given name is unique among the {@link CORENamedElement}s of the owners feature,
     *         false otherwise
     */
    public static boolean isUniqueName(EObject owner, EStructuralFeature feature, String name) {
        Collection<CORENamedElement> children =
                EMFModelUtil.collectElementsOfType(owner, feature, CorePackage.eINSTANCE.getCORENamedElement());

        for (CORENamedElement namedElement : children) {
            if (namedElement.getName() != null && namedElement.getName().equalsIgnoreCase(name)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Checks whether the given object is unique inside the owners feature.
     *
     * @param owner the owner containing {@link CoreNamedElement}s
     * @param feature the feature of the owner containing {@link CoreNamedElement}s
     * @param name the name to be checked for uniqueness
     * @param object the object to be checked for uniqueness (used for signature operation checking)
     * @return true, if the given name is unique among the {@link CoreNamedElement}s of the owners feature,
     *         false otherwise
     */
    public static boolean isUnique(EObject owner, EStructuralFeature feature, String name, EObject object) {
        if (feature == CdmPackage.Literals.CLASSIFIER__OPERATIONS) {
            Operation operation = (Operation) object;
            return isUniqueOperation(owner, name, operation.getReturnType(), operation.getParameters());
        } else if (object instanceof COREArtefact) {
            COREArtefact model = (COREArtefact) object;
            return isUniqueAspect(model, name);
        } else {
            return isUniqueName(owner, feature, name);
        }
    }

    /**
     * Checks whether the given model is unique among the same model type.
     * 
     * @param model the model to be checked for uniqueness
     * @param name the name to be checked for uniqueness
     * @return true, if the name is unique, false otherwise
     */
    private static boolean isUniqueAspect(COREArtefact model, String name) {
        Collection<CORENamedElement> children = EMFModelUtil.collectElementsOfType(model.getCoreConcern(),
                CorePackage.Literals.CORE_CONCERN__ARTEFACTS, model.eClass());

        for (CORENamedElement namedElement : children) {
            if (namedElement.getName() != null && namedElement.getName().equalsIgnoreCase(name)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Checks whether the given operation is unique inside the owners operations.
     * (Check name and the list of parameters)
     *
     * @param owner the owner containing {@link CORENamedElement}s
     * @param name the name to be checked for uniqueness
     * @param returnType the return type of the operation
     * @param parameters the list of parameters to be checked for uniqueness
     * @return true, if the given name (and this list of parameters) is unique
     *         among the {@link Operation}s of the owners feature, false otherwise
     */
    public static boolean isUniqueOperation(EObject owner, String name, Type returnType, List<Parameter> parameters) {
        @SuppressWarnings("unchecked")
        List<Operation> children = (List<Operation>) owner.eGet(CdmPackage.Literals.CLASSIFIER__OPERATIONS);

        for (Operation operationElement : children) {
            if (operationElement.getName() != null && operationElement.getName().equalsIgnoreCase(name)) {

                // If the one of two operations have null parameters and the other 0 parameters
                // Or if both operations have null parameters
                if ((parameters == null && operationElement.getParameters() == null)
                        || (parameters == null && operationElement.getParameters().size() == 0)
                        || (operationElement.getParameters() == null && parameters.size() == 0)) {
                    return false;
                }

                boolean equal = false;
                // If operations have parameters
                if (parameters != null && operationElement.getParameters() != null
                        && parameters.size() == operationElement.getParameters().size()) {
                    equal = true;
                    for (int i = 0; i < parameters.size() && equal; i++) {
                        if (parameters.get(i).getType() != operationElement.getParameters().get(i).getType()) {
                            equal = false;
                        }
                    }
                }
                
                // Check for equal return type names.
                if (returnType != null && operationElement.getReturnType() != null
                        && !returnType.getName().equals(operationElement.getReturnType().getName())) {
                    equal = false;
                }
                
                if (equal) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Constructs and returns related getter of the given attribute. If the operation already has a getter, it will
     * return null.
     *
     *
     * @param attribute - the attribute to extract its getter
     * @return the getter of the attribute
     */
    public static Operation createGetter(StructuralFeature attribute) {
        Class owner = (Class) attribute.eContainer();

        if (owner == null) {
            return null;
        }

        ClassDiagram cd = (ClassDiagram) owner.eContainer();
        String attributeName = StringUtil.toUpperCaseFirst(attribute.getName());
        Type attributeType = attribute.getType();

        // If the attribute parameter is an AssociationEnd object,
        // we have to check the multiplicity
        if (attribute instanceof AssociationEnd) {
            attributeType =
                    CdmModelUtil.getTypeDependingMultiplicityOfAssociationEnd((AssociationEnd) attribute,
                            cd);
        }

        String prefix = (attribute.getType() instanceof CDBoolean) ? "is" : "get";

        Operation newGetter = CdmModelUtil.createOperation(owner, prefix + attributeName, attributeType, null);
        if (newGetter != null && attribute.isStatic()) {
            newGetter.setStatic(true);
        }

        return newGetter;
    }

    /**
     * Constructs and returns related setter of the given attribute. If the operation already has a setter, it will
     * return null.
     *
     * @param attribute - the attribute to extract its setter
     * @param parameterName The name of the parameter
     * @return the setter of the attribute
     */
    public static Operation createSetter(StructuralFeature attribute, String parameterName) {
        Class owner = (Class) attribute.eContainer();

        if (owner == null) {
            return null;
        }

        // get the void type from the structural view
        ClassDiagram cd = (ClassDiagram) owner.eContainer();
        CDVoid voidType = CdmModelUtil.getVoidType(cd);
        String attributeName = StringUtil.toUpperCaseFirst(attribute.getName());

        Parameter parameter = CdmFactory.eINSTANCE.createParameter();
        Type attributeType = attribute.getType();

        // If the attribute parameter is an AssociationEnd object,
        // we have to check the multiplicity
        if (attribute instanceof AssociationEnd) {
            attributeType =
                    CdmModelUtil.getTypeDependingMultiplicityOfAssociationEnd((AssociationEnd) attribute,
                            cd);
        }

        parameter.setType(attributeType);
        parameter.setName(parameterName);
        List<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(parameter);

        Operation newSetter = CdmModelUtil.createOperation(owner, "set" + attributeName, voidType, parameters);
        if (newSetter != null && attribute.isStatic()) {
            newSetter.setStatic(true);
        }

        return newSetter;
    }

    /**
     * Check if the given attribute has already its getter into its owner.
     *
     * @param attribute - the attribute to check
     * @return true if the attribute has its getter into its owner
     */
    public static boolean isGetterUnique(StructuralFeature attribute) {
        Operation getter = createGetter(attribute);
        return getter == null;
    }

    /**
     * Check if the given attribute has already its setter into its owner.
     *
     * @param attribute - the attribute to check
     * @return true if the attribute has its setter into its owner
     */
    public static boolean isSetterUnique(StructuralFeature attribute) {
        Operation setter = createSetter(attribute, attribute.getName());
        return setter == null;
    }

    /**
     * Checks the multiplicity of the {@link AssociationEnd} in order to create or not an array type rather than a
     * simple type. Checks the upperBound : if it is upper than 1, it is an array of the {@link AssociationEnd} type.
     *
     * @param attribute {@link AssociationEnd} which have to be checked for multiplicity.
     * @param cd The corresponding {@link ClassDiagram}.
     * @return The new type of the attribute (array of the {@link AssociationEnd} type or just the
     *         {@link AssociationEnd} type.
     */
    public static Type getTypeDependingMultiplicityOfAssociationEnd(AssociationEnd attribute,
            ClassDiagram cd) {
        Type attributeType = attribute.getType();
        AssociationEnd asso = attribute;

        // If the upperBound is not 1, we need an array
        if (asso.getUpperBound() != 1) {
            CDSequence rSeq = null;
            for (Type t : cd.getTypes()) {
                if (t instanceof CDSequence && ((CDSequence) t).getType() == attributeType) {
                    rSeq = (CDSequence) t;
                }
            }
            if (rSeq == null) {
                rSeq = CdmFactory.eINSTANCE.createCDSequence();
                rSeq.setType(attributeType);
                cd.getTypes().add(rSeq);
            }
            attributeType = rSeq;
        }

        return attributeType;
    }

    /**
     * Gets the void type from the structural view.
     *
     * @param cd the cd
     * @return the void type, null if not found
     */
    public static CDVoid getVoidType(ClassDiagram cd) {
        for (Type type : cd.getTypes()) {
            if (CDVoid.class.isInstance(type)) {
                return (CDVoid) type;
            }
        }

        return null;
    }

    /**
     * Returns a list of all primitive types contained in the given structural view.
     *
     * @param cd the {@link ClassDiagram}
     * @return a list of all primitive types of the given structural view
     */
    public static List<PrimitiveType> getPrimitiveTypes(ClassDiagram cd) {
        List<PrimitiveType> primitiveTypes = new ArrayList<PrimitiveType>();

        for (Type type : cd.getTypes()) {
            if (type instanceof PrimitiveType) {
                primitiveTypes.add((PrimitiveType) type);
            }
        }

        return primitiveTypes;
    }
    
    /**
     * Returns the primitive type contained in the given structural view corresponding to the name.
     * 
     * @param cd the {@link ClassDiagram} containing the types
     * @param name the name of the primitive type
     * @return the {@link PrimitiveType} with the given name, null if none found
     */
    public static PrimitiveType getPrimitiveTypeByName(ClassDiagram cd, String name) {
        PrimitiveType type = null;
        
        for (PrimitiveType primitiveType : getPrimitiveTypes(cd)) {
            if (name.equals(primitiveType.getName())) {
                type = primitiveType;
                break;
            }
        }
        
        return type;
    }

    /**
     * Creates a new empty {@link ClassDiagram} containing default types.
     *
     * @param name - The name to give to the aspect
     * @return the newly created {@link ClassDiagram}
     */
    public static ClassDiagram createClassDiagram(String name) {
        return createClassDiagram(name, null);
    }
    
    /**
     * Creates an empty {@link ClassDiagram} containing default types.
     *
     * @param baseName - The baseName to give to the aspect.
     * @param concern - The concern containing the classdiagram. Only used to create a unique name.
     * @return the newly created {@link ClassDiagram}
     */
    public static ClassDiagram createClassDiagram(String baseName, COREConcern concern) {
        ClassDiagram cd = CdmFactory.eINSTANCE.createClassDiagram();
        
        cd.setName(baseName);
        

        // create default types
        createDefaultTypes(cd);

        // create empty layout
        createLayout(cd);

        return cd;
    }

    /**
     * Creates all the primitive types that do not currently exist in the structural view (and sets them...).
     *
     * @param cd the structural view of interest
     */
    public static void createDefaultTypes(ClassDiagram cd) {
        if (getTypeInstance(cd, CDVoid.class) == null) {
            cd.getTypes().add(CdmFactory.eINSTANCE.createCDVoid());
        }

        if (getTypeInstance(cd, CDAny.class) == null) {
            cd.getTypes().add(CdmFactory.eINSTANCE.createCDAny());
        }

        // add all primitive types
        EClass primitiveTypeClass = CdmPackage.eINSTANCE.getPrimitiveType();

        for (EClassifier classifier : CdmPackage.eINSTANCE.getEClassifiers()) {
            if (classifier instanceof EClass) {
                EClass clazz = (EClass) classifier;

                // is it a PrimitiveType but not an Enum or Array
                if (!clazz.isAbstract() && primitiveTypeClass.isSuperTypeOf(clazz)
                        && !CdmPackage.eINSTANCE.getCDEnum().equals(clazz)
                        && !CdmPackage.eINSTANCE.getCDArray().equals(clazz)) {

                    boolean alreadyExists = false;

                    // if the type already exists we don't want to add it another time
                    for (Type type : cd.getTypes()) {

                        if (type.eClass().equals(clazz)) {
                            alreadyExists = true;
                            break;
                        }

                    }

                    if (!alreadyExists) {
                        Type newObject = (Type) CdmFactory.eINSTANCE.create(clazz);
                        cd.getTypes().add(newObject);
                    }
                }
            }
        }
    }

    /**
     * Returns the type instance of the given class that is a type of the given class diagram.
     * If no such type is found, null is returned.
     *
     * @param cd the {@link ClassDiagram} containing the types
     * @param typeClass the {@link java.lang.Class} of which an instance is requested
     * @param <T> the type of the requested type
     * @return the type instance of the given class, null if none is found
     */
    private static <T extends Type> T getTypeInstance(ClassDiagram cd, java.lang.Class<T> typeClass) {
        for (final Type type : cd.getTypes()) {
            if (typeClass.isInstance(type)) {
                @SuppressWarnings("unchecked")
                T typed = (T) type;
                return typed;
            }
        }
        return null;
    }

    /**
     * Creates a new layout for a given {@link ClassDiagram}.
     * The layout is the {@link ca.mcgill.sel.ram.impl.ContainerMapImpl} specifically
     * that holds all {@link LayoutElement} for children of the given {@link ClassDiagram}.
     *
     * @param cd the {@link ClassDiagram} holding the {@link LayoutElement} for its children
     */
    public static void createLayout(ClassDiagram cd) {
        Layout layout = CdmFactory.eINSTANCE.createLayout();

        // workaround used here since creating the map, adding the values and then putting it doesn't work
        // EMF somehow does some magic with the passed map instance
        layout.getContainers().put(cd, new BasicEMap<EObject, LayoutElement>());

        cd.setLayout(layout);
    }


    /**
     * Collects a set of classifiers that contains the classifier, its super types and all other classifiers
     * that this classifiers is mapped to. Performs this "bottom-up", i.e., starting at the current class diagram up the
     * extended aspects hierarchy.
     * If the given classifier is not from the given class diagram, the classifier is resolved
     * to the one from the given aspect so that the "bottom-up" approach can be used.
     *
     * @param cd the Class Diagram containing the classifier
     * @param classifier the classifier all classifiers to retrieve for
     * @return a set of classifiers that this classifier is mapped to, including the classifier itself
     */
    public static Set<Classifier> collectClassifiersFor(ClassDiagram cd, Classifier classifier) {
        return collectClassifiersFor(cd, classifier, true);
    }

    /**
     * Collects a set of classifiers that contains the classifier, its super types and all other classifiers
     * that this classifiers is mapped to. Performs this "bottom-up", i.e., starting at the current aspect up the
     * extended aspects hierarchy.
     * If the given classifier is not from the given aspect, the classifier is resolved
     * to the one from the given aspect so that the "bottom-up" approach can be used.
     *
     * @param cd the aspect containing the classifier
     * @param classifier the classifier all classifiers to retrieve for
     * @param includeSuperTypes indicates if super types must be included in the final result
     * @return a set of classifiers that this classifier is mapped to, including the classifier itself
     */
    public static Set<Classifier> collectClassifiersFor(ClassDiagram cd, Classifier classifier,
            boolean includeSuperTypes) {
        Set<Classifier> classifiers = new HashSet<Classifier>();

        if (!classifier.eIsProxy()) {
            Classifier actualClassifier = resolveClassifier(cd, classifier);

            collectClassifiersRecursive(classifiers, cd, actualClassifier, includeSuperTypes);
            // Finally, when extending another aspect and a classifier of the other aspect is called,
            // but a classifier in the current aspect with the same name exists, it has to be considered as well.
            for (Classifier ownClassifier : cd.getClasses()) {
                for (Classifier extendedClassifier : new HashSet<Classifier>(classifiers)) {
                    if (isNameEqual(ownClassifier, extendedClassifier)) {
                        classifiers.add(ownClassifier);
                    }
                }
            }
        }

        return classifiers;
    }

    /**
     * Checks whether the name of two given classifiers are the same.
     * In case of implementation classes with generics, their type parameters have to match as well.
     *
     * @param classifier1 the first classifier
     * @param classifier2 the second classifier
     * @return true, if the name matches, false otherwise
     */
    public static boolean isNameEqual(Classifier classifier1, Classifier classifier2) {
        if (classifier2.getName().equals(classifier1.getName())) {
            if (classifier2 instanceof ImplementationClass
                    && classifier1 instanceof ImplementationClass) {
                ImplementationClass c1 = (ImplementationClass) classifier2;
                ImplementationClass c2 = (ImplementationClass) classifier1;
                if (isNameEqual(c1, c2)) {
                    return true;
                }
            } else if (classifier1 instanceof Class
                    && classifier2 instanceof Class) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks whether the two given implementation classes are the same.
     * Two implementation classes are the same if besides their name,
     * also the type parameters match in type and name.
     *
     * @param class1 the first {@link ImplementationClass}
     * @param class2 the second {@link ImplementationClass}
     * @return true, if they are the same, false otherwise
     */
    private static boolean isNameEqual(ImplementationClass class1, ImplementationClass class2) {
        if (class1.getInstanceClassName().equals(class2.getInstanceClassName())) {
            return typeParametersMatch(class1, class2);
        }

        return false;
    }

    /**
     * Returns whether the type parameters of two given implementation classes are the same.
     *
     * @param class1 the first {@link ImplementationClass}
     * @param class2 the second {@link ImplementationClass}
     * @return true, if they are the same, false otherwise
     */
    public static boolean typeParametersMatch(ImplementationClass class1, ImplementationClass class2) {
        if (class1.getTypeParameters().size() == class2.getTypeParameters().size()) {
            for (int index = 0; index < class1.getTypeParameters().size(); index++) {
                Type type = class1.getTypeParameters().get(index).getGenericType();
                Type type2 = class2.getTypeParameters().get(index).getGenericType();
                if ((type != null ^ type2 != null)
                        || (type != null && type2 != null && !type.getName().equals(type2.getName()))) {
                    return false;
                }
            }

            return true;
        }

        return false;
    }

    /**
     * Recursively collects a set of classifiers that contains the classifier, classifiers
     * that this classifiers is mapped to, and optionally its super types.
     * Performs this recursively for every classifier found, in order to retrieve the complete set of classifiers.
     *
     * @param classifiers the current set of found classifiers, which classifiers are added to
     * @param cd the class diagram containing the classifier
     * @param classifier the classifier all classifiers to retrieve for
     * @param includeSuperTypes indicates if super types must be included or not
     */
    private static void collectClassifiersRecursive(Set<Classifier> classifiers, ClassDiagram cd, Classifier classifier,
            boolean includeSuperTypes) {
        classifiers.add(classifier);
        COREArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(cd);

        Set<COREModelComposition> modelCompositions = new HashSet<COREModelComposition>();
        modelCompositions.addAll(artefact.getModelExtensions());
        modelCompositions.addAll(artefact.getModelReuses());

        for (COREModelComposition modelComposition : modelCompositions) {
            for (COREModelElementComposition<?> composition : modelComposition.getCompositions()) {
                COREMapping<?>  mapping = (COREMapping<?>) composition;
                if (mapping.getTo() == classifier) {
                    COREExternalArtefact currentArtefact = (COREExternalArtefact) modelComposition.getSource();
                    ClassDiagram source = (ClassDiagram) currentArtefact.getRootModelElement();
                    collectClassifiersRecursive(classifiers, source,
                            (Classifier) mapping.getFrom(), includeSuperTypes);
                }
            }
        }

        // Classifiers from extended aspects with the same name as the called classifier
        // need to be considered, because they are implicitly mapped.
        // At the same time, if the extendedClassifier is
        // an Implementation Class, it needs to be considered as well.
        modelCompositions.addAll(COREModelUtil.collectModelExtensions(cd));
        
        for (COREModelComposition modelComposition : modelCompositions) {
            COREExternalArtefact currentArtefact = (COREExternalArtefact) modelComposition.getSource();
            ClassDiagram classDiagram = (ClassDiagram) currentArtefact.getRootModelElement();
           
            

            for (Classifier extendedClassifier : cd.getClasses()) {
                if (modelComposition.eClass() == CorePackage.Literals.CORE_MODEL_EXTENSION
                        && !(extendedClassifier instanceof ImplementationClass)) {
                    if (classifier.getName().equals(extendedClassifier.getName())) {
                        collectClassifiersRecursive(classifiers, classDiagram, extendedClassifier,
                                includeSuperTypes);
                    }
                } else if (extendedClassifier instanceof ImplementationClass
                        && classifier instanceof ImplementationClass) {
                    ImplementationClass c1 = (ImplementationClass) extendedClassifier;
                    ImplementationClass c2 = (ImplementationClass) classifier;
                    if (isNameEqual(c1, c2)) {
                        collectClassifiersRecursive(classifiers, classDiagram, extendedClassifier,
                                includeSuperTypes);
                    }
                }
            }
        }

        // Add the super types of each possible classifier.
        if (includeSuperTypes) {
            for (Classifier superType : classifier.getSuperTypes()) {
                collectClassifiersRecursive(classifiers, cd, superType, includeSuperTypes);
            }
        }
    }

    /**
     * Resolves the given classifier to the corresponding classifier in the given class diagram.
     * Returns the given classifier if it is already contained in the class diagram.
     * For instance, mapping the classifier to another classifier B in the given class diagram
     * would result in B being returned.
     *
     * @param cd the {@link ClassDiagram}
     * @param classifier the {@link Classifier} to find the corresponding one in the aspect for
     * @return the given classifier, if it is already contained in the aspect,
     *         otherwise the located classifier in the aspect corresponding to the given classifier
     */
    public static Classifier resolveClassifier(ClassDiagram cd, Classifier classifier) {
        Classifier clazz = classifier;

        if (EcoreUtil.getRootContainer(classifier) != cd && !classifier.eIsProxy()) {
            Set<COREModelComposition> modelCompositions = new HashSet<>();
            modelCompositions.addAll(COREModelUtil.collectModelExtensions(cd));
            modelCompositions.addAll(COREModelUtil.collectAllModelReuses(cd));

            for (Classifier currentClassifier : cd.getClasses()) {
                if (isNameEqual(clazz, currentClassifier)) {
                    return currentClassifier;
                }
            }
        }

        return clazz;
    }

    /**
     * Finds the index of where to add a new operation to the class.
     * Constructors are added at the beginning, but after existing constructors.
     * Destructors are added after the constructors, but also after existing destructors.
     *
     * @param owner the class to add an operation to
     * @param type the type of the operation
     * @return the index of where to add a new operation for the given type
     */
    public static int findConstructorIndexFor(Classifier owner, OperationType type) {
        int index = 0;

        for (Operation operation : owner.getOperations()) {
            switch (operation.getOperationType()) {
                case CONSTRUCTOR:
                    index++;
                    break;
                case DESTRUCTOR:
                    if (type == OperationType.DESTRUCTOR) {
                        index++;
                    }
                    break;
                case NORMAL:
                    return index;
            }
        }

        return index;
    }

    /**
     * Returns whether the given class is empty.
     * A class is empty if it has no (or only partial) operations and attributes and no outgoing associations.
     * It is okay if there are incoming associations.
     *
     * @param clazz the {@link Class} to check
     * @return true, if the class is empty, false otherwise
     */
    public static boolean isClassEmpty(Class clazz) {
        for (AssociationEnd end : clazz.getAssociationEnds()) {
            if (end.isNavigable()) {
                return false;
            }
        }

        return clazz.getOperations().size() == 0 && clazz.getAttributes().size() == 0;
    }

    /**
     * Returns true if the given class can be not abstract. The class can't be not abstract if it contains at least one
     * abstract operation.
     *
     * @param clazz - the class asked
     * @return true if the class can be not abstract, false otherwise.
     */
    public static boolean classCanBeNotAbstract(Class clazz) {
        for (Operation op : clazz.getOperations()) {
            if (op.isAbstract()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Returns the implementation class contained in the structural view that has the given instance class name.
     * Returns null if it doesn't exist.
     *
     * @param cd the {@link ClassDiagram} that contains the classifiers
     * @param instanceClassName the fully qualified name of the implementation class
     * @return the {@link ImplementationClass}, null if none found
     */
    public static ImplementationClass getImplementationClassByName(ClassDiagram cd,
            String instanceClassName) {
        Collection<ImplementationClass> implClasses =
                EcoreUtil.getObjectsByType(cd.getClasses(), CdmPackage.Literals.IMPLEMENTATION_CLASS);
        for (ImplementationClass implClass : implClasses) {
            if (instanceClassName.equals(implClass.getInstanceClassName())) {
                return implClass;
            }
        }

        return null;
    }

    /**
     * Returns true if the given operation is a constructor or a destructor.
     *
     * @param operation - the operation to test.
     * @return true if the given operation is a constructor or a destructor.
     */
    public static boolean isConstructorOrDestructor(Operation operation) {
        OperationType op = operation.getOperationType();
        return op.equals(OperationType.CONSTRUCTOR) || op.equals(OperationType.DESTRUCTOR);
    }

    /**
     * Returns true if both operations have the same signature.
     *
     * @param operation1 The first {@link Operation}
     * @param operation2 The second {@link Operation}
     * @return whether both operations have the same signature
     */
    public static boolean hasSameSignature(Operation operation1, Operation operation2) {
        if (!operation1.getName().equals(operation2.getName())
                || operation1.getParameters().size() != operation2.getParameters().size()
                || !operation1.getReturnType().getName().equals(operation2.getReturnType().getName())) {
            return false;
        }
        
        for (int i = 0; i < operation1.getParameters().size(); i++) {
            if (!operation1.getParameters().get(i).getType().equals(operation2.getParameters().get(i).getType())) {
                return false;
            }
        }
        
        return true;
    }

    /**
     * Returns the multiplicity representation of the association end.
     *
     * @param associationEnd The end of interest.
     * @param isQualified Whether the association end is qualified
     * @return The multiplicity string for the association end.
     */
    public static String getMultiplicity(AssociationEnd associationEnd, boolean isQualified) {
        String upperBound;

        if (isQualified) {
            return "0..1";
        } else if (associationEnd.getUpperBound() == -1) {
            upperBound = "*";
        } else {
            upperBound = String.valueOf(associationEnd.getUpperBound());
        }

        if (associationEnd.getLowerBound() == associationEnd.getUpperBound()) {
            return upperBound;
        } else {
            StringBuilder builder = new StringBuilder();

            builder.append(associationEnd.getLowerBound());
            builder.append("..");
            builder.append(upperBound);

            return builder.toString();
        }
    }

    /**
     * Returns whether an association can be created between the two given classifiers.
     * 
     * @param from the {@link Classifier} from
     * @param to the {@link Classifier} to
     * @return true, if an association can be created, false otherwise
     */
    public static boolean canCreateAssociation(Classifier from, Classifier to) {
        return !from.isDataType() && !to.isDataType() && !(from instanceof ImplementationClass);
    }

    /**
     * Returns whether the from class can inherit from the to class.
     * 
     * @param from the {@link Classifier} from
     * @param to the {@link Classifier} to
     * @return true, if an inheritance can be created between the two, false otherwise
     */
    public static boolean canCreateInheritance(Classifier from, Classifier to) {
        return !from.isDataType() && !to.isDataType() && !(from instanceof ImplementationClass)
                    && from instanceof Class && !(to instanceof PrimitiveType);
    }
}
