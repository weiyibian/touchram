/**
 */
package ca.mcgill.sel.classdiagram;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.mcgill.sel.classdiagram.CdmPackage#getAttribute()
 * @model
 * @generated
 */
public interface Attribute extends StructuralFeature {
} // Attribute
