package ca.mcgill.sel.classdiagram.language.weaver;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;

import ca.mcgill.sel.classdiagram.Attribute;
import ca.mcgill.sel.classdiagram.CDAny;
import ca.mcgill.sel.classdiagram.CDEnum;
import ca.mcgill.sel.classdiagram.CDEnumLiteral;
import ca.mcgill.sel.classdiagram.CDVoid;
import ca.mcgill.sel.classdiagram.Class;
import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.classdiagram.Classifier;
import ca.mcgill.sel.classdiagram.ImplementationClass;
import ca.mcgill.sel.classdiagram.Operation;
import ca.mcgill.sel.classdiagram.Parameter;
import ca.mcgill.sel.classdiagram.PrimitiveType;
import ca.mcgill.sel.classdiagram.Type;
import ca.mcgill.sel.classdiagram.VisibilityType;
import ca.mcgill.sel.classdiagram.util.CdmModelUtil;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREModelExtension;
import ca.mcgill.sel.core.util.COREInterfaceUtil;
import ca.mcgill.sel.core.weaver.COREModelWeaver;
import ca.mcgill.sel.core.weaver.COREWeaver;
import ca.mcgill.sel.core.weaver.util.WeavingInformation;


/**
 * The ClassdiagramWeaver is capable of weaving two classdiagrams together.
 * This class is a singleton.
 * 
 * The weaver has two operations:
 * <ul>
 * <li><b>createEmptyModel:</b> which creates a new empty classdiagram model</li>
 * <li><b>weaveModel:</b> which weaves one classdiagram into another classdiagram according
 * to a provided CompositionSpecification</li>
 * </ul>
 *
 * @author joerg
 */

public final class ClassdiagramWeaver extends COREModelWeaver<ClassDiagram> {

    /**
     * Creates a new, empty classdiagram.
     * @return returns the newly created class diagram
     */
    @Override
    public ClassDiagram createEmptyModel() {
        ClassDiagram newClassdiagram = CdmModelUtil.createClassDiagram("newClassdiagram");
        return newClassdiagram;
    }

    /**
     * Weaves two directly dependent classdiagrams together. For each model element that is mapped,
     * the union of properties is calculated. Model elements that are not mapped are copied from the
     * source model into the target model.
     * 
     * @param targetArtefact the COREExternalArtefact of the base
     * @param target the classdiagram to weave into
     * @param composition the COREModelComposition that refers to the classdiagram that is to be woven
     * @param weavingInfo the weaving information which is going to remember which elements
     *        from the source model are mapped to which model elements in the base model
     *        and vice versa.
     * @param topdown a boolean that indicates whether we are doing top down or bottom up weaving.
     *        When weaving topdown, any names of mapped structural elements have to be copied
     *        from the source into the target.
     * 
     * @return the augmented base model containing the model elements from the source model as well
     */
    @Override
    public ClassDiagram weaveModel(COREExternalArtefact targetArtefact, ClassDiagram target,
            COREModelComposition modelComposition, WeavingInformation weavingInfo, boolean topdown) {

        ClassDiagram sourceCD = (ClassDiagram) ((COREExternalArtefact) modelComposition.getSource())
                .getRootModelElement();

        // add primitive type mappings to weaving information so that the primitive types are updated correctly 
        for (final Type typeFromSource : sourceCD.getTypes()) {
            // we only create a mapping if the target Type is a primitive type
            if (typeFromSource instanceof PrimitiveType) {
                final PrimitiveType primitiveTypeFromTarget;
                primitiveTypeFromTarget = getPrimitiveTypeFromClassdiagram(target,
                        typeFromSource.getName());
                if (primitiveTypeFromTarget != null) {
                    weavingInfo.add(typeFromSource, primitiveTypeFromTarget);
                }
            } else if (typeFromSource instanceof CDVoid) {
                final Type voidTypeFromTarget;
                voidTypeFromTarget = getVoidTypeFromClassdiagram(target);
                if (voidTypeFromTarget != null) {
                    weavingInfo.add(typeFromSource, voidTypeFromTarget);
                }
            } else if (typeFromSource instanceof CDAny) {
                final Type anyTypeFromBase;
                anyTypeFromBase = getAnyTypeFromClassdiagram(target);
                if (anyTypeFromBase != null) {
                    weavingInfo.add(typeFromSource, anyTypeFromBase);
                }
            }
        }
        
        // First go through all classifiers in the sourceCD, and either merge or copy them into the
        // base model
        for (Iterator<Classifier> iterator = sourceCD.getClasses().iterator(); iterator.hasNext(); ) {
            Classifier classFromSource = iterator.next();
            
            System.out.print("Handling " + classFromSource.getName() + ", which is ");
            
            // check if the class has to be woven
            if (weavingInfo.wasWoven(classFromSource)) {
                List<Classifier> mappedToTargetClasses = weavingInfo.getToElements(classFromSource);
                ArrayList<Classifier> replacedMappedToClasses = new ArrayList<Classifier>();
                ArrayList<Classifier> newImplentationClasses = new ArrayList<Classifier>();
                System.out.println(" mapped to: " + mappedToTargetClasses);
                // merge class
                for (Classifier targetClass : mappedToTargetClasses) {
                    if (topdown) {
                        if (classFromSource instanceof ImplementationClass) {
                            // if we are weaving topdown and the source class is an implementation class,
                            // then we need to make the target class in the base an implementation class.
                            // The only way to do that is to create a new implementation class in the base,
                            // and redirect all the references.
                            ImplementationClass newImplementationClass = EcoreUtil.copy(
                                    (ImplementationClass) classFromSource);
                            target.getClasses().add(newImplementationClass);  
                            // Now copy all properties to the implementation class
                            newImplementationClass.getAssociationEnds().addAll(targetClass.getAssociationEnds());
                            // Now redirect all references in the woven aspect to the new implementation class
                            for (EStructuralFeature.Setting s 
                                : EcoreUtil.UsageCrossReferencer.find(targetClass, target)) {
                                s.set(newImplementationClass);
                            }
                            // Now remove the non-implementation class in the base
                            target.getClasses().remove(targetClass);
                            // finally, update the weaving info
                            replacedMappedToClasses.add(targetClass);
                            newImplentationClasses.add(newImplementationClass);
                        } else {
                            // if we are weaving topdown we need to copy the name
                            targetClass.setName(classFromSource.getName());
                        }
                    }
                    if (targetClass instanceof ca.mcgill.sel.classdiagram.Class 
                            && classFromSource instanceof ca.mcgill.sel.classdiagram.Class) {
                        weaveAttributes((Class) targetClass, (Class) classFromSource, weavingInfo, topdown);
                        weaveOperations(targetClass, classFromSource,
                                target, targetArtefact, modelComposition instanceof COREModelExtension,
                                weavingInfo, topdown);
                        
                        // Weave inheritance
                        // Although this code will set the supertype reference to point to the super class in
                        // the source model, the reference updater will take care of changing the reference to
                        // the class in the target model eventually
                        for (Classifier parent : classFromSource.getSuperTypes()) {                    
                            if (!targetClass.getSuperTypes().contains(parent)) {
                                targetClass.getSuperTypes().add(parent);
                            }
                        }
                    }
                }
                // update the weaving information for all classes that were mapped to
                // implementation classes
                if (topdown && classFromSource instanceof ImplementationClass) {
                    for (int i = 0; i < replacedMappedToClasses.size(); i++) {
                        mappedToTargetClasses.remove(replacedMappedToClasses.get(i));
                        mappedToTargetClasses.add(newImplentationClasses.get(i));
                    }
                }

            } else {
                // there is no user-defined mapping, therefore we simply copy the model element from
                // the source model into the base
                System.out.println("not mapped");
                Classifier modelElementCopy = COREWeaver.copyModelElement(classFromSource, targetArtefact,
                        weavingInfo, modelComposition);
                target.getClasses().add(modelElementCopy);               
            }
        }    
        
        // now look for enums and do the same
        for (Iterator<Type> iterator = sourceCD.getTypes().iterator();
                iterator.hasNext(); ) {
            Type typeFromSource = iterator.next();
            
            if (typeFromSource instanceof CDEnum) {
                CDEnum enumFromSource = (CDEnum) typeFromSource;
                System.out.print("Handling " + enumFromSource.getName() + ", which is ");

                if (weavingInfo.wasWoven(enumFromSource)) {
                    List<CDEnum> mappedToTargetEnums = weavingInfo.getToElements(enumFromSource);                
                    System.out.println(" mapped to: " + mappedToTargetEnums);
                    for (CDEnum destEnum : mappedToTargetEnums) {
                        weaveEnumLiterals(destEnum, enumFromSource, weavingInfo);
                    }
                } else {
                    System.out.println("not mapped");
                    CDEnum modelElementCopy = COREWeaver.copyModelElement(enumFromSource, targetArtefact,
                        weavingInfo, modelComposition);
                    target.getTypes().add(modelElementCopy); 
                }
            }
        }

        
        return target;
    }

    /**
     * This is a helper method that searches all the primitive types in the
     * classdiagram and tries to find one that matches by name.
     * @param cd The classdiagram to search in
     * @param name The name of the primitive type to search for
     * @return The primitive type in the classdiagram if found otherwise null.
     */
    private static PrimitiveType getPrimitiveTypeFromClassdiagram(final ClassDiagram cd, final String name) {
        for (final Type primitiveTypeFromCD : cd.getTypes()) {
            if (primitiveTypeFromCD instanceof PrimitiveType
                    &&
                    primitiveTypeFromCD.getName().equals(name)) {
                return (PrimitiveType) primitiveTypeFromCD;
            }
        }
        return null;
    }

    /**
     * This is a helper method that searches all the primitive types in the
     * given class diagram and tries to find the CDAny type.
     * @param cd The class diagram to search in
     * @return The CDAny type instance in the class diagram if found otherwise null.
     */
    private static CDAny getAnyTypeFromClassdiagram(final ClassDiagram cd) {
        for (final Type typeFromCD : cd.getTypes()) {
            if (typeFromCD instanceof CDAny) {
                return (CDAny) typeFromCD;
            }
        }
        return null;
    }
    
    /**
     * This is a helper method that searches all the primitive types in the
     * given class diagram and tries to find one that matches by name.
     * @param cd The aspect to search in
     * @return The RVoid type in the aspect if found otherwise null if not found.
     */
    private static CDVoid getVoidTypeFromClassdiagram(final ClassDiagram cd) {
        for (final Type typeFromCD : cd.getTypes()) {
            if (typeFromCD instanceof CDVoid) {
                return (CDVoid) typeFromCD;
            }
        }
        return null;
    }
 
    /**
     * This method performs weaving of Enum Literals.
     * @param destinationEnum The enum we are merging sourceEnum into
     * @param sourceEnum The enum that will be merged into destinationEnum
     * @param weavingInformation The data structure containing all mapping information
     */
    public static void weaveEnumLiterals(final CDEnum destinationEnum, final CDEnum sourceEnum,
            final WeavingInformation weavingInformation) {
        
        for (final CDEnumLiteral sourceLiteral : sourceEnum.getLiterals()) {
            // if it was mapped it means we are renaming hence we do not need to modify
            // anything in the destination, otherwise we copy
            if (!weavingInformation.wasWoven(sourceLiteral)) {
                // create a copy of the literal that we will add to destinationEnum
                final CDEnumLiteral destinationLiteral = EcoreUtil.copy(sourceLiteral);
                
                // create a mapping between attributes indicating they have been woven
                weavingInformation.add(sourceLiteral, destinationLiteral);
                // and add the attribute to the class from the base
                destinationEnum.getLiterals().add(destinationLiteral);
            }
        }
    }

    /**
     * This method performs weaving of attributes.
     * @param targetClass The class we are merging sourceClass into
     * @param sourceClass The class that will be merged into targetClass
     * @param weavingInformation The data structure containing all mapping information
     * @param topdown boolean that indicates whether we are doing top down or bottom up weaving
     */
    public static void weaveAttributes(final Class targetClass, final Class sourceClass,
            final WeavingInformation weavingInformation, boolean topdown) {
        
        for (final Attribute sourceAttribute : sourceClass.getAttributes()) {
            // check to see if the attribute was not mapped
            // if it was mapped it means we are renaming hence we do not need to modify
            // anything in the higher level class
            if (!weavingInformation.wasWoven(sourceAttribute)) {
                // create a copy of the attribute that we will add to classFromHigherLevel
                final Attribute targetAttribute = EcoreUtil.copy(sourceAttribute);
                
                // create a mapping between attributes indicating they have been woven
                weavingInformation.add(sourceAttribute, targetAttribute);
                // and add the attribute to the class from the target model
                targetClass.getAttributes().add(targetAttribute);
            } else if (topdown) {
                for (Attribute destinationAttribute : weavingInformation.getToElements(sourceAttribute)) {
                    destinationAttribute.setName(sourceAttribute.getName());
                }
            }
        }
    }    

    /**
     * This is the main method that performs weaving of operations.
     * 
     * @param targetClass The class that sourceClass is being woven into
     * @param sourceClass The class that contains the operations we want to weave into targetClass
     * @param targetDiagram The classdiagram that targetClass belongs to
     * @param isExtends represents the type of weaving: extends or depends
     * @param weavingInformation The data structure containing all mapping information
     * @param targetArtefact the artefact we're weaving into
     * @param topdown boolean that indicates whether we are doing top down or bottom up weaving
     */
    public static void weaveOperations(final Classifier targetClass, final Classifier sourceClass,
            final ClassDiagram targetDiagram, final COREExternalArtefact targetArtefact, 
            final boolean isExtends, final WeavingInformation weavingInformation, boolean topdown) {

        for (final Operation opFromSource : sourceClass.getOperations()) {
            boolean opMapped = false;
            // First check to see if this operation was mapped.
            // See issue #112.
            List<Operation> toElements = weavingInformation.getToElements(opFromSource);
            if (toElements != null) {
                for (final Operation key : toElements) {
                    if (key.eContainer().equals(targetClass)) {
                        // Operation was mapped by user, operation already exists in weaving information,
                        // but not it's parameters, therefore, create weaving information for parameters
                        createParameterWeavingInformation(opFromSource, key, weavingInformation);
                        opMapped = true;
                        break;
                    }
                }
            }

            if (!opMapped) {
                Operation opCopy = EcoreUtil.copy(opFromSource);

                if (!isExtends && !(targetClass instanceof ImplementationClass)
                        && COREInterfaceUtil.isPublic(targetArtefact, opFromSource)) {
                    //TODO: set the visibilty of the operation to CONCERN!
                    //COREInterfaceUtil.(COREVisibilityType.CONCERN);
                    opCopy.setVisibility(VisibilityType.PACKAGE);
                }

                targetClass.getOperations().add(opCopy);
                weavingInformation.add(opFromSource, opCopy);

                createParameterWeavingInformation(opFromSource, opCopy, weavingInformation);
            }
        }
    }

    /**
     * Creates weaving information for all parameters of the operation. Parameters are
     * matched by index.
     * 
     * @param original The original operation
     * @param copy The copy of original
     * @param weavingInformation The data structure containing all mapping information
     */
    public static void createParameterWeavingInformation(Operation original, Operation copy,
            WeavingInformation weavingInformation) {
        for (int index = 0; index < original.getParameters().size(); index++) {
            Parameter oldParameter = original.getParameters().get(index);
            Parameter newParameter = copy.getParameters().get(index);
            weavingInformation.add(oldParameter, newParameter);
        }
    }



}
