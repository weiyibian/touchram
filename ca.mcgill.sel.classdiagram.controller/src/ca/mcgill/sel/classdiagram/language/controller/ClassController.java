package ca.mcgill.sel.classdiagram.language.controller;


import java.util.List;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.MoveCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import ca.mcgill.sel.classdiagram.Attribute;
import ca.mcgill.sel.classdiagram.CDVoid;
import ca.mcgill.sel.classdiagram.CdmFactory;
import ca.mcgill.sel.classdiagram.CdmPackage;
import ca.mcgill.sel.classdiagram.Class;
import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.classdiagram.Classifier;
import ca.mcgill.sel.classdiagram.ObjectType;
import ca.mcgill.sel.classdiagram.Operation;
import ca.mcgill.sel.classdiagram.OperationType;
import ca.mcgill.sel.classdiagram.Parameter;
import ca.mcgill.sel.classdiagram.StructuralFeature;
import ca.mcgill.sel.classdiagram.Type;
import ca.mcgill.sel.classdiagram.VisibilityType;
import ca.mcgill.sel.classdiagram.language.controller.util.CdmLanguageAction;
import ca.mcgill.sel.classdiagram.util.CdmModelUtil;
import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.COREAction;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.COREVisibilityType;

/**
 * The controller for {@link Class}es. Additionally contains operations for attributes, operations and parameters.
 *
 * @author mschoettle, yhattab
 */
public class ClassController extends BaseController {

    /**
     * Creates a new instance of {@link ClassController}.
     */
    protected ClassController() {
        // Prevent anyone outside this package to instantiate.
    }
    
    /*
     * COREPerspective Actions Validation Methods
     */
    
    public boolean canEditClass(COREPerspective perspective) {
        for (COREAction action : perspective.getActions()) {
            if (action.getName().equals(CdmLanguageAction.EDIT_CLASS.getName())) {
                return true;
            }
        }
        
        return false;
    }
    
    public boolean canEditVisibillity(COREPerspective perspective) {
        for (COREAction action : perspective.getActions()) {
            if (action.getName().equals(CdmLanguageAction.EDIT_VISIBILITY.getName())) {
                return true;
            }
        }
        
        return false;
    }
    
    public boolean canEditStatic(COREPerspective perspective) {
        for (COREAction action : perspective.getActions()) {
            if (action.getName().equals(CdmLanguageAction.EDIT_STATIC.getName())) {
                return true;
            }
        }
        
        return false;
    }

    public boolean canRemoveAttribute(COREPerspective perspective) {
        for (COREAction action : perspective.getActions()) {
            if (action.getName().equals(CdmLanguageAction.DELETE_ATTRIBUTE.getName())) {
                return true;
            }
        }
        
        return false;
    }

    public boolean canCreateAttribute(COREPerspective perspective) {
        for (COREAction action : perspective.getActions()) {
            if (action.getName().equals(CdmLanguageAction.ADD_ATTRIBUTE.getName())) {
                return true;
            }
        }
        
        return false;
    }

    public boolean canEditOperation(COREPerspective perspective) {
        for (COREAction action : perspective.getActions()) {
            if (action.getName().equals(CdmLanguageAction.EDIT_OPERATION.getName())) {
                return true;
            }
        }
        
        return false;
    }
    
    public boolean canRemoveOperation(COREPerspective perspective) {
        for (COREAction action : perspective.getActions()) {
            if (action.getName().equals(CdmLanguageAction.REMOVE_OPERATION.getName())) {
                return true;
            }
        }
        
        return false;
    }
    
    public boolean canCreateOperation(COREPerspective perspective) {
        for (COREAction action : perspective.getActions()) {
            if (action.getName().equals(CdmLanguageAction.ADD_OPERATION.getName())) {
                return true;
            }
        }
        
        return false;
    }
      
    public boolean canCreateParameter(COREPerspective perspective) {
        for (COREAction action : perspective.getActions()) {
            if (action.getName().equals(CdmLanguageAction.ADD_OPERATION_PARAMETER.getName())) {
                return true;
            }
        }
        
        return false;
    }


    /**
     * Moves the given classifier to a new position.
     *
     * @param classifier the classifier to move
     * @param x the new x position
     * @param y the new y position
     */
    public void moveClassifier(Classifier classifier, float x, float y) {
        ClassDiagram cd = (ClassDiagram) EcoreUtil.getRootContainer(classifier);
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(cd);

        Command moveClassCommand = createMoveCommand(editingDomain, cd, classifier, x, y);

        doExecute(editingDomain, moveClassCommand);
    }

    
    /**
     * Removes the given attribute.
     *
     * @param attribute the attribute to be removed
     */
    public void removeAttribute(Attribute attribute) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(attribute);
        CompoundCommand compoundCommand = new CompoundCommand();

        // Create remove Command.
        compoundCommand.append(RemoveCommand.create(editingDomain, attribute));

        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Creates a new attribute.
     *
     * @param owner the class the attribute should be added to
     * @param index the index where the attribute should be added at
     * @param name the name of the attribute
     * @param objectType the object type of the attribute
     * @throws IllegalArgumentException if the attribute already exists
     */
    public void createAttribute(Class owner, int index, String name, ObjectType objectType) {
        if (!CdmModelUtil.isUniqueName(owner, CdmPackage.Literals.CLASSIFIER__ATTRIBUTES, name)) {
            throw new IllegalArgumentException("The " + name + " attribute is not unique for attributes");
        }

        // Create the attribute with the given information.
        Attribute attribute = CdmFactory.eINSTANCE.createAttribute();
        attribute.setName(name);
        attribute.setType(objectType);

        // Execute add command.
        doAdd(owner, CdmPackage.Literals.CLASSIFIER__ATTRIBUTES, attribute, index);
    }
    
    /**
     * Set's the given attribute's position.
     *
     * @param attribute the attribute to be set
     * @param index the new index of the operation
     */
    public void setAttributePosition(Attribute attribute, int index) {
        ClassDiagram cd = (ClassDiagram) EcoreUtil.getRootContainer(attribute);
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(cd);

        Command command = new MoveCommand(editingDomain, attribute.eContainer(), CdmPackage.Literals.CLASSIFIER__ATTRIBUTES, attribute, index);

        doExecute(editingDomain, command);
    }
 
    /**
     * Removes the given operation.
     *
     * @param operation the operation to be removed
     */
    public void removeOperation(Operation operation) {
        ClassDiagram cd = (ClassDiagram) EcoreUtil.getRootContainer(operation);
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(cd);
        CompoundCommand compoundCommand = new CompoundCommand();

        compoundCommand.append(RemoveCommand.create(editingDomain, operation));

        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Creates a new operation.
     *
     * @param owner the class the operation should be added to
     * @param index the index the operation should be added at
     * @param name the name of the operation
     * @param visibility the {@link VisibilityType} to set for the operation
     * @param returnType the return type
     * @param parameters a list of parameters
     * @throws IllegalArgumentException if the operation already exists
     */
    public void createOperation(Class owner, int index, String name, VisibilityType visibility,
            Type returnType, List<Parameter> parameters) {

        // Create the operation with the given information.
        Operation operation = CdmModelUtil.createOperation(owner, name, returnType, parameters);
        
        if (operation != null) {
            addOperationToEditingDomain(owner, index, operation, visibility);
        } else {
            throw new IllegalArgumentException("An operation with the same signature already exists.");
        }
    }
    
    /**
     * Set's the given operation's position.
     *
     * @param operation the operation to be set
     * @param index the new index of the operation
     */
    public void setOperationPosition(Operation operation, int index) {
        ClassDiagram cd = (ClassDiagram) EcoreUtil.getRootContainer(operation);
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(cd);
                
        Command command = new MoveCommand(editingDomain, operation.eContainer(), CdmPackage.Literals.CLASSIFIER__OPERATIONS, operation, index);
        
        doExecute(editingDomain, command);
        }

    /**
     * Link the given operation to the Editing Domain with the command.
     *
     * @param owner - the class the operation should be added to
     * @param index - the index the operation should be added at
     * @param operation - the operation to add
     * @param visibility - the {@link VisibilityType} to set for the operation
     */
    private void addOperationToEditingDomain(Class owner, int index, Operation operation,
            VisibilityType visibility) {

        EditingDomain domain = EMFEditUtil.getEditingDomain(owner);
        Command command = createAddOperationCommand(domain, owner, operation, visibility, index);

        doExecute(domain, command);
    }

    /**
     * Create a command that add a new operation.
     *
     * @param domain the {@link EditingDomain}
     * @param owner the class the operation should be added to
     * @param operation the operation to add
     * @param visibility the {@link VisibilityType} to set for the operation
     * @param index the index the operation should be added at
     * @throws IllegalArgumentException if the operation already exists
     *
     * @return The {@link Command} created
     */
    private Command createAddOperationCommand(EditingDomain domain, Classifier owner, Operation operation,
            VisibilityType visibility, int index) {
        CompoundCommand compoundCommand = new CompoundCommand();
        compoundCommand.append(
                changeOperationAndClassVisibilityCommand(domain, owner, operation, visibility));
        compoundCommand.append(AddCommand.create(domain, owner, CdmPackage.Literals.CLASSIFIER__OPERATIONS, operation,
                index));

        return compoundCommand;
    }

    /**
     * Changes the visibility of the classifier accordingly when the visibility of an operation is changed to public.
     *
     * @param owner the classifier the operation belongs to
     * @param operation which visibility is changed
     * @param visibility the value of operation's {@link VisibilityType}
     */
    public void changeOperationAndClassVisibility(Classifier owner, Operation operation, VisibilityType visibility) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);
        Command command = changeOperationAndClassVisibilityCommand(editingDomain, owner, operation, visibility);
        doExecute(editingDomain, command);
    }

    /**
     * Returns the command that changes the visibility of the classifier accordingly when the visibility of an operation
     * is changed.
     *
     * @param editingDomain - The {@link EditingDomain} for the command
     * @param owner - The {@link Classifier} the operation belongs to
     * @param operation - The {@link Operation} which visibility is changed
     * @param visibility - The value of operation's {@link VisibilityType}
     * @return {@link Command} which is created.
     */
    Command changeOperationAndClassVisibilityCommand(EditingDomain editingDomain, Classifier owner,
            Operation operation,
            VisibilityType visibility) {
        CompoundCommand compoundCommand = new CompoundCommand();
        // If adding a public operation to a non public class, update the class visibility
        if (visibility == VisibilityType.PUBLIC && owner.getVisibility() != VisibilityType.PUBLIC) {
            // Update the classifier visibility
            // For CORE
            compoundCommand.append(changeCoreVisibilityCommand(editingDomain, 
                    owner, owner, COREVisibilityType.PUBLIC));
            // For RAM
            compoundCommand.append(SetCommand.create(editingDomain, 
                    owner, CdmPackage.Literals.CLASSIFIER__VISIBILITY, VisibilityType.PUBLIC));
        }
        // Update the operation visibility
        compoundCommand.append(changeOperationVisibilityCommand(editingDomain, operation, owner, visibility));
        return compoundCommand;
    }

    /**
     * Get the command used to change the {@link VisibilityType} of an {@link Operation}.
     * This does update the {@link COREVisibilityType} of the operation if necessary as well.
     *
     * @param editingDomain - the editing domain
     * @param operation - the operation
     * @param owner - the operation owner
     * @param visibilityType - the new visibility to set
     * @return the created command
     */
    private Command changeOperationVisibilityCommand(EditingDomain editingDomain, Operation operation, Classifier owner,
            VisibilityType visibilityType) {
        CompoundCommand command = new CompoundCommand();
        // Change the CORE visibility if necessary
        if (visibilityType == VisibilityType.PUBLIC) {
            Command tempCommand = changeCoreVisibilityCommand(editingDomain, 
                    operation, owner, COREVisibilityType.PUBLIC);
            if (tempCommand != null) {
                command.append(tempCommand);
            }
        } else {
            Command tempCommand = changeCoreVisibilityCommand(editingDomain, 
                    operation, owner, COREVisibilityType.CONCERN);
            if (tempCommand != null) {
                command.append(tempCommand);
            }
        }
        // Change only the RAM visibility
        command.append(SetCommand.create(editingDomain, operation, CdmPackage.Literals.OPERATION__VISIBILITY,
                visibilityType));
        return command;
    }

    /**
     * Creates an operation which gets the name of an attribute.
     *
     * @param attribute the attribute.
     */
    public void createGetterOperation(StructuralFeature attribute) {
        ClassDiagram cd = (ClassDiagram) EcoreUtil.getRootContainer(attribute);

        EditingDomain domain = EMFEditUtil.getEditingDomain(cd);
        Command command = this.createGetterOperationCommand(domain, cd, attribute, VisibilityType.PUBLIC);
        if (command != null) {
            doExecute(domain, command);
        }
    }

    /**
     * Returns a command that creates an operation which gets the name of an attribute.
     *
     * @param domain the {@link EditingDomain} to use for executing commands
     * @param cd The current Class Diagram
     * @param attribute the attribute.
     * @param visibilityType The visibility type
     *
     * @return the created {@link Command}
     */
    private Command createGetterOperationCommand(EditingDomain domain, ClassDiagram cd, StructuralFeature attribute,
            VisibilityType visibilityType) {
        Class owner = (Class) attribute.eContainer();
        int index = owner.getOperations().size();

        Operation operation = CdmModelUtil.createGetter(attribute);

        if (operation == null) {
            return null;
        }
        
        CompoundCommand compoundCommand = new CompoundCommand();

        compoundCommand.appendAndExecute(createAddOperationCommand(domain, owner, operation, visibilityType, index));

        return compoundCommand;
    }

    /**
     * Creates an operation which sets the name of an attribute.
     *
     * @param attribute the attribute.
     */
    public void createSetterOperation(StructuralFeature attribute) {
        ClassDiagram cd = (ClassDiagram) EcoreUtil.getRootContainer(attribute);

        EditingDomain domain = EMFEditUtil.getEditingDomain(cd);

        Command command = this.createSetterOperationCommand(domain, cd, attribute, attribute.getName(),
                VisibilityType.PUBLIC);

        if (command != null) {
            doExecute(domain, command);
        }
    }

    /**
     * Creates an operation which sets the name of an attribute.
     *
     * @param domain the {@link EditingDomain} to use for executing commands
     * @param cd The current Class Diagram
     * @param attribute the attribute.
     * @param parameterName The name of the parameter
     * @param visibilityType The visibility type
     *
     * @return The created {@link Command}
     */
    private Command createSetterOperationCommand(EditingDomain domain, ClassDiagram cd, StructuralFeature attribute,
            String parameterName, VisibilityType visibilityType) {

        Class owner = (Class) attribute.eContainer();
        int index = owner.getOperations().size();

        Operation operation = CdmModelUtil.createSetter(attribute, parameterName);
        if (operation == null) {
            return null;
        }

        return createAddOperationCommand(domain, owner, operation, visibilityType, index);
    }

    /**
     * Creates an operation which gets and sets the name of an attribute.
     *
     * @param attribute the attribute.
     */
    public void createGetterAndSetterOperation(StructuralFeature attribute) {
        ClassDiagram cd = (ClassDiagram) EcoreUtil.getRootContainer(attribute);

        EditingDomain domain = EMFEditUtil.getEditingDomain(cd);

        CompoundCommand compoundCommand = new CompoundCommand();

        Command getterCommand = this.createGetterOperationCommand(domain, cd, attribute, VisibilityType.PUBLIC);
        if (getterCommand != null) {
            compoundCommand.append(getterCommand);
        }

        Command setterCommand = this.createSetterOperationCommand(domain, cd, attribute, attribute.getName(),
                VisibilityType.PUBLIC);
        if (setterCommand != null) {
            compoundCommand.append(setterCommand);
        }

        doExecute(domain, compoundCommand);
    }

    /**
     * Creates a new constructor for the given class.
     *
     * @param owner the class to add a constructor to
     */
    public void createConstructor(Class owner) {

        Operation operation = CdmModelUtil.createOperation(owner, CdmModelUtil.CONSTRUCTOR_NAME, owner, null, false);
        if (operation != null) {
            operation.setOperationType(OperationType.CONSTRUCTOR);
            int index = CdmModelUtil.findConstructorIndexFor(owner, OperationType.CONSTRUCTOR);
            EditingDomain domain = EMFEditUtil.getEditingDomain(owner);
            Command command =
                    changeOperationAndClassVisibilityCommand(domain, owner, operation, VisibilityType.PUBLIC);
            CompoundCommand compundCommand = new CompoundCommand();
            compundCommand.append(command);
            compundCommand.append(AddCommand.create(domain, owner, CdmPackage.Literals.CLASSIFIER__OPERATIONS,
                    operation,
                    index));
            doExecute(domain, compundCommand);
        }

    }

    /**
     * Creates a new destructor for the given class.
     *
     * @param owner the class to add a destructor to
     */
    public void createDestructor(Class owner) {
        CDVoid voidType = CdmModelUtil.getVoidType((ClassDiagram) owner.eContainer());
        Operation operation = CdmModelUtil.createOperation(owner, CdmModelUtil.DESTRUCTOR_NAME, voidType, null, false);
        if (operation != null) {
            operation.setOperationType(OperationType.DESTRUCTOR);
            int index = CdmModelUtil.findConstructorIndexFor(owner, OperationType.DESTRUCTOR);

            EditingDomain domain = EMFEditUtil.getEditingDomain(owner);
            Command command =
                    changeOperationAndClassVisibilityCommand(domain, owner, operation, VisibilityType.PUBLIC);
            CompoundCommand compundCommand = new CompoundCommand();
            compundCommand.append(command);
            compundCommand.append(AddCommand.create(domain, owner, CdmPackage.Literals.CLASSIFIER__OPERATIONS,
                    operation,
                    index));
            doExecute(domain, compundCommand);
        }
    }

    /**
     * Creates a new parameter. In case the affected operation is used in message views, all messages are updated by
     * adding a corresponding parameter value mapping.
     *
     * @param owner the operation the parameter should be added to
     * @param index the index the parameter should be added at
     * @param name the name of the parameter
     * @param type the type of the parameter
     */
    public void createParameter(Operation owner, int index, String name, Type type) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);
        CompoundCommand compoundCommand = new CompoundCommand();

        Parameter parameter = CdmFactory.eINSTANCE.createParameter();
        parameter.setName(name);
        parameter.setType(type);

        compoundCommand.append(AddCommand.create(editingDomain, owner, CdmPackage.Literals.OPERATION__PARAMETERS,
                parameter, index));

        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Removes the given parameter. In case the affected operation is used in message views, all messages are updated by
     * removing the corresponding parameter value mapping.
     *
     * @param parameter the parameter to be removed
     */
    public void removeParameter(Parameter parameter) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(parameter.eContainer());
        CompoundCommand compoundCommand = new CompoundCommand();

        compoundCommand.append(RemoveCommand.create(editingDomain, parameter));

        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Adds the given classifier as a super type to the given class. Makes sure that the given super type can actually
     * be added, i.e., there is no existing inheritance between the two classes in either direction.
     *
     * @param subType the sub class the super type should be added to
     * @param superType the super type of the sub class
     */
    public void addSuperType(Class subType, Classifier superType) {
        // Avoid if same inheritance already exists.
        if (!CdmModelUtil.hasSuperClass(subType, superType)
                // and inheritance on the same class
                && (superType != subType)
                // and inheritance in the opposite direction
                && !((superType instanceof Class) && CdmModelUtil.hasSuperClass((Class) superType, subType))) {
            doAdd(subType, CdmPackage.Literals.CLASSIFIER__SUPER_TYPES, superType);
        }
    }

    /**
     * Removes the given classifier as the super type of the given class. Determines which of the given classes is the
     * sub-type and the super-type, i.e., it is not necessary to determine this beforehand.
     *
     * @param classifier1 the first classifier
     * @param classifier2 the second classifier
     */
    public void removeSuperType(Classifier classifier1, Classifier classifier2) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(classifier1);

        Class subType;
        Classifier superType;

        // figure out which is the super type
        if (classifier1 instanceof Class && CdmModelUtil.hasSuperClass((Class) classifier1, classifier2)) {
            subType = (Class) classifier1;
            superType = classifier2;
        } else {
            subType = (Class) classifier2;
            superType = classifier1;
        }

        // Create remove command.
        Command removeCommand =
                RemoveCommand.create(editingDomain, subType, CdmPackage.Literals.CLASSIFIER__SUPER_TYPES, superType);
        doExecute(editingDomain, removeCommand);
    }

    /**
     * Switched the abstract property of the given operation, and if its class is not abstract, set it abstract.
     *
     * @param operation the operation
     */
    public void switchOperationAbstract(Operation operation) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(operation);

        CompoundCommand compoundCommand = new CompoundCommand();

        boolean isOperationAbstract = operation.isAbstract();
        compoundCommand.append(SetCommand.create(editingDomain, operation, CdmPackage.Literals.OPERATION__ABSTRACT,
                !isOperationAbstract));

        if (!isOperationAbstract) {
            EObject clazz = operation.eContainer();
            boolean isClassAbstract = (Boolean) clazz.eGet(CdmPackage.Literals.CLASSIFIER__ABSTRACT);
            if (!isClassAbstract) {
                compoundCommand.append(SetCommand.create(editingDomain, clazz, CdmPackage.Literals.CLASSIFIER__ABSTRACT,
                        !isClassAbstract));
            }
        }

        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Switches the abstract property of the given class.
     *
     * @param clazz the class
     * @return true if the class has been switched. False otherwise.
     */
    public boolean switchAbstract(Class clazz) {
        if (!CdmModelUtil.classCanBeNotAbstract(clazz)) {
            return false;
        }
        doSwitch(clazz, CdmPackage.Literals.CLASSIFIER__ABSTRACT);
        return true;
    }
    
    /**
     * Switches the static property of the given attribute.
     *
     * @param attribute the attribute
     */
    public void switchAttributeStatic(Attribute attribute) {
        doSwitch(attribute, CdmPackage.Literals.STRUCTURAL_FEATURE__STATIC);
    }

    /**
     * Switches the static property of the given operation.
     *
     * @param operation the operation
     */
    public void switchOperationStatic(Operation operation) {
        doSwitch(operation, CdmPackage.Literals.OPERATION__STATIC);
    }
}
