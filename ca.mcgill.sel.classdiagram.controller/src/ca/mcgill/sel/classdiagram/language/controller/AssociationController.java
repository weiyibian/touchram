package ca.mcgill.sel.classdiagram.language.controller;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import ca.mcgill.sel.classdiagram.Association;
import ca.mcgill.sel.classdiagram.AssociationEnd;
import ca.mcgill.sel.classdiagram.CdmPackage;
import ca.mcgill.sel.classdiagram.Class;
import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.classdiagram.ReferenceType;
import ca.mcgill.sel.classdiagram.Type;
import ca.mcgill.sel.classdiagram.language.controller.util.CdmLanguageAction;
import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.COREAction;
import ca.mcgill.sel.core.COREPerspective;

/**
 * The controller for {@link Association}s and {@link AssociationEnd}s.
 *
 * @author mschoettle
 * @author cbensoussan
 * @author yhattab
 */
public class AssociationController extends BaseController {

    /**
     * Creates a new instance of {@link AssociationController}.
     */
    protected AssociationController() {
        // Prevent anyone outside this package to instantiate.
    }

    /*
     * COREPerspective Actions Validation Methods
     */
    
    public boolean canEditAssociation(COREPerspective perspective) {
        for (COREAction action : perspective.getActions()) {
            if (action.getName().equals(CdmLanguageAction.EDIT_ASSOCIATION.getName())) {
                return true;
            }
        }
        
        return false;
    }
    
    public boolean canDeleteAssociation(COREPerspective perspective) {
        for (COREAction action : perspective.getActions()) {
            if (action.getName().equals(CdmLanguageAction.DELETE_ASSOCIATION.getName())) {
                return true;
            }
        }
        
        return false;
    }

    
    /**
     * Deletes the given {@link Association}.
     *
     * @param cd the current {@link ClassDiagram}
     * @param association the {@link Association} to delete
     */
    public void deleteAssociation(ClassDiagram cd, Association association) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(association);
        
        Command deleteCommand =  deleteAssociation(cd, association, editingDomain);
        
        doExecute(editingDomain, deleteCommand);
    }

    /**
     * Deletes the given {@link Association}.
     *
     * @param Class Diagram the current {@link ClassDiagram}
     * @param association the {@link Association} to delete
     * @param editingDomain the {@link EditingDomain} to use for executing commands
     *TODO: the access modifier should be protected 
     * @return the created {@link CompoundCommand}
     */
    public CompoundCommand deleteAssociation(ClassDiagram cd, Association association, EditingDomain editingDomain) {
        CompoundCommand compoundCommand = new CompoundCommand();

        // create remove command for association
        compoundCommand.append(RemoveCommand.create(editingDomain, association));

        // create remove commands for association ends
        for (AssociationEnd associationEnd : association.getEnds()) {
            compoundCommand.append(RemoveCommand.create(editingDomain, associationEnd));
        }

        // Create command for removing ElementMap (includes the layout element) and execute it if possible (nary assoc.)
        Command removeElementMapCommand = createRemoveLayoutElementCommand(editingDomain, cd, association);
        if (removeElementMapCommand.canExecute()) {
            compoundCommand.append(removeElementMapCommand);
        }
        
        return compoundCommand;
    }

    /**
     * Sets the association class of an association.
     * @param association The {@link Association} for which to set the association class
     * @param associationClass The {@link Class} to set for the association
     */
    public void setAssociationClass(Association association, Class associationClass) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(association);
        CompoundCommand compoundCommand = new CompoundCommand();
        
        //set association class for association
        compoundCommand.append(SetCommand.create(editingDomain, association,
                CdmPackage.Literals.ASSOCIATION__ASSOCIATION_CLASS, associationClass));
        
        doExecute(editingDomain, compoundCommand);
    }
    
    /**
     * Removes the association class of an association, doesn't delete the Association Class.
     * @param association The {@link Association} from which to remove an association class
     */
    public void removeAssociationClass(Association association) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(association);
        
        Command removeAssociationClassCommand = SetCommand.create(editingDomain, association,
                CdmPackage.Literals.ASSOCIATION__ASSOCIATION_CLASS, SetCommand.UNSET_VALUE);
        
        doExecute(editingDomain, removeAssociationClassCommand);
    }
    
    /**
     * Sets the qualifier for the association end.
     * @param associationEnd The {@link AssociationEnd} for which to set the qualifier
     * @param type The {@link Type} to set the qualifier to
     */
    public void setQualifier(AssociationEnd associationEnd, Type qualifierType) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(associationEnd);
        CompoundCommand compoundCommand = new CompoundCommand();
        
        //set qualification for the association end
        compoundCommand.append(SetCommand.create(editingDomain, associationEnd,
                CdmPackage.Literals.ASSOCIATION_END__QUALIFIER, qualifierType));
        
        //set parent association's qualified boolean
        compoundCommand.append(SetCommand.create(editingDomain, associationEnd,
                CdmPackage.Literals.ASSOCIATION_END__REFERENCE_TYPE, ReferenceType.QUALIFIED));

        
        doExecute(editingDomain, compoundCommand);
    }
    
    /**
     * Removes the qualifier for the asociation end.
     * @param associationEnd The {@link AssociationEnd} from which to remove the qualifier
     */
    public void removeQualifier(AssociationEnd associationEnd) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(associationEnd);
        CompoundCommand compoundCommand = new CompoundCommand();
        
        //set qualification for the association end
        compoundCommand.append(SetCommand.create(editingDomain, associationEnd,
                CdmPackage.Literals.ASSOCIATION_END__QUALIFIER, SetCommand.UNSET_VALUE));
        
        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Sets the multiplicity of the given {@link AssociationEnd} according to the given lower and upper bound.
     *
     * @param cd the current {@link ClassDiagram}
     * @param associationEnd the {@link AssociationEnd} the multiplicity should be changed for
     * @param lowerBound the lower bound of the association end
     * @param upperBound the upper bound of the association end, -1 for many
     */
    public void setMultiplicity(ClassDiagram cd, AssociationEnd associationEnd, int lowerBound, int upperBound) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(associationEnd);
        AssociationEnd oppositeEnd = associationEnd.getOppositeEnd();
        CompoundCommand compoundCommand;
        if (oppositeEnd != null && oppositeEnd.isNavigable()) {
            compoundCommand = new BidirectionalCompoundCommand(cd, associationEnd, oppositeEnd);
        } else {
            compoundCommand = new UnidirectionalCompoundCommand(cd, associationEnd, oppositeEnd);
        }
        compoundCommand.append(SetCommand.create(editingDomain, associationEnd,
                CdmPackage.Literals.ASSOCIATION_END__LOWER_BOUND, lowerBound));
        compoundCommand.append(SetCommand.create(editingDomain, associationEnd,
                CdmPackage.Literals.ASSOCIATION_END__UPPER_BOUND, upperBound));
        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Sets the role name of the given {@link AssociationEnd}.
     *
     * @param cd the current {@link ClassDiagram}
     * @param associationEnd the {@link AssociationEnd} the role name should be changed for
     * @param roleName The new role name of the association end
     */
    public void setRoleName(ClassDiagram cd, AssociationEnd associationEnd, String roleName) {
        if (roleName.equals(associationEnd.getName())) {
            // do nothing if role name is unchanged
            return;
        }
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(associationEnd);
        CompoundCommand compoundCommand = new CompoundCommand();
        
        compoundCommand.append(SetCommand.create(editingDomain, associationEnd,
                CdmPackage.Literals.NAMED_ELEMENT__NAME, roleName));
        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Switches the navigable property of the given association end.
     * In case the opposite association end isn't navigable, both of the ends are switched
     *
     * @param cd the current {@link ClassDiagram}
     * @param associationEnd the {@link AssociationEnd} the navigable property should be switched of
     * @param oppositeEnd the opposite {@link AssociationEnd} from which the navigable property should be switched of
     */
    public void switchNavigable(ClassDiagram cd, AssociationEnd associationEnd, AssociationEnd oppositeEnd) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(associationEnd);
        CompoundCommand compoundCommand;

        if (associationEnd.isNavigable()) {
            if (oppositeEnd.isNavigable()) {
                compoundCommand = new UnidirectionalCompoundCommand(cd, associationEnd, oppositeEnd);
            } else {
                compoundCommand = new BidirectionalCompoundCommand(cd, associationEnd, oppositeEnd);
            }
        } else {
            // Switch navigability
            compoundCommand = new UnidirectionalCompoundCommand(cd, associationEnd, oppositeEnd);
            compoundCommand.append(SetCommand.create(editingDomain, associationEnd,
                    CdmPackage.Literals.ASSOCIATION_END__NAVIGABLE, !associationEnd.isNavigable()));
        }

        compoundCommand.append(SetCommand.create(editingDomain, oppositeEnd,
                CdmPackage.Literals.ASSOCIATION_END__NAVIGABLE, !oppositeEnd.isNavigable()));

        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Sets the reference type of the given association end to the new reference type.
     *
     * @param associationEnd the {@link AssociationEnd} the reference type should be changed of
     * @param referenceType the new {@link ReferenceType} to set
     */
    public void setReferenceType(AssociationEnd associationEnd, ReferenceType referenceType) {
        doSet(associationEnd, CdmPackage.Literals.ASSOCIATION_END__REFERENCE_TYPE, referenceType);
    }

    /**
     * Switches the static property of the given association end.
     *
     * @param associationEnd the {@link AssociationEnd} the static property should be switched of
     */
    public void switchStatic(AssociationEnd associationEnd) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(associationEnd);
        CompoundCommand compoundCommand = new CompoundCommand();
        
        compoundCommand.append(SetCommand.create(editingDomain, associationEnd, 
                CdmPackage.Literals.STRUCTURAL_FEATURE__STATIC, !associationEnd.isStatic()));
        doExecute(editingDomain, compoundCommand);
    }
    
    /**
     * Switches the ordering property of the given association end.
     *
     * @param associationEnd the {@link AssociationEnd} the ordering property should be switched of
     */
    public void switchOrdering(AssociationEnd associationEnd) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(associationEnd);
        CompoundCommand compoundCommand = new CompoundCommand();
        
        compoundCommand.append(SetCommand.create(editingDomain, associationEnd, 
                CdmPackage.Literals.ASSOCIATION_END__ORDERED, !associationEnd.isOrdered()));
        doExecute(editingDomain, compoundCommand);
    }
    
    /**
     * Switches the uniqueness property of the given association end.
     *
     * @param associationEnd the {@link AssociationEnd} the uniqueness property should be switched of
     */
    public void switchUniqueness(AssociationEnd associationEnd) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(associationEnd);
        CompoundCommand compoundCommand = new CompoundCommand();
        
        compoundCommand.append(SetCommand.create(editingDomain, associationEnd, 
                CdmPackage.Literals.ASSOCIATION_END__UNIQUE, !associationEnd.isUnique()));
        doExecute(editingDomain, compoundCommand);
    }

    /**
     * A specialized compound command for creating and deleting association feature selection.
     * @author celinebensoussan
     *
     */
    public class AssociationCompoundCommand extends CompoundCommand {

        /**
         * The current cd.
         */
        protected ClassDiagram cd;

        /**
         * The association end.
         */
        protected AssociationEnd associationEnd;

        /**
         * The opposite end.
         */
        protected AssociationEnd oppositeEnd;

        /**
         * Constructor without selected features.
         * @param cd The current cd
         * @param associationEnd The association end
         * @param oppositeEnd The opposite end
         */
        public AssociationCompoundCommand(ClassDiagram cd, AssociationEnd associationEnd,
                AssociationEnd oppositeEnd) {
            this.cd = cd;
            this.associationEnd = associationEnd;
            this.oppositeEnd = oppositeEnd;
        }
    }

    /**
     * A compound command that extends the association compound command.
     * It is used to set the feature selection of a unidirectional association.
     * @author celinebensoussan
     *
     */
    public class UnidirectionalCompoundCommand extends AssociationCompoundCommand {

        /**
         * Constructor without selected features.
         * @param cd The current cd
         * @param associationEnd The association end
         * @param oppositeEnd The opposite end
         */
        public UnidirectionalCompoundCommand(ClassDiagram cd, AssociationEnd associationEnd,
                AssociationEnd oppositeEnd) {
            super(cd, associationEnd, oppositeEnd);
        }

        @Override
        public void execute() {
            super.execute();
        }
    }

    /**
     * A compound command that extends the association compound command.
     * It is used to set the feature selection of a bidirectional association.
     * @author celinebensoussan
     *
     */
    public class BidirectionalCompoundCommand extends AssociationCompoundCommand {

        /**
         * Constructor without selected features.
         * @param cd The current cd
         * @param associationEnd The association end
         * @param oppositeEnd The opposite end
         */
        public BidirectionalCompoundCommand(ClassDiagram cd, AssociationEnd associationEnd,
                AssociationEnd oppositeEnd) {
            super(cd, associationEnd, oppositeEnd);
        }
        
        @Override
        public void execute() {
            super.execute();
        }
    }
}