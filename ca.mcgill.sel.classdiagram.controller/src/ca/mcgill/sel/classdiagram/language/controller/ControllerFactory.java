package ca.mcgill.sel.classdiagram.language.controller;

/**
 * A factory to obtain all controllers.
 * 
 * @author mschoettle, yhattab
 */
public final class ControllerFactory {
    
    /**
     * The singleton instance of this factory.
     */
    public static final ControllerFactory INSTANCE = new ControllerFactory();
    
    private ClassDiagramController classDiagramController;
    private ClassController classController;
    private EnumController enumController;
    private AssociationController associationController;
    private ImplementationClassController implementationClassController;
    
    /**
     * Creates a new instance of {@link ControllerFactory}.
     */
    private ControllerFactory() {
        
    }
    
    /**
     * Returns the controller for {@link ca.mcgill.sel.classdiagram.ClassDiagram}s.
     * 
     * @return the controller for {@link ca.mcgill.sel.classdiagram.ClassDiagram}s
     */
    public ClassDiagramController getClassDiagramController() {
        if (classDiagramController == null) {
            classDiagramController = new ClassDiagramController();
        }
        
        return classDiagramController;
    }
    
    /**
     * Returns the controller for {@link ca.mcgill.sel.classdiagram.Class}s.
     * 
     * @return the controller for {@link ca.mcgill.sel.classdiagram.Class}s.
     */
    public ClassController getClassController() {
        if (classController == null) {
            classController = new ClassController();
        }
        
        return classController;
    }
    
    /**
     * Returns the controller for {@link ca.mcgill.sel.classdiagram.CDEnum}s.
     * 
     * @return the controller for {@link ca.mcgill.sel.classdiagram.CDEnum}s.
     */
    public EnumController getEnumController() {
        if (enumController == null) {
            enumController = new EnumController();
        }
        
        return enumController;
    }
    
    /**
     * Returns the controller for {@link ca.mcgill.sel.classdiagram.Association}s
     * and {@link ca.mcgill.sel.classdiagram.AssociationEnd}s.
     * 
     * @return the controller for {@link ca.mcgill.sel.classdiagram.Association}s
     *         and {@link ca.mcgill.sel.classdiagram.AssociationEnd}s
     */
    public AssociationController getAssociationController() {
        if (associationController == null) {
            associationController = new AssociationController();
        }
        
        return associationController;
    }
    
    /**
     * Returns the controller for {@link ca.mcgill.sel.classdiagram.ImplementationClass}es.
     * 
     * @return the controller for {@link ca.mcgill.sel.classdiagram.ImplementationClass}es.
     */
    public ImplementationClassController getImplementationClassController() {
        if (implementationClassController == null) {
            implementationClassController = new ImplementationClassController();
        }
        
        return implementationClassController;
    }
    
}
