package ca.mcgill.sel.classdiagram.language.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.CORECIElement;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.COREAction;
import ca.mcgill.sel.core.util.COREArtefactUtil;

import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.classdiagram.Classifier;
import ca.mcgill.sel.classdiagram.ImplementationClass;
import ca.mcgill.sel.classdiagram.LayoutElement;
import ca.mcgill.sel.classdiagram.NamedElement;
import ca.mcgill.sel.classdiagram.Note;
import ca.mcgill.sel.classdiagram.Operation;
import ca.mcgill.sel.classdiagram.TypeParameter;
import ca.mcgill.sel.classdiagram.language.controller.util.CdmLanguageAction;
import ca.mcgill.sel.classdiagram.util.CdmModelUtil;
import ca.mcgill.sel.classdiagram.Association;
import ca.mcgill.sel.classdiagram.AssociationEnd;
import ca.mcgill.sel.classdiagram.Attribute;
import ca.mcgill.sel.classdiagram.CDEnum;
import ca.mcgill.sel.classdiagram.CDEnumLiteral;
import ca.mcgill.sel.classdiagram.CdmFactory;
import ca.mcgill.sel.classdiagram.CdmPackage;
import ca.mcgill.sel.classdiagram.Class;

/**
 * The controller for {@link ClassDiagram}s.
 *
 * @author mschoettle
 * @author yhattab
 */
public class ClassDiagramController extends BaseController {

    /**
     * The default prefix for role names.
     */
    private static final String PREFIX_ROLE_NAME = "my";

    /**
     * Creates a new instance of {@link ClassDiagramController}.
     */
    protected ClassDiagramController() {
        // Prevent anyone outside this package to instantiate.
    }
    
    /*
     * COREPerspective Actions Validation Methods
     */
    
    public boolean canCreateNewClass(COREPerspective perspective) {
        for (COREAction action : perspective.getActions()) {
            if (action.getName().equals(CdmLanguageAction.ADD_CLASS.getName())) {
                return true;
            }
        }
        
        return false;
    }
    
    public boolean canCreateImplementationClass(COREPerspective perspective) {
        for (COREAction action : perspective.getActions()) {
            if (action.getName().equals(CdmLanguageAction.ADD_IMPLEMENTATION_CLASS.getName())) {
                return true;
            }
        }
        
        return false;
    }

    public boolean canRemoveClassifier(COREPerspective perspective) {
        for (COREAction action : perspective.getActions()) {
            if (action.getName().equals(CdmLanguageAction.DELETE_CLASS.getName())) {
                return true;
            }
        }
        
        return false;
    }
    
    public boolean canEditClassifier(COREPerspective perspective) {
        for (COREAction action : perspective.getActions()) {
            if (action.getName().equals(CdmLanguageAction.EDIT_CLASS.getName())) {
                return true;
            }
        }
        
        return false;
    }
    
    public boolean canCreateAssociation(COREPerspective perspective) {
        for (COREAction action : perspective.getActions()) {
            if (action.getName().equals(CdmLanguageAction.ADD_ASSOCIATION.getName())) {
                return true;
            }
        }
        
        return false;
    }
    
    public boolean canRemoveNote(COREPerspective perspective) {
        for (COREAction action : perspective.getActions()) {
            if (action.getName().equals(CdmLanguageAction.DELETE_NOTE.getName())) {
                return true;
            }
        }
        
        return false;
    }
    
    public boolean canCreateEnum(COREPerspective perspective) {
        for (COREAction action : perspective.getActions()) {
            if (action.getName().equals(CdmLanguageAction.ADD_ENUM.getName())) {
                return true;
            }
        }
        
        return false;
    }

    public boolean canCreateNewNote(COREPerspective perspective) {
        for (COREAction action : perspective.getActions()) {
            if (action.getName().equals(CdmLanguageAction.ADD_NOTE.getName())) {
                return true;
            }
        }
        
        return false;
    }

    public boolean canCreateNaryAssociation(COREPerspective perspective) {
        for (COREAction action : perspective.getActions()) {
            if (action.getName().equals(CdmLanguageAction.ADD_NARY_ASSOCIATION.getName())) {
                return true;
            }
        }
        
        return false;
    }
    
    
    /**
     * Creates a new class as well as its layout element.
     *
     * @param owner the {@link ClassDiagram} the class should be added to
     * @param name the name of the class
     * @param dataType whether the class is a data type, <code>true</code> if it is
     * @param x the x position of the class
     * @param y the y position of the class
     */
    public void createNewClass(ClassDiagram owner, String name, boolean dataType, boolean isInteface,
            float x, float y) {
        Class newClass = CdmFactory.eINSTANCE.createClass();
        newClass.setName(name);
        newClass.setDataType(dataType);

        LayoutElement layoutElement = CdmFactory.eINSTANCE.createLayoutElement();
        layoutElement.setX(x);
        layoutElement.setY(y);

        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);

        // Create commands.
        Command addClassCommand = AddCommand.create(editingDomain, owner, CdmPackage.Literals.CLASS_DIAGRAM__CLASSES,
                newClass);
        Command createElementMapCommand =
                createAddLayoutElementCommand(editingDomain, owner, newClass, layoutElement);

        // Create compound command.
        CompoundCommand compoundCommand = new CompoundCommand();
        // If the layout element is added first, when the structural view gets notified (about a new class)
        // access to the layout element is already possible.
        compoundCommand.append(addClassCommand);
        compoundCommand.append(createElementMapCommand);

        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Creates a new implementation class.
     *
     * @param owner the class diagram containing the implementation class
     * @param name name of the implementation class.
     * @param generics the type parameters of the implementation class
     * @param x location of the implementation class in the x-axis.
     * @param y location of the implementation class in the y-axis.
     * @param isInterface whether this should be an interface or not
     * @param isAbstract whether this class is abstract
     * @param superTypes the set of super types the class to create has
     * @param subTypes the set of sub types (existing in the structural view) the class to create has
     */
    // CHECKSTYLE:IGNORE ParameterNumberCheck: Can't split, because it is an API method and all information is required.
    public void createImplementationClass(ClassDiagram owner, String name, List<TypeParameter> generics,
            boolean isInterface, boolean isAbstract,
            float x, float y, Set<String> superTypes, Set<ImplementationClass> subTypes) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);
        CompoundCommand compoundCommand = new CompoundCommand();

        ImplementationClass implementationClass = createImplementationClassCommand(editingDomain, compoundCommand,
                owner, name, generics, isInterface, isAbstract, x, y);

        // If the class has generics, we cannot determine at this point whether there are super or sub types,
        // because the generic type of the TypeParameter's will be unset.
        if (generics.isEmpty()) {
            if (superTypes != null) {
                for (String className : superTypes) {
                    ImplementationClass superType = CdmModelUtil.getImplementationClassByName(owner, className);
                    if (superType != null) {
                        compoundCommand.append(AddCommand.create(editingDomain, implementationClass,
                                CdmPackage.Literals.CLASSIFIER__SUPER_TYPES, superType));
                    }
                }
            }

            if (subTypes != null) {
                for (ImplementationClass subType : subTypes) {
                    compoundCommand.append(AddCommand.create(editingDomain, subType,
                            CdmPackage.Literals.CLASSIFIER__SUPER_TYPES, implementationClass));
                }
            }
        }

        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Creates a new implementation class.
     *
     * @param editingDomain the {@link EditingDomain} domain
     * @param command the {@link CompoundCommand} additional commands should be appended to
     * @param owner the Class Diagram containing the implementation class
     * @param name name of the implementation class.
     * @param generics the type parameters of the implementation class
     * @param x location of the implementation class in the x-axis.
     * @param y location of the implementation class in the y-axis.
     * @param isInterface whether this should be an interface or not
     * @param isAbstract whether this class is abstract
     * @return the {@link ImplementationClass} that will be added by the command
     */
    // CHECKSTYLE:IGNORE ParameterNumberCheck: Splitting operation into several methods will affect readability.
    private ImplementationClass createImplementationClassCommand(EditingDomain editingDomain, CompoundCommand command,
            ClassDiagram owner, String name, List<TypeParameter> generics, boolean isInterface, boolean isAbstract,
            float x, float y) {
        ImplementationClass newImpClass = CdmFactory.eINSTANCE.createImplementationClass();

        String[] splitName = name.split("\\.");
        newImpClass.setName(splitName[Math.max(splitName.length - 1, 0)]);
        newImpClass.setInstanceClassName(name);
        if (isInterface) {
            newImpClass.setInterface(isInterface);
        } else if (isAbstract) {
            newImpClass.setAbstract(isAbstract);
        }

        newImpClass.getTypeParameters().addAll(generics);

        LayoutElement layoutElement = CdmFactory.eINSTANCE.createLayoutElement();
        layoutElement.setX(x);
        layoutElement.setY(y);

        Command addImpClassCommand = AddCommand.create(editingDomain, owner,
                CdmPackage.Literals.CLASS_DIAGRAM__CLASSES, newImpClass);
        Command createElementMapCommand =
                createAddLayoutElementCommand(editingDomain, owner, newImpClass, layoutElement);

        // If the layout element is added first, when the structural view gets notified (about a new class)
        // access to the layout element is already possible.
        command.append(addImpClassCommand);
        command.append(createElementMapCommand);

        return newImpClass;
    }

    /**
     * Removes the given classifier. Furthermore, removes this classifier from all other classes where this class is a
     * super type of. If the classifier is a {@link Class}, also all associations and corresponding association ends are
     * removed. 
     * Furthermore, removes the layout element & the {@link CORECIElement}
     *
     * @param classifier the classifier that should be removed
     */
    public void removeClassifier(Classifier classifier) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(classifier);
        ClassDiagram cd = (ClassDiagram) classifier.eContainer();

        CompoundCommand compoundCommand = new CompoundCommand();
        
        // Create commands for removing all the {@link CORECIElement}
        CORECIElement classCIElement = COREArtefactUtil.getCIElementFor(classifier);
        if (classCIElement != null) {
            compoundCommand.append(deleteCORECIElementCommand(classCIElement));
        }

        if (classifier instanceof Class) {
            for (Attribute attribute : ((Class) classifier).getAttributes()) {
            	CORECIElement ciElement = COREArtefactUtil.getCIElementFor(attribute);
            	if (ciElement != null) {
            		compoundCommand.append(deleteCORECIElementCommand(ciElement));
            	}
            }
        }
        
        // Create commands for removing all message views defining behaviour of operations of this class.
        // Also remove the {@link CORECIElement} from operations
        for (Operation operation : classifier.getOperations()) {
            CORECIElement operationCIElement = COREArtefactUtil.getCIElementFor(operation);
            if (operationCIElement != null) {
                compoundCommand.append(deleteCORECIElementCommand(operationCIElement));
            }
        }

        // Create commands for removing this class as super type of other classes or vice versa.
        for (Classifier currentClassifier : cd.getClasses()) {
            if (currentClassifier.getSuperTypes().contains(classifier)) {
                compoundCommand.append(RemoveCommand.create(editingDomain, currentClassifier,
                        CdmPackage.Literals.CLASSIFIER__SUPER_TYPES, classifier));
            } else if (classifier.getSuperTypes().contains(currentClassifier)) {
                compoundCommand.append(RemoveCommand.create(editingDomain, classifier,
                        CdmPackage.Literals.CLASSIFIER__SUPER_TYPES, currentClassifier));
            }
        }

        List<Association> seenAssociations = new ArrayList<Association>();

        // Create commands for removing associations.
        for (AssociationEnd associationEnd : classifier.getAssociationEnds()) {
            Association association = associationEnd.getAssoc();
            associationEnd = association.getEnds().get(0);
            AssociationEnd otherAssociationEnd = association.getEnds().get(1);

            // Check if we already removed the association.
            // This only happens if we have self-associations.
            boolean alreadySeen = false;
            for (Association otherAssociation : seenAssociations) {
                if (otherAssociation.getEnds().get(0) == associationEnd
                        && otherAssociation.getEnds().get(1) == otherAssociationEnd) {
                    alreadySeen = true;
                    break;
                } else if (otherAssociation.getEnds().get(1) == associationEnd
                        && otherAssociation.getEnds().get(0) == otherAssociationEnd) {
                    alreadySeen = true;
                    break;
                }
            }

            seenAssociations.add(associationEnd.getAssoc());

            if (!alreadySeen) {
                compoundCommand.append(ControllerFactory.INSTANCE.getAssociationController()
                        .deleteAssociation(cd, association, editingDomain));
            }
        }

        // Create command for removing ElementMap (includes the layout element).
        Command removeElementMapCommand = createRemoveLayoutElementCommand(editingDomain, cd, classifier);
        compoundCommand.append(removeElementMapCommand);

        // Create command for removing class.
        Command removeClassCommand = RemoveCommand.create(editingDomain, classifier);
        compoundCommand.append(removeClassCommand);

        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Moves each {@link Classifier} in a list to a new position. Each classifier has its own (independent) position.
     *
     * @param cd the {@link ClassDiagram} that contains the classifiers
     * @param positionMap a map from {@link Classifier} to {@link LayoutElement}, which contains the new position
     */
    public void moveClassifiers(ClassDiagram cd, Map<Classifier, LayoutElement> positionMap) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(cd);

        if (positionMap.size() > 0) {
            // Create compound command.
            CompoundCommand moveClassifiersCommand = new CompoundCommand();
    
            for (Entry<Classifier, LayoutElement> entry : positionMap.entrySet()) {
                // get all required values
                Classifier classifier = entry.getKey();
                float x = entry.getValue().getX();
                float y = entry.getValue().getY();
    
                Command classifierMoveCommand = createMoveCommand(editingDomain, cd, classifier, x, y);
                moveClassifiersCommand.append(classifierMoveCommand);
            }
    
            doExecute(editingDomain, moveClassifiersCommand);
        }
    }
    
    /**
     * Moves each {@link EObject} in a list to a new position. Each object has its own position.
     *
     * @param cd the {@link ClassDiagram} that contains the classifiers
     * @param positionMap a map from {@link EObject} to {@link LayoutElement}, which contains the new position
     */
    public void moveNonClassifiers(ClassDiagram cd, Map<EObject, LayoutElement> nonClassifierPositionMap) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(cd);

        if (nonClassifierPositionMap.size() > 0) {
            // Create compound command.
            CompoundCommand moveClassifiersCommand = new CompoundCommand();
    
            for (Entry<EObject, LayoutElement> entry : nonClassifierPositionMap.entrySet()) {
                // get all required values
                EObject object = entry.getKey();
                float x = entry.getValue().getX();
                float y = entry.getValue().getY();
    
                Command classifierMoveCommand = createMoveCommand(editingDomain, cd, object, x, y);
                moveClassifiersCommand.append(classifierMoveCommand);
            }
    
            doExecute(editingDomain, moveClassifiersCommand);
        }
    }

    /**
     * Creates an association between the two given classes. The association ends have default values.
     *
     * @param owner the {@link ClassDiagram} the association should be added to
     * @param from the first class
     * @param to the second class
     * @param bidirectional whether the association should be bidirectional or not. If to or the from
     *            is an implementation class, this is not taken in account.
     */
    public void createAssociation(ClassDiagram owner, Classifier from, Classifier to, boolean bidirectional) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);

        // create compound command
        CompoundCommand compoundCommand = new CompoundCommand();

        // create association
        Association association = CdmFactory.eINSTANCE.createAssociation();
        association.setName(from.getName() + "_" + to.getName());

        boolean toIsNavigable = to instanceof ImplementationClass ? false : bidirectional;
        boolean fromIsNavigable = from instanceof ImplementationClass ? false : bidirectional || !toIsNavigable;

        // create from association end
        AssociationEnd fromEnd = this.createAssociationEnd(association, from, to, fromIsNavigable);

        compoundCommand.appendAndExecute(AddCommand.create(editingDomain, from,
                CdmPackage.Literals.CLASSIFIER__ASSOCIATION_ENDS, fromEnd));

        // create to association end
        AssociationEnd toEnd = this.createAssociationEnd(association, to, from, toIsNavigable);

        compoundCommand.appendAndExecute(AddCommand.create(editingDomain, to,
                CdmPackage.Literals.CLASSIFIER__ASSOCIATION_ENDS, toEnd));

        // create command for association
        compoundCommand.appendAndExecute(AddCommand.create(editingDomain, owner,
                CdmPackage.Literals.CLASS_DIAGRAM__ASSOCIATIONS, association));
        
        // execute commands
        doExecute(editingDomain, compoundCommand);
    }
    
    /**
     * Creates a note link between the note and the given classe.
     *
     * @param owner the {@link ClassDiagram} the association should be added to
     * @param note the {@link Note} linked from
     * @param annotatedElement the annotated {@link NamedElement}
     */
    public void createAnnotation(ClassDiagram owner, Note note, NamedElement annotatedElement) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);

        if(!note.getNotedElement().contains(annotatedElement)) {
            Command addNotedElementCommand = AddCommand.create(editingDomain, note,
                    CdmPackage.Literals.NOTE__NOTED_ELEMENT, annotatedElement);
            // execute command
            doExecute(editingDomain, addNotedElementCommand);
        }
    }

    /**
     * Removes the given note and its associated layout element
     *
     * @param note the note that should be removed
     */
    public void removeNote(Note note) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(note);
        ClassDiagram cd = (ClassDiagram) note.eContainer();
        
        CompoundCommand compoundCommand = new CompoundCommand();
        
        // Create command for removing ElementMap (includes the layout element).
        Command removeElementMapCommand = createRemoveLayoutElementCommand(editingDomain, cd, note);
        compoundCommand.append(removeElementMapCommand);

        // Create command for removing note.
        Command removeNoteCommand = RemoveCommand.create(editingDomain, note);
        compoundCommand.append(removeNoteCommand);

        doExecute(editingDomain, compoundCommand);
    }
    
    /**
     * Remove a note link between the note and the given class.
     *
     * @param note the {@link Note} linked from
     * @param annotatedElement the annotated {@link NamedElement}
     */
    public void removeAnnotation(Note note, NamedElement annotatedElement) {
        ClassDiagram cd = (ClassDiagram) note.eContainer();
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(cd);
        
        Command removeNotedElementCommand = RemoveCommand.create(editingDomain, note,
                CdmPackage.Literals.NOTE__NOTED_ELEMENT, annotatedElement);
        
        // execute command
        doExecute(editingDomain, removeNotedElementCommand);
    }
    
    /**
     * Remove all the annotations of the given note
     *
     * @param note the {@link Note} linked from
     */
    public void removeAnnotations(Note note) {
        ClassDiagram cd = (ClassDiagram) note.eContainer();
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(cd);
        
        CompoundCommand compoundCommand = new CompoundCommand();
        
        if (note.getNotedElement().size() > 0) {
            for (NamedElement element : note.getNotedElement()) {
                Command removeNotedElementCommand = RemoveCommand.create(editingDomain, note,
                        CdmPackage.Literals.NOTE__NOTED_ELEMENT, element);
                compoundCommand.append(removeNotedElementCommand);
            }
                
            // execute command
            doExecute(editingDomain, compoundCommand);
        }
    }

    /**
     * Creates an association end for the classifier "from" and "to" given.
     *
     * @param association the {@link StructuralView} the association should be added to
     * @param from the first class
     * @param to the second class
     * @param navigable indicate if we want the end to be navigable.
     *
     * @return the association end created
     */
    private AssociationEnd createAssociationEnd(Association association, Classifier from, Classifier to,
            boolean navigable) {
        // create an association end
        AssociationEnd associationEnd = CdmFactory.eINSTANCE.createAssociationEnd();
        associationEnd.setAssoc(association);
        associationEnd.setLowerBound(1);
        associationEnd.setNavigable(navigable);

        // The class could have no name.
        if (to.getName() != null || to.getName().isEmpty()) {
            // The default association end name is "my" + name of the target class
            // To assign a unique name to the association end
            // we must make sure that the class currently does not have already an
            // association end with that name
            String potentialNamePrefix = PREFIX_ROLE_NAME + to.getName();
            String potentialName = potentialNamePrefix;
            int index = 1;

            //if from is NULL, we're creating nary association
            if (from != null) {
                EList<AssociationEnd> ends = from.getAssociationEnds();
                if (ends.size() > 0) {
                    int i = 0;
                    AssociationEnd e;
                    while (i < ends.size()) {
                        e = ends.get(i);
                        if (e.getName().equals(potentialName)) {
                            potentialName = potentialNamePrefix + index;
                            index++;
                            i = 0;
                        } else {
                            i++;
                        }
                    }
                }
            }
            associationEnd.setName(potentialName);
        }

        return associationEnd;
    }

    /**
     * Create an enum and its layout element.
     *
     * @param owner the {@link ClassDiagram} the enum should be added to
     * @param name Name of the enum
     * @param x Position in the Structural view
     * @param y Position in the Structural view
     */
    public void createEnum(ClassDiagram owner, String name, float x, float y) {
        createImplementationEnum(owner, name, null, x, y, null);
    }

    /**
     * Creates a new implementation enum.
     *
     * @param owner the {@link ClassDiagram} the enum should be added to
     * @param name the name of the enum
     * @param instanceClassName the fully qualified name of the enum
     * @param x the x position of the enum
     * @param y the y position of the enum
     * @param literals the list of literals the enum contains
     */
    public void createImplementationEnum(ClassDiagram owner, String name, String instanceClassName, float x, float y,
            List<String> literals) {
        // Create an CDEnum object
        CDEnum newEnum = CdmFactory.eINSTANCE.createCDEnum();
        newEnum.setName(name);

        if (instanceClassName != null && !instanceClassName.isEmpty()) {
            newEnum.setInstanceClassName(instanceClassName);
        }

        if (literals != null) {
            for (String literal : literals) {
                CDEnumLiteral newLiteral = CdmFactory.eINSTANCE.createCDEnumLiteral();
                newLiteral.setName(literal);
                newEnum.getLiterals().add(newLiteral);
            }
        }

        // Create the layout element
        LayoutElement layoutElement = CdmFactory.eINSTANCE.createLayoutElement();
        layoutElement.setX(x);
        layoutElement.setY(y);

        // Get editing domain
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);

        // Create commands.
        Command addEnumCommand = AddCommand.create(editingDomain, owner, CdmPackage.Literals.CLASS_DIAGRAM__TYPES,
                newEnum);
        Command createElementMapCommand =
                createAddLayoutElementCommand(editingDomain, owner, newEnum, layoutElement);

        // Create compound command.
        CompoundCommand compoundCommand = new CompoundCommand();
        compoundCommand.append(addEnumCommand);
        compoundCommand.append(createElementMapCommand);

        doExecute(editingDomain, compoundCommand);
        owner.getTypes().add(newEnum);
    }

    /**
     * Creates a new note.
     *
     * @param owner the {@link ClassDiagram} the enum should be added to
     * @param name the name of the note
     * @param x the x position of the note
     * @param y the y position of the note
     */
    public void createNewNote(ClassDiagram cd, float x, float y) {
        //Create note
        Note newNote = CdmFactory.eINSTANCE.createNote();

        //Create layout element
        LayoutElement layoutElement = CdmFactory.eINSTANCE.createLayoutElement();
        layoutElement.setX(x);
        layoutElement.setY(y);
        
        //Get editing domain
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(cd);
        
        //Create Commands
        Command addNoteCommand = AddCommand.create(editingDomain, cd, CdmPackage.Literals.CLASS_DIAGRAM__NOTES,
                newNote);
        Command createElementMapCommand =
                createAddLayoutElementCommand(editingDomain, cd, newNote, layoutElement);

        // Create compound command.
        CompoundCommand compoundCommand = new CompoundCommand();
        compoundCommand.append(addNoteCommand);
        compoundCommand.append(createElementMapCommand);

        doExecute(editingDomain, compoundCommand);
        cd.getNotes().add(newNote);       
    }

    /**
     * Creates an Nary association between the given classes. The association ends have default values and if specified
     * a new Class is created as the Association Class of the Nary Association.
     *
     * @param cd the {@link ClassDiagram} the association should be added to
     * @param associatedClassifiers the classifiers to associate
     * @param position where to create the nary association diamond.
     * @param createAssociationClass true if need to create a new class as the association class of this association
     */
    public void createNaryAssociation(ClassDiagram cd, Set<Classifier> associatedClassifiers, float x, float y, 
            boolean createAssociationClass) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(cd);
        
        //Create layout element for association
        LayoutElement layoutElement = CdmFactory.eINSTANCE.createLayoutElement();
        layoutElement.setX(x);
        layoutElement.setY(y);

        // create compound command
        CompoundCommand compoundCommand = new CompoundCommand();

        // create the nary association
        Association association = CdmFactory.eINSTANCE.createAssociation();
        String associationName = "";
        
        for (Classifier clazz : associatedClassifiers) {
            associationName += "_" + clazz.getName();
            
            // create association end
            AssociationEnd assocEnd = this.createAssociationEnd(association, null, clazz, true);

            compoundCommand.append(AddCommand.create(editingDomain, clazz,
                    CdmPackage.Literals.CLASSIFIER__ASSOCIATION_ENDS, assocEnd));
        }
        //remove first "_" and set as name
        association.setName(associationName.substring(1));
        
        //create association class if needed
        if (createAssociationClass) {
            Class associationClass = CdmFactory.eINSTANCE.createClass();
            associationClass.setName(associationName.substring(1) + "_Association_Class");
            
            LayoutElement classLayoutElement = CdmFactory.eINSTANCE.createLayoutElement();
            classLayoutElement.setX(x);
            classLayoutElement.setY(y);
            
            // Create class commands.
            Command addClassCommand = AddCommand.create(editingDomain, cd, CdmPackage.Literals.CLASS_DIAGRAM__CLASSES,
                    associationClass);
            Command createElementMapCommand =
                    createAddLayoutElementCommand(editingDomain, cd, associationClass, classLayoutElement);

            compoundCommand.append(addClassCommand);
            compoundCommand.append(createElementMapCommand);
            association.setAssociationClass(associationClass);
        }
        

        // create command for adding association to class diagram
        compoundCommand.append(AddCommand.create(editingDomain, cd,
                CdmPackage.Literals.CLASS_DIAGRAM__ASSOCIATIONS, association));
        
        Command createElementMapCommand = createAddLayoutElementCommand(editingDomain, cd, association, layoutElement);
        compoundCommand.append(createElementMapCommand);
      
        // execute commands
        doExecute(editingDomain, compoundCommand);
        
    }   
}
