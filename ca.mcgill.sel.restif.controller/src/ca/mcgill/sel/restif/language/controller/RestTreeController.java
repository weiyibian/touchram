package ca.mcgill.sel.restif.language.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.restif.AccessMethod;
import ca.mcgill.sel.restif.DynamicFragment;
import ca.mcgill.sel.restif.LayoutElement;
import ca.mcgill.sel.restif.MethodType;
import ca.mcgill.sel.restif.NamedElement;
import ca.mcgill.sel.restif.PathFragment;
import ca.mcgill.sel.restif.Resource;
import ca.mcgill.sel.restif.RestIF;
import ca.mcgill.sel.restif.RestifFactory;
import ca.mcgill.sel.restif.RestifPackage;
import ca.mcgill.sel.restif.StaticFragment;

public class RestTreeController extends BaseController {

    /**
     * Creates a new instance of {@link RestTreeController}.
     */
    protected RestTreeController() {
        // Prevent anyone outside this package to instantiate.
    }
    
    public void moveNamedElements(RestIF restIF, Map<NamedElement, LayoutElement> positionMap) {
        // TODO Auto-generated method stub
        
    }

    public void moveNonNamedElements(RestIF restIF, Map<EObject, LayoutElement> nonClassifierPositionMap) {
        // TODO Auto-generated method stub
        
    }
    
    public void createRoot(RestIF owner, String name, float x, float y) {
        StaticFragment root = RestifFactory.eINSTANCE.createStaticFragment();
        root.setName(name);
        
        LayoutElement layoutElement = RestifFactory.eINSTANCE.createLayoutElement();
        layoutElement.setX(x);
        layoutElement.setY(y);

        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);
        
        // Create commands.
        Command setRootCommand = SetCommand.create(editingDomain, owner, RestifPackage.Literals.REST_IF__ROOT, root);
        Command createElementMapCommand = createAddLayoutElementCommand(editingDomain, owner, root, layoutElement);
        
        // Create compound command.
        CompoundCommand compoundCommand = new CompoundCommand();

        compoundCommand.append(setRootCommand);
        compoundCommand.append(createElementMapCommand);
        
        doExecute(editingDomain, compoundCommand);
    }
    
    public void removePathFragment(PathFragment parent, PathFragment pathFragment, RestIF restIF, LinkedHashMap<PathFragment, LayoutElement> childrenToLayoutElementMap,
            LinkedHashMap<PathFragment, PathFragment> childrenToParentMap, LinkedHashMap<PathFragment, PathFragment> childrenToParentEditedMap) {
        
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(restIF);
        CompoundCommand compoundCommand = new CompoundCommand();
        
        // reverse the list to remove from the child first
        List<PathFragment> keySet = new ArrayList<>(childrenToParentMap.keySet());
        Collections.reverse(keySet);
        Set<PathFragment> reversedKeySet = new LinkedHashSet<>(keySet);
      
        for (PathFragment child : reversedKeySet) {
            compoundCommand.append(RemoveCommand.create(editingDomain, childrenToParentMap.get(child), RestifPackage.Literals.PATH_FRAGMENT__CHILD, child));
        }
               
        compoundCommand.append(RemoveCommand.create(editingDomain, parent, RestifPackage.Literals.PATH_FRAGMENT__CHILD, pathFragment));
        
        for (PathFragment child : childrenToLayoutElementMap.keySet()) {        
            editingDomain = EMFEditUtil.getEditingDomain(childrenToParentEditedMap.get(child));
            
            PathFragment newChild = null;
            
            if (child instanceof StaticFragment) {
                newChild = RestifFactory.eINSTANCE.createStaticFragment();
                ((StaticFragment) newChild).setName(((StaticFragment) child).getName());
            } else {
                newChild = RestifFactory.eINSTANCE.createDynamicFragment();
                ((DynamicFragment) newChild).setPlaceholder(((DynamicFragment) child).getPlaceholder());
            }
            
            
            // update hashmap with the new child value
            PathFragment toDeleteKey = null;
            ArrayList<PathFragment> toDeleteValues = new ArrayList<>();
            
            for (PathFragment pf : childrenToParentEditedMap.keySet()) {
                if (pf.equals(child)) {
                    toDeleteKey = pf;
                }
                
                if (childrenToParentEditedMap.get(pf).equals(child)) {
                    toDeleteValues.add(pf);
                }
            }
            
            childrenToParentEditedMap.put(newChild, childrenToParentEditedMap.remove(toDeleteKey));
            for (PathFragment pf : toDeleteValues) {
                childrenToParentEditedMap.put(pf, newChild);
            }
                        
            compoundCommand.append(AddCommand.create(editingDomain, childrenToParentEditedMap.get(newChild), RestifPackage.Literals.PATH_FRAGMENT__CHILD, newChild));
            compoundCommand.append(createAddLayoutElementCommand(editingDomain, restIF, newChild, childrenToLayoutElementMap.get(child)));
        }
        
        doExecute(editingDomain, compoundCommand);
    }

    public void addResource(RestIF owner, Resource resource) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);

        Command addCommand = AddCommand.create(editingDomain, owner, RestifPackage.Literals.RESOURCE, resource);
        doExecute(editingDomain, addCommand);
    }
    
    public void addAccessMethodToResource(Resource owner, MethodType methodType) {        
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);
        AccessMethod accessMethod = RestifFactory.eINSTANCE.createAccessMethod();
        accessMethod.setType(methodType);
        
        Command addCommand = AddCommand.create(editingDomain, owner, RestifPackage.Literals.ACCESS_METHOD, accessMethod);
        doExecute(editingDomain, addCommand);
    }
    
    public void removeAccessMethodFromResource(Resource resource, MethodType methodType) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(resource);
        CompoundCommand compoundCommand = new CompoundCommand();

        AccessMethod accessMethodToRemove = null;
        
        for (AccessMethod accessMethod : resource.getAccessmethod()) {
            if (accessMethod.getType() == methodType) {
                accessMethodToRemove = accessMethod;
            }
        }
        
        // Create remove Command.
        compoundCommand.append(RemoveCommand.create(editingDomain, accessMethodToRemove));

        doExecute(editingDomain, compoundCommand);
    }

    public void addChildStaticFragment(RestIF restIF, PathFragment parent, String name, float x, float y) {
        StaticFragment child = RestifFactory.eINSTANCE.createStaticFragment();
        child.setName(name);
        
        LayoutElement layoutElement = RestifFactory.eINSTANCE.createLayoutElement();
        layoutElement.setX(x);
        layoutElement.setY(y);
                
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(parent);
        
        // Create commands.
        Command addChildCommand = AddCommand.create(editingDomain, parent, RestifPackage.Literals.PATH_FRAGMENT__CHILD, child);
        Command createElementMapCommand = createAddLayoutElementCommand(editingDomain, restIF, child, layoutElement);
                
        // Create compound command.
        CompoundCommand compoundCommand = new CompoundCommand();
        
        compoundCommand.append(addChildCommand);
        compoundCommand.append(createElementMapCommand);
        
        doExecute(editingDomain, compoundCommand);
    }

    public void addChildDynamicFragment(RestIF restIF, PathFragment parent, String name, float x, float y) {
        DynamicFragment child = RestifFactory.eINSTANCE.createDynamicFragment();
        child.setPlaceholder(name);
        
        LayoutElement layoutElement = RestifFactory.eINSTANCE.createLayoutElement();
        layoutElement.setX(x);
        layoutElement.setY(y);
                
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(parent);
        
        // Create commands.
        Command addChildCommand = AddCommand.create(editingDomain, parent, RestifPackage.Literals.PATH_FRAGMENT__CHILD, child);
        Command createElementMapCommand = createAddLayoutElementCommand(editingDomain, restIF, child, layoutElement);
                
        // Create compound command.
        CompoundCommand compoundCommand = new CompoundCommand();
        
        compoundCommand.append(addChildCommand);
        compoundCommand.append(createElementMapCommand);
        
        doExecute(editingDomain, compoundCommand);
    }

    public void setPathFragmentName(PathFragment pathFragment, String name) {
        if (pathFragment instanceof StaticFragment) {
            name = "/" + name;
        } else if (pathFragment instanceof DynamicFragment) {
            name = "/{" + name + "}";
        }
        
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(pathFragment);

        Command setCommand = null;
        
        if (pathFragment instanceof StaticFragment) {
            setCommand = SetCommand.create(editingDomain, pathFragment, RestifPackage.Literals.STATIC_FRAGMENT__NAME, name);
        // DynamicFragment
        } else {
            setCommand = SetCommand.create(editingDomain, pathFragment, RestifPackage.Literals.DYNAMIC_FRAGMENT__PLACEHOLDER, name);
        }
        
        doExecute(editingDomain, setCommand);
    }
}
