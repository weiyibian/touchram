/**
 */
package ca.mcgill.sel.ucm;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Path Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link ca.mcgill.sel.ucm.PathNode#getSucc <em>Succ</em>}</li>
 *   <li>{@link ca.mcgill.sel.ucm.PathNode#getPred <em>Pred</em>}</li>
 *   <li>{@link ca.mcgill.sel.ucm.PathNode#getCompRef <em>Comp Ref</em>}</li>
 * </ul>
 * </p>
 *
 * @see ca.mcgill.sel.ucm.UCMPackage#getPathNode()
 * @model
 * @generated
 */
public interface PathNode extends UCMModelElement {
    /**
     * Returns the value of the '<em><b>Succ</b></em>' reference list.
     * The list contents are of type {@link ca.mcgill.sel.ucm.NodeConnection}.
     * It is bidirectional and its opposite is '{@link ca.mcgill.sel.ucm.NodeConnection#getSource <em>Source</em>}'.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Succ</em>' reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Succ</em>' reference list.
     * @see ca.mcgill.sel.ucm.UCMPackage#getPathNode_Succ()
     * @see ca.mcgill.sel.ucm.NodeConnection#getSource
     * @model opposite="source"
     * @generated
     */
    EList<NodeConnection> getSucc();

    /**
     * Returns the value of the '<em><b>Pred</b></em>' reference list.
     * The list contents are of type {@link ca.mcgill.sel.ucm.NodeConnection}.
     * It is bidirectional and its opposite is '{@link ca.mcgill.sel.ucm.NodeConnection#getTarget <em>Target</em>}'.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Pred</em>' reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Pred</em>' reference list.
     * @see ca.mcgill.sel.ucm.UCMPackage#getPathNode_Pred()
     * @see ca.mcgill.sel.ucm.NodeConnection#getTarget
     * @model opposite="target"
     * @generated
     */
    EList<NodeConnection> getPred();

    /**
     * Returns the value of the '<em><b>Comp Ref</b></em>' reference.
     * It is bidirectional and its opposite is '{@link ca.mcgill.sel.ucm.ComponentRef#getNodes <em>Nodes</em>}'.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Comp Ref</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Comp Ref</em>' reference.
     * @see #setCompRef(ComponentRef)
     * @see ca.mcgill.sel.ucm.UCMPackage#getPathNode_CompRef()
     * @see ca.mcgill.sel.ucm.ComponentRef#getNodes
     * @model opposite="nodes"
     * @generated
     */
    ComponentRef getCompRef();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.ucm.PathNode#getCompRef <em>Comp Ref</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Comp Ref</em>' reference.
     * @see #getCompRef()
     * @generated
     */
    void setCompRef(ComponentRef value);

} // PathNode
