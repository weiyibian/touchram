/**
 */
package ca.mcgill.sel.ucm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Responsibility Ref</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link ca.mcgill.sel.ucm.ResponsibilityRef#getRespDef <em>Resp Def</em>}</li>
 * </ul>
 * </p>
 *
 * @see ca.mcgill.sel.ucm.UCMPackage#getResponsibilityRef()
 * @model
 * @generated
 */
public interface ResponsibilityRef extends PathNode {
    /**
     * Returns the value of the '<em><b>Resp Def</b></em>' reference.
     * It is bidirectional and its opposite is '{@link ca.mcgill.sel.ucm.Responsibility#getRespsRefs <em>Resps Refs</em>}'.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Resp Def</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Resp Def</em>' reference.
     * @see #setRespDef(Responsibility)
     * @see ca.mcgill.sel.ucm.UCMPackage#getResponsibilityRef_RespDef()
     * @see ca.mcgill.sel.ucm.Responsibility#getRespsRefs
     * @model opposite="respsRefs" required="true"
     * @generated
     */
    Responsibility getRespDef();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.ucm.ResponsibilityRef#getRespDef <em>Resp Def</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Resp Def</em>' reference.
     * @see #getRespDef()
     * @generated
     */
    void setRespDef(Responsibility value);

} // ResponsibilityRef
