/**
 */
package ca.mcgill.sel.ucm.impl;

import ca.mcgill.sel.core.impl.COREModelImpl;

import ca.mcgill.sel.ucm.Component;
import ca.mcgill.sel.ucm.ComponentRef;
import ca.mcgill.sel.ucm.NodeConnection;
import ca.mcgill.sel.ucm.PathNode;
import ca.mcgill.sel.ucm.Responsibility;
import ca.mcgill.sel.ucm.UCMMap;
import ca.mcgill.sel.ucm.UCMPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Map</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link ca.mcgill.sel.ucm.impl.UCMMapImpl#getConnections <em>Connections</em>}</li>
 *   <li>{@link ca.mcgill.sel.ucm.impl.UCMMapImpl#getNodes <em>Nodes</em>}</li>
 *   <li>{@link ca.mcgill.sel.ucm.impl.UCMMapImpl#getResps <em>Resps</em>}</li>
 *   <li>{@link ca.mcgill.sel.ucm.impl.UCMMapImpl#getComps <em>Comps</em>}</li>
 *   <li>{@link ca.mcgill.sel.ucm.impl.UCMMapImpl#getCompRefs <em>Comp Refs</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class UCMMapImpl extends COREModelImpl implements UCMMap {
    /**
     * The cached value of the '{@link #getConnections() <em>Connections</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getConnections()
     * @generated
     * @ordered
     */
    protected EList<NodeConnection> connections;

    /**
     * The cached value of the '{@link #getNodes() <em>Nodes</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getNodes()
     * @generated
     * @ordered
     */
    protected EList<PathNode> nodes;

    /**
     * The cached value of the '{@link #getResps() <em>Resps</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getResps()
     * @generated
     * @ordered
     */
    protected EList<Responsibility> resps;

    /**
     * The cached value of the '{@link #getComps() <em>Comps</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getComps()
     * @generated
     * @ordered
     */
    protected EList<Component> comps;

    /**
     * The cached value of the '{@link #getCompRefs() <em>Comp Refs</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getCompRefs()
     * @generated
     * @ordered
     */
    protected EList<ComponentRef> compRefs;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected UCMMapImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return UCMPackage.Literals.UCM_MAP;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<NodeConnection> getConnections() {
        if (connections == null) {
            connections = new EObjectContainmentEList<NodeConnection>(NodeConnection.class, this, UCMPackage.UCM_MAP__CONNECTIONS);
        }
        return connections;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<PathNode> getNodes() {
        if (nodes == null) {
            nodes = new EObjectContainmentEList<PathNode>(PathNode.class, this, UCMPackage.UCM_MAP__NODES);
        }
        return nodes;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<Responsibility> getResps() {
        if (resps == null) {
            resps = new EObjectContainmentEList<Responsibility>(Responsibility.class, this, UCMPackage.UCM_MAP__RESPS);
        }
        return resps;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<Component> getComps() {
        if (comps == null) {
            comps = new EObjectContainmentEList<Component>(Component.class, this, UCMPackage.UCM_MAP__COMPS);
        }
        return comps;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<ComponentRef> getCompRefs() {
        if (compRefs == null) {
            compRefs = new EObjectContainmentEList<ComponentRef>(ComponentRef.class, this, UCMPackage.UCM_MAP__COMP_REFS);
        }
        return compRefs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case UCMPackage.UCM_MAP__CONNECTIONS:
                return ((InternalEList<?>)getConnections()).basicRemove(otherEnd, msgs);
            case UCMPackage.UCM_MAP__NODES:
                return ((InternalEList<?>)getNodes()).basicRemove(otherEnd, msgs);
            case UCMPackage.UCM_MAP__RESPS:
                return ((InternalEList<?>)getResps()).basicRemove(otherEnd, msgs);
            case UCMPackage.UCM_MAP__COMPS:
                return ((InternalEList<?>)getComps()).basicRemove(otherEnd, msgs);
            case UCMPackage.UCM_MAP__COMP_REFS:
                return ((InternalEList<?>)getCompRefs()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case UCMPackage.UCM_MAP__CONNECTIONS:
                return getConnections();
            case UCMPackage.UCM_MAP__NODES:
                return getNodes();
            case UCMPackage.UCM_MAP__RESPS:
                return getResps();
            case UCMPackage.UCM_MAP__COMPS:
                return getComps();
            case UCMPackage.UCM_MAP__COMP_REFS:
                return getCompRefs();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case UCMPackage.UCM_MAP__CONNECTIONS:
                getConnections().clear();
                getConnections().addAll((Collection<? extends NodeConnection>)newValue);
                return;
            case UCMPackage.UCM_MAP__NODES:
                getNodes().clear();
                getNodes().addAll((Collection<? extends PathNode>)newValue);
                return;
            case UCMPackage.UCM_MAP__RESPS:
                getResps().clear();
                getResps().addAll((Collection<? extends Responsibility>)newValue);
                return;
            case UCMPackage.UCM_MAP__COMPS:
                getComps().clear();
                getComps().addAll((Collection<? extends Component>)newValue);
                return;
            case UCMPackage.UCM_MAP__COMP_REFS:
                getCompRefs().clear();
                getCompRefs().addAll((Collection<? extends ComponentRef>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case UCMPackage.UCM_MAP__CONNECTIONS:
                getConnections().clear();
                return;
            case UCMPackage.UCM_MAP__NODES:
                getNodes().clear();
                return;
            case UCMPackage.UCM_MAP__RESPS:
                getResps().clear();
                return;
            case UCMPackage.UCM_MAP__COMPS:
                getComps().clear();
                return;
            case UCMPackage.UCM_MAP__COMP_REFS:
                getCompRefs().clear();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case UCMPackage.UCM_MAP__CONNECTIONS:
                return connections != null && !connections.isEmpty();
            case UCMPackage.UCM_MAP__NODES:
                return nodes != null && !nodes.isEmpty();
            case UCMPackage.UCM_MAP__RESPS:
                return resps != null && !resps.isEmpty();
            case UCMPackage.UCM_MAP__COMPS:
                return comps != null && !comps.isEmpty();
            case UCMPackage.UCM_MAP__COMP_REFS:
                return compRefs != null && !compRefs.isEmpty();
        }
        return super.eIsSet(featureID);
    }

} //UCMMapImpl
