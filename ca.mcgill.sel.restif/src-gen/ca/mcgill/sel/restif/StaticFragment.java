/**
 */
package ca.mcgill.sel.restif;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Static Fragment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.restif.StaticFragment#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.restif.RestifPackage#getStaticFragment()
 * @model
 * @generated
 */
public interface StaticFragment extends PathFragment {
    /**
     * Returns the value of the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Name</em>' attribute.
     * @see #setName(String)
     * @see ca.mcgill.sel.restif.RestifPackage#getStaticFragment_Name()
     * @model
     * @generated
     */
    String getName();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.restif.StaticFragment#getName <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Name</em>' attribute.
     * @see #getName()
     * @generated
     */
    void setName(String value);

} // StaticFragment
