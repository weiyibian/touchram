/**
 */
package ca.mcgill.sel.restif;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Access Method</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.restif.AccessMethod#getInputParameter <em>Input Parameter</em>}</li>
 *   <li>{@link ca.mcgill.sel.restif.AccessMethod#getResult <em>Result</em>}</li>
 *   <li>{@link ca.mcgill.sel.restif.AccessMethod#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.restif.RestifPackage#getAccessMethod()
 * @model
 * @generated
 */
public interface AccessMethod extends EObject {
    /**
     * Returns the value of the '<em><b>Input Parameter</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.restif.Parameter}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Input Parameter</em>' containment reference list.
     * @see ca.mcgill.sel.restif.RestifPackage#getAccessMethod_InputParameter()
     * @model containment="true"
     * @generated
     */
    EList<Parameter> getInputParameter();

    /**
     * Returns the value of the '<em><b>Result</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Result</em>' containment reference.
     * @see #setResult(Body)
     * @see ca.mcgill.sel.restif.RestifPackage#getAccessMethod_Result()
     * @model containment="true"
     * @generated
     */
    Body getResult();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.restif.AccessMethod#getResult <em>Result</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Result</em>' containment reference.
     * @see #getResult()
     * @generated
     */
    void setResult(Body value);

    /**
     * Returns the value of the '<em><b>Type</b></em>' attribute.
     * The literals are from the enumeration {@link ca.mcgill.sel.restif.MethodType}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Type</em>' attribute.
     * @see ca.mcgill.sel.restif.MethodType
     * @see #setType(MethodType)
     * @see ca.mcgill.sel.restif.RestifPackage#getAccessMethod_Type()
     * @model required="true"
     * @generated
     */
    MethodType getType();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.restif.AccessMethod#getType <em>Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Type</em>' attribute.
     * @see ca.mcgill.sel.restif.MethodType
     * @see #getType()
     * @generated
     */
    void setType(MethodType value);

} // AccessMethod
