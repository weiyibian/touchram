/**
 */
package ca.mcgill.sel.restif;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Body</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.restif.Body#getEncoding <em>Encoding</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.restif.RestifPackage#getBody()
 * @model
 * @generated
 */
public interface Body extends Parameter {

    /**
     * Returns the value of the '<em><b>Encoding</b></em>' attribute.
     * The literals are from the enumeration {@link ca.mcgill.sel.restif.Encoding}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Encoding</em>' attribute.
     * @see ca.mcgill.sel.restif.Encoding
     * @see #setEncoding(Encoding)
     * @see ca.mcgill.sel.restif.RestifPackage#getBody_Encoding()
     * @model
     * @generated
     */
    Encoding getEncoding();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.restif.Body#getEncoding <em>Encoding</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Encoding</em>' attribute.
     * @see ca.mcgill.sel.restif.Encoding
     * @see #getEncoding()
     * @generated
     */
    void setEncoding(Encoding value);
} // Body
