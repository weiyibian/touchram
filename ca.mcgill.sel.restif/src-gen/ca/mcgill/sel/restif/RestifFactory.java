/**
 */
package ca.mcgill.sel.restif;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see ca.mcgill.sel.restif.RestifPackage
 * @generated
 */
public interface RestifFactory extends EFactory {
    /**
     * The singleton instance of the factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    RestifFactory eINSTANCE = ca.mcgill.sel.restif.impl.RestifFactoryImpl.init();

    /**
     * Returns a new object of class '<em>Rest IF</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Rest IF</em>'.
     * @generated
     */
    RestIF createRestIF();

    /**
     * Returns a new object of class '<em>Access Method</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Access Method</em>'.
     * @generated
     */
    AccessMethod createAccessMethod();

    /**
     * Returns a new object of class '<em>Static Fragment</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Static Fragment</em>'.
     * @generated
     */
    StaticFragment createStaticFragment();

    /**
     * Returns a new object of class '<em>Dynamic Fragment</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Dynamic Fragment</em>'.
     * @generated
     */
    DynamicFragment createDynamicFragment();

    /**
     * Returns a new object of class '<em>Resource</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Resource</em>'.
     * @generated
     */
    Resource createResource();

    /**
     * Returns a new object of class '<em>Header Parameter</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Header Parameter</em>'.
     * @generated
     */
    HeaderParameter createHeaderParameter();

    /**
     * Returns a new object of class '<em>Body</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Body</em>'.
     * @generated
     */
    Body createBody();

    /**
     * Returns a new object of class '<em>Layout</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Layout</em>'.
     * @generated
     */
    Layout createLayout();

    /**
     * Returns a new object of class '<em>Layout Element</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Layout Element</em>'.
     * @generated
     */
    LayoutElement createLayoutElement();

    /**
     * Returns the package supported by this factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the package supported by this factory.
     * @generated
     */
    RestifPackage getRestifPackage();

} //RestifFactory
