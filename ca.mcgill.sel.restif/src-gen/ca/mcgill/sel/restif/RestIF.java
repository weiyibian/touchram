/**
 */
package ca.mcgill.sel.restif;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Rest IF</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.restif.RestIF#getRoot <em>Root</em>}</li>
 *   <li>{@link ca.mcgill.sel.restif.RestIF#getResource <em>Resource</em>}</li>
 *   <li>{@link ca.mcgill.sel.restif.RestIF#getLayout <em>Layout</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.restif.RestifPackage#getRestIF()
 * @model
 * @generated
 */
public interface RestIF extends NamedElement {
    /**
     * Returns the value of the '<em><b>Root</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Root</em>' containment reference.
     * @see #setRoot(PathFragment)
     * @see ca.mcgill.sel.restif.RestifPackage#getRestIF_Root()
     * @model containment="true" required="true"
     * @generated
     */
    PathFragment getRoot();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.restif.RestIF#getRoot <em>Root</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Root</em>' containment reference.
     * @see #getRoot()
     * @generated
     */
    void setRoot(PathFragment value);

    /**
     * Returns the value of the '<em><b>Resource</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.restif.Resource}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Resource</em>' containment reference list.
     * @see ca.mcgill.sel.restif.RestifPackage#getRestIF_Resource()
     * @model containment="true" required="true"
     * @generated
     */
    EList<Resource> getResource();

    /**
     * Returns the value of the '<em><b>Layout</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Layout</em>' containment reference.
     * @see #setLayout(Layout)
     * @see ca.mcgill.sel.restif.RestifPackage#getRestIF_Layout()
     * @model containment="true"
     * @generated
     */
    Layout getLayout();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.restif.RestIF#getLayout <em>Layout</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Layout</em>' containment reference.
     * @see #getLayout()
     * @generated
     */
    void setLayout(Layout value);

} // RestIF
