/**
 */
package ca.mcgill.sel.restif.impl;

import ca.mcgill.sel.restif.AccessMethod;
import ca.mcgill.sel.restif.Body;
import ca.mcgill.sel.restif.MethodType;
import ca.mcgill.sel.restif.Parameter;
import ca.mcgill.sel.restif.RestifPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Access Method</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.restif.impl.AccessMethodImpl#getInputParameter <em>Input Parameter</em>}</li>
 *   <li>{@link ca.mcgill.sel.restif.impl.AccessMethodImpl#getResult <em>Result</em>}</li>
 *   <li>{@link ca.mcgill.sel.restif.impl.AccessMethodImpl#getType <em>Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AccessMethodImpl extends MinimalEObjectImpl.Container implements AccessMethod {
    /**
     * The cached value of the '{@link #getInputParameter() <em>Input Parameter</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getInputParameter()
     * @generated
     * @ordered
     */
    protected EList<Parameter> inputParameter;

    /**
     * The cached value of the '{@link #getResult() <em>Result</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getResult()
     * @generated
     * @ordered
     */
    protected Body result;

    /**
     * The default value of the '{@link #getType() <em>Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getType()
     * @generated
     * @ordered
     */
    protected static final MethodType TYPE_EDEFAULT = MethodType.GET;

    /**
     * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getType()
     * @generated
     * @ordered
     */
    protected MethodType type = TYPE_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected AccessMethodImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return RestifPackage.Literals.ACCESS_METHOD;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList<Parameter> getInputParameter() {
        if (inputParameter == null) {
            inputParameter = new EObjectContainmentEList<Parameter>(Parameter.class, this, RestifPackage.ACCESS_METHOD__INPUT_PARAMETER);
        }
        return inputParameter;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Body getResult() {
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public NotificationChain basicSetResult(Body newResult, NotificationChain msgs) {
        Body oldResult = result;
        result = newResult;
        if (eNotificationRequired()) {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, RestifPackage.ACCESS_METHOD__RESULT, oldResult, newResult);
            if (msgs == null) msgs = notification; else msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setResult(Body newResult) {
        if (newResult != result) {
            NotificationChain msgs = null;
            if (result != null)
                msgs = ((InternalEObject)result).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - RestifPackage.ACCESS_METHOD__RESULT, null, msgs);
            if (newResult != null)
                msgs = ((InternalEObject)newResult).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - RestifPackage.ACCESS_METHOD__RESULT, null, msgs);
            msgs = basicSetResult(newResult, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, RestifPackage.ACCESS_METHOD__RESULT, newResult, newResult));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public MethodType getType() {
        return type;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setType(MethodType newType) {
        MethodType oldType = type;
        type = newType == null ? TYPE_EDEFAULT : newType;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, RestifPackage.ACCESS_METHOD__TYPE, oldType, type));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case RestifPackage.ACCESS_METHOD__INPUT_PARAMETER:
                return ((InternalEList<?>)getInputParameter()).basicRemove(otherEnd, msgs);
            case RestifPackage.ACCESS_METHOD__RESULT:
                return basicSetResult(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case RestifPackage.ACCESS_METHOD__INPUT_PARAMETER:
                return getInputParameter();
            case RestifPackage.ACCESS_METHOD__RESULT:
                return getResult();
            case RestifPackage.ACCESS_METHOD__TYPE:
                return getType();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case RestifPackage.ACCESS_METHOD__INPUT_PARAMETER:
                getInputParameter().clear();
                getInputParameter().addAll((Collection<? extends Parameter>)newValue);
                return;
            case RestifPackage.ACCESS_METHOD__RESULT:
                setResult((Body)newValue);
                return;
            case RestifPackage.ACCESS_METHOD__TYPE:
                setType((MethodType)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case RestifPackage.ACCESS_METHOD__INPUT_PARAMETER:
                getInputParameter().clear();
                return;
            case RestifPackage.ACCESS_METHOD__RESULT:
                setResult((Body)null);
                return;
            case RestifPackage.ACCESS_METHOD__TYPE:
                setType(TYPE_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case RestifPackage.ACCESS_METHOD__INPUT_PARAMETER:
                return inputParameter != null && !inputParameter.isEmpty();
            case RestifPackage.ACCESS_METHOD__RESULT:
                return result != null;
            case RestifPackage.ACCESS_METHOD__TYPE:
                return type != TYPE_EDEFAULT;
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (type: ");
        result.append(type);
        result.append(')');
        return result.toString();
    }

} //AccessMethodImpl
