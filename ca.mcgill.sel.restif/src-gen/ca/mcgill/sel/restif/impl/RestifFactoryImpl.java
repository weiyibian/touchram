/**
 */
package ca.mcgill.sel.restif.impl;

import ca.mcgill.sel.restif.*;

import java.util.Map;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class RestifFactoryImpl extends EFactoryImpl implements RestifFactory {
    /**
     * Creates the default factory implementation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public static RestifFactory init() {
        try {
            RestifFactory theRestifFactory = (RestifFactory)EPackage.Registry.INSTANCE.getEFactory(RestifPackage.eNS_URI);
            if (theRestifFactory != null) {
                return theRestifFactory;
            }
        }
        catch (Exception exception) {
            EcorePlugin.INSTANCE.log(exception);
        }
        return new RestifFactoryImpl();
    }

    /**
     * Creates an instance of the factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public RestifFactoryImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EObject create(EClass eClass) {
        switch (eClass.getClassifierID()) {
            case RestifPackage.REST_IF: return createRestIF();
            case RestifPackage.ACCESS_METHOD: return createAccessMethod();
            case RestifPackage.STATIC_FRAGMENT: return createStaticFragment();
            case RestifPackage.DYNAMIC_FRAGMENT: return createDynamicFragment();
            case RestifPackage.RESOURCE: return createResource();
            case RestifPackage.HEADER_PARAMETER: return createHeaderParameter();
            case RestifPackage.BODY: return createBody();
            case RestifPackage.LAYOUT: return createLayout();
            case RestifPackage.CONTAINER_MAP: return (EObject)createContainerMap();
            case RestifPackage.ELEMENT_MAP: return (EObject)createElementMap();
            case RestifPackage.LAYOUT_ELEMENT: return createLayoutElement();
            default:
                throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object createFromString(EDataType eDataType, String initialValue) {
        switch (eDataType.getClassifierID()) {
            case RestifPackage.METHOD_TYPE:
                return createMethodTypeFromString(eDataType, initialValue);
            case RestifPackage.ENCODING:
                return createEncodingFromString(eDataType, initialValue);
            default:
                throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String convertToString(EDataType eDataType, Object instanceValue) {
        switch (eDataType.getClassifierID()) {
            case RestifPackage.METHOD_TYPE:
                return convertMethodTypeToString(eDataType, instanceValue);
            case RestifPackage.ENCODING:
                return convertEncodingToString(eDataType, instanceValue);
            default:
                throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public RestIF createRestIF() {
        RestIFImpl restIF = new RestIFImpl();
        return restIF;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public AccessMethod createAccessMethod() {
        AccessMethodImpl accessMethod = new AccessMethodImpl();
        return accessMethod;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public StaticFragment createStaticFragment() {
        StaticFragmentImpl staticFragment = new StaticFragmentImpl();
        return staticFragment;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public DynamicFragment createDynamicFragment() {
        DynamicFragmentImpl dynamicFragment = new DynamicFragmentImpl();
        return dynamicFragment;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Resource createResource() {
        ResourceImpl resource = new ResourceImpl();
        return resource;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public HeaderParameter createHeaderParameter() {
        HeaderParameterImpl headerParameter = new HeaderParameterImpl();
        return headerParameter;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Body createBody() {
        BodyImpl body = new BodyImpl();
        return body;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Layout createLayout() {
        LayoutImpl layout = new LayoutImpl();
        return layout;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public Map.Entry<EObject, EMap<EObject, LayoutElement>> createContainerMap() {
        ContainerMapImpl containerMap = new ContainerMapImpl();
        return containerMap;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public Map.Entry<EObject, LayoutElement> createElementMap() {
        ElementMapImpl elementMap = new ElementMapImpl();
        return elementMap;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public LayoutElement createLayoutElement() {
        LayoutElementImpl layoutElement = new LayoutElementImpl();
        return layoutElement;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public MethodType createMethodTypeFromString(EDataType eDataType, String initialValue) {
        MethodType result = MethodType.get(initialValue);
        if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertMethodTypeToString(EDataType eDataType, Object instanceValue) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public Encoding createEncodingFromString(EDataType eDataType, String initialValue) {
        Encoding result = Encoding.get(initialValue);
        if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertEncodingToString(EDataType eDataType, Object instanceValue) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public RestifPackage getRestifPackage() {
        return (RestifPackage)getEPackage();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @deprecated
     * @generated
     */
    @Deprecated
    public static RestifPackage getPackage() {
        return RestifPackage.eINSTANCE;
    }

} //RestifFactoryImpl
