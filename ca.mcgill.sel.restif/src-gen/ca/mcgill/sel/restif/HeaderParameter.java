/**
 */
package ca.mcgill.sel.restif;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Header Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.restif.HeaderParameter#getDefaultValue <em>Default Value</em>}</li>
 *   <li>{@link ca.mcgill.sel.restif.HeaderParameter#getName <em>Name</em>}</li>
 *   <li>{@link ca.mcgill.sel.restif.HeaderParameter#isMandatory <em>Mandatory</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.restif.RestifPackage#getHeaderParameter()
 * @model
 * @generated
 */
public interface HeaderParameter extends Parameter {
    /**
     * Returns the value of the '<em><b>Default Value</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Default Value</em>' attribute.
     * @see #setDefaultValue(String)
     * @see ca.mcgill.sel.restif.RestifPackage#getHeaderParameter_DefaultValue()
     * @model
     * @generated
     */
    String getDefaultValue();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.restif.HeaderParameter#getDefaultValue <em>Default Value</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Default Value</em>' attribute.
     * @see #getDefaultValue()
     * @generated
     */
    void setDefaultValue(String value);

    /**
     * Returns the value of the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Name</em>' attribute.
     * @see #setName(String)
     * @see ca.mcgill.sel.restif.RestifPackage#getHeaderParameter_Name()
     * @model
     * @generated
     */
    String getName();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.restif.HeaderParameter#getName <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Name</em>' attribute.
     * @see #getName()
     * @generated
     */
    void setName(String value);

    /**
     * Returns the value of the '<em><b>Mandatory</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Mandatory</em>' attribute.
     * @see #setMandatory(boolean)
     * @see ca.mcgill.sel.restif.RestifPackage#getHeaderParameter_Mandatory()
     * @model
     * @generated
     */
    boolean isMandatory();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.restif.HeaderParameter#isMandatory <em>Mandatory</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Mandatory</em>' attribute.
     * @see #isMandatory()
     * @generated
     */
    void setMandatory(boolean value);

} // HeaderParameter
