/**
 */
package ca.mcgill.sel.restif;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dynamic Fragment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.restif.DynamicFragment#getPlaceholder <em>Placeholder</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.restif.RestifPackage#getDynamicFragment()
 * @model
 * @generated
 */
public interface DynamicFragment extends PathFragment, Parameter {
    /**
     * Returns the value of the '<em><b>Placeholder</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Placeholder</em>' attribute.
     * @see #setPlaceholder(String)
     * @see ca.mcgill.sel.restif.RestifPackage#getDynamicFragment_Placeholder()
     * @model
     * @generated
     */
    String getPlaceholder();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.restif.DynamicFragment#getPlaceholder <em>Placeholder</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Placeholder</em>' attribute.
     * @see #getPlaceholder()
     * @generated
     */
    void setPlaceholder(String value);

} // DynamicFragment
