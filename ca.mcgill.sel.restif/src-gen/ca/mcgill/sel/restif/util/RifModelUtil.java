package ca.mcgill.sel.restif.util;

import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.restif.LayoutElement;
import ca.mcgill.sel.restif.Layout;
import ca.mcgill.sel.restif.RestIF;
import ca.mcgill.sel.restif.RestifFactory;

/**
 * Helper class with convenient static methods for working with Rif Tree objects.
 *
 */
public final class RifModelUtil {

    /**
     * Creates a new instance of {@link RifModelUtil}.
     */
    private RifModelUtil() {
        // suppress default constructor
    }
    
    public static RestIF createRifTree(String baseName, COREConcern concern) {
        RestIF restIF = RestifFactory.eINSTANCE.createRestIF();
                
        restIF.setName(baseName);
        
        // create empty layout
        createLayout(restIF);
        
        return restIF;
    }

    /**
     * Creates a new layout for a given {@link RestIF}.
     * The layout is the {@link ca.mcgill.sel.ram.impl.ContainerMapImpl} specifically
     * that holds all {@link LayoutElement} for children of the given {@link RestIF}.
     *
     * @param restIF the {@link RestIF} holding the {@link LayoutElement} for its children
     */
    public static void createLayout(RestIF restIF) {
        Layout layout = RestifFactory.eINSTANCE.createLayout();

        // workaround used here since creating the map, adding the values and then putting it doesn't work
        // EMF somehow does some magic with the passed map instance
        layout.getContainers().put(restIF, new BasicEMap<EObject, LayoutElement>());

        restIF.setLayout(layout);
    }

}
