/**
 */
package ca.mcgill.sel.restif;

import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Layout</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.restif.Layout#getContainers <em>Containers</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.restif.RestifPackage#getLayout()
 * @model
 * @generated
 */
public interface Layout extends EObject {
    /**
     * Returns the value of the '<em><b>Containers</b></em>' map.
     * The key is of type {@link org.eclipse.emf.ecore.EObject},
     * and the value is of type list of {@link java.util.Map.Entry<org.eclipse.emf.ecore.EObject, ca.mcgill.sel.restif.LayoutElement>},
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Containers</em>' map.
     * @see ca.mcgill.sel.restif.RestifPackage#getLayout_Containers()
     * @model mapType="ca.mcgill.sel.restif.ContainerMap&lt;org.eclipse.emf.ecore.EObject, ca.mcgill.sel.restif.ElementMap&gt;"
     * @generated
     */
    EMap<EObject, EMap<EObject, LayoutElement>> getContainers();

} // Layout
