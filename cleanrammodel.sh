#! /bin/bash
#
# Cleanup skript to remove files eclipse lost trace of, but comwardlu refuses to overwrite on hitting "genmodel" -> "generate model code".

# Remove the model code that would be overwritten
rm ca.mcgill.sel.ram/src/ca/mcgill/sel/ram/*java
rm -rf ca.mcgill.sel.ram/src/ca/mcgill/sel/ram/impl
echo " * Deleted ram/*java and ram/impl subdir."
# note that ca.mcgill.sel.ram/src/ca/mcgill/sel/ram/util must not be erased!

# Note that the editor contains custom parts. So it should not be overwritten as a whole.
rm -rf ca.mcgill.sel.ram.edit/src/ca/mcgill/sel/ram/provider/*java
echo " * Deleted ram.edit/*java."

# Remove the editor project, if existing
if [ -d ca.mcgill.sel.ram.editor ]; then
  rm -rf ca.mcgill.sel.ram.editor
  echo " * Deleted RAM editor project dir."
fi




