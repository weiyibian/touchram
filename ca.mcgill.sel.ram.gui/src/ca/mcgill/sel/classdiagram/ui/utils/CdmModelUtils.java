package ca.mcgill.sel.classdiagram.ui.utils;

import java.lang.reflect.GenericArrayType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.WildcardType;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.mt4j.components.MTComponent;
import org.mt4j.sceneManagement.Iscene;

import ca.mcgill.sel.classdiagram.CDArray;
import ca.mcgill.sel.classdiagram.CDCollection;
import ca.mcgill.sel.classdiagram.CDEnum;
import ca.mcgill.sel.classdiagram.CdmFactory;
import ca.mcgill.sel.classdiagram.CdmPackage;
import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.classdiagram.Classifier;
import ca.mcgill.sel.classdiagram.ImplementationClass;
import ca.mcgill.sel.classdiagram.ObjectType;
import ca.mcgill.sel.classdiagram.Operation;
import ca.mcgill.sel.classdiagram.PrimitiveType;
import ca.mcgill.sel.classdiagram.Type;
import ca.mcgill.sel.classdiagram.TypeParameter;
import ca.mcgill.sel.classdiagram.VisibilityType;
import ca.mcgill.sel.classdiagram.ui.scenes.DisplayClassDiagramScene;
import ca.mcgill.sel.classdiagram.ui.views.ClassDiagramView;
import ca.mcgill.sel.classdiagram.ui.views.OperationView;
import ca.mcgill.sel.classdiagram.ui.views.ParameterView;
import ca.mcgill.sel.classdiagram.util.CdmInterfaceUtil;
import ca.mcgill.sel.classdiagram.util.CdmModelUtil;
import ca.mcgill.sel.ram.StructuralView;
import ca.mcgill.sel.ram.loaders.RamClassLoader;
import ca.mcgill.sel.ram.loaders.exceptions.MissingJarException;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamKeyboard;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent.Cardinal;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.components.listeners.DefaultRamKeyboardListener;
import ca.mcgill.sel.ram.ui.perspective.PerspectiveControllerFactory;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.util.MetamodelRegex;


/**
 * A util class for correct creation of Class Diagram metamodel elements. 
 * Also provide getters for these elements and makes sure.
 * they are well formed.
 *
 * @author vbonnet
 * @author eyildirim
 * @author mschoettle
 * @author oalam
 * @author yhattab
 * @author hyacinthali
 */
public final class CdmModelUtils {

    private static final String GENERIC_DELIMITER_BEGIN = "<";

    private static final String GENERIC_DELIMITER_END = ">";

    private static final String ARRAY_DELIMITER_EMPTY = "[]";

    private static CdmModelUtils instance;

    private Map<EStructuralFeature, Map<Object, String>> prettyPrinters;

    /**
     * Creates a new instance with default pretty printers.
     */
    private CdmModelUtils() {
        prettyPrinters = new HashMap<EStructuralFeature, Map<Object, String>>();

        initializePrettyPrinters();
    }

    /**
     * Returns the singleton instance.
     *
     * @return the singleton instance
     */
    private static CdmModelUtils getInstance() {
        if (instance == null) {
            instance = new CdmModelUtils();
        }
        return instance;
    }

    /**
     * Initializes the pretty printers.
     */
    private void initializePrettyPrinters() {
        // pretty printer for Visibility
        Map<Object, String> prettyPrinter = new HashMap<Object, String>();

        
        prettyPrinter.put(VisibilityType.PUBLIC, Strings.SYMBOL_PUBLIC);
        prettyPrinter.put(VisibilityType.PACKAGE, Strings.SYMBOL_PACKAGE);
        prettyPrinter.put(VisibilityType.PRIVATE, Strings.SYMBOL_PRIVATE);
        prettyPrinter.put(VisibilityType.PROTECTED, Strings.SYMBOL_PROTECTED);

        prettyPrinters.put(CdmPackage.Literals.OPERATION__VISIBILITY, prettyPrinter);
        
        Map<Object, String> prettyPrinterForClasses = new HashMap<Object, String>();
        prettyPrinterForClasses.put(VisibilityType.PUBLIC, Strings.SYMBOL_PUBLIC);
        prettyPrinterForClasses.put(VisibilityType.PACKAGE, Strings.SYMBOL_PACKAGE);
        prettyPrinterForClasses.put(VisibilityType.PRIVATE, Strings.SYMBOL_PRIVATE);
        prettyPrinterForClasses.put(VisibilityType.PROTECTED, Strings.SYMBOL_PROTECTED);
     
        prettyPrinters.put(CdmPackage.Literals.CLASSIFIER__VISIBILITY, prettyPrinterForClasses);
    }

    /**
     * Returns a map for pretty printing the given {@link EStructuralFeature}. Returns null if it doesn't exist.
     *
     * @param feature
     *            the feature a pretty printer is looked for
     * @return the map for pretty printing, null if not existent
     */
    public static Map<Object, String> getPrettyPrinter(EStructuralFeature feature) {
        CdmModelUtils inst = getInstance();
        return inst.prettyPrinters.get(feature);
    }

    /**
     * Returns the {@link ObjectType} object with the given name. The type will be searched for in the
     * {@link ClassDiagram} of the active {@link ca.mcgill.sel.classdiagram.ClassDiagram}.
     *
     * @param name
     *            the name of the {@link ObjectType} that is looked for
     * @return the {@link ObjectType} object that corresponds to the given name
     * @see CdmModelUtils#getAttributeTypeByName(String, StructuralView)
     */
    public static ObjectType getAttributeTypeByName(String name) {
        return getAttributeTypeByName(name, ((DisplayClassDiagramScene) RamApp.getActiveScene()).getClassDiagram());
    }

    /**
     * Returns the {@link ObjectType} object with the given name that is contained in the given
     * {@link StructuralView}.
     *
     * @param name
     *            the name of the {@link ObjectType} that is looked for
     * @param cd
     *            the {@link StructuralView} containing the types
     * @return the {@link ObjectType} object of the given {@link StructuralView} that corresponds to the given name
     */
    public static ObjectType getAttributeTypeByName(String name, ClassDiagram cd) {
        
        if (name.matches(MetamodelRegex.REGEX_ARRAY_TYPE)) {
            return getArrayByName(name, true, cd);
        }
        
        for (ObjectType type : CdmInterfaceUtil.getAvailableAttributeTypes(cd)) {
            if (type.getName() != null && type.getName().toLowerCase().equals(name.toLowerCase())) {
                return type;
            }
        }

        return null;
    }

    /**
     * Returns the type for the given name, if it exists.
     *
     * @param name
     *            The name of the type.
     * @return The type with the given name in RamApp's active structural view; null if no such type exists.
     */
    public static Type getTypeByName(String name) {
        return getTypeByName(name, ((DisplayClassDiagramScene) RamApp.getActiveScene()).getClassDiagram());
    }

    /**
     * Returns the type for the given name, if it exists, of the given class diagram.
     *
     * @param name
     *            The name of the type to get.
     * @param cd
     *            The structural view of interest
     * @return The type associated with that name, null if no such type exists.
     */
    // CHECKSTYLE:IGNORE ReturnCount: This is definitively the best way to write this piece of code.
    public static Type getTypeByName(String name, ClassDiagram cd) {
        if ("void".equals(name)) {
            return CdmModelUtil.getVoidType(cd);
        }

        if (name.matches(MetamodelRegex.REGEX_ARRAY_TYPE)) {
            return getArrayByName(name, false, cd);
        }

        // if instance class name
        if (!name.contains(".")) {
            // TODO: find a better way
            // handle collections special for now
            if (name.contains(GENERIC_DELIMITER_BEGIN)) {
                // this will result in the type at index 0 and the typed parameter in index 1
                String[] parameterizedType = name.split("<|>");

                // get the type
                CDCollection type = null;
                for (Type currentType : cd.getTypes()) {
                    // TODO: once the name of collections is not set by OCL in the way of Set<String>
                    // instead of name just the type name has to be used (parameterizedType[0])
                    if (currentType.getName().equals(name)) {
                        type = (CDCollection) currentType;
                    }
                }

                // if the type hasn't been found we have to create it
                if (type == null) {
                    EClass typeClass = (EClass) CdmPackage.eINSTANCE.getEClassifier("CD" + parameterizedType[0]);

                    // the requested type doesn't exist
                    if (typeClass == null) {
                        return null;
                    }

                    // create a new instance of the type
                    type = (CDCollection) CdmFactory.eINSTANCE.create(typeClass);

                    // this ensures that we also find primitive types
                    ObjectType collectionType = (ObjectType) getTypeByName(parameterizedType[1], cd);

                    // if the class doesn't exist we cannot create the type
                    if (collectionType == null) {
                        return null;
                    }

                    type.setType(collectionType);
                    cd.getTypes().add(type);
                }

                return type;
            }

            for (PrimitiveType primType : CdmInterfaceUtil.getAvailablePrimitiveTypes(cd)) {
                if (primType.getName().equals(name)) {
                    return primType;
                }
            }

            for (Classifier clazz : CdmInterfaceUtil.getAvailableClasses(cd)) {
                if (clazz.getName() != null && clazz.getName().equals(name)) {
                    return clazz;
                }
            }
        } else {
            if ("java.lang.String".equals(name)) {
                return CdmModelUtils.getTypeByName("String");
            }
            for (Classifier clazz : cd.getClasses()) {
                if (clazz instanceof ImplementationClass) {
                    if (name.equals(getClassNameWithGenerics(clazz))) {
                        return clazz;
                    }
                }
            }
            for (Type type : cd.getTypes()) {
                if (type instanceof CDEnum) {
                    if (((CDEnum) type).getInstanceClassName() != null) {
                        if (name.equals(((CDEnum) type).getInstanceClassName())) {
                            return type;
                        }
                    }
                }
            }
        }

        return null;
    }

    /**
     * Returns the existing array of the class diagram if it exists.
     * Otherwise it creates a new array with the appropriate properties,
     * adds it to the structural view and returns it.
     * The array size may also be specified, but is optional.
     *
     * @param name the fully qualified name of the array (e.g., int[])
     * @param simpleTypesOnly whether only primitive or data types as array type are allowed
     * @param cd the {@link ClassDiagram} of interest
     * @return the {@link CDArray} corresponding to the given name,
     *         null if the primitive types only constraint is violated
     */
    private static CDArray getArrayByName(String name, boolean simpleTypesOnly, ClassDiagram cd) {
        CDArray type = null;

        for (Type currentType : cd.getTypes()) {
            if (currentType.getName().equals(name)) {
                type = (CDArray) currentType;
            }
        }

        if (type == null) {
            int arrayIndex = name.lastIndexOf('[');
            String arrayTypeName = name.substring(0, arrayIndex);
            String sizeString = name.substring(arrayIndex).replaceAll("\\[|\\]", "");

            ObjectType arrayType = (ObjectType) getTypeByName(arrayTypeName, cd);
            
            /**
             * In case of simple types only, the type has to either a primitive type
             * or a data type.
             */
            if (!simpleTypesOnly
                    || simpleTypesOnly
                            && (arrayType instanceof PrimitiveType
                                    || arrayType instanceof Classifier && ((Classifier) arrayType).isDataType())) {
                type = CdmFactory.eINSTANCE.createCDArray();
                type.setType(arrayType);
                
                if (!sizeString.isEmpty()) {
                    type.setSize(Integer.parseInt(sizeString));
                }
                
                cd.getTypes().add(type);
            }
        }

        return type;
    }

    /**
     * Extract the class name with generic parameters of an ObjectType.
     *
     * @param otype some ObjectType
     * @return the class name with generics
     */
    public static String getClassNameWithGenerics(ObjectType otype) {
        String name = otype.getName();
        // if implementation class but not a primitive type
        if (otype instanceof ImplementationClass && (!(otype instanceof PrimitiveType) || otype instanceof CDEnum)) {
            if (((ImplementationClass) otype).getInstanceClassName() != null) {
                name = ((ImplementationClass) otype).getInstanceClassName();
            }
        }
        if (otype instanceof Classifier) {
            // if has type parameters
            EList<TypeParameter> typeParams = ((Classifier) otype).getTypeParameters();
            if (typeParams.size() != 0) {
                name += GENERIC_DELIMITER_BEGIN;
                // go through every type parameter
                for (int i = 0; i < typeParams.size(); i++) {
                    TypeParameter tp = typeParams.get(i);
                    String tpName = tp.getName();
                    ObjectType genericType = tp.getGenericType();

                    // if implementation class or implementation enum extract name
                    if (genericType instanceof ImplementationClass || genericType instanceof CDEnum) {
                        if (((ImplementationClass) genericType).getInstanceClassName() != null) {
                            tpName = getClassNameWithGenerics(genericType);
                        }
                    } else if (genericType instanceof CDArray) {
                        tpName = getClassNameWithGenerics((ObjectType) ((CDArray) genericType).getType()) 
                                + ARRAY_DELIMITER_EMPTY;
                    } else if (genericType instanceof PrimitiveType
                            || genericType instanceof ca.mcgill.sel.ram.Class) {
                        tpName = genericType.getName();
                    }
                    // add to name
                    name += tpName;
                    // add comma if not last one
                    if (i != typeParams.size() - 1) {
                        name += ",";
                    }
                }
                name += GENERIC_DELIMITER_END;
            }
        }

        return name;
    }

    /**
     * Returns the {@link VisibilityType} from the given string.
     *
     * @param stringRepresentation
     *            The string representation of the visibility type.
     * @return The visibility type associated with the representation, null if none exists.
     */
    public static VisibilityType getRamVisibilityFromStringRepresentation(String stringRepresentation) {
        VisibilityType result = null;

        if (Strings.SYMBOL_PUBLIC.equals(stringRepresentation)) {
            result = VisibilityType.PUBLIC;
        } else if (Strings.SYMBOL_PACKAGE.equals(stringRepresentation)) {
            result = VisibilityType.PACKAGE;
        } else if (Strings.SYMBOL_PRIVATE.equals(stringRepresentation)) {
            result = VisibilityType.PRIVATE;
        } else if (Strings.SYMBOL_PROTECTED.equals(stringRepresentation)) {
            result = VisibilityType.PROTECTED;
        }

        return result;
    }

    /**
     * Get the name of a specified type. Replaces all type parameters by the type parameters of the owner.
     *
     * @param type some type
     * @param owner ImplementationClass that uses this type as return type or parameter.
     * @return name of the type
     */
    public static String getNameOfType(java.lang.reflect.Type type, ImplementationClass owner) {
        // get name
        String name = type.toString();

        // if it is a parameterized type get all type parameters
        if (type instanceof ParameterizedType) {
            // get raw name
            ParameterizedType pType = (ParameterizedType) type;
            name = pType.getRawType().toString().replaceAll("(interface|class|enum) ", "");

            // get all generics
            java.lang.reflect.Type[] generics = pType.getActualTypeArguments();
            if (generics.length != 0) {
                name += GENERIC_DELIMITER_BEGIN;
                for (int i = 0; i < generics.length; i++) {
                    // if also a parameterized type recursively get the name
                    if (generics[i] instanceof WildcardType) {
                        name += "?";
                    } else {
                        name += getNameOfType(generics[i], owner);
                    }

                    if (i != generics.length - 1) {
                        name += ",";
                    }
                }
                name += GENERIC_DELIMITER_END;
            }
        } else if (type instanceof GenericArrayType) {
            name = getNameOfType(((GenericArrayType) type).getGenericComponentType(), owner) + ARRAY_DELIMITER_EMPTY;
        } else if (isGenericOf(owner, name)) {
            name = CdmModelUtils.getClassNameWithGenerics(getGenericOf(owner, name).getGenericType());
        } else if (type instanceof Class) {
            if (((Class<?>) type).isArray()) {
                name = getNameOfType(((Class<?>) type).getComponentType(), owner) + ARRAY_DELIMITER_EMPTY;
            } else {
                name = ((Class<?>) type).getCanonicalName();
            }
        }
        // Replace java.lang.String to String
        return name.replaceAll("java.lang.String", "String");
    }

    /**
     * Gets the TypeParameter of some ImplementationClass given the type parameter name.
     *
     * @param owner of the type parameter
     * @param typeName name of type parameter
     * @return TypeParameter with the specified name
     */
    public static TypeParameter getGenericOf(ImplementationClass owner, String typeName) {
        for (TypeParameter tp : owner.getTypeParameters()) {
            if (typeName.equals(tp.getName())) {
                return tp;
            }
        }
        return null;
    }

    /**
     * Checks if ImplementationClass has a generic with the specified type parameter name.
     *
     * @param owner Some ImplementationClass.
     * @param typeName Type parameter name
     * @return true if owner contains the typeName, false otherwise
     */
    public static boolean isGenericOf(ImplementationClass owner, String typeName) {
        return getGenericOf(owner, typeName) != null;
    }

    /**
     * Returns a set of implementation classes that exist in the structural view
     * and have the given class as the super type.
     * Returns an empty set if none found.
     *
     * @param cd the {@link ClassDiagram} containing classes
     * @param className the fully qualified name of the potential super class
     * @return the set of existing implementation classes that are a subtype of the given class, empty set if none found
     * @throws MissingJarException if the given class could not be found
     */
    public static Set<ImplementationClass> getExistingSubTypesFor(ClassDiagram cd, String className)
            throws MissingJarException {
        Set<ImplementationClass> result = new HashSet<ImplementationClass>();

        Collection<ImplementationClass> classes =
                EcoreUtil.getObjectsByType(cd.getClasses(), CdmPackage.Literals.IMPLEMENTATION_CLASS);

        for (ImplementationClass classifier : classes) {
            Set<String> superTypes = RamClassLoader.INSTANCE.getAllSuperClassesFor(classifier.getInstanceClassName());
            if (superTypes.contains(className)) {
                result.add(classifier);
            }
        }

        return result;
    }

    /**
     * Add a parameter at the end of an operation.
     * It add the placeholder in the view and in the model
     *
     * @param operationView - the related {@link OperationView}
     */
    public static void createParameterEndOperation(final OperationView operationView) {
        final RamTextComponent textRow = new RamTextComponent();
        textRow.setBufferSize(Cardinal.SOUTH, OperationView.BUFFER_BOTTOM);
        textRow.setPlaceholderText(Strings.PH_PARAM);

        int visualIndex = operationView.getChildCount() - 1;
        operationView.addChild(visualIndex, textRow);

        final int nbParameters = operationView.getOperation().getParameters().size();

        // visual index for delimiter; after if it is the first, before otherwise
        final int delimiterIndex = visualIndex;

        if (nbParameters > 0) {
            operationView.addDelimiter(delimiterIndex);
        }
        final MTComponent delimiter = operationView.getChildByIndex(delimiterIndex);

        RamKeyboard keyboard = new RamKeyboard();

        keyboard.registerListener(new DefaultRamKeyboardListener() {
            @Override
            public void keyboardCancelled() {
                operationView.removeChild(textRow);
                if (delimiter != null) {
                    operationView.removeChild(delimiter);
                }
            }

            @Override
            public boolean verifyKeyboardDismissed() {
                try {
                    int nbParameters = operationView.getOperation().getParameters().size();
                    createParameterInModel(operationView.getOperation(), nbParameters, textRow.getText());
                } catch (final IllegalArgumentException e) {
                    return false;
                }
                if (delimiter != null) {
                    operationView.removeChild(delimiter);
                }
                operationView.removeChild(textRow);

                return true;
            }
        });

        textRow.showKeyboard(keyboard);
    }

    /**
     * Add a parameter around another parameter. It can be on the left or on the right.
     * It add the placeholder in the view and in the model
     *
     * @param left - true if the parameter has to be on the left of the given parameter.
     * @param parameterView - the related {@link ParameterView}
     */
    public static void createParameterAroundParameter(final boolean left, final ParameterView parameterView) {
        final OperationView operationView = (OperationView) parameterView.getParent();
        final RamTextComponent textRow = new RamTextComponent();
        textRow.setBufferSize(Cardinal.SOUTH, OperationView.BUFFER_BOTTOM);
        textRow.setPlaceholderText(Strings.PH_PARAM);

        int parameterViewIndex = operationView.getChildIndexOf(parameterView);
        int visualIndex = left ? parameterViewIndex : parameterViewIndex + 1;
        operationView.addChild(visualIndex, textRow);

        // visual index for delimiter; after if it is the first, before otherwise
        final int delimiterIndex = !left ? visualIndex : visualIndex + 1;
        operationView.addDelimiter(delimiterIndex);
        final MTComponent delimiter = operationView.getChildByIndex(delimiterIndex);

        RamKeyboard keyboard = new RamKeyboard();

        keyboard.registerListener(new DefaultRamKeyboardListener() {
            @Override
            public void keyboardCancelled() {
                operationView.removeChild(textRow);
                operationView.removeChild(delimiter);
            }

            @Override
            public boolean verifyKeyboardDismissed() {
                try {
                    int currentModelIndex =
                            operationView.getOperation().getParameters().indexOf(parameterView.getParameter());
                    int modelIndex = left ? currentModelIndex : currentModelIndex + 1;
                    createParameterInModel(operationView.getOperation(), modelIndex, textRow.getText());
                } catch (final IllegalArgumentException e) {
                    return false;
                }

                operationView.removeChild(delimiter);
                operationView.removeChild(textRow);

                return true;
            }
        });

        textRow.showKeyboard(keyboard);
    }

    /**
     * Creates a new parameter in the model.
     *
     * @param owner the operation the parameter should be added to
     * @param index the index in the list of parameters where the parameter should be added at
     * @param parameterString the string representation of the parameter (e.g., "&lt;type> &lt;parameterName>)
     */
    private static void createParameterInModel(Operation owner, int index, String parameterString) {
        Matcher matcher =
                Pattern.compile("^" + MetamodelRegex.REGEX_PARAMETER_DECLARATION + "$").matcher(parameterString);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("The string " + parameterString
                    + " does not conform to parameter syntax");
        }

        Type type = CdmModelUtils.getTypeByName(matcher.group(1));
        String nameString = matcher.group(3);
        if (type == null) {
            throw new IllegalArgumentException("No type with that name exists");
        }

        if (nameString == null) {
            throw new IllegalArgumentException("Parameter name did not match naming syntax");
        }
        
        EStructuralFeature containingFeature = CdmPackage.Literals.OPERATION__PARAMETERS;
        if (!CdmModelUtil.isUnique(owner, containingFeature, nameString, type)) {
            throw new IllegalArgumentException("Parameter names duplicate");
        }

        PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController()
            .createParameter(owner, index, nameString, type);
    }
    
    /**
     * Retrieves the class diagram view from the RamApp if viewing a class diagram.
     * @param app currently running RamApp
     * @return class diagram view currently displayed {@link ClassDiagramView}
     */
    public static ClassDiagramView getClassDiagramViewFromApp(RamApp app) {
        final Iscene scene = RamApp.getActiveScene();
        if (scene != null && scene instanceof DisplayClassDiagramScene) {
            final DisplayClassDiagramScene cdScene = (DisplayClassDiagramScene) scene;
            return cdScene.getClassDiagramView();
        }
        return null;
    }
}
