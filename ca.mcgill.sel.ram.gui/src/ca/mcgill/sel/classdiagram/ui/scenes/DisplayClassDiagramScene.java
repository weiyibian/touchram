package ca.mcgill.sel.classdiagram.ui.scenes;

import java.util.Collection;
import java.util.Stack;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.emf.edit.provider.INotifyChangedListener;
import org.mt4j.components.MTComponent;
import org.mt4j.components.visibleComponents.shapes.MTRectangle.PositionAnchor;

import ca.mcgill.sel.classdiagram.CdmPackage;
import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.classdiagram.impl.ContainerMapImpl;
import ca.mcgill.sel.classdiagram.ui.scenes.handler.impl.DisplayClassDiagramSceneHandler;
import ca.mcgill.sel.classdiagram.ui.views.ClassDiagramView;
import ca.mcgill.sel.classdiagram.util.CdmModelUtil;
import ca.mcgill.sel.classdiagram.ui.views.handler.ClassDiagramHandlerFactory;
import ca.mcgill.sel.commons.LoggerUtils;
import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.ConfirmPopup;
import ca.mcgill.sel.ram.ui.components.ConfirmPopup.OptionType;
import ca.mcgill.sel.ram.ui.components.RamPanelComponent;
import ca.mcgill.sel.ram.ui.components.listeners.RamPanelListener;
import ca.mcgill.sel.ram.ui.scenes.RamAbstractScene;
import ca.mcgill.sel.ram.ui.utils.GraphicalUpdater;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.utils.validation.ValidationManager;
import ca.mcgill.sel.ram.ui.views.containers.ValidationView;

import ca.mcgill.sel.ram.ui.views.handler.IDisplaySceneHandler;

/**
 * This scene displays everything relevant to a class diagram.
 *
 * @author yhattab
 * @author vbonnet
 * @author mschoettle
 */
public class DisplayClassDiagramScene extends RamAbstractScene<IDisplaySceneHandler>
        implements INotifyChangedListener, RamPanelListener {

    // Actions for the scene
    private static final String ACTION_WEAVE_ALL = "display.weave.all";
    private static final String ACTION_GENERATE = "display.generate";
    private static final String ACTION_BACK = "display.back";
    private static final String ACTION_OPEN_VALIDATOR = "display.validator";
    private static final String ACTION_CONCERN_BACK = "display.concern.back";
    private static final String ACTION_OPEN_TRACING = "display.tracing";
    private static final String ACTION_LAYOUT = "display.layout";
    private static final String ACTION_TOGGLE_NOTES = "display.toggle.notes";

    private static final String ACTION_WEAVE_STATE_MACHINES = "display.weave.state.machines";
    private static final String ACTION_WEAVE_STATE_MACHINES_NO_CSP = "display.weave.state.machines.no.csp";
    private static final String ACTION_CLOSE_SPLIT_VIEW = "display.close.splitview";

    // Name of the submenus
    private static final String SUBMENU_WEAVE = "sub.weave";
    private static final String SUBMENU_GOTO = "sub.goto";
    private static final String SUBMENU_SHOWPANELS = "sub.panel";
    private static final String SUBMENU_GENERATE = "sub.gen";
//    private static final String SUBMENU_BACK = "sub.back";

    private ClassDiagramView classDiagramView;
    
    private ValidationView aspectValidatorView;
    private ValidationManager validatorLauncher;

    private GraphicalUpdater graphicalUpdater;

    private ClassDiagram cd;
    
    private COREArtefact artefact;

    private MTComponent viewContainer;
    private Stack<MTComponent> previousViews;
    private MTComponent currentView;
        
    /**
     * Creates a scene that will display this aspect.
     *
     * @param app
     *            The application to which the scene belongs.
     * @param artefact
     *            The artefact the Scene will display.
     * @param cd
     *            The aspect this Scene will display.
     * @param sceneName
     *            the name of the scene
     */
    public DisplayClassDiagramScene(RamApp app, COREExternalArtefact artefact, ClassDiagram cd, String sceneName) {
        super(app, sceneName);
        this.cd = cd;
        this.artefact = artefact;
        
        this.handler = ClassDiagramHandlerFactory.INSTANCE.getDisplayClassDiagramSceneHandler();

        // views are added into this layer
        viewContainer = new MTComponent(app, "view container");
        getContainerLayer().addChild(viewContainer);

        // Graphical Updater linked to this aspect
        graphicalUpdater = RamApp.getApplication().getGraphicalUpdaterForModel(cd);

        buildViews();

        EMFEditUtil.addListenerFor(cd, this);
        
        // the following registers the artefact to listen to all changes to it's content, which
        // is all the changes to contained COREModelExtensions, COREModelReuses, and CORELinks
        // whenever a new mapping is created, deleted, or when the "to" element of a mapping changes,
        // the graphical updated is sent a referenceEvent so it can trigger updateStyle changes on all
        // registered views so that they can change to their "ghost" background color, if needed
        this.artefact.eAdapters().add(new EContentAdapter() {
            @Override
            public void notifyChanged(Notification notification) {
                super.notifyChanged(notification);
                if (notification.getFeature() == CorePackage.Literals.CORE_LINK__TO) {
                    // the model element of a "to" mapping was changed
                    // we need to trigger the updateStyle in all views representing the old element and the new
                    // element
                    if (notification.getOldValue() != null) {
                        graphicalUpdater.referenceEvent((EObject) notification.getOldValue());
                    }
                    if (notification.getNewValue() != null) {
                        graphicalUpdater.referenceEvent((EObject) notification.getNewValue());
                    }
                }
            } 
        });

        // The manager of the validation thread responsible for its launch (TODO currently unavailable for classDiagram)
//        validatorLauncher = new ValidationManager(cd, new Validator(), graphicalUpdater);
//        showValidation(true);

        setCommandStackListener(cd);
        
        repushSections();
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        if (handler != null) {
            String actionCommand = event.getActionCommand();

            if (ACTION_WEAVE_ALL.equals(actionCommand)) {
                handler.weaveAll(this);
            } else if (ACTION_WEAVE_STATE_MACHINES.equals(actionCommand)) {
                handler.weaveStateMachines(this);
            } else if (ACTION_WEAVE_STATE_MACHINES_NO_CSP.equals(actionCommand)) {
                handler.weaveAllNoCSPForStateViews(this);
            } else if (ACTION_GENERATE.equals(actionCommand)) {
                handler.generate(this);
            } else if (ACTION_BACK.equals(actionCommand)) {
                handler.back(this);
            } else if (ACTION_OPEN_VALIDATOR.equals(actionCommand)) {
                handler.showValidation(this);
            } else if (ACTION_CONCERN_BACK.equals(actionCommand)) {
                handler.switchToConcern(this);
            } else if (ACTION_OPEN_TRACING.equals(actionCommand)) {
                handler.showTracing(this);
            } else if (ACTION_CLOSE_SPLIT_VIEW.equals(actionCommand)) {
                handler.closeSplitView(this);
            } else if (ACTION_LAYOUT.equals(actionCommand)) {
                handler.layout(this);
            } else if (ACTION_TOGGLE_NOTES.equals(actionCommand)) {
                menu.toggleAction(((DisplayClassDiagramSceneHandler) handler).toggleNotes(this), ACTION_TOGGLE_NOTES);
            } else {
                super.actionPerformed(event);
            }
        } else {
            LoggerUtils.warn("No handler set for " + this.getClass().getName());
        }
    }


    /**
     * Builds the structural, state and initial message views.
     */
    private void buildViews() {
        // create layout if it doesn't exist yet
        if (cd.getLayout() == null) {
            CdmModelUtil.createLayout(cd);
        }

        ContainerMapImpl layout = EMFModelUtil.getEntryFromMap(cd.getLayout().getContainers(),
                cd);
        classDiagramView = new ClassDiagramView(cd, layout, getWidth(), getHeight());

        // we will get position of the upper left corner of the view instead of center.
        classDiagramView.setAnchor(PositionAnchor.UPPER_LEFT);

        previousViews = new Stack<MTComponent>();
        switchToView(classDiagramView);

        classDiagramView.setHandler(ClassDiagramHandlerFactory.INSTANCE.getClassDiagramViewHandler());
    }

    @Override
    public boolean destroy() {

        EMFEditUtil.removeListenerFor(cd, this);

        /**
         * Not all views are currently children, so we need to destroy them explicitly.
         * I.e., only the last active view was a children of the view container.
         */
        destroyViews();
        if (this.getPreviousScene() == null) {
            navbar.returnNormalViewFromSplit();
            navbar.concernSelectMode();
            RamApp.getActiveScene().getCanvas().addChild(navbar);
        }
        return super.destroy();
    }

    /**
     * Destroys all views that are part of this scene.
     * This includes those views that are not currently visible.
     */
    private void destroyViews() {
        classDiagramView.destroy();
    }

    /**
     * Returns the Class Diagram the current scene is displaying.
     *
     * @return The {@link ClassDiagram} that this scene is displaying.
     */
    public ClassDiagram getClassDiagram() {
        return cd;
    }
    
    /**
     * Return the COREExternalArtefact as a COREArtefact the current scene is using.
     * 
     * @return The artefact that this scene is using
     */
    public COREArtefact getArtefact() {
        return artefact;
    }

    /**
     * Gets the Graphical Updater of the aspect.
     *
     * @return the graphicalUpdater
     */
    public GraphicalUpdater getGraphicalUpdater() {
        return graphicalUpdater;
    }

    /**
     * Returns the Class Diagram view for the scene.
     *
     * @return The class diagram view for this scene.
     */
    public ClassDiagramView getClassDiagramView() {
        return classDiagramView;
    }

    /**
     * Shows all of the buttons in the menu for structural view.
     */
    private void showStructuralViewButtons() {
        showMainMenuButtons();
    }

    /**
     * Show buttons present in structural, message and state views.
     */
    private void showMainMenuButtons() {
        // remove unused actions
        menu.removeAction(ACTION_WEAVE_STATE_MACHINES);
        menu.removeAction(ACTION_WEAVE_STATE_MACHINES_NO_CSP);
        // menu.removeAction(ACTION_BACK);
        menu.removeAction(ACTION_CLOSE_SPLIT_VIEW);

        // Back to concern button
        /*
         * if (menu.getAction(ACTION_CONCERN_BACK) == null) {
         * this.getMenu().addAction(Strings.MENU_BACK_CONCERN, Icons.ICON_MENU_BACK, ACTION_CONCERN_BACK, this, true);
         * }
         * 
         * if (menu.getAction(ACTION_GENERATE) == null) {
         *   this.getMenu().addAction(Strings.MENU_GENERATE, Icons.ICON_MENU_GENERATE, ACTION_GENERATE,
         *           this, SUBMENU_GENERATE, true);
         *   }
         * 
         * if (menu.getAction(ACTION_LAYOUT) == null) {
         *   this.getMenu().addAction(Strings.MENU_LAYOUT, Icons.ICON_MENU_LAYOUT, ACTION_LAYOUT, this, 
         *           SUBMENU_WEAVE, true);
         *   }
         */       
        if (menu.getAction(ACTION_TOGGLE_NOTES) == null) {
            this.getMenu().addAction(Strings.MENU_TOGGLE_NOTES, Strings.MENU_TOGGLE_NOTES, 
                    Icons.ICON_MENU_SHOW_NOTES, Icons.ICON_MENU_HIDE_NOTES, ACTION_TOGGLE_NOTES, this, 
                    SUBMENU_GENERATE, true, true);
        }
    }


    /**
     * Replaces the current view with the given view. Remembers the old current view in case the user wants to go back
     * to it.
     *
     * @param view
     *            the view to display
     * @see #switchToPreviousView()
     */
    public void switchToView(MTComponent view) {
        this.switchToView(view, true);
    }

    /**
     * Replaces the current view with the given view. Remembers the old current view in case the user wants to go back
     * to it.
     *
     * @param view
     *            the view to display
     * @param saveCurrent - whether we want to remember the current view as the previous one.
     * @see #switchToPreviousView()
     */
    public void switchToView(MTComponent view, boolean saveCurrent) {
        if (saveCurrent) {
            previousViews.push(currentView);
        }
        
        currentView = view;
        viewContainer.removeAllChildren();
        viewContainer.addChild(view);
        showMenu();
        clearTemporaryComponents();
    }

    /**
     * Show menu related to the current view.
     */
    private void showMenu() {
        showStructuralViewButtons();
    }

    /**
     * Returns the current view that is displayed.
     *
     * @return the current view
     */
    public MTComponent getCurrentView() {
        return currentView;
    }

    /**
     * Displays the view that was displayed previously.
     *
     * @see #switchToView(MTComponent)
     */
    public void switchToPreviousView() {
        if (!previousViews.isEmpty()) {
            switchToView(previousViews.pop(), false);
        }
        navbar.popSection();
    }

    /**
     * Used to refresh the sections in the navigation bar whenever the scene is called.
     */
    public void repushSections() {
        if (artefact.getScene() != null && artefact.getScene().getRealizes().size() > 0) {
            COREFeature feature = artefact.getScene().getRealizes().get(0);
            
            Collection<ClassDiagram> classDiagrams = EMFModelUtil.collectElementsOfType(feature,
                    CorePackage.Literals.CORE_FEATURE__REALIZED_BY,
                    CdmPackage.Literals.CLASS_DIAGRAM);
            classDiagrams.remove(cd);
            if (classDiagrams.isEmpty()) {
                navbar.pushSectionJumpFeature(Icons.ICON_NAVIGATION_FEATURE,
                        feature.getName(), null, feature);
            } else {
                navbar.pushSectionJumpFeature(Icons.ICON_NAVIGATION_FEATURE,
                    feature.getName(), navbar.getFeatureAndConflict(), feature);
            }
            
            navbar.pushSectionJumpRM(Icons.ICON_NAVIGATION_ASPECT, cd.getName(), 
                    null, cd);
        } 
    }

    /**
     * Shows a confirm popup for the given aspect to ask the user whether the aspect should be saved.
     *
     * @param parent
     *            the scene where the popup should be displayed, usually the current scene
     * @param listener
     *            the listener to inform which option the user selected
     */
    public void showCloseConfirmPopup(RamAbstractScene<?> parent, ConfirmPopup.SelectionListener listener) {
        showCloseConfirmPopup(parent, listener, OptionType.YES_NO_CANCEL);
    }

    /**
     * Shows a confirm popup for the given class diagram to ask the user whether it should be saved.
     *
     * @param parent the scene where the popup should be displayed, usually the current scene
     * @param listener the listener to inform which option the user selected
     * @param options the buttons to display in the popup
     */
    public void showCloseConfirmPopup(RamAbstractScene<?> parent, ConfirmPopup.SelectionListener listener,
            OptionType options) {
        String message = Strings.MODEL_ASPECT + " " + cd.getName() + Strings.POPUP_MODIFIED_SAVE;
        ConfirmPopup saveConfirmPopup = new ConfirmPopup(message, options);
        saveConfirmPopup.setListener(listener);

        parent.displayPopup(saveConfirmPopup);
    }

    @Override
    public void notifyChanged(Notification notification) {
        if (notification.getFeature() == CorePackage.Literals.CORE_ARTEFACT__CORE_CONCERN
                && notification.getEventType() == Notification.SET
                && notification.getNewValue() == null) {
            // Go back to the concern if the link with it is undone.
            handler.switchToConcern(this);
        }
    }


    /**
     * Closes or opens the {@link ValidationView} depends on the boolean parameter.
     * Launches also the {@link ValidationManager}.
     *
     * @param toOpen true if you want to open it, false to close it.
     */
    public void showValidation(boolean toOpen) {
        if (toOpen && aspectValidatorView == null) {

            // Validator and the view for the aspect
            aspectValidatorView = new ValidationView(graphicalUpdater);
            aspectValidatorView.registerListener(this);

            // Starts the thread
            validatorLauncher.addListener(aspectValidatorView);
            validatorLauncher.startValidationThread();

            // To make the "validatorManager" able to collect possible new errors from the validator
            aspectValidatorView.setController(validatorLauncher);
            getContainerLayer().addChild(aspectValidatorView);

            if (this.getMenu().getAction(ACTION_OPEN_VALIDATOR) != null) {
                this.getMenu().removeAction(ACTION_OPEN_VALIDATOR);
            }

            // Launch a first validation
            validatorLauncher.launchValidation(true, 0);

        } else {
            validatorLauncher.stopValidationThread();
            validatorLauncher.removeListener(aspectValidatorView);

            if (containerLayer.containsChild(aspectValidatorView)) {
                containerLayer.removeChild(aspectValidatorView);
            }
            this.getMenu().addAction(Strings.PANEL_OPEN_VALIDATION, Icons.ICON_MENU_VALIDATOR, ACTION_OPEN_VALIDATOR,
                    this, SUBMENU_SHOWPANELS, true);
            aspectValidatorView = null;
        }

    }

    @Override
    public void panelClosed(RamPanelComponent panel) {
        if (panel == aspectValidatorView) {
            showValidation(false);
        } 
    }

    @Override
    protected void initMenu() {
        // Initialize the subMenus
        menu.addSubMenu(3, SUBMENU_SHOWPANELS);
        menu.addSubMenu(2, SUBMENU_GENERATE);
        menu.addSubMenu(2, SUBMENU_WEAVE);
        //menu.addSubMenu(1, SUBMENU_BACK);
        menu.addSubMenu(1, SUBMENU_GOTO);
    }

    @Override
    protected EObject getElementToSave() {
        return cd;
    }
}
