package ca.mcgill.sel.classdiagram.ui.scenes.handler.impl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.mt4j.sceneManagement.transition.BlendTransition;
import org.mt4j.sceneManagement.transition.SlideTransition;

import ca.mcgill.sel.commons.LoggerUtils;
import ca.mcgill.sel.commons.emf.util.CORECommandStack;
import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.core.util.COREModelUtil;

import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.classdiagram.ui.views.ClassDiagramView;
import ca.mcgill.sel.classdiagram.ui.scenes.DisplayClassDiagramScene;
import ca.mcgill.sel.classdiagram.ui.utils.CdmModelUtils;
import ca.mcgill.sel.ram.generator.cpp.CppGenerator;
import ca.mcgill.sel.ram.generator.java.JavaGenerator;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.ConfirmPopup;
import ca.mcgill.sel.ram.ui.components.RamPopup;
import ca.mcgill.sel.ram.ui.components.RamPopup.PopupType;
import ca.mcgill.sel.ram.ui.components.RamSelectorComponent;
import ca.mcgill.sel.ram.ui.components.browser.GenericFileBrowser;
import ca.mcgill.sel.ram.ui.components.browser.RamFileBrowser;
import ca.mcgill.sel.ram.ui.components.browser.RamFileBrowser.RamFileBrowserType;
import ca.mcgill.sel.ram.ui.components.browser.interfaces.FileBrowserListener;
import ca.mcgill.sel.ram.ui.components.browser.interfaces.RamFileBrowserListener;
import ca.mcgill.sel.ram.ui.components.listeners.AbstractDefaultRamSelectorListener;
import ca.mcgill.sel.ram.ui.components.navigationbar.NavigationBar;
import ca.mcgill.sel.ram.ui.components.navigationbar.SlideUpDownTransition;
import ca.mcgill.sel.ram.ui.layouts.AutomaticLayout;
import ca.mcgill.sel.ram.ui.layouts.Layout;
import ca.mcgill.sel.ram.ui.scenes.DisplayAspectScene;
import ca.mcgill.sel.ram.ui.scenes.DisplayConcernEditScene;
import ca.mcgill.sel.ram.ui.scenes.DisplayConcernSelectScene;
import ca.mcgill.sel.ram.ui.scenes.RamAbstractScene;
import ca.mcgill.sel.ram.ui.scenes.handler.impl.ConcernSelectSceneHandler;
import ca.mcgill.sel.ram.ui.utils.BasicActionsUtils;
import ca.mcgill.sel.ram.ui.utils.GUIConstants;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.OptionSelectorView;
import ca.mcgill.sel.ram.ui.views.handler.IDisplaySceneHandler;
//import ca.mcgill.sel.ram.ui.views.structural.CompositionSplitEditingView;
//import ca.mcgill.sel.ram.ui.views.structural.CompositionView;
//import ca.mcgill.sel.ram.ui.views.structural.CompositionsPanel;
//import ca.mcgill.sel.ram.ui.views.structural.StructuralDiagramView;
//import ca.mcgill.sel.ram.ui.views.structural.handler.ICompositionPanelHandler;
import ca.mcgill.sel.ram.ui.views.structural.CompositionView;


/**
 * The default handler for a {@link DisplayClassDiagramScene}.
 *
 * @author mschoettle
 * @author yhattab
 */
public class DisplayClassDiagramSceneHandler extends DefaultClassDiagramSceneHandler implements IDisplaySceneHandler {

    /**
     * A listener for a confirm popup.
     * Handles the user response on whether to save in case modifications occurred.
     */
    private final class SaveConfirmListener implements ConfirmPopup.SelectionListener {
        private final DisplayClassDiagramScene scene;

        /**
         * Creates a new instance for the given scene.
         * 
         * @param scene the scene for this listener
         */
        private SaveConfirmListener(DisplayClassDiagramScene scene) {
            this.scene = scene;
        }

        @Override
        public void optionSelected(int selectedOption) {
            if (selectedOption == ConfirmPopup.YES_OPTION) {
                BasicActionsUtils.saveModel(scene.getClassDiagram(), new FileBrowserListener() {
                    @Override
                    public void modelSaved(File file) {
                        NavigationBar.getInstance().popSection();
                        NavigationBar.popHistory();
                        if (scene.getPreviousScene() instanceof DisplayConcernSelectScene) {
                            DisplayConcernSelectScene prev = (DisplayConcernSelectScene) scene.getPreviousScene();
                            ConcernSelectSceneHandler prevH = (ConcernSelectSceneHandler) prev.getHandler();
                            prevH.switchToPreviousScene(prev);
                            NavigationBar.getInstance().wipeNavigationBar();

                        } else {
                            doSwitchToPreviousScene(scene);
                        }
                    }

                    @Override
                    public void modelLoaded(EObject model) {
                    }
                });
            } else if (selectedOption == ConfirmPopup.NO_OPTION) {

                if (EMFEditUtil.getCommandStack(scene.getClassDiagram()) instanceof CORECommandStack) {
                    CORECommandStack stack = (CORECommandStack) EMFEditUtil.getCommandStack(scene.getClassDiagram());
                    
                    if (stack.canRevertToLastSave()) {
                        stack.goToLastSave();
                    } else {
                        scene.displayPopup(Strings.POPUP_REVERT_IMPOSSIBLE, PopupType.ERROR);
                        return;
                    }
                } else {
                    LoggerUtils.warn(Strings.invalidCommandStackInstance(scene.getArtefact()));
                }
                
                NavigationBar.getInstance().popSection();
                NavigationBar.popHistory();
                checkForModelRemoval(scene);
                
                if (scene.getPreviousScene() instanceof DisplayConcernSelectScene) {
                    DisplayConcernSelectScene prev = (DisplayConcernSelectScene) scene.getPreviousScene();
                    ConcernSelectSceneHandler prevH = (ConcernSelectSceneHandler) prev.getHandler();
                    prevH.switchToPreviousScene(prev);
                    NavigationBar.getInstance().wipeNavigationBar();

                } else {
                    doSwitchToPreviousScene(scene);
                }
            }
                
        }
    }

    /**
     * A listener for a confirm popup to handle the case of a fast return to the Concern Scene.
     * Handles the user response on whether to save in case modifications occurred.
     */
    private final class SaveConfirmListenerConcern implements ConfirmPopup.SelectionListener {
        private final DisplayClassDiagramScene scene;

        /**
         * Creates a new instance for the given scene.
         * 
         * @param scene the scene for this listener
         */
        private SaveConfirmListenerConcern(DisplayClassDiagramScene scene) {
            this.scene = scene;
        }

        @Override
        public void optionSelected(int selectedOption) {
            NavigationBar navBar = NavigationBar.getInstance();
            
            if (selectedOption == ConfirmPopup.YES_OPTION) {
                BasicActionsUtils.saveModel(scene.getClassDiagram(), new FileBrowserListener() {
                    @Override
                    public void modelSaved(File file) {
                       
                        navBar.popSection(COREConcern.class, 
                                navBar.getCurrentConcern());
                    }

                    @Override
                    public void modelLoaded(EObject model) {
                    }
                });
            } else if (selectedOption == ConfirmPopup.NO_OPTION) {
                if (EMFEditUtil.getCommandStack(scene.getClassDiagram()) instanceof CORECommandStack) {
                    CORECommandStack stack = (CORECommandStack) EMFEditUtil.getCommandStack(scene.getClassDiagram());
                    
                    if (stack.canRevertToLastSave()) {
                        stack.goToLastSave();
                    } else {
                        scene.displayPopup(Strings.POPUP_REVERT_IMPOSSIBLE, PopupType.ERROR);
                        return;
                    }
                } else {
                    COREExternalArtefact externalArtefact = (COREExternalArtefact) scene.getArtefact();
                    LoggerUtils.warn(Strings.invalidCommandStackInstance(externalArtefact));
                }
                
                checkForModelRemoval(scene);
                navBar.popSection(COREConcern.class, navBar.getCurrentConcern());
            }
        }
    }
    
    /**
     * A listener for a confirm popup to handle the case of a fast return to the Concern Scene.
     * Handles the user response on whether to save in case modifications occurred.
     */
    private final class SaveConfirmListenerAspect implements ConfirmPopup.SelectionListener {
        private final DisplayClassDiagramScene scene;
//        private final ClassDiagram element;

        /**
         * Creates a new instance for the given scene.
         * 
         * @param scene the scene for this listener
         * @param element the Aspect of interest
         */
        private SaveConfirmListenerAspect(DisplayClassDiagramScene scene, ClassDiagram element) {
            this.scene = scene;
//            this.element = element;
        }

        @Override
        public void optionSelected(int selectedOption) {
            if (selectedOption == ConfirmPopup.YES_OPTION) {
                BasicActionsUtils.saveModel(scene.getClassDiagram(), new FileBrowserListener() {
                    @Override
                    public void modelSaved(File file) {
//                        TODO reenable after navigation bar supports class diagram
//                        NavigationBar.getInstance().loadNewAspect(scene, element);
                    }

                    @Override
                    public void modelLoaded(EObject model) {
                    }
                });
            } else if (selectedOption == ConfirmPopup.NO_OPTION) {
                if (EMFEditUtil.getCommandStack(scene.getClassDiagram()) instanceof CORECommandStack) {
                    CORECommandStack stack = (CORECommandStack) EMFEditUtil.getCommandStack(scene.getClassDiagram());
                    
                    if (stack.canRevertToLastSave()) {
                        stack.goToLastSave();
                    } else {
                        scene.displayPopup(Strings.POPUP_REVERT_IMPOSSIBLE, PopupType.ERROR);
                        return;
                    }
                } else {
                    LoggerUtils.warn("The CommandStack for " + scene.getClassDiagram()
                            + " should be an instance of CORECommandStack.");
                }

                checkForModelRemoval(scene);
//                TODO Reenable after making navbar work with class diagram
//                NavigationBar.getInstance().loadNewAspect(scene, element);
            }
        }
    }

    /**
     * The listener for the generate selector.
     */
    private final class GenerateSelectorListener extends AbstractDefaultRamSelectorListener<GenerateOptions> {

        private final DisplayClassDiagramScene scene;

        /**
         * Creates a new generate selector listener.
         *
         * @param scene the current {@link DisplayAspectScene}
         */
        private GenerateSelectorListener(DisplayClassDiagramScene scene) {
            this.scene = scene;
        }

        @Override
        public void elementSelected(
                RamSelectorComponent<GenerateOptions> selector,
                final GenerateOptions element) {
            final ClassDiagram cd = scene.getClassDiagram();
            RamFileBrowser browser = null;
            
            switch (element) {
                case ECORE:
                    String parentFolder = lastGeneratorSharedFolder.getAbsolutePath();
                    // Suggest the same folder where the aspect is saved in if it is serialized.
                    if (cd.eResource() != null) {
                        parentFolder = cd.eResource().getURI().trimSegments(1).toFileString();
                    }
                    File suggestedFile = new File(parentFolder, scene.getClassDiagram().getName() + ".ecore");
                    browser = new RamFileBrowser(RamFileBrowserType.SAVE, "ecore", suggestedFile);
                    break;
                default:
                    browser = new RamFileBrowser(RamFileBrowserType.FOLDER, "", lastGeneratorSharedFolder);
                    break;
            }
            browser.setCallbackThreaded(true);
            browser.setCallbackPopupMessage(Strings.POPUP_GENERATING);
            // CHECKSTYLE:IGNORE AnonInnerLength: Okay here.
            browser.addFileBrowserListener(new RamFileBrowserListener() {

                @Override
                public void fileSelected(final File path, RamFileBrowser fileBrowser) {
                    try {
                        // Acceleo just prints exceptions during generation on the error console.
                        // Make sure we can write to the folder beforehand.
                        // Any other error that occurs will most likely be a template problem.
                        if (path.isDirectory() && !Files.isWritable(path.toPath())) {
                            throw new IOException("Permission denied. Cannot write to " + path.getAbsolutePath());
                        }

                        switch (element) {
                            case ECORE:
//                                TODO Need to adapt EcoreGenerator to Class Diagram for now
//                                EcoreGenerator ecoreGenerator = new EcoreGenerator(cd, path);
//                                ecoreGenerator.doGenerate();
                                break;
                            case JAVA:
                                JavaGenerator javaGenerator = new JavaGenerator(cd, path,
                                        new ArrayList<Object>());
                                javaGenerator.doGenerate(null);
                                break;
                            case CPP:
                                CppGenerator cppGenerator = new CppGenerator(cd, path,
                                        new ArrayList<Object>());
                                cppGenerator.doGenerate(null);
                                break;
                        }

                        RamApp.getApplication().invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                scene.displayPopup(new RamPopup(
                                        Strings.popupCodeGenerated(element.toString(), path.getAbsolutePath()), true,
                                        PopupType.SUCCESS));
                            }
                        });

                        lastGeneratorSharedFolder = path;
                        // CHECKSTYLE:IGNORE IllegalCatch FOR 1 LINES: Need to catch all types
                    } catch (final Exception e) {
                        e.printStackTrace();
                        RamApp.getApplication().invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                scene.displayPopup(new RamPopup(Strings.POPUP_GENERATION_ERROR + e.getMessage(),
                                        true, PopupType.ERROR));
                            }
                        });
                    }
                }
            });
            browser.display();
        }
    }

    /**
     * Generator target languages or models.
     */
    private enum GenerateOptions {
        ECORE(Strings.OPT_GENERATE_ECORE),
        JAVA(Strings.OPT_GENERATE_JAVA),
        CPP(Strings.OPT_GENERATE_CPP);
        
        private String name;
        
        /**
         * Creates a new literal with the given name.
         * 
         * @param name the name the literal represents
         */
        GenerateOptions(String name) {
            this.name = name;
        }
        
        @Override
        public String toString() {
            return name;
        }
    }

    private static File lastGeneratorSharedFolder = new File(GUIConstants.DIRECTORY_MODELS).getAbsoluteFile();

    @Override
    public void generate(final RamAbstractScene<?> scene) {
        // Create the selector
        OptionSelectorView<GenerateOptions> selector =
                new OptionSelectorView<GenerateOptions>(GenerateOptions.values());
        RamApp.getActiveScene().addComponent(selector, scene.getMenu().getCenterPointGlobal());

        selector.registerListener(new GenerateSelectorListener((DisplayClassDiagramScene) scene));
    }

    @Override
    public void loadScene(RamAbstractScene<?> scene) {
        // Transition to the right

        // Ask the user to load a model
        GenericFileBrowser.loadModel("classdiagram", new FileBrowserListener() {

            @Override
            public void modelLoaded(EObject model) {
                RamApp.getApplication().loadScene(COREArtefactUtil.getReferencingExternalArtefact(model), model);
            }

            @Override
            public void modelSaved(File file) {
            }
        });
    }

    @Override
    public void showValidation(RamAbstractScene<?> scene) {
        ((DisplayClassDiagramScene) scene).showValidation(true);
    }

    @Override
    public void showTracing(RamAbstractScene<?> scene) {
    }

    @Override
    public void back(RamAbstractScene<?> scene) {
        ((DisplayClassDiagramScene) scene).switchToPreviousView();
    }

    @Override
    public void switchToMenu(RamAbstractScene<?> scene) {
        // to the left!
        scene.setTransition(new SlideTransition(RamApp.getApplication(), 700, false));
        // go to SelectAspectScene
        RamApp.getApplication().switchToBackground((DisplayClassDiagramScene) scene);
    }

    @Override
    public void weaveAll(RamAbstractScene<?> scene) {
    }

    @Override
    public void weaveStateMachines(RamAbstractScene<?> scene) {
    }

    @Override
    public void weaveAllNoCSPForStateViews(RamAbstractScene<?> scene) {
    }
     
    @Override
    public void switchToConcern(final RamAbstractScene<?> scene) {
        boolean isSaveNeeded = EMFEditUtil.getCommandStack(((DisplayClassDiagramScene) scene).getClassDiagram())
                .isSaveNeeded();
        if (isSaveNeeded) {
            showCloseConfirmPopup((DisplayClassDiagramScene) scene);
        } else {
            checkForModelRemoval((DisplayClassDiagramScene) scene);
            doSwitchToPreviousScene((DisplayClassDiagramScene) scene);
        }
    }

    /**Method that carries out a fast switch to Concern without checking for Model Removal.
     * Used when more than one DisplayClassDiagramScene is loaded in a cascaded way, 
     * to prevent the removal of Class Diagrams. 
     * @param scene the starting scene
     */
    public void fastSwitchToConcern(final DisplayClassDiagramScene scene) {
        boolean isSaveNeeded = EMFEditUtil.getCommandStack(scene.getClassDiagram()).isSaveNeeded();
        if (isSaveNeeded) {
            showCloseConfirmPopup(scene);
        } else {
            NavigationBar.getInstance().popSection();
            doSwitchToPreviousScene(scene);
        }
    }
    
    /**
     * Method called by the collapse environment when going back from a reuse concern scene.
     * @param scene reused Model scene the user is in
     */
    public void switchBackTo(final DisplayClassDiagramScene scene) {
        COREArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(scene.getClassDiagram());
        COREModelUtil.unloadExternalResources(artefact);
        
        scene.setTransition(new BlendTransition(RamApp.getApplication(), 500));
        
        DisplayClassDiagramScene pA = (DisplayClassDiagramScene) scene.getPreviousScene();
        pA.repushSections();
        pA.getCanvas().addChild(NavigationBar.getInstance());
        pA.switchToView(pA.getClassDiagramView());
        
        scene.getApplication().changeScene(pA);
        scene.getApplication().destroySceneAfterTransition(scene);
    }
   
    /**
     * Display a popup for the user to decided whether he wants to save the Class Diagram or leave the scene.
     *
     * @param scene - The {@link DisplayAspectScene} to consider
     */
    public void showCloseConfirmPopup(final DisplayClassDiagramScene scene) {
        scene.showCloseConfirmPopup(scene, new SaveConfirmListener(scene));
    }

    /**
     * Display a popup for the user to decided whether he wants to save the aspect or go back to ConcernScene.
     *
     * @param scene - The {@link DisplayAspectScene} to consider
     */
    public void showCloseConfirmBackToConcern(final DisplayClassDiagramScene scene) {
        scene.showCloseConfirmPopup(scene, new SaveConfirmListenerConcern(scene));
    }
    
    /**Called when moving from one Class Diagram directly to another Design Model, 
     * to check if the starting one needs save.
     * 
     * @param scene scene we're moving away from
     * @param element Aspect of the scene
     */
    public void showCloseConfirmNextAspect(final DisplayClassDiagramScene scene, final ClassDiagram element) {
        scene.showCloseConfirmPopup(scene, new SaveConfirmListenerAspect(scene, element));
    }

    /**
     * Performs the switching to the concern scene.
     * Unloads the resource and triggers the scene change to the previous scene.
     *
     * @param scene the current aspect scene
     */
    protected void doSwitchToPreviousScene(DisplayClassDiagramScene scene) {
        // Unload WovenAspect of this aspect when we leave
        COREArtefact artefact = scene.getArtefact();
        COREModelUtil.unloadExternalResources(artefact);
        if (scene.getPreviousScene() instanceof DisplayClassDiagramScene) {
            scene.setTransition(new SlideUpDownTransition(RamApp.getApplication(), 500, true));

            scene.getApplication().changeScene(scene.getPreviousScene());
            if (scene.getPreviousScene() instanceof DisplayClassDiagramScene) {
                ((DisplayClassDiagramScene) scene.getPreviousScene()).repushSections(); 
            }
            scene.getApplication().destroySceneAfterTransition(scene);
        } else if (scene.getPreviousScene() instanceof DisplayConcernEditScene) {
            // Temporary workaround until navigation properly takes care of all navigation.
            NavigationBar.getInstance().popSection();
            scene.getPreviousScene().getCanvas().addChild(NavigationBar.getInstance());
            scene.setTransition(new SlideTransition(RamApp.getApplication(), 500, false));

            scene.getApplication().changeScene(scene.getPreviousScene());
            scene.getApplication().destroySceneAfterTransition(scene);
        }

    }

    /**
     * Checks whether the artefact needs to be removed from the concern.
     * The artefact is removed, if it was not saved and therefore not contained anywhere.
     *
     * @param scene the current class diagram scene
     */
    protected static void checkForModelRemoval(DisplayClassDiagramScene scene) {

        ClassDiagram aspect = scene.getClassDiagram();
        COREArtefact artefact = scene.getArtefact();

        // That is the aspect is not saved at all.
        if (aspect.eResource() == null) {

            // Get all the features realizing it
            List<COREFeature> listOfFeatures = artefact.getScene().getRealizes();
            List<COREFeature> copyOfListOfFeature = new ArrayList<COREFeature>(listOfFeatures);

            // Loop through all the features and remove the realization
            for (COREFeature feature : copyOfListOfFeature) {
                // Do a pre-mature check to see if the feature realizes the scene,
                // Can exist scenario, where the bi directional link might not be true
                if (feature.getRealizedBy().contains(artefact.getScene())) {
                    feature.getRealizedBy().remove(artefact.getScene());
                }
            }
            
            COREConcern concern = artefact.getCoreConcern();

            // Remove from the list of artefacts
            if (concern != null) {
                concern.getArtefacts().remove(artefact);
            }
        }
    }

    /**
     * Handles hiding/showing notes in Class Diagram.
     * 
     * @param scene the affected {@link DisplayAspectScene}
     * @return the new visibility status of notes
     */
    public boolean toggleNotes(DisplayClassDiagramScene scene) {
        ClassDiagramView classDiagramView = scene.getClassDiagramView();
        
        return classDiagramView.toggleNotes();
        
        
    }   
    
    @Override
    public void layout(RamAbstractScene<?> scene) {
        Layout newLayout = new AutomaticLayout(70);
        newLayout.layout(CdmModelUtils.getClassDiagramViewFromApp(RamApp.getApplication()).getContainerLayer(), 
                Layout.LayoutUpdatePhase.FROM_PARENT);
    }

    @Override
    public void switchToCompositionEditMode(RamAbstractScene<?> scene, CompositionView compositionView) {
    }

    @Override
    public void closeSplitView(RamAbstractScene<?> displayAspectScene) {
        
    }
}
