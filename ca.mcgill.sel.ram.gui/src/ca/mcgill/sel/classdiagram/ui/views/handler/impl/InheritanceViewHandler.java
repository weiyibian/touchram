package ca.mcgill.sel.classdiagram.ui.views.handler.impl;

import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;

import ca.mcgill.sel.classdiagram.Class;
import ca.mcgill.sel.classdiagram.ui.views.CDEnd;
import ca.mcgill.sel.classdiagram.ui.views.ClassView;
import ca.mcgill.sel.classdiagram.ui.views.handler.IRelationshipViewHandler;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.perspective.COREPerspectiveUtil;
import ca.mcgill.sel.core.perspective.PerspectiveName;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamImageComponent;
import ca.mcgill.sel.ram.ui.components.RamSelectorComponent;
import ca.mcgill.sel.ram.ui.components.listeners.AbstractDefaultRamSelectorListener;
import ca.mcgill.sel.ram.ui.perspective.PerspectiveControllerFactory;
import ca.mcgill.sel.ram.ui.perspective.classdiagram.CdmPerspectiveController;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.views.OptionSelectorView;
import ca.mcgill.sel.ram.ui.views.OptionSelectorView.Iconified;
import ca.mcgill.sel.ram.ui.views.handler.BaseHandler;


/**
 * Default handler for inheritance view.
 * 
 * @author eyildirim
 * @author mschoettle
 * @author yhattab
 */
public class InheritanceViewHandler extends BaseHandler implements IRelationshipViewHandler {
    
    /**
     * The options for inheritance views.
     */
    private enum InheritanceOptions implements Iconified {
        DELETE(new RamImageComponent(Icons.ICON_DELETE, Colors.ICON_DELETE_COLOR));
        
        private RamImageComponent icon;
        
        /**
         * Creates a new option.
         * 
         * @param icon the icon to use for this option
         */
        InheritanceOptions(RamImageComponent icon) {
            this.icon = icon;
        }
        
        @Override
        public RamImageComponent getIcon() {
            return icon;
        }
        
    }
    
    @Override
    public boolean processTapAndHold(TapAndHoldEvent tapAndHoldEvent, CDEnd<?, ?> end) {
        // do nothing right now
        return true;
    }
    
    @Override
    public boolean processDoubleTap(TapEvent tapEvent, CDEnd<?, ?> end) {
        @SuppressWarnings("unchecked")
        final CDEnd<Class, ClassView> ramEnd1 = (CDEnd<Class, ClassView>) end;
        final CDEnd<Class, ClassView> ramEnd2 = ramEnd1.getOpposite();
        
        OptionSelectorView<InheritanceOptions> selector = 
                new OptionSelectorView<InheritanceViewHandler.InheritanceOptions>(InheritanceOptions.values());
        
        RamApp.getActiveScene().addComponent(selector, tapEvent.getLocationOnScreen());
        
        selector.registerListener(new AbstractDefaultRamSelectorListener<InheritanceViewHandler.InheritanceOptions>() {
            @Override
            public void elementSelected(RamSelectorComponent<InheritanceOptions> selector, InheritanceOptions element) {
                CdmPerspectiveController cdmController = PerspectiveControllerFactory.INSTANCE
                        .getCdmPerspectiveController();
                COREPerspective perspective = COREPerspectiveUtil.INSTANCE.getModelPerspective(ramEnd1.getModel());
                
                switch (element) {
                    case DELETE:
                        switch (perspective.getName()) {
                            case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE: 
                                cdmController.removeSuperType(ramEnd1.getModel(), ramEnd2.getModel());
                                break;
                            case PerspectiveName.DESIGN_MODEL_PERSPECTIVE: 
                                cdmController.removeSuperType(ramEnd1.getModel(), ramEnd2.getModel());
                                break;
                        } 
                        break;
                }
            }
        });
        
        return true;
    }
    
}
