package ca.mcgill.sel.classdiagram.ui.views.handler.impl;

import java.lang.reflect.Modifier;
import java.lang.reflect.TypeVariable;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.mt4j.input.inputData.InputCursor;
import org.mt4j.input.inputProcessors.MTGestureEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;
import org.mt4j.input.inputProcessors.componentProcessors.unistrokeProcessor.UnistrokeEvent;
import org.mt4j.input.inputProcessors.componentProcessors.unistrokeProcessor.UnistrokeUtils.UnistrokeGesture;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.classdiagram.Association;
import ca.mcgill.sel.classdiagram.CDEnum;
import ca.mcgill.sel.classdiagram.CdmFactory;
import ca.mcgill.sel.classdiagram.CdmPackage;
import ca.mcgill.sel.classdiagram.Class;
import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.classdiagram.Classifier;
import ca.mcgill.sel.classdiagram.ImplementationClass;
import ca.mcgill.sel.classdiagram.NamedElement;
import ca.mcgill.sel.classdiagram.Note;
import ca.mcgill.sel.classdiagram.Type;
import ca.mcgill.sel.classdiagram.TypeParameter;
import ca.mcgill.sel.classdiagram.impl.ElementMapImpl;
import ca.mcgill.sel.classdiagram.language.controller.ControllerFactory;
import ca.mcgill.sel.classdiagram.ui.utils.CdmModelUtils;
import ca.mcgill.sel.classdiagram.ui.views.BaseView;
import ca.mcgill.sel.classdiagram.ui.views.ClassDiagramView;
import ca.mcgill.sel.classdiagram.ui.views.ClassView;
import ca.mcgill.sel.classdiagram.ui.views.ClassifierView;
import ca.mcgill.sel.classdiagram.ui.views.ImplementationClassSelectorView;
import ca.mcgill.sel.classdiagram.ui.views.NaryAssociationNodeView;
import ca.mcgill.sel.classdiagram.ui.views.NoteView;
import ca.mcgill.sel.classdiagram.ui.views.handler.IClassDiagramViewHandler;
import ca.mcgill.sel.classdiagram.util.CdmModelUtil;
import ca.mcgill.sel.commons.StringUtil;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.perspective.COREPerspectiveUtil;
import ca.mcgill.sel.core.perspective.PerspectiveName;
import ca.mcgill.sel.ram.loaders.RamClassLoader;
import ca.mcgill.sel.ram.loaders.RamClassUtils;
import ca.mcgill.sel.ram.loaders.exceptions.MissingJarException;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamPopup.PopupType;
import ca.mcgill.sel.ram.ui.components.RamSelectorComponent;
import ca.mcgill.sel.ram.ui.components.listeners.AbstractDefaultRamSelectorListener;
import ca.mcgill.sel.ram.ui.components.navigationbar.NavigationBar;
import ca.mcgill.sel.ram.ui.perspective.PerspectiveControllerFactory;
import ca.mcgill.sel.ram.ui.perspective.classdiagram.CdmPerspectiveController;
import ca.mcgill.sel.ram.ui.utils.GUIConstants;
import ca.mcgill.sel.ram.ui.utils.MathUtils;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.utils.UnistrokeProcessorUtils;
import ca.mcgill.sel.ram.ui.views.AbstractView;
import ca.mcgill.sel.ram.ui.views.OptionSelectorView;
import ca.mcgill.sel.ram.ui.views.handler.impl.AbstractViewHandler;

/**
 * The default handler for a {@link ClassDiagramView}.
 *
 * @author mschoettle
 * @author yhattab
 */
public class ClassDiagramViewHandler extends AbstractViewHandler implements IClassDiagramViewHandler {

    /**
     * The options to display when tap-and-hold is performed, with a method to return the valid options for a context.
     */
    private enum CreateFeature {
        CREATE_CLASS, CREATE_DATA_TYPE, CREATE_ENUM, CREATE_NOTE,
        IMPORT_IMPLEMENTATION_CLASS, CREATE_NARY_RELATIONSHIP, CREATE_ASSOCIATION_CLASS;
        
        /**
         * Returns the valid options one can create given language actions and number of selected classes.
         * @param nbSelectedClasses number of classes currently selected
         * @return array of valid CreateFeature members
         */
        public static CreateFeature[] getValidValues(int nbSelectedClasses) {
            ArrayList<CreateFeature> validValues = new ArrayList<CreateFeature>();
            // this was used before implementing perspective
            // COREPerspective perspective = CdmModelUtils.getObjectCOREPerspective(null);
                                       
            COREPerspective perspective = NavigationBar.getInstance().getCurrentPerspective();
            if (perspective != null) {
                if (PerspectiveControllerFactory.INSTANCE.getActionValidator().canCreateNewClass(perspective)) {
                    validValues.add(CREATE_CLASS);
                    validValues.add(CREATE_DATA_TYPE);
                }
                if (PerspectiveControllerFactory.INSTANCE.getActionValidator().canCreateEnum(perspective)) {
                    validValues.add(CREATE_ENUM);
                }
                if (PerspectiveControllerFactory.INSTANCE.getActionValidator().canCreateNewNote(perspective)) {
                    validValues.add(CREATE_NOTE);
                }
                if (PerspectiveControllerFactory.INSTANCE.getActionValidator().canCreateNaryAssociation(perspective)) {
                    if (nbSelectedClasses > 1) {
                        validValues.add(CREATE_ASSOCIATION_CLASS);
                        if (nbSelectedClasses > 2) {
                            validValues.add(CREATE_NARY_RELATIONSHIP);
                        }
                    }
                }
                if (PerspectiveControllerFactory.INSTANCE.getActionValidator()
                        .canCreateImplementationClass(perspective)) {
                    validValues.add(IMPORT_IMPLEMENTATION_CLASS);
                }
            }
            
            return validValues.toArray(new CreateFeature[validValues.size()]);
        }
    }
    
    /**
     * The available options for relationships.
     */
    private enum CreateRelationship {
        ASSOCIATION, ASSOCIATION_CLASS, INHERITANCE, ANNOTATION
    }
    
    /**
     * Handles the creation of a new class. Takes care of opening a keyboard for the name once the class was added to
     * the structural view.
     *
     * @param view the structural view view representing the structural view
     * @param dataType whether the class should be a data type, <code>true</code> if it is
     * @param isInterface whether the class is an interface
     * @param position the position where the class should be located
     */
    @SuppressWarnings("static-method")
    private void createNewClass(final ClassDiagramView view, boolean dataType, boolean isInterface,
            final Vector3D position) {
        final ClassDiagram cd = (ClassDiagram) view.getClassDiagram();

        cd.eAdapters().add(new EContentAdapter() {

            private Class clazz;

            @Override
            public void notifyChanged(Notification notification) {
                if (notification.getFeature() == CdmPackage.Literals.CLASS_DIAGRAM__CLASSES) {
                    if (notification.getEventType() == Notification.ADD) {
                        clazz = (Class) notification.getNewValue();
                    }
                } else if (notification.getFeature() == CdmPackage.Literals.CONTAINER_MAP__VALUE) {
                    if (notification.getEventType() == Notification.ADD) {
                        ((ClassView) view.getClassViewOf(clazz)).showKeyboard();
                        ((ClassView) view.getClassViewOf(clazz)).clearNameField();
                        cd.eAdapters().remove(this);
                    }
                }
            }
        });

        // Look for the highest index used by any class
        String defaultName = dataType ? Strings.DEFAULT_DATATYPE_NAME : Strings.DEFAULT_CLASS_NAME;
        Set<String> names = new HashSet<String>();

        for (Classifier c : view.getClassDiagram().getClasses()) {
            names.add(c.getName());
        }        
        String className = StringUtil.createUniqueName(defaultName, names);
        
        COREPerspective perspective = COREPerspectiveUtil.INSTANCE.getModelPerspective(cd);
        switch (perspective.getName()) {
            case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE: 
                PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController()
                    .createNewClass(cd, className, dataType, 
                        isInterface, position.getX(), position.getY());
                break;
            case PerspectiveName.DESIGN_MODEL_PERSPECTIVE: 
                PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController()
                    .createNewClass(cd, className, dataType, 
                        isInterface, position.getX(), position.getY());
                break;
        }  
    }

    /**
     * Handles the creation of a note in the class diagram.
     *
     * @param view the view representing the current class diagram
     * @param position the position where the note should be located
     */
    @SuppressWarnings("static-method")
    private void createNewNote(final ClassDiagramView view, final Vector3D position) {
        final ClassDiagram cd = view.getClassDiagram();
        
        cd.eAdapters().add(new EContentAdapter() {
            
            private Note note;
            
            @Override
            public void notifyChanged(Notification notification) {
                if (notification.getFeature() == CdmPackage.Literals.CLASS_DIAGRAM__NOTES) {
                    if (notification.getEventType() == Notification.ADD) {
                        note = (Note) notification.getNewValue();
                    }
                } else if (notification.getFeature() == CdmPackage.Literals.CONTAINER_MAP__VALUE) {
                    if (notification.getEventType() == Notification.ADD) {
                        view.getNoteView(note).showKeyboard();
                        cd.eAdapters().remove(this);
                    }
                }
                
            }
        });
       
        COREPerspective perspective = COREPerspectiveUtil.INSTANCE.getModelPerspective(cd);
        switch (perspective.getName()) {
            case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE: 
                PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController().createNewNote(
                        cd, position.getX(), position.getY());
                break;
            case PerspectiveName.DESIGN_MODEL_PERSPECTIVE: 
                PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController().createNewNote(
                        cd, position.getX(), position.getY());
                break;
        } 
    }

   /**
     * Displays a selector with implementation classes that can be created.
     *
     * @param view View where the implementation class selector should be added
     * @param position The position where the selector should be added.
     */
    @SuppressWarnings("static-method")
    private void createNewImplementationClass(final ClassDiagramView view, final Vector3D position) {
        // Get the structural view
        final ClassDiagram cd = view.getClassDiagram();

        // Get all the implementation classes that already exists in the view (only non generic ones)
        List<String> existingImplementationClasses = new ArrayList<String>();
        for (Classifier classifier : view.getClassDiagram().getClasses()) {
            if (classifier instanceof ImplementationClass) {
                // If it's not generic add it to list
                if (((ImplementationClass) classifier).getTypeParameters().size() == 0) {
                    existingImplementationClasses.add(((ImplementationClass) classifier).getInstanceClassName());
                }
            }
        }

        // Create an implementation class selector view
        ImplementationClassSelectorView selector = new ImplementationClassSelectorView(existingImplementationClasses);

        selector.registerListener(new AbstractDefaultRamSelectorListener<String>() {
            @Override
            public void elementSelected(RamSelectorComponent<String> selector, String element) {
                handleImplementationClassSelection(cd, element, position.getX(), position.getY());
            }
        });

        // Add selector to screen + keyboard
        RamApp.getActiveScene().addComponent(selector, position);
        selector.displayKeyboard();
    }

    /**
     * Handles the selection of an implementation class to import by the user.
     *
     * @param cd the {@link ClassDiagram} the class should be added to
     * @param className the name
     * @param x the x position of the class
     * @param y the y position of the class
     */
    private static void handleImplementationClassSelection(ClassDiagram cd, String className,
            float x, float y) {
        CdmPerspectiveController cdmController = PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController();
        try {
            java.lang.Class<?> loadedClass = RamClassLoader.INSTANCE.retrieveClass(className);
            if (RamClassLoader.INSTANCE.isLoadableEnum(className)) {
                List<String> literals = new ArrayList<String>();
                for (Object literal : loadedClass.getEnumConstants()) {
                    literals.add(literal.toString());
                }
                cdmController.createImplementationEnum(cd, RamClassUtils.extractClassName(className),
                        className, x, y, literals);
            } else {
                List<TypeParameter> typeParameters = new ArrayList<TypeParameter>();
                for (TypeVariable<?> tv : loadedClass.getTypeParameters()) {
                    TypeParameter typeParameter = CdmFactory.eINSTANCE.createTypeParameter();
                    typeParameter.setName(tv.getName());
                    typeParameters.add(typeParameter);
                }
                boolean isInterface = RamClassLoader.INSTANCE.isLoadableInterface(className);
                boolean isAbstract = Modifier.isAbstract(loadedClass.getModifiers());
                Set<String> superTypes = RamClassLoader.INSTANCE.getAllSuperClassesFor(className);
                Set<ImplementationClass> subTypes = CdmModelUtils.getExistingSubTypesFor(cd, className);
                // If an element is selected create the implementation class with the controller
                cdmController.createImplementationClass(cd, className, typeParameters, isInterface, isAbstract,
                        x, y, superTypes, subTypes);
            }
        } catch (MissingJarException e) {
            RamApp.getActiveScene().displayPopup(Strings.popupClassNotFound(e.getMessage()), PopupType.ERROR);
        }
    }

    /**
     * Create an Enum to be added to the structural view.
     *
     * @param view View where the enum should be added
     * @param position The position where it should be added.
     */
    @SuppressWarnings("static-method")
    private void createNewEnum(final ClassDiagramView view, Vector3D position) {
        final ClassDiagram cd = view.getClassDiagram();

        // Will show keyboard when the enum view appears on screen
        cd.eAdapters().add(new EContentAdapter() {

            private CDEnum renum;

            @Override
            public void notifyChanged(Notification notification) {
                if (notification.getFeature() == CdmPackage.Literals.CLASS_DIAGRAM__TYPES) {
                    if (notification.getEventType() == Notification.ADD) {
                        renum = (CDEnum) notification.getNewValue();
                    }
                } else if (notification.getFeature() == CdmPackage.Literals.CONTAINER_MAP__VALUE) {
                    if (notification.getEventType() == Notification.ADD) {
                        view.getEnumView(renum).showKeyboard();
                        view.getEnumView(renum).clearNameField();
                        cd.eAdapters().remove(this);
                    }
                }
            }
        });

        // Find a unique name for the enum
        Set<String> names = new HashSet<String>();
        for (Type type : view.getClassDiagram().getTypes()) {
            if (type instanceof CDEnum) {
                names.add(((CDEnum) type).getName());
            }
        }        
        String enumName = StringUtil.createUniqueName(Strings.DEFAULT_ENUM_NAME, names);
        
        // Create the REnum using the respective perspective controller
        COREPerspective perspective = COREPerspectiveUtil.INSTANCE.getModelPerspective(cd);
        switch (perspective.getName()) {
            case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE: 
                PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController().createEnum(cd,
                        enumName, position.getX(), position.getY());
                break;
            case PerspectiveName.DESIGN_MODEL_PERSPECTIVE: 
                PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController().createEnum(cd,
                        enumName, position.getX(), position.getY());
                break;
        } 
    }

    /**
     * Create an nary association between the classifiers of the provided views.
     * @param cdView class diagram view where the association will be added
     * @param selectedClassifierViews classifierViews of classifiers to link with a nary association
     * @param position where it should be added
     */
    protected void createNaryAssociation(final ClassDiagramView cdView, Set<ClassifierView<?>> selectedClassifierViews, 
            Vector3D position) {
        final ClassDiagram cd = cdView.getClassDiagram();

        Set<Classifier> linkedClassifiers = new HashSet<Classifier>();
        for (ClassifierView<?> classView : selectedClassifierViews) {
            linkedClassifiers.add(classView.getClassifier());
        }

        // Create the association using the perspective controller
        COREPerspective perspective = COREPerspectiveUtil.INSTANCE.getModelPerspective(cd);
        switch (perspective.getName()) {
            case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE: 
                PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController().createNaryAssociation(
                        cd, linkedClassifiers, position.getX(), position.getY(), false);
                break;
            case PerspectiveName.DESIGN_MODEL_PERSPECTIVE: 
                PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController().createNaryAssociation(
                        cd, linkedClassifiers, position.getX(), position.getY(), false);
                break;
        } 
    }

    /**
     * Create an association between the selected class and a new Association class for that association.
     * @param cdView class diagram view where the association will be added
     * @param selectedClassifierViews classifierViews of classifiers to link with a nary association
     * @param position where it should be added
     */
    protected void createNewAssociationClass(final ClassDiagramView cdView, 
            Set<ClassifierView<?>> selectedClassifierViews, Vector3D position) {
        final ClassDiagram cd = cdView.getClassDiagram();
        
        cd.eAdapters().add(new EContentAdapter() {
            private Association association;
            private Class associationClass;

            @Override
            public void notifyChanged(Notification notification) {
                if (notification.getFeature() == CdmPackage.Literals.CONTAINER_MAP__VALUE) {
                    if (notification.getEventType() == Notification.ADD) {
                        ElementMapImpl elementMap = (ElementMapImpl) notification.getNewValue();
                        if (elementMap.getKey() instanceof Association) {
                            association = (Association) elementMap.getKey();
                            associationClass = association.getAssociationClass();
                            NaryAssociationNodeView associationView = cdView.getNaryAssociationNodeViewOf(association);
                            //move association class further from node
                            Vector3D newClassPosition = associationView.getAssociationClassCreationPosition();
                            ControllerFactory.INSTANCE.getClassController().moveClassifier(associationClass, 
                                    newClassPosition.getX(), newClassPosition.getY());
                            //allow input of the association class name
                            ((ClassView) cdView.getClassViewOf(associationClass)).showKeyboard();
                            ((ClassView) cdView.getClassViewOf(associationClass)).clearNameField();
                            cd.eAdapters().remove(this);
                        }
                    }
                }
            }
        });
        List<Classifier> linkedClassifiers = new ArrayList<Classifier>();
        for (ClassifierView<?> classView : selectedClassifierViews) {
            linkedClassifiers.add(classView.getClassifier());
        }

        //Create the association using the perspective controller
        COREPerspective perspective = COREPerspectiveUtil.INSTANCE.getModelPerspective(cd);
        switch (perspective.getName()) {
            case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE: 
                PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController().createNaryAssociation(cd, 
                        new HashSet<Classifier>(linkedClassifiers), position.getX(), position.getY(), true);
                break;
            case PerspectiveName.DESIGN_MODEL_PERSPECTIVE: 
                PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController().createNaryAssociation(cd, 
                        new HashSet<Classifier>(linkedClassifiers), position.getX(), position.getY(), true);
                break;
        } 
        
    }

    /**
     * This will check to see if the delete(x) gesture is done over a class. If yes it will delete the class.
     * Determining if the gesture was done over a class is by comparing the x and y values of the start ] and end
     * position of the mouse cursor with the position of the class.
     *
     * @param view this represent the structural view in which the gesture was performed on
     * @param startPosition The position of the cursor when the gesture was started
     * @param endPosition The position of the cursor when the gesture was ended
     * @param inputCursor the input cursor of the event
     */
    @SuppressWarnings("static-method")
    private void deleteClass(ClassDiagramView view, Vector3D startPosition, Vector3D endPosition,
            InputCursor inputCursor) {

        Vector3D intersection = UnistrokeProcessorUtils.getIntersectionPoint(startPosition, endPosition, inputCursor);

        float intersectionX = intersection.x;
        float intersectionY = intersection.y;

        // find the class(es) that are underneath the intersection
        for (BaseView<?> baseView : view.getBaseViews()) {
            float left = baseView.getX();
            float upper = baseView.getY();
            float right = left + baseView.getWidth();
            float lower = upper + baseView.getHeight();

            CdmPerspectiveController cdmController = PerspectiveControllerFactory.INSTANCE
                    .getCdmPerspectiveController();
            if (left < intersectionX && right > intersectionX && upper < intersectionY && lower > intersectionY) {
                if (baseView instanceof NoteView) {
                    COREPerspective perspective = COREPerspectiveUtil.INSTANCE.getModelPerspective(
                            ((NoteView) baseView).getNote());
                    switch (perspective.getName()) {
                        case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE: 
                            cdmController.removeNote(((NoteView) baseView).getNote());
                            break;
                        case PerspectiveName.DESIGN_MODEL_PERSPECTIVE: 
                            cdmController.removeNote(((NoteView) baseView).getNote());
                            break;
                    } 
                } else if (baseView instanceof NaryAssociationNodeView) {
                    COREPerspective perspective = COREPerspectiveUtil.INSTANCE.getModelPerspective(
                            view.getClassDiagram());
                    switch (perspective.getName()) {
                        case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE: 
                            cdmController.deleteAssociation(view.getClassDiagram(), 
                                    ((NaryAssociationNodeView) baseView).getAssociation());
                            break;
                        case PerspectiveName.DESIGN_MODEL_PERSPECTIVE: 
                            cdmController.deleteAssociation(view.getClassDiagram(), 
                                    ((NaryAssociationNodeView) baseView).getAssociation());
                            break;
                    } 
                } else {
                    COREPerspective perspective = COREPerspectiveUtil.INSTANCE.getModelPerspective(
                            baseView.getClassifier());
                    switch (perspective.getName()) {
                        case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE: 
                            cdmController.removeClassifier(baseView.getClassifier());
                            break;
                        case PerspectiveName.DESIGN_MODEL_PERSPECTIVE: 
                            cdmController.removeClassifier(baseView.getClassifier());
                            break;
                    } 
                }
                
                break;
            }
        }

    }

    @Override
    public void dragAllSelectedClasses(ClassDiagramView cdv, Vector3D directionVector) {
        for (BaseView<?> baseView : cdv.getSelectedElements()) {
            // create a copy of the translation vector, because translateGlobal modifies it
            baseView.translateGlobal(new Vector3D(directionVector));
        }
    }

    @Override
    public boolean handleDoubleTapOnClass(ClassDiagramView structuralView, BaseView<?> targetBaseView) {
        return false;
    }

    @Override
    public boolean handleTapAndHoldOnClass(ClassDiagramView structuralView, BaseView<?> targetClassifierView) {
        return false;
    }

    @Override
    public boolean processTapAndHoldEvent(final TapAndHoldEvent tapAndHoldEvent) {
        if (tapAndHoldEvent.isHoldComplete()) {
            final ClassDiagramView target = (ClassDiagramView) tapAndHoldEvent.getTarget();            
            final Vector3D position = tapAndHoldEvent.getLocationOnScreen();
            float tapLocationX = tapAndHoldEvent.getLocationOnScreen().x;
            float tapLocationY = tapAndHoldEvent.getLocationOnScreen().y;
            
            int nbSelected = target.getSelectedClassifierViews().size();
            OptionSelectorView<CreateFeature> selector = 
                        new OptionSelectorView<CreateFeature>(CreateFeature.getValidValues(nbSelected));
            
            RamApp.getActiveScene().addComponent(selector, tapAndHoldEvent.getLocationOnScreen());
            // CHECKSTYLE:IGNORE AnonInnerLength: Okay here.
            selector.registerListener(new AbstractDefaultRamSelectorListener<CreateFeature>() {

                @Override
                public void elementSelected(RamSelectorComponent<CreateFeature> selector, CreateFeature element) {
                    Vector3D associationPosition = new Vector3D(tapLocationX, tapLocationY);
                    switch (element) {
                        case CREATE_CLASS:
                            createNewClass(target, false, false, position);
                            break;
                        case CREATE_DATA_TYPE:
                            createNewClass(target, true, false, position);
                            break;
                        case CREATE_ENUM:
                            createNewEnum(target, position);
                            break;
                        case CREATE_NOTE:
                            createNewNote(target, position);
                            break;
                        case IMPORT_IMPLEMENTATION_CLASS:
                            createNewImplementationClass(target, position);
                            break;
                        case CREATE_ASSOCIATION_CLASS:
                            createNewAssociationClass(target, target.getSelectedClassifierViews(), associationPosition);
                            break;
                        case CREATE_NARY_RELATIONSHIP:
                            createNaryAssociation(target, target.getSelectedClassifierViews(), associationPosition);
                            break;    
                    }
                }
            });
        }
        return true;
    }

    @Override
    public boolean processTapEvent(TapEvent tapEvent) {
        if (tapEvent.isTapped()) {

            ClassDiagramView target = (ClassDiagramView) tapEvent.getTarget();
            target.deselect();
        }

        return true;
    }

    @Override
    public void handleUnistrokeGesture(AbstractView<?> target, UnistrokeGesture gesture, Vector3D startPosition,
            UnistrokeEvent event) {
        ClassDiagramView classDiagramView = (ClassDiagramView) target;

        switch (event.getId()) {
            case MTGestureEvent.GESTURE_ENDED:
                Vector3D startGesturePoint = event.getCursor().getStartPosition();
                Vector3D endGesturePoint = event.getCursor().getPosition();
                BaseView<?> startView = null;
                BaseView<?> endView = null;

                for (BaseView<?> view : classDiagramView.getBaseViews()) {
                    if (MathUtils.pointIsInRectangle(startGesturePoint, view, GUIConstants.MARGIN_ELEMENT_DETECTION)) {
                        startView = view;
                    }
                    if (view.containsPointGlobal(endGesturePoint)) {
                        endView = view;
                    }
                    
                    if (startView != null & endView != null) {
                        handleRelationshipCreation(classDiagramView, startView, endView, endGesturePoint);
                        return;
                    }
                }
                break;
        }

        switch (gesture) {
            case RECTANGLE:
                createNewClass(classDiagramView, false, false, startPosition);
                break;
            case X:
                Vector3D endPosition = event.getCursor().getPosition();
                deleteClass(classDiagramView, startPosition, endPosition, event.getCursor());
                break;
            case CIRCLE:
                createNewEnum(classDiagramView, startPosition);
                break;
            case TRIANGLE:
                createNewImplementationClass(classDiagramView, startPosition);
                break;
        }
    }

    /**
     * Handles creation of relationships for the given start (from) and end view (to).
     * 
     * @param classDiagramView the {@link ClassDiagramView} this gesture was performed in
     * @param startView the {@link BaseView} from which the gesture was performed
     * @param endView the {@link BaseView} to which the gesture was performed
     * @param endGesturePoint the position gesture finished at
     */
    private static void handleRelationshipCreation(ClassDiagramView classDiagramView,
            BaseView<?> startView, BaseView<?> endView,
            Vector3D endGesturePoint) {
        ClassDiagram cd = classDiagramView.getClassDiagram();
        EnumSet<CreateRelationship> availableOptions = EnumSet.allOf(CreateRelationship.class);
        //these will be null if views aren't instances of ClassifierView
        Classifier fromClass = startView.getClassifier();
        Classifier toClass = endView.getClassifier();
        
        NamedElement targetElement;
        Note fromNote;
        Association naryAssociation;
        //this approach allows to draw from or to a note or an nary association
        if (startView instanceof NoteView || startView instanceof NaryAssociationNodeView) {
            if (startView instanceof NoteView) {
                fromNote = ((NoteView) startView).getNote();
                naryAssociation = null;
                targetElement = (NamedElement) endView.getRepresented();
            } else {
                naryAssociation = ((NaryAssociationNodeView) startView).getAssociation();
                fromNote = null;
                targetElement = toClass;
            }
        } else if (endView instanceof NoteView || endView instanceof NaryAssociationNodeView) {
            if (endView instanceof NoteView) {
                fromNote = ((NoteView) endView).getNote();
                naryAssociation = null;
                targetElement = (NamedElement) startView.getRepresented();
            } else {
                naryAssociation = ((NaryAssociationNodeView) endView).getAssociation();
                fromNote = null;
                targetElement = fromClass;
            }
        } else {
            fromNote = null;
            targetElement = null;
            naryAssociation = null;
        }
         
        COREPerspective perspective = COREPerspectiveUtil.INSTANCE.getModelPerspective(cd);
        if (startView instanceof ClassifierView && endView instanceof ClassifierView 
                && PerspectiveControllerFactory.INSTANCE.getActionValidator()
                        .canCreateAssociation(perspective)) {
            //from a Classifer to another
            availableOptions.remove(CreateRelationship.ANNOTATION);
            availableOptions.remove(CreateRelationship.ASSOCIATION_CLASS);
            
            if (!CdmModelUtil.canCreateAssociation(fromClass, toClass)) {
                availableOptions.remove(CreateRelationship.ASSOCIATION);
            }
            if (!CdmModelUtil.canCreateInheritance(fromClass, toClass)) {
                availableOptions.remove(CreateRelationship.INHERITANCE);
            }
        } else if (fromNote != null) {
            //Linking a note to a classifier (excluding linking two notes)
            availableOptions.remove(CreateRelationship.ASSOCIATION);
            availableOptions.remove(CreateRelationship.INHERITANCE);
            availableOptions.remove(CreateRelationship.ASSOCIATION_CLASS);
            //check if there's already a link, can't link note to same element twice
            if (fromNote.getNotedElement().contains(targetElement)) {
                availableOptions.remove(CreateRelationship.ANNOTATION);
            }
        } else if (naryAssociation != null) {
            //Linking a class to an Nary association as an Association class
            availableOptions.remove(CreateRelationship.ASSOCIATION);
            availableOptions.remove(CreateRelationship.INHERITANCE);
            availableOptions.remove(CreateRelationship.ANNOTATION);
            //check if there's already an association class and that we're linking a Class
            if (naryAssociation.getAssociationClass() != null || !(targetElement instanceof Class)) {
                availableOptions.remove(CreateRelationship.ASSOCIATION_CLASS);
            }
        } else {
            availableOptions.remove(CreateRelationship.ANNOTATION);
            availableOptions.remove(CreateRelationship.ASSOCIATION);
            availableOptions.remove(CreateRelationship.ASSOCIATION_CLASS);
            availableOptions.remove(CreateRelationship.INHERITANCE);
        }
        
        if (!availableOptions.isEmpty()) {
            OptionSelectorView<CreateRelationship> selector = 
                    new OptionSelectorView<CreateRelationship>(availableOptions.toArray(new CreateRelationship[] { }));
            
            RamApp.getActiveScene().addComponent(selector, endGesturePoint);

            // CHECKSTYLE:IGNORE AnonInnerLength: Okay here.
            selector.registerListener(new AbstractDefaultRamSelectorListener<CreateRelationship>() {

                @Override
                public void elementSelected(RamSelectorComponent<CreateRelationship> selector,
                        CreateRelationship element) {
                    
                    CdmPerspectiveController cdmController = PerspectiveControllerFactory.INSTANCE
                            .getCdmPerspectiveController();
                    //COREPerspective perspective = PerspectiveUtil.INSTANCE.getModelPerspective(cd);
                    
                    switch (element) {
                        case ASSOCIATION:
                            boolean bidirectionality  = false;
                            if (!PerspectiveControllerFactory.INSTANCE.getActionValidator()
                                    .canEditVisibility(perspective)) {
                                //In a Domain Model (visibility disallowed), we always create bidirectional association
                                bidirectionality = true;
                            }
                            switch (perspective.getName()) {
                                case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE: 
                                    cdmController.createAssociation(cd, fromClass, toClass, bidirectionality);
                                    break;
                                case PerspectiveName.DESIGN_MODEL_PERSPECTIVE: 
                                    cdmController.createAssociation(cd, fromClass, toClass, bidirectionality);
                                    break;
                            }  
                            break;
                        case INHERITANCE:
                            switch (perspective.getName()) {
                                case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE: 
                                    cdmController.addSuperType((Class) fromClass, toClass);
                                    break;
                                case PerspectiveName.DESIGN_MODEL_PERSPECTIVE: 
                                    cdmController.addSuperType((Class) fromClass, toClass);
                                    break;
                            } 
                            break;
                        case ANNOTATION:
                            switch (perspective.getName()) {
                                case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE: 
                                    cdmController.createAnnotation(cd, fromNote, targetElement);
                                    break;
                                case PerspectiveName.DESIGN_MODEL_PERSPECTIVE: 
                                    cdmController.createAnnotation(cd, fromNote, targetElement);
                                    break;
                            }
                            break;
                        case ASSOCIATION_CLASS:
                            switch (perspective.getName()) {
                                case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE: 
                                    cdmController.setAssociationClass(naryAssociation, (Class) targetElement);
                                    break;
                                case PerspectiveName.DESIGN_MODEL_PERSPECTIVE: 
                                    cdmController.setAssociationClass(naryAssociation, (Class) targetElement);
                                    break;
                            } 
                            break;
                    }
                }
            });
        }
    }

}
