package ca.mcgill.sel.classdiagram.ui.views;

import org.eclipse.emf.common.notify.Notification;
import ca.mcgill.sel.classdiagram.AssociationEnd;
import ca.mcgill.sel.classdiagram.util.CdmModelUtil;
import ca.mcgill.sel.ram.ui.views.TextView;


/**
 * This is a rectangular text component for an association end multiplicity.
 * 
 * @author yhattab
 */
public class MultiplicityTextView extends TextView {

    /**
     * Creates a new text view for the multiplicity.
     *
     * @param associationEnd
     *            The association end to display
     */
    public MultiplicityTextView(AssociationEnd associationEnd) {
        // there is no feature for the multiplicity nor does it need to show full label
        super(associationEnd, null, false);
    }

    @Override
    protected String getModelText() {
        //Hardcoded second argument (isKeyIndexed) to false as it isn't relevant in classdiagrams
        return CdmModelUtil.getMultiplicity((AssociationEnd) getData(),
                ((AssociationEnd) getData()).getQualifier() != null);
    }

    @Override
    public void notifyChanged(Notification notification) {
        if (notification.getNotifier() == getData()) {
            updateText();
        }
    }

    @Override
    public void showKeyboard() {
        super.showKeyboard(this);
        getKeyboard().setSymbolsState(true);
    }
}
