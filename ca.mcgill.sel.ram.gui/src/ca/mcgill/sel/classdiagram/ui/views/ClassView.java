package ca.mcgill.sel.classdiagram.ui.views;

import org.eclipse.emf.common.notify.Notification;
import org.mt4j.util.MTColor;

import ca.mcgill.sel.classdiagram.Attribute;
import ca.mcgill.sel.classdiagram.CdmPackage;
import ca.mcgill.sel.classdiagram.Class;
import ca.mcgill.sel.classdiagram.Classifier;
import ca.mcgill.sel.classdiagram.LayoutElement;
import ca.mcgill.sel.classdiagram.Operation;
import ca.mcgill.sel.classdiagram.VisibilityType;
import ca.mcgill.sel.classdiagram.ui.views.handler.ClassDiagramHandlerFactory;
import ca.mcgill.sel.classdiagram.ui.views.handler.IClassViewHandler;
import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.perspective.COREPerspectiveUtil;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.perspective.PerspectiveControllerFactory;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.views.TextView;


/**
 * This view draws the representation of a Class onto a {@link ClassDiagramView}. It contains a name field, and
 * two lists for methods and attributes. ClassViews are inspectable and create a {@link ClassInspectorView} when click
 * on.
 *
 * @author vbonnet
 * @author eyildirim
 * @author mschoettle
 * @author yhattab
 */
public class ClassView extends ClassifierView<IClassViewHandler> {

    private Class cdmClass;

    /**
     * Whether not public operations should be displayed or not.
     */
    private boolean showAllOperations;

    private COREArtefact artefact;

    /**
     * Creates a new view representing the given class.
     *
     * @param structuralDiagram the {@link ClassDiagramView} that owns this view
     * @param clazz The RAM class to display.
     * @param layoutElement The position at which to display it.
     */
    public ClassView(ClassDiagramView structuralDiagram, Class clazz, LayoutElement layoutElement) {
        super(structuralDiagram, clazz, layoutElement);

        showAllOperations = false;
        
        // Add a listener to listen to any partiality change in the core metamodel
        artefact = COREArtefactUtil.getReferencingExternalArtefact(clazz);
        EMFEditUtil.addListenerFor(artefact, this);
    }

    /**
     * Adds an attribute view for the given attribute at the given index. The index corresponds to the position in the
     * container of attributes.
     *
     * @param index the index where to add the view at
     * @param attribute the attribute to add an attribute for
     */
    private void addAttributeField(int index, Attribute attribute) {
        AttributeView attributeView = new AttributeView(this, attribute);
        attributes.put(attribute, attributeView);

        attributesContainer.addChild(index, attributeView);
        attributeView.setHandler(ClassDiagramHandlerFactory.INSTANCE.getAttributeViewHandler());
    }

    /**
     * Returns the component containing all {@link AttributeView}s.
     *
     * @return the {@link RamRectangleComponent} containing all {@link AttributeView}s
     */
    public RamRectangleComponent getAttributesContainer() {
        return attributesContainer;
    }

    /**
     * Returns the class this view is visualizing.
     *
     * @return the ram {@link Class} this view is displaying.
     */
    public Class getRamClass() {
        return cdmClass;
    }

    @Override
    protected void initializeClass(Classifier classifier) {
        cdmClass = (Class) classifier;
        COREPerspective perspective = COREPerspectiveUtil.INSTANCE.getModelPerspective(classifier);
        if (perspective != null) {
            if (PerspectiveControllerFactory.INSTANCE.getActionValidator().canCreateAttribute(perspective)) {
                for (int i = 0; i < cdmClass.getAttributes().size(); i++) {
                    Attribute attr = cdmClass.getAttributes().get(i);
                    addAttributeField(i, attr);
                }
            }
        }

        initializeOperations(classifier);
        collapseOperations();
    }

    @Override
    public void notifyChanged(Notification notification) {
        // handle it first by the super class
        super.notifyChanged(notification);

        // this gets called for every ClassView (as there is only one item provider)
        // therefore an additional check is necessary
        if (notification.getNotifier() == cdmClass) {
            if (notification.getFeature() == CdmPackage.Literals.CLASSIFIER__ATTRIBUTES) {
                switch (notification.getEventType()) {
                    case Notification.REMOVE:
                        Attribute attribute = (Attribute) notification.getOldValue();
                        removeAttribute(attribute);
                        break;
                    case Notification.ADD:
                        attribute = (Attribute) notification.getNewValue();
                        addAttributeField(notification.getPosition(), attribute);
                        break;
                }
            }
        }
    }

    /**
     * Returns whether an operation should be displayed or not.
     * It will return true if
     * - showAllOperations is true OR
     * - it is public OR
     * - it is not mapped in an operation mapping OR
     * - it is not used in message views
     *
     * @param operation The operation to show or not
     *
     * @return whether operation should be shown or not
     */
    @Override
    protected boolean shouldShowOperation(Operation operation) {
        // TODO: Add the condition that an operation with a message view should be shown.
        return showAllOperations
                || operation.getVisibility() == VisibilityType.PUBLIC
                || !EMFModelUtil.referencedInFeature(operation, CorePackage.Literals.CORE_LINK__TO);
    }

    /**
     * Removes the view of the given attribute.
     *
     * @param attribute the {@link Attribute} to remove the view for
     */
    private void removeAttribute(final Attribute attribute) {
        if (attributes.containsKey(attribute)) {
            RamApp.getApplication().invokeLater(new Runnable() {

                @Override
                public void run() {
                    AttributeView attributeView = attributes.remove(attribute);

                    attributesContainer.removeChild(attributeView);
                    attributeView.destroy();
                }
            });
        }
    }

    /**
     * Sets color of the all attribute views to default attribute view color and setNoFill, setNoStroke to true.
     */
    public void setDefaultFillAndStrokeColorForAllAttributes() {
        for (AttributeView attView : attributes.values()) {
            attView.setFillColor(Colors.ATTRIBUTE_VIEW_DEFAULT_FILL_COLOR);
            attView.setStrokeColor(Colors.ATTRIBUTE_VIEW_DEFAULT_STROKE_COLOR);
            attView.setNoFill(true);
            attView.setNoStroke(true);
        }
    }

    /**
     * Sets color of the all operation views to default operation view color and setNoFill, setNoStroke to true.
     */
    public void setDefaultFillAndStrokeColorForAllOperations() {
        for (OperationView opView : operations.values()) {
            opView.setFillColor(Colors.OPERATION_VIEW_DEFAULT_FILL_COLOR);
            opView.setStrokeColor(Colors.OPERATION_VIEW_DEFAULT_STROKE_COLOR);
            opView.setNoFill(true);
            opView.setNoStroke(true);
        }
    }

    /**
     * Sets the fill and stroke color of the view representing the given {@link Attribute}.
     *
     * @param attributeToBeColored the {@link Attribute} whose view should be changed
     * @param colorFill the fill color to set
     * @param colorStroke the stroke color to set
     */
    public void setFillandStrokeColorOfAttribute(Attribute attributeToBeColored,
            MTColor colorFill, MTColor colorStroke) {
        AttributeView attributeView = attributes.get(attributeToBeColored);
        attributeView.setFillColor(colorFill);
        attributeView.setStrokeColor(colorStroke);

        attributeView.setNoFill(false);
        attributeView.setNoStroke(false);
    }

    /**
     * Sets the fill and stroke color of the given operation to the given colors.
     *
     * @param operationToBeColored OperationView of this operation will be colored.
     * @param colorFill fill color of the operation view.
     * @param colorStroke stroke color of the operation view.
     */
    public void setFillandStrokeColorOfOperation(Operation operationToBeColored,
            MTColor colorFill, MTColor colorStroke) {
        OperationView operationView = operations.get(operationToBeColored);
        operationView.setFillColor(colorFill);
        operationView.setStrokeColor(colorStroke);

        operationView.setNoFill(false);
        operationView.setNoStroke(false);
    }

    /**
     * Sets Fill color of the class view to default fill color.
     */
    public void setFillColorToDefault() {
        setFillColor(Colors.CLASS_VIEW_DEFAULT_FILL_COLOR);
    }

    /**
     * Displays the keyboard.
     */
    public void showKeyboard() {
        nameField.showKeyboard();
    }

    /**
     * Clear the name field of this view.
     *
     */
    public void clearNameField() {
        ((TextView) nameField).clearText();
    }

    /**
     * Returns the showAllOperations boolean attribute.
     * 
     * @return the showAllOperations boolean attribute
     */
    public boolean isShowingAllOperations() {
        return this.showAllOperations;
    }

    /**
     * Toggles the showAllOperations attribute.
     */
    public void toggleShowAllOperations() {
        showAllOperations = !showAllOperations;
        collapseOperations();
    }

    /**
     * Displays/Hide operations depending on the value of showAllOperations.
     */
    private void collapseOperations() {
        Classifier classifier = (Classifier) represented;
        if (showAllOperations) {
            for (int i = 0; i < classifier.getOperations().size(); i++) {
                Operation operation = classifier.getOperations().get(i);
                if (!this.containsChild(operations.get(operation))) {
                    operationsContainer.addChild(i, operations.get(operation));
                }
            }
        } else {
            for (Operation operation : classifier.getOperations()) {
                if (!shouldShowOperation(operation)) {
                    operationsContainer.removeChild(operations.get(operation));
                }
            }
        }
    }
    
    @Override
    public void destroy() {
        if (artefact != null) {
            EMFEditUtil.removeListenerFor(artefact, this);
        }
        super.destroy();
    }
}
