package ca.mcgill.sel.classdiagram.ui.views.handler.impl;

import java.util.Set;

import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;

import ca.mcgill.sel.classdiagram.CdmPackage;
import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.classdiagram.ImplementationClass;
import ca.mcgill.sel.classdiagram.ObjectType;
import ca.mcgill.sel.classdiagram.TypeParameter;
import ca.mcgill.sel.classdiagram.ui.utils.CdmModelUtils;
import ca.mcgill.sel.ram.loaders.RamClassLoader;
import ca.mcgill.sel.ram.loaders.exceptions.MissingJarException;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamPopup.PopupType;
import ca.mcgill.sel.ram.ui.components.RamSelectorComponent;
import ca.mcgill.sel.ram.ui.components.listeners.AbstractDefaultRamSelectorListener;
import ca.mcgill.sel.ram.ui.perspective.PerspectiveControllerFactory;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.SelectorView;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.impl.TextViewHandler;
import ca.mcgill.sel.ram.ui.views.structural.handler.ITypeParameterNameHandler;

/**
 * The handler for type parameter.
 * 
 * @author Franz
 * @author yhattab
 *
 */
public class TypeParameterNameHandler extends TextViewHandler implements ITypeParameterNameHandler {

    @Override
    public boolean shouldDismissKeyboard(TextView textView) {
        return false;
    }

    @Override
    public boolean processTapEvent(TapEvent tapEvent) {
        if (tapEvent.isDoubleTap()) {

            final TextView target = (TextView) tapEvent.getTarget();

            final TypeParameter typeParameter = (TypeParameter) target.getData();

            // if generic not set and not wildcard then show selector.

            if (typeParameter.getGenericType() == null
                    && !("?".equals(typeParameter.getName()))) {

                // create selector
                RamSelectorComponent<Object> selector = new SelectorView(target.getData(),
                        CdmPackage.Literals.TYPE_PARAMETER__GENERIC_TYPE);

                RamApp.getActiveScene().addComponent(selector, tapEvent.getLocationOnScreen());
                selector.registerListener(new AbstractDefaultRamSelectorListener<Object>() {
                    @Override
                    public void elementSelected(RamSelectorComponent<Object> selector, Object element) {
                        ImplementationClass classifier = (ImplementationClass) typeParameter.eContainer();
                        String className = classifier.getInstanceClassName();

                        try {
                            Set<String> superTypes = RamClassLoader.INSTANCE.getAllSuperClassesFor(className);
                            ClassDiagram classDiagram = (ClassDiagram) classifier.eContainer();
                            Set<ImplementationClass> subTypes =
                                    CdmModelUtils.getExistingSubTypesFor(classDiagram, className);
    
                            PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController().setTypeParameterType(
                                    classifier, typeParameter, (ObjectType) element, superTypes, subTypes);
                        } catch (MissingJarException e) {
                            RamApp.getActiveScene().displayPopup(Strings.popupClassNotFound(e.getMessage()),
                                    PopupType.ERROR);
                        }
                    }
                });
            }
        }

        return true;
    }

}
