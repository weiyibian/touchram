package ca.mcgill.sel.classdiagram.ui.views.handler.impl;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.classdiagram.Classifier;
import ca.mcgill.sel.classdiagram.Operation;
import ca.mcgill.sel.classdiagram.ui.utils.CdmModelUtils;
import ca.mcgill.sel.classdiagram.ui.views.OperationView;
import ca.mcgill.sel.classdiagram.ui.views.handler.IOperationViewHandler;
import ca.mcgill.sel.classdiagram.util.CdmModelUtil;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.menu.RamLinkedMenu;
import ca.mcgill.sel.ram.ui.perspective.PerspectiveControllerFactory;
import ca.mcgill.sel.ram.ui.perspective.classdiagram.CdmPerspectiveController;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.handler.BaseHandler;
import ca.mcgill.sel.ram.ui.views.handler.ILinkedMenuListener;


/**
 * The default handler for an {@link OperationView}.
 * 
 * @author mschoettle
 * @author yhattab
 */
public class OperationViewHandler extends BaseHandler implements IOperationViewHandler, ILinkedMenuListener {

    private static final String ACTION_OPERATION_REMOVE = "view.operation.remove";
    private static final String ACTION_PARAMETER_ADD = "view.parameter.add";
    private static final String ACTION_OPERATION_STATIC = "view.operation.static";
    private static final String ACTION_OPERATION_ABSTRACT = "view.operation.abstract";
    private static final String SUBMENU_CLASS_MORE = "sub.class.more";

    private static final String ACTION_OPERATION_SHIFT_UP = "view.operation.shiftUp";
    private static final String ACTION_OPERATION_SHIFT_DOWN = "view.operation.shiftDown";
    
    @Override
    public void createParameter(final OperationView operationView) {
        CdmModelUtils.createParameterEndOperation(operationView);
    }

    @Override
    public void removeOperation(OperationView operationView) {
        PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController()
            .removeOperation(operationView.getOperation());
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        String actionCommand = event.getActionCommand();
        RamRectangleComponent pressedButton = (RamRectangleComponent) event.getTarget();
        RamLinkedMenu linkedMenu = (RamLinkedMenu) pressedButton.getParentOfType(RamLinkedMenu.class);
        if (linkedMenu != null) {

            OperationView operationView = (OperationView) linkedMenu.getLinkedView();

            if (ACTION_OPERATION_REMOVE.equals(actionCommand)) {
                removeOperation(operationView);
            } else if (ACTION_PARAMETER_ADD.equals(actionCommand)) {
                createParameter(operationView);
            } else if (ACTION_OPERATION_STATIC.equals(actionCommand)) {
                setOperationStatic(operationView);
            } else if (ACTION_OPERATION_ABSTRACT.equals(actionCommand)) {
                switchToAbstract(operationView);
            } else if (ACTION_OPERATION_SHIFT_UP.equals(actionCommand)) {
                shiftOperationUp(operationView);
            } else if (ACTION_OPERATION_SHIFT_DOWN.equals(actionCommand)) {
                shiftOperationDown(operationView);
            }
        }
    }
    
    public void shiftOperationUp(OperationView operationView) {
        EList<Operation> operations = ((Classifier) operationView.getOperation().eContainer()).getOperations();
        int index = 0;
        
        for (int i = 0; i < operations.size(); i++) {
            if (operations.get(i).equals(operationView.getOperation())) {
                index = i;
            }
        }
        
        if (index > 0) {
            PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController()
                .setOperationPosition(operationView.getOperation(), index - 1);
        }
    }
    
    public void shiftOperationDown(OperationView operationView) {
        EList<Operation> operations = ((Classifier) operationView.getOperation().eContainer()).getOperations();
        int index = 0;
        
        for (int i = 0; i < operations.size(); i++) {
            if (operations.get(i).equals(operationView.getOperation())) {
                index = i;
            }
        }
        
        if (index < operations.size() - 1) {
            PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController()
                .setOperationPosition(operationView.getOperation(), index + 1);
        }
    }

    @Override
    public void setOperationStatic(OperationView operationView) {
        CdmPerspectiveController cdmController = PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController();
        cdmController.switchOperationStatic(operationView.getOperation());
    }

    @Override
    public void switchToAbstract(OperationView operationView) {
        CdmPerspectiveController cdmController = PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController();
        cdmController.switchOperationAbstract(operationView.getOperation());
    }

    @Override
    public EObject getEobject(RamRectangleComponent rectangle) {
        return ((OperationView) rectangle).getOperation();
    }

    @Override
    public void initMenu(RamLinkedMenu menu) {        
        menu.addAction(Strings.MENU_DELETE, Icons.ICON_MENU_TRASH, ACTION_OPERATION_REMOVE, this, true);
        OperationView operationView = (OperationView) menu.getLinkedView();
        if (operationView.isMutable()) {

            menu.addAction(Strings.MENU_PARAMETER_ADD, Icons.ICON_MENU_ADD_PARAMETER, ACTION_PARAMETER_ADD, this, true);

            // add the operation shift up and down buttons if there is more than 1 operation
            if (((Classifier) operationView.getOperation().eContainer()).getOperations().size() > 1) {
                menu.addAction(Strings.MENU_OPERATION_SHIFT_UP, 
                        Icons.ICON_MENU_ELEMENT_SHIFT_UP, ACTION_OPERATION_SHIFT_UP, this, true);
                menu.addAction(Strings.MENU_OPERATION_SHIFT_DOWN, 
                        Icons.ICON_MENU_ELEMENT_SHIFT_DOWN, ACTION_OPERATION_SHIFT_DOWN, this, true);
            }
            
            menu.addSubMenu(1, SUBMENU_CLASS_MORE);

            Operation operation = operationView.getOperation();
            if (!CdmModelUtil.isConstructorOrDestructor(operation)) {
                menu.addAction(Strings.MENU_STATIC, Strings.MENU_NO_STATIC, Icons.ICON_MENU_STATIC,
                        Icons.ICON_MENU_NOT_STATIC,
                        ACTION_OPERATION_STATIC, this, SUBMENU_CLASS_MORE, true, false);
            }

            menu.addAction(Strings.MENU_ABSTRACT, Strings.MENU_NOT_ABSTRACT, Icons.ICON_MENU_ABSTRACT,
                    Icons.ICON_MENU_NOT_ABSTRACT, ACTION_OPERATION_ABSTRACT, this, SUBMENU_CLASS_MORE, true,
                    false);
            menu.setLinkInCorners(false);
            updateButtons(menu);
        }

    }

    @Override
    public List<EObject> getEObjectToListenForUpdateMenu(RamRectangleComponent rectangle) {
        OperationView operationView = (OperationView) rectangle;
        List<EObject> ret = new ArrayList<EObject>();
        ret.add(operationView.getOperation());
        ret.add(COREArtefactUtil.getReferencingExternalArtefact(operationView.getOperation()));
        return ret;
    }

    @Override
    public void updateMenu(RamLinkedMenu menu, Notification notification) {
        if (notification.getEventType() == Notification.SET
                || notification.getEventType() == Notification.UNSET
                || notification.getEventType() == Notification.ADD
                || notification.getEventType() == Notification.REMOVE) {
            updateButtons(menu);
        }
    }

    /**
     * Updates buttons inside the menu.
     * 
     * @param menu - the menu which contains buttons.
     */
    private static void updateButtons(RamLinkedMenu menu) {
        Operation operation = (Operation) menu.geteObject();

        menu.toggleAction(operation.isStatic(), ACTION_OPERATION_STATIC);
        menu.toggleAction(operation.isAbstract(), ACTION_OPERATION_ABSTRACT);
    }

    @Override
    public RamRectangleComponent getVisualLinkedComponent(RamRectangleComponent link) {
        OperationView view = (OperationView) link;
        return view.getNameField();
    }

}
