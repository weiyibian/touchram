package ca.mcgill.sel.classdiagram.ui.views.handler.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.classdiagram.Association;
import ca.mcgill.sel.classdiagram.CdmPackage;
import ca.mcgill.sel.classdiagram.Class;
import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.classdiagram.ui.views.BaseView;
import ca.mcgill.sel.classdiagram.ui.views.ClassDiagramView;
import ca.mcgill.sel.classdiagram.ui.views.ClassView;
import ca.mcgill.sel.classdiagram.ui.views.NaryAssociationNodeView;
import ca.mcgill.sel.classdiagram.ui.views.handler.INaryAssociationNodeViewHandler;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.perspective.COREPerspectiveUtil;
import ca.mcgill.sel.core.perspective.PerspectiveName;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.menu.RamLinkedMenu;
import ca.mcgill.sel.ram.ui.perspective.PerspectiveControllerFactory;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.handler.ILinkedMenuListener;


/**
 * The default handler for a {@link NaryAssociationNodeView}.
 * 
 * @author yhattab
 */
public class NaryAssociationNodeViewHandler extends BaseViewHandler implements INaryAssociationNodeViewHandler, 
    ILinkedMenuListener {

    private static final String ACTION_ASSOCIATION_CLASS_TOGGLE = "view.association.class.toggle";
    
    @Override
    public void actionPerformed(ActionEvent event) {
        String actionCommand = event.getActionCommand();
        RamRectangleComponent pressedButton = (RamRectangleComponent) event.getTarget();
        RamLinkedMenu linkedMenu = (RamLinkedMenu) pressedButton.getParentOfType(RamLinkedMenu.class);
        if (linkedMenu != null) {

            NaryAssociationNodeView associationView = (NaryAssociationNodeView) linkedMenu.getLinkedView();

            if (ACTION_ASSOCIATION_CLASS_TOGGLE.equals(actionCommand)) {
                final Association association = (Association) associationView.getRepresented();
                if (association.getAssociationClass() == null) {
                    createAssociationClass(associationView);
                    linkedMenu.toggleAction(false, ACTION_ASSOCIATION_CLASS_TOGGLE);
                } else {
                    removeAssociationClass(associationView);
                    linkedMenu.toggleAction(true, ACTION_ASSOCIATION_CLASS_TOGGLE);
                }
            }
        }
        super.actionPerformed(event);
    }
    
    @Override
    public boolean processTapAndHoldEvent(TapAndHoldEvent tapAndHoldEvent) {
        if (tapAndHoldEvent.isHoldComplete()) {
            BaseView<?> target = (BaseView<?>) tapAndHoldEvent.getTarget();
            return target.getClassDiagramView().getHandler()
                    .handleTapAndHoldOnClass(target.getClassDiagramView(), target);
        }
        return false;
    }

    @Override
    public boolean processTapEvent(TapEvent tapEvent) {
        // Always flip the selection of the view.
        if (tapEvent.isTapped() || tapEvent.isDoubleTap()) {
            BaseView<?> target = (BaseView<?>) tapEvent.getTarget();
            target.setSelect(!target.getIsSelected());

            // Pass it to the structural view handler to handle more advanced gestures.
            if (tapEvent.isDoubleTap()) {
                ClassDiagramView structuralDiagramView = target.getClassDiagramView();
                return structuralDiagramView.getHandler().handleDoubleTapOnClass(structuralDiagramView, target);
            }
        }

        return false;
    }

    @Override
    public void initMenu(RamLinkedMenu menu) {
        super.initMenu(menu);
        Association association = (Association) menu.geteObject();
        menu.addAction(Strings.MENU_ASSOCIATION_CLASS_REMOVE, Strings.MENU_ASSOCIATION_CLASS_ADD, 
                Icons.ICON_MENU_REMOVE_ASSOCIATION_CLASS, Icons.ICON_MENU_ADD_ASSOCIATION_CLASS,
                ACTION_ASSOCIATION_CLASS_TOGGLE, this, SUBMENU_ADD, true, association.getAssociationClass() == null);
        
    }

    @Override
    public void createAssociationClass(NaryAssociationNodeView associationView) {
        ClassDiagramView view = associationView.getClassDiagramView();
        final ClassDiagram cd = view.getClassDiagram();
        final Association association = (Association) associationView.getRepresented();
        
        cd.eAdapters().add(new EContentAdapter() {

            private Class associationClass;

            @Override
            public void notifyChanged(Notification notification) {
                if (notification.getFeature() == CdmPackage.Literals.CLASS_DIAGRAM__CLASSES) {
                    if (notification.getEventType() == Notification.ADD) {
                        associationClass = (Class) notification.getNewValue();
                    }
                } else if (notification.getFeature() == CdmPackage.Literals.CONTAINER_MAP__VALUE) {
                    if (notification.getEventType() == Notification.ADD) {
                        ((ClassView) view.getClassViewOf(associationClass)).showKeyboard();
                        ((ClassView) view.getClassViewOf(associationClass)).clearNameField();
                        
                        PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController().setAssociationClass(
                                association, associationClass);
                        cd.eAdapters().remove(this);
                    }
                }
            }
        });
       
        String associationClassName = association.getName() + "_Association_Class";
        Vector3D newClassPosition = associationView.getAssociationClassCreationPosition();
        COREPerspective perspective = COREPerspectiveUtil.INSTANCE.getModelPerspective(cd);
        switch (perspective.getName()) {
            case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE: 
                PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController().createNewClass(cd,
                        associationClassName, false, false, 
                        newClassPosition.getX(), newClassPosition.getY());
                break;
            case PerspectiveName.DESIGN_MODEL_PERSPECTIVE: 
                PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController().createNewClass(cd,
                        associationClassName, false, false, 
                        newClassPosition.getX(), newClassPosition.getY());
                break;
        } 
    }

    @Override
    public void removeAssociationClass(NaryAssociationNodeView associationView) {
        final Association association = (Association) associationView.getRepresented(); 
        PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController().removeAssociationClass(association);
    }

    @Override
    public void removeRepresented(BaseView<?> baseView) {
        Association association = (Association) baseView.getRepresented();
        COREPerspective perspective = COREPerspectiveUtil.INSTANCE.getModelPerspective(association);
        switch (perspective.getName()) {
            case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE: 
                PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController().deleteAssociation(
                        EMFModelUtil.getRootContainerOfType(
                                association, CdmPackage.Literals.CLASS_DIAGRAM), association);
                break;
            case PerspectiveName.DESIGN_MODEL_PERSPECTIVE: 
                PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController().deleteAssociation(
                        EMFModelUtil.getRootContainerOfType(
                                association, CdmPackage.Literals.CLASS_DIAGRAM), association);
                break;
        } 

    }

}
