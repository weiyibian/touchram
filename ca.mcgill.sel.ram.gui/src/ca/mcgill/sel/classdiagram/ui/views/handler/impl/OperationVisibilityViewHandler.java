package ca.mcgill.sel.classdiagram.ui.views.handler.impl;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import ca.mcgill.sel.classdiagram.Classifier;
import ca.mcgill.sel.classdiagram.Operation;
import ca.mcgill.sel.classdiagram.VisibilityType;
import ca.mcgill.sel.ram.ui.perspective.PerspectiveControllerFactory;
import ca.mcgill.sel.ram.ui.views.handler.impl.TextViewHandler;

/**
 * The default handler for a {@link OperationVisibilityViewHandler}. It shows a selector whith the different 
 * visibilities as defined in {@link VisibilityType}
 * 
 * @author oalam
 * @author yhattab
 * */

public class OperationVisibilityViewHandler extends TextViewHandler {
    
    /**
     * Sets the value on the data object for the given feature.
     * 
     * @param data the object containing the feature
     * @param feature the feature of which the value should be changed
     * @param value the new value of the feature
     */
    @Override
    protected void setValue(EObject data, EStructuralFeature feature, Object value) {
        Operation op = (Operation) data;
        VisibilityType visibility = (VisibilityType) value;
        PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController().changeOperationAndClassVisibility(
                (Classifier) op.eContainer(), op, visibility);
    }
    
}
