package ca.mcgill.sel.classdiagram.ui.views;

import org.eclipse.emf.ecore.EObject;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.ram.ui.utils.Colors;

/**
 * Represents a relationship between a {@link Note} and a class.
 * 
 * @author yhattab
 */
public class AnnotationView extends RelationshipView<EObject, BaseView<?>> {
    
    /**
     * Distance from the end of the note to the center, the point where the next line is drawn (orthogonal).
     */
    private NoteView noteView;
    private BaseView<?> annotatedElementView;
    
    /**
     * Creates a new {@link AnnotationView} for the given note and class.
     * 
     * @param noteView
     *            the {@link NoteView} for the note
     * @param annotatedElementView
     *            the {@link BaseView} for the annotated element
     */
    public AnnotationView(NoteView noteView, BaseView<?> annotatedElementView) {
        super(noteView.getNote(), noteView, annotatedElementView.getRepresented(), annotatedElementView);
        setLineStyle(LineStyle.DOTTED);
        this.drawColor = Colors.ANNOTATION_STROKE_COLOR;
        
        
        this.noteView = noteView;
        this.annotatedElementView = annotatedElementView;
    }
    
    /**
     * Draws the line between the center of the note and the center of the annotated element (not using the CDEnds).
     */
    @Override
    protected void drawAllLines() {
        //
        Vector3D noteCenter = noteView.getCenterPointRelativeToParent();
        Vector3D elementCenter = annotatedElementView.getCenterPointRelativeToParent();
        
        if (noteView.isVisible()) {
            drawLine(noteCenter.getX(), noteCenter.getY(), null, elementCenter.getX(), elementCenter.getY());
        }
        
        //force linked views to redraw, avoiding line appearing on top of them
        noteView.sendToFront();
        annotatedElementView.sendToFront();
        
    }
      
    /**
     * Returns the view of the annotated element.
     * 
     * @return the {@link BaseView} of the annotated element
     */
    public BaseView<?> getAnnotatedElementView() {
        return annotatedElementView;
    }
    
    /**
     * Returns the view of the note.
     * 
     * @return the {@link NoteView} of the annotation origin
     */
    public NoteView getNoteView() {
        return noteView;
    }

    @Override
    public void update() {
        drawAllLines();
    }
}
