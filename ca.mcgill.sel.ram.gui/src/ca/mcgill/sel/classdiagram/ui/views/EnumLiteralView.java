package ca.mcgill.sel.classdiagram.ui.views;

import org.mt4j.util.font.IFont;

import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.util.COREModelUtil;
import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.classdiagram.CDEnumLiteral;
import ca.mcgill.sel.classdiagram.CdmPackage;
import ca.mcgill.sel.classdiagram.ui.views.handler.ClassDiagramHandlerFactory;
import ca.mcgill.sel.classdiagram.ui.views.handler.IEnumLiteralViewHandler;

import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamSpacerComponent;
import ca.mcgill.sel.ram.ui.layouts.HorizontalLayoutVerticallyCentered;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Fonts;
import ca.mcgill.sel.ram.ui.utils.GraphicalUpdater;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.IHandled;


/**
 * This view draws the representation of a CDEnumLiteral onto a {@link EnumView}. It only contains a name field.
 * 
 * @author Franz
 * @author yhattab
 * 
 */
public class EnumLiteralView extends RamRectangleComponent implements 
        IHandled<IEnumLiteralViewHandler> {

    private static final int LITERAL_FRONT_SPACE = 10;

    /**
     * The {@link GraphicalUpdater} for this view.
     */
    protected GraphicalUpdater graphicalUpdater;

    private TextView nameField;

    private CDEnumLiteral literal;

    private boolean isMutable;

    private IEnumLiteralViewHandler handler;

    /**
     * Constructor. Builds the enum literal view.
     * 
     * @param enumView {@link EnumView} the owner of this literal
     * @param literal {@link REnumLiteral} the model of this view
     * @param mutable true if the enum literal can be modified.
     */
    public EnumLiteralView(EnumView enumView, CDEnumLiteral literal, boolean mutable) {
        this.isMutable = mutable;
        this.literal = literal;
        addChild(new RamSpacerComponent(LITERAL_FRONT_SPACE, Fonts.FONT_SIZE));

        // Create name field for the literal
        //nameField = new TextView(literal, CorePackage.Literals.CORE_NAMED_ELEMENT__NAME);
        nameField = new TextView(literal, CdmPackage.Literals.NAMED_ELEMENT__NAME);
        nameField.setBufferSize(Cardinal.SOUTH, 0);
        nameField.setBufferSize(Cardinal.WEST, 0);
        nameField.setUniqueName(true);
        
        updateStyle();
        
        addChild(nameField);
        if (isMutable) {
            nameField.setHandler(ClassDiagramHandlerFactory.INSTANCE.getEnumLiteralNameHandler());
        }
        // set layout
        setLayout(new HorizontalLayoutVerticallyCentered(0));

        ClassDiagram cd = EMFModelUtil.getRootContainerOfType(literal, CdmPackage.Literals.CLASS_DIAGRAM);
        graphicalUpdater = RamApp.getApplication().getGraphicalUpdaterForModel(cd);
        graphicalUpdater.addGUListener(literal, this);
    }

    /**
     * Getter for {@link REnumLiteral} associated to this view.
     * 
     * @return {@link REnumLiteral} associated to this view.
     */
    public CDEnumLiteral getLiteral() {
        return literal;
    }

    @Override
    public void setHandler(IEnumLiteralViewHandler handler) {
        this.handler = handler;
    }

    @Override
    public IEnumLiteralViewHandler getHandler() {
        return handler;
    }

    /**
     * Returns the name field view.
     * 
     * @return the name field view
     */
    public TextView getNameField() {
        return nameField;
    }

    /**
     * Updates the style of this view depending on whether the attribute is a reference or not.
     */
    @Override
    protected void updateStyle() {
        IFont font;
        
        if (COREModelUtil.isReference(literal)) {
            font = Fonts.REFERENCE_FONT;
            setNoFill(false);
            setFillColor(Colors.ENUM_LITERAL_VIEW_REFERENCE_FILL_COLOR);
        } else {
            font = Fonts.DEFAULT_FONT;
            setNoFill(false);
            setFillColor(Colors.ENUM_LITERAL_VIEW_DEFAULT_FILL_COLOR);
        }
        
        nameField.setFont(font);
    }

}
