package ca.mcgill.sel.classdiagram.ui.views.wrappers;

import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.List;

import ca.mcgill.sel.classdiagram.ImplementationClass;
import ca.mcgill.sel.classdiagram.ui.utils.CdmModelUtils;

/**
 * An abstract callable member wrapper for Java callable members.
 * A callable member can be, for example, a method or a constructor.
 * 
 * @author Franz Garcia
 * @author yhattab
 */
public abstract class CallableMemberWrapper {
    
    /**
     * The default parameter name prefix.
     */
    public static final String DEFAULT_PARAM_NAME = "arg";
    
    /**
     * The owner of the callable member within the model.
     */
    protected ImplementationClass owner;
    
    /**
     * The modifiers of the callable member.
     */
    protected int modifier;
    
    /**
     * The name of the callable member.
     */
    protected String name;
    
    /**
     * The list of parameter types.
     */
    protected List<Type> parameters;
    
    /**
     * Creates a new callable member wrapper.
     * 
     * @param owner the {@link ImplementationClass} in the model this member belongs to
     * @param modifier the modifiers
     * @param name the name
     * @param parameters the list of parameter types
     */
    public CallableMemberWrapper(ImplementationClass owner, int modifier, String name, List<Type> parameters) {
        this.owner = owner;
        this.modifier = modifier;
        this.name = name;
        this.parameters = parameters;
    }
    
    /**
     * Getter for the owner of this constructor.
     * 
     * @return Implementation class owner of this method.
     */
    public ImplementationClass getOwner() {
        return owner;
    }
    
    /**
     * Getter for visibility.
     * 
     * @return String representing the visibility.
     */
    public String getVisibility() {
        String v = "public";
        if (Modifier.isProtected(this.modifier)) {
            v = "protected";
        } else if (Modifier.isPrivate(this.modifier)) {
            v = "private";
        }
        return v;
    }
    
    /**
     * Getter for the name of the method.
     * 
     * @return name of the method
     */
    public String getName() {
        return this.name;
    }
    
    /**
     * Getter for the parameters.
     * 
     * @return String with all the parameters
     */
    public String getParametersAsString() {
        int counter = 0;
        int numParam = this.parameters.size();
        String params = "";
        for (java.lang.reflect.Type someType : this.parameters) {
            String paramType = CdmModelUtils.getNameOfType(someType, owner);
            String parameterName = DEFAULT_PARAM_NAME + counter;
            params += paramType + " " + parameterName;
            if (counter != numParam - 1) {
                params += ", ";
            }
            counter++;
        }
        return params;
    }
    
    /**
     * Returns the list of parameter types of this callable member.
     * 
     * @return the list of parameter types
     */
    public List<Type> getParameters() {
        return this.parameters;
    }
    
    @Override
    public String toString() {
        return getStringRepresentation();
    }
    
    /**
     * Returns the string representation of this callable member.
     * 
     * @return the string reperesentation
     */
    protected abstract String getStringRepresentation();
    
}
