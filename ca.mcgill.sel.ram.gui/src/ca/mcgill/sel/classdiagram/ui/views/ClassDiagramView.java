package ca.mcgill.sel.classdiagram.ui.views;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.edit.provider.INotifyChangedListener;
import org.mt4j.components.TransformSpace;
import org.mt4j.input.gestureAction.TapAndHoldVisualizer;
import org.mt4j.input.inputProcessors.IGestureEventListener;
import org.mt4j.input.inputProcessors.componentProcessors.panProcessor.PanProcessorTwoFingers;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldProcessor;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapProcessor;
import org.mt4j.input.inputProcessors.componentProcessors.unistrokeProcessor.UnistrokeUtils.Direction;
import org.mt4j.input.inputProcessors.componentProcessors.unistrokeProcessor.UnistrokeUtils.UnistrokeGesture;
import org.mt4j.input.inputProcessors.componentProcessors.zoomProcessor.ZoomProcessor;
import org.mt4j.util.MTColor;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.classdiagram.Association;
import ca.mcgill.sel.classdiagram.AssociationEnd;
import ca.mcgill.sel.classdiagram.CDEnum;
import ca.mcgill.sel.classdiagram.CdmFactory;
import ca.mcgill.sel.classdiagram.CdmPackage;
import ca.mcgill.sel.classdiagram.Class;
import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.classdiagram.Classifier;
import ca.mcgill.sel.classdiagram.ImplementationClass;
import ca.mcgill.sel.classdiagram.LayoutElement;
import ca.mcgill.sel.classdiagram.NamedElement;
import ca.mcgill.sel.classdiagram.Note;
import ca.mcgill.sel.classdiagram.Type;
import ca.mcgill.sel.classdiagram.impl.ContainerMapImpl;
import ca.mcgill.sel.classdiagram.impl.ElementMapImpl;
import ca.mcgill.sel.classdiagram.ui.views.handler.ClassDiagramHandlerFactory;
import ca.mcgill.sel.classdiagram.ui.views.handler.IClassDiagramViewHandler;
import ca.mcgill.sel.classdiagram.ui.views.handler.INaryAssociationNodeViewHandler;
import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.perspective.PerspectiveName;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.navigationbar.NavigationBar;
import ca.mcgill.sel.ram.ui.events.MouseWheelProcessor;
import ca.mcgill.sel.ram.ui.events.RightClickDragProcessor;
import ca.mcgill.sel.ram.ui.events.UnistrokeProcessorLeftClick;
import ca.mcgill.sel.ram.ui.perspective.PerspectiveControllerFactory;
import ca.mcgill.sel.ram.ui.utils.GUIConstants;
import ca.mcgill.sel.ram.ui.views.AbstractView;

/**
 * The visual representation for a class diagram.
 * 
 * @author vbonnet
 * @author mschoettle
 * @author yhattab
 */
public class ClassDiagramView extends AbstractView<IClassDiagramViewHandler>
        implements INotifyChangedListener {

    private static final int DISTANCE_BETWEEN_CLASSES = 200;

    private Map<Classifier, ClassifierView<?>> classToViewMap;
    private Map<CDEnum, EnumView> enumToViewMap;
    private Map<Association, AssociationView> associationToViewMap;
    private Map<Note, NoteView> noteToViewMap;
    private Map<Association, NaryAssociationNodeView> naryAssociationToViewMap;
    private List<AnnotationView> annotationViewList;
    private List<InheritanceView> inheritanceViewList;
    private Set<BaseView<?>> selectedElements;

    private ContainerMapImpl layout;
    private ClassDiagram cd;

    /**
     * Creates a new ClassDiagramView for the given class diagram. For every class inside the class diagram its own
     * view is created as well as for every association. IMPORTANT: All the view elements related to the
     * ClassDiagramView are added to the containerLayer.
     * 
     * @param classdiagram the class diagram
     * @param layout some layout.
     * @param width The width over which the elements can be displayed
     * @param height The height over which the elements can be displayed.
     */
    public ClassDiagramView(ClassDiagram classdiagram, ContainerMapImpl layout, float width, float height) {
        super(width, height);

        this.layout = layout;
        this.cd = classdiagram;

        associationToViewMap = new HashMap<Association, AssociationView>();
        naryAssociationToViewMap = new HashMap<Association, NaryAssociationNodeView>();
        // maintain a map from class to the associated view since this is needed for creating associations
        classToViewMap = new HashMap<Classifier, ClassifierView<?>>();
        enumToViewMap = new HashMap<CDEnum, EnumView>();
        noteToViewMap = new HashMap<Note, NoteView>();
        annotationViewList = new ArrayList<AnnotationView>();
        inheritanceViewList = new ArrayList<InheritanceView>();

        buildAndLayout(width, height);

        EMFEditUtil.addListenerFor(classdiagram, this);
        // register to the ContainerMap to receive adds/removes of ElementMaps
        EMFEditUtil.addListenerFor(this.layout, this);

        // create the list for storing selected classifiers
        selectedElements = new HashSet<BaseView<?>>();
    }

    /**
     * Builds this view and layouts all classes within the dimensions given.
     * 
     * @param width the width of this view
     * @param height the height of this view
     */
    private void buildAndLayout(float width, float height) {
        // Collect all classifiers to display first.
        List<Classifier> classifiers = new ArrayList<Classifier>();

        for (Classifier clazz : cd.getClasses()) {
            classifiers.add(clazz);
        }

        for (Type type : cd.getTypes()) {
            if (type instanceof CDEnum) {
                classifiers.add((CDEnum) type);
            }
        }

        // Now add them to the structural view and layout the ones that don't have a layout yet.
        Vector3D position = new Vector3D(100, 100);
        int maxHeight = 0;

        float maxX = width;
        float maxY = height;
        for (Classifier classifier : classifiers) {
            LayoutElement layoutElement = layout == null ? null : layout.getValue().get(classifier);
            BaseView<?> classifierView = null;

            // Enums need to be tested before ImplementationClass,
            // because Enums are PrimitiveType, which is an ImplementationClass.
            if (classifier instanceof Class) {
                addClass((Class) classifier, layoutElement);
                classifierView = classToViewMap.get(classifier);
            } else if (classifier instanceof CDEnum) {
                CDEnum enumType = (CDEnum) classifier;
                addEnum(enumType, layoutElement, enumType.getInstanceClassName() != null);
                classifierView = enumToViewMap.get(enumType);
            } else if (classifier instanceof ImplementationClass) {
                addImplementationClass((ImplementationClass) classifier, layoutElement);
                classifierView = classToViewMap.get(classifier);
            }

            if (layoutElement == null) {

                layoutElement = CdmFactory.eINSTANCE.createLayoutElement();
                this.layout.getValue().put(classifier, layoutElement);

                // TODO: move to separate method or class (e.g. a layouter)
                // Move the class so they will be created next to each other.
                position.setX(position.getX() + DISTANCE_BETWEEN_CLASSES);
                // Really weird fix (3 * width) but it allows (for now) to have a wider class diagram after weaving.
                if (position.getX() + classifierView.getWidth() >= 3 * width) {
                    position.setX(DISTANCE_BETWEEN_CLASSES);
                    position.setY(position.getY() + maxHeight + DISTANCE_BETWEEN_CLASSES);
                    maxHeight = 0;
                }

                layoutElement.setX(position.getX());
                layoutElement.setY(position.getY());
                classifierView.setLayoutElement(layoutElement);

                maxHeight = Math.max(maxHeight, (int) classifierView.getHeightXY(TransformSpace.LOCAL));
                position.setX(position.getX() + classifierView.getWidthXY(TransformSpace.RELATIVE_TO_PARENT));
            } else {
                position.setX(Math.max(position.getX(), layoutElement.getX() + classifierView.getWidth()));
                position.setY(Math.max(position.getY(), layoutElement.getY() + classifierView.getHeight()));
            }

            maxX = Math.max(maxX, classifierView.getX() + classifierView.getWidth());
            maxY = Math.max(maxY, classifierView.getY() + classifierView.getHeight());
        }

        float scale = Math.min(width / maxX, height / maxY);
        scaleGlobal(scale, scale, 1f, new Vector3D(0, 0, 0));

        for (Note note : cd.getNotes()) {
            LayoutElement layoutElement = layout == null ? null : layout.getValue().get(note);
            addNote(note, layoutElement);
            NoteView noteView = noteToViewMap.get(note);

            position.setX(Math.max(position.getX(), layoutElement.getX() + noteView.getWidth()));
            position.setY(Math.max(position.getY(), layoutElement.getY() + noteView.getHeight()));

            maxX = Math.max(maxX, noteView.getX() + noteView.getWidth());
            maxY = Math.max(maxY, noteView.getY() + noteView.getHeight());
        }

        for (Association association : cd.getAssociations()) {
            addAssociationViews(association);
        }

        // Create inheritance links.
        // This can't be done above when iterating through all the classes
        // since a corresponding ClassView might not exist yet.
        for (Classifier classifier : cd.getClasses()) {

            for (Classifier superClass : classifier.getSuperTypes()) {
                addInheritanceView(classifier, superClass);
            }
        }

        // Create annotation links.
        for (Note note : cd.getNotes()) {
            for (NamedElement annotatedElement : note.getNotedElement()) {
                addAnnotationView(note, annotatedElement);
            }
        }
    }

    /**
     * Adds {@link AssociationView}s for the given association to this view.
     * One association is created between the two classifiers of the association,
     * or from the Nary node to all the classifiers accordingly.
     * 
     * @param association the association to add a view for
     */
    private void addAssociationViews(Association association) {
        // check if it's a normal or an nary association
        if (association.getEnds().size() <= 2 && association.getAssociationClass() == null) {
            // normal association creation
            AssociationEnd toEnd = association.getEnds().get(0);
            AssociationEnd fromEnd = association.getEnds().get(1);
            ClassifierView<?> fromEndClassifierView = classToViewMap.get(fromEnd.getClassifier());
            ClassifierView<?> toEndClassifierView = classToViewMap.get(toEnd.getClassifier());

            AssociationView view = new AssociationView(association, fromEnd, fromEndClassifierView,
                    toEnd, toEndClassifierView);
            view.setHandler(ClassDiagramHandlerFactory.INSTANCE.getAssociationViewHandler());
            view.setBackgroundColor(this.getFillColor());
            associationToViewMap.put(association, view);

            view.updateLines();
            addChild(view);
        } else {
            // Nary Association
            if (!naryAssociationToViewMap.containsKey(association)) {
                addNaryAssociationNodeView(association, layout.getValue().get(association));
            }
            NaryAssociationNodeView fromNaryNode = naryAssociationToViewMap.get(association);
            for (AssociationEnd toEnd : association.getEnds()) {
                ClassifierView<?> toEndClassifierView = classToViewMap.get(toEnd.getClassifier());

                NaryAssociationView view = new NaryAssociationView(association, fromNaryNode,
                        toEnd, toEndClassifierView, false);
                view.setBackgroundColor(this.getFillColor());

                fromNaryNode.addAssociationView(view);

                view.updateLines();
                addChild(view);
            }
            if (association.getAssociationClass() != null) {
                ClassifierView<?> associationClassView = classToViewMap.get(association.getAssociationClass());

                if (associationClassView != null) {
                    NaryAssociationView view = new NaryAssociationView(association, fromNaryNode,
                            association.getAssociationClass(), associationClassView, true);
                    view.setBackgroundColor(this.getFillColor());

                    fromNaryNode.setAssociationClassLink(view);

                    view.updateLines();
                    addChild(view);
                }
            }

        }
    }

    /**
     * Adds a new {@link ClassView} for the given class to this view.
     * 
     * @param clazz the class to add a view for
     * @param layoutElement the layout element containing layout information for the class
     */
    private void addClass(Class clazz, LayoutElement layoutElement) {
        // Create ClassView for standard classes and DataTypeView for datatypes.
        ClassView classView = null;

        if (clazz.isDataType()) {
            classView = new DataTypeView(this, clazz, layoutElement);
        } else {
            classView = new ClassView(this, clazz, layoutElement);
        }

        classToViewMap.put(clazz, classView);

        addChild(classView);

        classView.setHandler(ClassDiagramHandlerFactory.INSTANCE.getClassViewHandler());
    }

    /**
     * Add an enum to structural diagram view.
     * 
     * @param renum that we want to add
     * @param layoutElement of the enum view.
     * @param isImplementationEnum is it an implementation enum?
     */
    private void addEnum(CDEnum renum, LayoutElement layoutElement, boolean isImplementationEnum) {
        // this view is registered as observer on the class (it is done in the class' constructor)
        EnumView enumView = new EnumView(this, renum, layoutElement, isImplementationEnum);

        enumToViewMap.put(renum, enumView);

        addChild(enumView);

        enumView.setHandler(ClassDiagramHandlerFactory.INSTANCE.getEnumViewHandler());
    }

    /**
     * Add a note to the classs diagram view.
     * 
     * @param note that we want to add
     * @param layoutElement of the enum view.
     */
    private void addNote(Note note, LayoutElement layoutElement) {
        // this view is registered as observer on the class (it is done in the class' constructor)
        NoteView noteView = new NoteView(this, note, layoutElement);

        noteToViewMap.put(note, noteView);

        addChild(noteView);

        noteView.setHandler(ClassDiagramHandlerFactory.INSTANCE.getNoteViewHandler());
    }

    /**
     * Adds a new implementation class.
     * 
     * @param implementationClass to be added.
     * @param layoutElement of the ImplementationClassView
     */
    private void addImplementationClass(ImplementationClass implementationClass, LayoutElement layoutElement) {
        ImplementationClassView implementationClassView = new ImplementationClassView(this, implementationClass,
                layoutElement);
        classToViewMap.put(implementationClass, implementationClassView);

        addChild(implementationClassView);

        implementationClassView.setHandler(ClassDiagramHandlerFactory.INSTANCE.getImplementationClassViewHandler());
    }

    /**
     * Adds a link between association class and an nary association.
     * 
     * @param naryNodeView the view representing the association to which an association class is to be added.
     * @param associationClass the association class that is being added.
     */
    public void addAssociationClass(NaryAssociationNodeView naryNodeView, final Class associationClass) {
        final Association association = naryNodeView.getAssociation();
        ClassifierView<?> associationClassView =
                classToViewMap.get(associationClass);

        NaryAssociationView view = new NaryAssociationView(association, naryNodeView,
                associationClass, associationClassView, true);
        view.setBackgroundColor(this.getFillColor());

        view.updateLines();
        naryNodeView.setAssociationClassLink(view);
        addChild(view);
    }

    /**
     * Adds an {@link NaryAssociationNodeView} for an association and creates appropriate links to it.
     * 
     * @param association {@link Association} being represented by the view
     * @param layoutElement of the Association node {@link NaryAssociationNodeView}
     */
    public void addNaryAssociationNodeView(Association association, LayoutElement layoutElement) {
        NaryAssociationNodeView naryAssocView = new NaryAssociationNodeView(this, association, layoutElement);
        naryAssociationToViewMap.put(association, naryAssocView);

        addChild(naryAssocView);

        INaryAssociationNodeViewHandler naryHandler =
                ClassDiagramHandlerFactory.INSTANCE.getNaryAssociationNodeViewHandler();
        naryAssocView.setHandler(naryHandler);

    }

    /**
     * Adds an {@link InheritanceView} between the given super and sub class.
     * 
     * @param derivedClass the sub class
     * @param superClass the super class
     */
    public void addInheritanceView(Classifier derivedClass, Classifier superClass) {
        InheritanceView inheritanceView = new InheritanceView(classToViewMap.get(derivedClass),
                classToViewMap.get(superClass));
        // Interfaces should be handled differently.
        if (superClass instanceof ImplementationClass && ((ImplementationClass) superClass).isInterface()) {
            inheritanceView.setImplementsInheritance(true);
        }
        // Make sure that inheritance between implementation classes cannot be removed by the user.
        if (!(derivedClass instanceof ImplementationClass)) {
            inheritanceView.setHandler(ClassDiagramHandlerFactory.INSTANCE.getInheritanceViewHandler());
        }
        inheritanceViewList.add(inheritanceView);

        inheritanceView.updateLines();
        addChild(inheritanceView);
    }

    /**
     * Adds an {@link AnnotationView} between the given note and class.
     * 
     * @param note the note
     * @param annotatedElement the annotated class
     */
    public void addAnnotationView(Note note, NamedElement annotatedElement) {
        BaseView<?> annotatedElementView = null;
        NoteView noteView = noteToViewMap.get(note);

        if (classToViewMap.containsKey(annotatedElement)) {
            annotatedElementView = classToViewMap.get(annotatedElement);
        } else if (enumToViewMap.containsKey(annotatedElement)) {
            annotatedElementView = enumToViewMap.get(annotatedElement);
        } else if (naryAssociationToViewMap.containsKey(annotatedElement)) {
            annotatedElementView = naryAssociationToViewMap.get(annotatedElement);
        } else {
            return;
        }

        AnnotationView annotationView = new AnnotationView(noteView, annotatedElementView);

        annotationViewList.add(annotationView);

        addChild(annotationView);
        annotationView.updateLines();
    }

    /**
     * Changes the stroke color of the given class' view.
     * 
     * @param ramClass the class to change the stroke color of
     * @param color the color to change the stroke to
     */
    public void changeStrokeColorOfClassView(Class ramClass, MTColor color) {
        ClassifierView<?> view = classToViewMap.get(ramClass);
        view.setStrokeColor(color);
    }

    /**
     * Deselects all currently selected classifiers.
     */
    private void deselectClassifiers() {
        // Use separate set here, otherwise a concurrent modification occurs,
        // because the view notifies us that it was deselected, which triggers
        // the removal of the view from our set.
        for (BaseView<?> baseView : new HashSet<BaseView<?>>(selectedElements)) {
            baseView.setSelect(false);
        }

        selectedElements.clear();
    }

    /**
     * Removes the given {@link BaseView} from the list of selected views.
     * 
     * @param baseView the {@link BaseView} to remove
     */
    protected void elementDeselected(BaseView<?> baseView) {
        selectedElements.remove(baseView);
    }

    /**
     * Adds the given {@link BaseView} to the list of selected views.
     * 
     * @param baseView the baseView to add to the selection
     */
    protected void elementSelected(BaseView<?> baseView) {
        selectedElements.add(baseView);
    }

    /**
     * Deletes the view of the given classifier from this view.
     * 
     * @param classifier the {@link Classifier} to delete
     */
    private void deleteClassifier(final Classifier classifier) {
        RamApp.getApplication().invokeLater(new Runnable() {

            @Override
            public void run() {
                ClassifierView<?> classifierView = classToViewMap.remove(classifier);
                for (NaryAssociationNodeView assocView : naryAssociationToViewMap.values()) {
                    if (assocView.getAssociationClassLink().getToEnd().getComponentView() == classifierView) {
                        COREPerspective perspective = NavigationBar.getInstance().getCurrentPerspective();
                        switch (perspective.getName()) {
                            case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE:
                                PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController()
                                        .removeAssociationClass(assocView.getAssociation());
                                break;
                            case PerspectiveName.DESIGN_MODEL_PERSPECTIVE:
                                PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController()
                                        .removeAssociationClass(assocView.getAssociation());
                                break;
                        }
                    }
                }
                selectedElements.remove(classifierView);

                removeChild(classifierView);
                classifierView.destroy();
            }
        });
    }

    /**
     * Deletes some renum from view.
     * 
     * @param renum to be removed
     */
    private void deleteEnum(final CDEnum renum) {
        RamApp.getApplication().invokeLater(new Runnable() {

            @Override
            public void run() {
                EnumView enumView = enumToViewMap.remove(renum);

                removeChild(enumView);
                enumView.destroy();
            }
        });
    }

    /**
     * Deletes some note from view.
     * 
     * @param note to be removed
     */
    private void deleteNote(final Note note) {
        RamApp.getApplication().invokeLater(new Runnable() {

            @Override
            public void run() {
                NoteView noteView = noteToViewMap.remove(note);
                removeChild(noteView);
                noteView.destroy();
            }
        });
    }

    /**
     * Deselects all classes.
     */
    public void deselect() {
        deselectClassifiers();
    }

    @Override
    public void destroy() {
        // remove listeners
        EMFEditUtil.removeListenerFor(layout, this);
        EMFEditUtil.removeListenerFor(cd, this);

        // destroy relationships first
        for (InheritanceView view : inheritanceViewList) {
            view.destroy();
        }

        for (AssociationView view : associationToViewMap.values()) {
            view.destroy();
        }

        for (NaryAssociationNodeView view : naryAssociationToViewMap.values()) {
            view.destroy();
        }

        // destroy class views
        for (ClassifierView<?> view : getClassifierViews()) {
            view.destroy();
        }

        // destroy note views
        for (NoteView view : getNoteViews()) {
            view.destroy();
        }

        // do rest
        super.destroy();
    }

    /**
     * Returns all classifier views of this view.
     * 
     * @return the {@link ClassifierView} contained in this view.
     */
    public Collection<ClassifierView<?>> getClassifierViews() {
        return classToViewMap.values();
    }

    /**
     * Gets the {@link ClassifierView} of the specified classifier.
     * 
     * @param specifiedClass the Classifier element for which we want to get the classifier view
     * @return {@link ClassifierView}
     */
    public ClassifierView<?> getClassViewOf(Classifier specifiedClass) {
        return classToViewMap.get(specifiedClass);
    }

    /**
     * Gets the {@link NaryAssociationNodeView} of the specified association if it exists.
     * 
     * @param association the Association element for which we want to get the view
     * @return {@link NaryAssociationNodeView}
     */
    public NaryAssociationNodeView getNaryAssociationNodeViewOf(Association association) {
        return naryAssociationToViewMap.get(association);
    }

    /**
     * Get an {@link EnumView} within this structural view with an {@link CDEnum}.
     * 
     * @param renum the {@link CDnum} of which we want the enum view of
     * @return {@link EnumView} associated to the {@link CDEnum}
     */
    public EnumView getEnumView(CDEnum renum) {
        return enumToViewMap.get(renum);
    }

    /**
     * Returns all {@link ClassView}s contained in this class diagram.
     * 
     * @return the {@link ClassView}s contained in this view
     */
    public Collection<ClassView> getClassViews() {
        Collection<ClassView> classViews = new HashSet<ClassView>();

        for (ClassifierView<?> classifierView : classToViewMap.values()) {
            if (classifierView instanceof ClassView) {
                classViews.add((ClassView) classifierView);
            }
        }

        return classViews;
    }

    /**
     * Returns {@link NoteView} for a specified {@link Note}.
     * 
     * @param note {@link Note} to retrieve view for
     * @return the {@link NoteView} for this note
     */
    public NoteView getNoteView(Note note) {
        if (noteToViewMap.containsKey(note)) {
            return noteToViewMap.get(note);
        }
        return null;
    }

    /**
     * Returns all {@link NoteView}s contained in this class diagram.
     * 
     * @return the {@link NoteView}s contained in this view
     */
    public Collection<NoteView> getNoteViews() {
        Collection<NoteView> noteViews = new HashSet<NoteView>();

        for (BaseView<?> noteView : noteToViewMap.values()) {
            if (noteView instanceof NoteView) {
                noteViews.add((NoteView) noteView);
            }
        }

        return noteViews;
    }

    /**
     * Returns a collection of all base views.
     * This includes {@link ClassView}, {@link NoteView}, {@link ImplementationClassView} and {@link EnumView}.
     * 
     * @return a collection of all base views
     */
    public Collection<BaseView<?>> getBaseViews() {
        Collection<BaseView<?>> baseViews = new HashSet<BaseView<?>>();

        baseViews.addAll(classToViewMap.values());
        baseViews.addAll(enumToViewMap.values());
        baseViews.addAll(noteToViewMap.values());
        baseViews.addAll(naryAssociationToViewMap.values());

        return baseViews;
    }

    /**
     * Returns all currently selected {@link BaseView}s.
     * 
     * @return a set of all currently selected Base Views
     */
    public Set<BaseView<?>> getSelectedElements() {
        return selectedElements;
    }

    /**
     * Returns all currently selected {@link ClassifierView}s.
     * 
     * @return a set of all currently selected classifier views
     */
    public Set<ClassifierView<?>> getSelectedClassifierViews() {
        Set<ClassifierView<?>> selectedClassifiers = new HashSet<ClassifierView<?>>();
        for (BaseView<?> view : selectedElements) {
            if (view instanceof ClassifierView) {
                selectedClassifiers.add((ClassifierView<?>) view);
            }
        }
        return selectedClassifiers;
    }

    /**
     * Returns the {@link ClassDiagram} represented by this view.
     * 
     * @return the represented {@link ClassDiagram}
     */
    public ClassDiagram getClassDiagram() {
        return cd;
    }

    @Override
    public void notifyChanged(Notification notification) {
        if (notification.getFeature() == CdmPackage.Literals.CLASS_DIAGRAM__CLASSES) {
            Classifier classifier = null;
            switch (notification.getEventType()) {
                case Notification.ADD:
                    classifier = (Classifier) notification.getNewValue();
                    if (classifier instanceof Class) {
                        addClass((Class) classifier, layout.getValue().get(classifier));
                    } else if (classifier instanceof ImplementationClass) {
                        addImplementationClass((ImplementationClass) classifier, layout.getValue().get(classifier));
                    }

                    // create an inheritance for all super types
                    for (Classifier superClass : classifier.getSuperTypes()) {
                        addInheritanceView(classifier, superClass);
                    }
                    break;
                case Notification.REMOVE:
                    classifier = (Classifier) notification.getOldValue();
                    deleteClassifier(classifier);
                    break;
            }
        } else if (notification.getFeature() == CdmPackage.Literals.CONTAINER_MAP__VALUE) {
            if (notification.getEventType() == Notification.ADD) {
                ElementMapImpl elementMap = (ElementMapImpl) notification.getNewValue();
                if (elementMap.getKey() instanceof CDEnum) {
                    enumToViewMap.get(elementMap.getKey()).setLayoutElement(elementMap.getValue());
                } else if (elementMap.getKey() instanceof Classifier) {
                    classToViewMap.get(elementMap.getKey()).setLayoutElement(elementMap.getValue());
                } else if (elementMap.getKey() instanceof Note) {
                    noteToViewMap.get(elementMap.getKey()).setLayoutElement(elementMap.getValue());
                } else if (elementMap.getKey() instanceof Association) {
                    Association association = (Association) elementMap.getKey();
                    if (naryAssociationToViewMap.containsKey(association)) {
                        naryAssociationToViewMap.get(association).setLayoutElement(elementMap.getValue());
                    }
                }
            }
        } else if (notification.getFeature() == CdmPackage.Literals.CLASS_DIAGRAM__ASSOCIATIONS) {
            Association association = null;
            switch (notification.getEventType()) {
                case Notification.REMOVE:
                    association = (Association) notification.getOldValue();
                    if (association.getEnds().size() <= 2 && association.getAssociationClass() == null) {
                        removeAssociationView(association);
                    } else {
                        removeNaryAssociation(association);
                    }

                    break;
                case Notification.ADD:
                    association = (Association) notification.getNewValue();
                    addAssociationViews(association);
                    break;
            }
        } else if (notification.getFeature() == CdmPackage.Literals.CLASS_DIAGRAM__TYPES) {
            if (notification.getNewValue() instanceof CDEnum || notification.getOldValue() instanceof CDEnum) {
                CDEnum renum = null;
                switch (notification.getEventType()) {
                    case Notification.ADD:
                        renum = (CDEnum) notification.getNewValue();
                        addEnum(renum, layout.getValue().get(renum), renum.getInstanceClassName() != null);
                        break;
                    case Notification.REMOVE:
                        renum = (CDEnum) notification.getOldValue();
                        deleteEnum(renum);
                        break;
                }

            }
        } else if (notification.getFeature() == CdmPackage.Literals.CLASS_DIAGRAM__NOTES) {
            Note note = null;
            switch (notification.getEventType()) {
                case Notification.ADD:
                    note = (Note) notification.getNewValue();
                    addNote(note, layout.getValue().get(note));
                    for (NamedElement annotated : note.getNotedElement()) {
                        addAnnotationView(note, annotated);
                    }
                    break;
                case Notification.REMOVE:
                    note = (Note) notification.getOldValue();
                    deleteNote(note);
                    break;
            }
        }
    }

    @Override
    protected void registerGestureListeners(IGestureEventListener listener) {
        super.registerGestureListeners(listener);

        addGestureListener(TapProcessor.class, listener);
        addGestureListener(TapAndHoldProcessor.class, listener);
        addGestureListener(TapAndHoldProcessor.class, new TapAndHoldVisualizer(RamApp.getApplication(), getParent()));
    }

    @Override
    protected void registerInputProcessors() {
        registerInputProcessor(new TapProcessor(RamApp.getApplication(), GUIConstants.TAP_MAX_FINGER_UP, false,
                GUIConstants.TAP_DOUBLE_TAP_TIME));
        registerInputProcessor(new TapAndHoldProcessor(RamApp.getApplication(), GUIConstants.TAP_AND_HOLD_DURATION));
        registerInputProcessor(new PanProcessorTwoFingers(RamApp.getApplication()));
        registerInputProcessor(new RightClickDragProcessor(RamApp.getApplication()));
        registerInputProcessor(new ZoomProcessor(RamApp.getApplication()));
        registerInputProcessor(new MouseWheelProcessor(RamApp.getApplication()));

        UnistrokeProcessorLeftClick up = new UnistrokeProcessorLeftClick(RamApp.getApplication());
        up.addTemplate(UnistrokeGesture.CIRCLE, Direction.CLOCKWISE);
        up.addTemplate(UnistrokeGesture.CIRCLE, Direction.COUNTERCLOCKWISE);
        up.addTemplate(UnistrokeGesture.RECTANGLE, Direction.CLOCKWISE);
        up.addTemplate(UnistrokeGesture.RECTANGLE, Direction.COUNTERCLOCKWISE);
        up.addTemplate(UnistrokeGesture.CHECK, Direction.CLOCKWISE);
        up.addTemplate(UnistrokeGesture.TRIANGLE, Direction.COUNTERCLOCKWISE);
        up.addTemplate(UnistrokeGesture.TRIANGLE, Direction.CLOCKWISE);
        up.addTemplate(UnistrokeGesture.QUESTION, Direction.CLOCKWISE);
        up.addTemplate(UnistrokeGesture.X, Direction.CLOCKWISE);
        up.addTemplate(UnistrokeGesture.X, Direction.COUNTERCLOCKWISE);

        registerInputProcessor(up);
    }

    /**
     * Removes the {@link AssociationView} for the given {@link Association}.
     * 
     * @param association the association that was deleted
     */
    public void removeAssociationView(Association association) {
        AssociationView assocView = associationToViewMap.get(association);
        CDEnd<AssociationEnd, LinkableView<?>> ramEnd = assocView.getFromEnd();
        CDEnd<AssociationEnd, LinkableView<?>> ramEndOpposite = assocView.getToEnd();
        LinkableView<?> classViewFrom = ramEnd.getComponentView();
        LinkableView<?> classViewTo = ramEndOpposite.getComponentView();

        removeChild(assocView);
        associationToViewMap.remove(association);

        assocView.destroy();

        classViewFrom.removeRelationshipEnd(ramEnd);
        classViewTo.removeRelationshipEnd(ramEndOpposite);
    }

    /**
     * Removes an Nary Association view with all its containted views for the given {@link Association}.
     * 
     * @param association the association that was deleted
     */
    public void removeNaryAssociation(Association association) {
        NaryAssociationNodeView assocNodeView = naryAssociationToViewMap.get(association);

        for (NaryAssociationView assocView : assocNodeView.getAssociationViews()) {
            CDEnd<NamedElement, LinkableView<?>> toEnd = assocView.getToEnd();
            LinkableView<?> classView = toEnd.getComponentView();

            removeChild(assocView);

            assocView.destroy();
            classView.removeRelationshipEnd(toEnd);
        }

        assocNodeView.removeAssociationClassLink();

        removeChild(assocNodeView);
        naryAssociationToViewMap.remove(association);

        assocNodeView.destroy();
    }

    /**
     * Removes the {@link InheritanceView} between the given super and sub class.
     * 
     * @param derivedClass the sub class
     * @param superClass the super class
     */
    public void removeInheritanceView(Classifier derivedClass, Classifier superClass) {
        InheritanceView inherViewToBeDeleted = null;

        for (InheritanceView inherView : inheritanceViewList) {

            Classifier fromRamClass = inherView.getFromEnd().getModel();
            Classifier toRamClass = inherView.getToEnd().getModel();

            if (fromRamClass == derivedClass && toRamClass == superClass) {
                inherViewToBeDeleted = inherView;
                break;
            }
        }

        CDEnd<Classifier, ClassifierView<?>> ramEndFrom = inherViewToBeDeleted.getFromEnd();
        CDEnd<Classifier, ClassifierView<?>> ramEndTo = inherViewToBeDeleted.getToEnd();

        ClassifierView<?> classViewFrom = ramEndFrom.getComponentView();
        ClassifierView<?> classViewTo = ramEndTo.getComponentView();

        inheritanceViewList.remove(inherViewToBeDeleted);
        removeChild(inherViewToBeDeleted);

        inherViewToBeDeleted.destroy();

        classViewFrom.removeRelationshipEnd(ramEndFrom);
        classViewTo.removeRelationshipEnd(ramEndTo);

    }

    /**
     * Removes the {@link AnnotationView} between the given note and class.
     * 
     * @param note the note
     * @param annotatedElement the annotated {@link NamedElement}
     */
    public void removeAnnotationView(Note note, NamedElement annotatedElement) {
        AnnotationView annotViewToBeDeleted = null;

        for (AnnotationView annotView : annotationViewList) {

            Note noteFrom = annotView.getNoteView().getNote();
            NamedElement annotatedElementTo = (NamedElement) annotView.getAnnotatedElementView().getRepresented();

            if (noteFrom == note && annotatedElementTo == annotatedElement) {
                annotViewToBeDeleted = annotView;
                break;
            }
        }

        if (annotViewToBeDeleted != null) {
            annotationViewList.remove(annotViewToBeDeleted);
            removeChild(annotViewToBeDeleted);

            annotViewToBeDeleted.destroy();
        }

    }

    /**
     * Sets the fill and stroke color of all class views to the given fill and stroke color.
     * 
     * @param colorFill the fill color
     * @param colorStroke the stroke color
     */
    public void setFillAndStrokeColorOfAllClasses(MTColor colorFill, MTColor colorStroke) {
        for (ClassifierView<?> view : getClassifierViews()) {
            view.setFillColor(colorFill);
            view.setStrokeColor(colorStroke);
        }
    }

    /**
     * Overloaded method to also update the association's background color.
     * 
     * @param color
     *            The new background color
     */
    @Override
    public void setFillColor(MTColor color) {
        super.setFillColor(color);

        for (AssociationView a : associationToViewMap.values()) {
            a.setBackgroundColor(color);
        }
    }

    /**
     * Toggles the visibility of all NoteViews and their associated AnnotationViews.
     * 
     * @return the new visibility status of notes
     */
    public boolean toggleNotes() {
        boolean newVisibility = true;
        for (NoteView noteView : noteToViewMap.values()) {
            newVisibility = !noteView.isVisible();
            noteView.setVisible(newVisibility);
            for (RelationshipView<?, ?> annotView : noteView.getAllRelationshipViews()) {
                annotView.setVisible(newVisibility);
            }
            noteView.updateRelationships();
        }
        return newVisibility;
    }
}
