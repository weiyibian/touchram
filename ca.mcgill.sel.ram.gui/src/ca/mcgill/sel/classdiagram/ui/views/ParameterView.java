package ca.mcgill.sel.classdiagram.ui.views;

import org.mt4j.util.font.IFont;

import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.classdiagram.Parameter;
import ca.mcgill.sel.classdiagram.CdmPackage;
import ca.mcgill.sel.classdiagram.ui.views.handler.ClassDiagramHandlerFactory;
import ca.mcgill.sel.classdiagram.ui.views.handler.IParameterViewHandler;

import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.layouts.HorizontalLayoutVerticallyCentered;
import ca.mcgill.sel.ram.ui.utils.Fonts;
import ca.mcgill.sel.ram.ui.utils.GraphicalUpdater;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.IHandled;


/**
 * A {@link ParameterView} represents a {@link Parameter} and is contained inside an {@link OperationView}.
 * 
 * @author mschoettle
 * @author yhattab
 */
public class ParameterView extends RamRectangleComponent implements IHandled<IParameterViewHandler> {

    private TextView nameField;
    private TextView typeField;

    private Parameter parameter;

    private boolean isMutable;

    private GraphicalUpdater graphicalUpdater;

    private IParameterViewHandler handler;

    /**
     * Creates a new {@link ParameterView} for the given {@link Parameter}.
     * 
     * @param operationView the parent {@link OperationView}
     * @param parameter the {@link Parameter} to be represented
     * @param mutable is this parameter view mutable
     */
    public ParameterView(OperationView operationView, Parameter parameter, boolean mutable) {
        this.parameter = parameter;
        setBuffers(0);

        isMutable = mutable;

        typeField = new TextView(parameter, CdmPackage.Literals.TYPED_ELEMENT__TYPE);
        typeField.setBufferSize(Cardinal.SOUTH, OperationView.BUFFER_BOTTOM);
        typeField.setBufferSize(Cardinal.WEST, 2);
        typeField.setBufferSize(Cardinal.EAST, 2);
        addChild(typeField);

        nameField = new TextView(parameter, CdmPackage.Literals.NAMED_ELEMENT__NAME);
        nameField.setBufferSize(Cardinal.SOUTH, OperationView.BUFFER_BOTTOM);
        nameField.setBufferSize(Cardinal.EAST, 2);
        nameField.setBufferSize(Cardinal.WEST, 2);
        nameField.setUniqueName(true);
        addChild(nameField);

        setLayout(new HorizontalLayoutVerticallyCentered(Fonts.FONT_SIZE / 8));

        if (isMutable) {
            typeField.setHandler(ClassDiagramHandlerFactory.INSTANCE.getTextViewHandler());
            nameField.setHandler(ClassDiagramHandlerFactory.INSTANCE.getParameterNameHandler());
        }

        ClassDiagram cd = EMFModelUtil.getRootContainerOfType(parameter, CdmPackage.Literals.CLASS_DIAGRAM);
        graphicalUpdater = RamApp.getApplication().getGraphicalUpdaterForModel(cd);
        graphicalUpdater.addGUListener(parameter, this);
    }

    /**
     * Constructor. Makes a default mutable parameter.
     * 
     * @param operationView Owner of this parameter
     * @param parameter Parameter to add the the operationview
     */
    public ParameterView(OperationView operationView, Parameter parameter) {
        this(operationView, parameter, true);
    }

    @Override
    public void destroy() {
        super.destroy();

        graphicalUpdater.removeGUListener(parameter, this);
    }
    
    /**
     * Returns the {@link Parameter} that is represented by this view.
     * 
     * @return the represented {@link Parameter}
     */
    public Parameter getParameter() {
        return parameter;
    }

    @Override
    public IParameterViewHandler getHandler() {
        return this.handler;
    }

    @Override
    public void setHandler(IParameterViewHandler handler) {
        this.handler = handler;
    }

    /**
     * Returns the name component.
     * 
     * @return the name component
     */
    public TextView getNameField() {
        return nameField;
    }

    /**
     * Updates the style of this view.
     * 
     * @param font the font to use for text components of this view
     */
    protected void updateStyle(IFont font) {
        nameField.setFont(font);
        typeField.setFont(font);
    }

}
