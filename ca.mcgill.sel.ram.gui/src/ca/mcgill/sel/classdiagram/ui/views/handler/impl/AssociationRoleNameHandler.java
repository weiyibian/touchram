package ca.mcgill.sel.classdiagram.ui.views.handler.impl;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;

import ca.mcgill.sel.classdiagram.AssociationEnd;
import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.perspective.COREPerspectiveUtil;
import ca.mcgill.sel.core.perspective.PerspectiveName;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamSelectorComponent;
import ca.mcgill.sel.ram.ui.components.listeners.AbstractDefaultRamSelectorListener;
import ca.mcgill.sel.ram.ui.perspective.PerspectiveControllerFactory;
import ca.mcgill.sel.ram.ui.perspective.classdiagram.CdmPerspectiveController;
import ca.mcgill.sel.ram.ui.views.OptionSelectorView;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.impl.ValidatingTextViewHandler;
import ca.mcgill.sel.ram.ui.views.structural.handler.IAssociationRoleNameHandler;
import ca.mcgill.sel.ram.util.MetamodelRegex;

/**
 * The default handler for a {@link TextView} representing the role name of an {@link AssociationEnd}.
 * The name gets validated in order to only allow valid names.
 *
 * @author lmartellotto
 * @author yhattab
 *
 */
public class AssociationRoleNameHandler extends ValidatingTextViewHandler implements IAssociationRoleNameHandler {

    /**
     * Creates a new {@link AssociationRoleNameHandler}.
     */
    public AssociationRoleNameHandler() {
        super(MetamodelRegex.REGEX_TYPE_NAME);
    }

    /**
     * Features that can be used on the role name of an {@link AssociationEnd}.
     */
    private enum AssociationRoleFeatures {
        STATIC;
    }

    @Override
    public boolean processTapAndHoldEvent(TapAndHoldEvent tapAndHoldEvent) {
        if (tapAndHoldEvent.isHoldComplete()) {
            TextView target = (TextView) tapAndHoldEvent.getTarget();

            final AssociationEnd attribute = (AssociationEnd) target.getData();

            OptionSelectorView<AssociationRoleFeatures> selector =
                    new OptionSelectorView<AssociationRoleNameHandler.AssociationRoleFeatures>(
                            AssociationRoleFeatures.values());
            RamApp.getActiveScene().addComponent(selector, tapAndHoldEvent.getLocationOnScreen());

            selector.registerListener(
                    new AbstractDefaultRamSelectorListener<AssociationRoleNameHandler.AssociationRoleFeatures>() {
                        @Override
                        public void elementSelected(RamSelectorComponent<AssociationRoleFeatures> selector,
                                AssociationRoleFeatures element) {

                            COREPerspective perspective = COREPerspectiveUtil.INSTANCE.getModelPerspective(attribute);
                            switch (element) {
                                case STATIC:
                                    switch (perspective.getName()) {
                                        case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE: 
                                            PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController()
                                                .switchStatic(attribute);
                                            break;
                                        case PerspectiveName.DESIGN_MODEL_PERSPECTIVE: 
                                            PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController()
                                            .switchStatic(attribute);
                                            break;
                                    }
                                    break;
                            }
                        }

                    });
        }

        return true;
    }

    @Override
    protected void setValue(EObject data, EStructuralFeature feature, Object value) {
        final AssociationEnd associationEnd = (AssociationEnd) data;
        CdmPerspectiveController cdmController = PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController();
        
        COREPerspective perspective = COREPerspectiveUtil.INSTANCE.getModelPerspective(associationEnd);
        switch (perspective.getName()) {
            case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE: 
                cdmController.setRoleName((ClassDiagram) EcoreUtil.getRootContainer(associationEnd), associationEnd, 
                        value.toString());
                break;
            case PerspectiveName.DESIGN_MODEL_PERSPECTIVE: 
                cdmController.setRoleName((ClassDiagram) EcoreUtil.getRootContainer(associationEnd), associationEnd, 
                        value.toString());
                break;
        }  
    }

}
