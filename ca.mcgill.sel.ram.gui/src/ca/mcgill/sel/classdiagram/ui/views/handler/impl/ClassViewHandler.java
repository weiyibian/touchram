package ca.mcgill.sel.classdiagram.ui.views.handler.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.classdiagram.CdmFactory;
import ca.mcgill.sel.classdiagram.Class;
import ca.mcgill.sel.classdiagram.ObjectType;
import ca.mcgill.sel.classdiagram.Parameter;
import ca.mcgill.sel.classdiagram.PrimitiveType;
import ca.mcgill.sel.classdiagram.Type;
import ca.mcgill.sel.classdiagram.VisibilityType;
import ca.mcgill.sel.classdiagram.ui.utils.CdmModelUtils;
import ca.mcgill.sel.classdiagram.ui.views.ClassView;
import ca.mcgill.sel.classdiagram.ui.views.ClassifierView;
import ca.mcgill.sel.classdiagram.ui.views.handler.IClassViewHandler;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.perspective.COREPerspectiveUtil;
import ca.mcgill.sel.core.perspective.PerspectiveName;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamKeyboard;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.components.listeners.DefaultRamKeyboardListener;
import ca.mcgill.sel.ram.ui.components.menu.RamLinkedMenu;
import ca.mcgill.sel.ram.ui.components.navigationbar.NavigationBar;
import ca.mcgill.sel.ram.ui.perspective.PerspectiveControllerFactory;
import ca.mcgill.sel.ram.ui.perspective.classdiagram.CdmPerspectiveController;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.util.MetamodelRegex;


/**
 * The default handler for a {@link ClassView}.
 *
 * @author mschoettle
 * @author yhattab
 */
public class ClassViewHandler extends ClassifierViewHandler implements IClassViewHandler {

    /**
     * The action to add a new attribute.
     */
    protected static final String ACTION_ATTRIBUTE_ADD = "view.class.attribute.add";
    private static final String ACTION_DESTRUCTOR_ADD = "view.class.destructor.add";
    private static final String ACTION_ABSTRACT = "view.class.abstract";

    /**
     * Delimiter of a parameter (provided by the user) can be "," and optionally be followed by a whitespace.
     */
    private static final String PARAMETER_DELIMITER_PATTERN = ",\\s?";
    private static final String SUBMENU_CLASS_CHARACTERISTICS = "sub.class.partiality";

    @Override
    public void createAttribute(final ClassView classView) {
        final int index = classView.getRamClass().getAttributes().size();

        final RamTextComponent textRow = new RamTextComponent();
        textRow.setPlaceholderText(Strings.PH_ATTRIBUTE);
        final RamRectangleComponent attributesContainer = classView.getAttributesContainer();
        if (!classView.containsChild(attributesContainer)) {
            classView.addChild(attributesContainer);
        }
        attributesContainer.addChild(index, textRow);

        RamKeyboard keyboard = new RamKeyboard();
        keyboard.registerListener(new DefaultRamKeyboardListener() {
            @Override
            public void keyboardCancelled() {
                attributesContainer.removeChild(textRow);

            }

            @Override
            public boolean verifyKeyboardDismissed() {
                try {
                    createAttribute(classView.getRamClass(), index, textRow.getText());
                    attributesContainer.removeChild(textRow);

                } catch (final IllegalArgumentException e) {
                    return false;
                }
                return true;
            }
        });

        textRow.showKeyboard(keyboard);
    }

    @Override
    public void createOperation(final ClassifierView<?> classifierView) {
        final int index = classifierView.getOperationsContainer().getChildCount();

        final RamTextComponent textRow = new RamTextComponent();
        textRow.setPlaceholderText(Strings.PH_OPERATION);
        final RamRectangleComponent operationsContainer = classifierView.getOperationsContainer();
        operationsContainer.addChild(index, textRow);

        RamKeyboard keyboard = new RamKeyboard();
        keyboard.registerListener(new DefaultRamKeyboardListener() {
            @Override
            public void keyboardCancelled() {
                operationsContainer.removeChild(textRow);

            }

            @Override
            public boolean verifyKeyboardDismissed() {
                try {
                    Class ramClass = ((ClassView) classifierView).getRamClass();
                    createOperation(ramClass, ramClass.getOperations().size(), textRow.getText());
                    operationsContainer.removeChild(textRow);
                } catch (final IllegalArgumentException e) {
                    return false;
                }
                return true;
            }
        });

        textRow.showKeyboard(keyboard);
    }

    /**
     * Creates a new attribute based on the given attribute string (<code>"[typeName] [attributeName]"</code>).
     * The attribute is added to the class at the given index.
     *
     * @param owner the {@link Class} the attribute should be added to
     * @param index the index at which the attribute should be added to the class
     * @param attributeString the textual description of the attribute
     * @throws IllegalArgumentException if the given string does not match the expected pattern
     */
    private static void createAttribute(Class owner, int index, String attributeString) {
        Matcher matcher = Pattern.compile(MetamodelRegex.REGEX_ATTRIBUTE_DECLARATION).matcher(attributeString);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("The string " + attributeString + " is not valid syntax for attributes");
        }

        String name = matcher.group(3);
        ObjectType type = getTypeByNameIdentifier(matcher.group(1));

        COREPerspective perspective = COREPerspectiveUtil.INSTANCE.getModelPerspective(owner);
        switch (perspective.getName()) {
            case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE: 
                PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController()
                    .createAttribute(owner, index, name, type);
                break;
            case PerspectiveName.DESIGN_MODEL_PERSPECTIVE: 
                PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController()
                    .createAttribute(owner, index, name, type);
                break;
        }
    }

    /**
     * Creates a new operation based on the given operation string.
     * The string should match the following pattern:
     * <code>"[visibility] [returnType] [operationName]([optional parameters])"</code>.
     * The parameters need to be separated by comma and have to match the following pattern:
     * <code>"[parameterType] [parameterName]"</code>.
     * The operation is added to the class at the given index.
     *
     * @param owner the {@link Class} the operation should be added to
     * @param index the index at which the operation should be added to the class
     * @param operationString the textual description of the operation
     * @throws IllegalArgumentException if the given string does not match the expected pattern
     */
    private static void createOperation(Class owner, int index, String operationString) {
        Matcher matcher =
                Pattern.compile("^" + MetamodelRegex.REGEX_OPERATION_DECLARATION + "$").matcher(operationString);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("The string " + operationString
                    + " does not conform to operation syntax");
        }

        VisibilityType visibility = CdmModelUtils.getRamVisibilityFromStringRepresentation(matcher.group(1));
        if (visibility == null) {
            throw new IllegalArgumentException("The visibility is not valid");
        }

        Type returnType = getType(matcher.group(2));
        String name = matcher.group(4);
        List<Parameter> parameters = getParameters(matcher.group(5));

        PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController()
            .createOperation(owner, index, name, visibility,
                returnType, parameters);
    }

    /**
     * Creates and returns a parameter based on the given parameter string.
     * The parameter needs to match following pattern: <code>"[parameterType] [parameterName]"</code>.
     *
     * @param parameterString the textual description of the parameter
     * @return the {@link Parameter} based on the given string
     * @throws IllegalArgumentException if the string does not match the expected pattern
     */
    private static Parameter getParameter(String parameterString) {
        Matcher matcher = Pattern.compile(MetamodelRegex.REGEX_ATTRIBUTE_DECLARATION).matcher(parameterString);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("The type " + parameterString + " does not exist in the model");
        }

        Parameter parameter = CdmFactory.eINSTANCE.createParameter();
        Type type = getType(matcher.group(1));

        parameter.setType(type);
        parameter.setName(matcher.group(3));

        return parameter;
    }

    /**
     * Returns a list of parameters based on the given string.
     * The parameters need to be separated by comma and have to match the following pattern:
     * <code>"[parameterType] [parameterName]"</code>.
     *
     * @param parametersString the comma separated list of parameter strings
     * @return the list of {@link Parameter}s
     * @see #getParameter(String)
     */
    private static List<Parameter> getParameters(String parametersString) {
        List<Parameter> parameters = new ArrayList<Parameter>();

        if (parametersString != null && !parametersString.isEmpty()) {
            String[] parametersList = parametersString.split(PARAMETER_DELIMITER_PATTERN);

            for (String parameterString : parametersList) {
                Parameter parameter = getParameter(parameterString);
                parameters.add(parameter);
            }
        }

        return parameters;
    }

    /**
     * Returns the primitive type that matches the given type name.
     *
     * @param typeString the type name of the type
     * @return the {@link PrimitiveType} that matches the name of the type
     * @throws IllegalArgumentException if the primitive type does not exist
     */
    private static ObjectType getTypeByNameIdentifier(String typeString) {
        ObjectType type = CdmModelUtils.getAttributeTypeByName(typeString);

        if (type == null) {
            throw new IllegalArgumentException("The structural view does not contain a primitive type " + type);
        }

        return type;
    }

    /**
     * Returns the type that matches the given type name.
     *
     * @param typeString the type name
     * @return the {@link Type} that matches the name of the type
     * @throws IllegalArgumentException if the type does not exist
     */
    private static Type getType(String typeString) {
        Type returnType = CdmModelUtils.getTypeByName(typeString);
        if (returnType == null) {
            throw new IllegalArgumentException("The type " + typeString + " does not exist in the model");
        }

        return returnType;
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        String actionCommand = event.getActionCommand();
        RamRectangleComponent pressedButton = (RamRectangleComponent) event.getTarget();
        RamLinkedMenu linkedMenu = pressedButton.getParentOfType(RamLinkedMenu.class);
        if (linkedMenu != null) {

            ClassifierView<?> clazz = (ClassifierView<?>) linkedMenu.getLinkedView();

            if (ACTION_ATTRIBUTE_ADD.equals(actionCommand)) {
                createAttribute((ClassView) clazz);
            } else if (ACTION_DESTRUCTOR_ADD.equals(actionCommand)) {
                createDestructor(clazz);
            } else if (ACTION_ABSTRACT.equals(actionCommand)) {
                switchToAbstract(clazz);
            }
        }
        super.actionPerformed(event);
    }

    @Override
    public void switchToAbstract(ClassifierView<?> clazz) {
        CdmPerspectiveController cdmController = PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController();
        COREPerspective perspective = COREPerspectiveUtil.INSTANCE.getModelPerspective((Class) clazz.getClassifier());
        
        boolean switched = true;
        switch (perspective.getName()) {
            case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE: 
                switched = cdmController.switchAbstract((Class) clazz.getClassifier());
                break;
            case PerspectiveName.DESIGN_MODEL_PERSPECTIVE: 
                switched = cdmController.switchAbstract((Class) clazz.getClassifier());
                break;
        } 
        if (!switched) {
            RamApp.getActiveScene().displayPopup(Strings.POPUP_ABSTRACT_CLASS_NO_SWITCH);
        }
    }
    
    @Override
    public void initMenu(RamLinkedMenu menu) {
        super.initMenu(menu);
        COREPerspective perspective = NavigationBar.getInstance().getCurrentPerspective();
        if (PerspectiveControllerFactory.INSTANCE.getActionValidator()
                .canCreateOperation(perspective)) {
            menu.addAction(Strings.MENU_DESTRUCTOR_ADD, Icons.ICON_MENU_ADD_DESTRUCTOR, ACTION_DESTRUCTOR_ADD, this,
                    SUBMENU_OPERATION, true);
        }
        if (PerspectiveControllerFactory.INSTANCE.getActionValidator()
                .canCreateAttribute(perspective)) {
            menu.addAction(Strings.MENU_ATTRIBUTE_ADD, Icons.ICON_MENU_ADD_ATTRIBUTE, ACTION_ATTRIBUTE_ADD, this,
                    SUBMENU_ADD, true);
        }
        
        menu.addSubMenu(1, SUBMENU_CLASS_CHARACTERISTICS);
        
        menu.addAction(Strings.MENU_ABSTRACT, Strings.MENU_NOT_ABSTRACT, Icons.ICON_MENU_ABSTRACT,
                Icons.ICON_MENU_NOT_ABSTRACT, ACTION_ABSTRACT, this, SUBMENU_CLASS_CHARACTERISTICS, true, false);


        updateButtons(menu);
    }

    @Override
    public void updateMenu(RamLinkedMenu menu, Notification notification) {
        if (notification.getEventType() == Notification.SET
                || notification.getEventType() == Notification.UNSET
                || notification.getEventType() == Notification.ADD
                || notification.getEventType() == Notification.REMOVE) {
            updateButtons(menu);
        }
        super.updateMenu(menu, notification);
    }

    @Override
    public List<EObject> getEObjectToListenForUpdateMenu(RamRectangleComponent rectangle) {
        ClassView classView = (ClassView) rectangle;
        List<EObject> ret = new ArrayList<EObject>();
        ret.addAll(classView.getClassifier().getOperations());
        ret.addAll(super.getEObjectToListenForUpdateMenu(rectangle));
        ret.add(COREArtefactUtil.getReferencingExternalArtefact(classView.getClassifier()));
        return ret;
    }

    /**
     * Update buttons inside the menu.
     *
     * @param menu - the menu which contains buttons.
     */
    private static void updateButtons(RamLinkedMenu menu) {
        Class clazz = (Class) menu.geteObject();
        menu.toggleAction(clazz.isAbstract(), ACTION_ABSTRACT);
    }

    @Override
    public void createConstructor(ClassifierView<?> classifierView) {
        PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController().createConstructor(
                (Class) classifierView.getClassifier());
    }

    @Override
    public void createDestructor(ClassifierView<?> clazz) {
        PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController()
            .createDestructor((Class) clazz.getClassifier());
    }
}