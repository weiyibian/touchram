package ca.mcgill.sel.classdiagram.ui.views.handler;

import org.mt4j.input.inputProcessors.IGestureEventListener;

import ca.mcgill.sel.classdiagram.ui.views.OperationView;

/**
 * This interface is implemented by something that can handle events for a {@link OperationView}.
 * 
 * @author mschoettle
 * @author yhattab
 */
public interface IOperationViewHandler extends IGestureEventListener {

    /**
     * Handles creating a new {@link ca.mcgill.sel.ram.Parameter} at the given model index and visual index.
     * 
     * @param operationView
     *            the affected {@link OperationView}
     */
    void createParameter(OperationView operationView);

    /**
     * Handles the removal of an {@link ca.mcgill.sel.ram.Operation}.
     * 
     * @param operationView
     *            the affected {@link OperationView}
     */
    void removeOperation(OperationView operationView);

    /**
     * Handles the switch of an {@link ca.mcgill.sel.ram.Operation} to abstract.
     * 
     * @param operationView - the affected {@link OperationView}
     */
    void switchToAbstract(OperationView operationView);

    /**
     * Handles the toggle of an {@link ca.mcgill.sel.ram.Operation} to static.
     * 
     * @param operationView - the affected {@link OperationView}
     */
    void setOperationStatic(OperationView operationView);

}
