package ca.mcgill.sel.classdiagram.ui.views;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.edit.provider.INotifyChangedListener;
import org.mt4j.components.TransformSpace;
import org.mt4j.input.gestureAction.TapAndHoldVisualizer;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldProcessor;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.classdiagram.Association;
import ca.mcgill.sel.classdiagram.AssociationEnd;
import ca.mcgill.sel.classdiagram.NamedElement;
import ca.mcgill.sel.classdiagram.CdmPackage;
import ca.mcgill.sel.classdiagram.ui.views.handler.ClassDiagramHandlerFactory;
import ca.mcgill.sel.classdiagram.ui.views.CDEnd.Position;

import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.utils.Fonts;
import ca.mcgill.sel.ram.ui.utils.GUIConstants;
import ca.mcgill.sel.ram.ui.utils.GraphicalUpdater;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.ITextViewHandler;


/**
 * NaryAssociationViews are components used as an alternative to {@link AssociationView} for Nary Associations 
 * a {@link ClassDiagramView}. They draw the link between the {@link AssociationEnd} of a classifier and the nary 
 * diamond node {@link NaryAssociationNodeView} using the correct visual representation (if it's to an association
 * class or a normal class). It is a simplified {@link AssociationView} adapted to the nary context.
 *
 * @author yhattab
 */
public class NaryAssociationView extends RelationshipView<NamedElement, LinkableView<?>> implements
        INotifyChangedListener {

    /**
     * The offset of text from the association.
     */
    static final int TEXT_OFFSET_FROM_ASSOCIATION = 5;
    
    private static final int ROTATION = 90;
    private static final int ROTATION_FORWARD = 1;
    private static final int ROTATION_BACKWARD = -1;
    private static final int TEXT_OFFSET_FROM_CLASSIFIER = 15;
    
    private boolean isToAssociationClass;
    
    private TextView toEndRolename;

    private MultiplicityTextView toEndMultiplicity;

    private GraphicalUpdater graphicalUpdater;

    private Position lastFromPosition;

    /**
     * Creates a new single link from a an Nary diamond node to a an associationEnd attached to a class.
     *
     * @param association
     *            the represented {@link Association}
     * @param fromNaryAssocView
     *            the from {@link NaryAssociationNodeView}
     * @param to
     *            the to {@link NamedElement} that is either {@link AssociationEnd} or {@link Classifier} if this view 
     *            represents a link to an Association Class
     * @param toEndClassifierView
     *            the to {@link LinkableView}
     * @param isAssociationClass
     *            true if this view represents a link to an Association Class
     */
    public NaryAssociationView(Association association, NaryAssociationNodeView fromNaryAssocView,
            NamedElement to, LinkableView<?> toEndClassifierView, boolean isAssociationClass) {
        super(association, fromNaryAssocView, to, toEndClassifierView);
 
        EMFEditUtil.addListenerFor(association, this);
        this.isToAssociationClass = isAssociationClass;
        
        if (isAssociationClass) {
            setLineStyle(LineStyle.DOTTED);
        }
        
        ClassDiagram cd = EMFModelUtil.getRootContainerOfType(association, CdmPackage.Literals.CLASS_DIAGRAM);
        graphicalUpdater = RamApp.getApplication().getGraphicalUpdaterForModel(cd);
        graphicalUpdater.addGUListener(association, this);
    }
    
    /**
     * Returns true if NaryAssociationView represents link to Association Class.
     * @return true if Association Class, false otherwise
     */
    public boolean isAssociationClass() {
        return isToAssociationClass;
    }
    
    @Override
    protected void updateRelationshipEnds(RamRectangleComponent viewFrom, RamRectangleComponent viewTo,
            Position fromEndPosition, Position toEndPosition) {
        // now we set the positions.
        if (viewFrom instanceof LinkableView) {
            // process from end
            LinkableView<?> classViewFrom = (LinkableView<?>) viewFrom;
            LinkableView<?> classViewTo = (LinkableView<?>) viewTo;

            // if previous and current positions are different
            // also if fromEnd.getPosition() is null
            if (fromEndPosition != fromEnd.getPosition()) {
                classViewFrom.moveRelationshipEnd(fromEnd, fromEndPosition);
                classViewFrom.updateLayout();
            } else {
                // if position is the same reset the positions
                // for all ends on the same edge of this view.
                classViewFrom.setCorrectPosition(fromEnd);
            }

            // process to end
            if (toEndPosition != toEnd.getPosition()) {
                classViewTo.moveRelationshipEnd(toEnd, toEndPosition);
                classViewFrom.moveRelationshipEnd(fromEnd, fromEndPosition);
                classViewTo.updateLayout();
            } else {
                classViewTo.setCorrectPosition(toEnd);
            }
        }
    }

    /**
     * Creates text (multiplicity and role name) for the classifier end if they haven't been created yet.
     *
     * @param cdEnd the classifier end
     */
    private void createText(CDEnd<NamedElement, LinkableView<?>> cdEnd) {
        AssociationEnd toAssociationEnd;
        if (toEndRolename == null && !isToAssociationClass) {
            toAssociationEnd = (AssociationEnd) cdEnd.getModel();
            toEndRolename = createRoleNameView(toAssociationEnd);
            toEndMultiplicity = createMultiplicityView(toAssociationEnd);
            
            graphicalUpdater.addGUListener(toAssociationEnd, toEndRolename);
        }
    }

    /**
     * Moves the text (multiplicity and role name) for the given end.
     *
     * @param ramEnd the end the texts to move for
     * @param roleName the view for the role name
     * @param multiplicity the view for the multiplicity
     * @param lastPosition the last position the text was located at
     */
    private void moveText(CDEnd<NamedElement, LinkableView<?>> ramEnd, TextView roleName,
            MultiplicityTextView multiplicity, Position lastPosition) {

        if (roleName != null && multiplicity != null) {
            // If the position changed, rotate the texts back to it's regular position.
            if (lastPosition != null
                    && lastPosition != ramEnd.getPosition()) {
                rotateText(roleName, lastPosition, ROTATION_BACKWARD);
                rotateText(multiplicity, lastPosition, ROTATION_BACKWARD);
            }
            
            moveRoleName(roleName, ramEnd.getLocation(), ramEnd.getPosition());
            moveMultiplicity(multiplicity, ramEnd.getLocation(), ramEnd.getPosition());
            
            // Rotate the texts for top and bottom positions
            if (lastPosition != ramEnd.getPosition()) {
                rotateText(roleName, ramEnd.getPosition(), ROTATION_FORWARD);
                rotateText(multiplicity, ramEnd.getPosition(), ROTATION_FORWARD);
            }
        }
    }

    /**
     * Rotates the given text depending on the position and into the given direction.
     * 
     * @param text the text to rotate
     * @param position the position on which the text is located at
     * @param direction the rotation direction
     */
    private static void rotateText(RamTextComponent text, Position position, int direction) {
        float x = text.getPosition(TransformSpace.RELATIVE_TO_PARENT).getX();
        float y = text.getPosition(TransformSpace.RELATIVE_TO_PARENT).getY();
        
        // happily changing the anchor of the text area is sufficient in most cases
        switch (position) {
            case BOTTOM:
                text.rotateZ(new Vector3D(x, y), direction * -ROTATION);
                break;
            case TOP:
                text.rotateZ(new Vector3D(x, y), direction * ROTATION);
                break;
        }
    }

    /**
     * Moves the role name to the proper position.
     *
     * @param textView the view of the role name
     * @param currentPosition the current position of the role name
     * @param position the side of the association end on the attached classifier view
     */
    @SuppressWarnings("static-method")
    private void moveRoleName(RamTextComponent textView, Vector3D currentPosition, Position position) {
        float x = currentPosition.getX();
        float y = currentPosition.getY();

        // happily changing the anchor of the text area is sufficient in most cases
        switch (position) {
            case BOTTOM:
                textView.setAnchor(PositionAnchor.LOWER_RIGHT);
                x -= TEXT_OFFSET_FROM_ASSOCIATION;
                y += TEXT_OFFSET_FROM_CLASSIFIER;
                break;
            case TOP:
                textView.setAnchor(PositionAnchor.LOWER_RIGHT);
                x += TEXT_OFFSET_FROM_ASSOCIATION;
                y -= TEXT_OFFSET_FROM_CLASSIFIER;
                break;
            case LEFT:
                textView.setAnchor(PositionAnchor.LOWER_RIGHT);
                x -= TEXT_OFFSET_FROM_CLASSIFIER;
                y -= TEXT_OFFSET_FROM_ASSOCIATION;
                break;
            case RIGHT:
                textView.setAnchor(PositionAnchor.LOWER_LEFT);
                x += TEXT_OFFSET_FROM_CLASSIFIER;
                y -= TEXT_OFFSET_FROM_ASSOCIATION;
                break;
        }

        textView.setPositionRelativeToParent(new Vector3D(x, y));
    }

    /**
     * Moves the multiplicity to the proper position.
     *
     * @param textView the view of the multiplicity
     * @param currentPosition the current position of the multiplicity
     * @param position the side of the association end on the attached classifier view
     */
    @SuppressWarnings("static-method")
    private void moveMultiplicity(RamTextComponent textView, Vector3D currentPosition, Position position) {
        float x = currentPosition.getX();
        float y = currentPosition.getY();

        // gladly changing the anchor of the text area is sufficient in most cases
        switch (position) {
            case BOTTOM:
                textView.setAnchor(PositionAnchor.UPPER_RIGHT);
                x += TEXT_OFFSET_FROM_ASSOCIATION;
                y += TEXT_OFFSET_FROM_CLASSIFIER;
                break;
            case TOP:
                textView.setAnchor(PositionAnchor.UPPER_RIGHT);
                x -= TEXT_OFFSET_FROM_ASSOCIATION;
                y -= TEXT_OFFSET_FROM_CLASSIFIER;
                break;
            case LEFT:
                textView.setAnchor(PositionAnchor.UPPER_RIGHT);
                x -= TEXT_OFFSET_FROM_CLASSIFIER;
                y += TEXT_OFFSET_FROM_ASSOCIATION;
                break;
            case RIGHT:
                textView.setAnchor(PositionAnchor.UPPER_LEFT);
                x += TEXT_OFFSET_FROM_CLASSIFIER;
                y += TEXT_OFFSET_FROM_ASSOCIATION;
                break;
        }

        textView.setPositionRelativeToParent(new Vector3D(x, y));
    }

    /**
     * Creates a view for the role name.
     *
     * @param associationEnd the end a role name view to create for
     * @return a view for the role name
     */
    private TextView createRoleNameView(AssociationEnd associationEnd) {
        TextView roleName = new TextView(associationEnd, CdmPackage.Literals.NAMED_ELEMENT__NAME);
        // In rare cases the role name could be empty (issue #72).
        roleName.setPlaceholderText(Strings.PH_ENTER_ROLE_NAME);

        ITextViewHandler roleNameHandler = ClassDiagramHandlerFactory.INSTANCE.getAssociationRoleNameHandler();
        registerTextViewProcessors(roleName, roleNameHandler, true);

        roleName.setFont(Fonts.getSmallFontByColor(drawColor));
        roleName.setUnderlined(associationEnd.isStatic());
        roleName.setBufferSize(Cardinal.SOUTH, 0);
        addChild(roleName);

        return roleName;
    }

    /**
     * Creates a view for the multiplicity.
     *
     * @param associationEnd the end a multiplicity view to create for
     * @return a view for the multiplicity
     */
    private MultiplicityTextView createMultiplicityView(AssociationEnd associationEnd) {
        MultiplicityTextView multiplicity = new MultiplicityTextView(associationEnd);

        ITextViewHandler multiplicityHandler = ClassDiagramHandlerFactory.INSTANCE.getAssociationMultiplicityHandler();
        registerTextViewProcessors(multiplicity, multiplicityHandler, false);

        multiplicity.setFont(Fonts.getSmallFontByColor(drawColor));
        multiplicity.setBufferSize(Cardinal.SOUTH, 0);
        addChild(multiplicity);

        return multiplicity;
    }

    @Override
    public void destroy() {
        EMFEditUtil.removeListenerFor(getFromEnd().getModel(), this);
        super.destroy();
    }

    @Override
    public void notifyChanged(Notification notification) {
        if (notification.getNotifier() == getFromEnd().getModel()
                || notification.getNotifier() == getToEnd().getModel()) {
            if (notification.getFeature() == CdmPackage.Literals.STRUCTURAL_FEATURE__STATIC) {
                if (notification.getEventType() == Notification.SET) {
                    boolean newValue = notification.getNewBooleanValue();
                    if (notification.getNotifier() == getToEnd().getModel()) {
                        toEndRolename.setUnderlined(newValue);
                    }
                }
            }
        }
    }

    /**
     * Adds listeners to the textview parameter.
     *
     * @param textView The field where the listeners occurred
     * @param handler The text view handler to add
     * @param enabledTapAndHold If the tapAndHold event is enabled for this textview
     */
    private void registerTextViewProcessors(TextView textView, ITextViewHandler handler, boolean enabledTapAndHold) {

        textView.registerTapProcessor(handler);

        if (enabledTapAndHold) {
            textView.registerInputProcessor(new TapAndHoldProcessor(RamApp.getApplication(),
                    GUIConstants.TAP_AND_HOLD_DURATION));
            textView.addGestureListener(TapAndHoldProcessor.class,
                    new TapAndHoldVisualizer(RamApp.getApplication(), this));
            textView.addGestureListener(TapAndHoldProcessor.class, handler);
        }
    }

    /**
     * Redraws the lines between the to and from {@link AssociationEnd}s.
     */
    @Override
    protected void update() {
        // Calculate the offset required to move the lines and update the location of each end accordingly.
        Vector3D fromEndOffset = new Vector3D(0, 0);
        Vector3D toEndOffset = new Vector3D(0, 0);
        fromEnd.getLocation().addLocal(fromEndOffset);
        toEnd.getLocation().addLocal(toEndOffset);
        drawAllLines();
        
        createText(getToEnd());

        moveText(getToEnd(), toEndRolename, toEndMultiplicity, lastFromPosition);

        lastFromPosition = getToEnd().getPosition();

        if (toEndRolename != null && toEndMultiplicity != null) {
            toEndRolename.setVisible(true);
            toEndMultiplicity.setVisible(true);
        }
        
        if (getToEnd().getComponentView() instanceof ClassifierView) {
            ((ClassifierView<?>) getToEnd().getComponentView()).updateSpacerSize();
        }
    }
}
