package ca.mcgill.sel.classdiagram.ui.views.handler.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;

import ca.mcgill.sel.classdiagram.AssociationEnd;
import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.classdiagram.util.CdmModelUtil;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.perspective.COREPerspectiveUtil;
import ca.mcgill.sel.core.perspective.PerspectiveName;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.menu.RamLinkedMenu;
import ca.mcgill.sel.ram.ui.perspective.PerspectiveControllerFactory;
import ca.mcgill.sel.ram.ui.perspective.classdiagram.CdmPerspectiveController;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.ILinkedMenuListener;
import ca.mcgill.sel.ram.ui.views.handler.impl.TextViewHandler;
import ca.mcgill.sel.ram.util.MetamodelRegex;

/**
 * Handler for the multiplicity of an association. Handles the validation of multiplicity input and setting of
 * multiplicity to the model in the form of upper and lower bound.
 *
 * @author eyildirim
 * @author mschoettle
 * @author yhattab
 */
public class AssociationMultiplicityHandler extends TextViewHandler implements ILinkedMenuListener {

    /**
     * The index of the upper bound in the multiplicity string.
     */
    private static final int UPPER_BOUND_INDEX = 3;

    private static final String ACTION_TOGGLE_ORDERING = "view.association.ordering.toggle";
    private static final String ACTION_TOGGLE_UNIQUENESS = "view.association.uniqueness.toggle";

    @Override
    public void keyboardOpened(TextView textView) {
        // do nothing
    }

    @Override
    public boolean processTapEvent(TapEvent tapEvent) {
        if (tapEvent.isDoubleTap()) {
            TextView target = (TextView) tapEvent.getTarget();

            target.showKeyboard();
            return true;
        }

        return true;
    }

    /**
     * Set the multiplicity of the association end.
     * 
     * @param multiplicityString the string modified by user
     * @param associationEnd the association end being modified
     * @return true if the multiplicity string is valid
     */
    private static boolean setMultiplicity(String multiplicityString, AssociationEnd associationEnd) {
        // For example: 0..* --> group(0) = 0..*, group(1) = 0, group(2)= .. ,group(3) = *
        Matcher matcher = Pattern.compile(MetamodelRegex.REGEX_GROUP_MULTIPLICITY).matcher(multiplicityString);
        if (!matcher.matches()) {
            return false;
        }

        // it may be null because allowed inputs can be : number OR number..number OR number..*
        String upperBoundString = matcher.group(UPPER_BOUND_INDEX);
        int upperBound = Integer.MIN_VALUE;
        int lowerBound = Integer.parseInt(matcher.group(1));
        CdmPerspectiveController cdmPerspectiveController = 
                PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController();
        ClassDiagram cd = (ClassDiagram) EcoreUtil.getRootContainer(associationEnd);
        COREPerspective perspective = COREPerspectiveUtil.INSTANCE.getModelPerspective(cd);

        // upper and lower bounds have same value
        if (upperBoundString == null) {
            switch (perspective.getName()) {
                case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE: 
                case PerspectiveName.DESIGN_MODEL_PERSPECTIVE: 
                    cdmPerspectiveController.setMultiplicity(cd, associationEnd, lowerBound, upperBound);
                    cdmPerspectiveController.setRoleName(cd, associationEnd, CdmModelUtil.singularizeRoleName(
                            associationEnd.getName(),
                            associationEnd.getClassifier().getName()));
                    break;
            }  
            
        } else {
            if ("*".equals(upperBoundString)) {
                upperBound = -1;
            } else {
                upperBound = Integer.parseInt(upperBoundString);
                if (upperBound < lowerBound) {
                    return false;
                }
            }
            switch (perspective.getName()) {
                case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE: 
                case PerspectiveName.DESIGN_MODEL_PERSPECTIVE: 
                    cdmPerspectiveController.setMultiplicity(cd, associationEnd, lowerBound, upperBound);
                    break;
            }  
        }

        // change default name if we changed to singular or plural
        String roleName = associationEnd.getName();
        String classifierName = associationEnd.getClassifier().getName();
        if (associationEnd.getOppositeEnd() != null) {
            // for non nary associations, default name is given for opposite end classifier
            classifierName = associationEnd.getOppositeEnd().getClassifier().getName();
        }

        String newRoleName = CdmModelUtil.pluralizeRoleName(roleName, classifierName);
        if (upperBoundString == null) {
            if (lowerBound == 1) {
                newRoleName = CdmModelUtil.singularizeRoleName(roleName, classifierName);
            }
        } else if (upperBound == 1) {
            newRoleName = CdmModelUtil.singularizeRoleName(roleName, classifierName);
        }

        if (!newRoleName.equals(roleName)) {
            switch (perspective.getName()) {
                case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE: 
                case PerspectiveName.DESIGN_MODEL_PERSPECTIVE: 
                    cdmPerspectiveController.setRoleName(cd, associationEnd, newRoleName);
                    break;
            } 
        }

        return true;
    }

    @Override
    public boolean shouldDismissKeyboard(TextView textView) {
        String text = textView.getText();

        if (!text.matches(MetamodelRegex.REGEX_MULTIPLICITY)) {
            return false;
        }

        return setMultiplicity(text, (AssociationEnd) textView.getData());
    }

    @Override
    public void initMenu(RamLinkedMenu menu) {
        menu.addAction(Strings.MENU_TOGGLE_ORDERING, Icons.ICON_MENU_TOGGLE_ORDERING,
                ACTION_TOGGLE_ORDERING, this, true);
        menu.addAction(Strings.MENU_TOGGLE_UNIQUENESS, Icons.ICON_MENU_TOGGLE_UNIQUENESS,
                ACTION_TOGGLE_UNIQUENESS, this, true);

    }

    @Override
    public void actionPerformed(ActionEvent event) {
        String actionCommand = event.getActionCommand();
        RamRectangleComponent pressedButton = (RamRectangleComponent) event.getTarget();
        RamLinkedMenu linkedMenu = (RamLinkedMenu) pressedButton.getParentOfType(RamLinkedMenu.class);
        if (linkedMenu != null) {
            TextView represented = (TextView) linkedMenu.getLinkedView();
            AssociationEnd associationEnd = (AssociationEnd) represented.getData();

            COREPerspective perspective = COREPerspectiveUtil.INSTANCE.getModelPerspective(associationEnd);
            
            if (ACTION_TOGGLE_ORDERING.equals(actionCommand)) {
                switch (perspective.getName()) {
                    case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE: 
                    case PerspectiveName.DESIGN_MODEL_PERSPECTIVE: 
                        PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController()
                            .switchOrdering(associationEnd);
                        break;
                } 
            }
            if (ACTION_TOGGLE_UNIQUENESS.equals(actionCommand)) {
                switch (perspective.getName()) {
                    case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE: 
                    case PerspectiveName.DESIGN_MODEL_PERSPECTIVE: 
                        PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController()
                            .switchUniqueness(associationEnd);
                        break;
                }  
            }
        }

    }

    @Override
    public EObject getEobject(RamRectangleComponent rectangle) {
        return ((TextView) rectangle).getData();
    }

    @Override
    public List<EObject> getEObjectToListenForUpdateMenu(RamRectangleComponent rectangle) {
        ArrayList<EObject> ret = new ArrayList<EObject>();
        ret.add(((TextView) rectangle).getData());
        return ret;
    }

    @Override
    public void updateMenu(RamLinkedMenu menu, Notification notification) {
    }

    @Override
    public RamRectangleComponent getVisualLinkedComponent(RamRectangleComponent link) {
        return link;
    }
}
