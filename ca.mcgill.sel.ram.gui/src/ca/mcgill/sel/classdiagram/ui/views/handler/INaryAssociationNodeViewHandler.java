package ca.mcgill.sel.classdiagram.ui.views.handler;

import ca.mcgill.sel.classdiagram.ui.views.NaryAssociationNodeView;

/**
 * The implementation of this interface can handle events for a {@link NaryAssociationNodeView}.
 * 
 * @author yhattab
 */
public interface INaryAssociationNodeViewHandler extends IBaseViewHandler {

    /**
     * Create a new association class for that nary association.
     * 
     * @param associationView The NaryAssociationNodeView to which an association class should be added.
     */
    void createAssociationClass(NaryAssociationNodeView associationView);
    
    /**
     * Removes the association class for that nary association.
     * 
     * @param associationView The NaryAssociationNodeView from which an association class should be removed.
     */
    void removeAssociationClass(NaryAssociationNodeView associationView);

}
