package ca.mcgill.sel.classdiagram.ui.views;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.mt4j.components.TransformSpace;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.classdiagram.Attribute;
import ca.mcgill.sel.classdiagram.CdmPackage;
import ca.mcgill.sel.classdiagram.Classifier;
import ca.mcgill.sel.classdiagram.LayoutElement;
import ca.mcgill.sel.classdiagram.NamedElement;
import ca.mcgill.sel.classdiagram.Operation;
import ca.mcgill.sel.classdiagram.ui.views.CDEnd.Position;
import ca.mcgill.sel.classdiagram.ui.views.handler.ClassDiagramHandlerFactory;
import ca.mcgill.sel.classdiagram.ui.views.handler.IClassifierViewHandler;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.perspective.COREPerspectiveUtil;
import ca.mcgill.sel.core.util.COREModelUtil;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent.Alignment;
import ca.mcgill.sel.ram.ui.layouts.VerticalLayout;
import ca.mcgill.sel.ram.ui.perspective.PerspectiveControllerFactory;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Fonts;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.TextView;


/**
 * This view acts as a super class for all views that represent a sub-class of {@link Classifier}. It contains the basic
 * structure of a class and supports operations and relationships. Sub-classes may override the build methods in order
 * to change the visual appearance.
 *
 * @param <H> the handler interface that this component uses
 * @author mschoettle
 * @author yhattab
 */
public abstract class ClassifierView<H extends IClassifierViewHandler> extends LinkableView<H> {

    /**
     * Bottom buffer for attribute and operation container in order to prevent a static attribute or operation to be not
     * visible.
     */
    protected static final float BUFFER_CONTAINER_BOTTOM = 4f;

    /**
     * The minimum width of the view.
     */
    protected static final float MINIMUM_WIDTH = 150f;

    /**
     * The default icon size.
     */
    protected static final int ICON_SIZE = Fonts.DEFAULT_FONT.getFontAbsoluteHeight() + 2
            * RamTextComponent.DEFAULT_BUFFER_SIZE;

    /**
     * The buffer on the bottom (south) for each child).
     */
    protected static final int BUFFER_BOTTOM = 0;

    /**
     * The view container for attributes.
     */
    protected RamRectangleComponent attributesContainer;

    /**
     * The view container for operations.
     */
    protected RamRectangleComponent operationsContainer;

    /**
     * The view container for operations and the spacer.
     */
    protected RamRectangleComponent operationsAndSpacerContainer;
   
    /**
     * The spacer.
     */
    protected RamRectangleComponent classifierviewSpacer;

    /**
     * The minimum number of vertical slots needed so that the text on the association
     * ends coming into this class that are navigable are nicely readable.
     */
    protected int previousVerticalSlotsNeeded;

    /**
     * The minimum number of horizontal slots needed so that the text on the association
     * ends coming into this class that are navigable are nicely readable.
     */
    protected int previousHorizontalSlotsNeeded;

    /**
     * The map of operations to their views.
     */
    protected Map<Operation, OperationView> operations;

    /**
     * The map of attributes to their views.
     */
    protected Map<Attribute, AttributeView> attributes;

    /**
     * Whether operations can be edited.
     */
    protected boolean editableOperations;

    /**
     * TextView used to display the visibility.
     */
    protected TextView visibilityField;

    /**
     * Creates a new {@link ClassifierView} for a given classifier and layout element.
     *
     * @param structuralDiagramView the {@link ClassDiagramView} that is the parent of this view
     * @param classifier the {@link Classifier} this view represents
     * @param layoutElement the {@link LayoutElement} that contains the layout information for this classifier
     * @param editableOperations true, if operations can be edited, false otherwise
     */
    protected ClassifierView(ClassDiagramView structuralDiagramView, Classifier classifier,
            LayoutElement layoutElement, boolean editableOperations) {
        super(structuralDiagramView, classifier, layoutElement);
        
        this.editableOperations = editableOperations;

        setEnabled(true);
        setNoFill(false);
        setNoStroke(false);

        // build components
        build(classifier);

        // now init based on the class
        initializeClass(classifier);
        updateStyle();

        visibilityField.setHandler(ClassDiagramHandlerFactory.INSTANCE.getClassifierVisibilityHandler());
        
        setNameItalic(classifier.isAbstract());
    }
    
    /**
     * Creates a new {@link ClassifierView} for a given classifier and layout element. The operations are editable.
     *
     * @param structuralDiagramView the {@link ClassDiagramView} that is the parent of this view
     * @param classifier the {@link Classifier} this view represents
     * @param layoutElement the {@link LayoutElement} that contains the layout information for this classifier
     */
    protected ClassifierView(ClassDiagramView structuralDiagramView, Classifier classifier,
            LayoutElement layoutElement) {
        this(structuralDiagramView, classifier, layoutElement, true);
    }

    /**
     * Adds a new operation view for the given operation to this view at the given index. The index is an index inside
     * the {@link #operationsContainer}.
     *
     * @param index the index inside the operations container where this operation should be added to
     * @param operation the operation to add
     */
    protected void addOperationField(int index, Operation operation) {
        // Ensure that operation field is added at the end of the operations container and not at an
        // index out of bound , there might be more operations in the class than in the
        // operations container.
        int cleanedIndex = Math.min(index, operationsContainer.getChildCount());
        
        OperationView operationView = new OperationView(this, operation, editableOperations);
        operations.put(operation, operationView);

        operationView.setHandler(ClassDiagramHandlerFactory.INSTANCE.getOperationViewHandler());
        
        if (shouldShowOperation(operation)) {
            operationsContainer.addChild(cleanedIndex, operationView);
        }
    }
    
    /**
     * Whether the given operation should be shown in this view.
     * The default implementation always returns true, subclasses may override this behaviour.
     * 
     * @param operation the {@link Operation}
     * @return true, if the given operation should be shown, false otherwise
     */
    protected boolean shouldShowOperation(Operation operation) {
        return true;
    }

    @Override
    public void addRelationshipEndAtPosition(CDEnd<? extends EObject, ? extends RamRectangleComponent> end) {
        super.addRelationshipEndAtPosition(end);
        updateSpacerSize();
    }

    /**
     * Builds this view for the given classifier. Builds the name, attributes and operations.
     *
     * @param classifier the classifier for this view
     */
    protected void build(Classifier classifier) {
        setMinimumWidth(MINIMUM_WIDTH);

        // Creating the visibility field
        visibilityField = new TextView(classifier, CdmPackage.Literals.CLASSIFIER__VISIBILITY);
        visibilityField.setBufferSize(Cardinal.WEST, BUFFER_BOTTOM);
        //show visibility only if language actions allow it
        COREPerspective perspective = COREPerspectiveUtil.INSTANCE.getModelPerspective(classifier);
        if (perspective != null) {
            if (PerspectiveControllerFactory.INSTANCE.getActionValidator().canEditVisibility(perspective)) {
                nameContainer.addChild(visibilityField);
            }
        }
        buildNameField(classifier);
        if (perspective != null) {
            if (PerspectiveControllerFactory.INSTANCE.getActionValidator().canCreateAttribute(perspective)) {
                buildAttributesContainer();
            }
            
            if (PerspectiveControllerFactory.INSTANCE.getActionValidator().canCreateOperation(perspective)) {
                buildOperationsContainer();
            } else {
                //in the case of domain model, hide attributesContainer until if it's empty
                if (classifier.getAttributes().size() == 0 && attributesContainer != null) {
                    removeChild(attributesContainer);
                }
            }
        }
    }

    /**
     * Builds the name field for the given classifier.
     *
     * @param classifier the classifier for this view
     */
    protected void buildNameField(Classifier classifier) {
        TextView classNameField = new TextView(classifier, CdmPackage.Literals.NAMED_ELEMENT__NAME);
        classNameField.setUniqueName(false);
        classNameField.setAlignment(Alignment.CENTER_ALIGN);
        classNameField.setPlaceholderText(Strings.PH_ENTER_CLASS_NAME);
        classNameField.setHandler(ClassDiagramHandlerFactory.INSTANCE.getClassNameHandler());
        setNameField(classNameField);
    }

    /**
     * Builds the empty attributes container.
     */
    protected void buildAttributesContainer() {
        // create and add the attributes field
        attributes = new HashMap<Attribute, AttributeView>();
        attributesContainer = new RamRectangleComponent(new VerticalLayout());
        attributesContainer.setNoStroke(false);
        attributesContainer.setMinimumHeight(ICON_SIZE / 2);
        attributesContainer.setBufferSize(Cardinal.SOUTH, BUFFER_CONTAINER_BOTTOM);
        addChild(attributesContainer);
    }

    /**
     * Builds the empty operations container.
     */
    protected void buildOperationsContainer() {
        operationsAndSpacerContainer = new RamRectangleComponent(new VerticalLayout());
        operationsAndSpacerContainer.setNoStroke(false);
        addChild(operationsAndSpacerContainer);

        operations = new HashMap<Operation, OperationView>();
        operationsContainer = new RamRectangleComponent(new VerticalLayout());
        operationsContainer.setNoStroke(true);
        operationsContainer.setMinimumHeight(ICON_SIZE);
        operationsContainer.setBufferSize(Cardinal.SOUTH, BUFFER_CONTAINER_BOTTOM);
        operationsAndSpacerContainer.addChild(operationsContainer);
        
        classifierviewSpacer = new RamRectangleComponent();
        classifierviewSpacer.setNoFill(false);
        classifierviewSpacer.setLayout(new VerticalLayout());
        classifierviewSpacer.setAutoMinimizes(false);
        operationsAndSpacerContainer.addChild(classifierviewSpacer);
    }

    @Override
    public void destroy() {
        // destroy relationships
        for (RelationshipView<?, ?> view : getAllRelationshipViews()) {
            view.destroy();
        }

        relationshipEndByPosition.clear();

        // destroy rest
        super.destroy();
    }

    /**
     * Returns the classifier represented by this view.
     *
     * @return the {@link Classifier} represented by this view
     */
    @Override
    public Classifier getClassifier() {
        return (Classifier) represented;
    }

    /**
     * Returns the component containing all {@link OperationView}s.
     *
     * @return the {@link RamRectangleComponent} containing all {@link OperationView}s
     */
    public RamRectangleComponent getOperationsContainer() {
        return operationsContainer;
    }

    /**
     * Returns the {@link ClassDiagramView} which contains this view.
     *
     * @return the {@link ClassDiagramView} (parent of this view)
     */
    @Override
    public ClassDiagramView getClassDiagramView() {
        return classDiagramView;
    }

    /**
     * Initializes the view for the given classifier.
     *
     * @param classifier the classifier represented by this view
     */
    protected abstract void initializeClass(Classifier classifier);

    /**
     * Adds operation views to the view for each operation of the given classifier.
     *
     * @param classifier the classifier for which to add operation views for
     */
    protected void initializeOperations(Classifier classifier) {
        if (this.operationsContainer != null) {
            for (int i = 0; i < classifier.getOperations().size(); i++) {
                Operation operation = classifier.getOperations().get(i);
                addOperationField(i, operation);
            }
        }
    }
    
    @Override
    public void notifyChanged(Notification notification) {
        super.notifyChanged(notification);
        Classifier classifier = (Classifier) represented;
        if (notification.getNotifier() == classifier) {
            if (notification.getFeature() == CdmPackage.Literals.CLASSIFIER__OPERATIONS) {
                switch (notification.getEventType()) {
                    case Notification.REMOVE:
                        Operation operation = (Operation) notification.getOldValue();
                        removeOperation(operation);
                        break;
                    case Notification.ADD:
                        operation = (Operation) notification.getNewValue();
                        addOperationField(notification.getPosition(), operation);
                        break;
                    case Notification.MOVE:
                        operation = (Operation) notification.getNewValue();
                        setOperationPosition(operation, notification.getPosition());
                        break;    
                }
            } else if (notification.getFeature() == CdmPackage.Literals.CLASSIFIER__SUPER_TYPES) {
                switch (notification.getEventType()) {
                    case Notification.REMOVE:
                        Classifier superType = (Classifier) notification.getOldValue();
                        classDiagramView.removeInheritanceView(classifier, superType);
                        break;
                    case Notification.ADD:
                        superType = (Classifier) notification.getNewValue();
                        classDiagramView.addInheritanceView(classifier, superType);
                        break;
                } 
            } else if (notification.getFeature() == CdmPackage.Literals.CLASSIFIER__ABSTRACT) {
                boolean italic = notification.getNewBooleanValue();
                setNameItalic(italic);
            } else if (notification.getFeature() == CdmPackage.Literals.CLASSIFIER__ATTRIBUTES) {
                switch (notification.getEventType()) {
                    case Notification.MOVE:
                        Attribute attribute = (Attribute) notification.getNewValue();
                        setAttributePosition(attribute, notification.getPosition());
                        break;
                }
            }
        }
    }
   
    /**
     * Set the class name in italic.
     *
     * @param italic true if you want to set the class name in italic. false otherwise.
     */
    private void setNameItalic(boolean italic) {
        if (COREModelUtil.isReference(represented)) {
            if (italic) {
                nameField.setFont(Fonts.FONT_CLASS_NAME_REFERENCE_ITALIC);                
            } else {
                nameField.setFont(Fonts.FONT_CLASS_NAME_REFERENCE);
            }
        } else {
            if (italic) {
                nameField.setFont(Fonts.FONT_CLASS_NAME_ITALIC);                
            } else {
                nameField.setFont(Fonts.FONT_CLASS_NAME);
            }
        }
    }

    /**
     * Removes the view of the given operation from this view.
     *
     * @param operation the operation to remove
     */
    protected void removeOperation(final Operation operation) {
        if (operations.containsKey(operation)) {
            RamApp.getApplication().invokeLater(new Runnable() {

                @Override
                public void run() {
                    OperationView operationView = operations.remove(operation);

                    operationsContainer.removeChild(operationView);
                    operationView.destroy();
                }
            });
        }
    }
    
    public void setOperationPosition(Operation operation, int index) {
        RamApp.getApplication().invokeLater(new Runnable() {
            @Override
            public void run() {
                OperationView operationView = operations.get(operation);

                operationsContainer.removeChild(operationView);
                operationsContainer.addChild(index, operationView);
            }
        });
    }
    
    public void setAttributePosition(Attribute attribute, int index) {
        RamApp.getApplication().invokeLater(new Runnable() {
            @Override
            public void run() {
                AttributeView attributeView = attributes.get(attribute);

                attributesContainer.removeChild(attributeView);
                attributesContainer.addChild(index, attributeView);
            }
        });
    }

    @Override
    public void removeRelationshipEnd(CDEnd<? extends NamedElement, ? extends RamRectangleComponent> end) {
        super.removeRelationshipEnd(end);
        updateSpacerSize();
    }

    @Override
    public void scale(float x, float y, float z, Vector3D scalingPoint, TransformSpace transformSpace) {
        super.scale(x, y, z, scalingPoint, transformSpace);
    }

    /**
     * Determines how many operations are visible in the operation container.
     *
     * @return the number of visible opersations
     */
    protected int getNumberOfVisibleOperations() {
        int result = 0;
        for (Operation o : operations.keySet()) {
            if (shouldShowOperation(o)) {
                result++;
            }
        }
        return result;
    }

    /**
     * Calculates the new size of the spacer and sets the size in order to ensure that the association
     * end role names and multiplicities do not overlap.
     */
    void updateSpacerSize() {
        List<CDEnd<?, ?>> leftList = relationshipEndByPosition.get(Position.LEFT);
        List<CDEnd<?, ?>> rightList = relationshipEndByPosition.get(Position.RIGHT);            
    
        int leftSlots = 0;
        if (leftList.size() > 1) {
            leftSlots = calculateNumberOfSlots(leftList, Position.LEFT);
        }
        int rightSlots = 0;
        if (rightList.size() > 1) {
            rightSlots = calculateNumberOfSlots(rightList, Position.RIGHT);
        }
    
        int verticalSlotsNeeded = Math.max(leftSlots, rightSlots);
        boolean sizeChanged = false;
                                
        if (verticalSlotsNeeded != previousVerticalSlotsNeeded 
                && classifierviewSpacer != null && attributesContainer != null) {
            previousVerticalSlotsNeeded = verticalSlotsNeeded;
            float baseHeight = nameContainer.getHeight() + attributesContainer.getHeight()
                + attributesContainer.getBufferSize(Cardinal.SOUTH)
                + attributesContainer.getBufferSize(Cardinal.NORTH)
                + operationsContainer.getHeight();
            float desiredHeight = verticalSlotsNeeded * (Fonts.FONTSIZE_ASSOCIATION_TEXT
                    + AssociationView.TEXT_OFFSET_FROM_ASSOCIATION / 2);
            float newHeight = 0;
            
            if (verticalSlotsNeeded > 0) {
                newHeight = desiredHeight - baseHeight;
            }
            
            classifierviewSpacer.setMinimumHeight(newHeight);
            sizeChanged = true;
        }
        
        List<CDEnd<?, ?>> topList = relationshipEndByPosition.get(Position.TOP);
        List<CDEnd<?, ?>> bottomList = relationshipEndByPosition.get(Position.BOTTOM);            
    
        int topSlots = 0;
        if (topList.size() > 1) {
            topSlots = calculateNumberOfSlots(topList, Position.TOP);
        }
        
        int bottomSlots = 0;
        if (bottomList.size() > 1) {
            bottomSlots = calculateNumberOfSlots(bottomList, Position.BOTTOM);
        }
    
        int horizontalSlotsNeeded = Math.max(topSlots, bottomSlots);
        
        if (horizontalSlotsNeeded != previousHorizontalSlotsNeeded && classifierviewSpacer != null) {
            previousHorizontalSlotsNeeded = horizontalSlotsNeeded;
            float newWidth = horizontalSlotsNeeded * (Fonts.FONTSIZE_ASSOCIATION_TEXT
                    + AssociationView.TEXT_OFFSET_FROM_ASSOCIATION / 2);
            classifierviewSpacer.setMinimumWidth(newWidth);
            sizeChanged = true;
        }
        
        if (sizeChanged && classifierviewSpacer != null) {
            classifierviewSpacer.updateParent();
        }
    }
    
    @Override
    public void setSizeLocal(float width, float height) {
        super.setSizeLocal(width, height);
        updateRelationships();
    }

    @Override
    public void translate(Vector3D dirVect) {
        super.translate(dirVect);
        updateRelationships();
    }

    /**
     * Updates the style of this view depending on whether the classifier is a reference or not.
     */
    @Override
    protected void updateStyle() {
        Classifier classifier = (Classifier) represented;
        if (COREModelUtil.isReference(classifier)) {
            setFillColor(Colors.CLASS_VIEW_REFERENCE_FILL_COLOR);
            setStrokeColor(Colors.CLASS_VIEW_REFERENCE_STROKE_COLOR);
            visibilityField.setFont(Fonts.REFERENCE_FONT);
        } else {
            setFillColor(Colors.CLASS_VIEW_DEFAULT_FILL_COLOR);
            setStrokeColor(Colors.CLASS_VIEW_DEFAULT_STROKE_COLOR);
            visibilityField.setFont(Fonts.DEFAULT_FONT);
        }
        
        setNameItalic(classifier.isAbstract());
    }

}
