package ca.mcgill.sel.classdiagram.ui.views;

import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.classdiagram.AssociationEnd;
import ca.mcgill.sel.classdiagram.CdmPackage;
import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.perspective.COREPerspectiveUtil;
import ca.mcgill.sel.core.perspective.PerspectiveName;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamSelectorComponent;
import ca.mcgill.sel.ram.ui.components.listeners.AbstractDefaultRamSelectorListener;
import ca.mcgill.sel.ram.ui.perspective.PerspectiveControllerFactory;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Fonts;
import ca.mcgill.sel.ram.ui.views.TextView;

/**
 * View to show and Association End Qualifier.
 * @author yhattab
 *
 */
public class QualifierView extends TextView {

    private AssociationEnd associationEnd;
    
    /** 
     * Builds view for the qualifier.
     * @param associationEnd Association End to which the qualifier belongs
     */
    public QualifierView(AssociationEnd associationEnd) {
        super(associationEnd, CdmPackage.Literals.ASSOCIATION_END__QUALIFIER);
        
        this.associationEnd = associationEnd;

        setNoStroke(false);
        setNoFill(false);
        setFillColor(Colors.STRUCT_KEY_SELECTION_FILL_COLOR);
        setPlaceholderFont(Fonts.ASSOCIATION_PLACEHOLDER_FONT);
        setBufferSize(Cardinal.SOUTH, 0);
        
    }

    @Override
    protected Object getModelObject() {
        if (getData() instanceof AssociationEnd) {
            return ((AssociationEnd) getData()).getQualifier();
        }

        return super.getModelObject();
    }
    
    @Override
    public void updateText() {
        super.updateText();
    }

    @Override
    protected void updateTextView(Object oldValue, Object newValue) {
        super.updateTextView(oldValue, newValue);
    }
    
    /**
     * Shows selector for qualifier type and then sets it after one is selected.
     * @param associationEnd {@link AssociationEnd} to which a qualifier will be added 
     * @param locationForSelector location on screen to show the type selector
     */
    public static void selectQualifier(AssociationEnd associationEnd, Vector3D locationForSelector) {
        final ClassDiagram classDiagram = EMFModelUtil.getRootContainerOfType(associationEnd, 
                CdmPackage.Literals.CLASS_DIAGRAM);
        final QualifierSelectorView qualifierSelector = new QualifierSelectorView(classDiagram);
        
        RamApp.getActiveScene().addComponent(qualifierSelector, locationForSelector);

        qualifierSelector.registerListener(new AbstractDefaultRamSelectorListener<String>() {
            @Override
            public void elementSelected(RamSelectorComponent<String> selector, 
                    String element) {
                COREPerspective perspective = COREPerspectiveUtil.INSTANCE.getModelPerspective(associationEnd);
                switch (perspective.getName()) {
                    case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE: 
                        PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController()
                            .setQualifier(associationEnd, 
                                ((QualifierSelectorView) selector).getType(element));
                        break;
                    case PerspectiveName.DESIGN_MODEL_PERSPECTIVE: 
                        PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController()
                            .setQualifier(associationEnd, 
                                ((QualifierSelectorView) selector).getType(element));
                        break;
                } 
            }
        });
    }

    /**
     * Returns the association end this view is attached to.
     * @return {@link AssociationEnd} object whose qualifier this view represents
     */
    public AssociationEnd getAssociationEnd() {
        return associationEnd;
    }
}
