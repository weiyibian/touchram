package ca.mcgill.sel.classdiagram.ui.views.handler.impl;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;

import ca.mcgill.sel.classdiagram.AssociationEnd;
import ca.mcgill.sel.classdiagram.Type;
import ca.mcgill.sel.classdiagram.ui.views.QualifierView;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.perspective.COREPerspectiveUtil;
import ca.mcgill.sel.core.perspective.PerspectiveName;
import ca.mcgill.sel.ram.ui.perspective.PerspectiveControllerFactory;
import ca.mcgill.sel.ram.ui.views.handler.impl.TextViewHandler;

/**
 * The default handler for a Qualifier Text View. It allows selecting another qualifier type
 * 
 * @author yhattab
 * */

public class QualifierViewHandler extends TextViewHandler {
    
    /**
     * Sets the value of the text.
     * 
     * @param data the object containing the feature
     * @param feature the feature of which the value should be changed
     * @param value the new value of the feature
     */
    @Override
    protected void setValue(EObject data, EStructuralFeature feature, Object value) {
        COREPerspective perspective = COREPerspectiveUtil.INSTANCE.getModelPerspective((AssociationEnd) feature);
        switch (perspective.getName()) {
            case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE: 
                PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController()
                    .setQualifier((AssociationEnd) feature, 
                        (Type) value);
                break;
            case PerspectiveName.DESIGN_MODEL_PERSPECTIVE: 
                PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController()
                    .setQualifier((AssociationEnd) feature, 
                        (Type) value);
                break;
        } 
    }

    @Override
    public boolean processTapEvent(TapEvent tapEvent) {
        if (tapEvent.isDoubleTap()) {
            QualifierView qualifierView = (QualifierView) tapEvent.getTarget();
            QualifierView.selectQualifier(qualifierView.getAssociationEnd(), tapEvent.getLocationOnScreen());
            return true;
        }

        return false;
    }
}
