package ca.mcgill.sel.classdiagram.ui.views;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.edit.provider.INotifyChangedListener;
import org.mt4j.components.MTComponent;
import org.mt4j.util.font.IFont;

import ca.mcgill.sel.classdiagram.Attribute;
import ca.mcgill.sel.classdiagram.CdmPackage;
import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.classdiagram.ui.views.handler.ClassDiagramHandlerFactory;
import ca.mcgill.sel.classdiagram.ui.views.handler.IAttributeViewHandler;
import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.perspective.COREPerspectiveUtil;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.core.util.COREModelUtil;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.layouts.HorizontalLayoutVerticallyCentered;
import ca.mcgill.sel.ram.ui.perspective.PerspectiveControllerFactory;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Fonts;
import ca.mcgill.sel.ram.ui.utils.GraphicalUpdater;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.IHandled;


/**
 * Shows the type and name of an attribute. Displays the visibility of an attribute always as private.
 * 
 * @author mschoettle
 * @author yhattab
 */
public class AttributeView extends RamRectangleComponent implements INotifyChangedListener,
        IHandled<IAttributeViewHandler> {

    private RamTextComponent visibilityField;
    private TextView nameField;
    private TextView typeField;

    private Attribute attribute;

    private GraphicalUpdater graphicalUpdater;

    private IAttributeViewHandler handler;
    
    private COREArtefact artefact;

    /**
     * Creates an attribute view for the given {@link Attribute}.
     * 
     * @param classView
     *            the {@link ClassView} which contains this view.
     * @param attribute
     *            the {@link Attribute} that should be represented
     */
    public AttributeView(ClassView classView, Attribute attribute) {
        this.attribute = attribute;
        
        visibilityField = new RamTextComponent(Strings.SYMBOL_PROTECTED);
        visibilityField.setBufferSize(Cardinal.SOUTH, 0);
        //show visibility only if language actions allow it
        if (PerspectiveControllerFactory.INSTANCE.getActionValidator()
                .canEditVisibility(COREPerspectiveUtil.INSTANCE.getModelPerspective(attribute))) {
            addChild(visibilityField);
        }
        

        typeField = new TextView(attribute, CdmPackage.Literals.TYPED_ELEMENT__TYPE);
        typeField.setBufferSize(Cardinal.SOUTH, 0);
        typeField.setBufferSize(Cardinal.WEST, 4);
        addChild(typeField);
        typeField.setHandler(ClassDiagramHandlerFactory.INSTANCE.getTextViewHandler());

        nameField = new TextView(attribute, CdmPackage.Literals.NAMED_ELEMENT__NAME);
        nameField.setBufferSize(Cardinal.SOUTH, 0);
        nameField.setBufferSize(Cardinal.WEST, 0);
        nameField.setUniqueName(true);
        addChild(nameField);
        nameField.setHandler(ClassDiagramHandlerFactory.INSTANCE.getAttributeNameHandler());

        setLayout(new HorizontalLayoutVerticallyCentered(0));

        setUnderlined(attribute.isStatic());
        updateStyle();

        EMFEditUtil.addListenerFor(attribute, this);
        ClassDiagram cd = EMFModelUtil.getRootContainerOfType(attribute, CdmPackage.Literals.CLASS_DIAGRAM);
        graphicalUpdater = RamApp.getApplication().getGraphicalUpdaterForModel(cd);
        graphicalUpdater.addGUListener(attribute, this);
        
        // Add a listener to listen to any partiality change in the core metamodel
        artefact = COREArtefactUtil.getReferencingExternalArtefact(attribute);
        EMFEditUtil.addListenerFor(artefact, this);
    }
    
    @Override
    public void destroy() {
        super.destroy();

        graphicalUpdater.removeGUListener(attribute, this);
        EMFEditUtil.removeListenerFor(attribute, this);
        
        if (artefact != null) {
            EMFEditUtil.removeListenerFor(artefact, this);
        }
    }

    /**
     * Returns the {@link Attribute} that this view represents.
     * 
     * @return the {@link Attribute} represented by this view
     */
    public Attribute getAttribute() {
        return attribute;
    }

    @Override
    public IAttributeViewHandler getHandler() {
        return handler;
    }

    @Override
    public void notifyChanged(Notification notification) {
        // this gets called for every view (as there is only one item provider)
        // therefore an additional check is necessary
        if (notification.getNotifier() == attribute) {
            if (notification.getFeature() == CdmPackage.Literals.STRUCTURAL_FEATURE__STATIC) {
                if (notification.getEventType() == Notification.SET) {
                    boolean newValue = notification.getNewBooleanValue();

                    setUnderlined(newValue);
                }
            } 
        }
    }

    @Override
    public void setUnderlined(boolean underlined) {
        for (int i = 1; i < getChildCount(); i++) {
            MTComponent child = getChildByIndex(i);
            RamRectangleComponent text = (RamRectangleComponent) child;
            text.setUnderlined(underlined);
        }
    }

    @Override
    public void setHandler(IAttributeViewHandler handler) {
        this.handler = handler;
    }

    /**
     * Returns the name field view.
     * 
     * @return the name field view
     */
    public TextView getNameField() {
        return nameField;
    }

    /**
     * Updates the style of this view depending on whether the attribute is a reference or not.
     */
    @Override
    protected void updateStyle() {
        IFont font;
        
        if (COREModelUtil.isReference(attribute)) {
            font = Fonts.REFERENCE_FONT;
            setNoFill(false);
            setFillColor(Colors.ATTRIBUTE_VIEW_REFERENCE_FILL_COLOR);
        } else {
            font = Fonts.DEFAULT_FONT;
            setNoFill(false);
            setFillColor(Colors.ATTRIBUTE_VIEW_DEFAULT_FILL_COLOR);
        }
        
        visibilityField.setFont(font);
        typeField.setFont(font);
        nameField.setFont(font);
    }
}
