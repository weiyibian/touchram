package ca.mcgill.sel.classdiagram.ui.views.handler.impl;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.classdiagram.Attribute;
import ca.mcgill.sel.classdiagram.CdmPackage;
import ca.mcgill.sel.classdiagram.Classifier;
import ca.mcgill.sel.classdiagram.ui.views.AttributeView;
import ca.mcgill.sel.classdiagram.ui.views.handler.IAttributeViewHandler;
import ca.mcgill.sel.classdiagram.util.CdmModelUtil;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.perspective.COREPerspectiveUtil;
import ca.mcgill.sel.core.perspective.PerspectiveName;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.menu.RamLinkedMenu;
import ca.mcgill.sel.ram.ui.components.navigationbar.NavigationBar;
import ca.mcgill.sel.ram.ui.perspective.PerspectiveControllerFactory;
import ca.mcgill.sel.ram.ui.perspective.classdiagram.CdmPerspectiveController;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.handler.BaseHandler;
import ca.mcgill.sel.ram.ui.views.handler.ILinkedMenuListener;

/**
 * The default handler for an {@link AttributeView}.
 * 
 * @author mschoettle
 * @author yhattab
 */
public class AttributeViewHandler extends BaseHandler implements IAttributeViewHandler, ILinkedMenuListener {

    private static final String ACTION_ATTRIBUTE_REMOVE = "view.attribute.remove";
    private static final String ACTION_ATTRIBUTE_STATIC = "view.attribute.static";
    private static final String ACTION_ATTRIBUTE_GET = "view.attribute.get";
    private static final String ACTION_ATTRIBUTE_SET = "view.attribute.set";

    private static final String SUBMENU_GET_SET = "sub.getset";
    
    private static final String ACTION_ATTRIBUTE_SHIFT_UP = "view.attribute.shiftUp";
    private static final String ACTION_ATTRIBUTE_SHIFT_DOWN = "view.attribute.shiftDown";

    @Override
    public void removeAttribute(AttributeView attributeView) {
        COREPerspective perspective = COREPerspectiveUtil.INSTANCE.getModelPerspective(attributeView.getAttribute());
        switch (perspective.getName()) {
            case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE: 
                PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController().removeAttribute(
                        attributeView.getAttribute());
                break;
            case PerspectiveName.DESIGN_MODEL_PERSPECTIVE: 
                PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController().removeAttribute(
                        attributeView.getAttribute());
                break;
        }
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        String actionCommand = event.getActionCommand();
        RamRectangleComponent pressedButton = (RamRectangleComponent) event.getTarget();
        RamLinkedMenu linkedMenu = (RamLinkedMenu) pressedButton.getParentOfType(RamLinkedMenu.class);
        
        if (linkedMenu != null) {
            AttributeView attributeView = (AttributeView) linkedMenu.getLinkedView();

            if (ACTION_ATTRIBUTE_REMOVE.equals(actionCommand)) {
                removeAttribute(attributeView);
            } else if (ACTION_ATTRIBUTE_STATIC.equals(actionCommand)) {
                setAttributeStatic(attributeView);
            } else if (ACTION_ATTRIBUTE_GET.equals(actionCommand)) {
                generateGetter(attributeView);
            } else if (ACTION_ATTRIBUTE_SET.equals(actionCommand)) {
                generateSetter(attributeView);
            } else if (ACTION_ATTRIBUTE_SHIFT_UP.equals(actionCommand)) {
                shiftAttributeUp(attributeView);
            } else if (ACTION_ATTRIBUTE_SHIFT_DOWN.equals(actionCommand)) {
                shiftAttributeDown(attributeView);
            }
        }
    }
    
    public void shiftAttributeUp(AttributeView attributeView) {
        EList<Attribute> attributes = ((Classifier) attributeView.getAttribute().eContainer()).getAttributes();
        int index = 0;
        
        for (int i = 0; i < attributes.size(); i++) {
            if (attributes.get(i).equals(attributeView.getAttribute())) {
                index = i;
            }
        }
        
        if (index > 0) {
            PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController()
                .setAttributePosition(attributeView.getAttribute(), index - 1);
        }
    }
    
    public void shiftAttributeDown(AttributeView attributeView) {
        EList<Attribute> attributes = ((Classifier) attributeView.getAttribute().eContainer()).getAttributes();
        int index = 0;
        
        for (int i = 0; i < attributes.size(); i++) {
            if (attributes.get(i).equals(attributeView.getAttribute())) {
                index = i;
            }
        }
        
        if (index < attributes.size() - 1) {
            PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController()
                .setAttributePosition(attributeView.getAttribute(), index + 1);
        }
    }

    @Override
    public EObject getEobject(RamRectangleComponent rectangle) {
        return ((AttributeView) rectangle).getAttribute();
    }

    @Override
    public void initMenu(RamLinkedMenu menu) {
        menu.addAction(Strings.MENU_DELETE, Icons.ICON_MENU_TRASH, ACTION_ATTRIBUTE_REMOVE, this, true);
        
        AttributeView attributeView = (AttributeView) menu.getLinkedView();
                
        // add the attribute shift up and down buttons if there is more than 1 attribute
        if (((Classifier) attributeView.getAttribute().eContainer()).getAttributes().size() > 1) {
            menu.addAction(Strings.MENU_ATTRIBUTE_SHIFT_UP,
                    Icons.ICON_MENU_ELEMENT_SHIFT_UP, ACTION_ATTRIBUTE_SHIFT_UP, this, true);
            menu.addAction(Strings.MENU_ATTRIBUTE_SHIFT_DOWN,
                    Icons.ICON_MENU_ELEMENT_SHIFT_DOWN, ACTION_ATTRIBUTE_SHIFT_DOWN, this, true);
        }
        
        COREPerspective perspective = NavigationBar.getInstance().getCurrentPerspective();
        if (PerspectiveControllerFactory.INSTANCE.getActionValidator()
                .canEditStatic(perspective)) {
            menu.addAction(Strings.MENU_STATIC, Strings.MENU_NO_STATIC,
                    Icons.ICON_MENU_STATIC, Icons.ICON_MENU_NOT_STATIC, ACTION_ATTRIBUTE_STATIC, this,
                    true, attributeView.getAttribute().isStatic());
        }

        if (PerspectiveControllerFactory.INSTANCE.getActionValidator()
                .canCreateOperation(perspective)) {
            // sub menu for getter/setter if allowed in Language
            menu.addSubMenu(1, SUBMENU_GET_SET);
            menu.addAction(Strings.MENU_ATTRIBUTE_GETTER, Icons.ICON_MENU_ATTRIBUTE_GETTER, ACTION_ATTRIBUTE_GET, this,
                    SUBMENU_GET_SET, true);
            menu.addAction(Strings.MENU_ATTRIBUTE_SETTER, Icons.ICON_MENU_ATTRIBUTE_SETTER, ACTION_ATTRIBUTE_SET, this,
                    SUBMENU_GET_SET, true);
        }

        updateGetterSetterButton(menu);
        updateStaticButton(menu);
        menu.setLinkInCorners(false);
    }

    @Override
    public void setAttributeStatic(AttributeView attributeView) {
        CdmPerspectiveController cdmController = PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController();
        COREPerspective perspective = COREPerspectiveUtil.INSTANCE.getModelPerspective(attributeView.getAttribute());
        switch (perspective.getName()) {
            case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE: 
                cdmController.switchAttributeStatic(attributeView.getAttribute());
                break;
            case PerspectiveName.DESIGN_MODEL_PERSPECTIVE: 
                cdmController.switchAttributeStatic(attributeView.getAttribute());
                break;
        } 
    }

    @Override
    public void generateGetter(AttributeView attributeView) {
        CdmPerspectiveController cdmController = PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController();
        cdmController.createGetterOperation(attributeView.getAttribute());
    }

    @Override
    public void generateSetter(AttributeView attributeView) {
        CdmPerspectiveController cdmController = PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController();
        cdmController.createSetterOperation(attributeView.getAttribute());
    }

    @Override
    public void updateMenu(RamLinkedMenu menu, Notification notification) {

        if (notification.getEventType() == Notification.ADD || notification.getEventType() == Notification.REMOVE) {
            updateGetterSetterButton(menu);

        } else if (notification.getEventType() == Notification.SET
                || notification.getEventType() == Notification.UNSET) {
            if (notification.getFeature() == CdmPackage.Literals.STRUCTURAL_FEATURE__STATIC) {
                updateStaticButton(menu);
            }
        }
    }

    /**
     * Updates static button inside the menu.
     * 
     * @param menu - the menu which contains the static button.
     */
    private static void updateStaticButton(RamLinkedMenu menu) {
        Attribute attribute = (Attribute) menu.geteObject();
        menu.toggleAction(attribute.isStatic(), ACTION_ATTRIBUTE_STATIC);
    }

    /**
     * Updates getter and setter buttons inside the menu.
     * 
     * @param menu - the menu which contains the getter and setter buttons
     */
    private static void updateGetterSetterButton(RamLinkedMenu menu) {
        Attribute attribute = (Attribute) menu.geteObject();
        // Check if get exists
        menu.enableAction(!CdmModelUtil.isGetterUnique(attribute), ACTION_ATTRIBUTE_GET);
        // Check if set exists
        menu.enableAction(!CdmModelUtil.isSetterUnique(attribute), ACTION_ATTRIBUTE_SET);
    }

    @Override
    public List<EObject> getEObjectToListenForUpdateMenu(RamRectangleComponent rectangle) {
        AttributeView attribute = (AttributeView) rectangle;
        ArrayList<EObject> ret = new ArrayList<EObject>();
        ret.add(attribute.getAttribute().eContainer());
        ret.add(attribute.getAttribute());
        ret.add(COREArtefactUtil.getReferencingExternalArtefact(attribute.getAttribute()));
        return ret;
    }

    @Override
    public RamRectangleComponent getVisualLinkedComponent(RamRectangleComponent link) {
        return link;
    }

}
