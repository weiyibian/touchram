package ca.mcgill.sel.classdiagram.ui.views.handler;

import ca.mcgill.sel.classdiagram.ui.views.ClassView;
import ca.mcgill.sel.classdiagram.ui.views.ClassifierView;

/**
 * This interface is implemented by something that can handle events for a {@link ClassView}.
 * 
 * @author mschoettle
 * @author yhattab
 */
public interface IClassViewHandler extends IClassifierViewHandler {

    /**
     * Handles the creation of an {@link ca.mcgill.sel.ram.Attribute}.
     * 
     * @param classView the affected {@link ClassView}
     */
    void createAttribute(ClassView classView);

    /**
     * Handles the creation of a destructor.
     * 
     * @param clazz - the affected {@link ClassifierView}
     */
    void createDestructor(ClassifierView<?> clazz);

    /**
     * Handles the switch of a {@link ca.mcgill.sel.ram.Classifier} to abstract.
     * 
     * @param clazz - the affected {@link ClassifierView}
     */
    void switchToAbstract(ClassifierView<?> clazz);
}
