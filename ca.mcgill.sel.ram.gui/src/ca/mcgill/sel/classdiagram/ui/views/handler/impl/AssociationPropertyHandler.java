package ca.mcgill.sel.classdiagram.ui.views.handler.impl;

import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;

import ca.mcgill.sel.classdiagram.AssociationEnd;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.perspective.COREPerspectiveUtil;
import ca.mcgill.sel.core.perspective.PerspectiveName;
import ca.mcgill.sel.ram.ui.perspective.PerspectiveControllerFactory;
import ca.mcgill.sel.ram.ui.perspective.classdiagram.CdmPerspectiveController;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.impl.TextViewHandler;

/**
 * The default handler for a Qualifier Text View. It allows selecting another qualifier type
 * 
 * @author yhattab
 * */

public class AssociationPropertyHandler extends TextViewHandler {

    @Override
    public boolean processTapEvent(TapEvent tapEvent) {
        if (tapEvent.isDoubleTap()) {
            TextView view = (TextView) tapEvent.getTarget();
            CdmPerspectiveController cdmController = PerspectiveControllerFactory.INSTANCE
                    .getCdmPerspectiveController();
            AssociationEnd associationEnd = (AssociationEnd) view.getData();
            COREPerspective perspective = COREPerspectiveUtil.INSTANCE.getModelPerspective(associationEnd);
            
            //toggle 4 different combinations of ordering and uniqueness properties
            if (associationEnd != null) {
                if (associationEnd.isOrdered()) {
                    if (associationEnd.isUnique()) {
                        switch (perspective.getName()) {
                            case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE: 
                                cdmController.switchUniqueness(associationEnd);
                                break;
                            case PerspectiveName.DESIGN_MODEL_PERSPECTIVE: 
                                cdmController.switchUniqueness(associationEnd);
                                break;
                        } 
                    } else {
                        switch (perspective.getName()) {
                            case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE: 
                                cdmController.switchOrdering(associationEnd);
                                break;
                            case PerspectiveName.DESIGN_MODEL_PERSPECTIVE: 
                                cdmController.switchOrdering(associationEnd);
                                break;
                        } 
                    }
                } else {
                    if (associationEnd.isUnique()) {
                        switch (perspective.getName()) {
                            case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE: 
                                cdmController.switchOrdering(associationEnd);
                                break;
                            case PerspectiveName.DESIGN_MODEL_PERSPECTIVE: 
                                cdmController.switchOrdering(associationEnd);
                                break;
                        } 
                    } else {
                        switch (perspective.getName()) {
                            case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE: 
                                cdmController.switchUniqueness(associationEnd);
                                break;
                            case PerspectiveName.DESIGN_MODEL_PERSPECTIVE: 
                                cdmController.switchUniqueness(associationEnd);
                                break;
                        } 
                    }
                }
            }
            
            return true;
        }

        return false;
    }
}
