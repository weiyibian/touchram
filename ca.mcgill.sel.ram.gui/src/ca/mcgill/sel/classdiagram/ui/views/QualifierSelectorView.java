package ca.mcgill.sel.classdiagram.ui.views;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.classdiagram.Type;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamSelectorComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.components.listeners.RamSelectorListener;
import ca.mcgill.sel.ram.ui.events.listeners.ActionListener;

/**
 * A selector that will display all the types that can be used as a qualifier.
 *
 *
 * @author yhattab
 */
public class QualifierSelectorView extends RamSelectorComponent<String> 
                                            implements RamSelectorListener<String>, ActionListener {

    private static final int DEFAULT_MIN_NUM_TEXT = 1;
    
    /**
     * Holds all the {@link Type} names in the model.
     */
    private Map<String, Type> typeNames;

    /**
     * Constructor. Builds the type selector
     * 
     * @param classDiagram the {@link ClassDiagram} to get types from
     */
    public QualifierSelectorView(final ClassDiagram classDiagram) {
        registerListener(this);
        List<Type> types = new ArrayList<Type>();
        types.addAll(classDiagram.getTypes());
        types.addAll(classDiagram.getClasses());
        
        typeNames = new HashMap<String, Type>();
        for (Type type : types) {
            typeNames.put(type.getName(), type);
        }
        
        setMap(new LinkedHashMap<String, RamRectangleComponent>());
        setElements(new ArrayList<String>(typeNames.keySet()));
    }

    @Override
    public void textModified(RamTextComponent component) {
        if (component == inputField) {
            updateRecommendedList(component.getText());
        }
    }

    /**
     * Updates the displayed types to only the ones corresponding ton a search.
     *
     * @param text class being searched
     */
    private void updateRecommendedList(String text) {
        if (text.length() >= DEFAULT_MIN_NUM_TEXT) {
            // get entire list of types
            List<String> filteredTypeNames = new ArrayList<String>();
            for (String typeName : typeNames.keySet()) {
                if (typeName.toLowerCase().startsWith(text.trim().toLowerCase())) {
                    filteredTypeNames.add(typeName);
                }
            }
            // set a new map
            setMap(new LinkedHashMap<String, RamRectangleComponent>());
            // fill with new elements
            setElements(filteredTypeNames);
        } else {
            setElements(new ArrayList<String>(typeNames.keySet()));
        }
    }

    /**
     * Get the {@link Type} corresponding to a type name.
     * @param typeName the name of the Type to retrieve
     * @return the corresponding Type object
     */
    public Type getType(String typeName) {
        return typeNames.get(typeName);
    }

    @Override
    public void elementSelected(RamSelectorComponent<String> selector, String element) {
        destroy();
    }

    @Override
    public void closeSelector(RamSelectorComponent<String> selector) {
        destroy();
    }

    @Override
    public void elementDoubleClicked(RamSelectorComponent<String> selector, String element) {
    }

    @Override
    public void elementHeld(RamSelectorComponent<String> selector, String element) {

    }

    @Override
    public void actionPerformed(ActionEvent event) {
        
    }

}
