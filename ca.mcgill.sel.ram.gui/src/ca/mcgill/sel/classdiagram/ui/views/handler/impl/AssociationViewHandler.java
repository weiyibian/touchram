package ca.mcgill.sel.classdiagram.ui.views.handler.impl;

import org.eclipse.emf.ecore.util.EcoreUtil;
import org.mt4j.input.inputProcessors.componentProcessors.dragProcessor.DragEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;

import ca.mcgill.sel.classdiagram.AssociationEnd;
import ca.mcgill.sel.classdiagram.Class;
import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.classdiagram.ReferenceType;
import ca.mcgill.sel.classdiagram.ui.views.CDEnd;
import ca.mcgill.sel.classdiagram.ui.views.QualifierView;
import ca.mcgill.sel.classdiagram.ui.views.handler.IRelationshipViewHandler;
import ca.mcgill.sel.classdiagram.util.CdmModelUtil;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.perspective.COREPerspectiveUtil;
import ca.mcgill.sel.core.perspective.PerspectiveName;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamImageComponent;
import ca.mcgill.sel.ram.ui.components.RamSelectorComponent;
import ca.mcgill.sel.ram.ui.components.listeners.AbstractDefaultRamSelectorListener;
import ca.mcgill.sel.ram.ui.perspective.PerspectiveControllerFactory;
import ca.mcgill.sel.ram.ui.perspective.classdiagram.CdmPerspectiveController;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.views.OptionSelectorView;
import ca.mcgill.sel.ram.ui.views.OptionSelectorView.Iconified;
import ca.mcgill.sel.ram.ui.views.handler.BaseHandler;
import ca.mcgill.sel.restif.ui.views.BaseView;
import ca.mcgill.sel.restif.ui.views.handler.IAssociationViewHandler;


/**
 * The default handler for an {@link ca.mcgill.sel.ram.ui.views.structural.AssociationView}.
 *
 * @author eyildirim
 * @author mschoettle
 * @author yhattab
 */
public class AssociationViewHandler extends BaseHandler implements IAssociationViewHandler, IRelationshipViewHandler {

    /**
     * The options to display for an association end.
     */
    private enum AssociationOptions implements Iconified {
        COMPOSITION(new RamImageComponent(Icons.ICON_COMPOSITION, Colors.ICON_ADD_DEFAULT_COLOR)),
        AGGREGATION(new RamImageComponent(Icons.ICON_AGGREGATION, Colors.ICON_ADD_DEFAULT_COLOR)),
        REGULAR(new RamImageComponent(Icons.ICON_LINE, Colors.ICON_ADD_DEFAULT_COLOR)),
        QUALIFIER(new RamImageComponent(Icons.ICON_QUALIFIER, Colors.ICON_ADD_DEFAULT_COLOR)),
        DELETE(new RamImageComponent(Icons.ICON_DELETE, Colors.ICON_DELETE_COLOR));

        private RamImageComponent icon;

        /**
         * Creates a new option literal with the given icon.
         *
         * @param icon the icon to use for this option
         */
        AssociationOptions(RamImageComponent icon) {
            this.icon = icon;
        }

        @Override
        public RamImageComponent getIcon() {
            return icon;
        }

    }

    @Override
    public boolean processTapAndHold(TapAndHoldEvent tapAndHoldEvent, CDEnd<?, ?> end) {
        AssociationEnd associationEnd = (AssociationEnd) end.getModel();
        // Navigable associations from Implementation Classes to Classes are not allowed.
        // The end that will be made non-navigable cannot be the of the Class.
        // So we need to check that the navigable end (opposite) is not from an Implementation Class.
        // See issue #117.
        AssociationEnd oppositeEnd = associationEnd.getOppositeEnd();
        
        //check if this end is not linked to a qualifier or the node of an n-ary relationship
        if (oppositeEnd != null && oppositeEnd.getClassifier() instanceof Class
                && associationEnd.getAssoc().getEnds().size() <= 2 && associationEnd.getQualifier() == null 
                && CdmModelUtil.canCreateAssociation(associationEnd.getClassifier(), oppositeEnd.getClassifier())) {
           
            COREPerspective perspective = COREPerspectiveUtil.INSTANCE.getModelPerspective(associationEnd);
            switch (perspective.getName()) {
                case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE: 
                    PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController().switchNavigable(
                            (ClassDiagram) EcoreUtil.getRootContainer(associationEnd), oppositeEnd, associationEnd);
                    break;
                case PerspectiveName.DESIGN_MODEL_PERSPECTIVE: 
                    PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController().switchNavigable(
                            (ClassDiagram) EcoreUtil.getRootContainer(associationEnd), oppositeEnd, associationEnd);
                    break;
            }
        }

        return true;
    }

    @Override
    public boolean processDoubleTap(TapEvent tapEvent, CDEnd<?, ?> end) {
        final AssociationEnd associationEnd = (AssociationEnd) end.getModel();

        // Prevent changing the end belonging to an Implementation class (see issue #117).
        // Allow only deleting.
        AssociationOptions[] availableOptions;

        if (associationEnd.getClassifier() instanceof Class) {
            availableOptions = AssociationOptions.values();
        } else {
            availableOptions = new AssociationOptions[] {AssociationOptions.DELETE};
        }
        
        OptionSelectorView<AssociationOptions> selector =
                new OptionSelectorView<AssociationViewHandler.AssociationOptions>(availableOptions);

        RamApp.getActiveScene().addComponent(selector, tapEvent.getLocationOnScreen());

        // CHECKSTYLE:IGNORE AnonInnerLength: Okay here.
        selector.registerListener(new AbstractDefaultRamSelectorListener<AssociationViewHandler.AssociationOptions>() {
            @Override
            public void elementSelected(RamSelectorComponent<AssociationOptions> selector, AssociationOptions element) {
                CdmPerspectiveController cdmController = 
                        PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController();
                COREPerspective perspective = COREPerspectiveUtil.INSTANCE.getModelPerspective(associationEnd);

                switch (element) {
                    case COMPOSITION:
                        switch (perspective.getName()) {
                            case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE: 
                                cdmController.setReferenceType(associationEnd, ReferenceType.COMPOSITION);
                                break;
                            case PerspectiveName.DESIGN_MODEL_PERSPECTIVE: 
                                cdmController.setReferenceType(associationEnd, ReferenceType.COMPOSITION);
                                break;
                        } 
                        break;
                    case AGGREGATION:
                        switch (perspective.getName()) {
                            case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE: 
                                cdmController.setReferenceType(associationEnd, ReferenceType.AGGREGATION);
                                break;
                            case PerspectiveName.DESIGN_MODEL_PERSPECTIVE: 
                                cdmController.setReferenceType(associationEnd, ReferenceType.AGGREGATION);
                                break;
                        }
                        break;
                    case REGULAR:
                        switch (perspective.getName()) {
                            case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE: 
                                cdmController.setReferenceType(associationEnd, ReferenceType.REGULAR);
                                break;
                            case PerspectiveName.DESIGN_MODEL_PERSPECTIVE: 
                                cdmController.setReferenceType(associationEnd, ReferenceType.REGULAR);
                                break;
                        }
                        break;
                    case DELETE:
                        switch (perspective.getName()) {
                            case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE: 
                                cdmController.deleteAssociation((ClassDiagram) EcoreUtil
                                        .getRootContainer(associationEnd), associationEnd.getAssoc());
                                break;
                            case PerspectiveName.DESIGN_MODEL_PERSPECTIVE: 
                                cdmController.deleteAssociation((ClassDiagram) EcoreUtil
                                        .getRootContainer(associationEnd), associationEnd.getAssoc());
                                break;
                        }  
                        break;
                    case QUALIFIER:
                        if (associationEnd.getReferenceType() != ReferenceType.QUALIFIED 
                                && associationEnd.getOppositeEnd().getReferenceType() != ReferenceType.QUALIFIED) {
                            //allow only one qualifier per association
                            QualifierView.selectQualifier(associationEnd, tapEvent.getLocationOnScreen());
                        }
                        break;
                }
            }
        });

        return true;
    }

    @Override
    public void removeRepresented(BaseView<?> baseView) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public boolean processDragEvent(DragEvent dragEvent) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean processTapAndHoldEvent(TapAndHoldEvent tapAndHoldEvent) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean processTapEvent(TapEvent tapEvent) {
        // TODO Auto-generated method stub
        return false;
    }
}
