package ca.mcgill.sel.classdiagram.ui.views.handler.impl;

import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.classdiagram.Note;
import ca.mcgill.sel.classdiagram.ui.views.BaseView;
import ca.mcgill.sel.classdiagram.ui.views.NoteView;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.perspective.COREPerspectiveUtil;
import ca.mcgill.sel.core.perspective.PerspectiveName;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.menu.RamLinkedMenu;
import ca.mcgill.sel.ram.ui.perspective.PerspectiveControllerFactory;
import ca.mcgill.sel.ram.ui.perspective.classdiagram.CdmPerspectiveController;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.handler.ILinkedMenuListener;

/**
 * The default handler for a {@link Note}.
 * 
 * @author yhattab
 *
 */
public class NoteViewHandler extends BaseViewHandler implements ILinkedMenuListener {

    private static final String ACTION_DELETE_ANNOTATIONS = "view.note.annotations.delete";

    @Override
    public void actionPerformed(ActionEvent event) {
        String actionCommand = event.getActionCommand();
        RamRectangleComponent pressedButton = (RamRectangleComponent) event.getTarget();
        RamLinkedMenu linkedMenu = (RamLinkedMenu) pressedButton.getParentOfType(RamLinkedMenu.class);
        if (linkedMenu != null) {

            NoteView noteView = (NoteView) linkedMenu.getLinkedView();

            if (ACTION_DELETE_ANNOTATIONS.equals(actionCommand)) {
                CdmPerspectiveController cdmController = PerspectiveControllerFactory.INSTANCE
                            .getCdmPerspectiveController();

                Note note = noteView.getNote();
                COREPerspective perspective = COREPerspectiveUtil.INSTANCE.getModelPerspective(note);
                switch (perspective.getName()) {
                    case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE:
                        cdmController.removeAnnotations(note);
                        break;
                    case PerspectiveName.DESIGN_MODEL_PERSPECTIVE:
                        cdmController.removeAnnotations(note);
                        break;
                }
            }
        }
        super.actionPerformed(event);
    }

    @Override
    public List<EObject> getEObjectToListenForUpdateMenu(RamRectangleComponent rectangle) {
        return super.getEObjectToListenForUpdateMenu(rectangle);
    }

    @Override
    public void updateMenu(RamLinkedMenu menu, Notification notification) {
        super.updateMenu(menu, notification);
    }

    @Override
    public void initMenu(RamLinkedMenu menu) {
        super.initMenu(menu);
        menu.addAction(Strings.MENU_DELETE_ANNOTATIONS, Icons.ICON_MENU_CLEAR_SELECTION,
                ACTION_DELETE_ANNOTATIONS, this, SUBMENU_ADD, true);
    }

    @Override
    public void removeRepresented(BaseView<?> baseView) {
        COREPerspective perspective = COREPerspectiveUtil.INSTANCE.getModelPerspective((Note) 
                baseView.getRepresented());
        switch (perspective.getName()) {
            case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE:
                PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController().removeNote(
                        (Note) baseView.getRepresented());
                break;
            case PerspectiveName.DESIGN_MODEL_PERSPECTIVE:
                PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController().removeNote(
                        (Note) baseView.getRepresented());
                break;
        }
    }
}
