package ca.mcgill.sel.classdiagram.ui.views.handler.impl;

import java.util.HashSet;
import java.util.Set;

import ca.mcgill.sel.classdiagram.CDEnum;
import ca.mcgill.sel.classdiagram.CDEnumLiteral;
import ca.mcgill.sel.classdiagram.ui.views.BaseView;
import ca.mcgill.sel.classdiagram.ui.views.EnumView;
import ca.mcgill.sel.classdiagram.ui.views.handler.IEnumViewHandler;
import ca.mcgill.sel.commons.StringUtil;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.perspective.COREPerspectiveUtil;
import ca.mcgill.sel.core.perspective.PerspectiveName;
import ca.mcgill.sel.ram.ui.components.RamKeyboard;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.components.listeners.DefaultRamKeyboardListener;
import ca.mcgill.sel.ram.ui.components.menu.RamLinkedMenu;
import ca.mcgill.sel.ram.ui.perspective.PerspectiveControllerFactory;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.handler.ILinkedMenuListener;
import ca.mcgill.sel.ram.util.MetamodelRegex;


/**
 * The default handler for a {@link EnumView}.
 * 
 * @author Franz
 * @author yhattab
 */
public class EnumViewHandler extends BaseViewHandler implements IEnumViewHandler, ILinkedMenuListener {

    private static final String ACTION_ENUM_LITERAL_ADD = "view.enum.literal.add";

    @Override
    public void createLiteral(final EnumView enumView) {
        // Get the index where we should add the literal
        final int index = enumView.getCDEnum().getLiterals().size();

        // create new row
        final RamTextComponent textRow = new RamTextComponent();
        textRow.setPlaceholderText(Strings.PH_ENUM_LITERALS);

        // Get the container holding all the literals
        final RamRectangleComponent literalsContainer = enumView.getEnumLiteralsContainer();

        // Add the new text row to it
        literalsContainer.addChild(index, textRow);

        // Create keyboard and add listener
        RamKeyboard keyboard = new RamKeyboard();
        keyboard.registerListener(new DefaultRamKeyboardListener() {
            @Override
            public void keyboardCancelled() {
                // If cancelled remove the row and enable the edit buttons
                literalsContainer.removeChild(textRow);
            }

            @Override
            public boolean verifyKeyboardDismissed() {
                try {
                    // Create the literal in the model
                    createLiteral(enumView.getCDEnum(), index, textRow.getText());

                    // Remove text row and enable the edit buttons
                    literalsContainer.removeChild(textRow);
                } catch (final IllegalArgumentException e) {
                    return false;
                }
                return true;
            }
        });

        textRow.showKeyboard(keyboard);

    }

    /**
     * Creates a Enum Literal by calling the EnumController.
     * 
     * @param owner REnum that should contain this literal
     * @param index Index where we should insert it
     * @param enumLiteralName Name given to this new literal
     * @throws IllegalArgumentException If it is a invalid enum literal.
     */
    private void createLiteral(CDEnum owner, int index, String enumLiteralName) {
        if (!enumLiteralName.matches(MetamodelRegex.REGEX_ENUM_LITERAL)) {
            throw new IllegalArgumentException("The string " + enumLiteralName
                    + " is not valid syntax for enum literals");
        }
        String[] allEnumLiterals = enumLiteralName.split(",");

        hasDuplicateLiterals(allEnumLiterals);

        for (String literal : allEnumLiterals) {
            isEnumLiteralUniqueIn(owner, literal.trim());
        }

        int i = index;
        for (String literal : allEnumLiterals) {
            // Create literal in the model with controller
            String literalName = StringUtil.toUpperCaseFirst(literal.trim());
            COREPerspective perspective = COREPerspectiveUtil.INSTANCE.getModelPerspective(owner);
            switch (perspective.getName()) {
                case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE: 
                    PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController().createREnumLiteral(
                            owner, i++, literalName);
                    break;
                case PerspectiveName.DESIGN_MODEL_PERSPECTIVE: 
                    PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController().createREnumLiteral(
                            owner, i++, literalName);
                    break;
            }
        }
    }

    /**
     * Checks if literal is unique.
     * 
     * @param owner Owner of the literal.
     * @param literalName name of the literal.
     * @throws IllegalArgumentException
     */
    @SuppressWarnings("static-method")
    private void isEnumLiteralUniqueIn(CDEnum owner, String literalName) {
        for (CDEnumLiteral literal : owner.getLiterals()) {
            if (literal.getName().toLowerCase().equals(literalName.toLowerCase())) {
                throw new IllegalArgumentException("The string " + literalName
                        + " already exists in" + owner.getName());
            }
        }

    }

    /**
     * Checks if it has a duplicate literal.
     * 
     * @param literals All literals.
     */
    @SuppressWarnings("static-method")
    private void hasDuplicateLiterals(String[] literals) {
        Set<String> literalSet = new HashSet<String>();
        for (String literal : literals) {
            literalSet.add(literal.toLowerCase().trim());
        }
        if (literalSet.size() != literals.length) {
            throw new IllegalArgumentException("The string contains literal duplicates.");
        }
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        String actionCommand = event.getActionCommand();
        RamRectangleComponent pressedButton = (RamRectangleComponent) event.getTarget();
        RamLinkedMenu linkedMenu = (RamLinkedMenu) pressedButton.getParentOfType(RamLinkedMenu.class);
        if (linkedMenu != null) {

            EnumView enumView = (EnumView) linkedMenu.getLinkedView();

            if (ACTION_ENUM_LITERAL_ADD.equals(actionCommand)) {
                createLiteral(enumView);
            }
        }
        super.actionPerformed(event);
    }

    @Override
    public void initMenu(RamLinkedMenu menu) {
        super.initMenu(menu);
        menu.addAction(Strings.MENU_LITERAL_ADD, Icons.ICON_MENU_ADD_LITERAL, ACTION_ENUM_LITERAL_ADD, this,
                SUBMENU_ADD, true);
    }

    @Override
    public void removeRepresented(BaseView<?> baseView) {
        COREPerspective perspective = COREPerspectiveUtil.INSTANCE
                .getModelPerspective((CDEnum) baseView.getRepresented());
        switch (perspective.getName()) {
            case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE: 
                PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController().deleteEnum(
                        (CDEnum) baseView.getRepresented());
                break;
            case PerspectiveName.DESIGN_MODEL_PERSPECTIVE: 
                PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController().deleteEnum(
                        (CDEnum) baseView.getRepresented());
                break;
        }  
    }

}
