package ca.mcgill.sel.classdiagram.ui.views.handler.impl;

import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.classdiagram.ui.views.EnumLiteralView;
import ca.mcgill.sel.classdiagram.ui.views.handler.IEnumLiteralViewHandler;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.perspective.COREPerspectiveUtil;
import ca.mcgill.sel.core.perspective.PerspectiveName;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.menu.RamLinkedMenu;
import ca.mcgill.sel.ram.ui.perspective.PerspectiveControllerFactory;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.handler.BaseHandler;
import ca.mcgill.sel.ram.ui.views.handler.ILinkedMenuListener;


/**
 * The default handler for a {@link EnumLiteralView}.
 * 
 * @author Franz
 * @author yhattab
 */
public class EnumLiteralViewHandler extends BaseHandler implements IEnumLiteralViewHandler, ILinkedMenuListener {

    private static final String ACTION_LITERAL_REMOVE = "view.literal.remove";

    @Override
    public void removeLiteral(EnumLiteralView eLiteralView) {
        COREPerspective perspective = COREPerspectiveUtil.INSTANCE.getModelPerspective(eLiteralView.getLiteral());
        switch (perspective.getName()) {
            case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE: 
                PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController()
                    .removeLiteral(eLiteralView.getLiteral());
                break;
            case PerspectiveName.DESIGN_MODEL_PERSPECTIVE: 
                PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController()
                    .removeLiteral(eLiteralView.getLiteral());
                break;
        }
        
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        String actionCommand = event.getActionCommand();

        RamRectangleComponent pressedButton = (RamRectangleComponent) event.getTarget();
        RamLinkedMenu linkedMenu = (RamLinkedMenu) pressedButton.getParentOfType(RamLinkedMenu.class);
        if (linkedMenu != null) {

            EnumLiteralView literalView = (EnumLiteralView) linkedMenu.getLinkedView();

            if (ACTION_LITERAL_REMOVE.equals(actionCommand)) {
                removeLiteral(literalView);
            }
        }
    }

    @Override
    public EObject getEobject(RamRectangleComponent rectangle) {
        return ((EnumLiteralView) rectangle).getLiteral();
    }

    @Override
    public void initMenu(RamLinkedMenu menu) {
        menu.addAction(Strings.MENU_DELETE, Icons.ICON_MENU_TRASH, ACTION_LITERAL_REMOVE, this, true);

        menu.setLinkInCorners(false);
    }

    @Override
    public List<EObject> getEObjectToListenForUpdateMenu(RamRectangleComponent rectangle) {
        return null;
    }

    @Override
    public void updateMenu(RamLinkedMenu menu, Notification notification) {

    }

    @Override
    public RamRectangleComponent getVisualLinkedComponent(RamRectangleComponent link) {
        return link;
    }

}
