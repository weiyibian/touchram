package ca.mcgill.sel.classdiagram.ui.views;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.core.util.COREModelUtil;
import ca.mcgill.sel.classdiagram.Classifier;
import ca.mcgill.sel.classdiagram.LayoutElement;
import ca.mcgill.sel.classdiagram.CDEnum;
import ca.mcgill.sel.classdiagram.CDEnumLiteral;
import ca.mcgill.sel.classdiagram.CdmPackage;
import ca.mcgill.sel.classdiagram.ui.views.handler.ClassDiagramHandlerFactory;
import ca.mcgill.sel.classdiagram.ui.views.handler.IEnumViewHandler;

import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent.Alignment;
import ca.mcgill.sel.ram.ui.layouts.VerticalLayout;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Fonts;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.TextView;


/**
 * This view draws the representation of a CDEnum onto a {@link ClassDiagramView}. It contains a name field, and
 * a list for its literals.
 * 
 * @author Franz
 * @author yhattab
 * 
 */
public class EnumView extends LinkableView<IEnumViewHandler> {

    /**
     * The minimum width of the view.
     */
    protected static final float MINIMUM_WIDTH = 150f;

    /**
     * Reference on the related {@link REnum}.
     */
    protected CDEnum aEnum;

    /**
     * Literals eObject of the {@link REnum} and their {@link EnumLiteralView}.
     */
    protected Map<CDEnumLiteral, EnumLiteralView> literals;

    /**
     * Container for the literals.
     */
    protected RamRectangleComponent literalsContainer;
    /**
     * Indicates if the {@link REnum} is an implementation.
     */
    protected boolean isImplementationEnum;

    /**
     * Constructor.
     * 
     * @param structuralDiagramView that will contain this enum view.
     * @param someEnum Enum represented by this view.
     * @param layoutElement of this view.
     * @param isImpEnum true if implementation enum, false otherwise
     */
    protected EnumView(ClassDiagramView structuralDiagramView, CDEnum someEnum, LayoutElement layoutElement,
            boolean isImpEnum) {
        super(structuralDiagramView, someEnum, layoutElement);
        isImplementationEnum = isImpEnum;
        // Initialize fields
        aEnum = someEnum;
        literals = new HashMap<CDEnumLiteral, EnumLiteralView>();
        buildEnumView();
        setNoStroke(false);
        nameField.setBufferSize(Cardinal.SOUTH, 0);

        // initialize with given enum
        initializeEnum(someEnum);
        updateStyle();
    }

    /**
     * Initializes the enum view with the REnum.
     * 
     * @param someEnum REnum to initialize.
     */
    private void initializeEnum(CDEnum someEnum) {
        for (CDEnumLiteral literal : someEnum.getLiterals()) {
            addLiteralField(literalsContainer.getChildCount(), literal);
        }
    }

    /**
     * Will build the view.
     */
    protected void buildEnumView() {
        setMinimumWidth(MINIMUM_WIDTH);
        addNameField(aEnum, Strings.PH_ENTER_ENUM_NAME);
        addEnumTag();
        addLiteralsContainer();
    }

    /**
     * Adds container that will hold the literals.
     */
    protected void addLiteralsContainer() {
        // add literals container to view
        literalsContainer = new RamRectangleComponent(new VerticalLayout());
        literalsContainer.setNoStroke(false);
        literalsContainer.setMinimumHeight(ICON_SIZE);
        addChild(literalsContainer);
    }

    /**
     * Adds the name field.
     * 
     * @param classifier whose name we want to display.
     * @param placeholder text.
     */
    protected void addNameField(Classifier classifier, String placeholder) {
        // Add the name field to base view
        TextView nameField = new TextView(classifier, CdmPackage.Literals.NAMED_ELEMENT__NAME);
        nameField.setFont(Fonts.FONT_CLASS_NAME);
        nameField.setUniqueName(true);
        nameField.setAlignment(Alignment.CENTER_ALIGN);
        nameField.setPlaceholderText(placeholder);
        if (!isImplementationEnum) {
            nameField.setHandler(ClassDiagramHandlerFactory.INSTANCE.getEnumNameHandler());
        } else {
            nameField.setHandler(ClassDiagramHandlerFactory.INSTANCE.getImplementationEnumNameHandler());
        }
        setNameField(nameField);

    }

    /**
     * Adds the enum tag.
     */
    protected void addEnumTag() {
        // Create the Text field
        RamTextComponent enumTagField = new RamTextComponent();
        enumTagField.setFont(Fonts.FONT_ENUM_TAG);
        enumTagField.setAlignment(Alignment.CENTER_ALIGN);
        enumTagField.setText(Strings.TAG_ENUMERATION);
        enumTagField.setNoStroke(true);
        enumTagField.setBufferSize(Cardinal.SOUTH, 0);
        setTagField(enumTagField);
    }

    /**
     * Getter for the {@link REnum} associated to this view.
     * 
     * @return REnum associated to this view
     */
    public CDEnum getCDEnum() {
        return aEnum;
    }

    /**
     * Adds a enum literal at the specified index.
     * 
     * @param index where to insert.
     * @param literal we want to insert.
     */
    private void addLiteralField(int index, CDEnumLiteral literal) {
        // Add an enum literal
        EnumLiteralView literalView = new EnumLiteralView(this, literal, !isImplementationEnum);
        literals.put(literal, literalView);

        literalsContainer.addChild(index, literalView);
        literalView.setHandler(ClassDiagramHandlerFactory.INSTANCE.getEnumLiteralViewHandler());
    }

    /**
     * Getter for the {@link RamRectangleComponent} corresponding to the enum literals.
     * 
     * @return RamRectangleComponent corresponding to the enum literals
     */
    public RamRectangleComponent getEnumLiteralsContainer() {
        return literalsContainer;
    }

    @Override
    public void notifyChanged(Notification notification) {
        // handle it first by the super class
        super.notifyChanged(notification);

        // this gets called for every ClassView (as there is only one item provider)
        // therefore an additional check is necessary
        if (notification.getNotifier() == aEnum) {
            if (notification.getFeature() == CdmPackage.Literals.CD_ENUM__LITERALS) {
                switch (notification.getEventType()) {
                    case Notification.REMOVE:
                        CDEnumLiteral literal = (CDEnumLiteral) notification.getOldValue();
                        removeLiteral(literal);
                        break;
                    case Notification.ADD:
                        literal = (CDEnumLiteral) notification.getNewValue();
                        addLiteralField(notification.getPosition(), literal);
                        break;
                }
            } 
        }
    }

    /**
     * Removes a specified literal.
     * 
     * @param literal to be removed.
     */
    private void removeLiteral(final CDEnumLiteral literal) {
        if (literals.containsKey(literal)) {
            RamApp.getApplication().invokeLater(new Runnable() {

                @Override
                public void run() {
                    EnumLiteralView literalView = literals.remove(literal);

                    literalsContainer.removeChild(literalView);
                    literalView.destroy();
                }
            });
        }
    }

    /**
     * Show keyboard for name field of this view.
     * 
     */
    public void showKeyboard() {
        nameField.showKeyboard();
    }

    /**
     * Clear the name field of this view.
     * 
     */
    public void clearNameField() {
        ((TextView) nameField).clearText();
    }
    
    @Override
    public void setSizeLocal(float width, float height) {
        super.setSizeLocal(width, height);
        updateRelationships();
    }

    @Override
    public void translate(Vector3D dirVect) {
        super.translate(dirVect);
        updateRelationships();
    }

    /**
     * Updates the style of this view depending on whether the classifier is a reference or not.
     */
    @Override
    protected void updateStyle() {
        if (COREModelUtil.isReference(aEnum)) {
            setFillColor(Colors.ENUM_VIEW_REFERENCE_FILL_COLOR);
            setStrokeColor(Colors.ENUM_VIEW_REFERENCE_STROKE_COLOR);
            nameField.setFont(Fonts.FONT_CLASS_NAME_REFERENCE);
            tagNameField.setFont(Fonts.FONT_ENUM_TAG_REFERENCE);
        } else {
            setFillColor(Colors.ENUM_VIEW_DEFAULT_FILL_COLOR);
            setStrokeColor(Colors.ENUM_VIEW_DEFAULT_STROKE_COLOR);
            nameField.setFont(Fonts.FONT_CLASS_NAME);
            tagNameField.setFont(Fonts.FONT_ENUM_TAG);
        }
    }

}
