package ca.mcgill.sel.classdiagram.ui.views;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.edit.provider.INotifyChangedListener;
import org.mt4j.components.TransformSpace;
import org.mt4j.components.visibleComponents.shapes.MTPolygon;
import org.mt4j.input.gestureAction.TapAndHoldVisualizer;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldProcessor;
import org.mt4j.util.MTColor;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.classdiagram.Association;
import ca.mcgill.sel.classdiagram.AssociationEnd;
import ca.mcgill.sel.classdiagram.CdmPackage;
import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.classdiagram.ImplementationClass;
import ca.mcgill.sel.classdiagram.ReferenceType;
import ca.mcgill.sel.classdiagram.ui.views.CDEnd.Position;
import ca.mcgill.sel.classdiagram.ui.views.handler.ClassDiagramHandlerFactory;
import ca.mcgill.sel.classdiagram.ui.views.handler.impl.QualifierViewHandler;
import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.perspective.COREPerspectiveUtil;
import ca.mcgill.sel.core.perspective.PerspectiveName;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.perspective.PerspectiveControllerFactory;
import ca.mcgill.sel.ram.ui.utils.Fonts;
import ca.mcgill.sel.ram.ui.utils.GUIConstants;
import ca.mcgill.sel.ram.ui.utils.GraphicalUpdater;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.ITextViewHandler;


/**
 * AssociationViews are components used to draw {@link Association}s onto the {@link ClassDiagramView}. They draw
 * {@link AssociationEnd}s using the correct visual representation and the link them together using a minimal set of
 * vertical and horizontal lines. All elements in this component change color when highlighted.
 *
 * @author yhattab
 * @author vbonnetf
 * @author mschoettle
 * @author eyildirim
 * @author tdimeco
 * @author cbensoussan
 */
public class AssociationView extends RelationshipView<AssociationEnd, LinkableView<?>> implements
        INotifyChangedListener {

    /**
     * The offset of text from the association.
     */
    static final int TEXT_OFFSET_FROM_ASSOCIATION = 5;
    
    private static final int ARROW_SIZE = 20;
    private static final int ROTATION = 90;
    private static final int ROTATION_FORWARD = 1;
    private static final int ROTATION_BACKWARD = -1;
    private static final int TEXT_OFFSET_FROM_CLASSIFIER = 15;
    private static final String FROM = "FROM";
    private static final String TO = "TO";
    
    private TextView fromEndRolename;
    private TextView toEndRolename;

    private MultiplicityTextView fromEndMultiplicity;
    private MultiplicityTextView toEndMultiplicity;

    private TextView fromEndProperty;
    private TextView toEndProperty;

    private TextView fromEndQualifier;
    private TextView toEndQualifier;

    private GraphicalUpdater graphicalUpdater;

    private Position lastFromPosition;
    private Position lastToPosition;

    /**
     * Creates a new AssociationView.
     *
     * @param association
     *            the represented {@link Association}
     * @param from
     *            the from {@link AssociationEnd}
     * @param fromEndClassifierView
     *            the from {@link LinkableView}
     * @param to
     *            the to {@link AssociationEnd}
     * @param toEndClassifierView
     *            the to {@link LinkableView}
     * @see RelationshipView#RelationshipView(ca.mcgill.sel.ram.NamedElement, LinkableView,
     *      ca.mcgill.sel.ram.NamedElement, LinkableView)
     */
    public AssociationView(Association association, AssociationEnd from, LinkableView<?> fromEndClassifierView,
            AssociationEnd to, LinkableView<?> toEndClassifierView) {
        super(from, fromEndClassifierView, to, toEndClassifierView);
        
        //create Qualifiers
        if (from.getQualifier() != null) {
            fromEndQualifier = createQualifierView(from);
            updateLines();
        }
        if (to.getQualifier() != null) {
            toEndQualifier = createQualifierView(to);
            updateLines();
        }

        // We do not need to add a listener for the "to" side, this listener already listens on the whole class
        EMFEditUtil.addListenerFor(from, this);

        ClassDiagram cd = EMFModelUtil.getRootContainerOfType(association, CdmPackage.Literals.CLASS_DIAGRAM);
        graphicalUpdater = RamApp.getApplication().getGraphicalUpdaterForModel(cd);
        graphicalUpdater.addGUListener(association, this);
    }
    
    @Override
    protected void updateRelationshipEnds(RamRectangleComponent viewFrom, RamRectangleComponent viewTo,
            Position fromEndPosition, Position toEndPosition) {
     // now we set the positions.
        if (viewFrom instanceof LinkableView) {
            // process from end
            LinkableView<?> classViewFrom = (LinkableView<?>) viewFrom;
            LinkableView<?> classViewTo = (LinkableView<?>) viewTo;

            // if previous and current positions are different
            // also if fromEnd.getPosition() is null
            if (fromEndPosition != fromEnd.getPosition()) {
                classViewFrom.moveRelationshipEnd(fromEnd, fromEndPosition);
                classViewFrom.updateLayout();
            } else {
                // if position is the same reset the positions
                // for all ends on the same edge of this view.
                classViewFrom.setCorrectPosition(fromEnd);
            }

            // process to end
            if (toEndPosition != toEnd.getPosition()) {
                classViewTo.moveRelationshipEnd(toEnd, toEndPosition);
                classViewFrom.moveRelationshipEnd(fromEnd, fromEndPosition);
                classViewTo.updateLayout();
            } else {
                classViewTo.setCorrectPosition(toEnd);
            }
        } else if (viewFrom instanceof RamRectangleComponent) {
            // TODO: This is what everyone should do (see issue #109).
            IRelationshipEndView fromRelationshipView = null;
            IRelationshipEndView toRelationshipView = null;
            if (fromEnd.getComponentView() instanceof IRelationshipEndView) {
                fromRelationshipView = (IRelationshipEndView) fromEnd.getComponentView();
            }
            if (toEnd.getComponentView() instanceof IRelationshipEndView) {
                toRelationshipView = (IRelationshipEndView) toEnd.getComponentView();
            }
            // End has to be moved somewhere else.
            if (fromEndPosition != fromEnd.getPosition()) {
                Position oldPosition = fromEnd.getPosition();
                fromEnd.setPosition(fromEndPosition);
                if (fromRelationshipView != null) {
                    fromRelationshipView.moveRelationshipEnd(fromEnd, oldPosition, fromEndPosition);
                }
            } else {
                if (fromRelationshipView != null) {
                    fromRelationshipView.updateRelationshipEnd(fromEnd);
                }
            }

            if (toEndPosition != toEnd.getPosition()) {
                Position oldPosition = toEnd.getPosition();
                toEnd.setPosition(toEndPosition);
                if (toRelationshipView != null) {
                    toRelationshipView.moveRelationshipEnd(toEnd, oldPosition, toEndPosition);
                }
                // The from end might have to be fixed depending on what happened to the to end.
                if (fromRelationshipView != null) {
                    fromRelationshipView.updateRelationshipEnd(fromEnd);
                }
            } else {
                if (toRelationshipView != null) {
                    toRelationshipView.updateRelationshipEnd(toEnd);
                }
            }
        }
    }

    /**
     * Returns the from or to end of the association view given a text field from the wanted end.
     * @param textView A text view from the wanted end
     * @return The from or to end of the association view
     */
    public CDEnd<AssociationEnd, LinkableView<?>> getEnd(TextView textView) {
        if (!this.containsChild(textView)) {
            return null;
        }

        if (textView == fromEndProperty || textView == fromEndQualifier || textView == fromEndRolename
                || textView == fromEndMultiplicity) {
            return this.getFromEnd();
        } else {
            return this.getToEnd();
        }
    }

    /**
     * Creates texts (multiplicity and role name) for the given ends if they haven't been created yet.
     *
     * @param cdEnd the first end
     * @param cdEnd2 the second (opposite) end
     */
    private void createTexts(CDEnd<AssociationEnd, LinkableView<?>> cdEnd, CDEnd<AssociationEnd,
            LinkableView<?>> cdEnd2) {
        if (fromEndRolename == null) {
            fromEndRolename = createRoleNameView(cdEnd.getModel());
            fromEndMultiplicity = createMultiplicityView(cdEnd.getModel());
            if ((cdEnd.getModel().getUpperBound() > 1
                    || cdEnd.getModel().getUpperBound() == -1)
                    && !(cdEnd.getModel().eContainer() instanceof ImplementationClass)) {
                fromEndProperty = createPropertyView(cdEnd.getModel());
            }
            
            AssociationEnd fromEnd = cdEnd.getModel();
            if (fromEnd.eIsSet(CdmPackage.Literals.ASSOCIATION_END__QUALIFIER)) {
                fromEndQualifier = createQualifierView(fromEnd);
            }
            graphicalUpdater.addGUListener(cdEnd.getModel(), fromEndRolename);
        }

        if (toEndRolename == null) {
            toEndRolename = createRoleNameView(cdEnd2.getModel());
            toEndMultiplicity = createMultiplicityView(cdEnd2.getModel());
            if ((cdEnd2.getModel().getUpperBound() > 1
                    || cdEnd2.getModel().getUpperBound() == -1)
                    && !(cdEnd2.getModel().eContainer() instanceof ImplementationClass)) {
                toEndProperty = createPropertyView(cdEnd2.getModel());
            }
            
            AssociationEnd toEnd = cdEnd.getModel();
            if (toEnd.eIsSet(CdmPackage.Literals.ASSOCIATION_END__QUALIFIER)) {
                toEndQualifier = createQualifierView(toEnd);
            }
            graphicalUpdater.addGUListener(cdEnd2.getModel(), toEndRolename);
        }
    }

    /**
     * Moves the texts (multiplicity and role name) for the given end.
     *
     * @param ramEnd the end the texts to move for
     * @param roleName the view for the role name
     * @param multiplicity the view for the multiplicity
     * @param featureSelection the view for the feature selection
     * @param qualifier the view for the key selection
     * @param lastPosition the last position the text was located at
     */
    private void moveTexts(CDEnd<AssociationEnd, LinkableView<?>> ramEnd, TextView roleName,
            MultiplicityTextView multiplicity, TextView featureSelection, TextView qualifier, 
            Position lastPosition) {
        CDEnd<AssociationEnd, LinkableView<?>> opposite = ramEnd.getOpposite();

        // If the position changed, rotate the texts back to it's regular position.
        if (lastPosition != null
                && lastPosition != opposite.getPosition()) {
            rotateText(roleName, lastPosition, ROTATION_BACKWARD);
            rotateText(multiplicity, lastPosition, ROTATION_BACKWARD);
            
            if (featureSelection != null) {
                rotateText(featureSelection, lastPosition, ROTATION_BACKWARD);
            }
            
            if (qualifier != null) {
                rotateText(qualifier, lastPosition, ROTATION_BACKWARD);
            }
        }
        
        Vector3D qualifierSize = new Vector3D(0, 0);
        if (qualifier != null) {
            qualifierSize.setX(qualifier.getWidth());
            qualifierSize.setY(qualifier.getHeight());
        }
        
        moveRoleName(roleName, opposite.getLocation(), opposite.getPosition(),
                qualifierSize);
        moveMultiplicity(multiplicity, opposite.getLocation(), opposite.getPosition(),
                qualifierSize);
        
        if (featureSelection != null) {
            moveFeatureSelection(featureSelection, opposite.getLocation(), opposite.getPosition(),
                    qualifierSize, multiplicity.getWidth());
        }
        
        if (qualifier != null) {
            moveQualifier(qualifier, opposite.getLocation(), opposite.getPosition());            
        }
        
        // Rotate the texts for top and bottom positions
        if (lastPosition != opposite.getPosition()) {
            rotateText(roleName, opposite.getPosition(), ROTATION_FORWARD);
            rotateText(multiplicity, opposite.getPosition(), ROTATION_FORWARD);
            
            if (featureSelection != null) {
                rotateText(featureSelection, opposite.getPosition(), ROTATION_FORWARD);
            }
        
            if (qualifier != null) {
                rotateText(qualifier, opposite.getPosition(), ROTATION_FORWARD);
            }
        }
    }

    /**
     * Rotates the given text depending on the position and into the given direction.
     * 
     * @param text the text to rotate
     * @param position the position on which the text is located at
     * @param direction the rotation direction
     */
    private static void rotateText(RamTextComponent text, Position position, int direction) {
        float x = text.getPosition(TransformSpace.RELATIVE_TO_PARENT).getX();
        float y = text.getPosition(TransformSpace.RELATIVE_TO_PARENT).getY();
        
        // happily changing the anchor of the text area is sufficient in most cases
        switch (position) {
            case BOTTOM:
                text.rotateZ(new Vector3D(x, y), direction * -ROTATION);
                break;
            case TOP:
                text.rotateZ(new Vector3D(x, y), direction * ROTATION);
                break;
        }
    }

    /**
     * Moves the role name to the proper position.
     *
     * @param textView the view of the role name
     * @param currentPosition the current position of the role name
     * @param position the side of the association end on the attached classifier view
     * @param keySelectionSize the size of the key selection box
     */
    @SuppressWarnings("static-method")
    private void moveRoleName(RamTextComponent textView, Vector3D currentPosition,
            Position position, Vector3D keySelectionSize) {
        float x = currentPosition.getX();
        float y = currentPosition.getY();

        // happily changing the anchor of the text area is sufficient in most cases
        switch (position) {
            case BOTTOM:
                textView.setAnchor(PositionAnchor.LOWER_RIGHT);
                x -= TEXT_OFFSET_FROM_ASSOCIATION;
                y += TEXT_OFFSET_FROM_CLASSIFIER + keySelectionSize.getY();
                break;
            case TOP:
                textView.setAnchor(PositionAnchor.LOWER_RIGHT);
                x += TEXT_OFFSET_FROM_ASSOCIATION;
                y -= TEXT_OFFSET_FROM_CLASSIFIER + keySelectionSize.getY();
                break;
            case LEFT:
                textView.setAnchor(PositionAnchor.LOWER_RIGHT);
                x -= TEXT_OFFSET_FROM_CLASSIFIER + keySelectionSize.getX();
                y -= TEXT_OFFSET_FROM_ASSOCIATION;
                break;
            case RIGHT:
                textView.setAnchor(PositionAnchor.LOWER_LEFT);
                x += TEXT_OFFSET_FROM_CLASSIFIER + keySelectionSize.getX();
                y -= TEXT_OFFSET_FROM_ASSOCIATION;
                break;
        }

        textView.setPositionRelativeToParent(new Vector3D(x, y));
    }

    /**
     * Moves the multiplicity to the proper position.
     *
     * @param textView the view of the multiplicity
     * @param currentPosition the current position of the multiplicity
     * @param position the side of the association end on the attached classifier view
     * @param keySelectionSize the size of the key selection box
     */
    @SuppressWarnings("static-method")
    private void moveMultiplicity(RamTextComponent textView, Vector3D currentPosition,
            Position position, Vector3D keySelectionSize) {
        float x = currentPosition.getX();
        float y = currentPosition.getY();

        // gladly changing the anchor of the text area is sufficient in most cases
        switch (position) {
            case BOTTOM:
                textView.setAnchor(PositionAnchor.UPPER_RIGHT);
                x += TEXT_OFFSET_FROM_ASSOCIATION;
                y += TEXT_OFFSET_FROM_CLASSIFIER + keySelectionSize.getY();
                break;
            case TOP:
                textView.setAnchor(PositionAnchor.UPPER_RIGHT);
                x -= TEXT_OFFSET_FROM_ASSOCIATION;
                y -= TEXT_OFFSET_FROM_CLASSIFIER + keySelectionSize.getY();
                break;
            case LEFT:
                textView.setAnchor(PositionAnchor.UPPER_RIGHT);
                x -= TEXT_OFFSET_FROM_CLASSIFIER + keySelectionSize.getX();
                y += TEXT_OFFSET_FROM_ASSOCIATION;
                break;
            case RIGHT:
                textView.setAnchor(PositionAnchor.UPPER_LEFT);
                x += TEXT_OFFSET_FROM_CLASSIFIER + keySelectionSize.getX();
                y += TEXT_OFFSET_FROM_ASSOCIATION;
                break;
        }

        textView.setPositionRelativeToParent(new Vector3D(x, y));
    }

    /**
     * Moves the feature selection to the proper position.
     *
     * @param textView the view of the feature selection
     * @param currentPosition the current position of the feature selection
     * @param position the side of the association end on the attached classifier view
     * @param keySelectionSize the size of the key selection box
     * @param multiplicityWidth the width of the multiplicity text view
     */
    @SuppressWarnings("static-method")
    private void moveFeatureSelection(RamTextComponent textView, Vector3D currentPosition,
            Position position, Vector3D keySelectionSize, float multiplicityWidth) {
        float x = currentPosition.getX();
        float y = currentPosition.getY();

        // gladly changing the anchor of the text area is sufficient in most cases
        switch (position) {
            case BOTTOM:
                textView.setAnchor(PositionAnchor.UPPER_RIGHT);
                x += TEXT_OFFSET_FROM_ASSOCIATION;
                y += TEXT_OFFSET_FROM_CLASSIFIER + multiplicityWidth + keySelectionSize.getY();
                break;
            case TOP:
                textView.setAnchor(PositionAnchor.UPPER_RIGHT);
                x -= TEXT_OFFSET_FROM_ASSOCIATION;
                y -= TEXT_OFFSET_FROM_CLASSIFIER + multiplicityWidth + keySelectionSize.getY();
                break;
            case LEFT:
                textView.setAnchor(PositionAnchor.UPPER_RIGHT);
                x -= TEXT_OFFSET_FROM_CLASSIFIER + multiplicityWidth + keySelectionSize.getX();
                y += TEXT_OFFSET_FROM_ASSOCIATION;
                break;
            case RIGHT:
                textView.setAnchor(PositionAnchor.UPPER_LEFT);
                x += TEXT_OFFSET_FROM_CLASSIFIER + multiplicityWidth + keySelectionSize.getX();
                y += TEXT_OFFSET_FROM_ASSOCIATION;
                break;
        }

        textView.setPositionRelativeToParent(new Vector3D(x, y));
    }

    /**
     * Moves the multiplicity to the proper position.
     *
     * @param textView the view of the multiplicity
     * @param currentPosition the current position of the multiplicity
     * @param position the side of the association end on the attached classifier view
     */
    @SuppressWarnings("static-method")
    private void moveQualifier(TextView textView, Vector3D currentPosition, Position position) {
        float x = currentPosition.getX();
        float y = currentPosition.getY();

        // gladly changing the anchor of the text area is sufficient in most cases
        switch (position) {
            case BOTTOM:
                textView.setAnchor(PositionAnchor.LOWER_LEFT);
                x += textView.getHeight() / 2;
                break;
            case TOP:
                textView.setAnchor(PositionAnchor.LOWER_LEFT);
                x -= textView.getHeight() / 2;
                break;
            case LEFT:
                textView.setAnchor(PositionAnchor.UPPER_LEFT);
                y -= textView.getHeight() / 2;
                break;
            case RIGHT:
                textView.setAnchor(PositionAnchor.UPPER_RIGHT);
                y -= textView.getHeight() / 2;
                break;
        }

        textView.setPositionRelativeToParent(new Vector3D(x, y));
    }

    /**
     * Creates a view for the role name.
     *
     * @param associationEnd the end a role name view to create for
     * @return a view for the role name
     */
    private TextView createRoleNameView(AssociationEnd associationEnd) {
        TextView roleName = new TextView(associationEnd, CdmPackage.Literals.NAMED_ELEMENT__NAME);
        // In rare cases the role name could be empty (issue #72).
        roleName.setPlaceholderText(Strings.PH_ENTER_ROLE_NAME);

        ITextViewHandler roleNameHandler = ClassDiagramHandlerFactory.INSTANCE.getAssociationRoleNameHandler();
        registerTextViewProcessors(roleName, roleNameHandler, true);

        roleName.setFont(Fonts.getSmallFontByColor(drawColor));
        roleName.setUnderlined(associationEnd.isStatic());
        roleName.setBufferSize(Cardinal.SOUTH, 0);
        addChild(roleName);

        return roleName;
    }

    /**
     * Creates a view for the multiplicity.
     *
     * @param associationEnd the end a multiplicity view to create for
     * @return a view for the multiplicity
     */
    private MultiplicityTextView createMultiplicityView(AssociationEnd associationEnd) {
        MultiplicityTextView multiplicity = new MultiplicityTextView(associationEnd);

        ITextViewHandler multiplicityHandler = ClassDiagramHandlerFactory.INSTANCE.getAssociationMultiplicityHandler();
        registerTextViewProcessors(multiplicity, multiplicityHandler, true);

        multiplicity.setFont(Fonts.getSmallFontByColor(drawColor));
        multiplicity.setBufferSize(Cardinal.SOUTH, 0);
        addChild(multiplicity);

        return multiplicity;
    }

    /**
     * Creates a view for the feature selection.
     *
     * @param associationEnd the end a feature selection view to create for
     * @return a view for the feature selection
     */
    private TextView createPropertyView(AssociationEnd associationEnd) {
        TextView property = new AssociationPropertyView(associationEnd);
        
        ITextViewHandler propertyHandler = 
                ClassDiagramHandlerFactory.INSTANCE.getAssociationPropertyHandler();
        registerTextViewProcessors(property, propertyHandler, false);
        property.setFont(Fonts.getSmallFontByColor(drawColor));
        addChild(property);

        if (getFromEnd().getModel() == associationEnd
                && lastFromPosition != null) {
            rotateText(property, lastFromPosition, ROTATION_FORWARD);
        } else if (getToEnd().getModel() == associationEnd
                && lastToPosition != null) {
            rotateText(property, lastToPosition, ROTATION_FORWARD);
        }

        return property;
    }

    /**
     * Creates a view for the Association End Qualifier.
     *
     * @param associationEnd the association end to qualify
     * @return a view for the qualifier
     */
    private TextView createQualifierView(AssociationEnd associationEnd) {
        QualifierView qualifierView = new QualifierView(associationEnd);
        qualifierView.setPlaceholderText(Strings.PH_KEY);
        
        registerTextViewProcessors(qualifierView, new QualifierViewHandler(), true);

        addChild(qualifierView);
        
        // Since the qualifier is shown on the other side, 
        // the opposite position needs to be used.
        // we need to use the opposite here.
        if (getFromEnd().getModel() == associationEnd
                && lastToPosition != null) {
            rotateText(qualifierView, lastToPosition, ROTATION_FORWARD);
        } else if (getToEnd().getModel() == associationEnd
                && lastFromPosition != null) {
            rotateText(qualifierView, lastFromPosition, ROTATION_FORWARD);
        }

        return qualifierView;
    }

    @Override
    public void destroy() {
        EMFEditUtil.removeListenerFor(getFromEnd().getModel(), this);
        super.destroy();
    }

    /**
     * Draws the correct polygon for an association end at the given location and position on the class.
     *
     * @param end the end to draw
     * @param x the x position of the end
     * @param y the y position of the end
     * @param position the side of the end on the view
     */
    private void drawAssociationEnd(AssociationEnd end, float x, float y, Position position) {
        ReferenceType type = end.getReferenceType();
        MTPolygon polygon = null;
        MTPolygon polygon2 = null;
        //Check if operations are allowed, if not then it's a domain model and we don't display arrows
        boolean isArrowVisible = PerspectiveControllerFactory.INSTANCE.getActionValidator()
                .canCreateOperation(COREPerspectiveUtil.INSTANCE.getModelPerspective(end));

        switch (type) {
            case AGGREGATION:
                polygon = new CompositionPolygon(x, y, drawColor);
                rotateShape(polygon, x, y, position);
                polygon.setFillColor(backgroundColor);

                if (!end.isNavigable()) {
                    polygon2 = getShiftedRotatedArrowPolygon(x, y, drawColor, position, isArrowVisible);

                }
                break;
            case COMPOSITION:
                polygon = new CompositionPolygon(x, y, drawColor);
                rotateShape(polygon, x, y, position);

                if (!end.isNavigable()) {
                    polygon2 = getShiftedRotatedArrowPolygon(x, y, drawColor, position, isArrowVisible);

                }

                break;
            case REGULAR:
                if (!end.isNavigable()) {
                    polygon = new ArrowPolygon(x, y, drawColor, isArrowVisible);
                    rotateShape(polygon, x, y, position);
                }
                break;
        }

        if (polygon != null) {
            addChild(polygon);
        }
        
        if (polygon2 != null) {
            addChild(polygon2);
        }
    }

    /**
     * Draws the given association end based on all information contained inside.
     *
     * @param cdEnd the end to draw
     */
    private void drawAssociationEnd(CDEnd<AssociationEnd, LinkableView<?>> cdEnd) {
        drawAssociationEnd(cdEnd.getModel(), cdEnd.getLocation().getX(),
                cdEnd.getLocation().getY(), cdEnd.getPosition());
    }

    /**
     * This function is used to draw a navigability arrow head along with the aggregation/composition shape. It happens
     * when the reference type is aggregation or composition and navigability is negative.
     *
     * @param x the x position
     * @param y the y position
     * @param drawColor the color
     * @param position the current position
     * @param isArrowVisible if the arrows show be visible or not
     * @return a new polygon that is properly positioned
     */
    private MTPolygon getShiftedRotatedArrowPolygon(float x, float y, MTColor drawColor, Position position, 
            boolean isArrowVisible) {
        MTPolygon polygon = null;

        Vector3D rotationPoint = null;
        // rotate depending on position
        switch (position) {
            case BOTTOM:
                polygon = new ArrowPolygon(x, y + ARROW_SIZE, drawColor, isArrowVisible);
                rotationPoint = new Vector3D(x, y + ARROW_SIZE, 1);
                polygon.rotateZ(rotationPoint, ROTATION);
                break;
            case TOP:
                polygon = new ArrowPolygon(x, y - ARROW_SIZE, drawColor, isArrowVisible);
                rotationPoint = new Vector3D(x, y - ARROW_SIZE, 1);
                polygon.rotateZ(rotationPoint, -ROTATION);
                break;

            case LEFT:
                polygon = new ArrowPolygon(x - ARROW_SIZE, y, drawColor, isArrowVisible);
                rotationPoint = new Vector3D(x - ARROW_SIZE, y, 1);
                polygon.rotateZ(rotationPoint, 2 * ROTATION);
                break;

            case RIGHT:
                polygon = new ArrowPolygon(x + ARROW_SIZE, y, drawColor, isArrowVisible);
                // rotationPoint = new Vector3D(x + 20, y, 1);
                break;
        }

        return polygon;
    }

    @Override
    public void notifyChanged(Notification notification) {
        if (notification.getNotifier() == getFromEnd().getModel()
                || notification.getNotifier() == getToEnd().getModel()) {
            if (notification.getFeature() == CdmPackage.Literals.STRUCTURAL_FEATURE__STATIC) {
                if (notification.getEventType() == Notification.SET) {
                    boolean newValue = notification.getNewBooleanValue();
                    if (notification.getNotifier() == getFromEnd().getModel()) {
                        fromEndRolename.setUnderlined(newValue);
                    } else {
                        toEndRolename.setUnderlined(newValue);
                    }
                }
            } else if (notification.getFeature() == CdmPackage.Literals.ASSOCIATION_END__REFERENCE_TYPE) {
                AssociationEnd associationEnd = (AssociationEnd) notification.getNotifier();
                if (associationEnd == getFromEnd().getModel()) {
                    if (notification.getEventType() == Notification.SET
                            && notification.getOldValue() == ReferenceType.QUALIFIED
                            && notification.getNewValue() != ReferenceType.QUALIFIED) {
                        
                        COREPerspective perspective = COREPerspectiveUtil.INSTANCE.getModelPerspective(associationEnd);
                        switch (perspective.getName()) {
                            case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE: 
                            case PerspectiveName.DESIGN_MODEL_PERSPECTIVE: 
                                PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController()
                                    .removeQualifier(associationEnd);
                                break;
                        } 
                        
                        destroyKeySelection(FROM);
                    }
                } else if (associationEnd == getToEnd().getModel()) {
                    if (notification.getEventType() == Notification.SET
                            && notification.getOldValue() == ReferenceType.QUALIFIED
                            && notification.getNewValue() != ReferenceType.QUALIFIED) {
                        COREPerspective perspective = COREPerspectiveUtil.INSTANCE.getModelPerspective(associationEnd);
                        switch (perspective.getName()) {
                            case PerspectiveName.DOMAIN_MODEL_PERSPECTIVE: 
                            case PerspectiveName.DESIGN_MODEL_PERSPECTIVE: 
                                PerspectiveControllerFactory.INSTANCE.getCdmPerspectiveController()
                                    .removeQualifier(associationEnd);
                                break;
                        } 
                        
                        destroyKeySelection(TO);
                    }
                }
                updateLines();
            } else if (notification.getFeature() == CdmPackage.Literals.ASSOCIATION_END__UPPER_BOUND) {
                if (notification.getOldIntValue() == 1
                        && (notification.getNewIntValue() > 1 || notification.getNewIntValue() == -1)) {
                    if (notification.getNotifier() == getFromEnd().getModel()) {
                        if (fromEndProperty == null) {
                            fromEndProperty =
                                    createPropertyView((AssociationEnd) notification.getNotifier());
                        }
                    } else {
                        if (toEndProperty == null) {
                            toEndProperty =
                                    createPropertyView((AssociationEnd) notification.getNotifier());
                        }
                    }
                } else if ((notification.getOldIntValue() > 1 || notification.getOldIntValue() == -1)
                        && notification.getNewIntValue() == 1) {
                    if (notification.getNotifier() == getFromEnd().getModel()) {
                        destroyFeatureSelection(FROM);
                    } else if (notification.getNotifier() == getToEnd().getModel()) {
                        destroyFeatureSelection(TO);
                    }
                    shouldUpdate();
                }
                updateLines();
            } else if (notification.getFeature() == CdmPackage.Literals.ASSOCIATION_END__QUALIFIER) {
                AssociationEnd associationEnd = (AssociationEnd) notification.getNotifier();
                if (associationEnd == getFromEnd().getModel()) {
                    if (notification.getEventType() == Notification.SET) {
                        destroyKeySelection(FROM);
                        fromEndQualifier = createQualifierView(associationEnd);
                        updateLines();
                    }
                } else if (associationEnd == getToEnd().getModel()) {
                    if (notification.getEventType() == Notification.SET) {
                        destroyKeySelection(TO);
                        toEndQualifier = createQualifierView(associationEnd);
                        updateLines();
                    }
                }
                shouldUpdate();
            } else if (notification.getEventType() == Notification.SET) {
                shouldUpdate();
            }
        }
    }

    /**
     * Adds listeners to the textview parameter.
     *
     * @param textView The field where the listeners occurred
     * @param handler The text view handler to add
     * @param enabledTapAndHold If the tapAndHold event is enabled for this textview
     */
    private void registerTextViewProcessors(TextView textView, ITextViewHandler handler, boolean enabledTapAndHold) {

        textView.registerTapProcessor(handler);

        if (enabledTapAndHold) {
            textView.registerInputProcessor(new TapAndHoldProcessor(RamApp.getApplication(),
                    GUIConstants.TAP_AND_HOLD_DURATION));
            textView.addGestureListener(TapAndHoldProcessor.class,
                    new TapAndHoldVisualizer(RamApp.getApplication(), this));
            textView.addGestureListener(TapAndHoldProcessor.class, handler);
        }
    }

    /**
     * Redraws the lines between the to and from {@link AssociationEnd}s.
     */
    @Override
    protected void update() {
        // Calculate the offset required to move the lines and update the location of each end accordingly.
        Vector3D fromEndOffset = getQualifierOffset(fromEndQualifier, getFromEnd());
        Vector3D toEndOffset = getQualifierOffset(toEndQualifier, getToEnd());
        fromEnd.getLocation().addLocal(fromEndOffset);
        toEnd.getLocation().addLocal(toEndOffset);
        drawAllLines();
        
        drawAssociationEnd(getFromEnd());
        drawAssociationEnd(getToEnd());

        // create texts if they don't exist yet
        createTexts(getFromEnd(), getToEnd());

        // update the texts position
        moveTexts(getFromEnd(), fromEndRolename, fromEndMultiplicity, fromEndProperty, toEndQualifier,
                lastFromPosition);
        moveTexts(getToEnd(), toEndRolename, toEndMultiplicity, toEndProperty, fromEndQualifier,
                lastToPosition);

        lastFromPosition = getToEnd().getPosition();
        lastToPosition = getFromEnd().getPosition();

        // update visibility depending on navigable
        boolean fromEndNavigable = getFromEnd().getModel().isNavigable();
        fromEndRolename.setVisible(fromEndNavigable);
        fromEndMultiplicity.setVisible(fromEndNavigable);
        if (fromEndProperty != null) {
            fromEndProperty.setVisible(fromEndNavigable);
        }
        if (fromEndQualifier != null) {
            fromEndQualifier.setVisible(fromEndNavigable);
        }

        boolean toEndNavigable = getToEnd().getModel().isNavigable();
        toEndRolename.setVisible(toEndNavigable);
        toEndMultiplicity.setVisible(toEndNavigable);
        if (toEndProperty != null) {
            toEndProperty.setVisible(toEndNavigable);
        }
        if (toEndQualifier != null) {
            toEndQualifier.setVisible(toEndNavigable);
        }
        
        if (getFromEnd().getComponentView() instanceof ClassifierView) {
            ((ClassifierView<?>) getFromEnd().getComponentView()).updateSpacerSize();
        }
        if (getToEnd().getComponentView() instanceof ClassifierView) {
            ((ClassifierView<?>) getToEnd().getComponentView()).updateSpacerSize();
        }
    }

    /**
     * Destroy feature selection.
     *
     * @param side The side (FROM or TO) to delete the selections for
     */
    private void destroyFeatureSelection(String side) {
        if (FROM.equals(side)) {
            if (fromEndProperty != null) {
                fromEndProperty.destroy();
                fromEndProperty = null;
            }
        } else if (TO.equals(side)) {
            if (toEndProperty != null) {
                toEndProperty.destroy();
                toEndProperty = null;
            }
        }
    }

    /**
     * Destroy key selection.
     *
     * @param side The side (FROM or TO) to delete the selections for
     */
    private void destroyKeySelection(String side) {
        //TODO fix
        if (FROM.equals(side)) {
            if (fromEndQualifier != null) {
                fromEndQualifier.destroy();
                fromEndQualifier = null;
            }
        } else if (TO.equals(side)) {
            if (toEndQualifier != null) {
                toEndQualifier.destroy();
                toEndQualifier = null;
            }
        }
    }

    /**
     * The offset of the association qualifier if there is one.
     *
     * @param textView The key selection text view to calculate the offset for
     * @param cdEnd The ram end for which there might be an offset
     * @return the of the offset
     */
    private static Vector3D getQualifierOffset(TextView textView, CDEnd<AssociationEnd, LinkableView<?>> cdEnd) {
        Vector3D offset = new Vector3D();
        if (textView == null) {
            offset.setX(0);
            offset.setY(0);
        } else {
            switch (cdEnd.getPosition()) {
                case BOTTOM:
                    offset.setX(0);
                    offset.setY(textView.getWidth());
                    break;
                case TOP:
                    offset.setX(0);
                    offset.setY(-textView.getWidth());
                    break;
                case LEFT:
                    offset.setX(-textView.getWidth());
                    offset.setY(0);
                    break;
                case RIGHT:
                    offset.setX(textView.getWidth());
                    offset.setY(0);
                    break;
            }
        }
        return offset;
    }
}
