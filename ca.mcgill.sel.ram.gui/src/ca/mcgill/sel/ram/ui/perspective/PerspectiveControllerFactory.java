package ca.mcgill.sel.ram.ui.perspective;

import ca.mcgill.sel.core.perspective.ActionValidator;
import ca.mcgill.sel.ram.ui.perspective.classdiagram.CdmPerspectiveController;

/**
 * A factory to obtain all perspectiveControllers related to COREPerspective.
 * @author hyacinthali
 *
 */
public final class PerspectiveControllerFactory {
    
    /**
     * The singleton instance of this factory.
     * 
     */
    public static final PerspectiveControllerFactory INSTANCE = new PerspectiveControllerFactory();

    private ActionValidator actionValidator;
    private CdmPerspectiveController cdmPerspectiveController;

    /**
     * Creates a new instance of {@link PerspectiveControllerFactory}.
     */
    private PerspectiveControllerFactory() {

    }

    /**
     * Returns the controller for {@link ActionValidator}s.
     *
     * @return the controller for {@link ActionValidator}s
     */
    public ActionValidator getActionValidator() {
        if (actionValidator == null) {
            actionValidator = new ActionValidator();
        }
        return actionValidator;
    }
        
    /**
     * Returns the controller for {@link CdmPerspectiveController}s.
     *
     * @return the controller for {@link CdmPerspectiveController}s
     */
    public CdmPerspectiveController getCdmPerspectiveController() {
        if (cdmPerspectiveController == null) {
            cdmPerspectiveController = new CdmPerspectiveController();
        }
        return cdmPerspectiveController;
    }

}
