package ca.mcgill.sel.ram.ui.perspective.classdiagram;

import java.util.List;
import java.util.Set;

import ca.mcgill.sel.classdiagram.Association;
import ca.mcgill.sel.classdiagram.AssociationEnd;
import ca.mcgill.sel.classdiagram.Attribute;
import ca.mcgill.sel.classdiagram.CDEnum;
import ca.mcgill.sel.classdiagram.CDEnumLiteral;
import ca.mcgill.sel.classdiagram.Class;
import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.classdiagram.Classifier;
import ca.mcgill.sel.classdiagram.ImplementationClass;
import ca.mcgill.sel.classdiagram.NamedElement;
import ca.mcgill.sel.classdiagram.Note;
import ca.mcgill.sel.classdiagram.ObjectType;
import ca.mcgill.sel.classdiagram.Operation;
import ca.mcgill.sel.classdiagram.Parameter;
import ca.mcgill.sel.classdiagram.ReferenceType;
import ca.mcgill.sel.classdiagram.StructuralFeature;
import ca.mcgill.sel.classdiagram.Type;
import ca.mcgill.sel.classdiagram.TypeParameter;
import ca.mcgill.sel.classdiagram.VisibilityType;
import ca.mcgill.sel.classdiagram.language.controller.ControllerFactory;

/**
 * Controller for calling delete language actions and its related perspective tasks.
 * @author hyacinthali
 *
 */
public class CdmPerspectiveController {
    /**
     * Creates a new instance of {@link CdmPerspectiveController}.
     */
    public CdmPerspectiveController() {
    }
    
    /**
     * Calls language action to create a new class as well as its layout element.
     * @param owner
     * @param name
     * @param dataType
     * @param isInterface
     * @param x
     * @param y
     */
    public void createNewClass(ClassDiagram owner, String name, boolean dataType, boolean isInterface,
            float x, float y) {
        ControllerFactory.INSTANCE.getClassDiagramController().createNewClass(owner,
                name, dataType, isInterface, x, y);

    }

    /**
     * Call language to remove the given classifier and potentially its associated model elements.
     * 
     * @param classifier the classifier that should be removed
     */
    public void removeClassifier(Classifier classifier) {
        ControllerFactory.INSTANCE.getClassDiagramController().removeClassifier(classifier);
    }

    /**
     * Calls language action to create a new note.
     * @param cd
     * @param x
     * @param y
     */
    public void createNewNote(ClassDiagram cd, float x, float y) {
        ControllerFactory.INSTANCE.getClassDiagramController().createNewNote(cd, x, y);
    }
    
    /**
     * Calls language action to remove the given note and its associated layout element.
     * @param note
     */
    public void removeNote(Note note) {
        ControllerFactory.INSTANCE.getClassDiagramController().removeNote(note);
    }

    /**
     * Calls language action to create an association between the two given classes.
     * The association ends have default values.
     *
     * @param owner the {@link ClassDiagram} the association should be added to
     * @param from the first class
     * @param to the second class
     * @param bidirectional whether the association should be bidirectional or not. If to or the from
     *            is an implementation class, this is not taken in account.
     */
    public void createAssociation(ClassDiagram owner, Classifier from, Classifier to, boolean bidirectional) {
        ControllerFactory.INSTANCE.getClassDiagramController().createAssociation(owner, from, to,
                bidirectional);
    }

    /**
     * Calls language action to delete the given {@link Association}.
     *
     * @param cd the current {@link ClassDiagram}
     * @param association the {@link Association} to delete
     */
    public void deleteAssociation(ClassDiagram cd, Association association) {
        ControllerFactory.INSTANCE.getAssociationController().deleteAssociation(cd, association);
    }

    /**
     * Calls language action to set the qualifier for the association end.
     * @param associationEnd
     * @param qualifierType
     */
    public void setQualifier(AssociationEnd associationEnd, Type qualifierType) {
        ControllerFactory.INSTANCE.getAssociationController().setQualifier(associationEnd, qualifierType);
    }

    /**
     * Calls language action to remove the qualifier for the association end.
     * 
     * @param associationEnd The {@link AssociationEnd} from which to remove the qualifier
     */
    public void removeQualifier(AssociationEnd associationEnd) {
        ControllerFactory.INSTANCE.getAssociationController().removeQualifier(associationEnd);
    }

    /**
     * Calls language action to set the multiplicity of the given {@link AssociationEnd} according
     * to the given lower and upper bound.
     *
     * @param cd the current {@link ClassDiagram}
     * @param associationEnd the {@link AssociationEnd} the multiplicity should be changed for
     * @param lowerBound the lower bound of the association end
     * @param upperBound the upper bound of the association end, -1 for many
     */
    public void setMultiplicity(ClassDiagram cd, AssociationEnd associationEnd, int lowerBound, int upperBound) {
        ControllerFactory.INSTANCE.getAssociationController().setMultiplicity(cd, associationEnd,
                lowerBound, upperBound);
    }

    /**
     * Calls language action to set the role name of the given {@link AssociationEnd}.
     *
     * @param cd the current {@link ClassDiagram}
     * @param associationEnd the {@link AssociationEnd} the role name should be changed for
     * @param roleName The new role name of the association end
     */
    public void setRoleName(ClassDiagram cd, AssociationEnd associationEnd, String roleName) {
        ControllerFactory.INSTANCE.getAssociationController().setRoleName(cd, associationEnd, roleName);
    }

    /**
     * Calls language action to switch the navigable property of the given association end.
     * In case the opposite association end isn't navigable, both of the ends are switched
     *
     * @param cd the current {@link ClassDiagram}
     * @param associationEnd the {@link AssociationEnd} the navigable property should be switched of
     * @param oppositeEnd the opposite {@link AssociationEnd} from which the navigable property should be switched of
     */
    public void switchNavigable(ClassDiagram cd, AssociationEnd associationEnd, AssociationEnd oppositeEnd) {
        ControllerFactory.INSTANCE.getAssociationController().switchNavigable(cd, associationEnd,
                oppositeEnd);
    }

    /**
     * Calls language action to set the reference type of the given association end to the new reference type.
     *
     * @param associationEnd the {@link AssociationEnd} the reference type should be changed of
     * @param referenceType the new {@link ReferenceType} to set
     */
    public void setReferenceType(AssociationEnd associationEnd, ReferenceType referenceType) {
        ControllerFactory.INSTANCE.getAssociationController().setReferenceType(associationEnd,
                referenceType);
    }

    /**
     * Calls language action to switch the static property of the given association end.
     *
     * @param associationEnd the {@link AssociationEnd} the static property should be switched of
     */
    public void switchStatic(AssociationEnd associationEnd) {
        ControllerFactory.INSTANCE.getAssociationController().switchStatic(associationEnd);
    }

    /**
     * Calls language action to switch the ordering property of the given association end.
     *
     * @param associationEnd the {@link AssociationEnd} the ordering property should be switched of
     */
    public void switchOrdering(AssociationEnd associationEnd) {
        ControllerFactory.INSTANCE.getAssociationController().switchOrdering(associationEnd);
    }

    /**
     * Calls language action Switches the uniqueness property of the given association end.
     *
     * @param associationEnd the {@link AssociationEnd} the uniqueness property should be switched of
     */
    public void switchUniqueness(AssociationEnd associationEnd) {
        ControllerFactory.INSTANCE.getAssociationController().switchUniqueness(associationEnd);
    }

    /**
     * Calls language action to create an Nary association between the given classes.
     * @param cd
     * @param associatedClassifiers
     * @param x
     * @param y
     * @param createAssociationClass
     */
    public void createNaryAssociation(ClassDiagram cd, Set<Classifier> associatedClassifiers, float x, float y,
            boolean createAssociationClass) {
        ControllerFactory.INSTANCE.getClassDiagramController().createNaryAssociation(cd,
                associatedClassifiers, x, y, createAssociationClass);
    }

    /**
     * Calls language action to create an enum and its layout element.
     *
     * @param owner the {@link ClassDiagram} the enum should be added to
     * @param name Name of the enum
     * @param x Position in the Structural view
     * @param y Position in the Structural view
     */
    public void createEnum(ClassDiagram owner, String name, float x, float y) {
        ControllerFactory.INSTANCE.getClassDiagramController().createEnum(owner, name, x, y);
    }
    
    /**
     * Calls language action to remove a specified REnumLiteral from the model.
     * @param literal REnumLiteral to be removed.
     */
    public void removeLiteral(CDEnumLiteral literal) {
        ControllerFactory.INSTANCE.getEnumController().removeLiteral(literal);
    }
    
    /**
     * Calls language action to create an REnum literal in the model.
     * @param owner REnum that will own this literal.
     * @param index Index where we should insert it.
     * @param name Name of the literal.
     * @return The created REnumLiteral.
     */
    public CDEnumLiteral createREnumLiteral(CDEnum owner, int index, String name) {
        return ControllerFactory.INSTANCE.getEnumController().createREnumLiteral(owner, index, name);
    }
    
    /**
     * Calls language action to removes the given {@link CDEnum} and its associated layout element.
     *
     * @param cdEnum the Enum that should be removed
     */
    public void deleteEnum(CDEnum cdEnum) {
        ControllerFactory.INSTANCE.getEnumController().deleteEnum(cdEnum);
    }
    
    /**
     * Calls language action to create an attribute.
     * @param owner
     * @param index
     * @param name
     * @param objectType
     */
    public void createAttribute(Class owner, int index, String name, ObjectType objectType) {
        ControllerFactory.INSTANCE.getClassController().createAttribute(owner, index, name, objectType);
    }
    
    /**
     * Set's the given attribute's position.
     *
     * @param attribute the attribute to be set
     * @param index the new index of the operation
     */
    public void setAttributePosition(Attribute attribute, int index) {
        ControllerFactory.INSTANCE.getClassController().setAttributePosition(attribute, index);
    }

    /**
     * Calls language action to set the association class of an association.
     * @param association
     * @param associationClass
     */
    public void setAssociationClass(Association association, Class associationClass) {
        ControllerFactory.INSTANCE.getAssociationController().setAssociationClass(association, associationClass);
    }

    /**
     * Calls language to remove the association class of an association, doesn't delete the Association Class.
     * @param association
     */
    public void removeAssociationClass(Association association) {
        ControllerFactory.INSTANCE.getAssociationController().removeAssociationClass(association);
    }

    /**
     * Calls language action to add the given classifier as a super type to the given class.
     * @param subType
     * @param superType
     */
    public void addSuperType(Class subType, Classifier superType) {
        ControllerFactory.INSTANCE.getClassController().addSuperType(subType, superType);
    }

    /**
     * Calls language action to remove the given classifier as the super type of the given class. 
     * @param classifier1
     * @param classifier2
     */
    public void removeSuperType(Classifier classifier1, Classifier classifier2) {
        ControllerFactory.INSTANCE.getClassController().removeSuperType(classifier1, classifier2);
    }

    /**
     * Calls language action to switch the abstract property of the given class.
     * @param clazz
     * @return returnValue
     */
    public boolean switchAbstract(Class clazz) {
        boolean returnValue = ControllerFactory.INSTANCE.getClassController().switchAbstract(clazz);
        return returnValue; 
    }
    
    /**
     * Calls language action to switches the static property of the given attribute.
     * @param attribute
     */
    public void switchAttributeStatic(Attribute attribute) {
        ControllerFactory.INSTANCE.getClassController().switchAttributeStatic(attribute);
    }

    /**
     * Calls language action to create a note link between the note and the given class.
     * @param owner
     * @param note
     * @param annotatedElement
     */
    public void createAnnotation(ClassDiagram owner, Note note, NamedElement annotatedElement) {
        ControllerFactory.INSTANCE.getClassDiagramController().createAnnotation(owner, note, 
                annotatedElement);
    }
    
    /**
     * Calls language action to remove all the annotations of the given note.
     * @param note
     */
    public void removeAnnotations(Note note) {
        ControllerFactory.INSTANCE.getClassDiagramController().removeAnnotations(note);
    }
    
    /**
     * Calls language action to remove the given attribute.
     *
     * @param attribute the attribute to be removed
     */
    public void removeAttribute(Attribute attribute) {
        ControllerFactory.INSTANCE.getClassController().removeAttribute(attribute);
    }

    /**
     * Calls language action to create a new implementation class.
     * @param owner
     * @param name
     * @param generics
     * @param isInterface
     * @param isAbstract
     * @param x
     * @param y
     * @param superTypes
     * @param subTypes
     */
    // CHECKSTYLE:IGNORE ParameterNumberCheck: Can't split, because it is an API method and all information is required.
    public void createImplementationClass(ClassDiagram owner, String name, List<TypeParameter> generics,
            boolean isInterface, boolean isAbstract,
            float x, float y, Set<String> superTypes, Set<ImplementationClass> subTypes) {
        ControllerFactory.INSTANCE.getClassDiagramController().createImplementationClass(owner, name,
                generics, isInterface, isAbstract, x, y, superTypes, subTypes);

    }
    
    /**
     * Creates a new implementation enum.
     * @param owner
     * @param name
     * @param instanceClassName
     * @param x
     * @param y
     * @param literals
     */
    public void createImplementationEnum(ClassDiagram owner, String name, String instanceClassName, float x, float y,
            List<String> literals) {
        ControllerFactory.INSTANCE.getClassDiagramController().createImplementationEnum(
                owner, name, instanceClassName, x, y, literals);

    }

    /**
     * Adds an operation to an existing implementation class.
     * @param owner
     * @param index
     * @param name
     * @param visibility
     * @param returnType
     * @param parameters
     * @param isStatic
     */
    public void addOperation(ImplementationClass owner, int index, String name, VisibilityType visibility,
            Type returnType, List<Parameter> parameters, boolean isStatic) {
        ControllerFactory.INSTANCE.getImplementationClassController().addOperation(
                owner, index, name, visibility, returnType, parameters, isStatic);
    }
    
    /**
     * Set's the given operation's position.
     *
     * @param operation the operation to be set
     * @param index the new index of the operation
     */
    public void setOperationPosition(Operation operation, int index) {
        ControllerFactory.INSTANCE.getClassController().setOperationPosition(operation, index);
    }
    
    /**
     * Adds a constructor to an existing implementation class.
     * @param owner
     * @param name
     * @param visibility
     * @param parameters
     */
    public void addConstructor(ImplementationClass owner, String name, VisibilityType visibility,
            List<Parameter> parameters) {
        ControllerFactory.INSTANCE.getImplementationClassController().addConstructor(
                owner, name, visibility, parameters);
    }

    /**
     * Sets the type parameter's generic type.
     * @param owner
     * @param typeParameter
     * @param genericType
     * @param superTypes
     * @param subTypes
     */
    public void setTypeParameterType(ImplementationClass owner, TypeParameter typeParameter, ObjectType genericType,
            Set<String> superTypes, Set<ImplementationClass> subTypes) {
        ControllerFactory.INSTANCE.getImplementationClassController().setTypeParameterType(
                owner, typeParameter, genericType, superTypes, subTypes);
    }

    /**
     * Removes the given operation.
     * @param operation
     */
    public void removeOperation(Operation operation) {
        ControllerFactory.INSTANCE.getClassController().removeOperation(operation);
    }

    /**
     * Creates a new operation.
     * @param owner
     * @param index
     * @param name
     * @param visibility
     * @param returnType
     * @param parameters
     */
    public void createOperation(Class owner, int index, String name, VisibilityType visibility,
            Type returnType, List<Parameter> parameters) {
        ControllerFactory.INSTANCE.getClassController().createOperation(owner, index, name, visibility, 
                returnType, parameters);
    }

    /**
     * Changes the visibility of the classifier accordingly when the visibility of an operation is changed to public.
     * @param owner
     * @param operation
     * @param visibility
     */
    public void changeOperationAndClassVisibility(Classifier owner, Operation operation, VisibilityType visibility) {
        ControllerFactory.INSTANCE.getClassController().changeOperationAndClassVisibility(owner, operation,
                visibility);
    }
    
    /**
     * Creates an operation which gets the name of an attribute.
     * @param attribute
     */
    public void createGetterOperation(StructuralFeature attribute) {
        ControllerFactory.INSTANCE.getClassController().createGetterOperation(attribute);
    }

    /**
     * Creates an operation which sets the name of an attribute.
     * @param attribute
     */
    public void createSetterOperation(StructuralFeature attribute) {
        ControllerFactory.INSTANCE.getClassController().createSetterOperation(attribute);
    }

    /**
     * Creates a new constructor for the given class.
     * @param owner
     */
    public void createConstructor(Class owner) {
        ControllerFactory.INSTANCE.getClassController().createConstructor(owner);
    }
    
    /**
     * Creates a new destructor for the given class.
     * @param owner
     */
    public void createDestructor(Class owner) {
        ControllerFactory.INSTANCE.getClassController().createDestructor(owner);
    }
   
    /**
     * Creates a new parameter.
     * @param owner
     * @param index
     * @param name
     * @param type
     */
    public void createParameter(Operation owner, int index, String name, Type type) {
        ControllerFactory.INSTANCE.getClassController().createParameter(owner, index, name, type);
    }

    /**
     * Removes the given parameter.
     * @param parameter
     */
    public void removeParameter(Parameter parameter) {
        ControllerFactory.INSTANCE.getClassController().removeParameter(parameter);
    }

    /**
     * Switches the abstract property of the given operation.
     * @param operation
     */
    public void switchOperationAbstract(Operation operation) {
        ControllerFactory.INSTANCE.getClassController().switchOperationAbstract(operation);
    }

    /**
     * Switches the static property of the given operation.
     * @param operation
     */
    public void switchOperationStatic(Operation operation) {
        ControllerFactory.INSTANCE.getClassController().switchOperationStatic(operation);
    }
}
