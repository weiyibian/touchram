package ca.mcgill.sel.ram.ui.layouts;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.mt4j.components.MTComponent;
import org.mt4j.components.TransformSpace;
import org.mt4j.util.math.Vector3D;

import com.mxgraph.layout.mxGraphLayout;
import com.mxgraph.layout.hierarchical.mxHierarchicalLayout;
import com.mxgraph.model.mxCell;
import com.mxgraph.view.mxGraph;

import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.LayoutElement;
import ca.mcgill.sel.ram.RamFactory;
import ca.mcgill.sel.ram.StructuralView;
import ca.mcgill.sel.ram.controller.ControllerFactory;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.views.RelationshipView;
import ca.mcgill.sel.ram.ui.views.structural.BaseView;
import ca.mcgill.sel.ram.ui.views.structural.ClassView;
import ca.mcgill.sel.ram.ui.views.structural.ImplementationClassView;

/**
 * A Smart layout that uses the layouting properties of jgraphx and arranges views hierarchically if they have
 * edges and in lines to the right if they do not.
 * 
 * @author burns
 *
 */
public class AutomaticLayout extends AbstractBaseLayout {
    
    private static boolean isSelected;
    private float padding;

    /**
     * Creates a new automatic layout and has no padding. 
     */
    public AutomaticLayout() {
        this(0);
    }
    
    /**
     * Creates a new horizontal layout which is centered horizontally and has the given padding.
     * 
     * @param padding the padding to use to separate components
     */
    public AutomaticLayout(float padding) {
        this.padding = padding;
    }


    @Override
    public void layout(RamRectangleComponent component, LayoutUpdatePhase updatePhase) {
        mxGraph graph = new mxGraph();
        mxGraph lonerGraph = new mxGraph();
        Object parent = graph.getDefaultParent();
        ArrayList<Object> loners = new ArrayList<Object>();
        
        graph.getModel().beginUpdate();
        
        try {
            loners = buildGraph(parent, graph, component);
            mxGraphLayout newLayout = new mxHierarchicalLayout(graph);
            ((mxHierarchicalLayout) newLayout).setIntraCellSpacing(padding);
            ((mxHierarchicalLayout) newLayout).setInterRankCellSpacing(padding);
            ((mxHierarchicalLayout) newLayout).setInterHierarchySpacing(padding);
            setEdgesOrthogonal(newLayout, graph);
            newLayout.execute(parent);
        } finally {
            lonerGraph.getModel().beginUpdate();
            updateViews(graph, component);
            lonerGraph.addCells(loners.toArray());
            layoutLoners(loners, component);
            updateModel(component);
            graph.getModel().endUpdate();
        }
        if (updatePhase == LayoutUpdatePhase.FROM_CHILD && !getForceToKeepSize()) {
            setMinimumSize(component, getRight(component), getBottom(component));
        }
    }
    
    /**Builds a graph using the scene's views.
     * @param parent 
     * @param graph 
     * @param component 
     * @return Returns the array list of non attached vertices and builds the graph of attached ones.
     * 
     */
    private static ArrayList<Object> buildGraph(Object parent, mxGraph graph, RamRectangleComponent component) {
        ArrayList<Object> loners = new ArrayList<Object>();
        isSelected = false;
        
        for (MTComponent i : component.getChildren()) {
            if (!(i instanceof BaseView)) {
                continue;
            }
            if (((BaseView<?>) i).getIsSelected()) {
                isSelected = true;
                addVertex((BaseView<?>) i, parent, graph, null, loners, component);
            }
        }
        
        if (!isSelected) {
            for (MTComponent i : component.getChildren()) {
                if (!(i instanceof BaseView<?>)) {
                    continue;
                }
                addVertex((BaseView<?>) i, parent, graph, null, loners, component);
            }
        }
        return loners;
    }
    
    /**
     * Adds a vertex to the graph.
     * 
     * @param newVertex The vertex to be added
     * @param parent the parent of the graph
     * @param graph The graph to add to
     * @param newEdge The edge to add to the vertex (if it exists)
     * @param loners ArrayList of all vertices without edges
     * @param component Container view containing all views
     */
    private static void addVertex(BaseView<?> newVertex, Object parent, mxGraph graph, mxCell newEdge, 
            ArrayList<Object> loners, RamRectangleComponent component) {
        Object firstVertex;
        if (!isVertexAdded(newVertex, graph)) {
            firstVertex = graph.insertVertex(parent, Integer.toString(newVertex.getID()), null, newVertex.getX(), 
                    newVertex.getY(), newVertex.getWidth(), newVertex.getHeight());
    
    
            if (newVertex instanceof ImplementationClassView || newVertex instanceof ClassView) {
                Set<RelationshipView<?, ? extends RamRectangleComponent>> edges = null;
    
                if (newVertex instanceof ClassView) {
                    edges = ((ClassView) newVertex).getAllRelationshipViews();
                } else if (newVertex instanceof ImplementationClassView) {
                    edges = ((ImplementationClassView) newVertex).getAllRelationshipViews();
                }
                if (newEdge != null) {
                    newEdge.setTarget((mxCell) firstVertex);
                }
                
                if (edges.isEmpty()) {
                    graph.addSelectionCell(firstVertex);
                    loners.add(firstVertex);
                }
    
                for (RelationshipView<?, ?> edge : edges) {
                    if (edge.getToEnd().getComponentView().getID() == newVertex.getID()) {
                        mxCell edge1 = (mxCell) graph.insertEdge(parent, Integer.toString(newVertex.getID()), 
                                null, firstVertex, null);
                        addVertex((BaseView<?>) edge.getFromEnd().getComponentView(), 
                                parent, graph, edge1, loners, component);
                    }
                }
            }
        } else {
            if (newEdge != null) {
                for (Object v : graph.getChildVertices(graph.getDefaultParent())) {
                    mxCell vertex = (mxCell) v;
                    if (Objects.equals(vertex.getId(), Integer.toString(newVertex.getID()))) {
                        newEdge.setTarget(vertex);
                    }
                }
            }
        }
    }
    
    /**Updates Scene Views according to the layout algorithm.
     * @param graph 
     * @param component 
     * 
     */
    private void updateViews(mxGraph graph, RamRectangleComponent component) {
        float bottom = getBottom(component) + padding;
        for (Object i : graph.getChildCells(graph.getDefaultParent())) {
            mxCell c = (mxCell) i;
            for (MTComponent j : component.getChildren()) {
                if (!(j instanceof BaseView<?>) || j instanceof RelationshipView) {
                    continue;
                }
                BaseView<?> child = (BaseView<?>) j;
                if (Objects.equals(c.getId(), Integer.toString(child.getID()))) {
                    if (isSelected) {
                        child.setPositionRelativeToParent(new Vector3D((float) c.getGeometry().getX() + padding, 
                                (float) c.getGeometry().getY() + bottom, 0));
                    } else {
                        child.setPositionRelativeToParent(new Vector3D((float) c.getGeometry().getX() + padding, 
                            (float) c.getGeometry().getY() + padding, 0));
                    }
                }
            }
        }
    }
    
    /**
     * Updates the model with the new positions.
     * @param component the containerView holding the ClassViews
     */
    private static void updateModel(RamRectangleComponent component) {
        Map<Classifier, LayoutElement> positionMap = new HashMap<Classifier, LayoutElement>();
        StructuralView structuralView = null;
        for (MTComponent j : component.getChildren()) {
            if (!(j instanceof BaseView<?>) || j instanceof RelationshipView) {
                continue;
            }
            BaseView<?> child = (BaseView<?>) j;
            structuralView = child.getStructuralDiagramView().getStructuralView();
            Vector3D position = child.getPosition(TransformSpace.RELATIVE_TO_PARENT);
            LayoutElement layoutElement = RamFactory.eINSTANCE.createLayoutElement();
            layoutElement.setX(position.getX());
            layoutElement.setY(position.getY());
            positionMap.put(child.getClassifier(), layoutElement);
        }
        ControllerFactory.INSTANCE.getStructuralViewController().moveClassifiers(structuralView, positionMap);

    }
    
    
    /**
     * Checks if a vertex is already Added to the graph.
     * @param newVertex the vertex to check
     * @param graph graph
     * @return true if it is added
     */
    private static boolean isVertexAdded(BaseView<?> newVertex, mxGraph graph) {
        for (Object v : graph.getChildVertices(graph.getDefaultParent())) {
            mxCell vertex = (mxCell) v;
            if (Objects.equals(vertex.getId(), Integer.toString(newVertex.getID()))) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Sets all the edges in the graph to be orthogonal.
     * 
     * @param layout The layout to set the edges orthogonal
     * @param graph the graph that contains the edges
     * 
     */
    private static void setEdgesOrthogonal(mxGraphLayout layout, mxGraph graph) {
        for (Object i : graph.getChildCells(graph.getDefaultParent())) {
            mxCell cell = (mxCell) i;
            if (cell.isEdge()) {
                layout.setOrthogonalEdge(cell, true);
            }
        }
    }
    
    /**
     * Gets the rightmost coordinate of non selected BaseViews.
     * @param component the container layer of the diagram
     * @return returns the x coordinate of the right of the rightmost non selected class
     */
    private static float getRight(RamRectangleComponent component) {
        float right = 0;
        for (MTComponent i : component.getChildren()) {
            if (!(i instanceof BaseView)) {
                continue;
            } else {
                if (((BaseView<?>) i).getPosition(TransformSpace.RELATIVE_TO_PARENT).x > right 
                        && !((BaseView<?>) i).getIsSelected()) {
                    right = ((BaseView<?>) i).getPosition(TransformSpace.RELATIVE_TO_PARENT).x;
                }
            }
        }
        return right;
    }
    
    /**
     * Gets the bottom coordinate of non selected BaseViews.
     * @param component the container layer of the diagram
     * @return returns the y coordinate of the bottom of the lowest non selected class
     */
    private static float getBottom(RamRectangleComponent component) {
        float bottom = 0;
        for (MTComponent i : component.getChildren()) {
            if (!(i instanceof BaseView)) {
                continue;
            } else {
                if (((BaseView<?>) i).getPosition(TransformSpace.RELATIVE_TO_PARENT).y > bottom 
                        + ((BaseView<?>) i).getHeight() && !((BaseView<?>) i).getIsSelected()) {
                    bottom = ((BaseView<?>) i).getPosition(TransformSpace.RELATIVE_TO_PARENT).y 
                            + ((BaseView<?>) i).getHeight();
                }
            }
        }
        return bottom;
    }
    /**
     * Lays out the vertices without any edges to the right of the ones with.
     * 
     * @param loners An ArrayList containing the vertices with no edges
     * @param component The RamRectangleComponent who's children are the views we wish to layout
     * 
     */
    private void layoutLoners(ArrayList<Object> loners, RamRectangleComponent component) {
        if (loners.size() == 0) {
            return;
        }
        mxCell first = (mxCell) loners.get(0);
        float lastX = 0;
        for (MTComponent c : component.getChildren()) {
            if (!(c instanceof BaseView<?>) || c instanceof RelationshipView) {
                continue;
            }
            BaseView<?> child = (BaseView<?>) c;
            if (Objects.equals(first.getId(), Integer.toString(child.getID()))) {
                lastX = child.getPosition(TransformSpace.RELATIVE_TO_PARENT).x;
            }
        }
        
        for (Object i : loners) {
            mxCell vertex = (mxCell) i;
            if (vertex.getGeometry().getX() < lastX) {
                for (MTComponent c : component.getChildren()) {
                    if (!(c instanceof BaseView<?>) || c instanceof RelationshipView) {
                        continue;
                    }
                    BaseView<?> child = (BaseView<?>) c;
                    if (Objects.equals(vertex.getId(), Integer.toString(child.getID()))) {
                        first = vertex;
                        lastX = child.getPosition(TransformSpace.RELATIVE_TO_PARENT).x;
                    }
                }
            }
        }
        float startX = lastX;
        float maxHeight = -padding;
        float lastWidth = (float) first.getGeometry().getWidth();
        float newX = 0;
        float newY = padding;
        int rows = (int) Math.ceil(Math.sqrt(loners.size()));
        int index = 0;
        for (int i = 0; i < rows; i++) {
            lastWidth = -padding;
            newY = newY + maxHeight + padding;
            maxHeight = 0;
            lastX = startX;
            for (int j = 0; j < rows; j++) {
                if (index < loners.size()) {
                    mxCell vertex = (mxCell) loners.get(index);
                    if ((float) vertex.getGeometry().getHeight() > maxHeight) {
                        maxHeight = (float) ((mxCell) loners.get(index)).getGeometry().getHeight();
                    }
                    for (MTComponent c : component.getChildren()) {
                        if (!(c instanceof BaseView<?>) || c instanceof RelationshipView) {
                            continue;
                        }
                        BaseView<?> child = (BaseView<?>) c;
                        if (Objects.equals(vertex.getId(), Integer.toString(child.getID()))) {
                            newX = lastX + lastWidth + padding;
                            child.setPositionRelativeToParent(new Vector3D(newX, newY));
                            lastWidth = child.getWidth();
                            lastX = newX;
                        }
                    }
                }
                index++;
            }
        }   
    }
}