package ca.mcgill.sel.ram.ui.views.structural.handler.impl;

import java.io.File;

import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREModelExtension;
import ca.mcgill.sel.core.controller.COREControllerFactory;
import ca.mcgill.sel.core.controller.ReuseController;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.core.util.COREModelUtil;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamPopup.PopupType;
import ca.mcgill.sel.ram.ui.components.browser.GenericFileBrowser;
import ca.mcgill.sel.ram.ui.components.browser.interfaces.FileBrowserListener;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.structural.CompositionSplitEditingView;
import ca.mcgill.sel.ram.ui.views.structural.handler.ICompositionContainerViewHandler;

/**
 * Handles events for a {@link ca.mcgill.sel.ram.ui.views.structural.CompositionContainerView} which is showing the
 * list of compositions in an aspect.
 * 
 * @author eyildirim
 * @author oalam
 */
public class AspectExtensionsContainerViewHandler implements ICompositionContainerViewHandler {
    
    @Override
    public void deleteModelComposition(COREModelComposition modelComposition) {
        // Disallow deleting the COREModelComposition if split view is enabled.
        boolean splitModeEnabled =
                RamApp.getActiveAspectScene().getCurrentView() instanceof CompositionSplitEditingView;
        
        if (!splitModeEnabled) {
            COREControllerFactory.INSTANCE.getReuseController()
                .removeModelExtension((COREModelExtension) modelComposition);
        } else {
            RamApp.getActiveAspectScene().displayPopup(Strings.POPUP_CANT_DELETE_INST_EDIT);
        }
    }

    @Override
    public void loadBrowser(final COREArtefact mainAspect) {

        // Ask the user to load an aspect
        GenericFileBrowser.loadModel("ram", new FileBrowserListener() {

            @Override
            public void modelLoaded(final EObject model) {
                extendAspect(mainAspect, (Aspect) model);
            }

            @Override
            public void modelSaved(File file) {
            }
        });
    }
    

    @Override
    public void tailorExistingReuse(COREArtefact artefact) {
        // not implemented
        
    }

    /**
     * Create an extend relationship between the given aspect.
     * Does nothing if the creation is not valid.
     *
     * @param extendingArtefact - The aspect that extends
     * @param aspect - The aspect to extend.
     */
    private static void extendAspect(final COREArtefact extendingArtefact, final Aspect aspect) {
        RamApp.getApplication().invokeLater(new Runnable() {
            @SuppressWarnings("unlikely-arg-type")
            @Override
            public void run() {
                String errorMessage = null;
                Aspect extendingAspect = (Aspect) ((COREExternalArtefact) extendingArtefact).getRootModelElement();
                // Look for error cases.
                if (aspect.equals(extendingAspect)) {
                    errorMessage = Strings.POPUP_ERROR_SELF_EXTENDS;
                } else if (COREModelUtil.collectExtendedModels(aspect).contains(extendingAspect)) {
                    errorMessage = Strings.POPUP_ERROR_CYCLIC_EXTENDS;
                }
                // Check if this aspect is already extended. Cannot use collectExtendedAspects
                // because we don't want the check to be recursive here.
                for (COREModelComposition composition : extendingArtefact.getModelExtensions()) {
                    if (composition.getSource() == aspect) {
                        errorMessage = Strings.POPUP_ERROR_SAME_EXTENDS;
                        break;
                    }
                }
                if (errorMessage != null) {
                    RamApp.getActiveScene().displayPopup(errorMessage, PopupType.ERROR);
                    return;
                }
                // We can create the composition.
                ReuseController controller = COREControllerFactory.INSTANCE.getReuseController();
                controller.createModelExtension(extendingArtefact,
                        COREArtefactUtil.getReferencingExternalArtefact(aspect));
            }
        });
    }


}
