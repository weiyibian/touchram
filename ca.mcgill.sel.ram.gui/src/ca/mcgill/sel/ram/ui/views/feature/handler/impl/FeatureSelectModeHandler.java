package ca.mcgill.sel.ram.ui.views.feature.handler.impl;

import java.util.LinkedList;
import java.util.List;

import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;

import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.events.listeners.ITapAndHoldListener;
import ca.mcgill.sel.ram.ui.events.listeners.ITapListener;
import ca.mcgill.sel.ram.ui.scenes.DisplayConcernSelectScene;
import ca.mcgill.sel.ram.ui.scenes.DisplayConcernSelectScene.DisplayMode;
import ca.mcgill.sel.ram.ui.utils.SelectionsSingleton;
import ca.mcgill.sel.ram.ui.views.feature.FeatureView;
import ca.mcgill.sel.ram.ui.views.handler.BaseHandler;

/**
 * Handler class used to perform action on events read in the Select mode of the Feature Model Display.
 * 
 * @author Nishanth
 */
public class FeatureSelectModeHandler extends BaseHandler implements ITapListener, ITapAndHoldListener {

    @Override
    public boolean processTapEvent(final TapEvent tapEvent) {

        if (tapEvent.isTapped()) {

            final DisplayConcernSelectScene scene = RamApp.getActiveConcernSelectScene();
            final FeatureView featureView = (FeatureView) tapEvent.getTarget();
            if (scene == null) {
                return true;
            }
            if (SelectionsSingleton.getInstance().containsSelectedFeature(featureView.getSelectionFeature(), false)) {
                SelectionsSingleton.getInstance().removeSelection(featureView.getSelectionFeature());
            } else {
                SelectionsSingleton.getInstance().addSelection(featureView.getSelectionFeature());
            }
            
            scene.getReusePanel().setElements(scene.getReexposedReuses());
            if (!scene.getReusePanel().setSelectedReuse(scene.getCurrentSelectedReuse())) {
                scene.setCurrentSelectedReuse(null);
            }
            if (scene.getReuseDiagramView() != null) {
                scene.destroyReuseDiagram();
            }
            if (scene.getCurrentSelectedReuse() != null) {
                scene.handleNewSelectedReuse();
            }
            RamApp.getApplication().invokeLater(new Runnable() {
                @Override
                public void run() {
                    if (scene.getCurrentMode() == DisplayMode.FULL) {
                        scene.selectionChanged(true);
                    } else {
                        scene.drawFeatureDiagram(true);
                    }
                }
            });
        }
        return true;
    }

    @Override
    public boolean processTapAndHoldEvent(TapAndHoldEvent tapAndHoldEvent) {
        final DisplayConcernSelectScene scene = RamApp.getActiveConcernSelectScene();

        if (tapAndHoldEvent.isHoldComplete() && scene != null) {

            FeatureView featureIcon = (FeatureView) tapAndHoldEvent.getTarget();

            if (SelectionsSingleton.getInstance().containsDeReexposedFeature(featureIcon.getSelectionFeature(), true)) {
                // The feature or one of its parent was de-reexposed: re-expose those.
                FeatureView current = featureIcon;
                List<FeatureView> toReexpose = new LinkedList<FeatureView>();
                toReexpose.add(featureIcon);
                while (current.getParentFeatureView() != null) {
                    current = current.getParentFeatureView();
                    // We want to reexpose it again
                    toReexpose.add(current);
                    // The given feature was de-reexposed
                    if (SelectionsSingleton.getInstance()
                            .containsDeReexposedFeature(current.getSelectionFeature(), true)) {
                        // De-reexpose all its children, we only want the parent feature to be exposed
                        for (FeatureView child : current.getChildrenFeatureViews()) {
                            SelectionsSingleton.getInstance().removeReexposition(child.getSelectionFeature());
                        }
                    }
                }
                // Reexpose all needed features
                for (FeatureView view : toReexpose) {
                    SelectionsSingleton.getInstance().addReexposition(view.getSelectionFeature());
                }
            } else {
                SelectionsSingleton.getInstance().removeReexposition(featureIcon.getSelectionFeature());
            }
            
            scene.getReusePanel().setElements(scene.getReexposedReuses());
            // if the current reuse given is in the list
            if (!scene.getReusePanel().setSelectedReuse(scene.getCurrentSelectedReuse())) {
                scene.setCurrentSelectedReuse(null);
            }
            if (scene.getReuseDiagramView() != null) {
                scene.destroyReuseDiagram();
            }
            if (scene.getCurrentSelectedReuse() != null) {
                scene.handleNewSelectedReuse();
            }

            // Have to repopulate in case we are in next mode and a child of OR/XOR is auto-(de)selected
            RamApp.getApplication().invokeLater(new Runnable() {
                @Override
                public void run() {
                    if (scene.getCurrentMode() == DisplayMode.NEXT) {
                        scene.drawFeatureDiagram(true);
                    } else {
                        scene.selectionChanged(true);
                    }
                }
            });
        }

        return true;
    }

}
