package ca.mcgill.sel.ram.ui.views.structural.handler.impl;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;

import ca.mcgill.sel.core.CORECIElement;
import ca.mcgill.sel.core.COREMappingCardinality;
import ca.mcgill.sel.core.controller.COREControllerFactory;
import ca.mcgill.sel.core.controller.MappingCardinalityController;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamPopup.PopupType;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.impl.TextViewHandler;
import ca.mcgill.sel.ram.ui.views.structural.handler.IMappingCardinalityHandler;
import ca.mcgill.sel.ram.util.MetamodelRegex;

/**
 * Handler for the cardinality of a class. Handles the validation of the cardinality input and setting of
 * cardinality to the model in the form of upper and lower bound.
 *
 * @author jmilleret
 */
public class MappingCardinalityHandler extends TextViewHandler implements IMappingCardinalityHandler {

    @Override
    public void keyboardOpened(TextView textView) {
        // do nothing
    }
    
    @Override
    public boolean processTapEvent(TapEvent tapEvent) {
        if (tapEvent.isDoubleTap()) {
            TextView target = (TextView) tapEvent.getTarget();
            target.showKeyboard();
            return true;
        }

        return true;
    }
    
    /**
     * Set the cardinality of the element the {@link CORECIElement} contains.
     * 
     * @param cardinalityString - The string modified by the user
     * @param ciElement - The {@link CORECIElement} being modified
     * @return true if the cardinalityString is valid
     */
    private static boolean setCardinality(String cardinalityString, CORECIElement ciElement) {
        Matcher matcher = Pattern.compile(MetamodelRegex.REGEX_GROUP_MAPPING_CARDINALITIES).matcher(cardinalityString);
        if (matcher.matches()) {
            return setMappingCardinality(cardinalityString, ciElement, matcher);
        }
        matcher = Pattern.compile(MetamodelRegex.REGEX_GROUP_REFERENCE_CARDINALITIES).matcher(cardinalityString);
        if (matcher.matches()) {
            return setReferenceCardinality(cardinalityString, ciElement, matcher);
        }
        return false;
    }
    
    /**
     * Set a new / changing mapping cardinality.
     * 
     * @param cardinalityString - The string modified by the user
     * @param ciElement - The {@link CORECIElement} being modified
     * @param matcher - The {@link Matcher} used to parse the cardinalityString
     * @return true if there was no problem
     */
    private static boolean setMappingCardinality(String cardinalityString, 
            CORECIElement ciElement, Matcher matcher) {
        int lowerBound = Integer.parseInt(matcher.group("lowerBound"));
        // The upperBound can be a *
        String upperBoundString = matcher.group("upperBound");
        String name = matcher.group("name");
        int upperBound = "*".equals(upperBoundString) ? -1 : Integer.parseInt(upperBoundString);
        
        if (upperBound != -1 && lowerBound > upperBound) {
            return false;
        }
        
        if (name != null && gatherMappingCardinality(name, ciElement, false) != null) {
            RamApp.getActiveScene().displayPopup(Strings.popupCardinalityAlreadyExists(name),
                    PopupType.ERROR);
            return false;
        }
        
        MappingCardinalityController controller = COREControllerFactory.INSTANCE.getMappingCardinalityController();
        controller.setMappingCardinality(ciElement, name, lowerBound, upperBound);
        
        return true;
    }
    
    /**
     * Set a new reference cardinality.
     * 
     * @param cardinalityString - The string modified by the user
     * @param ciElement - The {@link CORECIElement} being modified
     * @param matcher - The {@link Matcher} used to parse the cardinalityString
     * @return true if there was no problem
     */
    private static boolean setReferenceCardinality(String cardinalityString, 
            CORECIElement ciElement, Matcher matcher) {
        Set<COREMappingCardinality> cardinalitiesRes = new HashSet<>();
        
        if (matcher.group("optionalName") == null) {
            String uniqueMapping = matcher.group("mandatoryName");
            COREMappingCardinality cardinality = gatherMappingCardinality(uniqueMapping, ciElement, false);
            if (cardinality == null) {
                RamApp.getActiveScene().displayPopup(Strings.popupCardinalityDoesNotExist(uniqueMapping),
                        PopupType.ERROR);
                return false;
            } else {
                cardinalitiesRes.add(cardinality);
            }
        } else {
            List<String> cardinalities = Arrays.asList(cardinalityString.split("\\*"));
            for (String cardinality : cardinalities) {
                COREMappingCardinality mappingCardinality = gatherMappingCardinality(cardinality, ciElement, false);
                if (mappingCardinality == null) {
                    RamApp.getActiveScene().displayPopup(Strings.popupCardinalityDoesNotExist(cardinality),
                            PopupType.ERROR);
                    return false;
                } else {
                    cardinalitiesRes.add(mappingCardinality);
                }
            }
        }
        
        // Create the old / new cardinalities list
        Set<COREMappingCardinality> oldCardinalities = new HashSet<>();
        Set<COREMappingCardinality> newCardinalities = new HashSet<>();
        
        for (COREMappingCardinality cardinality : cardinalitiesRes) {
            if (!ciElement.getReferenceCardinality().contains(cardinality)) {
                newCardinalities.add(cardinality);
            }
        }
        for (COREMappingCardinality cardinality : ciElement.getReferenceCardinality()) {
            if (!cardinalitiesRes.contains(cardinality)) {
                oldCardinalities.add(cardinality);
            }
        }
        
        if (!(newCardinalities.isEmpty() && oldCardinalities.isEmpty())) {
            MappingCardinalityController controller = 
                    COREControllerFactory.INSTANCE.getMappingCardinalityController();
            controller.setReferenceCardinalities(ciElement, newCardinalities, oldCardinalities);
            
        }
        return true;
    }
    
    /**
     * Gather the {@link COREMappingCardinality} the name given is referring to.
     * Check for all the {@link CORECIElement} mapped to the ciElement given.
     * 
     * @param name - The name referring to the {@link COREMappingCardinality} we want to gather
     * @param ciElement - The CORECIElement associated to the element we want to add the cardinality to.
     * @param includeSelf - If the result should include or not the ciElement given
     * @return the {@link COREMappingCardinality} retrieved. If none exists, return null;
     */
    private static COREMappingCardinality gatherMappingCardinality(String name, CORECIElement ciElement,
            boolean includeSelf) {
        Collection<CORECIElement> ciElements = 
                COREArtefactUtil.getAllCIElementAssociated(ciElement.getModelElement());
        if (!includeSelf) {
            ciElements.remove(ciElement);
        }
        for (CORECIElement ciElementGathered : ciElements) {
            COREMappingCardinality cardinality = ciElementGathered.getMappingCardinality();
            if (cardinality != null && cardinality.getName() != null && cardinality.getName().equals(name)) {
                return cardinality;
            }
        }
        return null;
    }
    
    
    @Override
    public boolean shouldDismissKeyboard(TextView textView) {
        String text = textView.getText();

        if (!text.matches(MetamodelRegex.REGEX_CARDINALITIES)) {
            return false;
        }

        return setCardinality(text, (CORECIElement) textView.getData());
    }

    @Override
    public boolean processTapAndHoldEvent(TapAndHoldEvent tapAndHoldEvent) {
        return false;
    }
}
