package ca.mcgill.sel.ram.ui.views.structural.handler.impl;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.mt4j.components.MTComponent;

import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.CORECIElement;
import ca.mcgill.sel.core.COREPartialityType;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.ram.AbstractMessageView;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.AspectMessageView;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.MessageView;
import ca.mcgill.sel.ram.MessageViewReference;
import ca.mcgill.sel.ram.Operation;
import ca.mcgill.sel.ram.RAMPartialityType;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.controller.ClassController;
import ca.mcgill.sel.ram.controller.ControllerFactory;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamButton;
import ca.mcgill.sel.ram.ui.components.RamImageComponent;
import ca.mcgill.sel.ram.ui.components.RamKeyboard;
import ca.mcgill.sel.ram.ui.components.RamPopup.PopupType;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent.Cardinal;
import ca.mcgill.sel.ram.ui.components.RamSelectorComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.components.listeners.DefaultRamKeyboardListener;
import ca.mcgill.sel.ram.ui.components.listeners.RamSelectorListener;
import ca.mcgill.sel.ram.ui.components.menu.RamLinkedMenu;
import ca.mcgill.sel.ram.ui.events.listeners.ActionListener;
import ca.mcgill.sel.ram.ui.scenes.DisplayAspectScene;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.SelectorView;
import ca.mcgill.sel.ram.ui.views.handler.BaseHandler;
import ca.mcgill.sel.ram.ui.views.handler.ILinkedMenuListener;
import ca.mcgill.sel.ram.ui.views.structural.OperationView;
import ca.mcgill.sel.ram.ui.views.structural.handler.IOperationViewHandler;
import ca.mcgill.sel.ram.util.MessageViewUtil;
import ca.mcgill.sel.ram.util.RAMModelUtil;

/**
 * The default handler for an {@link OperationView}.
 * 
 * @author mschoettle
 */
public class OperationViewHandler extends BaseHandler implements IOperationViewHandler, ILinkedMenuListener {

    private static final String ACTION_OPERATION_REMOVE = "view.operation.remove";
    private static final String ACTION_PARAMETER_ADD = "view.parameter.add";
    private static final String ACTION_OPERATION_STATIC = "view.operation.static";
    private static final String ACTION_OPERATION_ABSTRACT = "view.operation.abstract";
    private static final String ACTION_OPERATION_CONCERN_PARTIAL = "view.operation.concern.partial";
    private static final String ACTION_OPERATION_PUBLIC_PARTIAL = "view.operation.public.partial";
    private static final String ACTION_OPERATION_NOT_PARTIAL = "view.operation.not.partial";
    private static final String ACTION_OPERATION_CARDINALITY = "view.class.cardinality";
    private static final String ACTION_OPERATION_GOTO_MESSAGEVIEW = "view.operation.messageview";
    private static final String ACTION_OPERATION_ADVICE = "view.operation.advice";
    private static final String ACTION_OPERATION_SHIFT_UP = "view.operation.shiftUp";
    private static final String ACTION_OPERATION_SHIFT_DOWN = "view.operation.shiftDown";

    private static final String SUBMENU_CLASS_PARTIALITY = "sub.class.partiality";
    private static final String SUBMENU_CLASS_MORE = "sub.class.more";
    
    /**
     * A selector for the selection of "affected by" (AspectMessageViews) of a given message view.
     * Allows to select existing ones or the creation of a new one.
     *  
     * @author mschoettle
     */
    private class AspectMessageViewSelector extends SelectorView {
        
        private static final String ACTION_NEW = "advice.new";
        
        private RamButton addButton;
        
        /**
         * Creates a new selector for the affected by of the given message view.
         * 
         * @param messageView the {@link AbstractMessageView} to show its affected by for
         */
        AspectMessageViewSelector(AbstractMessageView messageView) {
            super(messageView, RamPackage.Literals.ABSTRACT_MESSAGE_VIEW__AFFECTED_BY);
            
            showTextField(false);
            
            RamImageComponent addImage =
                    new RamImageComponent(Icons.ICON_ADD, Colors.ICON_ADD_MESSAGEVIEW_COLOR, Icons.ICON_SIZE,
                            Icons.ICON_SIZE);
            addButton = new RamButton(addImage);
            addButton.setActionCommand(ACTION_NEW);
            inputContainer.addChild(addButton);
        }
        
        @Override
        protected void initialize() {
            @SuppressWarnings("unchecked")
            List<Object> affectedBys = (List<Object>) data.eGet(feature);
            setElements(affectedBys);
        }
        
        /**
         * Adds the given listener to all extra buttons that this selector contains.
         *
         * @param listener the listener to add
         */
        public void addActionListener(ActionListener listener) {
            addButton.addActionListener(listener);
        }
        
    }

    @Override
    public void createParameter(final OperationView operationView) {
        createParameterEndOperation(operationView);
    }

    @Override
    public void removeOperation(OperationView operationView) {
        ControllerFactory.INSTANCE.getClassController().removeOperation(operationView.getOperation());
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        String actionCommand = event.getActionCommand();
        RamRectangleComponent pressedButton = (RamRectangleComponent) event.getTarget();
        RamLinkedMenu linkedMenu = (RamLinkedMenu) pressedButton.getParentOfType(RamLinkedMenu.class);
        if (linkedMenu != null) {

            OperationView operationView = (OperationView) linkedMenu.getLinkedView();

            if (ACTION_OPERATION_REMOVE.equals(actionCommand)) {
                removeOperation(operationView);
            } else if (ACTION_PARAMETER_ADD.equals(actionCommand)) {
                createParameter(operationView);
            } else if (ACTION_OPERATION_STATIC.equals(actionCommand)) {
                setOperationStatic(operationView);
            } else if (ACTION_OPERATION_ABSTRACT.equals(actionCommand)) {
                switchToAbstract(operationView);
            } else if (ACTION_OPERATION_CONCERN_PARTIAL.equals(actionCommand)) {
                switchPartiality(operationView, RAMPartialityType.PROTECTED);
            } else if (ACTION_OPERATION_PUBLIC_PARTIAL.equals(actionCommand)) {
                switchPartiality(operationView, RAMPartialityType.PUBLIC);
            } else if (ACTION_OPERATION_NOT_PARTIAL.equals(actionCommand)) {
                switchPartiality(operationView, RAMPartialityType.NONE);
            } else if (ACTION_OPERATION_CARDINALITY.equals(actionCommand)) {
                switchCORECIElementExistence(operationView);
            } else if (ACTION_OPERATION_GOTO_MESSAGEVIEW.equals(actionCommand)) {
                goToMessageView(operationView);
            } else if (ACTION_OPERATION_ADVICE.equals(actionCommand)) {
                goToAdviceView(operationView);
            } else if (ACTION_OPERATION_SHIFT_UP.equals(actionCommand)) {
                shiftOperationUp(operationView);
            } else if (ACTION_OPERATION_SHIFT_DOWN.equals(actionCommand)) {
                shiftOperationDown(operationView);
            }
        }
    }
    
    public void shiftOperationUp(OperationView operationView) {        
        EList<Operation> operations = ((Classifier) operationView.getOperation().eContainer()).getOperations();
        int index = 0;
        
        for (int i = 0; i < operations.size(); i++) {
            if (operations.get(i).equals(operationView.getOperation())) {
                index = i;
            }
        }
        
        if (index > 0) {
            ControllerFactory.INSTANCE.getClassController()
                .setOperationPosition(operationView.getOperation(), index - 1);        
        }
    }
    
    public void shiftOperationDown(OperationView operationView) {
        EList<Operation> operations = ((Classifier) operationView.getOperation().eContainer()).getOperations();
        int index = 0;
        
        for (int i = 0; i < operations.size(); i++) {
            if (operations.get(i).equals(operationView.getOperation())) {
                index = i;
            }
        }
        
        if (index < operations.size() - 1) {
            ControllerFactory.INSTANCE.getClassController()
                .setOperationPosition(operationView.getOperation(), index + 1);        
        }
    }

    @Override
    public void setOperationStatic(OperationView operationView) {
        ClassController controller = ControllerFactory.INSTANCE.getClassController();
        controller.switchOperationStatic(operationView.getOperation());
    }

    @Override
    public void switchToAbstract(OperationView operationView) {
        ClassController controller = ControllerFactory.INSTANCE.getClassController();
        controller.switchOperationAbstract(operationView.getOperation());
    }

    @Override
    public void switchPartiality(OperationView operationView, RAMPartialityType type) {
        ClassController controller = ControllerFactory.INSTANCE.getClassController();
     
        controller.changeMappableElementPartiality(operationView.getOperation(), type);
    }
    
    @Override
    public void switchCORECIElementExistence(OperationView operationView) {
        ClassController controller = ControllerFactory.INSTANCE.getClassController();
        
        CORECIElement ciElement = COREArtefactUtil.getCIElementFor(operationView.getOperation());
        
        if (ciElement == null) {
            controller.createNewCORECIElement(operationView.getOperation());
        } else {
            controller.deleteCORECIElement(ciElement);
        }
    }

    @Override
    public void goToMessageView(OperationView operationView) {
        Operation operation = operationView.getOperation();
        final DisplayAspectScene displayAspectScene = RamApp.getActiveAspectScene();
        final Aspect aspect = EMFModelUtil.getRootContainerOfType(operation, RamPackage.Literals.ASPECT);

        boolean isPartial = operation.getPartiality() != RAMPartialityType.NONE;
        boolean isAbstract = operation.isAbstract();

        if (!isPartial && !isAbstract) {
            if (!MessageViewUtil.isMessageViewDefined(aspect, operation)) {
                // Register a listener so that the message view can be displayed
                // when it was successfully added.
                aspect.eAdapters().add(new EContentAdapter() {
                    @Override
                    public void notifyChanged(Notification notification) {
                        if (notification.getFeature() == RamPackage.Literals.ASPECT__MESSAGE_VIEWS
                                && notification.getEventType() == Notification.ADD) {
                            if (notification.getNewValue() instanceof MessageView) {
                                MessageView messageView = (MessageView) notification.getNewValue();
                                displayAspectScene.showMessageView(messageView);
                                aspect.eAdapters().remove(this);
                            }
                        }
                    }
                });

                ControllerFactory.INSTANCE.getAspectController().createMessageView(aspect, operation);
            } else {
                MessageView messageView = MessageViewUtil.getMessageViewFor(aspect, operation);
                
                /**
                 * If a message view is defined but no message view existent in the current aspect,
                 * it means that the message view is defined in another (either extended or reused) model.
                 */
                if (messageView != null) {
                    displayAspectScene.showMessageView(messageView);
                } else {
                    displayAspectScene.displayPopup(Strings.POPUP_MESSAGE_VIEW_EXISTS);
                }
            }
        } else {
            displayAspectScene.displayPopup(Strings.POPUP_PARTIAL_ABSTRACT_NO_BEHAVIOR);
        }
    }

    @Override
    public void goToAdviceView(OperationView operationView) {
        final Operation operation = operationView.getOperation();
        final DisplayAspectScene displayAspectScene = RamApp.getActiveAspectScene();
        final Aspect aspect = EMFModelUtil.getRootContainerOfType(operation, RamPackage.Literals.ASPECT);

        // TODO: Temporary: In case there is already an aspect message view that advices this operation, show that one.
        AbstractMessageView messageView = getMessageView(aspect, operation);
        
        if (messageView != null && !messageView.getAffectedBy().isEmpty()) {
            final AspectMessageViewSelector selector = new AspectMessageViewSelector(messageView);
            selector.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent event) {
                    if (AspectMessageViewSelector.ACTION_NEW.equals(event.getActionCommand())) {
                        createAspectMessageView(displayAspectScene, aspect, operation);
                        selector.destroy();
                    }
                }
                
            });
            
            selector.registerListener(new RamSelectorListener<Object>() {
                
                @Override
                public void elementSelected(RamSelectorComponent<Object> selector, Object element) {
                    AspectMessageView aspectMessageView = (AspectMessageView) element;
                    
                    displayAspectScene.showMessageView(aspectMessageView);
                }
                
                @Override
                public void elementHeld(RamSelectorComponent<Object> selector, Object element) {
                    // Do nothing.
                }
                
                @Override
                public void elementDoubleClicked(RamSelectorComponent<Object> selector, Object element) {
                    // Do nothing.
                }
                
                @Override
                public void closeSelector(RamSelectorComponent<Object> selector) {
                    // Do nothing.
                }
            });
            
            
            RamApp.getActiveScene().addComponent(selector, operationView.getCenterPointGlobal());
        } else {
            createAspectMessageView(displayAspectScene, aspect, operation);
        }

    }

    /**
     * Ensures that a new AspectMessageView is created and shown afterwards.
     * 
     * @param displayAspectScene the current {@link DisplayAspectScene}
     * @param aspect the {@link Aspect} the message view should be added to
     * @param operation the {@link Operation} the message view is advicing
     */
    private static void createAspectMessageView(final DisplayAspectScene displayAspectScene, Aspect aspect,
            Operation operation) {
        // Register a listener so that the message view can be displayed
        // when it was successfully added.
        EContentAdapter listener = new EContentAdapter() {
            @Override
            public void notifyChanged(Notification notification) {
                if (notification.getFeature() == RamPackage.Literals.ASPECT__MESSAGE_VIEWS
                        && notification.getEventType() == Notification.ADD) {
                    if (notification.getNewValue() instanceof AspectMessageView) {
                        AspectMessageView messageView = (AspectMessageView) notification.getNewValue();
                        displayAspectScene.showMessageView(messageView);
                    }
                }
            }
        };
        
        aspect.eAdapters().add(listener);
        
        try {
            ControllerFactory.INSTANCE.getAspectController().createAspectMessageView(aspect, operation);
        } catch (IllegalArgumentException e) {
            displayAspectScene.displayPopup(e.getLocalizedMessage(), PopupType.ERROR);
        }
        
        aspect.eAdapters().remove(listener);
    }

    /**
     * Returns the message view for the given operation.
     * If the operation is defined in the given aspect, the {@link MessageView} is returned (if it exists).
     * Otherwise, the {@link MessageViewReference} to the actual message view is returned, if it exists.
     * 
     * @param aspect the current {@link Aspect}
     * @param operation the {@link Operation} the {@link MessageView} to return for
     * @return the {@link MessageView} or {@link MessageViewReference}, null if none exists
     */
    private static AbstractMessageView getMessageView(Aspect aspect, Operation operation) {
        AbstractMessageView messageView = MessageViewUtil.getMessageViewFor(aspect, operation);
        
        // If there is no MessageView defined, try to see if there is a MessageViewReference.
        if (messageView == null) {
            Operation fromOperation = RAMModelUtil.getMappedFrom(operation);
            messageView = MessageViewUtil.getMessageViewReferenceFor(aspect, fromOperation);
        }
        
        return messageView;
    }

    @Override
    public EObject getEobject(RamRectangleComponent rectangle) {
        return ((OperationView) rectangle).getOperation();
    }

    @Override
    public void initMenu(RamLinkedMenu menu) {        
        menu.addAction(Strings.MENU_DELETE, Icons.ICON_MENU_TRASH, ACTION_OPERATION_REMOVE, this, true);

        OperationView operationView = (OperationView) menu.getLinkedView();
        if (operationView.isMutable()) {

            menu.addAction(Strings.MENU_PARAMETER_ADD, Icons.ICON_MENU_ADD_PARAMETER, ACTION_PARAMETER_ADD, this, true);

            // add the operation shift up and down buttons if there is more than 1 operation
            if (((Classifier) operationView.getOperation().eContainer()).getOperations().size() > 1) {
                menu.addAction(Strings.MENU_OPERATION_SHIFT_UP,
                        Icons.ICON_MENU_ELEMENT_SHIFT_UP, ACTION_OPERATION_SHIFT_UP, this, true);
                menu.addAction(Strings.MENU_OPERATION_SHIFT_DOWN,
                        Icons.ICON_MENU_ELEMENT_SHIFT_DOWN, ACTION_OPERATION_SHIFT_DOWN, this, true);
            }
            
            menu.addSubMenu(1, SUBMENU_CLASS_PARTIALITY);
            menu.addAction(Strings.MENU_PUBLIC_PARTIAL, Icons.ICON_MENU_PUBLIC_PARTIAL, ACTION_OPERATION_PUBLIC_PARTIAL,
                    this, SUBMENU_CLASS_PARTIALITY, true);
            menu.addAction(Strings.MENU_CONCERN_PARTIAL, Icons.ICON_MENU_CONCERN_PARTIAL,
                    ACTION_OPERATION_CONCERN_PARTIAL, this, SUBMENU_CLASS_PARTIALITY, true);
            menu.addAction(Strings.MENU_NO_PARTIAL, Icons.ICON_MENU_NOT_PARTIAL, ACTION_OPERATION_NOT_PARTIAL,
                    this, SUBMENU_CLASS_PARTIALITY, true);
            menu.addAction(Strings.MENU_CARDINALITY, Strings.MENU_NOT_CARDINALITY, Icons.ICON_MENU_CARDINALITY,
                    Icons.ICON_MENU_NOT_CARDINALITY, ACTION_OPERATION_CARDINALITY, this,
                    SUBMENU_CLASS_PARTIALITY, true, false);

            menu.addSubMenu(1, SUBMENU_CLASS_MORE);

            Operation operation = operationView.getOperation();
            if (!RAMModelUtil.isConstructorOrDestructor(operation)) {
                menu.addAction(Strings.MENU_STATIC, Strings.MENU_NO_STATIC, Icons.ICON_MENU_STATIC,
                        Icons.ICON_MENU_NOT_STATIC,
                        ACTION_OPERATION_STATIC, this, SUBMENU_CLASS_MORE, true, false);
            }

            menu.addAction(Strings.MENU_ABSTRACT, Strings.MENU_NOT_ABSTRACT, Icons.ICON_MENU_ABSTRACT,
                    Icons.ICON_MENU_NOT_ABSTRACT, ACTION_OPERATION_ABSTRACT, this, SUBMENU_CLASS_MORE, true,
                    false);

            menu.addAction(Strings.MENU_GOTO_MESSAGEVIEW, Icons.ICON_MENU_MESSAGE_VIEW,
                    ACTION_OPERATION_GOTO_MESSAGEVIEW,
                    this, true);
            menu.addAction(Strings.MENU_ADVICE, Icons.ICON_MENU_ADVICE, ACTION_OPERATION_ADVICE, this, true);
            menu.setLinkInCorners(false);
            updateButtons(menu);
        }

    }

    @Override
    public List<EObject> getEObjectToListenForUpdateMenu(RamRectangleComponent rectangle) {
        OperationView operationView = (OperationView) rectangle;
        List<EObject> ret = new ArrayList<EObject>();
        ret.add(operationView.getOperation());
        ret.add(COREArtefactUtil.getReferencingExternalArtefact(operationView.getOperation()));
        return ret;
    }

    @Override
    public void updateMenu(RamLinkedMenu menu, Notification notification) {
        if (notification.getEventType() == Notification.SET
                || notification.getEventType() == Notification.UNSET
                || notification.getEventType() == Notification.ADD
                || notification.getEventType() == Notification.REMOVE) {
            updateButtons(menu);
        }
    }

    /**
     * Updates buttons inside the menu.
     * 
     * @param menu - the menu which contains buttons.
     */
    private static void updateButtons(RamLinkedMenu menu) {
        Operation operation = (Operation) menu.geteObject();

        Object obj = operation.eGet(RamPackage.Literals.MAPPABLE_ELEMENT__PARTIALITY);
        menu.enableAction(!obj.equals(RAMPartialityType.NONE), ACTION_OPERATION_NOT_PARTIAL);
        menu.enableAction(!obj.equals(RAMPartialityType.PROTECTED), ACTION_OPERATION_CONCERN_PARTIAL);
        menu.enableAction(!obj.equals(RAMPartialityType.PUBLIC), ACTION_OPERATION_PUBLIC_PARTIAL);

        menu.toggleAction(operation.isStatic(), ACTION_OPERATION_STATIC);
        menu.toggleAction(operation.isAbstract(), ACTION_OPERATION_ABSTRACT);
        
        CORECIElement ciElement = COREArtefactUtil.getCIElementFor(operation);
        
        if (ciElement != null) {
            menu.enableAction(ciElement.getPartiality() == COREPartialityType.NONE, ACTION_OPERATION_CARDINALITY);
            menu.toggleAction(true, ACTION_OPERATION_CARDINALITY);
        } else {
            menu.enableAction(true, ACTION_OPERATION_CARDINALITY);
            menu.toggleAction(false, ACTION_OPERATION_CARDINALITY);
        }
    }

    @Override
    public RamRectangleComponent getVisualLinkedComponent(RamRectangleComponent link) {
        OperationView view = (OperationView) link;
        return view.getNameField();
    }
    
    /**
     * Add a parameter at the end of an operation.
     * It add the placeholder in the view and in the model
     *
     * @param operationView - the related {@link OperationView}
     */
    private static void createParameterEndOperation(final OperationView operationView) {
        final RamTextComponent textRow = new RamTextComponent();
        textRow.setBufferSize(Cardinal.SOUTH, OperationView.BUFFER_BOTTOM);
        textRow.setPlaceholderText(Strings.PH_PARAM);

        int visualIndex = operationView.getChildCount() - 1;
        operationView.addChild(visualIndex, textRow);

        final int nbParameters = operationView.getOperation().getParameters().size();

        // visual index for delimiter; after if it is the first, before otherwise
        final int delimiterIndex = visualIndex;

        if (nbParameters > 0) {
            operationView.addDelimiter(delimiterIndex);
        }
        final MTComponent delimiter = operationView.getChildByIndex(delimiterIndex);

        RamKeyboard keyboard = new RamKeyboard();

        keyboard.registerListener(new DefaultRamKeyboardListener() {
            @Override
            public void keyboardCancelled() {
                operationView.removeChild(textRow);
                if (delimiter != null) {
                    operationView.removeChild(delimiter);
                }
            }

            @Override
            public boolean verifyKeyboardDismissed() {
                try {
                    int nbParameters = operationView.getOperation().getParameters().size();
                    ControllerFactory.INSTANCE.getClassController()
                        .createParameter(operationView.getOperation(), nbParameters, textRow.getText());
                } catch (final IllegalArgumentException e) {
                    return false;
                }
                if (delimiter != null) {
                    operationView.removeChild(delimiter);
                }
                operationView.removeChild(textRow);

                return true;
            }
        });

        textRow.showKeyboard(keyboard);
    }
    
}
