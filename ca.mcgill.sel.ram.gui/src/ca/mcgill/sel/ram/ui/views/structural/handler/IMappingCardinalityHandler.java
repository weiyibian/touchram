package ca.mcgill.sel.ram.ui.views.structural.handler;

import ca.mcgill.sel.ram.ui.events.listeners.ITapAndHoldListener;
import ca.mcgill.sel.ram.ui.views.handler.ITextViewHandler;

/**
 * This interface is implemented by something that can handle events for a {@link ca.mcgill.sel.ram.ui.views.TextView}
 * representing the name of a cardinality.
 * 
 * @author jmilleret
 */
public interface IMappingCardinalityHandler extends ITextViewHandler, ITapAndHoldListener {

}
