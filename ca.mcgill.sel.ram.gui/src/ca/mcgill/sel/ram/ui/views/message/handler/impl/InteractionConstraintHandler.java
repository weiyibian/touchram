package ca.mcgill.sel.ram.ui.views.message.handler.impl;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.List;

import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.ram.CombinedFragment;
import ca.mcgill.sel.ram.InteractionFragment;
import ca.mcgill.sel.ram.InteractionOperand;
import ca.mcgill.sel.ram.Message;
import ca.mcgill.sel.ram.ObjectType;
import ca.mcgill.sel.ram.OpaqueExpression;
import ca.mcgill.sel.ram.controller.ControllerFactory;
import ca.mcgill.sel.ram.controller.FragmentsController;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamImageComponent;
import ca.mcgill.sel.ram.ui.components.RamSelectorComponent;
import ca.mcgill.sel.ram.ui.components.listeners.AbstractDefaultRamSelectorListener;
import ca.mcgill.sel.ram.ui.components.listeners.RamSelectorListener;
import ca.mcgill.sel.ram.ui.events.listeners.ITapAndHoldListener;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.views.OptionSelectorView;
import ca.mcgill.sel.ram.ui.views.OptionSelectorView.Iconified;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.util.MessageViewUtil;
import ca.mcgill.sel.ram.util.MetamodelRegex;
import ca.mcgill.sel.ram.util.RAMInterfaceUtil;

/**
 * The handler for text views responsible for visualizing interaction constraints.
 * Allows the text view to be oblivious of the specific value specification used for the constraint.
 * This means that the first time an event is processed, it will figure out the actual object and
 * feature, which will be set for the view.
 * 
 * @author mschoettle
 */
public class InteractionConstraintHandler extends ValueSpecificationHandler implements ITapAndHoldListener {
    
    /**
     * The options to display for an interaction operand.
     */
    private enum OperandOptions implements Iconified {
        DEFINE_TEMPORARY_VARIABLE(new RamImageComponent(Icons.ICON_ADD, Colors.ICON_ADD_MESSAGEVIEW_COLOR)),
        ADD_OPERAND(new RamImageComponent(Icons.ICON_ADD, Colors.ICON_ADD_MESSAGEVIEW_COLOR)),
        REMOVE_OPERAND(new RamImageComponent(Icons.ICON_DELETE, Colors.ICON_DELETE_COLOR));
        
        private RamImageComponent icon;
        
        /**
         * Creates a new option literal with the given icon.
         * 
         * @param icon the icon to use for this option
         */
        OperandOptions(RamImageComponent icon) {
            this.icon = icon;
        }
        
        @Override
        public RamImageComponent getIcon() {
            return icon;
        }
    }
    
    @Override
    public boolean processTapAndHoldEvent(TapAndHoldEvent tapAndHoldEvent) {
        if (tapAndHoldEvent.isHoldComplete() && !isEditEnabled()) {
            TextView target = (TextView) tapAndHoldEvent.getTarget();
            final CombinedFragment combinedFragment = (CombinedFragment) target.getData().eContainer();
            final InteractionOperand operand = (InteractionOperand) target.getData();
            final int index = combinedFragment.getOperands().indexOf(operand);
            
            EnumSet<OperandOptions> availableOptions = EnumSet.allOf(OperandOptions.class);
            
            if (index == 0) {
                availableOptions.remove(OperandOptions.REMOVE_OPERAND);
            }
            
            OptionSelectorView<OperandOptions> selector =
                    new OptionSelectorView<OperandOptions>(availableOptions.toArray(new OperandOptions[] {}));
            
            final Vector3D tapLocation = tapAndHoldEvent.getLocationOnScreen();
            RamApp.getActiveScene().addComponent(selector, tapLocation);
            
            selector.registerListener(new AbstractDefaultRamSelectorListener<OperandOptions>() {
                @Override
                public void elementSelected(RamSelectorComponent<OperandOptions> selector, OperandOptions element) {
                    FragmentsController controller = ControllerFactory.INSTANCE.getFragmentsController();
                    
                    switch (element) {
                        case DEFINE_TEMPORARY_VARIABLE:
                            handleCreateTemporaryVariable(operand, tapLocation);
                            break;
                        case ADD_OPERAND:
                            controller.createInteractionOperand(combinedFragment, index + 1);
                            break;
                        case REMOVE_OPERAND:
                            controller.removeInteractionOperand(operand);
                            break;
                    }
                }
            });
        }
        
        return true;
    }
    
    /**
     * Handles the user request to create a temporary variable for the operand.
     * Uses the constraint text to propose variable names.
     * Upon selection by the user, the available types are presented.
     * 
     * @param operand the operand in the model the gesture is performed on/for
     * @param tapLocation the location where the gesture occurred
     */
    private static void handleCreateTemporaryVariable(final InteractionOperand operand, final Vector3D tapLocation) {
        String constraint = ((OpaqueExpression) operand.getInteractionConstraint()).getBody();
        
        if (constraint != null && constraint.length() > 0) {
            String[] identifiers = constraint.split(" ");
            List<String> validChoices = new ArrayList<>();
            
            for (String identifier : identifiers) {
                if (identifier.matches(MetamodelRegex.REGEX_TYPE_NAME)) {
                    validChoices.add(identifier);
                }
            }
            
            RamSelectorComponent<String> selector = new RamSelectorComponent<>(validChoices);
            selector.registerListener(new RamSelectorListener<String>() {
                
                @Override
                public void elementSelected(RamSelectorComponent<String> selector, String element) {
                    handleVariableNameSelection(operand, tapLocation, element);
                    selector.destroy();
                }
                
                @Override
                public void elementHeld(RamSelectorComponent<String> selector, String element) {
                    
                }
                
                @Override
                public void elementDoubleClicked(RamSelectorComponent<String> selector, String element) {
                    
                }
                
                @Override
                public void closeSelector(RamSelectorComponent<String> selector) {
                    selector.destroy();
                }
            });
            
            RamApp.getActiveScene().addComponent(selector, tapLocation);
        }
    }
    
    /**
     * Handles the user request upon selection of the name for the variable.
     * Shows all available types the variable should have to the user.
     * Upon selection, the temporary property is created with the chosen properties.
     * 
     * @param operand the operand in the model the gesture is performed on/for
     * @param tapLocation the location where the gesture occurred
     * @param name the chosen name for the variable
     */
    private static void handleVariableNameSelection(final InteractionOperand operand, Vector3D tapLocation,
            final String name) {
        // Build choice of values.
        final List<ObjectType> choiceOfValues = new ArrayList<>();
        choiceOfValues.addAll(RAMInterfaceUtil.getAvailableClasses(operand));
        choiceOfValues.addAll(RAMInterfaceUtil.getAvailablePrimitiveTypes(operand));
        
        RamSelectorComponent<ObjectType> typeSelector = new RamSelectorComponent<>(choiceOfValues);
        // Use order of list.
        typeSelector.setElementsOrder(new Comparator<ObjectType>() {
            
            @Override
            public int compare(ObjectType o1, ObjectType o2) {
                return Integer.compare(choiceOfValues.indexOf(o1), choiceOfValues.indexOf(o2));
            }
        });
        typeSelector.registerListener(new RamSelectorListener<ObjectType>() {

            @Override
            public void elementSelected(RamSelectorComponent<ObjectType> selector, ObjectType element) {
                Message message = MessageViewUtil.findInitialMessage((InteractionFragment) operand.eContainer());
                ControllerFactory.INSTANCE.getFragmentsController()
                    .createAndAddTemporaryProperty(message, name, element);
                
                closeSelector(selector);
            }

            @Override
            public void elementDoubleClicked(RamSelectorComponent<ObjectType> selector, ObjectType element) {
                
            }

            @Override
            public void elementHeld(RamSelectorComponent<ObjectType> selector, ObjectType element) {
                
            }

            @Override
            public void closeSelector(RamSelectorComponent<ObjectType> selector) {
                selector.destroy();
            }
            
        });
        
        RamApp.getActiveScene().addComponent(typeSelector, tapLocation);
    }
    
}
