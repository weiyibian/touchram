package ca.mcgill.sel.ram.ui.views.impact;

import java.math.RoundingMode;
import java.util.Map;

import org.mt4j.util.MTColor;

import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.COREContribution;
import ca.mcgill.sel.core.COREImpactModel;
import ca.mcgill.sel.core.COREImpactNode;
import ca.mcgill.sel.core.COREModelReuse;
import ca.mcgill.sel.core.COREWeightedLink;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.evaluator.im.PropagationResult;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.MathUtils;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.impact.handler.HandlerFactoryImpactModel;
import ca.mcgill.sel.ram.ui.views.impact.handler.impl.FeatureImpactViewHandler;
import ca.mcgill.sel.ram.ui.views.impact.handler.impl.GoalImpactViewHandler;
import ca.mcgill.sel.ram.ui.views.impact.handler.impl.ImpactModelElementViewHandler;
import ca.mcgill.sel.ram.ui.views.impact.handler.impl.ImpactSelectDiagramHandler;

/**
 * Class used to represent an impact model. It is used to create the impact model. This class only describe which
 * Handler we should use for the edit mode of the Impact model.
 *
 * @author Romain
 *
 */
public class ImpactSelectDiagramView extends ImpactDiagramView<ImpactSelectDiagramHandler> {

    private PropagationResult propagationResult;

    /**
     * Create a {@link ImpactSelectDiagramView}. It has a root goal and builds the model from it.
     *
     * @param im the {@link COREImpactModel} of this view
     * @param rootNode the root {@link COREImpactNode}
     * @param propagationResult The result of the evaluation. It associates impact model elements with their weight
     * @param width the width of this view
     * @param height the height of this view
     */
    public ImpactSelectDiagramView(COREImpactModel im, COREImpactNode rootNode,
            PropagationResult propagationResult, float width,
            float height) {
        super(im, rootNode, width, height);

        this.propagationResult = propagationResult;

        this.changeElementColors();
    }

    /**
     * Change the color of the feature selected and the goals used.
     */
    private void changeElementColors() {

        for (ImpactModelElementView<? extends ImpactModelElementViewHandler> view : impactMap.values()) {
            COREImpactNode impactModelElement = view.getImpactModelElement();

            Float impactValue;

            if (propagationResult == null) {
                impactValue = 0f;
            } else {
                impactValue = propagationResult.getEvaluationForImpactNode(impactModelElement);

            }

            impactValue =
                    impactValue == null ? 0.00f : MathUtils.round(impactValue, 2, RoundingMode.HALF_UP);

            TextView name = view.getTextView();
            name.updateText();

            name.setText(
                    name.getText() + " : " + impactValue);

            MTColor color;

            if (impactValue != 0.0f) {
                color = new MTColor(2 * (255 - (impactValue * 2.55f)), 2 * impactValue
                        * 2.55f, 0f);
            } else {
                color = Colors.FEATURE_NOT_SELECTED_FILL_COLOR;
            }

            if (view instanceof GoalImpactView) {
                if (propagationResult != null) {
                    for (COREContribution contribution : impactModelElement.getIncoming()) {
                        Float sourceValue = propagationResult.getEvaluationForImpactNode(contribution.getSource());
                        if (sourceValue != null && sourceValue != 0) {
                            color = new MTColor(2 * (255 - (impactValue * 2.55f)), 2 * impactValue
                                    * 2.55f, 0f);
                            break;
                        }
                    }
                }
            } else if (view instanceof FeatureImpactView) {
                FeatureImpactView featureImpactView = (FeatureImpactView) view;

                Map<COREWeightedLink, RamRectangleComponent> weightedLinks =
                        featureImpactView.getWeightedLinkMap();

                for (COREWeightedLink weightedLink : weightedLinks.keySet()) {
                    COREModelReuse modelReuse =
                            EMFModelUtil.getRootContainerOfType(weightedLink, CorePackage.Literals.CORE_MODEL_REUSE);
                    PropagationResult reusePropagationResult;

                    if (propagationResult == null) {
                        reusePropagationResult = null;
                    } else {
                        reusePropagationResult =
                                propagationResult.getEvaluationsForReuse(modelReuse.getReuse());
                    }

                    Float mappingImpact;
                    if (reusePropagationResult != null) {
                        mappingImpact = reusePropagationResult
                                .getEvaluationForImpactNode(weightedLink.getFrom());
                    } else {
                        mappingImpact = 0f;
                    }

                    mappingImpact =
                            mappingImpact == null ? 0.00f : MathUtils.round(mappingImpact, 2, RoundingMode.HALF_UP);

                    WeightedLinkLineView textViewContainerComponent =
                            (WeightedLinkLineView) weightedLinks.get(weightedLink);

                    textViewContainerComponent.getNameTextView().setText(
                            textViewContainerComponent.getNameTextView().getText() + " : " + mappingImpact);

                    textViewContainerComponent.setNoFill(false);

                    if (impactValue != 0f) {
                        textViewContainerComponent
                                .setFillColor(new MTColor(2 * (255 - (mappingImpact * 2.55f)), 2 * mappingImpact
                                        * 2.55f, 0f));
                    } else {
                        textViewContainerComponent
                                .setFillColor(Colors.FEATURE_NOT_SELECTED_FILL_COLOR);
                    }
                }
            }

            view.changeColor(color);

        }
    }

    @Override
    protected void setContributionViewHandler(ContributionView contributionView) {
    }

    @Override
    protected void setGoalImpactViewHandler(ImpactModelElementView<GoalImpactViewHandler> view) {
        view.setHandler(HandlerFactoryImpactModel.INSTANCE.getGoalImpactViewHandler());
    }

    @Override
    protected void setFeatureImpactViewHandler(ImpactModelElementView<FeatureImpactViewHandler> view) {
        view.setHandler(HandlerFactoryImpactModel.INSTANCE.getFeatureImpactViewHandler());
    }

    /**
     * Return the map that contains the impact for each goal.
     *
     * @return the map that contains the impact for each goal
     */
    public PropagationResult getGoalImpactMap() {
        return propagationResult;
    }
}
