package ca.mcgill.sel.ram.ui.views.feature;

import java.awt.geom.Rectangle2D;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.abego.treelayout.NodeExtentProvider;
import org.abego.treelayout.TreeLayout;
import org.abego.treelayout.util.DefaultConfiguration;
import org.abego.treelayout.util.DefaultTreeForTreeLayout;
import org.mt4j.components.visibleComponents.shapes.MTEllipse;
import org.mt4j.components.visibleComponents.shapes.MTPolygon;
import org.mt4j.util.math.Vector3D;
import org.mt4j.util.math.Vertex;

import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.COREFeatureModel;
import ca.mcgill.sel.core.COREFeatureRelationshipType;
import ca.mcgill.sel.core.COREModelReuse;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamImageComponent;
import ca.mcgill.sel.ram.ui.components.RamLineComponent;
import ca.mcgill.sel.ram.ui.scenes.AbstractConcernScene;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.GUIConstants;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.MathUtils;
import ca.mcgill.sel.ram.ui.utils.autolayout.DefaultNodeExtentProvider;
import ca.mcgill.sel.ram.ui.views.AbstractView;
import ca.mcgill.sel.ram.ui.views.RelationshipView.LineStyle;
import ca.mcgill.sel.ram.ui.views.feature.helpers.SelectionFeature;
import ca.mcgill.sel.ram.ui.views.feature.helpers.SelectionFeature.FeatureSelectionStatus;
import ca.mcgill.sel.ram.ui.views.handler.IAbstractViewHandler;
import processing.core.PImage;

/**
 * Class used to represent a feature model.
 * Has a root feature and builds the model from it
 * 
 * @author CCamillieri
 * @param <T> The type of handler for this view
 */
public abstract class FeatureDiagramView<T extends IAbstractViewHandler> extends AbstractView<T> {

    /**
     * Display options for a given {@link FeatureView}.
     */
    protected enum DisplayOptions {
        /** The view will be displayed. */
        SHOW,
        /** The view is to be collapsed : it will be displayed but its children hidden. */
        COLLAPSE,
        /** The view will not be displayed. */
        HIDE
    }

    // The initial level / depth to be shown based on screen resolution
    private static final float INITIAL_NUMBER_OF_LEVELS = 4;

    // The maximum distance between two features while being displayed.
    private static final float MAXIMUM_VERTICAL_DISTANCE = 100;

    // The minimum distance between two features while being displayed.
    private static final float INITIAL_HORIZONTAL_DISTANCE = 50;

    private static final float SCALING_FEATURE_ICON = 5f / 6f;

    /** Tree layout representing the diagram. */
    private static TreeLayout<FeatureView> treeLayout;

    /** Tree containing the features hierarchy. */
    protected DefaultTreeForTreeLayout<FeatureView> tree;

    /** Map containing the {@link FeatureView}s of the feature model. */
    protected Map<COREFeature, FeatureView> features;

    /** Feature model displayed by this diagram. */
    protected COREFeatureModel featureModel;
    
    /** Current selected reuse. */
    protected COREReuse reuse;

    /** {@link FeatureView} that have been collapsed by the user. */
    protected Set<FeatureView> collapsedFeatures;
    
    /** The scene we are currently in. */
    protected AbstractConcernScene<?, ?> scene;

    private float gapBetweenLevels;

    /** The model reuse if there is one. */
    private COREModelReuse modelReuse;
    

    /**
     * Create a feature diagram representation view.
     * 
     * @param width - The width of the view
     * @param height - The height of the view
     * @param scene - The current parent scene
     * @param fm - The {@link COREFeatureModel} the diagram represents
     * @param reuse - The Reuse for the Reuse Diagram View
     * @param modelReuse the modelReuse selected to display. null if default.
     */
    public FeatureDiagramView(float width, float height, AbstractConcernScene<?, ?> scene, 
            COREFeatureModel fm, COREReuse reuse, COREModelReuse modelReuse) {
        super(width, height);
        this.scene = scene;
        this.featureModel = fm;
        this.features = new HashMap<COREFeature, FeatureView>();
        this.collapsedFeatures = new HashSet<FeatureView>();
        this.reuse = reuse;
        this.modelReuse = modelReuse;
        // Compute the gap between the levels based on the height of the screen, divided by the desired value of user.
        this.gapBetweenLevels = RamApp.getActiveScene().getHeight() / INITIAL_NUMBER_OF_LEVELS;
        if (gapBetweenLevels >= MAXIMUM_VERTICAL_DISTANCE) {
            gapBetweenLevels = MAXIMUM_VERTICAL_DISTANCE;
        }
        
        // Create the views for all the features
        COREFeature root = featureModel.getRoot();
        initFeatureViews(root, null);
        
        // Build the tree
        populateTree();

        // Compute positions for the features
        computeFeaturesPosition();

        // Add the views to the container. Also creates lines and circles/triangles for relationships and initializes
        // listeners
        displayDiagram();
    }
    
    /**
     * Create a feature diagram representation view.
     * 
     * @param width - The width of the view
     * @param height - The height of the view
     * @param fm - The {@link COREFeatureModel} the diagram represents
     */
    public FeatureDiagramView(float width, float height, COREFeatureModel fm) {
        super(width, height);
        this.featureModel = fm;
        this.features = new HashMap<COREFeature, FeatureView>();
        this.collapsedFeatures = new HashSet<FeatureView>();
        // Compute the gap between the levels based on the height of the screen, divided by the desired value of user.
        this.gapBetweenLevels = RamApp.getActiveScene().getHeight() / INITIAL_NUMBER_OF_LEVELS;
        if (gapBetweenLevels >= MAXIMUM_VERTICAL_DISTANCE) {
            gapBetweenLevels = MAXIMUM_VERTICAL_DISTANCE;
        }
        
        // Create the views for all the features
        COREFeature root = featureModel.getRoot();
        initFeatureViews(root, null);
        
        // Build the tree
        populateTree();

        // Compute positions for the features
        computeFeaturesPosition();

        // Add the views to the container. Also creates lines and circles/triangles for relationships and initializes
        // listeners
        displayDiagram();
    }

    /**
     * Get the list of collapsed {@link FeatureView}s.
     * 
     * @return the list of collapsed {@link FeatureView}s.
     */
    public Set<FeatureView> getCollapsedFeatures() {
        return collapsedFeatures;
    }

    @Override
    public void destroy() {
        // Destroy all FeatureViews
        for (FeatureView feature : getFeatureViews()) {
            destroyFeatureView(feature);
        }
        // Clear lists
        features.clear();
        super.destroy();
    }

    /**
     * Destroy a featureView.
     * 
     * @param view - The {@link FeatureView} to destroy
     */
    protected void destroyFeatureView(FeatureView view) {
        if (containerLayer.containsChild(view)) {
            if (view.getLineToParent() != null) {
                containerLayer.removeChild(view.getLineToParent());
            }
            if (view.getRelationshipCircle() != null) {
                containerLayer.removeChild(view.getRelationshipCircle());
            }
            if (view.getRelationshipTriangle() != null) {
                containerLayer.removeChild(view.getRelationshipTriangle());
            }
            removeFeatureIcon(view);
            containerLayer.removeChild(view);
        }
        collapsedFeatures.remove(view);
        view.destroy();
    }

    /**
     * Removes the image associated with a {@link FeatureView}, if there is one.
     * 
     * @param feature - The {@link FeatureView} of which we want to remove the icon
     */
    public void removeFeatureIcon(FeatureView feature) {
        RamImageComponent image = feature.getImage();
        if (image != null) {
            if (containerLayer.containsChild(image)) {
                containerLayer.removeChild(image);
            }
            feature.setImage(null);
            image.destroy();
        }
    }
    
    /**
     * Method is used to get the corresponding Feature View of the CORE Feature.
     * 
     * @param coreFeature - The CORE Feature for which the FeatureView is to be fetched.
     * @return FeatureView
     */
    public FeatureView getFeatureView(COREFeature coreFeature) {
        return features.get(coreFeature);
    }

    /*
     * ----------------- INITIALIZATION ---------------
     */
    /**
     * Create the {@link FeatureView} for a given {@link COREFeature}.
     * 
     * @param feature - The {@link COREFeature}
     * @param parent - The parent {@link FeatureView}, if any
     */
    private void initFeatureViews(COREFeature feature, FeatureView parent) {
        FeatureView featureView = features.get(feature);
        // Create the element only if the view doesn't already exist
        if (featureView == null) {
            featureView = addNewFeature(feature);
        }
        // Create the children elements
        for (COREFeature child : feature.getChildren()) {
            initFeatureViews(child, featureView);
        }
    }

    /**
     * Create a new {@link FeatureView} and add link it to the others.
     * 
     * @param feature - The {@link COREFeature} represented by the view
     * @return The new {@link FeatureView}
     */
    protected FeatureView addNewFeature(COREFeature feature) {
        return addNewFeature(feature, features.get(feature.getParent()));
    }

    /**
     * Create a new {@link FeatureView} and add link it to the others.
     * 
     * @param feature - The {@link COREFeature} represented by the view
     * @param parent - The parent {@link FeatureView} we want for the feature.
     * @return The new {@link FeatureView}
     */
    protected final FeatureView addNewFeature(COREFeature feature, FeatureView parent) {
        COREFeature parentFeature = parent != null ? parent.getFeature() : feature.getParent();
        FeatureView child = null;
        
        child = features.get(feature);
        
        COREFeatureRelationshipType rel = feature.getParentRelationship();

        // If the view already exist, move it to its new parent
        if (child != null) {
            // Move to new parent
            FeatureView oldParent = child.getParentFeatureView();
            if (oldParent != null && !oldParent.equals(parent)) {
                oldParent.getChildrenFeatureViews().remove(child);
            }
            child.setParentFeatureView(parent);
        } else {
            boolean editName = parentFeature != null;
            child = createFeature(feature, parent, rel, editName);
        }
        // Set the correct relationship for the parent FeatureView if needed
        if (parent != null && !parent.getChildrenFeatureViews().contains(child)) {
            int parentIndex = parentFeature.getChildren().indexOf(feature);

            parent.getChildrenFeatureViews().add(parentIndex, child);
            if (child.getParentRelationship() == COREFeatureRelationshipType.OR
                    || child.getParentRelationship() == COREFeatureRelationshipType.XOR) {
                parent.setChildrenRelationship(rel);
            }
        }
        
        // Add to the list of features
        features.put(feature, child);
        
        return child;
    }

    /**
     * Create a new {@link FeatureView} with given parameters.
     * 
     * @param feature - The {@link COREFeature} represented by the view
     * @param parent - The parent {@link FeatureView} if any
     * @param type - The {@link COREFeatureRelationshipType} for the feature
     * @param editName - Whether we want to be able to edit the name of the feature
     * @return The new {@link FeatureView}
     */
    protected FeatureView createFeature(COREFeature feature, FeatureView parent, COREFeatureRelationshipType type,
            boolean editName) {
        SelectionFeature parentFeature = parent != null ? parent.getSelectionFeature() : null;
        SelectionFeature selectionFeature;
        
        if (this instanceof ReuseDiagramView && reuse != null) {
            if (modelReuse == null) {
                modelReuse = reuse.getModelReuses().get(0);
            }
            
            selectionFeature = new SelectionFeature(feature, modelReuse, parentFeature);
            FeatureView ft = new FeatureView(selectionFeature, parent, type, false);
            return ft;
        }

        selectionFeature = new SelectionFeature(feature, null, parentFeature);
        return new FeatureView(selectionFeature, parent, type, editName);
    }

    /**
     * Register the listeners for all {@link FeatureView}s.
     */
    private void registerListeners() {
        for (FeatureView v : features.values()) {
            registerListeners(v);
        }
    }

    /**
     * Register the listeners for a given {@link FeatureView}.
     * 
     * @param feature - The {@link FeatureView} to register listeners for.
     */
    protected abstract void registerListeners(FeatureView feature);

    /*
     * ----------------------- DISPLAY -------------------
     */
    /**
     * Display the whole {@link FeatureDiagramView} according to user defined preferences.
     * Only chosen features will be shown (see shouldDisplay())
     */
    private void displayDiagram() {
        displayFeature(tree.getRoot());
        registerListeners();
    }

    /**
     * Redraw the diagram of features.
     * 
     * @param repopulate - Whether the tree has to be rebuilt or not. For example is a {@link FeatureView} has been
     *            added or removed
     */
    public void updateFeaturesDisplay(boolean repopulate) {
        containerLayer.removeAllChildren();
        if (repopulate) {
            populateTree();
        }
        computeFeaturesPosition();
        displayDiagram();
        placeIcons();
    }

    /**
     * Display the given {@link FeatureView} and its children.
     * Draws line to parent and if necessary symbols for the relationships.
     * 
     * @param feature - The {@link FeatureView} to display.
     */
    protected void displayFeature(FeatureView feature) {
        // Display feature
        feature.setPositionRelativeToParent(new Vector3D(feature.getXposition(), feature.getYposition(), 0));
        containerLayer.addChild(feature);

        for (FeatureView child : tree.getChildren(feature)) {
            displayFeature(child);
        }

        // Draw line to parent
        drawLine(feature, feature.getParentFeatureView());
    }

    /**
     * Function called to draw a line between parent and the child.
     * 
     * @param child - child feature
     * @param parent - parent feature
     */
    protected void drawLine(FeatureView child, FeatureView parent) {

        MTPolygon triangle = null;
        // Draw XOR/OR relationShip only if not collapsed
        if (tree.getChildrenList(child).size() > 0) {
            triangle = drawRelationshipTriangle(child);
        }
        // Draw the circle for MANDATORY/OPTIONALlationship
        MTEllipse circle = drawRelationshipCircle(child);

        child.updateRelationshipColor();

        int radius = (circle != null) ? (int) child.getRelationshipCircle().getRadiusX() : 0;

        float xposition = child.getXposition();
        float yposition = child.getYposition() - child.getHeight() / 2;

        if (parent != null) {

            float xposition2 = parent.getXposition();
            float yposition2 = parent.getYposition() + parent.getHeight() / 2;
            RamLineComponent line = child.getLineToParent();

            if (line != null) {
                // Destroy previous line
                containerLayer.removeChild(line);
                line.destroy();
            }

            line = new RamLineComponent(xposition, yposition - radius, xposition2, yposition2);

            float deltaY = yposition - yposition2;
            float deltaX = xposition - xposition2;
            float degrees = 0;

            degrees = (float) Math.toDegrees(Math.atan2(deltaY, deltaX));

            child.setAngleWithRespectToParent(360 - degrees);
            child.setLineToParent(line);

            containerLayer.addChild(line);
        }

        if (circle != null) {
            circle.setPositionGlobal(new Vector3D(xposition, yposition - radius, 0));
            circle.setPickable(true);
            containerLayer.addChild(circle);
        }
        if (triangle != null) {
            containerLayer.addChild(triangle);
        }

    }

    /**
     * Place the icons corresponding to selections on all features.
     */
    public void placeIcons() {
        placeIcons(getRootFeature());
    }

    /**
     * Place the icons corresponding to selections on a feature and its children.
     * (only if they are currently being displayed)
     * 
     * @param feature - The {@link FeatureView} to consider
     */
    private void placeIcons(FeatureView feature) {

        for (FeatureView child : feature.getChildrenFeatureViews()) {
            // Recursively add icons to the children
            placeIcons(child);
        }

        // Remove the previous icon for the feature.
        removeFeatureIcon(feature);

        if (!containerLayer.containsChild(feature)) {
            return;
        }

        FeatureSelectionStatus selectionStatus = feature.getFeatureSelectionStatus();
        RamImageComponent image = feature.getImage();

        // Add icon corresponding to state
        PImage iconImage = null;
        switch (selectionStatus) {
            case AUTO_SELECTED:
                iconImage = Icons.ICON_AUTO_SELECTED;
                break;
            case SELECTED:
                iconImage = Icons.ICON_SELECTED;
                break;
            case RE_EXPOSED:
                iconImage = Icons.ICON_REEXPOSED;
                break;
            case WARNING_AUTO_SELECTED:
            case WARNING_SELECTED:
            case WARNING_NOT_SELECTED:
                iconImage = Icons.ICON_SELECTION_CLASH;
                break;
        }

        if (iconImage != null) {
            image = new RamImageComponent(iconImage, Colors.FEATURE_SELECTMODE_ICON_COLOR,
                    feature.getHeight() * SCALING_FEATURE_ICON, feature.getHeight() * SCALING_FEATURE_ICON);
            Vector3D position = new Vector3D(feature.getXposition() + feature.getWidth() / 2,
                    feature.getYposition() - feature.getHeight() / 6);
            image.setPositionGlobal(position);
            getContainerLayer().addChild(image);
            feature.setImage(image);
        }
    }

    /**
     * Draws the view associated for OR or XOR relationships if any.
     * 
     * @param pass - The {@link FeatureView} to consider.
     * @return the created view, or null if none.
     */
    private MTPolygon drawRelationshipTriangle(FeatureView pass) {
        MTPolygon triangle = pass.getRelationshipTriangle();

        boolean needTriangle = pass.getChildrenRelationship() == COREFeatureRelationshipType.XOR
                || pass.getChildrenRelationship() == COREFeatureRelationshipType.OR;

        // Delete the existing triangle since event if we need one, we need to redraw it so that it is well positionned
        if (triangle != null) {
            if (containerLayer.containsChild(triangle)) {
                containerLayer.removeChild(triangle);
            }
            triangle.destroy();
            pass.setRelationshipTriangle(null);
        }

        if (!needTriangle) {
            return null;
        }
        // Find the first child that is not a reuse
        // TODO: remove later
        FeatureView firstChild = null;
        for (FeatureView fv : pass.getChildrenFeatureViews()) {
            if (!fv.isReuse()) {
                firstChild = fv;
                break;
            }
        }
        if (firstChild == null) {
            return null;
        }

        FeatureView lastChild = pass.getChildrenFeatureViews().get(pass.getChildrenFeatureViews().size() - 1);
        Vertex[] vertices = new Vertex[3];
        vertices[0] = new Vertex(pass.getXposition(), pass.getYposition() + pass.getHeight() / 2);
        vertices[1] = new Vertex(firstChild.getXposition(), firstChild.getYposition() - pass.getHeight() / 2);
        vertices[2] = new Vertex(lastChild.getXposition(), lastChild.getYposition() - pass.getHeight() / 2);
        double ratio = 0.2;
        float xposition1 = (float) ((1 - ratio) * vertices[0].getX() + ratio * vertices[1].getX());
        float yposition1 = (float) ((1 - ratio) * vertices[0].getY() + ratio * vertices[1].getY());

        float xposition2 = (float) ((1 - ratio) * vertices[0].getX() + ratio * vertices[2].getX());
        float yposition2 = (float) ((1 - ratio) * vertices[0].getY() + ratio * vertices[2].getY());

        vertices[1] = new Vertex(xposition1, yposition1);
        vertices[2] = new Vertex(xposition2, yposition2);

        triangle = new MTPolygon(RamApp.getApplication(), vertices);
        triangle.setNoStroke(false);
        triangle.setStrokeColor(Colors.DEFAULT_ELEMENT_STROKE_COLOR);
        triangle.setStrokeWeight(2.0f);

        if (pass.getChildrenRelationship() == COREFeatureRelationshipType.OR) {
            triangle.setNoFill(false);
            triangle.setFillColor(Colors.FEATURE_OR_RELATIONSHIP_COLOR);
        } else if (pass.getChildrenRelationship() == COREFeatureRelationshipType.XOR) {
            triangle.setNoFill(true);
        }

        pass.setRelationshipTriangle(triangle);

        return triangle;
    }
    
    /**
     * Creates the circle for the relationship for the given view.
     * 
     * @param width the width of the view where the circle will be attached to
     * @return the circle
     */
    public static MTEllipse createRelationshipCircle(float width) {
        MTEllipse relCircle = new MTEllipse(RamApp.getApplication(), new Vector3D(width / 2, -6, 0), 8, 8);
        relCircle.removeAllGestureEventListeners();
        relCircle.setStrokeColor(Colors.DEFAULT_ELEMENT_STROKE_COLOR);
        relCircle.setStrokeWeight(2f);
        
        return relCircle;
    }

    /**
     * Draws the view associated for MANDATORY or OPTIONAL relationships if any.
     * 
     * @param pass - The {@link FeatureView} to consider.
     * @return the created view, or null if none.
     */
    private MTEllipse drawRelationshipCircle(FeatureView pass) {
        boolean needCircle = pass.getParent() != null
                && (pass.getParentRelationship() == COREFeatureRelationshipType.MANDATORY
                || pass.getParentRelationship() == COREFeatureRelationshipType.OPTIONAL);

        if (!needCircle) {
            if (pass.getRelationshipCircle() != null) {
                if (containerLayer.containsChild(pass.getRelationshipCircle())) {
                    containerLayer.removeChild(pass.getRelationshipCircle());
                }
                pass.getRelationshipCircle().destroy();
                pass.setCircle(null);
            }
            return null;
        } else if (pass.getRelationshipCircle() == null) {
            MTEllipse relCircle = createRelationshipCircle(pass.getWidth());
            pass.setCircle(relCircle);
        }
        
        return pass.getRelationshipCircle();
    }

//    /**
//     * Creates an arrow polygon at the given location.
//     * 
//     * @param x the x position
//     * @param y the y position
//     * @return the polygon representing an arrow
//     */
//    private static MTPolygon createArrowPolygon(float x, float y) {
//        Vertex start = new Vertex(x + 12, y - 5);
//        Vertex center = new Vertex(x, y);
//        Vertex end = new Vertex(x + 12, y + 5);
//
//        MTPolygon polygon = new MTPolygon(RamApp.getApplication(), new Vertex[] { start, center, end });
//
//        // trick to use polygon as a polyline
//        polygon.setNoFill(true);
//        polygon.setStrokeWeight(2.0f);
//        polygon.setStrokeColor(Colors.DEFAULT_ELEMENT_STROKE_COLOR);
//        polygon.setEnabled(false);
//        polygon.setPickable(false);
//
//        return polygon;
//    }

    /**
     * Function called to collect all Aspects realized by the Features in the diagram.
     * Used to unload resources.
     *
     * @return SetofAspects
     */
    public Set<Aspect> collectRealizedAspects() {
        Set<Aspect> aspects = new HashSet<Aspect>();
        
        for (COREArtefact a : featureModel.getCoreConcern().getArtefacts()) {
            if (a instanceof COREExternalArtefact) {
                if (((COREExternalArtefact) a).getRootModelElement() instanceof Aspect) {
                    Aspect aspect = (Aspect) ((COREExternalArtefact) a).getRootModelElement();
                    aspects.add(aspect);
                }
            }
        }
        return aspects;
    }

    /**
     * Get the FeatureView closest to the position in the diagram, if there is one.
     * 
     * @param position - The position to check
     * @return the closest {@link FeatureView} or null if there is no close enough feature
     */
    public FeatureView liesAround(Vector3D position) {
        for (FeatureView feature : getFeatureViews()) {
            if (MathUtils.pointIsInRectangle(position, feature, GUIConstants.MARGIN_ELEMENT_DETECTION)) {
                return feature;
            }
        }
        return null;
    }

    /**
     * Get the FeatureView containing the given position in the diagram, if there is one.
     * 
     * @param position - The position to check
     * @return the closest {@link FeatureView} or null if there is no feature at this position
     */
    public FeatureView liesInside(Vector3D position) {
        for (FeatureView feature : getFeatureViews()) {
            if (feature.containsPointGlobal(position)) {
                return feature;
            }
        }
        return null;
    }

    /**
     * Get all the {@link FeatureView} of the diagram.
     * 
     * @return the set of {@link FeatureView}
     */
    public Set<FeatureView> getFeatureViews() {
        Set<FeatureView> views = new HashSet<FeatureView>(features.values());

        return views;
    }

    /**
     * Get the root {@link FeatureView}.
     * 
     * @return The root {@link FeatureView}
     */
    public FeatureView getRootFeature() {
        return features.get(featureModel.getRoot());
    }

    /*
     * --------------- LAYOUT --------------------
     */
    /**
     * Function to return back the data structure of Rectangle.
     * 
     * @param node - Pass the node from which the rectangle needs to be extracted.
     * @return Rectangle - Return the rectangle corresponding to the node.
     */
    private static Rectangle2D.Double getBoundsOfNode(FeatureView node) {
        return treeLayout.getNodeBounds().get(node);
    }

    /**
     * Function called to get all the children of the parent.
     * 
     * @param parent - the feature
     * @return Children
     */
    private static Iterable<FeatureView> getChildren(FeatureView parent) {
        return treeLayout.getTree().getChildren(parent);
    }

    /**
     * Function called to set the position of all the features. In turn calls recursive functions which further make the
     * alignment perfect.
     */
    private void computeFeaturesPosition() {
        FeatureView rootFeature = getRootFeature();
        recursiveCall(rootFeature, 0, (float) gapBetweenLevels, 0);

        // This is to to determine at what percentage the feature is to be moved in relative spacing to width of screen.
        float offsetValue = rootFeature.getXposition() - (getWidth() / 2);
        offSet(rootFeature, offsetValue);
    }

    /**
     * Populate the tree layout with the {@link FeatureView} we want to display.
     */
    private void populateTree() {
        FeatureView rootFeature = getRootFeature();
        if (rootFeature == null) {
            return;
        }

        // Create a tree with root feature as per the existing data structure in jar.
        tree = new DefaultTreeForTreeLayout<FeatureView>(rootFeature);

        // With retrospect to rootFeature, add all to the featureViews.
        recurseAndAdd(rootFeature);

        // Copy the horizontal minimum distance.
        double gapBetweenNodes = INITIAL_HORIZONTAL_DISTANCE;

        // A modelReuse is created according to the data structure provided in the jar, passing the above two values.
        DefaultConfiguration<FeatureView> configuration = new DefaultConfiguration<FeatureView>(gapBetweenLevels,
                gapBetweenNodes);

        // Create the NodeExtentProvider for TextInBox nodes
        NodeExtentProvider<FeatureView> nodeExtentProvider = new DefaultNodeExtentProvider<FeatureView>();

        // Create the layout - Created by passing the tree, the modelReuse and the nodeExtentProvider.
        treeLayout = new TreeLayout<FeatureView>(tree, nodeExtentProvider, configuration);
    }

    /**
     * Offsets position of a feature and its children on the X axis.
     * 
     * @param feature - the feature to move
     * @param offSetValue - the offset to apply
     */
    private void offSet(FeatureView feature, float offSetValue) {
        feature.setXposition(feature.getXposition() - offSetValue);

        for (FeatureView child : feature.getChildrenFeatureViews()) {
            offSet(child, offSetValue);
        }
    }

    /**
     * Function called to adjust all the final positions of the feature. Recursive function. Calls all its children by
     * itself.
     *
     * @param feature - The feature to which the position is to be computed.
     * @param offsetX - The offset for the X axis.
     * @param offsetY - The gap between each level of the Feature Model.
     * @param position - THe horizontal position of the node.
     */
    private void recursiveCall(FeatureView feature, float offsetX, float offsetY, int position) {
        Rectangle2D.Double b1 = getBoundsOfNode(feature);
        double x1 = b1.getCenterX();
        double y1 = ((position * offsetY) + ((position + 1) * offsetY)) / 2;

        feature.setXposition((float) x1);
        feature.setYposition((float) y1);

        for (FeatureView child : getChildren(feature)) {
            recursiveCall(child, offsetX, offsetY, position + 1);
        }
    }

    /**
     * Function to recursively add all features from Feature Model to layouting data structure.
     * 
     * @param feature - feature to add to the tree
     */
    protected void recurseAndAdd(FeatureView feature) {
        for (FeatureView child : feature.getChildrenFeatureViews()) {
            DisplayOptions display = shouldDisplay(child);
            if (display == DisplayOptions.HIDE) {
                continue;
            }
            tree.addChild(feature, child);
            if (display != DisplayOptions.COLLAPSE) {
                recurseAndAdd(child);
                child.setLineStipple(LineStyle.SOLID.getStylePattern());
            } else {
                child.setLineStipple(LineStyle.DOTTED.getStylePattern());
            }
        }
    }

    /**
     * Check whether a {@link FeatureView} should be displayed or not.
     * This can be used to prevent some children and/or their children to be displayed.
     * 
     * @param child - The {@link FeatureView} we want to display
     * @return Whether the view should be displayed, hidden or collapsed
     */
    protected DisplayOptions shouldDisplay(FeatureView child) {
        if (collapsedFeatures.contains(child)) {
            return DisplayOptions.COLLAPSE;
        }
        return DisplayOptions.SHOW;
    }

    /**
     * Stop collapsing the given feature if it has no right to be collapsed anymore.
     * 
     * @param feature - the feature to expand
     */
    private void checkCollapseValildity(FeatureView feature) {
        if (collapsedFeatures.contains(feature) && !canCollapse(feature)) {
            collapsedFeatures.remove(feature);
            feature.setLineStipple(LineStyle.SOLID.getStylePattern());
        }
    }

    /**
     * Check if we have the right to collapse a given feature.
     * We can't if it's the root, or if it has no children.
     * 
     * @param featureView - The given {@link FeatureView}
     * @return whether the feature can be collapsed or not.
     */
    protected boolean canCollapse(FeatureView featureView) {
        if (featureView == null || featureView.getParentFeatureView() == null) {
            return false;
        }
        // Check if the feature has children only display children COREFeatures.
        return !featureView.getChildrenFeatureViews().isEmpty();
    }

    /**
     * Check all the features to see if they have to be un-collapsed.
     */
    public void checkCollapseValidity() {
        // Remove features that can no longer be collapsed
        for (FeatureView feature : features.values()) {
            checkCollapseValildity(feature);
        }
    }

    /**
     * Check if the diagram has at least a collapsed element.
     * 
     * @param onlyVisible - whether we want to check all elements, or only those that are visible in the current mode
     * @return true if at least a feature is collapsed
     */
    public final boolean hasCollapsedElements(boolean onlyVisible) {
        return getAllCollapsed(onlyVisible).size() > 0;
    }

    /**
     * Get all the currently collapsed {@link FeatureView}s.
     * 
     * @param onlyVisible - Whether we should return all collapsed features or only those visible in the current mode.
     * @return The set of collapsed {@link FeatureView}s
     */
    protected Set<FeatureView> getAllCollapsed(boolean onlyVisible) {
        Set<FeatureView> collapsed = new HashSet<FeatureView>();
        // Add normal features
        for (FeatureView feature : features.values()) {
            collapsed.addAll(getCollapsedFeatures(feature, onlyVisible));
        }

        return collapsed;
    }

    /**
     * Check is a given feature is currently collapsed or displayed.
     * If it is, return the list of collapsed features above this one in the hierarchy.
     * 
     * @param feature - The {@link COREFeature} to check
     * @return The collapsed features, or an empty list if none.
     */
    protected Set<FeatureView> getCollapsedFeatures(FeatureView feature) {
        return getCollapsedFeatures(feature, false);
    }

    /**
     * Check is a given feature is currently collapsed or displayed.
     * If it is, return the list of collapsed features above this one in the hierarchy.
     * 
     * @param feature - The {@link COREFeature} to check
     * @param onlyVisible - Whether we should return all collapsed features, or only the ones we see in current mode
     * @return The collapsed features, or an empty list if none.
     */
    protected Set<FeatureView> getCollapsedFeatures(FeatureView feature, boolean onlyVisible) {
        if (feature == null) {
            return new HashSet<FeatureView>();
        }
        Set<FeatureView> collapsed = new HashSet<FeatureView>();
        if (collapsedFeatures.contains(feature)) {
            collapsed.add(feature);
        }
        if (feature.getParentFeatureView() != null) {
            collapsed.addAll(getCollapsedFeatures(feature.getParentFeatureView(), onlyVisible));
        }
        return collapsed;
    }

}