package ca.mcgill.sel.ram.ui.views.message;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.provider.INotifyChangedListener;
import org.mt4j.components.TransformSpace;
import org.mt4j.input.gestureAction.TapAndHoldVisualizer;
import org.mt4j.input.inputProcessors.componentProcessors.dragProcessor.DragProcessor;
import org.mt4j.input.inputProcessors.componentProcessors.panProcessor.PanProcessorTwoFingers;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldProcessor;
import org.mt4j.input.inputProcessors.componentProcessors.zoomProcessor.ZoomProcessor;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.ram.AbstractMessageView;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.AspectMessageView;
import ca.mcgill.sel.ram.AssignmentStatement;
import ca.mcgill.sel.ram.CombinedFragment;
import ca.mcgill.sel.ram.ExecutionStatement;
import ca.mcgill.sel.ram.FragmentContainer;
import ca.mcgill.sel.ram.Gate;
import ca.mcgill.sel.ram.Interaction;
import ca.mcgill.sel.ram.InteractionFragment;
import ca.mcgill.sel.ram.InteractionOperand;
import ca.mcgill.sel.ram.LayoutElement;
import ca.mcgill.sel.ram.Lifeline;
import ca.mcgill.sel.ram.Message;
import ca.mcgill.sel.ram.MessageEnd;
import ca.mcgill.sel.ram.MessageOccurrenceSpecification;
import ca.mcgill.sel.ram.MessageSort;
import ca.mcgill.sel.ram.MessageView;
import ca.mcgill.sel.ram.Operation;
import ca.mcgill.sel.ram.OriginalBehaviorExecution;
import ca.mcgill.sel.ram.Parameter;
import ca.mcgill.sel.ram.ParameterValue;
import ca.mcgill.sel.ram.ParameterValueMapping;
import ca.mcgill.sel.ram.RamFactory;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.StructuralFeature;
import ca.mcgill.sel.ram.StructuralFeatureValue;
import ca.mcgill.sel.ram.TypedElement;
import ca.mcgill.sel.ram.ValueSpecification;
import ca.mcgill.sel.ram.impl.ContainerMapImpl;
import ca.mcgill.sel.ram.impl.ElementMapImpl;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.events.MouseWheelProcessor;
import ca.mcgill.sel.ram.ui.events.RightClickDragProcessor;
import ca.mcgill.sel.ram.ui.events.listeners.IDragListener;
import ca.mcgill.sel.ram.ui.layouts.AbstractBaseLayout;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.GUIConstants;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.AbstractView;
import ca.mcgill.sel.ram.ui.views.IRelationshipEndView;
import ca.mcgill.sel.ram.ui.views.RamEnd;
import ca.mcgill.sel.ram.ui.views.RamEnd.Position;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.ITextViewHandler;
import ca.mcgill.sel.ram.ui.views.message.handler.HandlerFactoryMessageView;
import ca.mcgill.sel.ram.ui.views.message.handler.IMessageViewHandler;
import ca.mcgill.sel.ram.ui.views.message.helpers.CompositeInteraction;
import ca.mcgill.sel.ram.util.MessageViewUtil;
import ca.mcgill.sel.ram.util.RamSwitch;

/**
 * The view responsible for visualizing {@link MessageView}s.
 * It is responsible for maintaining its children (lifelines, messages and fragments)
 * and layouting the message view as a whole. Layouting is done from top down in the order of how fragments are ordered.
 * 
 * @author mschoettle
 */
public class MessageViewView extends AbstractView<IMessageViewHandler> implements INotifyChangedListener {

    /**
     * The width for the invisible box that contains events and the line.
     */
    public static final float BOX_WIDTH = 25f;

    /**
     * The height for the invisible box that contains events and the line.
     */
    public static final float BOX_HEIGHT = 20f;

    /**
     * Helper class to handle the adding of different kinds of interaction fragments.
     */
    private final class AddFragmentSwitcher extends RamSwitch<Object> {
        @Override
        public Object caseExecutionStatement(ExecutionStatement object) {
            addStatementView(object);
            return Boolean.TRUE;
        }

        @Override
        public Object caseCombinedFragment(CombinedFragment object) {
            addCombinedFragment(object);
            return Boolean.TRUE;
        }

        @Override
        public Object caseAssignmentStatement(AssignmentStatement object) {
            addAssignmentStatement(object);
            return Boolean.TRUE;
        }

        @Override
        public Object caseMessageOccurrenceSpecification(MessageOccurrenceSpecification end) {
            addMessageEnd(end);
            return Boolean.TRUE;
        }

        @Override
        public Object caseOriginalBehaviorExecution(OriginalBehaviorExecution object) {
            addOriginalBehaviourView(object);
            return Boolean.TRUE;
        }
    }

    /**
     * Helper class to handle the removal of different kinds of interaction fragments.
     */
    private final class RemoveFragmentSwitcher extends RamSwitch<Object> {
        @Override
        public Object caseExecutionStatement(ExecutionStatement object) {
            removeInteractionFragment(object);
            return Boolean.TRUE;
        }

        @Override
        public Object caseCombinedFragment(CombinedFragment object) {
            removeCombinedFragment(object);
            return Boolean.TRUE;
        }

        @Override
        public Object caseAssignmentStatement(AssignmentStatement object) {
            removeInteractionFragment(object);
            return Boolean.TRUE;
        }

        @Override
        public Object caseMessageOccurrenceSpecification(MessageOccurrenceSpecification object) {
            removeInteractionFragment(object);
            return Boolean.TRUE;
        }
        
        @Override
        public Object caseOriginalBehaviorExecution(OriginalBehaviorExecution object) {
            removeInteractionFragment(object);
            return Boolean.TRUE;
        }
    }

    /**
     * The view representing a {@link ca.mcgill.sel.ram.Gate}.
     * It is an invisible component that can be used for a relationship view.
     * 
     * @see IRelationshipEndView
     * @author mschoettle
     */
    private class GateView extends RamRectangleComponent implements IRelationshipEndView {

        /**
         * Creates a new view for a gate at the given position and dimensions.
         * The positions and dimension should reflect the corresponding ones to the opposite end.
         * 
         * @param x the x position
         * @param y the y position
         * @param width the width
         * @param height the height
         */
        GateView(float x, float y, float width, float height) {
            super(x, y, width, height);
        }

        @Override
        public void moveRelationshipEnd(RamEnd<?, ?> end, Position oldPosition, Position newPosition) {
            updateRelationshipEnd(end);
        }

        @Override
        public void updateRelationshipEnd(RamEnd<?, ?> end) {
            RamEnd<?, ?> otherEnd = end.getOpposite();

            if (end.getLocation().getY() != otherEnd.getLocation().getY()) {
                Vector3D location = new Vector3D(otherEnd.getLocation());
                location.setX(end.getLocation().getX());
                end.setLocation(location);
                // Always force the position to be right.
                end.setPosition(Position.RIGHT);
                setPositionRelativeToParent(location);
            }
        }

        @Override
        public void removeRelationshipEnd(RamEnd<?, ?> end) {
            destroy();
        }
    }

    /**
     * The internal layout of this view.
     * It receives layout requests from children and performs appropriate layouting.
     * 
     * @author mschoettle
     */
    private class InternalLayout extends AbstractBaseLayout {

        @Override
        public void layout(RamRectangleComponent component, LayoutUpdatePhase updatePhase) {
            layoutCombinedFragments();
        }

    }

    /**
     * The fixed y position of lifelines.
     */
    public static final float LIFELINE_Y = 100.0f;
    private static final float LIFELINE_START_X = 100.0f;
    private static final float LIFELINE_SPACING = 20.0f;

    private Aspect aspect;
    private AbstractMessageView messageView;
    private CompositeInteraction compositeSpecification;
    private Operation specifies;
    private ContainerMapImpl layout;

    private Map<TypedElement, LifelineView> lifelines;
    private Map<Message, MessageCallView> messages;
    private Map<CombinedFragment, CombinedFragmentView> combinedFragments;
    private Map<InteractionFragment, RamRectangleComponent> fragments;

    private boolean building;

    private RamSwitch<Object> addFragmentSwitcher = new AddFragmentSwitcher();
    private RamSwitch<Object> removeFragmentSwitcher = new RemoveFragmentSwitcher();

    /**
     * Creates a new view for the given message view.
     * 
     * @param messageView the {@link MessageView} to visualize
     * @param layout the layout of this message view
     * @param width the width to use
     * @param height the height to use
     */
    public MessageViewView(AbstractMessageView messageView, ContainerMapImpl layout, float width, float height) {
        super(width, height);

        this.aspect = EMFModelUtil.getRootContainerOfType(messageView, RamPackage.Literals.ASPECT);
        this.messageView = messageView;
        this.layout = layout;

        if (messageView instanceof MessageView) {
            MessageView actualMessageView = (MessageView) messageView;
            compositeSpecification = new CompositeInteraction(actualMessageView.getSpecification());
            specifies = actualMessageView.getSpecifies();
        } else {
            AspectMessageView actualMessageView = (AspectMessageView) messageView;
            compositeSpecification = new CompositeInteraction(actualMessageView.getAdvice());
            specifies = actualMessageView.getPointcut();
        }

        lifelines = new HashMap<>();
        messages = new HashMap<>();
        combinedFragments = new HashMap<>();
        fragments = new HashMap<>();

        setNoFill(true);
        setLayout(new InternalLayout());

        build();
        layoutMessageView();

        EMFEditUtil.addListenerFor(aspect, this);
        EMFEditUtil.addListenerFor(compositeSpecification.getInitialInteraction(), this);
        EMFEditUtil.addListenerFor(layout, this);
    }

    @Override
    public void destroy() {
        EMFEditUtil.removeListenerFor(aspect, this);
        EMFEditUtil.removeListenerFor(layout, this);

        for (Interaction interaction : compositeSpecification.getInteractions()) {
            EMFEditUtil.removeListenerFor(interaction, this);
        }

        for (LifelineView lifelineView : lifelines.values()) {
            for (Lifeline lifeline : lifelineView.getRepresentedLifelines()) {
                EMFEditUtil.removeListenerFor(lifeline, this);
            }
        }

        for (CombinedFragment combinedFragment : combinedFragments.keySet()) {
            for (InteractionOperand operand : combinedFragment.getOperands()) {
                EMFEditUtil.removeListenerFor(operand, this);
            }
        }

        super.destroy();
    }

    @Override
    protected void registerInputProcessors() {
        registerInputProcessor(new PanProcessorTwoFingers(RamApp.getApplication()));
        registerInputProcessor(new RightClickDragProcessor(RamApp.getApplication()));
        registerInputProcessor(new ZoomProcessor(RamApp.getApplication()));
        registerInputProcessor(new MouseWheelProcessor(RamApp.getApplication()));
    }

    /**
     * Builds the visualization of this view.
     * Takes care of initializing all contents and the actual elements of the message view.
     */
    private void build() {
        building = true;
                
        for (Lifeline lifeline : compositeSpecification.getLifelines()) {
            LayoutElement layoutElement = layout.getValue().get(lifeline);

            // This should only happen for lifelines created outside TouchCORE, e.g., in the Eclipse editor.
            if (layoutElement == null) {
                layoutElement = RamFactory.eINSTANCE.createLayoutElement();
                layout.getValue().put(lifeline, layoutElement);
            }

            addLifelineView(lifeline, layout.getValue().get(lifeline));
        }

        buildFragments(compositeSpecification.getInitialInteraction());

        for (Message message : compositeSpecification.getMessages()) {
            addMessageView(message);
        }

        building = false;
    }

    /**
     * Builds all fragments in the appropriate order to display in this view.
     * Can be called recursively for different containers.
     * 
     * @param container the container
     */
    private void buildFragments(FragmentContainer container) {
        building = true;

        for (InteractionFragment fragment : compositeSpecification.getVisualizedFragments(container)) {
            addFragmentSwitcher.doSwitch(fragment);
        }

        building = false;
    }

    /**
     * Adds a new view for the given lifeline to this view with a custom layout element.
     * 
     * @param lifeline the {@link Lifeline} a view is required for
     * @param layoutElement the layout element
     */
    private void addLifelineView(Lifeline lifeline, LayoutElement layoutElement) {
        EMFEditUtil.addListenerFor(lifeline, this);

        // Get the map between parameters and their actual parameter value mapping
        Map<Parameter, ParameterValueMapping> parameterMap = compositeSpecification.getParameterMappings();
        TypedElement actualRepresents = null;

        // If there is a mapping, the life line must represent the actual parameter
        if (parameterMap.containsKey(lifeline.getRepresents())) {
            ParameterValueMapping argument = parameterMap.get(lifeline.getRepresents());
            actualRepresents = extractTypedElement(argument.getValue());
        }

        // If no mapping was found, or if the parameter value mapping has a null value, take the
        // represents of the given life line
        if (actualRepresents == null) {
            actualRepresents = lifeline.getRepresents();
        }

        LifelineView view = null;

        if (!lifelines.containsKey(actualRepresents)) {
            view = new LifelineView(this, actualRepresents, lifeline, layoutElement);
            view.setHandler(HandlerFactoryMessageView.INSTANCE.getLifelineViewHandler());
            addChild(view);
        } else {
            view = lifelines.get(actualRepresents);
        }

        lifelines.put(lifeline.getRepresents(), view);
        lifelines.put(actualRepresents, view);
    }

    /**
     * Removes the given lifeline from this view.
     * 
     * @param lifeline the {@link Lifeline} to remove
     */
    private void removeLifelineView(Lifeline lifeline) {
        LifelineView lifelineView = lifelines.get(lifeline.getRepresents());
        lifelineView.removeRepresentingLifeline(lifeline);
        EMFEditUtil.removeListenerFor(lifeline, this);

        if (lifeline.getRepresents() != lifelineView.getRepresentedTypedElement()) {
            lifelines.remove(lifeline.getRepresents());
        }

        if (lifelineView.getRepresentedLifelines().isEmpty()) {
            lifelines.remove(lifelineView.getRepresentedTypedElement());
            removeChild(lifelineView);
            lifelineView.destroy();
        }
    }

    /**
     * Adds the given message end to the covered lifeline and the corresponding message.
     * 
     * @param end the message end to add
     */
    private void addMessageEnd(MessageOccurrenceSpecification end) {
        Message message = end.getMessage();
        Lifeline lifeline = end.getCovered().get(0);
        LifelineView view = lifelines.get(lifeline.getRepresents());

        /*
         * When a message to a non existing life line is created, it is possible that this life line
         * is already represented. If it is the case, we need to add the life line to the existing view
         */
        if (!view.getRepresentedLifelines().contains(lifeline)) {
            view.addRepresentingLifeline(lifeline);
        }

        boolean isSendEvent = message.getSendEvent() == end;
        RamRectangleComponent endView = null;

        if (!isSendEvent && message.getMessageSort() == MessageSort.CREATE_MESSAGE
                && !message.isSelfMessage()) {
            endView = view;
        } else {
            boolean allowMessageCreation = messageCreationAllowedAfter(message, end);
            endView = view.addMessageEnd(end, allowMessageCreation);
        }

        fragments.put(end, endView);

        MessageCallView messageCallView = messages.get(message);

        if (messageCallView != null) {
            if (isSendEvent) {
                messageCallView.getFromEnd().setComponentView(endView);
            } else {
                messageCallView.getToEnd().setComponentView(endView);
            }

            layoutMessageView();
        }
    }

    /**
     * Returns whether after the given message end, another message or fragment can be added.
     * After send events, message creation is allowed, unless there is a reply message.
     * After receive events, message creation is only allowed when nested behaviour can be defined.
     * 
     * @param message the message the end belongs to
     * @param end the message end
     * @return true, if creation is allowed after, false otherwise
     */
    protected boolean messageCreationAllowedAfter(Message message, MessageEnd end) {
        Operation signature = message.getSignature();
        boolean messageViewDefined = false;

        if (signature != null && specifies != signature) {
            Aspect signatureAspect = EMFModelUtil.getRootContainerOfType(signature, RamPackage.Literals.ASPECT);
            /**
             * Make sure that the signature still exists in the aspect.
             * It could have been deleted and would then cause problems here.
             */
            if (signatureAspect != null) {
                messageViewDefined = MessageViewUtil.isMessageViewDefined(signatureAspect, signature);
                /**
                 * Make sure nested behaviour can only be defined when the message view is within the same aspect.
                 *
                 * @see issue #390, #396 and #425
                 */
                messageViewDefined |= signatureAspect != messageView.eContainer();
            }
        }

        boolean allowMessageCreation = false;

        // Allow creation after sending a message where no return is expected.
        if (signature != null && signature.getReturnType() != null) {
            if (message.isSelfMessage()) {
                allowMessageCreation = false;
            } else {
                Interaction interaction = message.getInteraction();
                Message initialMessage = null;

                // The interaction can be null if the message is a temporary one
                if (interaction != null) {
                    InteractionFragment initialFragment = interaction.getFragments().get(0);
                    initialMessage = ((MessageOccurrenceSpecification) initialFragment).getMessage();
                }

                allowMessageCreation = message.getMessageSort() != MessageSort.REPLY
                        && (messageViewDefined || message != initialMessage);
            }
        }

        /**
         * Revert for the receive event to make sure it is the opposite.
         */
        if (message.getReceiveEvent() == end) {
            allowMessageCreation = !allowMessageCreation;
        }

        return allowMessageCreation;
    }

    /**
     * Adds a new view for the given message to this view.
     * A message is represented by a view on each end and a view representing the actual call.
     * The end views are added to the corresponding lifelines.
     * In case an end is a gate and not placed on a lifeline,
     * a {@link GateView} is used instead and added to this view at the left side.
     * 
     * @param message the {@link Message} to add
     */
    private void addMessageView(Message message) {
        MessageEnd sendEvent = message.getSendEvent();
        MessageEnd receiveEvent = message.getReceiveEvent();
        @SuppressWarnings("unlikely-arg-type")
        RamRectangleComponent sendEventView = fragments.get(sendEvent);
        @SuppressWarnings("unlikely-arg-type")
        RamRectangleComponent receiveEventView = fragments.get(receiveEvent);
        LifelineView lifelineToConnectWith = null;

        // Create Gates right away.
        if (sendEvent instanceof Gate) {
            sendEventView = new GateView(0, 0, BOX_WIDTH, BOX_HEIGHT);
            addChild(sendEventView);
        } else if (receiveEvent instanceof Gate) {
            receiveEventView = new GateView(0, 0, BOX_WIDTH, BOX_HEIGHT);

            if (!compositeSpecification.getInitialInteraction().getMessages().contains(message)) {
                Lifeline callingLifeline = compositeSpecification.getCallingLifeline(message.getInteraction());
                lifelineToConnectWith = lifelines.get(callingLifeline.getRepresents());
            }

            addChild(receiveEventView);
        }

        MessageCallView messageCallView = new MessageCallView(message, this, sendEventView, receiveEventView);
        addChild(messageCallView);
        messageCallView.setHandler(HandlerFactoryMessageView.INSTANCE.getMessageHandler());
        messages.put(message, messageCallView);

        layoutMessageView();

        if (lifelineToConnectWith != null) {
            lifelineToConnectWith.addConnectedMessageEnd(receiveEvent);
        }

        if (sendEventView != null && receiveEventView != null) {
            messageCallView.updateLines();
        }
    }

    /**
     * Removes the given message from this view.
     * The ends are removed from their attached views.
     * 
     * @param message the {@link Message} to remove
     */
    private void removeMessageView(Message message) {
        MessageCallView messageCallView = messages.remove(message);

        IRelationshipEndView fromView = (IRelationshipEndView) messageCallView.getFromEnd().getComponentView();
        IRelationshipEndView toView = (IRelationshipEndView) messageCallView.getToEnd().getComponentView();
        fromView.removeRelationshipEnd(messageCallView.getFromEnd());
        toView.removeRelationshipEnd(messageCallView.getToEnd());

        removeChild(messageCallView);
        messageCallView.destroy();

        layoutMessageView();
    }

    /**
     * Requests that all message lines are updated.
     * This triggers a recalculation and redrawing of each message call.
     */
    public void updateMessageViews() {
        for (MessageCallView messageCall : messages.values()) {
            if (messageCall.getFromEnd().getComponentView() != null
                    && messageCall.getToEnd().getComponentView() != null) {
                messageCall.updateLines();
            }
        }
    }

    /**
     * Adds the given execution statement to the lifeline view it covers.
     * 
     * @param executionStatement the {@link ExecutionStatement} to add
     */
    private void addStatementView(ExecutionStatement executionStatement) {
        Lifeline lifeline = executionStatement.getCovered().get(0);
        LifelineView lifelineView = lifelines.get(lifeline.getRepresents());

        // Currently we assume that the statements specification is always an opaque expression.
        // If this changes we need to adaptively get the actual value.
        TextView textView = new TextView(executionStatement, RamPackage.Literals.EXECUTION_STATEMENT__SPECIFICATION);
        textView.setNoFill(false);
        textView.setNoStroke(false);
        textView.setFillColor(Colors.MESSAGE_STATEMENT_FILL_COLOR);

        ITextViewHandler textViewHandler = HandlerFactoryMessageView.INSTANCE.getExecutionStatementHandler();
        IDragListener dragHandler = HandlerFactoryMessageView.INSTANCE.getDragHandler();
        
        textView.registerInputProcessor(new DragProcessor(RamApp.getApplication()));
        textView.registerTapProcessor(textViewHandler);
        textView.registerInputProcessor(
                new TapAndHoldProcessor(RamApp.getApplication(), GUIConstants.TAP_AND_HOLD_DURATION));
        
        textView.addGestureListener(DragProcessor.class, dragHandler);
        textView.addGestureListener(TapAndHoldProcessor.class, textViewHandler);
        textView.addGestureListener(TapAndHoldProcessor.class,
                new TapAndHoldVisualizer(RamApp.getApplication(), RamApp.getActiveScene().getCanvas()));
        textView.setPlaceholderText(Strings.PH_SPECIFY_STATEMENT);
        addChild(textView);

        lifelineView.addStatement(textView, executionStatement);
        fragments.put(executionStatement, textView);

        layoutMessageView();
    }

    /**
     * Creates a view for the given assignment statement and adds it to the lifeline view it covers.
     * 
     * @param assignmentStatement the {@link AssignmentStatement} to add
     */
    private void addAssignmentStatement(AssignmentStatement assignmentStatement) {
        Lifeline lifeline = assignmentStatement.getCovered().get(0);
        LifelineView lifelineView = lifelines.get(lifeline.getRepresents());

        AssignmentStatementView assignmentView = new AssignmentStatementView(assignmentStatement);
        addChild(assignmentView);
        assignmentView.setHandler(HandlerFactoryMessageView.INSTANCE.getAssignmentStatementHandler());

        lifelineView.addAssignment(assignmentView, assignmentStatement);
        fragments.put(assignmentStatement, assignmentView);

        layoutMessageView();
    }

    /**
     * Adds the given original behaviour execution fragment to the lifeline view it covers.
     * 
     * @param originalBehaviorExecution the {@link OriginalBehaviorExecution} to add
     */
    private void addOriginalBehaviourView(OriginalBehaviorExecution originalBehaviorExecution) {
        Lifeline lifeline = originalBehaviorExecution.getCovered().get(0);
        LifelineView lifelineView = lifelines.get(lifeline.getRepresents());

        // Currently we assume that the statements specification is always an opaque expression.
        // If this changes we need to adaptively get the actual value.
        TextView textView = new TextView(originalBehaviorExecution, RamPackage.Literals.NAMED_ELEMENT__NAME);
        textView.setNoFill(false);
        textView.setNoStroke(false);
        textView.setFillColor(Colors.DEFAULT_ELEMENT_FILL_COLOR);
        textView.setBufferSize(Cardinal.NORTH, 10);
        textView.setBufferSize(Cardinal.EAST, 10);
        textView.setBufferSize(Cardinal.WEST, 10);
        textView.setBufferSize(Cardinal.SOUTH, 0);
        
        IDragListener dragHandler = HandlerFactoryMessageView.INSTANCE.getDragHandler();
        textView.registerInputProcessor(new DragProcessor(RamApp.getApplication()));
        textView.addGestureListener(DragProcessor.class, dragHandler);

        addChild(textView);

        lifelineView.addOriginalBehaviour(textView, originalBehaviorExecution);
        fragments.put(originalBehaviorExecution, textView);

        layoutMessageView();
    }

    /**
     * Removes the interaction fragment from its covered lifeline view.
     * 
     * @param interactionFragment the {@link InteractionFragment} to remove
     */
    private void removeInteractionFragment(InteractionFragment interactionFragment) {
        RamRectangleComponent view = fragments.get(interactionFragment);

        if (interactionFragment instanceof MessageOccurrenceSpecification) {
            Message message = ((MessageOccurrenceSpecification) interactionFragment).getMessage();
            collapse(message);
        }

        Lifeline lifeline = interactionFragment.getCovered().get(0);
        LifelineView lifelineView = lifelines.get(lifeline.getRepresents());

        // Ensure that it is not the receive event of a create message,
        // which is not actually placed on the lifeline view.
        if (fragments.get(interactionFragment) != lifelineView) {
            lifelineView.removeInteractionFragment(interactionFragment);
            view.destroy();
            layoutMessageView();
        }

        fragments.remove(interactionFragment);
    }

    /**
     * Adds the given combined fragment to this view.
     * 
     * @param combinedFragment the {@link CombinedFragment} to add
     */
    private void addCombinedFragment(CombinedFragment combinedFragment) {
        CombinedFragmentView combinedFragmentView = new CombinedFragmentView(this, combinedFragment);
        addChild(combinedFragmentView);

        combinedFragments.put(combinedFragment, combinedFragmentView);
        fragments.put(combinedFragment, combinedFragmentView);

        for (InteractionOperand operand : combinedFragment.getOperands()) {
            addOperandContents(combinedFragment, operand);
        }
    }

    /**
     * Removes the combined fragment from this view.
     * Removes the combined fragment from all covered lifeline views.
     * 
     * @param combinedFragment the {@link CombinedFragment} to remove
     */
    private void removeCombinedFragment(CombinedFragment combinedFragment) {
        for (InteractionOperand operand : combinedFragment.getOperands()) {
            removeOperandContents(operand);
        }

        CombinedFragmentView combinedFragmentView = combinedFragments.remove(combinedFragment);
        fragments.remove(combinedFragment);

        // For debug purpose only.
        if (combinedFragmentView == null) {
            System.err.println("Should not happen: Something went wrong in the order of removing objects.");
        }

        // At this point compositeInteraction.getCoveredLifelines returns an empty list, since there is no
        // more visualized fragments in this combined fragment
        for (Lifeline lifeline : combinedFragment.getCovered()) {
            LifelineView lifelineView = lifelines.get(lifeline.getRepresents());
            lifelineView.removeCombinedFragment(combinedFragment);
        }

        removeChild(combinedFragmentView);
        combinedFragmentView.destroy();

        layoutMessageView();
    }

    /**
     * Adds all fragments of the given operand to the message view UI.
     * 
     * @param combinedFragment the {@link CombinedFragment} the operand belongs to
     * @param operand the operand whose fragments to add to the UI
     */
    public void addOperandContents(CombinedFragment combinedFragment, InteractionOperand operand) {
        buildFragments(operand);

        layoutMessageView();

        EMFEditUtil.addListenerFor(operand, this);
    }

    /**
     * Removes all fragments of the given operand from the message view UI.
     * 
     * @param operand the operand whose fragments to remove from the UI
     */
    public void removeOperandContents(InteractionOperand operand) {
        // Ensure that the previous building state is set back
        // in case this is called as part of a larger internal operation.
        boolean previousBuildingState = building;

        building = true;

        for (InteractionFragment fragment : operand.getFragments()) {
            if (fragments.containsKey(fragment)) {
                removeFragmentSwitcher.doSwitch(fragment);
            }
        }

        building = previousBuildingState;

        layoutMessageView();

        EMFEditUtil.removeListenerFor(operand, this);
    }

    /**
     * Returns a list of all lifeline views this view visualizes.
     * 
     * @return the list of all lifeline views
     */
    public Collection<LifelineView> getLifelineViews() {
        return new HashSet<>(lifelines.values());
    }

    /**
     * Returns the lifeline view for the given lifeline.
     * If there is no view for the lifeline, null is returned.
     * 
     * @param lifeline the {@link Lifeline}
     * @return the lifeline view for the given lifeline, null if no view exists
     */
    public LifelineView getLifelineView(Lifeline lifeline) {
        return lifelines.get(lifeline.getRepresents());
    }

    /**
     * Returns the message call view for the given message.
     * If there is no view for the message, null is returned.
     * 
     * @param message the {@link Message}
     * @return the message call view for the given message, null if no view exists
     */
    public MessageCallView getMessageCallView(Message message) {
        return messages.get(message);
    }

    /**
     * Returns the {@link AbstractMessageView} visualized by this view.
     * 
     * @return the message view
     */
    public AbstractMessageView getMessageView() {
        return messageView;
    }

    /**
     * Returns the combined fragment view for a given combined fragment.
     * 
     * @param combinedFragment the {@link CombinedFragment}
     * @return the view
     */
    public CombinedFragmentView getCombinedFragmentView(CombinedFragment combinedFragment) {
        return combinedFragments.get(combinedFragment);
    }

    /**
     * Returns the {@link Interaction} visualized by this view.
     * 
     * @return the interaction
     */
    public Interaction getSpecification() {
        return compositeSpecification.getInitialInteraction();
    }

    /**
     * Returns the {@link CompositeInteraction} visualized by this view.
     * 
     * @return the composite interaction
     */
    public CompositeInteraction getCompositeSpecification() {
        return compositeSpecification;
    }

    /**
     * Returns the operation this message view specifies.
     * In case of an {@link AspectMessageView}, it is the pointcut.
     * 
     * @return the operation this message view specifies.
     */
    public Operation getSpecifies() {
        return specifies;
    }

    /**
     * Layouts the complete view.
     * This includes lifelines, combined fragments, other fragments and the message calls.
     */
    public void layoutMessageView() {
        if (building) {
            return;
        }

        layoutLifelines();
        layoutCombinedFragments();

        Interaction initial = compositeSpecification.getInitialInteraction();
        layoutFragments(initial, getFirstLifelineY());

        updateMessageViews();
    }

    /**
     * Returns the global y-position of the first lifeline (target).
     * 
     * @return the y-position of the first lifeline
     */
    private float getFirstLifelineY() {
        // Get the target lifeline.
        Lifeline lifeline = getSpecification().getLifelines().get(0);
        // For some reason there is a small difference between the y position which is needed.
        return getLifelineView(lifeline).getPosition(TransformSpace.GLOBAL).getY() + 6;
    }

    /**
     * Layouts all lifelines.
     * The lifelines are placed at a certain height and along the x-axis with a specified distance between them.
     * Lifelines that contain a stereotype in their name are placed slightly higher such that the bottom of each name
     * is flush among all lifelines.
     */
    private void layoutLifelines() {
        float currentLifelineX = LIFELINE_START_X;
        Set<Lifeline> layoutedLifelines = new HashSet<Lifeline>();

        /*
         * Layout lifelines if they have no position. Otherwise they might have been moved on the x-axis,
         * which we allow to do.
         * It is required to get the list of ALL fragments in the message view, i.e. including the ones located
         * within the combined fragments. This allows to avoid missing the life lines that are only used by
         * messages located within combined fragments.
         */
        for (InteractionFragment fragment : compositeSpecification.getAllFragments()) {
            // In case the message add event hasn't been received yet there will be no lifeline that is covered.
            // This is the case when a message and a reply are added.
            // The structure exists, but not all commands have been executed.
            for (Lifeline lifeline : fragment.getCovered()) {
                if (!layoutedLifelines.contains(lifeline)) {
                    LifelineView lifelineView = lifelines.get(lifeline.getRepresents());
                    LayoutElement layoutElement = lifelineView.getLayoutElement();

                    /**
                     * Lifelines of metaclasses are double as high as regular lifelines.
                     * This causes problems when dealing with combined fragments or when drawing a message
                     * from a message view of a create message to one (as the first message).
                     * Therefore, these lifelines are moved up so that all lifelines are flush on the bottom
                     * of their name container.
                     * 
                     * @see issue #230
                     */
                    if (lifeline.getRepresents() instanceof StructuralFeature) {
                        StructuralFeature structuralFeature = (StructuralFeature) lifeline.getRepresents();

                        if (structuralFeature.isStatic()) {
                            float y = LIFELINE_Y - (lifelineView.getNameHeight() / 2);
                            if (y != lifelineView.getLayoutElement().getY()) {
                                lifelineView.getLayoutElement().setY(y);
                            }
                        }
                    }

                    if (layoutElement.getX() == 0) {
                        float x = lifelines.keySet().size() > 1 ? currentLifelineX + 50 : currentLifelineX;
                        lifelineView.getLayoutElement().setX(x);
                    }

                    if (layoutElement.getY() == 0) {
                        lifelineView.getLayoutElement().setY(LIFELINE_Y);
                    }

                    // Make sure that the last spacer on the life line has the minimum height
                    RamRectangleComponent lastSpacer = lifelineView.getLastSpacer();
                    if (lastSpacer.getHeight() > BOX_HEIGHT) {
                        lastSpacer.setMinimumHeight(BOX_HEIGHT);
                        lastSpacer.updateParent();
                    }

                    // Set the new life line x
                    float increasedLifelineX = lifelineView.getX() + lifelineView.getWidth();
                    currentLifelineX = Math.max(currentLifelineX, increasedLifelineX) + LIFELINE_SPACING;

                    lifelineView.updateMessageCallViews();
                    layoutedLifelines.add(lifeline);
                }
            }
        }
    }

    /**
     * Layouts all fragments in the given fragment container.
     * Can be called recursively for different fragment containers.
     * 
     * @param container the fragment container
     * @param nextfragmentY the y-position for the next fragment
     * @return the y-position for the next fragment
     */
    private float layoutFragments(FragmentContainer container, float nextfragmentY) {
        float previousFragmentY = nextfragmentY;
        float currentFragmentY = nextfragmentY;
        boolean receiveEventNext = false;

        // Make sure that all messages are updated, since this might be necessary due to moving the lifelines around.
        for (InteractionFragment fragment : compositeSpecification.getVisualizedFragments(container)) {
            List<Lifeline> coveredLifelines = compositeSpecification.getCoveredLifelines(fragment);

            for (Lifeline lifeline : coveredLifelines) {
                LifelineView lifelineView = lifelines.get(lifeline.getRepresents());

                // In case of a create message the lifeline has to be moved down depending on the name height.
                if (fragment instanceof MessageOccurrenceSpecification) {
                    MessageOccurrenceSpecification messageEnd = (MessageOccurrenceSpecification) fragment;

                    // If the message was removed, but the message ends are still there,
                    // ignore them.
                    if (!messages.containsKey(messageEnd.getMessage())) {
                        continue;
                    }

                    if (messageEnd.getMessage().getMessageSort() == MessageSort.CREATE_MESSAGE
                            && !messageEnd.getMessage().isSelfMessage()
                            && messageEnd.getMessage().getReceiveEvent() == messageEnd) {
                        float lifelineNameHeight = lifelineView.getNameHeight();
                        float difference = lifelineNameHeight - BOX_HEIGHT;
                        float lifelineY = currentFragmentY - (difference / 2f);
                        /**
                         * Convert global position to relative one.
                         */
                        lifelineY = getGlobalVecToParentRelativeSpace(lifelineView, new Vector3D(0, lifelineY)).y;
                        lifelineView.getLayoutElement().setY(lifelineY);
                        currentFragmentY += lifelineNameHeight;
                        receiveEventNext = false;
                        continue;
                    }
                }

                RamRectangleComponent fragmentView = lifelineView.getFragmentView(fragment);
                // view is null when message was just removed
                if (fragmentView != null) {
                    RamRectangleComponent spacer = lifelineView.getSpacerForFragmentAt(fragment);

                    // Start with the position of the first spacer.
                    if (currentFragmentY == getFirstLifelineY()) {
                        currentFragmentY = spacer.getPosition(TransformSpace.GLOBAL).getY();
                    }

                    if (!receiveEventNext) {
                        currentFragmentY += BOX_HEIGHT;
                    }

                    Vector3D currentPosition = fragmentView.getPosition(TransformSpace.GLOBAL);

                    if (currentPosition.getY() != currentFragmentY) {
                        float difference = currentFragmentY - currentPosition.getY();
                        float height = spacer.getHeight() + difference;
                        // Fix the height to not go below the minimum size.
                        if (height < BOX_HEIGHT) {
                            height = BOX_HEIGHT;
                        }

                        spacer.setMinimumHeight(height);
                        // Need to inform parent manually.
                        spacer.updateParent();
                    }

                    if (fragment instanceof MessageOccurrenceSpecification) {
                        MessageOccurrenceSpecification messageEnd = (MessageOccurrenceSpecification) fragment;
                        Message message = messageEnd.getMessage();

                        /**
                         * Prevent moving downwards if the next event will be a receive event.
                         * However, if it is a self message we need to move downwards.
                         * If it is the send event of a reply that ends in a gate, there will be no next event.
                         */
                        if (!message.isSelfMessage() && message.getSendEvent() == messageEnd
                                && message.getReceiveEvent().eClass() != RamPackage.Literals.GATE) {
                            receiveEventNext = true;
                        } else {
                            receiveEventNext = false;
                        }
                    }

                    if (fragment instanceof CombinedFragment) {
                        CombinedFragment combinedFragment = (CombinedFragment) fragment;
                        CombinedFragmentView combinedFragmentView = combinedFragments.get(combinedFragment);

                        if (coveredLifelines.indexOf(lifeline) == 0) {
                            Vector3D position = combinedFragmentView.getPosition(TransformSpace.GLOBAL);
                            position.setY(currentFragmentY);
                            combinedFragmentView.setPositionGlobal(position);

                            receiveEventNext = true;
                        }

                        // Only increase y if it is the last lifeline.
                        int lastIndex = coveredLifelines.size() - 1;
                        if (coveredLifelines.indexOf(lifeline) == lastIndex) {
                            for (InteractionOperand operand : combinedFragment.getOperands()) {
                                float operandHeight = combinedFragmentView.getOperandMinimumHeight(operand);

                                if (operand.getFragments().size() > 0) {
                                    operandHeight += layoutFragments(operand, currentFragmentY + operandHeight);
                                }

                                // Add an additional space due to the additional spacer in the operand.
                                // This also supports empty operands.
                                operandHeight += BOX_HEIGHT;
                                combinedFragmentView.setOperandHeight(operand, operandHeight);

                                currentFragmentY += operandHeight;
                            }

                            combinedFragmentView.updateLayout();
                            receiveEventNext = false;
                        }
                    } else if (!receiveEventNext) {
                        currentFragmentY += fragmentView.getHeight();
                    }
                }
            }
        }

        return currentFragmentY - previousFragmentY;
    }

    /**
     * Layouts all combined fragments of this view.
     * The width is determined by the position of the covered lifelines.
     */
    private void layoutCombinedFragments() {
        for (Entry<CombinedFragment, CombinedFragmentView> entry : combinedFragments.entrySet()) {
            CombinedFragmentView combinedFragmentView = entry.getValue();
            Set<InteractionFragment> visualizedFragments =
                    compositeSpecification.getVisualizedFragments(entry.getKey());

            float minX = Float.MAX_VALUE;
            float maxX = -1;

            for (Lifeline lifeline : compositeSpecification.getCoveredLifelines(entry.getKey())) {
                LifelineView lifelineView = lifelines.get(lifeline.getRepresents());

                // The life line view can be null if it was not yet added
                if (lifelineView == null) {
                    continue;
                }

                Vector3D position = lifelineView.getPosition(TransformSpace.GLOBAL);
                minX = Math.min(minX, position.getX() - LIFELINE_SPACING);
                maxX = Math.max(maxX, position.getX() + lifelineView.getWidth() + LIFELINE_SPACING);

                /**
                 * Take into consideration the size of other fragments as well,
                 * since they could be quite wide.
                 */
                Interaction initial = compositeSpecification.getInitialInteraction();
                for (InteractionFragment fragment : compositeSpecification.getVisualizedFragments(initial,
                        lifelineView.getRepresentedLifelines(), true)) {
                    /**
                     * Check whether the fragment is part of this combined fragment
                     * in its containment hierarchy.
                     */
                    if (entry.getKey() != fragment && visualizedFragments.contains(fragment)) {
                        RamRectangleComponent fragmentView = fragments.get(fragment);
                        if (fragmentView != null) {
                            Vector3D fragmentPosition = fragmentView.getPosition(TransformSpace.GLOBAL);
                            
                            // Also ensure that nested combined fragments are also visually spaced. 
                            maxX = Math.max(maxX, fragmentPosition.getX() + fragmentView.getWidth() + LIFELINE_SPACING);
                            minX = Math.min(minX, fragmentPosition.getX() - LIFELINE_SPACING);
                        }
                    }
                }
            }

            Vector3D position = combinedFragmentView.getPosition(TransformSpace.GLOBAL);
            position.setX(minX);
            combinedFragmentView.setPositionGlobal(position);
            combinedFragmentView.setMinimumWidth(maxX - minX);
        }
    }

    @Override
    public void notifyChanged(Notification notification) {
        // Note: If the notifications are invoked later certain problems are encountered:
        // 1. moving self messages does not work (see commits of #536).
        // 2. fragments are not cached which causes problems when removing them 
        EObject notifier = (EObject) notification.getNotifier();
        Object feature = notification.getFeature();

        Interaction interaction = EMFModelUtil.getRootContainerOfType(notifier, RamPackage.Literals.INTERACTION);

        // Notification could come from the interaction or an operand.
        if (notifier == compositeSpecification.getInitialInteraction()
                || compositeSpecification.containsInteraction(notifier)
                || compositeSpecification.containsInteraction(interaction)) {
            if (feature == RamPackage.Literals.FRAGMENT_CONTAINER__FRAGMENTS) {
                if (notification.getEventType() == Notification.ADD) {
                    EObject newValue = (EObject) notification.getNewValue();

                    // Ensure the fragment hasn't been already added (multiple notifications possible).
                    if (!fragments.containsKey(newValue)) {
                        addFragmentSwitcher.doSwitch(newValue);
                    }
                } else if (notification.getEventType() == Notification.ADD_MANY) {
                    if (notification.getNewValue() instanceof Collection<?>) {
                        @SuppressWarnings("unchecked")
                        Collection<InteractionFragment> newValues =
                                (Collection<InteractionFragment>) notification.getNewValue();

                        for (InteractionFragment fragment : newValues) {
                            // Ensure the fragment hasn't been already added (multiple notifications possible).
                            if (!fragments.containsKey(fragment)) {
                                addFragmentSwitcher.doSwitch(fragment);
                            }
                        }
                    }
                } else if (notification.getEventType() == Notification.MOVE) {
                    InteractionFragment fragment = (InteractionFragment) notification.getNewValue();

                    if (fragment instanceof MessageOccurrenceSpecification) {
                        collapse(((MessageOccurrenceSpecification) fragment).getMessage());
                    }
                    
                    removeFragmentSwitcher.doSwitch(fragment);
                    addFragmentSwitcher.doSwitch(fragment);
                } else if (notification.getEventType() == Notification.REMOVE) {
                    EObject oldValue = (EObject) notification.getOldValue();

                    // Ensure the fragment hasn't been removed already (multiple notifications possible).
                    if (fragments.containsKey(oldValue)) {
                        removeFragmentSwitcher.doSwitch(oldValue);
                    }
                }
            } else if (feature == RamPackage.Literals.INTERACTION__LIFELINES) {
                switch (notification.getEventType()) {
                    case Notification.ADD:
                        Lifeline lifeline = (Lifeline) notification.getNewValue();
                        if (!lifelines.containsKey(lifeline.getRepresents())) {
                            /*
                             * It is possible to receive notifications from a life line not contained in the
                             * initial interaction (e.g. if a life line is created in the original interaction of an
                             * expanded message. In this case, add a new layout element.
                             */
                            if (compositeSpecification.getInitialInteraction().getLifelines().contains(lifeline)) {
                                addLifelineView(lifeline, layout.getValue().get(lifeline));
                            } else {
                                addLifelineView(lifeline, RamFactory.eINSTANCE.createLayoutElement());
                            }
                        }
                        break;
                    case Notification.REMOVE:
                        lifeline = (Lifeline) notification.getOldValue();
                        if (lifelines.containsKey(lifeline.getRepresents())) {
                            removeLifelineView(lifeline);
                        }
                        break;
                }
            } else if (feature == RamPackage.Literals.LIFELINE__REPRESENTS) {
                TypedElement oldValue = (TypedElement) notification.getOldValue();
                TypedElement newValue = (TypedElement) notification.getNewValue();

                if (lifelines.containsKey(oldValue)) {
                    LifelineView lifelineView = lifelines.remove(oldValue);
                    lifelineView.setRepresents(newValue);
                    lifelines.put(newValue, lifelineView);
                }

            } else if (feature == RamPackage.Literals.INTERACTION__MESSAGES) {
                switch (notification.getEventType()) {
                    case Notification.ADD:
                        Message message = (Message) notification.getNewValue();
                        if (!messages.containsKey(message)) {
                            addMessageView(message);
                        }
                        break;
                    case Notification.REMOVE:
                        message = (Message) notification.getOldValue();
                        if (messages.containsKey(message)) {
                            removeMessageView(message);
                        }
                        break;
                }
            }
        } else if (feature == RamPackage.Literals.ASPECT__MESSAGE_VIEWS) {
            switch (notification.getEventType()) {
                case Notification.REMOVE:
                    if (notification.getOldValue() instanceof MessageView) {
                        MessageView affectedMessageView = (MessageView) notification.getOldValue();
                        Interaction removedInteraction = affectedMessageView.getSpecification();
                        collapse(compositeSpecification.getStartMessage(removedInteraction));
                    }
                    break;
            }
        } else if (notifier == layout) {
            if (feature == RamPackage.Literals.CONTAINER_MAP__VALUE) {
                if (notification.getEventType() == Notification.ADD) {
                    ElementMapImpl elementMap = (ElementMapImpl) notification.getNewValue();
                    Lifeline lifeline = (Lifeline) elementMap.getKey();
                    if (lifelines.containsKey(lifeline.getRepresents())) {
                        LifelineView lifelineView = lifelines.get(lifeline.getRepresents());
                        lifelineView.setLayoutElement(elementMap.getValue());
                    }
                }
            }
        }
    }

    /**
     * Expands a message.
     * 
     * @param message The message to expand
     */
    public void expand(Message message) {
        if (compositeSpecification.isExpanded(message)) {
            return;
        }

        building = true;

        // Collapse all the messages that have the same signature
        if (!compositeSpecification.isRecursiveCall(message)) {
            for (Message curMessage : compositeSpecification.getAllMessages()) {
                if (curMessage.getSignature() == message.getSignature()) {
                    collapse(curMessage);
                }
            }
        }

        Interaction interaction = compositeSpecification.expandMessage(message);
        EMFEditUtil.addListenerFor(interaction, this);

        // Parameter mappings
        Map<Parameter, ParameterValueMapping> parameterMap = compositeSpecification.getParameterMappings();
        for (ParameterValueMapping argument : message.getArguments()) {
            TypedElement typedElement = extractTypedElement(argument.getValue());

            if (typedElement instanceof Parameter && parameterMap.containsKey(typedElement)) {
                typedElement = extractTypedElement(parameterMap.get(typedElement).getValue());
            }

            if (lifelines.containsKey(typedElement)) {
                LifelineView view = lifelines.get(typedElement);
                lifelines.put(argument.getParameter(), view);
            }
        }

        // Prepare the covered life lines
        MessageOccurrenceSpecification startFragment = (MessageOccurrenceSpecification) message.getReceiveEvent();
        LifelineView lifelineView = lifelines.get(startFragment.getCovered().get(0).getRepresents());
        lifelineView.setupExpand(startFragment, interaction);

        // Add the mapping between the life line view and the target life line
        Lifeline target = interaction.getFragments().get(0).getCovered().get(0);
        lifelines.put(target.getRepresents(), lifelineView);

        for (Lifeline lifeline : compositeSpecification.getLifelines()) {
            if (!lifelines.containsKey(lifeline.getRepresents())) {
                /*
                 * Add a life line view with a new layout element. Creating a new layout element is
                 * always needed here, since this part of the code is only reached for the expanded
                 * life line (i.e. the life lines that are not part of the initial interaction).
                 */
                addLifelineView(lifeline, RamFactory.eINSTANCE.createLayoutElement());
            } else {
                LifelineView view = lifelines.get(lifeline.getRepresents());
                view.addRepresentingLifeline(lifeline);
            }
        }

        // If the message is expanded in an operand, setup all the life lines
        FragmentContainer actualContainer = compositeSpecification.getActualRootContainer(startFragment);
        if (actualContainer instanceof InteractionOperand) {
            CombinedFragment combinedFragment = (CombinedFragment) actualContainer.eContainer();
            combinedFragments.get(combinedFragment).setUp();
        }

        for (InteractionFragment fragment : compositeSpecification.getVisualizedFragments(interaction)) {
            addFragmentSwitcher.doSwitch(fragment);
        }

        for (Message curMessage : compositeSpecification.getMessages()) {
            if (!messages.containsKey(curMessage)) {
                addMessageView(curMessage);
            }
        }

        building = false;
        layoutMessageView();
    }

    /**
     * Returns the typed element of a given value specification.
     * 
     * @param value the value specification
     * @return the typed element
     */
    private static TypedElement extractTypedElement(ValueSpecification value) {
        if (value instanceof StructuralFeatureValue) {
            return ((StructuralFeatureValue) value).getValue();
        } else if (value instanceof ParameterValue) {
            return ((ParameterValue) value).getParameter();
        }

        return null;
    }

    /**
     * Collapses a message.
     * 
     * @param message The message
     */
    public void collapse(Message message) {
        if (!compositeSpecification.isExpanded(message)) {
            return;
        }

        building = true;

        MessageOccurrenceSpecification startFragment = (MessageOccurrenceSpecification) message.getReceiveEvent();

        // Collapse the fragments
        for (InteractionFragment fragment : compositeSpecification.getVisualizedFragments(message)) {
            if (fragments.containsKey(fragment)) {
                collapseFragment(fragment, true);
            }
        }

        /*
         * Get the list of all unused life lines and interactions.
         * The unused elements are found by doing the logical difference between the old set of
         * elements (i.e. before collapsing) and the set of elements after collapsing.
         * Although this is potentially an expensive operation, it is necessary, since one cannot be
         * entirely sure that an element (e.g. a life line) is not used without taking in account all
         * the elements in the message view (for instance, another message could use this life line,
         * and therefore, this life line must not be removed).
         */
        Set<Lifeline> unusedLifelines = new HashSet<>(compositeSpecification.getLifelines());
        Interaction interaction = compositeSpecification.collapseMessage(message);
        unusedLifelines.removeAll(compositeSpecification.getLifelines());

        // Remove the unused life lines
        for (Lifeline lifeline : unusedLifelines) {
            if (lifelines.containsKey(lifeline.getRepresents())) {
                removeLifelineView(lifeline);
            }
        }

        // If the message was expanded in a combined fragment, clean up the expanded life lines
        // (i.e. remove the place holder and the operand views)
        FragmentContainer actualContainer = compositeSpecification.getActualRootContainer(startFragment);
        if (actualContainer instanceof InteractionOperand) {
            CombinedFragment combinedFragment = (CombinedFragment) actualContainer.eContainer();

            if (combinedFragment != null) {
                combinedFragments.get(combinedFragment).cleanUp();
            }
        }

        // Unregister from the unused interaction
        EMFEditUtil.removeListenerFor(interaction, this);

        // Clean up the life line
        LifelineView lifelineView = lifelines.get(startFragment.getCovered().get(0).getRepresents());
        lifelineView.cleanUpCollapse(startFragment, interaction);

        // Remove the mapping between the life line view and the target life line
        Lifeline target = interaction.getFragments().get(0).getCovered().get(0);
        lifelines.remove(target.getRepresents(), lifelineView);

        building = false;
        layoutMessageView();
    }

    /**
     * Collapses the given fragment.
     * 
     * @param fragment the fragment
     * @param removeFragments indicates whether the fragments must be removed.
     *            In general, this must be <code>true</code> (the fragments are removed).
     */
    private void collapseFragment(InteractionFragment fragment, boolean removeFragments) {
        if (fragment instanceof CombinedFragment) {
            for (InteractionOperand operand : ((CombinedFragment) fragment).getOperands()) {
                /*
                 * Remove the all the message call views in the combined fragment, but keep the send
                 * and receive fragments. These fragments will be removed by the removeCombinedFragment method
                 */
                for (InteractionFragment curFragment : compositeSpecification.getVisualizedFragments(operand)) {
                    collapseFragment(curFragment, false);
                }
            }
        } else if (fragment instanceof MessageOccurrenceSpecification) {
            Message message = ((MessageOccurrenceSpecification) fragment).getMessage();
            if (messages.containsKey(message)) {
                removeMessageView(message);
            }
        }

        if (removeFragments) {
            removeFragmentSwitcher.doSwitch(fragment);
        }
    }

    /**
     * Refreshes the given message by collapsing it an expanding it again.
     * 
     * @param message the message
     */
    public void refresh(Message message) {
        List<Message> expandedMessages = compositeSpecification.getExpandedMessages();

        collapse(message);

        for (Message expanded : expandedMessages) {
            expand(expanded);
        }
    }
}