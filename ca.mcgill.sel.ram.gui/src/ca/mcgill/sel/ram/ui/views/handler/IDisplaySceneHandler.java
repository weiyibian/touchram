package ca.mcgill.sel.ram.ui.views.handler;

import ca.mcgill.sel.ram.ui.scenes.DisplayAspectScene;
import ca.mcgill.sel.ram.ui.scenes.RamAbstractScene;
import ca.mcgill.sel.ram.ui.scenes.handler.IRamAbstractSceneHandler;
import ca.mcgill.sel.ram.ui.views.structural.CompositionView;

/**
 * This interface is implemented by something that can handle events for a {@link DisplayAspectScene}.
 * 
 * @author mschoettle
 */
public interface IDisplaySceneHandler extends IRamAbstractSceneHandler {

    /**
     * Generates artifacts (e.g., code or other models).
     * 
     * @param scene the affected {@link DisplayAspectScene}
     */
    void generate(RamAbstractScene<?> scene);

    /**
     * Handles loading of a new scene.
     * 
     * @param scene
     *            the affected {@link DisplayAspectScene}
     */
    void loadScene(RamAbstractScene<?> scene);

    /**
     * Opens or closes the automatic validation for the {@link DisplayAspectScene}.
     * 
     * @param scene
     *            the affected {@link DisplayAspectScene}
     */
    void showValidation(RamAbstractScene<?> scene);

    /**
     * Opens or closes the tracing for the {@link DisplayAspectScene}.
     * 
     * @param scene
     *            the affected {@link DisplayAspectScene}
     */
    void showTracing(RamAbstractScene<?> scene);


    /**
     * Handles going back from the current view to the previous view.
     * 
     * @param scene
     *            the affected {@link DisplayAspectScene}
     */
    void back(RamAbstractScene<?> scene);

    /**
     * Handles switching into split composition edit mode (the split view where you can do mappings).
     * 
     * @param scene
     *            the affected
     * @param compositionView
     *            the composition view for which we will create an edit mode.
     */
    void switchToCompositionEditMode(RamAbstractScene<?> scene, CompositionView compositionView);

    /**
     * Handles switching to the menu.
     * 
     * @param scene
     *            the affected {@link DisplayAspectScene}
     */
    void switchToMenu(RamAbstractScene<?> scene);

    /**
     * Handles weaving of the complete aspect hierarchy.
     * 
     * @param scene
     *            the affected {@link DisplayAspectScene}
     */
    void weaveAll(RamAbstractScene<?> scene);

    /**
     * Handles weaving of the state machines inside an aspect.
     *
     * @param scene - The affected {@link DisplayAspectScene}
     */
    void weaveStateMachines(RamAbstractScene<?> scene);

    /**
     * Handles weaving of the complete aspect hierarchy Without applying CSP composition to the state machines of the
     * state views.
     *
     * @param scene the affected {@link DisplayAspectScene}
     */
    void weaveAllNoCSPForStateViews(RamAbstractScene<?> scene);

    /**
     * Handles the switching to concern when the user wants to return back
     * to the concern scene he was editing.
     *
     * @param displayAspectScene the affected {@link DisplayAspectScene}
     */
    void switchToConcern(RamAbstractScene<?> displayAspectScene);

    /**
     * Handles the closing of the split view.
     * 
     * @param displayAspectScene - The current scene
     */
    void closeSplitView(RamAbstractScene<?> displayAspectScene);
    
    /**
     * Handles the automatic layout of views.
     * 
     * @param scene the affected {@link DisplayAspectScene}
     */
    void layout(RamAbstractScene<?> scene);
    
}
