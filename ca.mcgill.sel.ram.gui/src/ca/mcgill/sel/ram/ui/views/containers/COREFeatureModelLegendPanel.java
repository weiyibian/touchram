package ca.mcgill.sel.ram.ui.views.containers;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.mt4j.util.MTColor;

import ca.mcgill.sel.ram.ui.components.RamImageComponent;
import ca.mcgill.sel.ram.ui.components.RamListComponent.Namer;
import ca.mcgill.sel.ram.ui.components.RamPanelListComponent;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.layouts.HorizontalLayoutVerticallyCentered;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;

/**
 * Panel container that visualizes the legend of a feature model.
 * 
 * @author mschoettle
 */
public class COREFeatureModelLegendPanel extends RamPanelListComponent<String> {
    
    private static final Map<String, RamImageComponent> LEGEND_ITEMS = new LinkedHashMap<>();
    
    static {
        LEGEND_ITEMS.put("Mandatory", new RamImageComponent(Icons.ICON_FM_LEGEND_MANDATORY, MTColor.WHITE));
        LEGEND_ITEMS.put("Optional", new RamImageComponent(Icons.ICON_FM_LEGEND_OPTIONAL, MTColor.WHITE));
        LEGEND_ITEMS.put("Alternative (OR)", new RamImageComponent(Icons.ICON_FM_LEGEND_ALTERNATIVE, MTColor.WHITE));
        LEGEND_ITEMS.put("Exclusive (XOR)", new RamImageComponent(Icons.ICON_FM_LEGEND_EXCLUSIVE, MTColor.WHITE));
    }

    /**
     * Creates a new panel with the given horizontal and vertical sticks.
     * 
     * @param horizontalStick the horizontalStick for the panel
     * @param verticalStick the verticalStick for the panel
     */
    public COREFeatureModelLegendPanel(HorizontalStick horizontalStick, VerticalStick verticalStick) {
        super(5, Strings.LABEL_CONTAINER_FM_LEGEND, horizontalStick, verticalStick);
        
        final List<String> elements = new ArrayList<>(LEGEND_ITEMS.keySet());
        
        setNamer(new Namer<String>() {

            @Override
            public RamRectangleComponent getDisplayComponent(String element) {                
                RamRectangleComponent component = new RamRectangleComponent(new HorizontalLayoutVerticallyCentered(5f));
                component.setNoStroke(false);
                component.setNoFill(false);
                component.setFillColor(MTColor.WHITE);
                
                component.addChild(LEGEND_ITEMS.get(element));
                component.addChild(new RamTextComponent(element));
                
                return component;
            }

            @Override
            public String getSortingName(String element) {                
                return String.valueOf(elements.indexOf(element));
            }

            @Override
            public String getSearchingName(String element) {
                return getSortingName(element);
            }
        });
        
        setElements(elements);
        showContent(false);
    }

}
