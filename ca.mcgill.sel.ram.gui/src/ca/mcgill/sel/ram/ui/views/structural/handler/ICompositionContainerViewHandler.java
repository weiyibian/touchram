package ca.mcgill.sel.ram.ui.views.structural.handler;

import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREModelComposition;

/**
 * This interface can be implemented by a handler that can handle events for a
 * {@link ca.mcgill.sel.ram.ui.views.structural.CompositionContainerView} which is showing the list of
 * model compositions in a model.
 * 
 * @author eyildirim
 * @author joerg
 */
public interface ICompositionContainerViewHandler {

    /**
     * Deletes a model composition from its model.
     * 
     * @param modelComposition the {@link COREModelComposition} to delete
     */
    void deleteModelComposition(COREModelComposition modelComposition);

    /**
     * Loads the aspect browser.
     * 
     * @param artefact the current artefact
     */
    void loadBrowser(COREArtefact artefact);
    
    /**
     * Opens the selector to then select a preexisting reuse (and tailor it to your needs).
     * 
     * @param artefact we are in
     */
    void tailorExistingReuse(COREArtefact artefact);
    
}
