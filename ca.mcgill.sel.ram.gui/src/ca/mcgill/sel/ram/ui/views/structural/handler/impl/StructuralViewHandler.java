package ca.mcgill.sel.ram.ui.views.structural.handler.impl;

import java.lang.reflect.Modifier;
import java.lang.reflect.TypeVariable;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.mt4j.input.inputData.InputCursor;
import org.mt4j.input.inputProcessors.MTGestureEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;
import org.mt4j.input.inputProcessors.componentProcessors.unistrokeProcessor.UnistrokeEvent;
import org.mt4j.input.inputProcessors.componentProcessors.unistrokeProcessor.UnistrokeUtils.UnistrokeGesture;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.commons.StringUtil;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.Attribute;
import ca.mcgill.sel.ram.Class;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.ImplementationClass;
import ca.mcgill.sel.ram.Operation;
import ca.mcgill.sel.ram.REnum;
import ca.mcgill.sel.ram.REnumLiteral;
import ca.mcgill.sel.ram.RamFactory;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.StructuralView;
import ca.mcgill.sel.ram.Type;
import ca.mcgill.sel.ram.TypeParameter;
import ca.mcgill.sel.ram.controller.ControllerFactory;
import ca.mcgill.sel.ram.controller.StructuralViewController;
import ca.mcgill.sel.ram.loaders.RamClassLoader;
import ca.mcgill.sel.ram.loaders.RamClassUtils;
import ca.mcgill.sel.ram.loaders.exceptions.MissingJarException;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamPopup.PopupType;
import ca.mcgill.sel.ram.ui.components.RamSelectorComponent;
import ca.mcgill.sel.ram.ui.components.listeners.AbstractDefaultRamSelectorListener;
//import ca.mcgill.sel.core.perspective.EvlStandalone;
import ca.mcgill.sel.ram.ui.utils.GUIConstants;
import ca.mcgill.sel.ram.ui.utils.MathUtils;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.utils.UnistrokeProcessorUtils;
import ca.mcgill.sel.ram.ui.views.AbstractView;
import ca.mcgill.sel.ram.ui.views.OptionSelectorView;
import ca.mcgill.sel.ram.ui.views.handler.impl.AbstractViewHandler;
import ca.mcgill.sel.ram.ui.views.structural.AttributeView;
import ca.mcgill.sel.ram.ui.views.structural.BaseView;
import ca.mcgill.sel.ram.ui.views.structural.ClassView;
import ca.mcgill.sel.ram.ui.views.structural.ClassifierView;
import ca.mcgill.sel.ram.ui.views.structural.EnumLiteralView;
import ca.mcgill.sel.ram.ui.views.structural.EnumView;
import ca.mcgill.sel.ram.ui.views.structural.ImplementationClassSelectorView;
import ca.mcgill.sel.ram.ui.views.structural.OperationView;
import ca.mcgill.sel.ram.ui.views.structural.StructuralDiagramView;
import ca.mcgill.sel.ram.ui.views.structural.handler.IStructuralViewHandler;
import ca.mcgill.sel.ram.util.RAMInterfaceUtil;
import ca.mcgill.sel.ram.util.RAMModelUtil;

/**
 * The default handler for a {@link StructuralDiagramView}.
 *
 * @author mschoettle
 */
public class StructuralViewHandler extends AbstractViewHandler implements IStructuralViewHandler {
    //define a tolerant range of every points in the line gesture
    private static float tolerantRange = 20;
    
    //define a tolerant angle of the fist point and last point
    private static double tolerantAngle = 9;
    /**
     * The options to display when tap-and-hold is performed.
     */
    private enum CreateFeature {
        CREATE_CLASS, CREATE_DATA_TYPE, CREATE_ENUM, IMPORT_CLASS, IMPORT_DATA_TYPE, IMPORT_ENUM,
        IMPORT_IMPLEMENTATION_CLASS 
    }
    
    /**
     * The available options for relationships.
     */
    private enum CreateRelationship {
        ASSOCIATION, INHERITANCE
    }

    /**
     * Handles the creation of a new class. Takes care of opening a keyboard for the name once the class was added to
     * the structural view.
     *
     * @param view the structural view view representing the structural view
     * @param dataType whether the class should be a data type, <code>true</code> if it is
     * @param position the position where the class should be located
     */
    @SuppressWarnings("static-method")
    private void createNewClass(final StructuralDiagramView view, boolean dataType, final Vector3D position) {
        final StructuralView structuralView = view.getStructuralView();

        final Aspect aspect = (Aspect) structuralView.eContainer();

        aspect.eAdapters().add(new EContentAdapter() {

            private Class clazz;
            
            @Override
            public void notifyChanged(final Notification notification) {
                EContentAdapter adapter = this;
                RamApp.getApplication().invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        if (notification.getFeature() == RamPackage.Literals.STRUCTURAL_VIEW__CLASSES) {
                            if (notification.getEventType() == Notification.ADD) {
                                clazz = (Class) notification.getNewValue();
                            }
                        } else if (notification.getFeature() == RamPackage.Literals.CONTAINER_MAP__VALUE) {
                            if (notification.getEventType() == Notification.ADD) {
                                ((ClassView) view.getClassViewOf(clazz)).showKeyboard();
                                ((ClassView) view.getClassViewOf(clazz)).clearNameField();
                                aspect.eAdapters().remove(adapter);
                            }
                        }
                    }
                });
            }
        });

        // Look for the highest index used by any class
        String defaultName = dataType ? Strings.DEFAULT_DATATYPE_NAME : Strings.DEFAULT_CLASS_NAME;
        Set<String> names = new HashSet<String>();

        for (Classifier c : view.getStructuralView().getClasses()) {
            names.add(c.getName());
        }        
        String className = StringUtil.createUniqueName(defaultName, names);

        ControllerFactory.INSTANCE.getStructuralViewController().createNewClass(structuralView,
                className, dataType, position.getX(), position.getY());
        
//        try {
//            EvlStandalone.runEpsilon(aspect);
//        } catch (Exception e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
        
    }

    /**
     * Handles the importing of a class from an extended or reused aspect. Takes care of opening a
     * selector to allow the user to choose the desired class to import.
     *
     * @param view the structural view view representing the structural view
     * @param dataType whether the class should be a data type, <code>true</code> if it is
     * @param position the position where the class should be located
     */
    @SuppressWarnings("static-method")
    private void importClass(final StructuralDiagramView view, boolean dataType, final Vector3D position) {
        final StructuralView structuralView = view.getStructuralView();
        final Aspect aspect = (Aspect) structuralView.eContainer();
                
        List<Classifier> classes = new ArrayList<>();
        
        if (dataType) {
            classes.addAll(RAMInterfaceUtil.getAvailableExternalDatatypes(aspect));
        } else {
            classes.addAll(RAMInterfaceUtil.getAvailableExternalClasses(aspect));
        }
        
        RamSelectorComponent<Classifier> selector = new RamSelectorComponent<Classifier>(classes);
        selector.registerListener(new AbstractDefaultRamSelectorListener<Classifier>() {
            @Override
            public void elementSelected(RamSelectorComponent<Classifier> selector, Classifier element) {
                ControllerFactory.INSTANCE.getStructuralViewController().importClassifier(structuralView,
                        element, position.getX(), position.getY());
                selector.destroy();
            }
        });

        // Add selector to screen + keyboard
        RamApp.getActiveScene().addComponent(selector, position);
    }

    /**
     * Handles the importing of an enum from an extended or reused aspect. Takes care of opening a
     * selector to allow the user to choose the desired enum to import.
     *
     * @param view the structural view view representing the structural view
     * @param position the position where the class should be located
     */
    @SuppressWarnings("static-method")
    private void importEnum(final StructuralDiagramView view, final Vector3D position) {
        final StructuralView structuralView = view.getStructuralView();
        final Aspect aspect = (Aspect) structuralView.eContainer();
                
        List<REnum> enums = new ArrayList<>();
        
        enums.addAll(RAMInterfaceUtil.getAvailableExternalEnums(aspect));
        
        RamSelectorComponent<REnum> selector = new RamSelectorComponent<REnum>(enums);
        selector.registerListener(new AbstractDefaultRamSelectorListener<REnum>() {
            @Override
            public void elementSelected(RamSelectorComponent<REnum> selector, REnum element) {
                ControllerFactory.INSTANCE.getStructuralViewController().importEnum(structuralView,
                        element, position.getX(), position.getY());
                selector.destroy();
            }
        });

        // Add selector to screen + keyboard
        RamApp.getActiveScene().addComponent(selector, position);
    }

   /**
     * Displays a selector with implementation classes that can be created.
     *
     * @param view View where the implementation class selector should be added
     * @param position The position where the selector should be added.
     */
    @SuppressWarnings("static-method")
    private void createNewImplementationClass(final StructuralDiagramView view, final Vector3D position) {
        // Get the structural view
        final StructuralView structuralView = view.getStructuralView();

        // Get all the implementation classes that already exists in the view (only non generic ones)
        List<String> existingImplementationClasses = new ArrayList<String>();
        for (Classifier classifier : view.getStructuralView().getClasses()) {
            if (classifier instanceof ImplementationClass) {
                // If it's not generic add it to list
                if (((ImplementationClass) classifier).getTypeParameters().size() == 0) {
                    existingImplementationClasses.add(((ImplementationClass) classifier).getInstanceClassName());
                }
            }
        }

        // Create an implementation class selector view
        ImplementationClassSelectorView selector = new ImplementationClassSelectorView(existingImplementationClasses);

        selector.registerListener(new AbstractDefaultRamSelectorListener<String>() {
            @Override
            public void elementSelected(RamSelectorComponent<String> selector, String element) {
                handleImplementationClassSelection(structuralView, element, position.getX(), position.getY());
            }
        });

        // Add selector to screen + keyboard
        RamApp.getActiveScene().addComponent(selector, position);
        selector.displayKeyboard();
    }

    /**
     * Handles the selection of an implementation class to import by the user.
     *
     * @param structuralView the {@link StructuralView} the class should be added to
     * @param className the name
     * @param x the x position of the class
     * @param y the y position of the class
     */
    private static void handleImplementationClassSelection(StructuralView structuralView, String className,
            float x, float y) {
        StructuralViewController controller = ControllerFactory.INSTANCE.getStructuralViewController();
        try {
            java.lang.Class<?> loadedClass = RamClassLoader.INSTANCE.retrieveClass(className);
            if (RamClassLoader.INSTANCE.isLoadableEnum(className)) {
                List<String> literals = new ArrayList<String>();
                for (Object literal : loadedClass.getEnumConstants()) {
                    literals.add(literal.toString());
                }
                controller.createImplementationEnum(structuralView, RamClassUtils.extractClassName(className),
                        className, x, y, literals);
            } else {
                List<TypeParameter> typeParameters = new ArrayList<TypeParameter>();
                for (TypeVariable<?> tv : loadedClass.getTypeParameters()) {
                    TypeParameter typeParameter = RamFactory.eINSTANCE.createTypeParameter();
                    typeParameter.setName(tv.getName());
                    typeParameters.add(typeParameter);
                }
                boolean isInterface = RamClassLoader.INSTANCE.isLoadableInterface(className);
                boolean isAbstract = Modifier.isAbstract(loadedClass.getModifiers());
                Set<String> superTypes = RamClassLoader.INSTANCE.getAllSuperClassesFor(className);
                Set<ImplementationClass> subTypes = RamClassLoader.getExistingSubTypes(structuralView, className);
                // If an element is selected create the implementation class with the controller
                controller.createImplementationClass(structuralView, className, typeParameters, isInterface, isAbstract,
                        x, y, superTypes, subTypes);
            }
        } catch (MissingJarException e) {
            RamApp.getActiveScene().displayPopup(Strings.popupClassNotFound(e.getMessage()), PopupType.ERROR);
        }
    }

    /**
     * Create an Enum to be added to the structural view.
     *
     * @param view View where the enum should be added
     * @param position The position where it should be added.
     */
    @SuppressWarnings("static-method")
    private void createNewEnum(final StructuralDiagramView view, Vector3D position) {
        final StructuralView structuralView = view.getStructuralView();

        final Aspect aspect = (Aspect) structuralView.eContainer();

        // Will show keyboard when the enum view appears on screen
        aspect.eAdapters().add(new EContentAdapter() {

            private REnum renum;

            @Override
            public void notifyChanged(final Notification notification) {
                EContentAdapter adapter = this;
                RamApp.getApplication().invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        if (notification.getFeature() == RamPackage.Literals.STRUCTURAL_VIEW__TYPES) {
                            if (notification.getEventType() == Notification.ADD) {
                                renum = (REnum) notification.getNewValue();
                            }
                        } else if (notification.getFeature() == RamPackage.Literals.CONTAINER_MAP__VALUE) {
                            if (notification.getEventType() == Notification.ADD) {
                                view.getEnumView(renum).showKeyboard();
                                view.getEnumView(renum).clearNameField();
                                aspect.eAdapters().remove(adapter);
                            }
                        }
                    }
                });
            }
        });

        // Find a unique name for the enum
        Set<String> names = new HashSet<String>();
        for (Type type : view.getStructuralView().getTypes()) {
            if (type instanceof REnum) {
                names.add(((REnum) type).getName());
            }
        }        
        String enumName = StringUtil.createUniqueName(Strings.DEFAULT_ENUM_NAME, names);
        
        // Create the REnum using the structural view controller
        ControllerFactory.INSTANCE.getStructuralViewController().createEnum(structuralView,
                enumName, position.getX(), position.getY());

    }

    /**
     * This will check to see if the delete(x) gesture is done over a class. If yes it will delete the class.
     * Determining if the gesture was done over a class is by comparing the x and y values of the start ] and end
     * position of the mouse cursor with the position of the class.
     *
     * @param structuralView this represent the structural view in which the gesture was performed on
     * @param startPosition The position of the cursor when the gesture was started
     * @param endPosition The position of the cursor when the gesture was ended
     * @param inputCursor the input cursor of the event
     */
    @SuppressWarnings("static-method")
    private void deleteClass(StructuralDiagramView structuralView, Vector3D startPosition, Vector3D endPosition,
            InputCursor inputCursor) {

        Vector3D intersection = UnistrokeProcessorUtils.getIntersectionPoint(startPosition, endPosition, inputCursor);

        float intersectionX = intersection.x;
        float intersectionY = intersection.y;

        // find the class(es) that are underneath the intersection
        for (BaseView<?> baseView : structuralView.getBaseViews()) {
            float left = baseView.getX();
            float upper = baseView.getY();
            float right = left + baseView.getGlobalWidth();
            float lower = upper + baseView.getGlobalHeight();

            if (left < intersectionX && right > intersectionX && upper < intersectionY && lower > intersectionY) {
                ControllerFactory.INSTANCE.getStructuralViewController().removeClassifier(baseView.getClassifier());
                break;
            }
        }

    }

    @Override
    public void dragAllSelectedClasses(StructuralDiagramView svd, Vector3D directionVector) {
        for (BaseView<?> baseView : svd.getSelectedElements()) {
            // create a copy of the translation vector, because translateGlobal modifies it
            baseView.translateGlobal(new Vector3D(directionVector));
        }
    }

    @Override
    public boolean handleDoubleTapOnClass(StructuralDiagramView structuralView, BaseView<?> targetBaseView) {
        return false;
    }

    @Override
    public boolean handleTapAndHoldOnClass(StructuralDiagramView structuralView, BaseView<?> targetClassifierView) {
        return false;
    }

    @Override
    public boolean processTapAndHoldEvent(final TapAndHoldEvent tapAndHoldEvent) {
        if (tapAndHoldEvent.isHoldComplete()) {
            final StructuralDiagramView target = (StructuralDiagramView) tapAndHoldEvent.getTarget();
            OptionSelectorView<CreateFeature> selector = new OptionSelectorView<CreateFeature>(CreateFeature.values());

            final Vector3D position = tapAndHoldEvent.getLocationOnScreen();
            RamApp.getActiveScene().addComponent(selector, position);

            selector.registerListener(new AbstractDefaultRamSelectorListener<CreateFeature>() {

                @Override
                public void elementSelected(RamSelectorComponent<CreateFeature> selector, CreateFeature element) {
                    switch (element) {
                        case CREATE_CLASS:
                            createNewClass(target, false, position);
                            break;
                        case CREATE_DATA_TYPE:
                            createNewClass(target, true, position);
                            break;
                        case CREATE_ENUM:
                            createNewEnum(target, position);
                            break;
                        case IMPORT_CLASS:
                            importClass(target, false, position);
                            break;
                        case IMPORT_DATA_TYPE:
                            importClass(target, true, position);
                            break;
                        case IMPORT_ENUM:
                            importEnum(target, position);
                            break;
                        case IMPORT_IMPLEMENTATION_CLASS:
                            createNewImplementationClass(target, position);
                            break;
                    }
                }
            });
        }
        return true;
    }

    @Override
    public boolean processTapEvent(TapEvent tapEvent) {
        if (tapEvent.isTapped()) {

            StructuralDiagramView target = (StructuralDiagramView) tapEvent.getTarget();

            target.deselect();
        }

        return true;
    }

    @Override
    public void handleUnistrokeGesture(AbstractView<?> target, UnistrokeGesture gesture, Vector3D startPosition,
            UnistrokeEvent event) {
        StructuralDiagramView structuralDiagramView = (StructuralDiagramView) target;

        switch (event.getId()) {
            case MTGestureEvent.GESTURE_ENDED:
                Vector3D startGesturePoint = event.getCursor().getStartPosition();
                Vector3D endGesturePoint = event.getCursor().getPosition();
                
                //check horizontal
                //check whether is a line
                boolean isLine = UnistrokeProcessorUtils
                        .isHorizontalLine(event.getCursor(), tolerantRange, tolerantAngle);
                boolean isLongEnough = UnistrokeProcessorUtils
                        .isLongEnough(structuralDiagramView, startGesturePoint.getX(), endGesturePoint.getX());
                if (isLine && isLongEnough) {
                    Vector3D midGesturePoint = startGesturePoint.getAdded(endGesturePoint).scaleLocal((float) 0.5);
                            
                    //check if attribute or operation selected in class
                    for (ClassifierView<?> view : structuralDiagramView.getClassifierViews()) {
                                
                        AttributeView attributeView = 
                                (AttributeView) view.getAttributesContainer().getChildByPoint(midGesturePoint);
                        OperationView operationView = 
                                (OperationView) view.getOperationsContainer().getChildByPoint(midGesturePoint);
                                
                        if (attributeView == null && operationView != null) {
                            Operation operation = operationView.getOperation();
                            ControllerFactory.INSTANCE.getClassController().removeOperation(operation);
                        } else if (attributeView != null && operationView == null) {
                            Attribute attribute = attributeView.getAttribute();
                            ControllerFactory.INSTANCE.getClassController().removeAttribute(attribute);
                        }
                    }
                            
                    //check if literal selected in enum
                    for (EnumView view : structuralDiagramView.getEnumViews()) {
                        EnumLiteralView literalView = 
                                (EnumLiteralView) view.getEnumLiteralsContainer().getChildByPoint(midGesturePoint);
                        if (literalView != null) {
                            REnumLiteral literal = literalView.getLiteral();
                            ControllerFactory.INSTANCE.getEnumController().removeLiteral(literal);
                        } 
                    }
                    break;
                } 
           
                ClassifierView<?> startClass = null;
                ClassifierView<?> endClass = null;

                for (ClassifierView<?> view : structuralDiagramView.getClassifierViews()) {
                    if (MathUtils.pointIsInRectangle(startGesturePoint, view, GUIConstants.MARGIN_ELEMENT_DETECTION)) {
                        startClass = view;
                    }
                    if (view.containsPointGlobal(endGesturePoint)) {
                        endClass = view;
                    }
                    
                    if (startClass != null & endClass != null) {
                        handleRelationshipCreation(structuralDiagramView, startClass, endClass, endGesturePoint);
                        return;
                    }
                }
                break;
        }

        switch (gesture) {
            case RECTANGLE:
                createNewClass(structuralDiagramView, false, startPosition);
                break;
            case X:
                Vector3D endPosition = event.getCursor().getPosition();
                deleteClass(structuralDiagramView, startPosition, endPosition, event.getCursor());
                break;
            case CIRCLE:
                createNewEnum(structuralDiagramView, startPosition);
                break;
            case TRIANGLE:
                createNewImplementationClass(structuralDiagramView, startPosition);
                break;
        }
    }

    /**
     * Handles creation of relationships for the given start (from) and end class view (to).
     * 
     * @param structuralDiagramView the {@link StructuralDiagramView} this gesture was performed in
     * @param startClassView the {@link ClassifierView} from which the gesture was performed
     * @param endClassView the {@link ClassifierView} to which the gesture was performed
     * @param endGesturePoint the position gesture finished at
     */
    private static void handleRelationshipCreation(StructuralDiagramView structuralDiagramView,
            ClassifierView<?> startClassView, ClassifierView<?> endClassView,
            Vector3D endGesturePoint) {
        StructuralView structuralView = structuralDiagramView.getStructuralView();
        Classifier fromClass = startClassView.getClassifier();
        Classifier toClass = endClassView.getClassifier();
        
        EnumSet<CreateRelationship> availableOptions = EnumSet.allOf(CreateRelationship.class);
        
        if (!RAMModelUtil.canCreateAssociation(fromClass, toClass)) {
            availableOptions.remove(CreateRelationship.ASSOCIATION);
        }
        if (!RAMModelUtil.canCreateInheritance(fromClass, toClass)) {
            availableOptions.remove(CreateRelationship.INHERITANCE);
        }
        
        if (!availableOptions.isEmpty()) {
            OptionSelectorView<CreateRelationship> selector = 
                    new OptionSelectorView<CreateRelationship>(availableOptions.toArray(new CreateRelationship[] { }));
            
            RamApp.getActiveScene().addComponent(selector, endGesturePoint);

            selector.registerListener(new AbstractDefaultRamSelectorListener<CreateRelationship>() {

                @Override
                public void elementSelected(RamSelectorComponent<CreateRelationship> selector,
                        CreateRelationship element) {
                    switch (element) {
                        case ASSOCIATION:
                            ControllerFactory.INSTANCE.getStructuralViewController().createAssociation(
                                    structuralView, fromClass, toClass, false);
                            break;
                        case INHERITANCE:
                            ControllerFactory.INSTANCE.getClassController().addSuperType((Class) fromClass, toClass);
                            break;
                    }
                }
            });
        }
    }

}
