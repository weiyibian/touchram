package ca.mcgill.sel.ram.ui.views.containers;

import java.math.RoundingMode;
import java.util.List;

import org.mt4j.sceneManagement.transition.SlideTransition;

import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREImpactNode;
import ca.mcgill.sel.core.evaluator.im.PropagationResult;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamListComponent;
import ca.mcgill.sel.ram.ui.scenes.AbstractConcernScene;
import ca.mcgill.sel.ram.ui.utils.MathUtils;

/**
 * Container class used to contain all the impact model elements in the Concern. It also display the weight of each goal
 * according to the selection of the user.
 *
 * @author Nishanth
 * @author Romain
 *
 */
public class COREImpactConcernSelectContainer extends COREImpactContainer {

    private AbstractConcernScene<?, ?> currentScene;
    private PropagationResult propagationResult;

    /**
     * A namer that is capable of displaying a hierarchy of {@link COREImpactNode}. It will not use a
     * text view to display the titles.
     */
    private class InternalNamer extends InternalImpactModelElementNamer {
        /**
         * Build a namer that is capable of displaying a hierarchy of {@link COREImpactNode}. It will not use a
         * text view to display the titles.
         */
        InternalNamer() {
            super(false, false);
        }
    }

    /**
     * The container takes in the default path of the container. It displays all the aspects in the folder.
     *
     * @param concern - The concern which contains the Impact Models.
     * @param scene - The scene of the current Concern.
     * @param propagationResult The result of the evaluation. It associates impact model elements with their weight
     */
    public COREImpactConcernSelectContainer(COREConcern concern, AbstractConcernScene<?, ?> scene,
            PropagationResult propagationResult) {
        super(concern);

        currentScene = scene;

        setNamer(new InternalNamer());

        setPropagationResult(propagationResult);
    }

    /**
     * Build the goals list with the given {@link PropagationResult}.
     *
     * @param propagationResult - the {@link PropagationResult} to use.
     */
    public void setPropagationResult(PropagationResult propagationResult) {
        List<COREImpactNode> superGoals = this.getSuperGoals();

        this.propagationResult = propagationResult;

        if (propagationResult.getEvaluations().size() == 0) {
            for (COREImpactNode impactModelElement : superGoals) {
                this.propagationResult.getEvaluations().put(impactModelElement, 0.00f);
            }
        }

        setElements(superGoals);
    }

    /**
     * Retrieve the name to display in the {@link ca.mcgill.sel.ram.ui.components.RamTextComponent} for this element. By
     * default, it return just the name of the element. But the child of this class can override this operation and
     * return something else.
     *
     * @param element the element we want to display
     * @return the name for this element. By default, we call the operation getName() on the element.
     */
    @Override
    protected String getNameByElement(COREImpactNode element) {
        Float value = propagationResult.getEvaluations().get(element);
        value = value == null ? 0.00f : MathUtils.round(value, 2, RoundingMode.HALF_UP);
        return element.getName() + ": " + value;
    }

    /**
     * Show the impact model in select mode with this element as the main goal of the view.
     */
    @Override
    protected void goalClicked(RamListComponent<COREImpactNode> list, COREImpactNode element) {
        currentScene.setTransition(new SlideTransition(RamApp.getApplication(), 700, true));
        currentScene.getApplication().showImpactModelSelectMode(currentScene.getConcern(), element, propagationResult);
    }

    @Override
    protected void goalDoubleClicked(RamListComponent<COREImpactNode> list, COREImpactNode element) {

    }

}