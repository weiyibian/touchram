package ca.mcgill.sel.ram.ui.views.structural;


import ca.mcgill.sel.ram.Class;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.LayoutElement;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent.Alignment;
import ca.mcgill.sel.ram.ui.utils.Fonts;
import ca.mcgill.sel.ram.ui.utils.Strings;


/**
 * This view draws the RAM representation of a data type onto a {@link StructuralDiagramView}. 
 * It contains a name field, and two lists for methods and attributes. 
 * ClassViews are inspectable and create a {@link ClassInspectorView} when click on.
 *
 * @author Alec Harmon
 */
public class DataTypeView extends ClassView {

    /**
     * Creates a new view representing the given class.
     *
     * @param structuralDiagram the {@link StructuralDiagramView} that owns this view
     * @param clazz The RAM class to display.
     * @param layoutElement The position at which to display it.
     */
    public DataTypeView(StructuralDiagramView structuralDiagram, Class clazz, LayoutElement layoutElement) {
        super(structuralDiagram, clazz, layoutElement);

        addDataTypeTag();
    }
    
    /**
     * Adds the DataType tag.
     */
    protected void addDataTypeTag() {
        // Create the Text field
        RamTextComponent dataTypeTagField = new RamTextComponent();
        dataTypeTagField.setFont(Fonts.FONT_ENUM_TAG);
        dataTypeTagField.setAlignment(Alignment.CENTER_ALIGN);
        dataTypeTagField.setText(Strings.TAG_DATATYPE);
        dataTypeTagField.setNoStroke(true);
        dataTypeTagField.setBufferSize(Cardinal.SOUTH, 0);
        setTagField(dataTypeTagField);
    }
    
    @Override
    protected void buildNameField(Classifier classifier) {
        super.buildNameField(classifier);
        
        nameField.setPlaceholderText(Strings.PH_ENTER_DATATYPE_NAME);
    }
   
}
