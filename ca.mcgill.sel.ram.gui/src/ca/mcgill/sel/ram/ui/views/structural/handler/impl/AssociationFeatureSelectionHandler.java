package ca.mcgill.sel.ram.ui.views.structural.handler.impl;


import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.eclipse.emf.common.command.AbortExecutionException;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.core.util.COREModelUtil;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.AssociationEnd;
import ca.mcgill.sel.ram.controller.AssociationController;
import ca.mcgill.sel.ram.controller.ControllerFactory;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamPopup.PopupType;
import ca.mcgill.sel.ram.ui.scenes.DisplayConcernSelectScene;
import ca.mcgill.sel.ram.ui.scenes.handler.impl.ConcernSelectSceneHandler;
import ca.mcgill.sel.ram.ui.utils.SelectionsSingleton;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.impl.TextViewHandler;
import ca.mcgill.sel.ram.util.Constants;

/**
 * Handler for the feature selection of an association. Handles the display of the association feature model and setting
 * of the feature selection to the model.
 *
 * @author cbensoussan
 */
public class AssociationFeatureSelectionHandler extends TextViewHandler {

    /**
     * List of features from the association concern that should not be displayed to the user.
     */
    private static final List<String> FEATURES_TO_HIDE = Arrays.asList(
            Constants.BIDIRECTIONAL_FEATURE_NAME,
            Constants.MAXIMUM_FEATURE_NAME,
            Constants.MINIMUM_FEATURE_NAME,
            Constants.FEATURE_NAME_ONE);

    // CHECKSTYLE:IGNORE ReturnCount: Not sure how :/
    @Override
    public boolean processTapEvent(TapEvent tapEvent) {
        if (tapEvent.isDoubleTap()) {
            final TextView target = (TextView) tapEvent.getTarget();
            final AssociationEnd associationEnd = (AssociationEnd) target.getData();
            final Aspect aspect = RamApp.getActiveAspectScene().getAspect();
            COREExternalArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(aspect);
            final COREConcern concern = artefact.getCoreConcern();

            COREConcern associationConcern = null;
            final CompoundCommand command;

            try {
                associationConcern = COREModelUtil.getLocalConcern(Constants.ASSOCIATION_CONCERN_LOCATION,
                        concern.eResource().getURI());

                if (associationConcern == null) {
                    RamApp.getActiveScene().displayPopup(Strings.POPUP_ERROR_SELF_REUSE);
                    return true;
                }
                
                // Some features of the Association concern are hidden to the user because they are selected
                // automatically based on the multiplicity and navigability of the association.
                command = hideFeaturesCommand(associationConcern);
            } catch (IOException e) {
                RamApp.getActiveScene().displayPopup(Strings.POPUP_ERROR_CONCERN_NOT_FOUND, PopupType.ERROR);
                return true;
            } catch (AbortExecutionException e) {
                RamApp.getActiveScene().displayPopup("An error occurred while displaying the concern.");
                return true;
            }

            SelectionsSingleton.getInstance().clearAll();
            if (associationEnd.getFeatureSelection() != null) {
                SelectionsSingleton.getInstance()
                        .setBaseConfig(associationEnd.getFeatureSelection().getConfiguration());
            }

            RamApp.getApplication().displayFeatureModelSelectScene(associationConcern, new ConcernSelectSceneHandler() {
                @Override
                public void reuse(DisplayConcernSelectScene scene) {
                    scene.displayPopup(Strings.POPUP_REUSING);
                    AssociationController associationController = ControllerFactory.INSTANCE
                            .getAssociationController();

                    // Restore the hidden features.
                    command.undo();

                    associationController.changeFeatureSelection(aspect, associationEnd,
                            SelectionsSingleton.getInstance().getSelectedConfiguration());

                    switchToPreviousSceneWithoutUndo(scene);
                    SelectionsSingleton.getInstance().clearAll();
                }

                @Override
                public void switchToPreviousScene(DisplayConcernSelectScene scene) {
                    super.switchToPreviousScene(scene);

                    // Restore the hidden features.
                    command.undo();
                }

                public void switchToPreviousSceneWithoutUndo(DisplayConcernSelectScene scene) {
                    super.switchToPreviousScene(scene);
                }
            }, artefact, null);
        }
        return true;
    }

    /**
     * Remove features of the Association Concern that should be hidden from users.
     * Hidden features are One, Bidirectional, Maximum and Minimum
     * @param concern The concern to remove features from
     *
     * @return The {@link CompoundCommand}
     */
    private static CompoundCommand hideFeaturesCommand(COREConcern concern) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(concern);
        CompoundCommand compoundCommand = new CompoundCommand();

        for (COREFeature feature : concern.getFeatureModel().getFeatures()) {
            if (FEATURES_TO_HIDE.contains(feature.getName())) {
                compoundCommand.append(RemoveCommand.create(editingDomain, feature.getParent(),
                        CorePackage.Literals.CORE_FEATURE__CHILDREN, feature));
            }
        }

        if (!compoundCommand.canExecute()) {
            throw new AbortExecutionException(Strings.commandNotExecutable(compoundCommand));
        }

        editingDomain.getCommandStack().execute(compoundCommand);
        return compoundCommand;
    }
}
