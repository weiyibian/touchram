package ca.mcgill.sel.ram.ui.views.structural.handler;

import ca.mcgill.sel.ram.AttributeMapping;
import ca.mcgill.sel.ram.ClassifierMapping;
import ca.mcgill.sel.ram.EnumLiteralMapping;
import ca.mcgill.sel.ram.EnumMapping;
import ca.mcgill.sel.ram.OperationMapping;
import ca.mcgill.sel.ram.ParameterMapping;

/**
 * This interface is implemented by handlers that handle events related to mappings.
 * 
 * @author eyildirim
 */
public interface IMappingContainerViewHandler {
    
    /**
     * Adds a new {@link AttributeMapping}.
     * 
     * @param classifierMapping the current classifier mapping
     */
    void addAttributeMapping(ClassifierMapping classifierMapping);
    
    /**
     * Adds a new {@link EnumLiteralMapping}.
     * 
     * @param enumMapping the current enum mapping
     */
    void addEnumLiteralMapping(EnumMapping enumMapping);
    
    /**
     * Adds a new {@link OperationMapping}.
     * 
     * @param classifierMapping the current classifier mapping
     */
    void addOperationMapping(ClassifierMapping classifierMapping);
    
    /**
     * Adds a new {@link ParameterMapping}.
     * 
     * @param operationMapping the current operation mapping
     */
    void addParameterMapping(OperationMapping operationMapping);
    
    /**
     * Deletes the given {@link AttributeMapping}.
     * 
     * @param attributeMapping the attribute mapping to delete
     */
    void deleteAttributeMapping(AttributeMapping attributeMapping);
    
    /**
     * Deletes the given {@link EnumLiteralMapping}.
     * 
     * @param enumLiteralMapping the enum literal mapping to delete
     */
    void deleteEnumLiteralMapping(EnumLiteralMapping enumLiteralMapping);
    
   /**
     * Deletes the give {@link ClassifierMapping}.
     * 
     * @param classifierMapping the classifier mapping to delete
     */
    void deleteClassifierMapping(ClassifierMapping classifierMapping);
    
    /**
     * Deletes the give {@link EnumMapping}.
     * 
     * @param enumMapping the enum mapping to delete
     */
    void deleteEnumMapping(EnumMapping enumMapping);
    
   /**
     * Deletes the given {@link OperationMapping}.
     * 
     * @param operationMapping the operation mapping to delete
     */
    void deleteOperationMapping(OperationMapping operationMapping);
    
    /**
     * Deletes the given {@link ParameterMapping}.
     * 
     * @param parameterMapping the parameter mapping to delete
     */
    void deleteParameterMapping(ParameterMapping parameterMapping);
}
