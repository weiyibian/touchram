package ca.mcgill.sel.ram.ui.views.structural;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREModelElementComposition;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.ram.ClassifierMapping;
import ca.mcgill.sel.ram.EnumMapping;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.listeners.UINotifyChangedListener;
import ca.mcgill.sel.ram.ui.events.listeners.ActionListener;
import ca.mcgill.sel.ram.ui.layouts.VerticalLayout;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.views.handler.HandlerFactory;
import ca.mcgill.sel.ram.ui.views.handler.IHandled;
import ca.mcgill.sel.ram.ui.views.structural.handler.ICompositionViewHandler;

/**
 * This view contains all the views related to a model composition
 * and details of the compositions of it. 
 * {@link CompositionContainerView} can contain any number of this type of views.
 * 
 * @author eyildirim
 */
public class CompositionView extends RamRectangleComponent implements ActionListener,
        IHandled<ICompositionViewHandler>, UINotifyChangedListener {
    
    /**
     * The action to add a classifier mapping.
     */
    protected static final String ACTION_CLASSIFIER_MAPPING_ADD = "mapping.classifier.add";

    /**
     * The action to add an enum mapping.
     */
    protected static final String ACTION_ENUM_MAPPING_ADD = "mapping.enum.add";
   
    private COREModelComposition modelComposition;
    /**
     * The CompositionContainerView which it belongs to.
     */
    private CompositionContainerView myCompositionContainerView;
    
    /**
     * Every composition view has an composition title view object.
     */
    private CompositionTitleView myCompositionTitleView;
    
    /**
     * We will show/hide this container by adding/removing it as a child.
     * It keeps all the mapping related containers.
     */
    private RamRectangleComponent hideableMappingContainer;
    
    /**
     * Map for easy accessing to views through mapping information.
     */
    private Map<COREMapping<?>, RamRectangleComponent> mappingToMappingContainerView;
    
    /**
     * Status of the compositionView.
     * It is either expanded (detailed view, in which all the class mappings)
     * or collapsed (class mappings are hidden by removing the
     * hideableMappingContainer from the children of CompositionView)
     */
    private boolean detailedViewOn;
    
    private ICompositionViewHandler handler;
    
    /**
     * Creates a new composition view.
     * 
     * @param modelComposition the model composition to create a view for
     * @param reuse the reuse the composition is for
     * @param containerView  the composition container view that contains this composition view. 
     */
    public CompositionView(COREModelComposition modelComposition, COREReuse reuse, 
                                CompositionContainerView containerView) {
        setBufferSize(Cardinal.NORTH, 0);
        setBufferSize(Cardinal.SOUTH, 0);
        setBufferSize(Cardinal.WEST, 5);
        setBufferSize(Cardinal.EAST, 5);
        
        detailedViewOn = false;
        
        setHandler(HandlerFactory.INSTANCE.getCompositionViewHandler());
        myCompositionContainerView = containerView;

        this.modelComposition = modelComposition;
        mappingToMappingContainerView = new HashMap<COREMapping<?>, RamRectangleComponent>();
        
        setNoFill(false);
        setNoStroke(true);
        // setStrokeWeight(2);
        setFillColor(Colors.COMPOSITION_VIEW_FILL_COLOR);
        
        myCompositionTitleView = new CompositionTitleView(this, reuse, detailedViewOn);
        this.addChild(myCompositionTitleView);
        
        hideableMappingContainer = new RamRectangleComponent();
        hideableMappingContainer.setBuffers(0);
        hideableMappingContainer.setNoStroke(true);
        hideableMappingContainer.setLineStipple((short) 0xAAAAA);
        hideableMappingContainer.setStrokeWeight(2 / 10);
        hideableMappingContainer.setStrokeColor(Colors.DEFAULT_ELEMENT_STROKE_COLOR);
        hideableMappingContainer.setLayout(new VerticalLayout(0));
        
        setLayout(new VerticalLayout(1));
        
        addAllMappings();
        
        // get updates from the composition in case of a change in the mappings
        EMFEditUtil.addListenerFor(modelComposition, this);
    }
    
    @Override
    public void actionPerformed(ActionEvent event) {
        String actionCommand = event.getActionCommand();
        
        if (ACTION_CLASSIFIER_MAPPING_ADD.equals(actionCommand)) {
            handler.addClassifierMapping(this);
        }
    }
    
    /**
     * Adds all mappings to this view.
     */
    private void addAllMappings() {        
        for (COREModelElementComposition<?> composition : modelComposition.getCompositions()) {
            COREMapping<?> mapping = (COREMapping<?>) composition;
            addMappingContainerView(mapping);
        }
    }
    
    /**
     * This method adds a {@link ClassifierMappingContainerView} for the given {@link ClassifierMapping} or
     * a {@link EnumMappingContainerView} for the given {@link EnumMapping}.
     * 
     * @param newMapping
     *            the new {@link COREMapping} to be added
     */
    public void addMappingContainerView(COREMapping<?> newMapping) {
        RamRectangleComponent mappingContainerView;
        
        if (newMapping instanceof ClassifierMapping) {
            mappingContainerView = new ClassifierMappingContainerView((ClassifierMapping) newMapping);
        } else {
            mappingContainerView = new EnumMappingContainerView((EnumMapping) newMapping);
        }
        hideableMappingContainer.addChild(mappingContainerView);
        mappingToMappingContainerView.put(newMapping, mappingContainerView);
    }
    
    /**
     * Removes the given mapping from the view.
     * 
     * @param deletedMapping the {@link COREMapping} to delete
     */
    public void deleteMappingContainerView(COREMapping<?> deletedMapping) {
        // view to be deleted
        RamRectangleComponent mappingContainerView =
                mappingToMappingContainerView.get(deletedMapping);
        hideableMappingContainer.removeChild(mappingContainerView);
        mappingToMappingContainerView.remove(deletedMapping);
        mappingContainerView.destroy();
    }
    
    @Override
    public void destroy() {
        super.destroy();
        
        /**
         * The container needs to be destroyed manually if it is currently not displayed.
         */
        if (!detailedViewOn) {
            hideableMappingContainer.destroy();
        }
        
        if (modelComposition != null) {
            EMFEditUtil.removeListenerFor(modelComposition, this);
        }
    }
    
    @Override
    public ICompositionViewHandler getHandler() {
        return handler;
    }
    
    /**
     * Gets the {@link COREModelComposition} that is associated to this view.
     * 
     * @return this view's {@link COREModelComposition} object
     */
    public COREModelComposition getModelComposition() {
        return modelComposition;
    }
    
    /**
     * Gets the {@link CompositionContainerView} which it belongs to.
     * 
     * @return {@link CompositionContainerView} which it belongs to
     */
    public CompositionContainerView getCompositionContainerView() {
        return myCompositionContainerView;
    }
    
    /**
     * Hides the composition mapping details.
     */
    public void hideMappingDetails() {
        if (isDetailedViewOn()) {
            this.removeChild(hideableMappingContainer);
            myCompositionTitleView.hideButtons();
            setDetailedViewOn(false);
        }
    }
    
    /**
     * Is detailed mapping view on for the model composition.
     * 
     * @return true if detailed view is on false otherwise
     */
    public boolean isDetailedViewOn() {
        return detailedViewOn;
    }
    
    @Override
    public void handleNotification(Notification notification) {
        if (notification.getNotifier() == modelComposition) {
            if (notification.getFeature() == CorePackage.Literals.CORE_MODEL_COMPOSITION__COMPOSITIONS) {
                switch (notification.getEventType()) {
                    case Notification.ADD:
                        addMappingContainerView((COREMapping<?>) notification.getNewValue());
                        break;
                    case Notification.REMOVE:
                        deleteMappingContainerView((COREMapping<?>) notification.getOldValue());
                        break;
                }
            }
        }
    }
    
    /**
     * Set the detailedViewOn value.
     * 
     * @param detailedViewOn whether the detailed view should be enabled
     */
    public void setDetailedViewOn(boolean detailedViewOn) {
        this.detailedViewOn = detailedViewOn;
    }
    
    @Override
    public void setHandler(ICompositionViewHandler handler) {
        this.handler = handler;
        
    }
    
    /**
     * Shows composition mapping details.
     */
    public void showMappingDetails() {
        if (!containsChild(hideableMappingContainer)) {
            this.addChild(hideableMappingContainer);
            myCompositionTitleView.showButtons();
            setDetailedViewOn(true);
        }
    }
    
}
