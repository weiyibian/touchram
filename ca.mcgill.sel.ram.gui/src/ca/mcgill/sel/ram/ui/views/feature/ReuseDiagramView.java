package ca.mcgill.sel.ram.ui.views.feature;

import org.mt4j.input.gestureAction.TapAndHoldVisualizer;
import org.mt4j.input.inputProcessors.componentProcessors.panProcessor.PanProcessorTwoFingers;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldProcessor;
import org.mt4j.input.inputProcessors.componentProcessors.zoomProcessor.ZoomProcessor;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.core.COREModelReuse;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.events.MouseWheelProcessor;
import ca.mcgill.sel.ram.ui.events.RightClickDragProcessor;
import ca.mcgill.sel.ram.ui.scenes.AbstractConcernScene;
import ca.mcgill.sel.ram.ui.scenes.DisplayConcernSelectScene;
import ca.mcgill.sel.ram.ui.views.feature.helpers.SelectionFeature.FeatureSelectionStatus;
import ca.mcgill.sel.ram.ui.views.handler.IAbstractViewHandler;

/**
 * Represents graphically a Reuse.
 * @author arthurls
 */
public class ReuseDiagramView extends FeatureDiagramView<IAbstractViewHandler> {
    
    /**
     * Constructor.
     * @param width of the component
     * @param height of the component
     * @param scene the current scene we are in
     * @param reuse the reuse to display in s[lit view
     * @param modelReuse the configuration selected to display. null if default.
     */
    public ReuseDiagramView(float width, float height, AbstractConcernScene<?, ?> scene, 
            COREReuse reuse, COREModelReuse modelReuse) {
        super(width, height, scene, reuse.getReusedConcern().getFeatureModel(), reuse, 
                modelReuse);
        Vector3D position = new Vector3D(0, height);
        this.setPositionGlobal(position);
    }
    
    @Override
    protected void registerInputProcessors() {
        registerInputProcessor(new PanProcessorTwoFingers(RamApp.getApplication()));
        registerInputProcessor(new RightClickDragProcessor(RamApp.getApplication()));
        registerInputProcessor(new ZoomProcessor(RamApp.getApplication()));
        registerInputProcessor(new MouseWheelProcessor(RamApp.getApplication()));
    }

    @Override
    protected void registerListeners(FeatureView feature) {
        if (scene instanceof DisplayConcernSelectScene) {
            if (feature.getFeatureSelectionStatus() == FeatureSelectionStatus.RE_EXPOSED) {
                feature.setListenersReuseSelectMode();
                
                feature.addGestureListener(TapAndHoldProcessor.class, new TapAndHoldVisualizer(RamApp.getApplication(),
                        containerLayer));
            }
        }
    }
    
    @Override
    protected void displayFeature(FeatureView feature) {
        // Display feature
        feature.setPositionRelativeToParent(new Vector3D(feature.getXposition(), feature.getYposition(), 0));
        
        containerLayer.addChild(feature);

        for (FeatureView child : tree.getChildren(feature)) {
            displayFeature(child);
        }

        // Draw line to parent
        drawLine(feature, feature.getParentFeatureView());
    }
    
    /**
     * Update color for each {@link FeatureView} based on its selection status.
     */
    public void updateFeatureColors() {
        for (FeatureView feature : getFeatureViews()) {
            feature.updateSelectionColor();
        }
    }

    @Override
    public void updateFeaturesDisplay(boolean repopulate) {
        super.updateFeaturesDisplay(repopulate);
        // Put correct color to features
        updateFeatureColors();
    }

}
