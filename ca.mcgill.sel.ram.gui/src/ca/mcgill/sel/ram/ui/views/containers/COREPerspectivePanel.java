package ca.mcgill.sel.ram.ui.views.containers;

import java.util.Collection;

import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.ram.ui.components.RamListComponent.Namer;
import ca.mcgill.sel.ram.ui.components.RamPanelListComponent;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.components.listeners.RamListListener;
import ca.mcgill.sel.ram.ui.layouts.HorizontalLayoutVerticallyCentered;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Strings;

/**
 * Panel container that visualizes the Perspectives that are currently loaded in TouchCORE.
 * 
 * @author joerg
 */
public class COREPerspectivePanel extends RamPanelListComponent<COREPerspective> {
    
    private Collection<COREPerspective> perspectives;
    
    /**
     * Creates a new panel with the given horizontal and vertical sticks.
     * 
     * @param horizontalStick The horizontalStick for the panel
     * @param verticalStick The verticalStick for the panel
     * @param perspectives List of the perspectives to display
     * @param listener The custom listener for that panel
     */
    public COREPerspectivePanel(HorizontalStick horizontalStick, VerticalStick verticalStick,
            Collection<COREPerspective> perspectives, RamListListener<COREPerspective> listener) {
        super(5, Strings.LABEL_PERSPECTIVE_PANEL, horizontalStick, verticalStick);
        
        this.perspectives = perspectives;
        setNamer(new InternalPerspectivePanelNamer());
        setListener(listener);
        setElements(perspectives);
    }

    
    /**
     * Internal Perspective Panel Namer.
     */
    private class InternalPerspectivePanelNamer implements Namer<COREPerspective> {

        @Override
        public RamRectangleComponent getDisplayComponent(COREPerspective perspective) {
            return createPerspectivePanelComponent(perspective, false);
        }

        @Override
        public String getSortingName(COREPerspective perspective) {
            return perspective.getName();
        }

        @Override
        public String getSearchingName(COREPerspective perspective) {
            return perspective.getName();
        }
        
        /**
         * Internal method to create an element of the list.
         * @param p the perspective we want to display in the list
         * @param noFill 
         * @return the RamRectangle Component
         */
        private RamRectangleComponent createPerspectivePanelComponent(COREPerspective p, boolean noFill) {
            RamRectangleComponent view = new RamRectangleComponent();
            view.setLayout(new HorizontalLayoutVerticallyCentered());

            // Removes quotes of the message
            RamTextComponent text = new RamTextComponent(p.getName());
            text.setPickable(false);

            // Adds spaces in left and right
            final int space = 5;
            view.setBufferSize(Cardinal.WEST, space);
            view.setBufferSize(Cardinal.EAST, space);

            view.addChild(text);

            view.setNoFill(noFill);
            view.setFillColor(Colors.DEFAULT_ELEMENT_FILL_COLOR);
            view.setNoStroke(false);

            return view;
        }
    }
    
    /**
     * Returns the list of perspectives contained in the list.
     * @return list of perspectives
     */
    public Collection<COREPerspective> getPerspectives() {
        return perspectives;
    }
    
    /**
     * Changes the color of the current selected reuse in the list.
     * Returns true if the reuse given is in the list
     * Returns false if the reuse is not in the list
     * @param p the perspective to highlight in the list
     * @return list.contains(p)
     */
    public boolean setSelectedReuse(COREPerspective p) {
        if (list.getElementMap().containsKey(p)) {
            list.getElementMap().get(p).setFillColor(Colors.FEATURE_VIEW_IS_REUSING);
            return true;
        }
        return false;
    }
    
    public void clearSelections() {
        for (COREPerspective p : perspectives) {
            list.getElementMap().get(p).setFillColor(Colors.DEFAULT_ELEMENT_FILL_COLOR);
        }
    }
    
}
