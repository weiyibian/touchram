package ca.mcgill.sel.ram.ui.views.structural.handler.impl;

import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;

import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.controller.ControllerFactory;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.scenes.DisplayAspectScene;
import ca.mcgill.sel.ram.ui.views.handler.BaseHandler;
import ca.mcgill.sel.ram.ui.views.handler.HandlerFactory;
import ca.mcgill.sel.ram.ui.views.handler.IDisplaySceneHandler;
import ca.mcgill.sel.ram.ui.views.structural.CompositionSplitEditingView;
import ca.mcgill.sel.ram.ui.views.structural.CompositionView;
import ca.mcgill.sel.ram.ui.views.structural.handler.ICompositionViewHandler;

/**
 * The default handler for a {@link CompositionView}.
 * 
 * @author eyildirim
 */
public class CompositionViewHandler extends BaseHandler implements ICompositionViewHandler {
    
    @Override
    public void addClassifierMapping(CompositionView compositionView) {
        COREModelComposition composition = compositionView.getModelComposition();
        ControllerFactory.INSTANCE.getMappingsController().createClassifierMapping(composition);
    }
    
    @Override
    public void addEnumMapping(CompositionView compositionView) {
        COREModelComposition composition = compositionView.getModelComposition();
        ControllerFactory.INSTANCE.getMappingsController().createEnumMapping(composition);
    }
    
    @Override
    public void hideMappingDetails(CompositionView compositionView) {
        compositionView.hideMappingDetails();
    }
    
    @Override
    public boolean processTapEvent(TapEvent tapEvent) {
        // we don't want anything to happen when we tap in an empty area in composition view
        return true;
    }
    
    @Override
    public void showExternalAspectOfComposition(CompositionView compositionView) {
        COREModelComposition modelComposition = compositionView.getModelComposition();
        Aspect source = (Aspect) modelComposition.getSource();
        boolean splitCompositionModeEnabled = isSplitViewEnabled(RamApp.getActiveAspectScene());
        if (!splitCompositionModeEnabled) {
            RamApp.getApplication().loadScene(COREArtefactUtil.getReferencingExternalArtefact(source),
                    source);
        } else {
            RamApp.getActiveAspectScene().getHandler().back(RamApp.getActiveAspectScene());
            RamApp.getApplication().loadScene(COREArtefactUtil.getReferencingExternalArtefact(source),
                    source);
        }
        
    }
    
    @Override
    public void showMappingDetails(CompositionView compositionView) {
        compositionView.showMappingDetails();
    }
    
    @Override
    public void switchToSplitView(CompositionView compositionView) {
        // Get the latest used display aspect before switching into composition edit mode
        
        DisplayAspectScene displayAspectScene = RamApp.getActiveAspectScene();
        boolean splitCompositionModeEnabled = displayAspectScene != null && isSplitViewEnabled(displayAspectScene);
        
        // Get the handler of the display aspect scene to call its function for switching into composition edit mode.
        IDisplaySceneHandler handler = HandlerFactory.INSTANCE.getDisplayAspectSceneHandler();
        
        if (splitCompositionModeEnabled) {
            // we are already in split composition editing mode , do the opposite..
            handler.closeSplitView(displayAspectScene);
        } else {
            // trigger the mode switching
            handler.switchToCompositionEditMode(displayAspectScene, compositionView);
        }
    }
    
    /**
     * Returns whether split view is currently enabled.
     * 
     * @param scene the current {@link DisplayAspectScene}
     * @return true, whether split view is enabled, false otherwise
     */
    private static boolean isSplitViewEnabled(DisplayAspectScene scene) {
        return scene.getCurrentView() instanceof CompositionSplitEditingView;
    }
    
}
