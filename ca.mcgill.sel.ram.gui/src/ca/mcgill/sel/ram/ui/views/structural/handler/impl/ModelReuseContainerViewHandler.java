package ca.mcgill.sel.ram.ui.views.structural.handler.impl;

import java.util.List;

import org.mt4j.sceneManagement.transition.BlendTransition;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREModelReuse;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.core.controller.COREControllerFactory;
import ca.mcgill.sel.core.util.COREInterfaceUtil;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamSelectorComponent;
import ca.mcgill.sel.ram.ui.components.browser.CoreFileBrowser;
import ca.mcgill.sel.ram.ui.components.browser.interfaces.CoreFileBrowserListener;
import ca.mcgill.sel.ram.ui.components.listeners.AbstractDefaultRamSelectorListener;
import ca.mcgill.sel.ram.ui.scenes.handler.IConcernSelectSceneHandler;
import ca.mcgill.sel.ram.ui.scenes.handler.impl.ConcernSelectSceneHandler;
import ca.mcgill.sel.ram.ui.utils.GUIConstants;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.handler.HandlerFactory;
import ca.mcgill.sel.ram.ui.views.structural.CompositionSplitEditingView;
import ca.mcgill.sel.ram.ui.views.structural.handler.ICompositionContainerViewHandler;

/**
 * Handles events for a {@link ca.mcgill.sel.ram.ui.views.structural.CompositionContainerView} which is showing the
 * list of model reuses in a model.
 *
 * @author oalam
 */
public class ModelReuseContainerViewHandler implements ICompositionContainerViewHandler {
    
    @Override
    public void deleteModelComposition(COREModelComposition modelComposition) {
        // Disallow deleting if split view is enabled.
        boolean splitModeEnabled =
                RamApp.getActiveAspectScene().getCurrentView() instanceof CompositionSplitEditingView;
        if (!splitModeEnabled) {
            COREModelReuse modelReuse = (COREModelReuse) modelComposition;
            COREControllerFactory.INSTANCE.getReuseController().removeModelReuse(modelReuse);            
        } else {
            RamApp.getActiveAspectScene().displayPopup("You can not delete the model reuse while editing it.");
        }
    }

    @Override
    public void loadBrowser(final COREArtefact artefact) {
        CoreFileBrowser.setInitialFolder(GUIConstants.DIRECTORY_LIBRARIES);

        // Ask the user to select an aspect that should be depended on
        CoreFileBrowser.loadCoreFile(new CoreFileBrowserListener() {

            @Override
            public void concernLoaded(COREConcern concern) {
                IConcernSelectSceneHandler handler = HandlerFactory.INSTANCE.getDisplayFMSceneSelectHandler();
                if (concern == null) {
                    RamApp.getActiveScene().displayPopup(Strings.POPUP_ERROR_SELF_REUSE);
                } else {
                    RamApp.getActiveScene().setTransition(new BlendTransition(RamApp.getApplication(), 700));
                    RamApp.getApplication().displayFeatureModelSelectScene(concern, handler, artefact, null);
                }
            }
        }, true);
    }

    @Override
    public void tailorExistingReuse(COREArtefact artefact) {
        COREConcern concern = (COREConcern) artefact.eContainer();
        
        List<COREReuse> reuses = COREInterfaceUtil.getReuses(concern);
        RamSelectorComponent<COREReuse> selector = new RamSelectorComponent<COREReuse>(reuses);
        
        Vector3D vect = new Vector3D(RamApp.getApplication().mouseX + selector.getWidth() / 2, 
                RamApp.getApplication().mouseY);
        RamApp.getActiveScene().addComponent(selector, vect);
        
        selector.registerListener(new AbstractDefaultRamSelectorListener<COREReuse>() {
            @Override
            public void elementSelected(RamSelectorComponent<COREReuse> selector, COREReuse element) {
                ConcernSelectSceneHandler selectHandler = new ConcernSelectSceneHandler();
                RamApp.getApplication()
                    .displayFeatureModelSelectScene(element.getReusedConcern(), selectHandler, artefact, element);
                selector.destroy();
                
            }
        });
    }
}
