package ca.mcgill.sel.ram.ui.views.feature.handler.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.mt4j.components.MTComponent;
import org.mt4j.components.TransformSpace;
import org.mt4j.input.inputProcessors.MTGestureEvent;
import org.mt4j.input.inputProcessors.componentProcessors.dragProcessor.DragEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;
import org.mt4j.sceneManagement.transition.SlideTransition;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.classdiagram.util.CdmModelUtil;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREExternalLanguage;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.COREFeatureModel;
import ca.mcgill.sel.core.COREFeatureRelationshipType;
import ca.mcgill.sel.core.CORELanguage;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.COREScene;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.controller.COREControllerFactory;
import ca.mcgill.sel.core.controller.FeatureController;
import ca.mcgill.sel.core.perspective.COREPerspectiveUtil;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.core.util.COREModelUtil;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.ui.LanguageConstant;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamListComponent.Namer;
import ca.mcgill.sel.ram.ui.components.RamPopup.PopupType;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamSelectorComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.components.browser.GenericFileBrowser;
import ca.mcgill.sel.ram.ui.components.browser.interfaces.FileBrowserListener;
import ca.mcgill.sel.ram.ui.components.listeners.AbstractDefaultRamSelectorListener;
import ca.mcgill.sel.ram.ui.components.listeners.RamSelectorListener;
import ca.mcgill.sel.ram.ui.components.navigationbar.NavigationBar;
import ca.mcgill.sel.ram.ui.events.DelayedDrag;
import ca.mcgill.sel.ram.ui.scenes.AbstractConcernScene;
import ca.mcgill.sel.ram.ui.scenes.DisplayConcernEditScene;
import ca.mcgill.sel.ram.ui.scenes.RamAbstractScene;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.GUIConstants;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.OptionSelectorView;
import ca.mcgill.sel.ram.ui.views.feature.FeatureDiagramEditView;
import ca.mcgill.sel.ram.ui.views.feature.FeatureView;
import ca.mcgill.sel.ram.ui.views.feature.handler.IFeatureHandler;
import ca.mcgill.sel.ram.ui.views.handler.BaseHandler;
import ca.mcgill.sel.ram.util.RAMModelUtil;
import ca.mcgill.sel.usecases.util.UcModelUtil;
import ca.mcgill.sel.restif.util.RifModelUtil;

/**
 * Feature Handler, used to handle events on the Feature.
 *
 * @author Nishanth
 * @author ddevine
 */
public class FeatureEditModeHandler extends BaseHandler implements IFeatureHandler {

    private DelayedDrag dragAction = new DelayedDrag(GUIConstants.DELAYED_DRAG_MIN_DRAG_DISTANCE);

    /**
     * Enum containing all the options to be shown when tap and hold is complete.
     */
    private enum CreateFeature {
        OPEN_REALIZATION_MODEL, NEW_REALIZATION_MODEL,
        IMPORT_REALIZATION_MODEL, ASSOCIATE_CONFLICT_RESOLUTION_MODEL, DELETE_FEATURE,
        ADD_OPTIONAL, ADD_MANDATORY, ADD_XOR,
        ADD_OR, COLLAPSE, EXPAND
    }

    /**
     * Namer for the selector of scenes to show.
     */
    private class SceneSelectorNamer implements Namer<COREScene> {
        @Override
        public RamRectangleComponent getDisplayComponent(COREScene element) {
            RamTextComponent textComponent = new
                    RamTextComponent(element.getPerspectiveName() + ": " + element.getName());
            textComponent.setNoFill(false);
            textComponent.setFillColor(Colors.DEFAULT_ELEMENT_FILL_COLOR);
            textComponent.setAutoMaximizes(true);
            textComponent.setNoStroke(false);
            textComponent.setStrokeColor(Colors.DEFAULT_ELEMENT_STROKE_COLOR);
            return textComponent;
        }

        @Override
        public String getSortingName(COREScene element) {
            return element.getName();
        }

        @Override
        public String getSearchingName(COREScene element) {
            return element.getName();
        }
    }

    /**
     * Listener for the selector of aspects to show.
     */
    private class ModelSelectorListener implements RamSelectorListener<COREScene> {

        @Override
        public void elementSelected(RamSelectorComponent<COREScene> selector, COREScene element) {

            COREPerspective p = COREPerspectiveUtil.INSTANCE.getPerspective(element.getPerspectiveName());

            // in case the current perspective is not set, set it
            NavigationBar.getInstance().setCurrentPerspective(p);
            
            COREExternalArtefact artefact;
            if (p.getDefault() != null) {
                artefact = (COREExternalArtefact) element.getArtefacts().get(p.getDefault()).get(0);
            } else {
                artefact = (COREExternalArtefact) element.getArtefacts().values().iterator().next().get(0);
            }
            showModel(artefact, artefact.getRootModelElement());
            selector.destroy();
        }

        @Override
        public void elementHeld(RamSelectorComponent<COREScene> selector, COREScene element) {
        }

        @Override
        public void elementDoubleClicked(RamSelectorComponent<COREScene> selector, COREScene element) {
        }

        @Override
        public void closeSelector(RamSelectorComponent<COREScene> selector) {
            selector.destroy();
        }
    }

    /**
     * Listener for the selector of scenes that are shown when the concern designer selects "Associate Conflict
     * Resolution Model".
     */
    private final class SceneSelectorListener implements RamSelectorListener<COREScene> {

        private COREFeature selectedFeature;

        private SceneSelectorListener(COREFeature feature) {
            selectedFeature = feature;
        }

        @Override
        public void elementSelected(RamSelectorComponent<COREScene> selector, COREScene element) {
            handleAssociateConflictResolutionModel(element, selectedFeature);
            selector.destroy();
        }

        @Override
        public void elementHeld(RamSelectorComponent<COREScene> selector, COREScene element) {
        }

        @Override
        public void elementDoubleClicked(RamSelectorComponent<COREScene> selector, COREScene element) {
        }

        @Override
        public void closeSelector(RamSelectorComponent<COREScene> selector) {
            selector.destroy();
        }
    }

    @Override
    public boolean processTapAndHoldEvent(TapAndHoldEvent tapAndHoldEvent) {
        // Get the current working concern scene.
        DisplayConcernEditScene concernScene = RamApp.getActiveConcernEditScene();

        if (tapAndHoldEvent.isHoldComplete() && concernScene != null) {
            FeatureView featureIcon = (FeatureView) tapAndHoldEvent.getTarget();
            showFeatureOptions(concernScene, featureIcon, tapAndHoldEvent.getLocationOnScreen());
        }
        return true;
    }

    /**
     * Shows the options for the feature.
     *
     * @param scene the current scene
     * @param view the view of the selected feature
     * @param location the location on the screen where the options should be shown
     */
    private void showFeatureOptions(final DisplayConcernEditScene scene,
            final FeatureView view, final Vector3D location) {

        OptionSelectorView<CreateFeature> selector = new OptionSelectorView<CreateFeature>(getActions(scene, view));

        scene.addComponent(selector, location);

        // CHECKSTYLE:IGNORE AnonInnerLength: They can be more than 30 lines
        selector.registerListener(new AbstractDefaultRamSelectorListener<CreateFeature>() {
            @Override
            public void elementSelected(RamSelectorComponent<CreateFeature> selector, CreateFeature element) {
                switch (element) {
                    case OPEN_REALIZATION_MODEL:
                        handleShowModel(view, location);
                        break;
                    case ADD_OPTIONAL:
                        addChild(scene, view.getFeature(), -1, COREFeatureRelationshipType.OPTIONAL);
                        break;
                    case ADD_MANDATORY:
                        addChild(scene, view.getFeature(), -1, COREFeatureRelationshipType.MANDATORY);
                        break;
                    case ADD_XOR:
                        addChild(scene, view.getFeature(), -1, COREFeatureRelationshipType.XOR);
                        break;
                    case ADD_OR:
                        addChild(scene, view.getFeature(), -1, COREFeatureRelationshipType.OR);
                        break;
                    case DELETE_FEATURE:
                        view.getHandler().deleteFeature(scene, view);
                        break;
                    case NEW_REALIZATION_MODEL:
                        showPerspectiveOptions(scene, view, location);
                        break;
                    case IMPORT_REALIZATION_MODEL:
                        loadBrowserAndAssociate(scene, view, location);
                        break;
                    case ASSOCIATE_CONFLICT_RESOLUTION_MODEL:
                        handleShowScene(view, location);
                        break;
                    case COLLAPSE:
                    case EXPAND:
                        view.getHandler().hideFeature(scene, view);
                        break;
                }
            }


        });
    }


    /**
     * Shows the perspective languages that can be used to realize a feature. If there is only one language
     * in the perspective then that one language is automatically chosen.
     *
     * @param scene the current scene
     * @param view the view of the selected feature
     * @param perspective the perspective of the language
     * @param location the location on the screen where the options should be shown
     */
    private static void showPerspectiveLanguageOptions(final DisplayConcernEditScene scene,
            final FeatureView view, final Vector3D location, COREPerspective perspective) {

        // in case the current perspective is not set, set it
        NavigationBar.getInstance().setCurrentPerspective(perspective);
        
        Set<String> models = perspective.getLanguages().keySet();

        if (models.size() == 1) {
            // there is only one model in this perspective, so we can directly create this model
            handleNewDesignModel(scene.getConcern(), view.getFeature(), perspective,
                    models.iterator().next());
        } else {
            RamSelectorComponent<String> selector;

            selector = new RamSelectorComponent<String>(
                    new ArrayList<String>(perspective.getLanguages().keySet()));

            scene.addComponent(selector, location);

            selector.registerListener(new AbstractDefaultRamSelectorListener<String>() {

                @Override
                public void elementSelected(RamSelectorComponent<String> selector,
                        String element) {
                    handleNewDesignModel(scene.getConcern(), view.getFeature(), perspective, element);
                    selector.destroy();
                }
            });
        }
    }

    /**
     * Shows the perspectives that are currently loaded in TouchCORE that can be used to realize a feature.
     * If there is already a current selected perspective in the perspective panel then that perspective is
     * automatically selected and the languages of the perspective are displayed instead.
     *
     * @param scene the current scene
     * @param view the view of the selected feature
     * @param location the location on the screen where the options should be shown
     */
    private static void showPerspectiveOptions(final DisplayConcernEditScene scene,
            final FeatureView view, final Vector3D location) {

        if (scene.getCurrentSelectedPerspective() == null) {
            // the modeller has not selected a perspective, so let's display the list of currently loaded
            // perspectives so one can be chosen
            Collection<COREPerspective> perspectives = COREPerspectiveUtil.INSTANCE.getPerspectives();

            RamSelectorComponent<COREPerspective> selector;

            selector = new RamSelectorComponent<COREPerspective>(
                    new ArrayList<COREPerspective>(perspectives));

            scene.addComponent(selector, location);

            selector.registerListener(new AbstractDefaultRamSelectorListener<COREPerspective>() {

                @Override
                public void elementSelected(RamSelectorComponent<COREPerspective> selector, COREPerspective element) {
                    showPerspectiveLanguageOptions(scene, view, location, element);
                    selector.destroy();
                }
            });
        } else {
            // the modeller has already selected a perspective in the perspective reuse panel
            showPerspectiveLanguageOptions(scene, view, location, scene.getCurrentSelectedPerspective());
        }
    }

    /**
     * Get the possible actions on the given feature.
     *
     * @param scene - The current scene
     * @param feature - The given {@link FeatureView}
     * @return the set of available actions
     */
    private static CreateFeature[] getActions(DisplayConcernEditScene scene, FeatureView feature) {
        EnumSet<CreateFeature> options = EnumSet.allOf(CreateFeature.class);
        // Remove collapse and expand options depending of the state.
        if (feature.getIsRoot()) {
            options.remove(CreateFeature.DELETE_FEATURE);
            options.remove(CreateFeature.COLLAPSE);
            options.remove(CreateFeature.EXPAND);
        } else if (!feature.isReuse() && !feature.isParentReuse()) {
            options.remove(CreateFeature.COLLAPSE);
            options.remove(CreateFeature.EXPAND);
        } else if (feature.getChildrenFeatureViews().isEmpty()) {
            options.remove(CreateFeature.COLLAPSE);
            options.remove(CreateFeature.EXPAND);
        } else if (feature.getFeature().getChildren().isEmpty()) {
            options.remove(CreateFeature.COLLAPSE);
            options.remove(CreateFeature.EXPAND);
        } else if (!scene.isFeatureCollapsed(feature)) {
            options.remove(CreateFeature.EXPAND);
        } else {
            options.remove(CreateFeature.COLLAPSE);
        }

        // Remove 'open realizing model' if there is none.
        List<COREScene> scenes = feature.getFeature().getRealizedBy();
        COREPerspective currentPerspective = NavigationBar.getInstance().getCurrentPerspective();

        if (currentPerspective == null && scenes.isEmpty()) {
            options.remove(CreateFeature.OPEN_REALIZATION_MODEL);
        } else if (currentPerspective != null) {
            boolean foundScene = false;
            for (COREScene s : scenes) {
                if (s.getPerspectiveName().contentEquals(
                        currentPerspective.getName())) {
                    foundScene = true;
                }
            }
            if (!foundScene) {
                options.remove(CreateFeature.OPEN_REALIZATION_MODEL);
            }
        }
        return options.toArray(new CreateFeature[0]);
    }

    /**
     * Function called, when an external aspect is to be loaded and associated with a feature.
     *
     * @param scene the current scene
     * @param view - The {@link FeatureView} we want to associate with the new aspect
     * @param location - where to display the popup
     */
    private static void loadBrowserAndAssociate(DisplayConcernEditScene scene, final FeatureView view,
            final Vector3D location) {
        //TODO: Update so that models of any perspective/language can be added

        if (scene.getCurrentSelectedPerspective() == null) {
            // the modeller has not selected a perspective, so let's display the list of currently loaded
            // perspectives so one can be chosen
            Collection<COREPerspective> perspectives = COREPerspectiveUtil.INSTANCE.getPerspectives();

            RamSelectorComponent<COREPerspective> selector;

            selector = new RamSelectorComponent<COREPerspective>(
                    new ArrayList<COREPerspective>(perspectives));

            scene.addComponent(selector, location);

            selector.registerListener(new AbstractDefaultRamSelectorListener<COREPerspective>() {

                @Override
                public void elementSelected(RamSelectorComponent<COREPerspective> selector, COREPerspective element) {
                    loadBrowserAndAssociateCont(scene, view, location, element);
                    selector.destroy();
                }
            });
        } else {
            // the modeller has already selected a perspective in the perspective reuse panel
            loadBrowserAndAssociateCont(scene, view, location, scene.getCurrentSelectedPerspective());
        }
    }

    /**
     * Operation that gives the user choices of which model role the imported model should play.
     * @param scene the scene
     * @param view the featureview
     * @param location the location that was clicked
     * @param perspective the selected perspective
     */
    private static void loadBrowserAndAssociateCont(DisplayConcernEditScene scene, final FeatureView view,
            final Vector3D location, COREPerspective perspective) {

        Set<String> models = perspective.getLanguages().keySet();

        if (models.size() == 1) {
            // there is only one model in this perspective, so we can directly associate to this role
            loadBrowserAndAssociateCont2(scene, view, perspective,
                    models.iterator().next());
        } else {
            RamSelectorComponent<String> selector;

            selector = new RamSelectorComponent<String>(
                    new ArrayList<String>(perspective.getLanguages().keySet()));

            scene.addComponent(selector, location);

            selector.registerListener(new AbstractDefaultRamSelectorListener<String>() {

                @Override
                public void elementSelected(RamSelectorComponent<String> selector,
                        String element) {
                    loadBrowserAndAssociateCont2(scene, view, perspective, element);
                    selector.destroy();
                }
            });
        }

    }

    /**
     * Operation that gives the user choices of which model role the imported model should play.
     * @param scene the scene
     * @param view the featureview
     * @param perspective the selected perspective
     * @param roleName the role the imported model should play
     */
    private static void loadBrowserAndAssociateCont2(DisplayConcernEditScene scene, final FeatureView view,
            COREPerspective perspective, String roleName) {

        COREExternalLanguage language = (COREExternalLanguage) perspective.getLanguages().get(roleName);


        GenericFileBrowser.loadModel(language.getFileExtension(), new FileBrowserListener() {

            @Override
            public void modelLoaded(EObject model) {
                COREFeatureModel fM = (COREFeatureModel) view.getFeature().eContainer();
                COREConcern concern = (COREConcern) fM.eContainer();
                COREControllerFactory.INSTANCE.getFeatureController()
                    .addModelAndAssociate(concern, view.getFeature(), perspective,
                        roleName, model, "unnamed");
            }

            @Override
            public void modelSaved(File file) {
            }
        });
    }

    @Override
    public boolean processTapEvent(final TapEvent tapEvent) {
        if (tapEvent.isTapped()) {
            FeatureView featureView = (FeatureView) tapEvent.getTarget();
            // First parent is a ramRectangle component, the second is the feature diagram view
            FeatureDiagramEditView featureDiagramEditView =
                    (FeatureDiagramEditView) featureView.getParent().getParent();
            DisplayConcernEditScene scene = featureDiagramEditView.getScene();

            scene.handleClickedFeature(featureView);
        }

        return true;
    }

    @Override
    public boolean processDragEvent(DragEvent dragEvent) {

        DisplayConcernEditScene scene = RamApp.getActiveConcernEditScene();
        if (scene == null) {
            return true;
        }

        FeatureView featureIcon = (FeatureView) dragEvent.getTarget();

        switch (dragEvent.getId()) {
            case MTGestureEvent.GESTURE_STARTED:
            case MTGestureEvent.GESTURE_UPDATED:
                dragAction.processGestureEvent(dragEvent);
                for (MTComponent comp : featureIcon.getAllDragContainers()) {
                    dragEvent.setTarget(comp);
                    dragAction.processGestureEvent(dragEvent);
                }
                if (dragAction.wasDragPerformed()) {
                    updateFeatureColors(scene.getFeatureViews(), featureIcon);
                }
                break;
            case MTGestureEvent.GESTURE_ENDED:
                if (dragAction.wasDragPerformed()) {
                    setPositionUpdate(featureIcon, scene);
                    // Reset highlight
                    for (FeatureView fv : scene.getFeatureViews()) {
                        fv.highlight(false);
                    }
                }

                break;
        }
        return true;
    }

    /**
     * Display realizing scene(s) for the given feature.
     * If there is more than one, opens a selector with models.
     *
     * @param featureIcon - The {@link FeatureView} to display model for.
     * @param selectorLocation - Location to display the selector on screen.
     */
    private void handleShowModel(FeatureView featureIcon, Vector3D selectorLocation) {
        RamAbstractScene<?> scene = RamApp.getActiveScene();
        if (scene instanceof DisplayConcernEditScene) {

            DisplayConcernEditScene displayScene = (DisplayConcernEditScene) scene;
            displayScene.removeReuseSelectionIfNeeded();


            EList<COREScene> realizingScenes = featureIcon.getFeature().getRealizedBy();
            // filtering the realizing scenes based on the selected perspective, if any
            COREPerspective selectedPerspective = NavigationBar.getInstance().getCurrentPerspective();
            List<COREScene> perspectiveScenes = new ArrayList<COREScene>();
            for (COREScene s : realizingScenes) {
                if (selectedPerspective == null || s.getPerspectiveName().equals(selectedPerspective.getName())) {
                    perspectiveScenes.add(s);
                }
            }
            if (perspectiveScenes.size() == 1) {
                COREScene s = perspectiveScenes.iterator().next();
                COREExternalArtefact artefact;
                if (selectedPerspective != null && selectedPerspective.getDefault() != null) {
                    artefact = (COREExternalArtefact) s.getArtefacts().get(selectedPerspective.getDefault())
                            .get(0);
                } else {
                    artefact = (COREExternalArtefact) s.getArtefacts().values().iterator().next().get(0);
                }
                showModel(artefact, artefact.getRootModelElement());
            } else if (perspectiveScenes.size() > 1) {
                RamSelectorComponent<COREScene> selector;
                selector = new RamSelectorComponent<COREScene>(new SceneSelectorNamer());                
                selector.setElements(perspectiveScenes);
                selector.registerListener(new ModelSelectorListener());
                // Display selector
                scene.addComponent(selector, selectorLocation);
            }
        }
    }

    /**
     * Display all scene(s) except those already linked to the given feature.
     *
     * @param featureIcon - The {@link FeatureView} to display model for.
     * @param selectorLocation - Location to display the selector on screen.
     */
    private void handleShowScene(FeatureView featureIcon, Vector3D selectorLocation) {
        RamAbstractScene<?> scene = RamApp.getActiveScene();
        if (scene instanceof DisplayConcernEditScene) {
            DisplayConcernEditScene displayScene = (DisplayConcernEditScene) scene;
            ArrayList<COREScene> realizationScenes = new ArrayList<COREScene>(displayScene.getConcern().getScenes());
            realizationScenes.removeAll(featureIcon.getFeature().getRealizedBy());
            if (realizationScenes.size() > 0) {
                RamSelectorComponent<COREScene> selector;
                selector = new RamSelectorComponent<COREScene>(new SceneSelectorNamer());
                selector.setElements(realizationScenes);
                selector.registerListener(new SceneSelectorListener(featureIcon.getFeature()));
                // Display selector
                scene.addComponent(selector, selectorLocation);
            }
        }
    }

    /**
     * Handles the request to associate an existing scene to a feature, this creating a conflict resolution scene.
     *
     * @param scene the scene to add to the feature
     * @param feature the feature that will be realized by the scene
     */
    private static void handleAssociateConflictResolutionModel(COREScene scene, COREFeature feature) {
        FeatureController controller = COREControllerFactory.INSTANCE.getFeatureController();
        controller.associateSceneToFeature(scene, feature);
    }

    /**
     * Handles the request to create a new design model and associate it to a feature.
     *
     * @param concern the concern to add it to
     * @param feature the feature being realized by the design model
     * @param perspective the of the language {@link COREExternalLanguage}
     * @param roleName the name of the role the model plays in the perspective that should be created
     */
    private static void handleNewDesignModel(COREConcern concern, COREFeature feature, COREPerspective perspective,
            String roleName) {

        final EObject externalModel;

        NavigationBar.getInstance().setCurrentPerspective(perspective);
        //TODO: This needs to change if in the future perspectives can contain other perspectives!
        CORELanguage lang = perspective.getLanguages().get(roleName);
        switch (lang.getName()) {

            case LanguageConstant.RAM_LANGUAGE_NAME:
                externalModel = (Aspect) RAMModelUtil.createAspect(feature.getName(), concern);
                break;
            case LanguageConstant.DOMAIN_MODEL_LANGUAGE_NAME:
            case LanguageConstant.CLASS_DIAGRAM_LANGUAGE_NAME:
                externalModel = CdmModelUtil.createClassDiagram(feature.getName(), concern);
                break;
            case LanguageConstant.UC_LANGUAGE_NAME:
                externalModel = UcModelUtil.createUseCaseDiagram(feature.getName());
                break;
            case LanguageConstant.REST_TREE_LANGUAGE_NAME:
                externalModel = RifModelUtil.createRifTree(feature.getName(), concern);
                break;
            default:
                externalModel = null;
        }

        FeatureController controller = COREControllerFactory.INSTANCE.getFeatureController();
        controller.addModelAndAssociate(concern, feature, perspective, roleName, externalModel,
                feature.getName());
        COREExternalArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(externalModel);

        try {
            COREPerspectiveUtil.INSTANCE.saveModel(concern, roleName, artefact);
        } catch (IOException e) {
            RamApp.getActiveScene()
                .displayPopup(Strings.POPUP_ERROR_FILE_SAVE + e.getMessage(), PopupType.ERROR);
        }

        showModel(artefact, externalModel);


//        // TODO Check for that save
//        GenericFileBrowser.saveModel(externalModel, lang.getFileExtension(),
//            feature.getName(), new FileBrowserListener() {
//            @Override
//            public void modelSaved(File file) {
//
//                FeatureController controller = COREControllerFactory.INSTANCE.getFeatureController();
//                controller.addModelAndAssociate(concern, feature, perspective, externalModel, feature.getName());
//
//                showModel(COREArtefactUtil.getReferencingExternalArtefact(externalModel), externalModel);
//            }
//
//            @Override
//            public void modelLoaded(EObject model) {
//                // TODO Auto-generated method stub
//
//            }
//        });
    }

    /**
     * Highlight the underlying {@link FeatureView} when dragging a feature.
     *
     * @param set - The set of features to examine
     * @param feature - The dragged feature
     */
    private static void updateFeatureColors(Set<FeatureView> set, FeatureView feature) {
        // reset all highlights
        for (FeatureView fv : set) {
            fv.highlight(false);
        }
        // highlight underlying feature, if any
        FeatureView featureView = getUnderlyingFeature(set, feature);
        if (featureView != null) {
            featureView.highlight(true);
            feature.highlight(true);
        } else {
            // Check if we are to be moved
            FeatureView parentV = feature.getParentFeatureView();
            int newPosition = getFeaturePosition(feature);

            if (newPosition == -1) {
                return;
            }

            newPosition += COREPerspectiveUtil.INSTANCE.getReuses(parentV.getFeature(), null).size();
            int offset = feature.getCurrentPosition() - newPosition;
            int prevPosition;
            int nextPosition;
            // To the left
            if (offset > 0) {
                prevPosition = newPosition - 1;
                nextPosition = newPosition;
            } else {
                prevPosition = newPosition;
                nextPosition = newPosition + 1;
            }
            if (prevPosition >= 0 && prevPosition < parentV.getChildrenFeatureViews().size()) {
                FeatureView prevChild = parentV.getChildrenFeatureViews().get(prevPosition);
                if (!prevChild.isReuse()) {
                    prevChild.highlight(true);
                    parentV.highlight(true);
                }
            }
            if (nextPosition >= 0 && nextPosition < parentV.getChildrenFeatureViews().size()) {
                FeatureView nextChild = parentV.getChildrenFeatureViews().get(nextPosition);
                nextChild.highlight(true);
                parentV.highlight(true);
            }
        }
    }

    /**
     * Function used to translate the Feature when dragged.
     * In addition to translating the Feature,
     * all its children and the line associated with its parent has to be translated.
     *
     * @param position - The position after translation.
     * @param featureIcon - The Feature on which the drag gesture was called.
     */
    public void setNewTranslation(Vector3D position, FeatureView featureIcon) {
        // drag also the children
        for (MTComponent each : featureIcon.getAllDragContainers()) {
            each.translate(position);
        }
    }

    @Override
    public void setPositionUpdate(FeatureView featureIcon, AbstractConcernScene<?, ?> scene) {
        /*
         * First checks if a feature had to be added as a child of another feature
         * This is horizontal movement, where intersection adds it as a child.
         */
        FeatureView target = getUnderlyingFeature(scene.getFeatureViews(), featureIcon);
        if (target != null && target != featureIcon && !target.equals(featureIcon.getParentFeatureView())) {

            if (target.getChildrenRelationship() == COREFeatureRelationshipType.XOR
                    || target.getChildrenRelationship() == COREFeatureRelationshipType.OR) {
                COREControllerFactory.INSTANCE.getFeatureController().addExistingFeature(scene.getConcern(),
                        featureIcon.getFeature(), target.getFeature(), target.getChildrenRelationship());
            } else {
                COREFeatureRelationshipType rel = featureIcon.getFeature().getParentRelationship();
                if (rel == COREFeatureRelationshipType.MANDATORY || rel == COREFeatureRelationshipType.OPTIONAL) {
                    COREControllerFactory.INSTANCE.getFeatureController().addExistingFeature(scene.getConcern(),
                            featureIcon.getFeature(), target.getFeature(), rel);
                } else {
                    COREControllerFactory.INSTANCE.getFeatureController()
                        .addExistingFeature(scene.getConcern(),
                            featureIcon.getFeature(), target.getFeature(),
                            COREFeatureRelationshipType.OPTIONAL);
                }
            }
            return;
        }

        /* Check if we want to change the position of the feature */
        int newPosition = getFeaturePosition(featureIcon);
        if (newPosition != -1) {
            COREControllerFactory.INSTANCE.getFeatureController().changePositionOfFeature(scene.getConcern(),
                    featureIcon.getParentFeatureView().getFeature(), featureIcon.getFeature(), newPosition);
        } else {
            // Move the feature back to its original position
            resetFeatureLocation(featureIcon, scene);
        }
    }

    /**
     * Return the given {@link FeatureView} to its default location.
     *
     * @param feature - The feature to move
     * @param scene - The scene
     */
    private void resetFeatureLocation(FeatureView feature, AbstractConcernScene<?, ?> scene) {
        Vector3D initPosition = new Vector3D(feature.getXposition(), feature.getYposition());
        Vector3D currentPosition = feature.getPosition(TransformSpace.RELATIVE_TO_PARENT);
        Vector3D translation = initPosition.subtractLocal(currentPosition);
        feature.translate(translation);
        setNewTranslation(translation, feature);
    }

    /**
     * Return the current position of a feature related to its sibling on the screen.
     * This is used when a feature is dragged to find out were it should be placed in the end.
     *
     * @param feature - The {@link FeatureView}
     * @return the new position, or -1 if the position hasn't changed
     */
    private static int getFeaturePosition(FeatureView feature) {
        int initialPosition = feature.getCurrentPosition();
        // Get the siblings
        List<FeatureView> siblings = new ArrayList<FeatureView>();
        for (FeatureView fv : feature.getParentFeatureView().getChildrenFeatureViews()) {
            if (!fv.equals(feature) && !fv.isReuse()) {
                siblings.add(fv);
            } else if (fv.isReuse() && fv.getCurrentPosition() < feature.getCurrentPosition()) {
                // if there is a reuse, index should not take it in account
                initialPosition--;
            }
        }
        int newPosition = initialPosition;
        for (FeatureView sib : siblings) {
            if (feature.getCurrentPosition() < sib.getCurrentPosition()) {
                // The feature is originally located left of its sibling
                if (feature.getCenterPointGlobal().getX() > sib.getX() + sib.getWidth()) {
                    // the feature as now moved farther to the right of the sibling
                    newPosition++;
                }
            } else {
                // The feature is originally located right of its sibling
                if (feature.getCenterPointGlobal().getX() < sib.getX()) {
                    // the feature as now moved farther to the left of the sibling
                    newPosition--;
                }
            }
        }
        if (newPosition != initialPosition) {
            return newPosition;
        }
        return -1;
    }

    /**
     * Get the {@link FeatureView} lying under the given one. Used during drag events to check where the gesture ended.
     *
     * @param list - The list of {@link FeatureView}s to examine
     * @param feature - The dragged {@link FeatureView}
     * @return the feature the gesture ended on, or null if none
     */
    private static FeatureView getUnderlyingFeature(Set<FeatureView> list, FeatureView feature) {
        list.remove(feature);
        for (FeatureView fv : list) {
            if (fv.containsPointGlobal(feature.getCenterPointGlobal())) {
                return fv;
            }
        }
        return null;
    }

    /**
     * Function called to associate an aspect as RealizedBy for the Feature.
     *
     * @param concern - The concern containing the feature.
     * @param feature - The feature to be realized by the aspect
     * @param model - The {@link Aspect} to associate to the feature
     */
    public static void setAspect(COREConcern concern, COREFeature feature, COREArtefact model) {
        //TODO: figure out if this is still needed
    }

    /**
     * Add a child feature to the given feature.
     *
     * @param scene - the current scene
     * @param parentFeature - the feature to add a child to
     * @param position - the position to add the child at
     * @param relationshipType - relationship for the child
     */
    @Override
    public void addChild(DisplayConcernEditScene scene, COREFeature parentFeature, int position,
            COREFeatureRelationshipType relationshipType) {

        // parentFeature.eContainer() corresponds to the Feature Model
        parentFeature.eContainer().eAdapters().add(new EContentAdapter() {
            @Override
            public void notifyChanged(Notification notification) {
                EContentAdapter adapter = this;
                RamApp.getApplication().invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        if (notification.getFeature() == CorePackage.Literals.CORE_FEATURE_MODEL__FEATURES) {
                            if (notification.getEventType() == Notification.ADD) {
                                COREFeature coreFeature = (COREFeature) notification.getNewValue();
                                scene.getFeatureView(coreFeature).showKeyboard();
                                scene.getFeatureView(coreFeature).clearNameField();
                                parentFeature.eContainer().eAdapters().remove(adapter);
                            }
                        }
                    }
                });
            }
        });

        final String childName = COREModelUtil.createUniqueNameFromElements(Strings.DEFAULT_FEATURE_NAME,
                new HashSet<COREFeature>(scene.getConcern().getFeatureModel().getFeatures()));

        FeatureController controller = COREControllerFactory.INSTANCE.getFeatureController();

        controller.addFeature(scene.getConcern(), position, parentFeature, childName, relationshipType);

    }

    @Override
    public void renameFeature(FeatureView view) {
        if (view.getIsRoot()) {
            return;
        }
        view.showKeyboard();
    }

    @Override
    public void deleteFeature(DisplayConcernEditScene scene, FeatureView view) {
        if (!view.getIsRoot() && !view.isReuse()) {
            // Get the feature and its children
            List<COREFeature> featuresDeleted = view.collectFeatures();

            // Check if there are any model reuse left
            for (COREFeature feature : featuresDeleted) {
                Collection<COREScene> scenes = feature.getRealizedBy();

                for (COREScene s : scenes) {
                    ArrayList<COREArtefact> artefacts = COREArtefactUtil.getAllArtefactsOfScene(s);

                    for (COREArtefact a : artefacts) {
                        if (a.getModelReuses().size() > 0) {
                            scene.displayPopup(Strings.POPUP_CANT_DELETE_FEATURE);
                            return;
                        }

                    }
                }
            }

            FeatureController controller = COREControllerFactory.INSTANCE.getFeatureController();
            controller.deleteFeature(scene.getConcern(), featuresDeleted);
        }
    }

    @Override
    public void hideFeature(DisplayConcernEditScene scene, FeatureView view) {
        scene.switchCollapse(view);
    }

    /**
     * Display the scene for a model.
     *
     * @param artefact - The {@link COREArtefact} to display
     * @param model - The model to display
     */
    public static void showModel(COREExternalArtefact artefact, EObject model) {
        RamApp.getActiveScene().setTransition(new SlideTransition(RamApp.getApplication(), 700, true));
        RamApp.getApplication().loadScene(artefact, model);
    }

}