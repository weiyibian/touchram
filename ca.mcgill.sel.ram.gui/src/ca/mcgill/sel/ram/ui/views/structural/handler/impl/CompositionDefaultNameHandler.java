package ca.mcgill.sel.ram.ui.views.structural.handler.impl;

import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;
import org.mt4j.sceneManagement.transition.BlendTransition;

import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREModelReuse;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamSelectorComponent;
import ca.mcgill.sel.ram.ui.components.listeners.AbstractDefaultRamSelectorListener;
import ca.mcgill.sel.ram.ui.components.navigationbar.NavigationBar;
import ca.mcgill.sel.ram.ui.components.navigationbar.SlideUpDownTransition;
import ca.mcgill.sel.ram.ui.scenes.DisplayAspectScene;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.utils.WeaverRunner;
import ca.mcgill.sel.ram.ui.views.OptionSelectorView;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.HandlerFactory;
import ca.mcgill.sel.ram.ui.views.handler.IDisplaySceneHandler;
import ca.mcgill.sel.ram.ui.views.handler.impl.TextViewHandler;
import ca.mcgill.sel.ram.ui.views.structural.CompositionSplitEditingView;
import ca.mcgill.sel.ram.ui.views.structural.CompositionTitleView;
import ca.mcgill.sel.ram.ui.views.structural.CompositionView;
import ca.mcgill.sel.ram.ui.views.structural.handler.ICompositionNameHandler;

/**
 * The default handler for a model composition name which is represented by a {@link TextView} in the
 * {@link CompositionTitleView}.
 * 
 * @author eyildirim
 */
public class CompositionDefaultNameHandler extends TextViewHandler implements ICompositionNameHandler {

    /**
     * Enum with possible actions to do on model compositions.
     */
    private enum Actions {
        OPEN_DESIGN_MODEL, WEAVE_DESIGN_MODEL
    }

    @Override
    public boolean processTapAndHoldEvent(final TapAndHoldEvent tapAndHoldEvent) {
        if (tapAndHoldEvent.isHoldComplete()) {
            OptionSelectorView<Actions> selector = new OptionSelectorView<Actions>(Actions.values());
            ((DisplayAspectScene) RamApp.getActiveScene())
                    .addComponent(selector, tapAndHoldEvent.getLocationOnScreen());

            selector.registerListener(new AbstractDefaultRamSelectorListener<Actions>() {

                @Override
                public void elementSelected(RamSelectorComponent<Actions> selector, Actions element) {
                    final TextView target = (TextView) tapAndHoldEvent.getTarget();
                    if (!(target.getParent() instanceof CompositionTitleView)) {
                        return;
                    }
                    CompositionView view = (CompositionView) target.getParent().getParent();
                    COREModelComposition modelComposition = view.getModelComposition();

                    if (element == Actions.WEAVE_DESIGN_MODEL) {
                        weaveAspect(modelComposition);
                    } else if (element == Actions.OPEN_DESIGN_MODEL) {
                        openAspect(modelComposition);
                    }
                }
            });
        }

        return true;
    }

    /**
     * Open the aspect corresponding to the view.
     * 
     * @param modelComposition the model composition to display
     */
    protected void openAspect(COREModelComposition modelComposition) {
        // Get the handler of the display aspect scene to call its function for switching into composition edit mode.
        IDisplaySceneHandler handler = HandlerFactory.INSTANCE.getDisplayAspectSceneHandler();

        // Get the latest used display aspect before switching into composition edit mode
        DisplayAspectScene displayAspectScene = RamApp.getActiveAspectScene();

        Aspect source = (Aspect) ((COREExternalArtefact) modelComposition.getSource()).getRootModelElement();
        if (displayAspectScene != null && isSplitViewEnabled(displayAspectScene)) {
            // we are already in split composition editing mode, switch back to normal mode and then load aspect.
            handler.closeSplitView(displayAspectScene);
            RamApp.getApplication().loadScene((COREExternalArtefact) modelComposition.getSource(), source);
        } else {
            //loading external aspect specifically coming from a Reuse context and not an Extend
            if (modelComposition instanceof COREModelReuse) {
                displayAspectScene.setTransition(new BlendTransition(RamApp.getApplication(), 700));
                NavigationBar.getInstance().collapseStartingEnvironment(displayAspectScene.getAspect());
                RamApp.getApplication().loadScene((COREExternalArtefact) modelComposition.getSource(), source);
            } else {
                displayAspectScene.setTransition(new SlideUpDownTransition(RamApp.getApplication(), 700, false));
                RamApp.getApplication().loadScene((COREExternalArtefact) modelComposition.getSource(), source);
            }
        }
        return;
    }

    /**
     * Open the current aspect with the selected one.
     * 
     * @param modelComposition the model composition to display
     */
    private static void weaveAspect(COREModelComposition modelComposition) {
        // check if the split composition editing mode is enabled. If not we can do the weaving.
        boolean splitModeEnabled = isSplitViewEnabled(RamApp.getActiveAspectScene());

        if (!splitModeEnabled) {
            // Weave the tapped composition
            WeaverRunner.getInstance().weaveSingle(modelComposition);
        } else {
            RamApp.getActiveScene().displayPopup(Strings.POPUP_CANT_WEAVE_EDITMODE);
        }
    }

    /**
     * Returns whether split view mode is enabled.
     * 
     * @param scene the current scene
     * @return true, if split view mode is enabled, false otherwise
     */
    private static boolean isSplitViewEnabled(DisplayAspectScene scene) {
        return scene.getCurrentView() instanceof CompositionSplitEditingView;
    }

}
