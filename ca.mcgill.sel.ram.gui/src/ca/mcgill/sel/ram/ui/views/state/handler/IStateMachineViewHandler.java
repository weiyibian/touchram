package ca.mcgill.sel.ram.ui.views.state.handler;

import ca.mcgill.sel.ram.ui.events.listeners.IDragListener;
import ca.mcgill.sel.ram.ui.events.listeners.ITapAndHoldListener;
import ca.mcgill.sel.ram.ui.events.listeners.ITapListener;
import ca.mcgill.sel.ram.ui.views.state.StateComponentView;
import ca.mcgill.sel.ram.ui.views.state.StateMachineView;

/**
 * Interface for state machine view handlers.
 * 
 * @author abirayed
 */
public interface IStateMachineViewHandler extends IDragListener, ITapAndHoldListener, ITapListener {
    
    /**
     * Method that is invoked when a state is double tapped.
     * 
     * @param stateMachineView the state machine view
     * @param stateComponentView the state component view
     * @return whether or not the double tap was handled
     */
    boolean handleDoubleTapOnState(StateMachineView stateMachineView, StateComponentView stateComponentView);
    
}
