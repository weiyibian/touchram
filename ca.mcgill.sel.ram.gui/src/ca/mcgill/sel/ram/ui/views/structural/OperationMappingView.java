package ca.mcgill.sel.ram.ui.views.structural;

import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.ram.OperationMapping;
import ca.mcgill.sel.ram.ui.components.RamButton;
import ca.mcgill.sel.ram.ui.components.RamImageComponent;
import ca.mcgill.sel.ram.ui.components.RamRoundedRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent.Alignment;
import ca.mcgill.sel.ram.ui.events.listeners.ActionListener;
import ca.mcgill.sel.ram.ui.layouts.HorizontalLayoutVerticallyCentered;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Fonts;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.HandlerFactory;

/**
 * This view shows the operation mapping such as OperationA-->OperationB along with a delete button.
 * 
 * @author eyildirim
 */
public class OperationMappingView extends RamRoundedRectangleComponent implements ActionListener {

    /**
     * The action to delete the operation mapping.
     */
    protected static final String ACTION_OPERATION_MAPPING_DELETE = "view.operationMapping.delete";

    /**
     * The action to add the parameter mapping.
     */
    private static final String ACTION_PARAMETER_MAPPING_ADD = "view.parameterMapping.add";
    
    private static final float ICON_SIZE = Fonts.FONTSIZE_COMPOSITION + 2;

    private OperationMapping myOperationMapping;

    // button to delete Operation Mapping
    private RamButton buttonOperationMappingDelete;

    // Button to add Parameter Mapping.
    private RamButton buttonParameterMappingAdd;

    // OperationMapping from element
    private TextView textOperationMappingFromElement;

    // OperationMapping to element
    private TextView textOperationMappingToElement;

    // image for an arrow between mapping elements
    private RamImageComponent arrow;

    /**
     * Creates a new view for the given operation mapping.
     *
     * @param operationMapping - the {@link OperationMapping} this view should visualize
     */
    public OperationMappingView(OperationMapping operationMapping) {
        super(4);
        setNoStroke(true);
        setNoFill(true);
        setBuffers(0);
        
        myOperationMapping = operationMapping;

        // Add a button for deleting the operation mapping
        RamImageComponent deleteOperationMappingImage = new RamImageComponent(Icons.ICON_DELETE,
                Colors.ICON_DELETE_COLOR);
        deleteOperationMappingImage.setMinimumSize(Fonts.FONTSIZE_COMPOSITION + 2, Fonts.FONTSIZE_COMPOSITION + 2);
        deleteOperationMappingImage.setMaximumSize(Fonts.FONTSIZE_COMPOSITION + 2, Fonts.FONTSIZE_COMPOSITION + 2);
        buttonOperationMappingDelete = new RamButton(deleteOperationMappingImage);
        buttonOperationMappingDelete.setActionCommand(ACTION_OPERATION_MAPPING_DELETE);
        buttonOperationMappingDelete.addActionListener(this);
        addChild(buttonOperationMappingDelete);

        // Add "Op:" text
        RamTextComponent opText = new RamTextComponent(Strings.LABEL_OPERATION);
        opText.setFont(Fonts.FONT_COMPOSITION);
        opText.setFont(Fonts.FONT_COMPOSITION);
        opText.setBufferSize(Cardinal.SOUTH, 0);
        opText.setBufferSize(Cardinal.EAST, 0);
        // Sets the indentation
        opText.setBufferSize(Cardinal.WEST, Fonts.FONTSIZE_COMPOSITION);
        this.addChild(opText);

        // Add the operation From
        textOperationMappingFromElement = new TextView(myOperationMapping, CorePackage.Literals.CORE_LINK__FROM);
        textOperationMappingFromElement.setHandler(HandlerFactory.INSTANCE.getTextViewHandler());
        textOperationMappingFromElement.setFont(Fonts.FONT_COMPOSITION);
        textOperationMappingFromElement.setBufferSize(Cardinal.SOUTH, 0);
        textOperationMappingFromElement.setBufferSize(Cardinal.WEST, 0);
        textOperationMappingFromElement.setBufferSize(Cardinal.EAST, 0);
        textOperationMappingFromElement.setAlignment(Alignment.CENTER_ALIGN);
        textOperationMappingFromElement.setPlaceholderText(Strings.PH_SELECT_OPERATION);
        textOperationMappingFromElement.setAutoMinimizes(true);
        this.addChild(textOperationMappingFromElement);

        // Add arrow
        arrow = new RamImageComponent(Icons.ICON_ARROW_RIGHT, Colors.ICON_ARROW_COLOR);
        arrow.setMinimumSize(Fonts.FONTSIZE_COMPOSITION + 2, Fonts.FONTSIZE_COMPOSITION + 2);
        arrow.setMaximumSize(Fonts.FONTSIZE_COMPOSITION + 2, Fonts.FONTSIZE_COMPOSITION + 2);
        this.addChild(arrow);

        // Add the operation To
        textOperationMappingToElement = new TextView(myOperationMapping, CorePackage.Literals.CORE_LINK__TO);
        textOperationMappingToElement.setFont(Fonts.FONT_COMPOSITION);
        textOperationMappingToElement.setBufferSize(Cardinal.SOUTH, 0);
        textOperationMappingToElement.setBufferSize(Cardinal.WEST, 0);
        textOperationMappingToElement.setBufferSize(Cardinal.EAST, 0);
        textOperationMappingToElement.setAlignment(Alignment.CENTER_ALIGN);
        textOperationMappingToElement.setPlaceholderText(Strings.PH_SELECT_OPERATION);
        textOperationMappingToElement.setAutoMinimizes(true);
        textOperationMappingToElement.setHandler(HandlerFactory.INSTANCE.getOperationMappingToElementHandler());
        this.addChild(textOperationMappingToElement);
        
        // Add button to add a parameter mapping
        RamImageComponent parameterMappingAddImage = new RamImageComponent(Icons.ICON_PARAMETER_MAPPING_ADD,
                Colors.ICON_STRUCT_DEFAULT_COLOR);
        parameterMappingAddImage.setMinimumSize(ICON_SIZE, ICON_SIZE);
        parameterMappingAddImage.setMaximumSize(ICON_SIZE, ICON_SIZE);
        buttonParameterMappingAdd = new RamButton(parameterMappingAddImage);
        buttonParameterMappingAdd.setActionCommand(ACTION_PARAMETER_MAPPING_ADD);
        buttonParameterMappingAdd.addActionListener(this);
        addChild(buttonParameterMappingAdd);

        setLayout(new HorizontalLayoutVerticallyCentered(Fonts.FONTSIZE_COMPOSITION / 5));
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        String actionCommand = event.getActionCommand();

        if (ACTION_OPERATION_MAPPING_DELETE.equals(actionCommand)) {
            HandlerFactory.INSTANCE.getMappingContainerViewHandler().deleteOperationMapping(myOperationMapping);
        } else if (ACTION_PARAMETER_MAPPING_ADD.equals(actionCommand)) {
            HandlerFactory.INSTANCE.getMappingContainerViewHandler().addParameterMapping(myOperationMapping);
        }
    }

    /**
     * Getter for the Operation Mapping.
     * 
     * @return {@link OperationMapping}
     */
    public OperationMapping getOperationMapping() {
        return myOperationMapping;
    }
}
