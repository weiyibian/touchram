package ca.mcgill.sel.ram.ui.views.structural.handler.impl;

import java.io.File;

import org.eclipse.emf.ecore.EObject;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;

import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.ConfirmPopup;
import ca.mcgill.sel.ram.ui.components.ConfirmPopup.OptionType;
import ca.mcgill.sel.ram.ui.components.ConfirmPopup.SelectionListener;
import ca.mcgill.sel.ram.ui.components.browser.interfaces.FileBrowserListener;
import ca.mcgill.sel.ram.ui.scenes.DisplayAspectScene;
import ca.mcgill.sel.ram.ui.utils.BasicActionsUtils;

/**
 * The default handler for a model extension name of an extended aspect.
 * 
 * @author eyildirim
 */
public class CompositionExtendNameHandler extends CompositionDefaultNameHandler {

    @Override
    public boolean processTapEvent(TapEvent tapEvent) {
        // do nothing
        return true;
    }

    @Override
    protected void openAspect(final COREModelComposition composition) {
        final DisplayAspectScene scene = RamApp.getActiveAspectScene();

        // If the aspect is new and has not been saved we have to save it first.
        if (scene != null && scene.getAspect().eResource() == null) {
            SelectionListener selectionListener = new ConfirmPopup.SelectionListener() {
                @Override
                public void optionSelected(int selectedOption) {
                    if (selectedOption == ConfirmPopup.YES_OPTION) {
                        BasicActionsUtils.saveModel(scene.getAspect(), new FileBrowserListener() {
                            @Override
                            public void modelSaved(File file) {
                                CompositionExtendNameHandler.super.openAspect(composition);
                            }
                            @Override
                            public void modelLoaded(EObject model) {
                            }
                        });
                    }
                }
            };
            scene.showCloseConfirmPopup(scene, selectionListener, OptionType.YES_CANCEL);
        } else {
            super.openAspect(composition);
        }
    }
}
