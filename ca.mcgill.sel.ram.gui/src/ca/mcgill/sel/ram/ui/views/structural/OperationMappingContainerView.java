package ca.mcgill.sel.ram.ui.views.structural;

import org.eclipse.emf.common.notify.Notification;
import org.mt4j.components.MTComponent;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.ram.OperationMapping;
import ca.mcgill.sel.ram.ParameterMapping;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamRoundedRectangleComponent;
import ca.mcgill.sel.ram.ui.components.listeners.UINotifyChangedListener;
import ca.mcgill.sel.ram.ui.layouts.VerticalLayout;
import ca.mcgill.sel.ram.ui.utils.Colors;

/**
 * This view contains an OperationMappingView and all mapping related container views for an operation mapping.
 * 
 * @author arthurls
 */
public class OperationMappingContainerView extends RamRoundedRectangleComponent implements UINotifyChangedListener {
    
    // It is useful to keep the child as a reference for hiding/showing purposes
    private OperationMappingView myOperationMappingView;

    // Operation Mapping information
    private OperationMapping myOperationMapping;

    // All the parameter mapping related views will be in this view:
    private RamRectangleComponent hideableParameterContainer;
    
    /**
     * Creates a new mapping container view.
     * 
     * @param operationMapping {@link OperationMapping} which we want to create an
     *                                      {@link OperationMappingContainerView} for.
     */
    public OperationMappingContainerView(OperationMapping operationMapping) {
        super(0);
        setNoStroke(false);
        setStrokeWeight(3);
        setNoFill(false);
        setFillColor(Colors.MAPPING_CONTAINER_VIEW_FILL_COLOR);

        myOperationMappingView = new OperationMappingView(operationMapping);
        this.addChild(myOperationMappingView);

        myOperationMapping = operationMapping;

        // Container for all the Parameters
        hideableParameterContainer = new RamRectangleComponent();
        hideableParameterContainer.setLayout(new VerticalLayout(0));
        hideableParameterContainer.setFillColor(Colors.ATTRIBUTE_MAPPING_VIEW_FILL_COLOR);
        hideableParameterContainer.setNoFill(false);
        hideableParameterContainer.setBufferSize(Cardinal.EAST, 0);
        
        this.addChild(hideableParameterContainer);

        setLayout(new VerticalLayout());

        setBuffers(0);
        
        // Adds all the parameter mappings related to this operation mapping.
        addAllParameterMappings();

        EMFEditUtil.addListenerFor(myOperationMapping, this);
    }

    /**
     * Adds all the parameters mappings which belongs to the operation mapping.
     */
    private void addAllParameterMappings() {
        for (ParameterMapping parameterMapping : myOperationMapping.getParameterMappings()) {
            addParameterMappingView(parameterMapping);
        }
    }
    
    /**
     * Adds a {@link ParameterMappingView} to the mapping container view.
     * 
     * @param newParameterMapping the {@link ParameterMapping} to add a view for
     */
    private void addParameterMappingView(ParameterMapping newParameterMapping) {
        ParameterMappingView parameterMappingView = new ParameterMappingView(newParameterMapping);
        hideableParameterContainer.addChild(parameterMappingView);
    }
    
    /**
     * Deletes a {@link ParameterMappingView} from the mapping container view.
     * 
     * @param deletedParameterMapping the {@link ParameterMapping} to remove the view for
     */
    private void deleteParameterMappingView(ParameterMapping deletedParameterMapping) {
        MTComponent[] parameterMappingViews = hideableParameterContainer.getChildren();
        for (MTComponent view : parameterMappingViews) {
            if (view instanceof ParameterMappingView) {
                ParameterMappingView parameterMappingView = (ParameterMappingView) view;
                if (parameterMappingView.getParameterMapping() == deletedParameterMapping) {
                    hideableParameterContainer.removeChild(parameterMappingView);
                }
            }
        }
    }

    @Override
    public void destroy() {
        EMFEditUtil.removeListenerFor(myOperationMapping, this);
        super.destroy();
    }

    /**
     * Getter for OperationMapping information of the view.
     * 
     * @return {@link OperationMapping}
     */
    public OperationMapping getOperationMapping() {
        return myOperationMapping;
    }

    @Override
    public void handleNotification(Notification notification) {
        if (notification.getNotifier() == myOperationMapping) {
            if (notification.getFeature() == CorePackage.Literals.CORE_MAPPING__MAPPINGS) {
                switch (notification.getEventType()) {
                    case Notification.ADD:
                        ParameterMapping newParameterMapping = (ParameterMapping) notification.getNewValue();
                        addParameterMappingView(newParameterMapping);
                        break;

                    case Notification.REMOVE:
                        ParameterMapping oldParameterMapping = (ParameterMapping) notification.getOldValue();
                        deleteParameterMappingView(oldParameterMapping);
                        break;
                }
            }
            
        }
    }
}
