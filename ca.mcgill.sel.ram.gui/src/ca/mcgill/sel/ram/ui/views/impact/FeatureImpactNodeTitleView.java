package ca.mcgill.sel.ram.ui.views.impact;

import ca.mcgill.sel.core.COREImpactNode;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.ram.ui.views.TextView;

/**
 * This view describes the first line of a FeatureImpactNodeView.
 *
 * @author Romain
 *
 */
public class FeatureImpactNodeTitleView extends AbstractFeatureImpactNodeLineView {

    private TextView name;

    /**
     * Create a new {@link TextView} for the name.
     *
     * @param container the container object.
     */
    public void createNameTextView(COREImpactNode container) {
        this.removeNameTextView();

        this.name = new TextView(container, CorePackage.Literals.CORE_NAMED_ELEMENT__NAME);

        this.addChild(this.name);
    }

    /**
     * Create a new {@link TextView} for the weight.
     *
     * @param node the {@link COREImpactNode} that contains the weight.
     */
    public void createWeightTextView(COREImpactNode node) {
        this.removeWeightTextView();

        this.weight = new TextView(node, CorePackage.Literals.CORE_FEATURE_IMPACT_NODE__RELATIVE_FEATURE_WEIGHT);

        if (handler != null) {
            this.weight.registerTapProcessor(handler);
        }

        this.addChild(0, this.weight);
    }

    /**
     * Set a {@link TextView} for the name.
     *
     * @param textView the textView to set
     */
    public void setNameTextView(TextView textView) {
        this.removeNameTextView();

        this.name = textView;

        this.addChild(this.name);
    }

    /**
     * Remove {@link TextView} for the name.
     */
    @Override
    public void removeNameTextView() {
        if (name != null && this.containsChild(name)) {
            this.removeChild(name);
            name = null;
        }
    }

    /**
     * Retrieve {@link TextView} for the name.
     *
     * @return the first {@link TextView}.
     */
    @Override
    public TextView getNameTextView() {
        return this.name;
    }
}
