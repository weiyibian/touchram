package ca.mcgill.sel.ram.ui.views.structural;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREModelExtension;
import ca.mcgill.sel.core.COREModelReuse;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.ram.ui.components.RamButton;
import ca.mcgill.sel.ram.ui.components.RamExpendableComponent;
import ca.mcgill.sel.ram.ui.components.RamImageComponent;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.components.listeners.UINotifyChangedListener;
import ca.mcgill.sel.ram.ui.events.listeners.ActionListener;
import ca.mcgill.sel.ram.ui.layouts.HorizontalLayout;
import ca.mcgill.sel.ram.ui.layouts.HorizontalLayoutVerticallyCentered;
import ca.mcgill.sel.ram.ui.layouts.VerticalLayout;
import ca.mcgill.sel.ram.ui.scenes.DisplayAspectScene;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Fonts;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.views.structural.handler.ICompositionContainerViewHandler;

/**
 * An {@link CompositionContainerView} is a {@link RamExpendableComponent} which shows all the
 * model compositions that a {@link COREArtefact} has. By using this container it is possible to see the details of a
 * {@link COREModelComposition}.
 *
 * @author eyildirim
 * @author oalam
 * @author joerg
 */
public class CompositionContainerView extends RamExpendableComponent implements
        UINotifyChangedListener, ActionListener {

    private static final String ACTION_COMPOSITION_ADD = "view.composition.add";
    private static final String ACTION_TAILOR_EXISTING_REUSE = "view.tailor.existing.reuse";

    private static final float ICON_SIZE = Icons.ICON_ADD.width - 8;

    // The artefact which has all the model compositions
    private COREArtefact artefact;

    // button for adding compositions
    private RamButton buttonPlus;

    // A container located at the top of the composition container which contains composition adding buttons for
    // classifier mappings and enum mappings. More
    // buttons can be added in the future.
    private final RamRectangleComponent buttonContainer;
    private final RamRectangleComponent titleContainer;
    private final RamRectangleComponent compositionsContainer;

    private Map<COREModelComposition, CompositionView> compositionViews;
    private boolean isModelReuseContainer;

    private ICompositionContainerViewHandler handler;

    /**
     * Constructor.
     *
     * @param displayAspectScene is the scene which has this compositionContainerView.
     * @param isModelReuseContainer is a boolean to indicate whether the CompositionsContainerView is called to show
     *            the modelreuses.
     * @param title - the title for the view
     */
    public CompositionContainerView(DisplayAspectScene displayAspectScene, boolean isModelReuseContainer,
            String title) {
        super(0, ICON_SIZE);

        this.isModelReuseContainer = isModelReuseContainer;
        artefact = displayAspectScene.getArtefact();

        setNoFill(true);
        setTitleNoFill(true);
        setTitleNoStroke(true);
        setEnabled(true);
        setNoStroke(true);

        EMFEditUtil.addListenerFor(artefact, this);

        /* Build title */
        titleContainer = new RamRectangleComponent(new HorizontalLayoutVerticallyCentered());

        RamTextComponent titleText = new RamTextComponent(title);
        titleText.setFont(Fonts.INST_CONTAINER_TITLE_FONT);
        // create a row of container for the buttons and add it to the top of the menu
        buttonContainer = new RamRectangleComponent();
        buttonContainer.setLayout(new HorizontalLayout());
        initButtons();
        titleContainer.addChild(buttonContainer);
        titleContainer.addChild(titleText);
        setTitle(titleContainer);

        /* Build content */
        compositionsContainer = new RamRectangleComponent(new VerticalLayout());

        // keep a map of composition views. We used Linked hash map because we want to keep the orders that the
        // elements are added.
        compositionViews = Collections.synchronizedMap(new LinkedHashMap<COREModelComposition, CompositionView>());

        // create all composition views and add them to the container
        if (isModelReuseContainer) {
            addAllModelReuseViews();
        } else {
            addAllExtensionViews();
        }

        setHideableComponent(compositionsContainer);
        // Expanded by default
        showHideableComponent();
    }

    /**
     * Set handler for this {@link CompositionContainerView}.
     *
     * @param handler - the {@link ICompositionContainerViewHandler} to set.
     */
    public void setHandler(ICompositionContainerViewHandler handler) {
        this.handler = handler;
    }

    /**
     * Get handler for this {@link CompositionContainerView}.
     *
     * @return the {@link ICompositionContainerViewHandler} to for this view.
     */
    public ICompositionContainerViewHandler getHandler() {
        return handler;
    }

    /**
     * Getter for isModelReuseContainer.
     *
     * @return isModelReuseContainer - whether this container is to show model reuses.
     */
    public boolean isModelReuseContainer() {
        return isModelReuseContainer;
    }

    /**
     * Adds the plus button.
     */
    public void initButtons() {
        buttonContainer.removeAllChildren();
        // add plus button to the top row button container
        RamImageComponent addImage = new RamImageComponent(Icons.ICON_ADD, Colors.ICON_STRUCT_DEFAULT_COLOR,
                ICON_SIZE, ICON_SIZE);

        buttonPlus = new RamButton(addImage);
        buttonPlus.setActionCommand(ACTION_COMPOSITION_ADD);
        buttonPlus.addActionListener(this);
        buttonContainer.addChild(buttonPlus);
        
        // add a button to add an existing reuse
        RamImageComponent rImage = 
                new RamImageComponent(Icons.ICON_TAILOR_REUSE, Colors.ICON_STRUCT_DEFAULT_COLOR, ICON_SIZE, ICON_SIZE);

        RamButton buttonAddExistingReuse = new RamButton(rImage);
        buttonAddExistingReuse.setActionCommand(ACTION_TAILOR_EXISTING_REUSE);
        buttonAddExistingReuse.addActionListener(this);
        buttonContainer.addChild(buttonAddExistingReuse);
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        String actionCommand = event.getActionCommand();

        if (ACTION_COMPOSITION_ADD.equals(actionCommand)) {
            //TODO: JK: I uncommented the following two lines. I think they are not needed anymore
            // but this needs to be verified :)
            // if (isModelReuseContainer && artefact.getScene() != null) {
            //    RamApp.getActiveScene().displayPopup(Strings.popupAspectNeedsRealize(artefact.getName()),
            //            PopupType.ERROR);
            // } else {
            getHandler().loadBrowser(artefact);
            // }
        } else if (ACTION_TAILOR_EXISTING_REUSE.equals(actionCommand)) {            
            getHandler().tailorExistingReuse(artefact);
            
        } else {
            super.actionPerformed(event);
        }
    }

    /**
     * Adds all extension views.
     */
    private void addAllExtensionViews() {
        for (COREModelExtension extension : artefact.getModelExtensions()) {
            createCompositionView(extension, null);
        }
    }

    /**
     * Adds all ModelReuseViews.
     */
    private void addAllModelReuseViews() {
        for (COREModelReuse modelReuse : artefact.getModelReuses()) {
            if (!isAssociationReuse(modelReuse)) {
                createCompositionView(modelReuse, modelReuse.getReuse());
            }
        }
    }

    /**
     * Creates a composition view and put composition data into the map along with its mapping view. It also adds
     * itself as a child.
     *
     * @param composition composition to be created
     * @return composition just created.
     */
    private CompositionView createCompositionView(COREModelComposition composition) {
        return createCompositionView(composition, null);
    }

    /**
     * Creates a composition view and puts composition data into the map along with its mapping view.
     * It also adds itself as a child.
     *
     * @param composition the composition to create a view for
     * @param reuse the reuse the composition is for
     * @return the composition view that was created
     */
    private CompositionView createCompositionView(COREModelComposition composition, COREReuse reuse) {
        CompositionView view = new CompositionView(composition, reuse, this);
        compositionViews.put(composition, view);
        compositionsContainer.addChild(view);
        return view;
    }

    /**
     * Deletes a composition view.
     *
     * @param modelComposition is the model composition to be deleted.
     */
    private void deleteCompositionView(COREModelComposition modelComposition) {
        CompositionView view = compositionViews.get(modelComposition);
        compositionViews.remove(modelComposition);
        compositionsContainer.removeChild(view);
        view.destroy();
    }

    @Override
    public void destroy() {
        EMFEditUtil.removeListenerFor(artefact, this);
        super.destroy();
    }

    /**
     * Gets the button container of the view.
     *
     * @return buttonContainer
     */
    public RamRectangleComponent getButtonContainer() {
        return buttonContainer;
    }

    /**
     * Gets the map for the composition views.
     *
     * @return the map for all Compositions to InstantiatoinViews.
     */
    public Map<COREModelComposition, CompositionView> getCompositionViews() {
        return compositionViews;
    }

    /**
     * Gets the plus button.
     *
     * @return plus button
     */
    public RamButton getPlusButton() {
        return buttonPlus;
    }

    /**
     * Gets the visible biggest container of the {@link CompositionContainerView}.
     *
     * @return biggest container of the {@link CompositionContainerView}.
     */
    public RamRectangleComponent getCompositionsContainer() {
        return compositionsContainer;
    }

    @Override
    public void handleNotification(Notification notification) {

        if (notification.getNotifier() == artefact) {

            if (notification.getFeature() == CorePackage.Literals.CORE_ARTEFACT__MODEL_REUSES
                    && isModelReuseContainer) {
                switch (notification.getEventType()) {
                    case Notification.ADD:
                        COREModelReuse modelReuseToAdd = (COREModelReuse) notification.getNewValue();
                        if (!isAssociationReuse(modelReuseToAdd)) {
                            createCompositionView(modelReuseToAdd, modelReuseToAdd.getReuse());
                        }
                        break;
                    case Notification.REMOVE:
                        COREModelReuse modelReuseToRemove = (COREModelReuse) notification.getOldValue();
                        // TODO: arthur -> check if isAssociationReuse usefull
                        //if (!isAssociationReuse(modelReuseToRemove)) {
                        deleteCompositionView(modelReuseToRemove);
                        //}
                        break;
                }
            } else if (notification.getFeature() == CorePackage.Literals.CORE_ARTEFACT__MODEL_EXTENSIONS
                    && !isModelReuseContainer) {
                switch (notification.getEventType()) {
                    case Notification.ADD:
                        COREModelExtension compositionNew = (COREModelExtension) notification.getNewValue();
                        createCompositionView(compositionNew);
                        break;

                    case Notification.REMOVE:
                        COREModelExtension compositionOld = (COREModelExtension) notification.getOldValue();
                        deleteCompositionView(compositionOld);
                        break;
                }
            }
        }
    }

    /**
     * Returns whether if the model reuse is a reuse of the association concern.
     *
     * @param modelReuse The model reuse to check for.
     * @return true if the model reuse is a reuse of the association concern.
     */
    private static boolean isAssociationReuse(COREModelReuse modelReuse) {
        //return Constants.ASSOCIATION_CONCERN_NAME.equals(modelReuse.getReuse().getReusedConcern().getName());
        return false;
    }
}
