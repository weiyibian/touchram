package ca.mcgill.sel.ram.ui.views.structural.handler.impl;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.mt4j.components.MTComponent;

import ca.mcgill.sel.core.CORECIElement;
import ca.mcgill.sel.core.COREPartialityType;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.ram.Parameter;
import ca.mcgill.sel.ram.RAMPartialityType;
import ca.mcgill.sel.ram.controller.ClassController;
import ca.mcgill.sel.ram.controller.ControllerFactory;
import ca.mcgill.sel.ram.ui.components.RamKeyboard;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent.Cardinal;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.components.listeners.DefaultRamKeyboardListener;
import ca.mcgill.sel.ram.ui.components.menu.RamLinkedMenu;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.handler.BaseHandler;
import ca.mcgill.sel.ram.ui.views.handler.ILinkedMenuListener;
import ca.mcgill.sel.ram.ui.views.structural.OperationView;
import ca.mcgill.sel.ram.ui.views.structural.ParameterView;
import ca.mcgill.sel.ram.ui.views.structural.handler.IParameterViewHandler;

/**
 * The default handler for {@link ParameterView}.
 * 
 * @author g.Nicolas
 *
 */
public class ParameterViewHandler extends BaseHandler implements IParameterViewHandler, ILinkedMenuListener {

    private static final String ACTION_PARAMETER_REMOVE = "view.parameter.remove";
    private static final String ACTION_PARAMETER_ADD_BEFORE = "view.parameter.add.before";
    private static final String ACTION_PARAMETER_ADD_AFTER = "view.parameter.add.after";
    
    private static final String ACTION_PARAMETER_CONCERN_PARTIAL = "view.parameter.partial.concern";
    private static final String ACTION_PARAMETER_PUBLIC_PARTIAL = "view.parameter.partial.public";
    private static final String ACTION_PARAMETER_NOT_PARTIAL = "view.parameter.partial.none";
    private static final String ACTION_PARAMETER_CARDINALITY = "view.parameter.cardinality";


    private static final String SUBMENU_ADD_PARAMETER = "sub.parameter.add";
    private static final String SUBMENU_PARTIALITY = "sub.partiality";
    
    @Override
    public void removeParameter(ParameterView parameterView) {
        ControllerFactory.INSTANCE.getClassController().removeParameter(parameterView.getParameter());
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        String actionCommand = event.getActionCommand();
        RamRectangleComponent pressedButton = (RamRectangleComponent) event.getTarget();
        RamLinkedMenu linkedMenu = (RamLinkedMenu) pressedButton.getParentOfType(RamLinkedMenu.class);
        if (linkedMenu != null) {

            ParameterView parameterView = (ParameterView) linkedMenu.getLinkedView();
            if (actionCommand.equals(ACTION_PARAMETER_REMOVE)) {
                removeParameter(parameterView);
            } else if (actionCommand.equals(ACTION_PARAMETER_ADD_BEFORE)) {
                addParameterBefore(parameterView);
            } else if (actionCommand.equals(ACTION_PARAMETER_ADD_AFTER)) {
                addParameterAfter(parameterView);
            } else if (ACTION_PARAMETER_CONCERN_PARTIAL.equals(actionCommand)) {
                switchPartiality(parameterView, COREPartialityType.CONCERN);
            } else if (ACTION_PARAMETER_PUBLIC_PARTIAL.equals(actionCommand)) {
                switchPartiality(parameterView, COREPartialityType.PUBLIC);
            } else if (ACTION_PARAMETER_NOT_PARTIAL.equals(actionCommand)) {
                switchPartiality(parameterView, COREPartialityType.NONE);
            }
        }
    }

    @Override
    public EObject getEobject(RamRectangleComponent rectangle) {
        return ((ParameterView) rectangle).getParameter();
    }

    @Override
    public void initMenu(RamLinkedMenu menu) {
        menu.addAction(Strings.MENU_DELETE, Icons.ICON_MENU_TRASH, ACTION_PARAMETER_REMOVE, this, true);

        menu.addSubMenu(1, SUBMENU_ADD_PARAMETER);
        menu.addAction(Strings.MENU_PARAMETER_ADD_BEFORE, Icons.ICON_MENU_ADD_PARAMETER_BEFORE,
                ACTION_PARAMETER_ADD_BEFORE, this, true);
        menu.addAction(Strings.MENU_PARAMETER_ADD_AFTER, Icons.ICON_MENU_ADD_PARAMETER_AFTER,
                ACTION_PARAMETER_ADD_AFTER, this, true);
        
        // sub menu for partiality
        menu.addSubMenu(1, SUBMENU_PARTIALITY);
        menu.addAction(Strings.MENU_PUBLIC_PARTIAL, Icons.ICON_MENU_PUBLIC_PARTIAL, ACTION_PARAMETER_PUBLIC_PARTIAL,
                this, SUBMENU_PARTIALITY, true);
        menu.addAction(Strings.MENU_CONCERN_PARTIAL, Icons.ICON_MENU_CONCERN_PARTIAL, ACTION_PARAMETER_CONCERN_PARTIAL,
                this, SUBMENU_PARTIALITY, true);
        menu.addAction(Strings.MENU_NO_PARTIAL, Icons.ICON_MENU_NOT_PARTIAL, ACTION_PARAMETER_NOT_PARTIAL,
                this, SUBMENU_PARTIALITY, true);
        
        updatePartialityButtons(menu);
    }

    @Override
    public List<EObject> getEObjectToListenForUpdateMenu(RamRectangleComponent rectangle) {
        ParameterView parameter = (ParameterView) rectangle;
        ArrayList<EObject> ret = new ArrayList<EObject>();
        ret.add(parameter.getParameter().eContainer());
        ret.add(parameter.getParameter());
        return ret;
    }

    @Override
    public void updateMenu(RamLinkedMenu menu, Notification notification) {
        if (notification.getEventType() == Notification.SET || notification.getEventType() == Notification.UNSET) {
            Parameter parameter = (Parameter) menu.geteObject();

            CORECIElement ciElement = COREArtefactUtil.getCIElementFor(parameter);
            
            if (ciElement != null) {
                menu.enableAction(ciElement.getPartiality() == COREPartialityType.NONE, ACTION_PARAMETER_CARDINALITY);
                menu.toggleAction(true, ACTION_PARAMETER_CARDINALITY);
            } else {
                menu.enableAction(true, ACTION_PARAMETER_CARDINALITY);
                menu.toggleAction(false, ACTION_PARAMETER_CARDINALITY);
            }

            if (notification.getFeature() == CorePackage.Literals.CORECI_ELEMENT__PARTIALITY) {
                updatePartialityButtons(menu);
            }
        }
    }

    @Override
    public RamRectangleComponent getVisualLinkedComponent(RamRectangleComponent link) {
        ParameterView parameterView = (ParameterView) link;
        return parameterView.getNameField();
    }

    @Override
    public void addParameterBefore(ParameterView parameterView) {
        createParameterAroundParameter(true, parameterView);
    }

    @Override
    public void addParameterAfter(ParameterView parameterView) {
        createParameterAroundParameter(false, parameterView);
    }
    

    /**
     * Add a parameter around another parameter. It can be on the left or on the right.
     * It add the placeholder in the view and in the model
     *
     * @param left - true if the parameter has to be on the left of the given parameter.
     * @param parameterView - the related {@link ParameterView}
     */
    private static void createParameterAroundParameter(boolean left, final ParameterView parameterView) {
        final OperationView operationView = (OperationView) parameterView.getParent();
        final RamTextComponent textRow = new RamTextComponent();
        textRow.setBufferSize(Cardinal.SOUTH, OperationView.BUFFER_BOTTOM);
        textRow.setPlaceholderText(Strings.PH_PARAM);

        int parameterViewIndex = operationView.getChildIndexOf(parameterView);
        int visualIndex = left ? parameterViewIndex : parameterViewIndex + 1;
        operationView.addChild(visualIndex, textRow);

        // visual index for delimiter; after if it is the first, before otherwise
        final int delimiterIndex = !left ? visualIndex : visualIndex + 1;
        operationView.addDelimiter(delimiterIndex);
        final MTComponent delimiter = operationView.getChildByIndex(delimiterIndex);

        RamKeyboard keyboard = new RamKeyboard();

        keyboard.registerListener(new DefaultRamKeyboardListener() {
            @Override
            public void keyboardCancelled() {
                operationView.removeChild(textRow);
                operationView.removeChild(delimiter);
            }

            @Override
            public boolean verifyKeyboardDismissed() {
                try {
                    int currentModelIndex =
                            operationView.getOperation().getParameters().indexOf(parameterView.getParameter());
                    int modelIndex = left ? currentModelIndex : currentModelIndex + 1;
                    ControllerFactory.INSTANCE.getClassController()
                        .createParameter(operationView.getOperation(), modelIndex, textRow.getText());
                } catch (final IllegalArgumentException e) {
                    return false;
                }

                operationView.removeChild(delimiter);
                operationView.removeChild(textRow);

                return true;
            }
        });

        textRow.showKeyboard(keyboard);
    }
    
    /**
     * Updates buttons for partiality inside the menu.
     * 
     * @param menu - the menu which contains the static button.
     */
    private static void updatePartialityButtons(RamLinkedMenu menu) {
        Parameter parameter = (Parameter) menu.geteObject();
        
        COREPartialityType partiality = COREArtefactUtil.getPartialityFor(parameter);
        menu.enableAction(partiality == COREPartialityType.NONE, ACTION_PARAMETER_NOT_PARTIAL);
        menu.enableAction(partiality == COREPartialityType.CONCERN, ACTION_PARAMETER_CONCERN_PARTIAL);
        menu.enableAction(partiality == COREPartialityType.PUBLIC, ACTION_PARAMETER_PUBLIC_PARTIAL);
    }

    @Override
    public void switchPartiality(ParameterView parameter, COREPartialityType type) {
        ClassController controller = ControllerFactory.INSTANCE.getClassController();
        if (type == COREPartialityType.PUBLIC) {
            controller.changeMappableElementPartiality(parameter.getParameter(), RAMPartialityType.PUBLIC);
        } else if (type == COREPartialityType.CONCERN) {
            controller.changeMappableElementPartiality(parameter.getParameter(), RAMPartialityType.PROTECTED);
        } else {
            controller.changeMappableElementPartiality(parameter.getParameter(), RAMPartialityType.NONE);            
        }
    }

}
