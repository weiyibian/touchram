package ca.mcgill.sel.ram.ui.views.structural;

import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.ram.ParameterMapping;
import ca.mcgill.sel.ram.ui.components.RamButton;
import ca.mcgill.sel.ram.ui.components.RamImageComponent;
import ca.mcgill.sel.ram.ui.components.RamRoundedRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent.Alignment;
import ca.mcgill.sel.ram.ui.events.listeners.ActionListener;
import ca.mcgill.sel.ram.ui.layouts.HorizontalLayoutVerticallyCentered;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Fonts;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.HandlerFactory;

/**
 * This view shows the parameter mapping such as Param: paramFrom-->paramTo along with a delete button.
 * 
 * @author arthurls
 */
public class ParameterMappingView extends RamRoundedRectangleComponent implements ActionListener {

    /**
     * The action to delete the parameter mapping.
     */
    protected static final String ACTION_PARAMETER_MAPPING_DELETE = "view.parameterMapping.delete";

    private ParameterMapping myParameterMapping;
    
    private RamButton buttonParameterMappingDelete;
    
    private TextView textParameternMappingToElement;
    
    // image for an arrow between mapping elements
    private RamImageComponent arrow;

    /**
     * Creates a new view for the given parameter mapping.
     *
     * @param parameterMapping - the {@link ParameterMapping} this view should visualize
     */
    public ParameterMappingView(ParameterMapping parameterMapping) {
        super(4);
        setNoStroke(true);
        setNoFill(true);
        setBuffers(0);

        myParameterMapping = parameterMapping;
        
        // Add a button for deleting the parameter mapping
        RamImageComponent deleteParameterMappingImage = new RamImageComponent(Icons.ICON_DELETE,
                Colors.ICON_DELETE_COLOR);
        deleteParameterMappingImage.setMinimumSize(Fonts.FONTSIZE_COMPOSITION + 2, Fonts.FONTSIZE_COMPOSITION + 2);
        deleteParameterMappingImage.setMaximumSize(Fonts.FONTSIZE_COMPOSITION + 2, Fonts.FONTSIZE_COMPOSITION + 2);
        buttonParameterMappingDelete = new RamButton(deleteParameterMappingImage);
        buttonParameterMappingDelete.setActionCommand(ACTION_PARAMETER_MAPPING_DELETE);
        buttonParameterMappingDelete.addActionListener(this);
        addChild(buttonParameterMappingDelete);
        
        // Add "Param:" text
        RamTextComponent opText = new RamTextComponent(Strings.LABEL_PARAMETER);
        opText.setFont(Fonts.FONT_COMPOSITION);
        opText.setFont(Fonts.FONT_COMPOSITION);
        opText.setBufferSize(Cardinal.SOUTH, 0);
        opText.setBufferSize(Cardinal.EAST, 0);
        // Sets the indentation
        opText.setBufferSize(Cardinal.WEST, Fonts.FONTSIZE_COMPOSITION * 2.1f);
        this.addChild(opText);

        // Add parameter From
        textParameternMappingToElement = new TextView(myParameterMapping, CorePackage.Literals.CORE_LINK__FROM);
        textParameternMappingToElement.setFont(Fonts.FONT_COMPOSITION);
        textParameternMappingToElement.setBufferSize(Cardinal.SOUTH, 0);
        textParameternMappingToElement.setBufferSize(Cardinal.WEST, 0);
        textParameternMappingToElement.setBufferSize(Cardinal.EAST, 0);
        textParameternMappingToElement.setAlignment(Alignment.CENTER_ALIGN);
        textParameternMappingToElement.setPlaceholderText(Strings.PH_SELECT_PARAMETER);
        textParameternMappingToElement.setAutoMinimizes(true);
        textParameternMappingToElement.setHandler(HandlerFactory.INSTANCE.getParameterNameHandler());
        this.addChild(textParameternMappingToElement);
        
        // Add arrow
        arrow = new RamImageComponent(Icons.ICON_ARROW_RIGHT, Colors.ICON_ARROW_COLOR);
        arrow.setMinimumSize(Fonts.FONTSIZE_COMPOSITION + 2, Fonts.FONTSIZE_COMPOSITION + 2);
        arrow.setMaximumSize(Fonts.FONTSIZE_COMPOSITION + 2, Fonts.FONTSIZE_COMPOSITION + 2);
        this.addChild(arrow);
        
        // add parameter To
        textParameternMappingToElement = new TextView(myParameterMapping, CorePackage.Literals.CORE_LINK__TO);
        textParameternMappingToElement.setFont(Fonts.FONT_COMPOSITION);
        textParameternMappingToElement.setBufferSize(Cardinal.SOUTH, 0);
        textParameternMappingToElement.setBufferSize(Cardinal.WEST, 0);
        textParameternMappingToElement.setBufferSize(Cardinal.EAST, 0);
        textParameternMappingToElement.setAlignment(Alignment.CENTER_ALIGN);
        textParameternMappingToElement.setPlaceholderText(Strings.PH_SELECT_PARAMETER);
        textParameternMappingToElement.setAutoMinimizes(true);
        textParameternMappingToElement.setHandler(HandlerFactory.INSTANCE.getParameterNameHandler());
        this.addChild(textParameternMappingToElement);
        
        setLayout(new HorizontalLayoutVerticallyCentered(Fonts.FONTSIZE_COMPOSITION / 5));
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        String actionCommand = event.getActionCommand();

        if (ACTION_PARAMETER_MAPPING_DELETE.equals(actionCommand)) {
            HandlerFactory.INSTANCE.getMappingContainerViewHandler().deleteParameterMapping(myParameterMapping);
        }
    }
    
    /**
     * Getter for the Parameter Mapping.
     * 
     * @return {@link ParameterMapping}
     */
    public ParameterMapping getParameterMapping() {
        return myParameterMapping;
    }
}
