package ca.mcgill.sel.ram.ui.views.feature.helpers;

import java.util.Set;

import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.COREModelReuse;
import ca.mcgill.sel.core.controller.COREControllerFactory;
import ca.mcgill.sel.core.controller.FeatureController;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.scenes.DisplayConcernEditScene;
import ca.mcgill.sel.ram.ui.utils.Strings;

/**
 * Class used to represent a Constraint between two features. The constraint owner is where the constraint originates
 * from to the target.
 * 
 * @author Nishanth
 */
public class Constraint {

    /**
     * Represents the different kind of constraints that exist.
     */
    public enum ConstraintType {
        /**
         * Constraint used to express that two features cannot be selected at the same time.
         */
        EXCLUDES(Strings.SYMBOL_EXCLUDE_CONSTRAINT),
        /**
         * Constraint used to express that a feature requires another one to be selected.
         */
        REQUIRES(Strings.SYMBOL_REQUIRE_CONSTRAINT);

        private String symbol;

        /**
         * Constructor for {@link ConstraintType}.
         * 
         * @param symbol - the symbol which represents the constraint.
         */
        ConstraintType(String symbol) {
            this.symbol = symbol;
        }

        /**
         * Get the symbol which represents the constraint.
         * 
         * @return the symbol associated with the constraint
         */
        public String getSymbol() {
            return this.symbol;
        }
    };

    private SelectionFeature owner;
    private SelectionFeature target;
    private ConstraintType type;

    /**
     * Constructor for the constraint class.
     * 
     * @param owner - The owner of the constraint.
     * @param target - The COREFeature to which the constraint is applied to.
     * @param type - the type of the constraint
     */
    public Constraint(SelectionFeature owner, SelectionFeature target, ConstraintType type) {
        this(owner, target, type, null);
    }

    /**
     * Constructor for the constraint class.
     * 
     * @param owner - The owner of the constraint.
     * @param target - The COREFeature to which the constraint is applied to.
     * @param type - the type of the constraint
     * @param reuse - the model reuse in which the features are contained, if any
     */
    public Constraint(SelectionFeature owner, SelectionFeature target, ConstraintType type, COREModelReuse reuse) {
        this.owner = owner;
        this.target = target;
        this.type = type;
    }

    /**
     * Function called when a constraint has be deleted.
     */
    public void delete() {
        DisplayConcernEditScene concernScene = RamApp.getActiveConcernEditScene();

        if (concernScene != null) {
            FeatureController controller = COREControllerFactory.INSTANCE.getFeatureController();
            if (type == ConstraintType.EXCLUDES) {
                controller.removeExcludesConstraint(concernScene.getConcern(),
                        owner.getCoreFeature(), target.getCoreFeature());
            } else if (type == ConstraintType.REQUIRES) {
                controller.removeRequiresConstraint(concernScene.getConcern(),
                        owner.getCoreFeature(), target.getCoreFeature());
            }
        }
    }

    @Override
    public String toString() {
        return owner.getCoreFeature().getName() + " " + type.getSymbol() + " " + target.getCoreFeature().getName();
    }

    /**
     * Function used to return the owner of the constraint.
     * 
     * @return - The owner of the constraint.
     */
    public COREFeature getOwner() {
        return owner.getCoreFeature();
    }

    /**
     * Function used to return the {@link SelectionFeature} for the owner of the constraint.
     * 
     * @return - The owner of the constraint.
     */
    public SelectionFeature getOwnerSelectionFeature() {
        return owner;
    }

    /**
     * Function used to return the target of the constraint.
     * 
     * @return - The target of the constraint.
     */
    public COREFeature getTarget() {
        return target.getCoreFeature();
    }

    /**
     * Function used to return the type of the constraint.
     * 
     * @return the type of the constraint
     */
    public ConstraintType getType() {
        return type;
    }

    /**
     * Get the symbol used to display the given Constraint.
     * 
     * @return the symbol representing the constraint.
     */
    public String getSymbol() {
        return type.getSymbol();
    }

    /**
     * Get the reuse from which mapped features come from.
     * 
     * @return the reuse, or null if features don't come from a reuse.
     */
    public COREModelReuse getModelReuse() {
        return owner.getCoreModelReuse();
    }

    /**
     * Check if a constraint is valid in regard of the selected and reexposed features.
     * 
     * @param selectedFeatures - the set of selected features
     * @param reExposedFeatures - the set of reexposed features
     * @return true if the constraint is valid, false otherwise
     */
    public boolean isValid(Set<COREFeature> selectedFeatures, Set<COREFeature> reExposedFeatures) {
        boolean result = true;
        
        COREFeature ownerFeature = getOwner();
        COREFeature targetFeature = getTarget();

        switch (type) {
            case EXCLUDES:
                if ((selectedFeatures.contains(ownerFeature)
                        && (selectedFeatures.contains(targetFeature) || reExposedFeatures.contains(targetFeature)))
                        || (selectedFeatures.contains(targetFeature) && reExposedFeatures.contains(ownerFeature))) {
                    result = false;
                }
                break;
            case REQUIRES:
                if (selectedFeatures.contains(ownerFeature) && !selectedFeatures.contains(targetFeature)
                        || (reExposedFeatures.contains(ownerFeature)
                        && !(selectedFeatures.contains(targetFeature) || reExposedFeatures.contains(targetFeature)))) {
                    result = false;
                }
                break;
        }

        return result;
    }

    /**
     * Get the name of the reuse of form Reuse1.Reuse2.Reuse3...
     *
     * @return unique name for the reuse this constraint is for.
     */
    public String getReuseLongName() {
        String res = "";
        
        COREModelReuse lastReuse = this.getModelReuse();
        if (lastReuse == null) {
            return res;
        }

        SelectionFeature feature = this.getOwnerSelectionFeature();
        while (feature.getParentFeature() != null) {
            feature = feature.getParentFeature();
            COREModelReuse newReuse = feature.getCoreModelReuse();
            if (newReuse == null) {
                res += getModelReuse().getReuse().getName();
                break;
            } else if (newReuse != lastReuse) {
                res = newReuse.getReuse().getName() + "." + res;
                lastReuse = newReuse;
            }
        }
        return res;
    }

}
