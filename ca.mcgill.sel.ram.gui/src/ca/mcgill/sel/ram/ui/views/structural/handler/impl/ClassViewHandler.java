package ca.mcgill.sel.ram.ui.views.structural.handler.impl;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.core.CORECIElement;
import ca.mcgill.sel.core.COREPartialityType;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.ram.Class;
import ca.mcgill.sel.ram.RAMPartialityType;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.controller.ClassController;
import ca.mcgill.sel.ram.controller.ControllerFactory;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamKeyboard;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.components.listeners.DefaultRamKeyboardListener;
import ca.mcgill.sel.ram.ui.components.menu.RamLinkedMenu;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.structural.ClassView;
import ca.mcgill.sel.ram.ui.views.structural.ClassifierView;
import ca.mcgill.sel.ram.ui.views.structural.handler.IClassViewHandler;

/**
 * The default handler for a {@link ClassView}.
 *
 * @author mschoettle
 */
public class ClassViewHandler extends ClassifierViewHandler implements IClassViewHandler {

    /**
     * The action to add a new attribute.
     */
    protected static final String ACTION_ATTRIBUTE_ADD = "view.class.attribute.add";
    private static final String ACTION_DESTRUCTOR_ADD = "view.class.destructor.add";
    private static final String ACTION_PUBLIC_PARTIAL = "view.class.public_partial";
    private static final String ACTION_CONCERN_PARTIAL = "view.class.concern_partial";
    private static final String ACTION_NOT_PARTIAL = "view.class.not_partial";
    private static final String ACTION_CARDINALITY = "view.class.cardinality";
    private static final String ACTION_ABSTRACT = "view.class.abstract";
    private static final String ACTION_COLLAPSE_OPERATIONS = "view.class.operations.collapse";

    private static final String SUBMENU_CLASS_CHARACTERISTICS = "sub.class.partiality";

    @Override
    public void createAttribute(final ClassifierView<?> classifierView) {
        final int index = classifierView.getClassifier().getAttributes().size();

        final RamTextComponent textRow = new RamTextComponent();
        textRow.setPlaceholderText(Strings.PH_ATTRIBUTE);
        final RamRectangleComponent attributesContainer = classifierView.getAttributesContainer();
        attributesContainer.addChild(index, textRow);

        RamKeyboard keyboard = new RamKeyboard();
        keyboard.registerListener(new DefaultRamKeyboardListener() {
            @Override
            public void keyboardCancelled() {
                attributesContainer.removeChild(textRow);

            }

            @Override
            public boolean verifyKeyboardDismissed() {
                try {
                    Class classifier = (Class) classifierView.getClassifier();
                    ControllerFactory.INSTANCE.getClassController()
                        .createAttribute(classifier, index, textRow.getText());
                    attributesContainer.removeChild(textRow);

                } catch (final IllegalArgumentException e) {
                    return false;
                }
                return true;
            }
        });

        textRow.showKeyboard(keyboard);
    }

    @Override
    public void createOperation(final ClassifierView<?> classifierView) {
        final int index = classifierView.getOperationsContainer().getChildCount();

        final RamTextComponent textRow = new RamTextComponent();
        textRow.setPlaceholderText(Strings.PH_OPERATION);
        final RamRectangleComponent operationsContainer = classifierView.getOperationsContainer();
        operationsContainer.addChild(index, textRow);

        RamKeyboard keyboard = new RamKeyboard();
        keyboard.registerListener(new DefaultRamKeyboardListener() {
            @Override
            public void keyboardCancelled() {
                operationsContainer.removeChild(textRow);
            }

            @Override
            public boolean verifyKeyboardDismissed() {
                try {
                    Class classifier = (Class) classifierView.getClassifier();
                    ControllerFactory.INSTANCE.getClassController()
                        .createOperation(classifier, classifier.getOperations().size(), textRow.getText());
                    
                    operationsContainer.removeChild(textRow);
                } catch (final IllegalArgumentException e) {
                    return false;
                }
                return true;
            }
        });

        textRow.showKeyboard(keyboard);
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        String actionCommand = event.getActionCommand();
        RamRectangleComponent pressedButton = (RamRectangleComponent) event.getTarget();
        RamLinkedMenu linkedMenu = pressedButton.getParentOfType(RamLinkedMenu.class);
        if (linkedMenu != null) {

            ClassifierView<?> clazz = (ClassifierView<?>) linkedMenu.getLinkedView();

            if (ACTION_ATTRIBUTE_ADD.equals(actionCommand)) {
                createAttribute((ClassView) clazz);
            } else if (ACTION_DESTRUCTOR_ADD.equals(actionCommand)) {
                createDestructor(clazz);
            } else if (ACTION_PUBLIC_PARTIAL.equals(actionCommand)) {
                switchPartiality(clazz, RAMPartialityType.PUBLIC);
            } else if (ACTION_CONCERN_PARTIAL.equals(actionCommand)) {
                switchPartiality(clazz, RAMPartialityType.PROTECTED);
            } else if (ACTION_NOT_PARTIAL.equals(actionCommand)) {
                switchPartiality(clazz, RAMPartialityType.NONE);
            } else if (ACTION_ABSTRACT.equals(actionCommand)) {
                switchToAbstract(clazz);
            } else if (ACTION_CARDINALITY.equals(actionCommand)) {
                switchCORECIElementExistence(clazz);
            } else if (ACTION_COLLAPSE_OPERATIONS.equals(actionCommand)) {
                ClassView classView = (ClassView) clazz;
                classView.toggleShowAllOperations();
                linkedMenu.toggleAction(classView.isShowingAllOperations(), ACTION_COLLAPSE_OPERATIONS);
            }
        }
        super.actionPerformed(event);
    }

    @Override
    public void switchToAbstract(ClassifierView<?> clazz) {
        ClassController controller = ControllerFactory.INSTANCE.getClassController();
        boolean switched = controller.switchAbstract((Class) clazz.getClassifier());
        if (!switched) {
            RamApp.getActiveScene().displayPopup(Strings.POPUP_ABSTRACT_CLASS_NO_SWITCH);
        }
    }

    @Override
    public void switchPartiality(ClassifierView<?> classifierView, RAMPartialityType type) {
        ClassController controller = ControllerFactory.INSTANCE.getClassController();
        controller.changeMappableElementPartiality(classifierView.getClassifier(), type);
    }
    
    @Override
    public void switchCORECIElementExistence(ClassifierView<?> classifierView) {
        ClassController controller = ControllerFactory.INSTANCE.getClassController();
        
        CORECIElement ciElement = COREArtefactUtil.getCIElementFor(classifierView.getClassifier());
        
        if (ciElement == null) {
            controller.createNewCORECIElement(classifierView.getClassifier());
        } else {
            controller.deleteCORECIElement(ciElement);
        }
    }

    @Override
    public void initMenu(RamLinkedMenu menu) {
        super.initMenu(menu);
        menu.addAction(Strings.MENU_DESTRUCTOR_ADD, Icons.ICON_MENU_ADD_DESTRUCTOR, ACTION_DESTRUCTOR_ADD, this,
                SUBMENU_OPERATION, true);
        menu.addAction(Strings.MENU_ATTRIBUTE_ADD, Icons.ICON_MENU_ADD_ATTRIBUTE, ACTION_ATTRIBUTE_ADD, this,
                SUBMENU_ADD, true);

        menu.addSubMenu(1, SUBMENU_CLASS_CHARACTERISTICS);
        menu.addAction(Strings.MENU_PUBLIC_PARTIAL, Icons.ICON_MENU_PUBLIC_PARTIAL, ACTION_PUBLIC_PARTIAL, this,
                SUBMENU_CLASS_CHARACTERISTICS, true);
        menu.addAction(Strings.MENU_CONCERN_PARTIAL, Icons.ICON_MENU_CONCERN_PARTIAL, ACTION_CONCERN_PARTIAL,
                this, SUBMENU_CLASS_CHARACTERISTICS, true);
        menu.addAction(Strings.MENU_NO_PARTIAL, Icons.ICON_MENU_NOT_PARTIAL, ACTION_NOT_PARTIAL, this,
                SUBMENU_CLASS_CHARACTERISTICS, true);
        menu.addAction(Strings.MENU_CARDINALITY, Strings.MENU_NOT_CARDINALITY, Icons.ICON_MENU_CARDINALITY,
                Icons.ICON_MENU_NOT_CARDINALITY, ACTION_CARDINALITY, this,
                SUBMENU_CLASS_CHARACTERISTICS, true, false);
        menu.addAction(Strings.MENU_ABSTRACT, Strings.MENU_NOT_ABSTRACT, Icons.ICON_MENU_ABSTRACT,
                Icons.ICON_MENU_NOT_ABSTRACT, ACTION_ABSTRACT, this, SUBMENU_CLASS_CHARACTERISTICS, true, false);

        menu.addAction(Strings.MENU_EXPAND_OPERATIONS, Strings.MENU_COLLAPSE_OPERATIONS,
                Icons.ICON_EXPAND_OPERATIONS, Icons.ICON_COLLAPSE_OPERATIONS,
                ACTION_COLLAPSE_OPERATIONS, this, SUBMENU_CLASS_CHARACTERISTICS, true, false);

        updateButtons(menu);
        updateVisibilityButton(menu);
    }

    @Override
    public void updateMenu(RamLinkedMenu menu, Notification notification) {
        if (notification.getEventType() == Notification.SET
                || notification.getEventType() == Notification.UNSET
                || notification.getEventType() == Notification.ADD
                || notification.getEventType() == Notification.REMOVE) {
            updateButtons(menu);
            if (notification.getFeature() == RamPackage.Literals.MAPPABLE_ELEMENT__PARTIALITY
                    || notification.getFeature() == CorePackage.Literals.CORE_ARTEFACT__CI_ELEMENTS) {
                updateVisibilityButton(menu);
            }
        }
        super.updateMenu(menu, notification);
    }

    @Override
    public List<EObject> getEObjectToListenForUpdateMenu(RamRectangleComponent rectangle) {
        ClassView classView = (ClassView) rectangle;
        List<EObject> ret = new ArrayList<EObject>();
        ret.addAll(classView.getClassifier().getOperations());
        ret.addAll(super.getEObjectToListenForUpdateMenu(rectangle));
        ret.add(COREArtefactUtil.getReferencingExternalArtefact(classView.getClassifier()));
        return ret;
    }

    /**
     * Update buttons inside the menu.
     *
     * @param menu - the menu which contains buttons.
     */
    private static void updateButtons(RamLinkedMenu menu) {
        Class clazz = (Class) menu.geteObject();
        menu.toggleAction(clazz.isAbstract(), ACTION_ABSTRACT);

        Object obj = clazz.eGet(RamPackage.Literals.MAPPABLE_ELEMENT__PARTIALITY);
        menu.enableAction(!obj.equals(RAMPartialityType.NONE), ACTION_NOT_PARTIAL);
        menu.enableAction(!obj.equals(RAMPartialityType.PROTECTED), ACTION_CONCERN_PARTIAL);
        menu.enableAction(!obj.equals(RAMPartialityType.PUBLIC), ACTION_PUBLIC_PARTIAL);
    }
    
    /**
     * Update cardinality button inside the menu.
     * 
     * @param menu - the menu which contains the cardinality button.
     */
    private static void updateVisibilityButton(RamLinkedMenu menu) {
        Class clazz = (Class) menu.geteObject();
        CORECIElement ciElement = COREArtefactUtil.getCIElementFor(clazz);
        
        if (ciElement != null) {
            menu.enableAction(ciElement.getPartiality() == COREPartialityType.NONE, ACTION_CARDINALITY);
            menu.toggleAction(true, ACTION_CARDINALITY);
        } else {
            menu.enableAction(true, ACTION_CARDINALITY);
            menu.toggleAction(false, ACTION_CARDINALITY);
        }
    }

    @Override
    public void createConstructor(ClassifierView<?> classifierView) {
        ControllerFactory.INSTANCE.getClassController().createConstructor((Class) classifierView.getClassifier());
    }

    @Override
    public void createDestructor(ClassifierView<?> clazz) {
        ControllerFactory.INSTANCE.getClassController().createDestructor((Class) clazz.getClassifier());
    }
}