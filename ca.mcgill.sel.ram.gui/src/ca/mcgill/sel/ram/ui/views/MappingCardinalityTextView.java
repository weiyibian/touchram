package ca.mcgill.sel.ram.ui.views;


import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import ca.mcgill.sel.core.CORECIElement;
import ca.mcgill.sel.core.COREMappingCardinality;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.util.COREArtefactUtil;

/**
 * A text component used to display mapping cardinality.
 * @author jmilleret
 */
public class MappingCardinalityTextView extends TextView {

    /**
     * Creates a new text view for the mapping cardinality.
     *
     * @param element - The element that have a mapping cardinality
     */
    public MappingCardinalityTextView(CORECIElement element) {
        super(element, null, false);
    }
    
    @Override
    protected String getModelText() {
        return COREArtefactUtil.getMappingCardinalityText((CORECIElement) getData());
    }

    @Override
    public void notifyChanged(Notification notification) {
        CORECIElement ciElement = (CORECIElement) getData();
        Collection<COREMappingCardinality> referenceCardinalities = ciElement.getReferenceCardinality();
        
        // Check if the feature is what we want
        if (notification.getFeature() == CorePackage.Literals.CORECI_ELEMENT__MAPPING_CARDINALITY 
                || notification.getFeature() == CorePackage.Literals.CORECI_ELEMENT__REFERENCE_CARDINALITY) {
            // Check if the notifier is the same as the ciElement or as one of its reference cardinalities
            if (notification.getNotifier() == ciElement 
                    || (referenceCardinalities != null 
                        && referenceCardinalities.contains(notification.getOldValue()))) {
                updateText();
            }
        }
    }

}
