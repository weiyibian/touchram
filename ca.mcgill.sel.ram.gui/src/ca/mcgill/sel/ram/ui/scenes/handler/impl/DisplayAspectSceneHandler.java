package ca.mcgill.sel.ram.ui.scenes.handler.impl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.mt4j.sceneManagement.transition.BlendTransition;
import org.mt4j.sceneManagement.transition.SlideTransition;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.commons.LoggerUtils;
import ca.mcgill.sel.commons.emf.util.CORECommandStack;
import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.core.util.COREModelUtil;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.generator.GeneratorFlags;
import ca.mcgill.sel.ram.generator.cpp.CppGenerator;
import ca.mcgill.sel.ram.generator.ecore.EcoreGenerator;
import ca.mcgill.sel.ram.generator.java.JavaGenerator;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.ConfirmPopup;
import ca.mcgill.sel.ram.ui.components.RamPopup;
import ca.mcgill.sel.ram.ui.components.RamPopup.PopupType;
import ca.mcgill.sel.ram.ui.components.RamSelectorComponent;
import ca.mcgill.sel.ram.ui.components.browser.GenericFileBrowser;
import ca.mcgill.sel.ram.ui.components.browser.RamFileBrowser;
import ca.mcgill.sel.ram.ui.components.browser.RamFileBrowser.RamFileBrowserType;
import ca.mcgill.sel.ram.ui.components.browser.interfaces.FileBrowserListener;
import ca.mcgill.sel.ram.ui.components.browser.interfaces.RamFileBrowserListener;
import ca.mcgill.sel.ram.ui.components.listeners.AbstractDefaultRamSelectorListener;
import ca.mcgill.sel.ram.ui.components.navigationbar.NavigationBar;
import ca.mcgill.sel.ram.ui.components.navigationbar.SlideUpDownTransition;
import ca.mcgill.sel.ram.ui.layouts.AutomaticLayout;
import ca.mcgill.sel.ram.ui.layouts.Layout;
import ca.mcgill.sel.ram.ui.scenes.DisplayAspectScene;
import ca.mcgill.sel.ram.ui.scenes.DisplayConcernEditScene;
import ca.mcgill.sel.ram.ui.scenes.DisplayConcernSelectScene;
import ca.mcgill.sel.ram.ui.scenes.RamAbstractScene;
import ca.mcgill.sel.ram.ui.utils.BasicActionsUtils;
import ca.mcgill.sel.ram.ui.utils.GUIConstants;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.utils.WeaverRunner;
import ca.mcgill.sel.ram.ui.views.OptionSelectorView;
import ca.mcgill.sel.ram.ui.views.handler.IDisplaySceneHandler;
import ca.mcgill.sel.ram.ui.views.structural.CompositionSplitEditingView;
import ca.mcgill.sel.ram.ui.views.structural.CompositionView;
import ca.mcgill.sel.ram.ui.views.structural.CompositionsPanel;
import ca.mcgill.sel.ram.ui.views.structural.StructuralDiagramView;
import ca.mcgill.sel.ram.ui.views.structural.handler.ICompositionPanelHandler;

/**
 * The default handler for a {@link DisplayAspectScene}.
 *
 * @author mschoettle
 * @author Maximilian Schiedermeier (tweaks for maven compatibility)
 */
public class DisplayAspectSceneHandler extends DefaultRamSceneHandler implements IDisplaySceneHandler {

    /**
     * A listener for a confirm popup. Handles the user response on whether to
     * save in case modifications occurred.
     */
    private final class SaveConfirmListener implements ConfirmPopup.SelectionListener {
        private final DisplayAspectScene scene;

        /**
         * Creates a new instance for the given scene.
         * 
         * @param scene the scene for this listener
         */
        private SaveConfirmListener(DisplayAspectScene scene) {
            this.scene = scene;
        }

        @Override
        public void optionSelected(int selectedOption) {
            if (selectedOption == ConfirmPopup.YES_OPTION) {
                BasicActionsUtils.saveModel(scene.getAspect(), new FileBrowserListener() {
                    @Override
                    public void modelSaved(File file) {
                        NavigationBar.getInstance().popSection();
                        NavigationBar.popHistory();
                        if (scene.getPreviousScene() instanceof DisplayConcernSelectScene) {
                            DisplayConcernSelectScene prev = (DisplayConcernSelectScene) scene.getPreviousScene();
                            ConcernSelectSceneHandler prevH = (ConcernSelectSceneHandler) prev.getHandler();
                            prevH.switchToPreviousScene(prev);
                            NavigationBar.getInstance().wipeNavigationBar();

                        } else {
                            doSwitchToPreviousScene(scene);
                        }
                    }

                    @Override
                    public void modelLoaded(EObject model) {
                    }
                });
            } else if (selectedOption == ConfirmPopup.NO_OPTION) {

                if (EMFEditUtil.getCommandStack(scene.getAspect()) instanceof CORECommandStack) {
                    CORECommandStack stack = (CORECommandStack) EMFEditUtil.getCommandStack(scene.getAspect());

                    if (stack.canRevertToLastSave()) {
                        stack.goToLastSave();
                    } else {
                        scene.displayPopup(Strings.POPUP_REVERT_IMPOSSIBLE, PopupType.ERROR);
                        return;
                    }
                } else {
                    LoggerUtils.warn(Strings.invalidCommandStackInstance(scene.getArtefact()));
                }

                NavigationBar.getInstance().popSection();
                NavigationBar.popHistory();
                checkForModelRemoval(scene);

                if (scene.getPreviousScene() instanceof DisplayConcernSelectScene) {
                    DisplayConcernSelectScene prev = (DisplayConcernSelectScene) scene.getPreviousScene();
                    ConcernSelectSceneHandler prevH = (ConcernSelectSceneHandler) prev.getHandler();
                    prevH.switchToPreviousScene(prev);
                    NavigationBar.getInstance().wipeNavigationBar();

                } else {
                    doSwitchToPreviousScene(scene);
                }
            }

        }
    }

    /**
     * A listener for a confirm popup to handle the case of a fast return to the
     * Concern Scene. Handles the user response on whether to save in case
     * modifications occurred.
     */
    private final class SaveConfirmListenerConcern implements ConfirmPopup.SelectionListener {
        private final DisplayAspectScene scene;

        /**
         * Creates a new instance for the given scene.
         * 
         * @param scene the scene for this listener
         */
        private SaveConfirmListenerConcern(DisplayAspectScene scene) {
            this.scene = scene;
        }

        @Override
        public void optionSelected(int selectedOption) {
            NavigationBar navBar = NavigationBar.getInstance();

            if (selectedOption == ConfirmPopup.YES_OPTION) {
                BasicActionsUtils.saveModel(scene.getAspect(), new FileBrowserListener() {
                    @Override
                    public void modelSaved(File file) {

                        navBar.popSection(COREConcern.class, navBar.getCurrentConcern());
                    }

                    @Override
                    public void modelLoaded(EObject model) {
                    }
                });
            } else if (selectedOption == ConfirmPopup.NO_OPTION) {
                if (EMFEditUtil.getCommandStack(scene.getAspect()) instanceof CORECommandStack) {
                    CORECommandStack stack = (CORECommandStack) EMFEditUtil.getCommandStack(scene.getAspect());

                    if (stack.canRevertToLastSave()) {
                        stack.goToLastSave();
                    } else {
                        scene.displayPopup(Strings.POPUP_REVERT_IMPOSSIBLE, PopupType.ERROR);
                        return;
                    }
                } else {
                    COREExternalArtefact externalArtefact = (COREExternalArtefact) scene.getArtefact();
                    LoggerUtils.warn(Strings.invalidCommandStackInstance(externalArtefact));
                }

                checkForModelRemoval(scene);
                navBar.popSection(COREConcern.class, navBar.getCurrentConcern());
            }
        }
    }

    /**
     * A listener for a confirm popup to handle the case of a fast return to the
     * Concern Scene. Handles the user response on whether to save in case
     * modifications occurred.
     */
    private final class SaveConfirmListenerAspect implements ConfirmPopup.SelectionListener {
        private final DisplayAspectScene scene;
        private final Aspect element;

        /**
         * Creates a new instance for the given scene.
         * 
         * @param scene the scene for this listener
         * @param element the Aspect of interest
         */
        private SaveConfirmListenerAspect(DisplayAspectScene scene, Aspect element) {
            this.scene = scene;
            this.element = element;
        }

        @Override
        public void optionSelected(int selectedOption) {
            if (selectedOption == ConfirmPopup.YES_OPTION) {
                BasicActionsUtils.saveModel(scene.getAspect(), new FileBrowserListener() {
                    @Override
                    public void modelSaved(File file) {
                        NavigationBar.getInstance().loadNewAspect(scene, element);
                    }

                    @Override
                    public void modelLoaded(EObject model) {
                    }
                });
            } else if (selectedOption == ConfirmPopup.NO_OPTION) {
                if (EMFEditUtil.getCommandStack(scene.getAspect()) instanceof CORECommandStack) {
                    CORECommandStack stack = (CORECommandStack) EMFEditUtil.getCommandStack(scene.getAspect());

                    if (stack.canRevertToLastSave()) {
                        stack.goToLastSave();
                    } else {
                        scene.displayPopup(Strings.POPUP_REVERT_IMPOSSIBLE, PopupType.ERROR);
                        return;
                    }
                } else {
                    LoggerUtils.warn("The CommandStack for " + scene.getAspect()
                            + " should be an instance of CORECommandStack.");
                }

                checkForModelRemoval(scene);
                NavigationBar.getInstance().loadNewAspect(scene, element);
            }
        }
    }

    /**
     * The listener for the generate selector.
     */
    private final class GenerateSelectorListener extends AbstractDefaultRamSelectorListener<GenerateOptions> {

        private final DisplayAspectScene scene;

        /**
         * Creates a new generate selector listener.
         *
         * @param scene the current {@link DisplayAspectScene}
         */
        private GenerateSelectorListener(DisplayAspectScene scene) {
            this.scene = scene;
        }

        @Override
        public void elementSelected(RamSelectorComponent<GenerateOptions> selector, final GenerateOptions element) {
            final Aspect aspect = scene.getAspect();
            RamFileBrowser browser = null;

            switch (element) {
                case ECORE:
                    String parentFolder = lastGeneratorSharedFolder.getAbsolutePath();
                    // Suggest the same folder where the aspect is saved in if
                    // it is
                    // serialized.
                    if (aspect.eResource() != null) {
                        parentFolder = aspect.eResource().getURI().trimSegments(1).toFileString();
                    }
                    File suggestedFile = new File(parentFolder, scene.getAspect().getName() + ".ecore");
                    browser = new RamFileBrowser(RamFileBrowserType.SAVE, "ecore", suggestedFile);
                    break;
                default:
                    browser = new RamFileBrowser(RamFileBrowserType.FOLDER, "", lastGeneratorSharedFolder);
                    break;
            }
            browser.setCallbackThreaded(true);
            browser.setCallbackPopupMessage(Strings.POPUP_GENERATING);
            // CHECKSTYLE:IGNORE AnonInnerLength: Okay here.
            browser.addFileBrowserListener(new RamFileBrowserListener() {

                @Override
                public void fileSelected(final File path, RamFileBrowser fileBrowser) {
                    try {
                        // Acceleo just prints exceptions during generation on
                        // the error console.
                        // Make sure we can write to the folder beforehand.
                        // Any other error that occurs will most likely be a
                        // template problem.
                        if (path.isDirectory() && !Files.isWritable(path.toPath())) {
                            throw new IOException("Permission denied. Cannot write to " + path.getAbsolutePath());
                        }

                        switch (element) {
                            case ECORE:
                                EcoreGenerator ecoreGenerator = new EcoreGenerator(aspect, path);
                                ecoreGenerator.doGenerate();
                                break;
                            case JAVA:
                                generateVanillaJavaCode(aspect, path);
                                break;
                            case MAVEN:
                                MavenGeneratorHandlerTools.generateMavenProjectCode(aspect, path);
                                break;
                            case CPP:
                                CppGenerator cppGenerator = new CppGenerator(aspect, path, new ArrayList<Object>());
                                cppGenerator.doGenerate(null);
                                break;
                        }

                        // If no exceptions were thrown by aboive switch cases,
                        // display a success message...
                        RamApp.getApplication().invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                scene.displayPopup(new RamPopup(
                                        Strings.popupCodeGenerated(element.toString(), path.getAbsolutePath()), true,
                                        PopupType.SUCCESS));
                            }
                        });

                        lastGeneratorSharedFolder = path;

                        // ... Otherwise display fail message
                        // CHECKSTYLE:IGNORE IllegalCatch FOR 1 LINES: Need to catch all types
                    } catch (final Exception e) {
                        e.printStackTrace();
                        RamApp.getApplication().invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                scene.displayPopup(new RamPopup(Strings.POPUP_GENERATION_ERROR + e.getMessage(), true,
                                        PopupType.ERROR));
                            }
                        });
                    }
                }
            });
            browser.display();
        }
    }

    /**
     * Generator target languages or models.
     */
    private enum GenerateOptions {
        ECORE(Strings.OPT_GENERATE_ECORE), JAVA(Strings.OPT_GENERATE_JAVA), MAVEN(Strings.OPT_GENERATE_MAVEN),
        CPP(Strings.OPT_GENERATE_CPP);

        private String name;

        /**
         * Creates a new literal with the given name.
         * 
         * @param name the name the literal represents
         */
        GenerateOptions(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    private static File lastGeneratorSharedFolder = new File(GUIConstants.DIRECTORY_MODELS).getAbsoluteFile();

    @Override
    public void generate(final RamAbstractScene<?> scene) {
        // Create the selector
        OptionSelectorView<GenerateOptions> selector = new OptionSelectorView<GenerateOptions>(
                GenerateOptions.values());
        RamApp.getActiveScene().addComponent(selector, scene.getMenu().getCenterPointGlobal());

        selector.registerListener(new GenerateSelectorListener((DisplayAspectScene) scene));
    }

    @Override
    public void loadScene(RamAbstractScene<?> scene) {
        // Transition to the right

        // Ask the user to load an aspect
        GenericFileBrowser.loadModel("ram", new FileBrowserListener() {

            @Override
            public void modelLoaded(EObject model) {
                RamApp.getApplication().loadScene(COREArtefactUtil.getReferencingExternalArtefact(model), model);
            }

            @Override
            public void modelSaved(File file) {
            }
        });
    }

    @Override
    public void showValidation(RamAbstractScene<?> scene) {
        ((DisplayAspectScene) scene).showValidation(true);
    }

    @Override
    public void showTracing(RamAbstractScene<?> scene) {
        ((DisplayAspectScene) scene).showTracing(true);
    }

    @Override
    public void back(RamAbstractScene<?> scene) {
        ((DisplayAspectScene) scene).switchToPreviousView();
    }

    @Override
    public void switchToCompositionEditMode(final RamAbstractScene<?> scene, final CompositionView compositionView) {
        COREModelComposition composition = compositionView.getModelComposition();
        // First try to load the scene to make sure it can be loaded, then do
        // the rest.
        RamApp application = RamApp.getApplication();
        COREArtefact model = composition.getSource();
        Aspect aspect = null;
        if (model instanceof COREExternalArtefact) {
            EObject artefact = ((COREExternalArtefact) model).getRootModelElement();
            if (artefact instanceof Aspect) {
                aspect = (Aspect) artefact;
            }
        }
        DisplayAspectScene loadedScene = (DisplayAspectScene) application.getExistingOrCreateModelScene(aspect);

        if (loadedScene != null) {
            CompositionSplitEditingView splitView = new CompositionSplitEditingView((DisplayAspectScene) scene,
                    loadedScene, composition);

            ((DisplayAspectScene) scene).switchToView(splitView);

            // Let the container view know about mode change. it will do
            // operations like adding a check button to the
            // compositions container to get the command of closing split view,
            // showing only the title of the composition that it being edited
            // etc..
            CompositionsPanel container = ((DisplayAspectScene) scene).getCompositionsContainerViewContainerView();
            ICompositionPanelHandler handler = container.getHandler();
            handler.switchToCompositionSplitEditMode(container, composition);
        } else {
            scene.displayPopup(Strings.popupAspectMalformed(composition.getSource().getName()), PopupType.ERROR);
        }
    }

    @Override
    public void switchToMenu(RamAbstractScene<?> scene) {
        // to the left!
        scene.setTransition(new SlideTransition(RamApp.getApplication(), 700, false));
        // go to SelectAspectScene
        RamApp.getApplication().switchToBackground((DisplayAspectScene) scene);
    }

    @Override
    public void weaveAll(RamAbstractScene<?> scene) {
        // TODO: mschoettle: Add a more elaborate check if there are no
        // compositions left. Check if there are unwoven
        // message or state views.
        WeaverRunner.getInstance().weaveAll();
    }

    @Override
    public void weaveStateMachines(RamAbstractScene<?> scene) {
        WeaverRunner.getInstance().weaveStateMachines();
    }

    @Override
    public void weaveAllNoCSPForStateViews(RamAbstractScene<?> scene) {
        WeaverRunner.getInstance().weaveAllNoCSPForStateViews();
    }

    /**
     * Removes the {@link CompositionSplitEditingView} from the scene. In
     * addition it takes care of any additional steps that are required to
     * remove the splitting of two structural views.
     *
     * @param scene the affected {@link DisplayAspectScene}
     */
    private static void removeCompositionSplitView(DisplayAspectScene scene) {
        CompositionSplitEditingView removedSplitView = (CompositionSplitEditingView) scene.getCurrentView();

        // Update composition containers
        scene.getCompositionsContainerViewContainerView().getHandler().switchFromCompositionSplitEditMode(
                scene.getCompositionsContainerViewContainerView(), removedSplitView.getComposition());

        // --------<Changes related to relocationing of the structural diagram
        // views>

        /**
         * This puts both higher and lower level {@link Aspect}'s
         * {@link StructuralDiagramView}s back to their previous location (to
         * wherever they belonged before enabling the split view. They belong to
         * a layer within their {@link DisplayAspectScene} ).
         */
        removedSplitView.getHandler().doFinalOperationsBeforeClosing();
        DisplayAspectScene lowerLevelAspectScene = removedSplitView.getDisplayAspectSceneLowerLevel();
        StructuralDiagramView higherStructuralDiagramView = removedSplitView.getDisplayAspectSceneHigherLevel()
                .getStructuralDiagramView();
        StructuralDiagramView lowerStructuralDiagramView = removedSplitView.getDisplayAspectSceneLowerLevel()
                .getStructuralDiagramView();

        // Switch back to the previous views in both scenes.
        scene.switchToPreviousView();
        lowerLevelAspectScene.switchToPreviousView();

        // In the split composition edit mode Lower level aspect's structural
        // diagram view repositioned to the half
        // bottom of the
        // screen (or somewhere else if the functionality of moving up and down
        // is added later), so we need to
        // reposition it before putting
        // it back to its original place in display aspect scene.
        lowerStructuralDiagramView.setPositionGlobal(new Vector3D(0, 0));

        // -------- </Changes related to relocationing of the structural diagram
        // views>

        // change to default colors, remove highlights
        lowerStructuralDiagramView.setNoFill(true);

        // TODO: eyildirim: splitView.destroy --> dont forget to remove the
        // listeners related to it..
        // we don't need the split view anymore.

        // Remove the structural views from the split view before it gets
        // destroyed,
        // in case they are still a child.
        // The check whether it is contained as a child is required, because
        // removeChild automatically sets the parent to null, even if it is not
        // a child of that component.
        if (removedSplitView.containsChild(higherStructuralDiagramView)) {
            removedSplitView.removeChild(higherStructuralDiagramView);
        }

        if (removedSplitView.containsChild(lowerStructuralDiagramView)) {
            removedSplitView.removeChild(lowerStructuralDiagramView);
        }

        removedSplitView.destroy();
        RamApp.getApplication().closeAspectScene(lowerLevelAspectScene);
    }

    @Override
    public void switchToConcern(final RamAbstractScene<?> scene) {
        boolean isSaveNeeded = EMFEditUtil.getCommandStack(((DisplayAspectScene) scene).getAspect()).isSaveNeeded();
        if (isSaveNeeded) {
            showCloseConfirmPopup((DisplayAspectScene) scene);
        } else {
            checkForModelRemoval((DisplayAspectScene) scene);
            doSwitchToPreviousScene((DisplayAspectScene) scene);
        }
    }

    /**
     * Method that carries out a fast switch to Concern without checking for
     * Model Removal. Used when more than one DisplayAspectScene is loaded in a
     * cascaded way, to prevent the removal of Aspects.
     * 
     * @param scene the starting scene
     */
    public void fastSwitchToConcern(final DisplayAspectScene scene) {
        boolean isSaveNeeded = EMFEditUtil.getCommandStack(scene.getAspect()).isSaveNeeded();
        if (isSaveNeeded) {
            showCloseConfirmPopup(scene);
        } else {
            NavigationBar.getInstance().popSection();
            doSwitchToPreviousScene(scene);
        }
    }

    /**
     * Method called by the collapse environment when going back from a reuse
     * concern scene.
     * 
     * @param scene reused Model scene the user is in
     */
    public void switchBackTo(final DisplayAspectScene scene) {
        COREArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(scene.getAspect());
        COREModelUtil.unloadExternalResources(artefact);

        scene.setTransition(new BlendTransition(RamApp.getApplication(), 500));

        DisplayAspectScene pA = (DisplayAspectScene) scene.getPreviousScene();
        pA.repushSections();
        pA.getCanvas().addChild(NavigationBar.getInstance());
        pA.switchToView(pA.getStructuralDiagramView());

        scene.getApplication().changeScene(pA);
        scene.getApplication().destroySceneAfterTransition(scene);
    }

    /**
     * Display a popup for the user to decided whether he wants to save the
     * aspect or leave the scene.
     *
     * @param scene - The {@link DisplayAspectScene} to consider
     */
    public void showCloseConfirmPopup(final DisplayAspectScene scene) {
        scene.showCloseConfirmPopup(scene, new SaveConfirmListener(scene));
    }

    /**
     * Display a popup for the user to decided whether he wants to save the
     * aspect or go back to ConcernScene.
     *
     * @param scene - The {@link DisplayAspectScene} to consider
     */
    public void showCloseConfirmBackToConcern(final DisplayAspectScene scene) {
        scene.showCloseConfirmPopup(scene, new SaveConfirmListenerConcern(scene));
    }

    /**
     * Called when moving from one Aspect directly to another Design Model, to
     * check if the starting one needs save.
     * 
     * @param scene scene we're moving away from
     * @param element Aspect of the scene
     */
    public void showCloseConfirmNextAspect(final DisplayAspectScene scene, final Aspect element) {
        scene.showCloseConfirmPopup(scene, new SaveConfirmListenerAspect(scene, element));
    }

    /**
     * Performs the switching to the concern scene. Unloads the resource and
     * triggers the scene change to the previous scene.
     *
     * @param scene the current aspect scene
     */
    protected void doSwitchToPreviousScene(DisplayAspectScene scene) {
        // Unload WovenAspect of this aspect when we leave
        COREArtefact artefact = scene.getArtefact();
        COREModelUtil.unloadExternalResources(artefact);
        if (scene.getPreviousScene() instanceof DisplayAspectScene) {
            scene.setTransition(new SlideUpDownTransition(RamApp.getApplication(), 500, true));

            scene.getApplication().changeScene(scene.getPreviousScene());
            if (scene.getPreviousScene() instanceof DisplayAspectScene) {
                ((DisplayAspectScene) scene.getPreviousScene()).repushSections();
            }
            scene.getApplication().destroySceneAfterTransition(scene);
        } else if (scene.getPreviousScene() instanceof DisplayConcernEditScene) {
            // Temporary workaround until navigation properly takes care of all
            // navigation.
            NavigationBar.getInstance().popSection();
            scene.getPreviousScene().getCanvas().addChild(NavigationBar.getInstance());
            scene.setTransition(new SlideTransition(RamApp.getApplication(), 500, false));

            scene.getApplication().changeScene(scene.getPreviousScene());
            scene.getApplication().destroySceneAfterTransition(scene);
        }

    }

    /**
     * Checks whether the artefact needs to be removed from the concern. The
     * artefact is removed, if it was not saved and therefore not contained
     * anywhere.
     *
     * @param scene the current aspect scene
     */
    protected static void checkForModelRemoval(DisplayAspectScene scene) {

        Aspect aspect = scene.getAspect();
        COREArtefact artefact = scene.getArtefact();

        // That is the aspect is not saved at all.
        if (aspect.eResource() == null && artefact.getScene() != null) {

            // Get all the features realizing it
            List<COREFeature> listOfFeatures = artefact.getScene().getRealizes();
            List<COREFeature> copyOfListOfFeature = new ArrayList<COREFeature>(listOfFeatures);

            // Loop through all the features and remove the realization
            for (COREFeature feature : copyOfListOfFeature) {
                // Do a pre-mature check to see if the feature realizes the
                // scene,
                // Can exist scenario, where the bi directional link might not
                // be true
                if (feature.getRealizedBy().contains(artefact.getScene())) {
                    feature.getRealizedBy().remove(artefact.getScene());
                }
            }

            COREConcern concern = artefact.getCoreConcern();

            // Remove from the list of artefacts
            if (concern != null) {
                concern.getArtefacts().remove(artefact);
            }
        }
    }

    @Override
    public void closeSplitView(RamAbstractScene<?> scene) {
        if (((DisplayAspectScene) scene).getCurrentView() instanceof CompositionSplitEditingView) {
            removeCompositionSplitView((DisplayAspectScene) scene);
        }
    }

    @Override
    public void layout(RamAbstractScene<?> scene) {
        Layout newLayout = new AutomaticLayout(70);
        newLayout.layout(RamApp.getActiveStructuralView().getContainerLayer(), Layout.LayoutUpdatePhase.FROM_PARENT);
    }

    /**
     * Takes a ram model and generates vanilla java code at the specified. Note
     * that you have to include all dependencies to external libraries into the
     * classpath manually, for the generated code to be compilable and
     * executable,. location.
     * 
     * @author (arguments, dedicated method) Maximilian Schiedermeier
     * @param aspect
     * @param path
     * @throws IOException
     */
    @SuppressWarnings("static-method")
    private void generateVanillaJavaCode(Aspect aspect, File path) throws IOException {
        ArrayList<String> vanillaGeneratorArguments = new ArrayList<String>();
        vanillaGeneratorArguments.add(GeneratorFlags.VOID.toString());
        JavaGenerator javaGenerator = new JavaGenerator(aspect, path, vanillaGeneratorArguments);
        javaGenerator.doGenerate(null);
    }
}
