package ca.mcgill.sel.ram.ui.scenes.handler.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;

import ca.mcgill.sel.ram.ArtifactSignature;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.generator.GeneratorFlags;
import ca.mcgill.sel.ram.generator.java.JavaGenerator;
import ca.mcgill.sel.ram.generator.maven.PomGenerator;

/**
 * Helper class for the DisplayAspectSceneHandler. Everything related to
 * initializing the generation of a maven project+sources is implemented here as
 * static methods.
 * 
 * @author Maximilian Schiedermeier
 *
 */
public abstract class MavenGeneratorHandlerTools {

    /**
     * Takes a ram model and generates java code in a maven project layout and a
     * corresponding maven configuration file (pom.xml) at the specified
     * location.
     * 
     * @author Maximilian Schiedermeier
     * @param aspect as the model to be interpreted
     * @param path as the parent folder for the generated files and directores.
     * @throws IOException
     */
    public static void generateMavenProjectCode(Aspect aspect, File path) throws IOException {
        // verify that the model contains all information required for a maven
        // project generation
        if (!isMavenSupportedByModel(aspect)) {
            throw new MavenGeneratorException("Provided model does not support generation of maven"
                    + " projects. Ensure the model defines a valid build-signature.");
        }

        // Analyze the model signature, build package string (overrides default
        // package string: aspectname)
        String[] packageString = extractPackageString(aspect);

        // Prepare parent directory name for maven project.
        String mavenProjectRootDir = "generated-maven-project";

        // Build the maven project folder structure and run the java code
        // generator, but place the sources where maven expects them
        // ("mavenProjectParentDir/src/main/java/group.id/artifactid/...")
        File mavenProjectSourceLocation = prepareMavenProjectLayout(path, packageString, mavenProjectRootDir);
        ArrayList<String> generatorArguments = new ArrayList<String>();
        generatorArguments.add(GeneratorFlags.MAVENSIGNATURE.name());

        JavaGenerator mavenJavaGenerator = new JavaGenerator(aspect, mavenProjectSourceLocation, generatorArguments);
        mavenJavaGenerator.doGenerate(null);

        // Generate an additional "pom.xml" (maven configuration file)
        PomGenerator mavenGenerator = new PomGenerator(aspect,
                new File(path.getPath() + File.separator + mavenProjectRootDir), new ArrayList<Object>());
        mavenGenerator.doGenerate(null);
    }

    /**
     * Takes a quick look at a received model and verifies it provides an own
     * signature and artifactId and points to a main class/operation as
     * launcher.
     * 
     * @Author Maximilian Schiedermeier
     * @param model as the model to be inspected.
     * @return boolean whether the received model supports signature based built
     * tools (maven, gradle, etc...)
     */
    private static boolean isMavenSupportedByModel(Aspect model) {

        // verify that the model defines an own signature
        ArtifactSignature signature = extractBuildSignature(model);
        if (signature == null) {
            return false;
        }

        // verify that all required signature subfields are set.
        if (signature.getArtifactId().isEmpty() || signature.getGroupId().isEmpty()
                || signature.getVersion().isEmpty()) {
            return false;
        }

        // verify that the model defines a valid launcher classifier/operation
        Classifier launcherClassifier = extractLauncherClassifier(model);
        if (launcherClassifier == null) {
            return false;
        }
        return true;
    }

    private static Classifier extractLauncherClassifier(Aspect model) {
        return model.getStructuralView().getLauncherclass();
    }

    /**
     * Extracts the build (own) Signature that defined artifactId, groupId and
     * version from a model.
     *
     * @param model as the model containing the signature information
     * @return the extracted artifactSignature. Returns null if the signature
     * does not exist.
     */
    private static ArtifactSignature extractBuildSignature(Aspect model) {
        return model.getStructuralView().getSignature();
    }

    /**
     * Analyzed a models signature and build the corresponding maven package
     * prefix hierarchy. Prefix means, that individual packages might still
     * imply a fanning-out folder structure, below the returned list. Example:
     * If your groupId/artifactId makes this method return
     * /eu/kartoffelquadrat/foo/ then on file system there might still be
     * subdirectories for different packages. However all these packages share
     * the prefix eu.kartoffelqudrat.foo.
     *
     * @param model the model to be analyzed
     * @return a string[] where each element defines a hierarchy in the
     * corresponding maven package structure.
     */
    private static String[] extractPackageString(Aspect model) {
        ArtifactSignature signature = extractBuildSignature(model);

        // Split up grouIt at "dots". Each fragment defines a package hierarchy.
        // Note: For whatever reason eclipse overrides the default Arrays.asList
        // method and returns an immutable list. So I have to copy it.
        Collection<String> packageFragments = new LinkedList<String>();
        packageFragments.addAll(Arrays.asList(signature.getGroupId().split("[.]")));

        // Final hierarchy is the artifactId (must not contain a "dot"), so we
        // run the check and add it to the list of groupId-fragments.
        String artifactId = signature.getArtifactId();
        if (artifactId.contains("[.]")) {
            throw new MavenGeneratorException("Artifact ID must not contain a  dot (\".\")");
        }
        packageFragments.add(artifactId);

        // append the groupId as last fragment of final package structure.
        return packageFragments.toArray(new String[packageFragments.size()]);
    }

    /**
     * Prepares the standard maven project layout for a given target directory.
     * Only relevant for maven-project code generation.
     * 
     * @Author Maximilian Schiedermeier
     * @param targetDir as the base directory where the maven project shall be
     * created.
     * @param packagePrefix as an array of strings to be used for the target
     * artifact's group id, e.g.new String[]{"eu", "kartoffelquadrat"}
     * @param projectShell as the id of the target maven project, e.g.
     * "timeservice"
     * @return the full path to where the java sources should reside in this
     * particular maven project.
     */
    private static File prepareMavenProjectLayout(File targetDir, String[] packagePrefix, String projectShell) {
        // get the absolute path of the target directory, where the maven
        // project layout shall be created
        String baseTargetDirPath = targetDir.getPath();

        // add the following:
        // ./src/main/java/group.id/artifactid/
        StringBuilder sb = new StringBuilder(baseTargetDirPath).append(File.separator);
        sb.append(projectShell).append(File.separator);
        sb.append("src").append(File.separator);
        sb.append("main").append(File.separator);
        sb.append("java").append(File.separator);
        for (String groupIdChunk : packagePrefix) {
            sb.append(groupIdChunk).append(File.separator);
        }
        File mavenSourceStructureDir = new File(sb.toString());
        mavenSourceStructureDir.mkdirs();
        return mavenSourceStructureDir;
    }
}
