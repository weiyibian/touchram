package ca.mcgill.sel.ram.ui.scenes;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.mt4j.components.visibleComponents.shapes.MTRectangle;
import org.mt4j.util.MTColor;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.COREFeatureModel;
import ca.mcgill.sel.core.COREImpactModel;
import ca.mcgill.sel.core.COREImpactNode;
import ca.mcgill.sel.core.COREModelReuse;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.impl.LayoutContainerMapImpl;
import ca.mcgill.sel.core.perspective.COREPerspectiveUtil;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.ConfirmPopup;
import ca.mcgill.sel.ram.ui.components.RamLineComponent;
import ca.mcgill.sel.ram.ui.components.RamListComponent;
import ca.mcgill.sel.ram.ui.components.RamPanelComponent.HorizontalStick;
import ca.mcgill.sel.ram.ui.components.RamPanelComponent.VerticalStick;
import ca.mcgill.sel.ram.ui.components.browser.GenericFileBrowser;
import ca.mcgill.sel.ram.ui.components.listeners.RamListListener;
import ca.mcgill.sel.ram.ui.components.listeners.UINotifyChangedListener;
import ca.mcgill.sel.ram.ui.scenes.handler.IConcernEditSceneHandler;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.GUIConstants;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.containers.COREConstraintContainer;
import ca.mcgill.sel.ram.ui.views.containers.COREFeatureModelLegendPanel;
import ca.mcgill.sel.ram.ui.views.containers.COREImpactConcernEditContainer;
import ca.mcgill.sel.ram.ui.views.containers.COREPerspectivePanel;
import ca.mcgill.sel.ram.ui.views.containers.COREReusePanel;
import ca.mcgill.sel.ram.ui.views.feature.FeatureDiagramEditView;
import ca.mcgill.sel.ram.ui.views.feature.FeatureView;
import ca.mcgill.sel.ram.ui.views.feature.ReuseDiagramView;
import ca.mcgill.sel.ram.ui.views.feature.helpers.Constraint;
import ca.mcgill.sel.ram.ui.views.feature.helpers.Constraint.ConstraintType;
import ca.mcgill.sel.ram.ui.views.feature.helpers.SelectionFeature;
import ca.mcgill.sel.ram.ui.views.handler.HandlerFactory;

/**
 * Class for the FeatureModel scene of the concern.
 *
 * @author Nishanth
 */
public class DisplayConcernEditScene extends AbstractConcernScene<FeatureDiagramEditView, IConcernEditSceneHandler>
        implements UINotifyChangedListener {
    
    /**
     * Private Class that defines the behaviour of the COREReusePanel selector.
     * 
     * Selects or unselects the clicked element.
     * If the reuse wasn't selected, draw it's corresponding ReuseDiagram.
     * If it was selected, deletes the ReuseDiagram and shows nothing on the bottom half of the scene.
     */
    private class ReusePanelSelectorListener implements RamListListener<COREReuse> {

        @Override
        public void elementSelected(RamListComponent<COREReuse> list, COREReuse reuse) {
            // Reuse feature display
            COREReuse currentReuse = getCurrentSelectedReuse();
            if (currentReuse != null) {
                list.getElementMap().get(currentReuse).setFillColor(Colors.DEFAULT_ELEMENT_FILL_COLOR);
                destroyReuseDiagram();
            }
            if (!reuse.equals(currentReuse)) {
                setCurrentSelectedReuse(reuse);
                destroyFeatureDiagram();
                drawFeatureDiagram(true);
                drawReuseDiagram(reuse, null);
                list.getElementMap().get(reuse).setFillColor(Colors.FEATURE_VIEW_IS_REUSING);
            } else {
                setCurrentSelectedReuse(null);
                destroyFeatureDiagram();
                drawFeatureDiagram(true);
            }
            
        }

        @Override
        public void elementDoubleClicked(RamListComponent<COREReuse> list, COREReuse reuse) {
            // Do nothing
        }

        @Override
        public void elementHeld(RamListComponent<COREReuse> list, COREReuse reuse) {
            // Do nothing
        }
    }

    /**
     * Private Class that defines the behaviour of the COREPerspectivePanel selector.
     * 
     * Selects or unselects the clicked element.
     */
    private class PerspectivePanelSelectorListener implements RamListListener<COREPerspective> {

        @Override
        public void elementSelected(RamListComponent<COREPerspective> list, COREPerspective p) {
            COREPerspective currentPerspective = getCurrentSelectedPerspective();
            if (currentPerspective != null) {
                list.getElementMap().get(currentPerspective).setFillColor(Colors.DEFAULT_ELEMENT_FILL_COLOR);
            }
            if (!p.equals(currentPerspective)) {
                setCurrentSelectedPerspective(p);
                list.getElementMap().get(p).setFillColor(Colors.FEATURE_VIEW_IS_REUSING);
            } else {
                setCurrentSelectedPerspective(null);
            }
            
        }

        @Override
        public void elementDoubleClicked(RamListComponent<COREPerspective> list, COREPerspective reuse) {
            // Do nothing
        }

        @Override
        public void elementHeld(RamListComponent<COREPerspective> list, COREPerspective reuse) {
            // Do nothing
        }
    }

    private static final String ACTION_MENU = "display.menu";
    private static final String ACTION_NEW_ASPECT = "new.aspect";
    private static final String ACTION_NEW_IMPACT_MODEL = "new.impact.model";
    private static final String ACTION_WEAVE = "weave.all";
    private static final String ACTION_EXPAND_FEATURES = "expand.feature";

    // name for sub menus
    private static final String SUBMENU_REUSES = "sub.reuses";

    private COREConstraintContainer constraintContainer;
    private COREImpactConcernEditContainer impactEditContainer;
    
    // List of selected feature. Used for partial weaving
    private List<FeatureView> selectedFeatures;
    
    private COREModelReuse currentSelectedModelReuse;
    
    /**
     * Constructor called when the concern is loaded. Initializes everything and adds to the TopLayer.
     *
     * @param app - The current {@link RamApp}
     * @param concern - The {@link COREConcern} to display
     */
    public DisplayConcernEditScene(RamApp app, COREConcern concern) {

        // Calling the constructor of the Abstract scene with the name of the concern
        super(app, concern.getName(), true);

        // Assigning the concern and the filePath
        this.concern = concern;
        filePath = new File(concern.eResource().getURI().trimSegments(1).toFileString());

        concernRectangle = new MTRectangle(app, getWidth(), getHeight());
        concernRectangle.setFillColor(Colors.BACKGROUND_COLOR);
        concernRectangle.setNoFill(false);
        concernRectangle.setPickable(false);
        concernRectangle.unregisterAllInputProcessors();

        EMFEditUtil.addListenerFor(concern.getFeatureModel(), this);
        EMFEditUtil.addListenerFor(concern, this);
        COREImpactModel im = concern.getImpactModel();

        if (im != null) {
            EMFEditUtil.addListenerFor(im, this);
        }

        build();

        setCommandStackListener(concern);

        navbar.pushSection(concern.getName(), navbar.getCoreNamer(), concern);
        navbar.setCurrentConcern(concern);
        
        selectedFeatures = new ArrayList<FeatureView>();
    }

    /**
     * Build the view.
     */
    @Override
    protected void build() {
        // Get the root of the feature model
        COREFeatureModel fm = concern.getFeatureModel();
        root = fm.getRoot();

        // Draw feature diagram
        drawFeatureDiagram(true);

        // Draw reuse oriented elements
        ReusePanelSelectorListener listener = new ReusePanelSelectorListener();
        reusePanel = new COREReusePanel(HorizontalStick.LEFT, VerticalStick.CENTER, concern.getReuses(), listener);
        containerLayer.addChild(reusePanel);
        
        // Draw loaded perspectives
        PerspectivePanelSelectorListener perspectiveListener = new PerspectivePanelSelectorListener();        
        perspectivePanel = new COREPerspectivePanel(HorizontalStick.RIGHT, VerticalStick.CENTER,
                COREPerspectiveUtil.INSTANCE.getPerspectives(), perspectiveListener);
        containerLayer.addChild(perspectivePanel);
        
        // Line between the concern and the reuse
        splitLine = new RamLineComponent(MTColor.BLACK, 
                reusePanel.getWidth(), getHeight() / 2, getWidth(), getHeight() / 2);
        
        // Container for the constraints defined in the concern
        constraintContainer = new COREConstraintContainer(HorizontalStick.RIGHT, VerticalStick.BOTTOM, true);
        constraintContainer.setElements(getConstraints(false));

        // Container for the goals defined in the concern's impact model
        impactEditContainer = new COREImpactConcernEditContainer(concern, this);

        COREFeatureModelLegendPanel legend =
                new COREFeatureModelLegendPanel(HorizontalStick.CENTER, VerticalStick.BOTTOM);

        containerLayer.addChild(legend);
        // containerLayer.addChild(aspectContainer);
        containerLayer.addChild(constraintContainer);
        containerLayer.addChild(impactEditContainer);
    }
    
    @Override
    protected void initMenu() {
        this.getMenu().addSubMenu(1, GUIConstants.MENU_EXTRA);
        this.getMenu().addAction(Strings.MENU_NEW_ASPECT, Icons.ICON_MENU_ADD_ASPECT, ACTION_NEW_ASPECT, 
                this, GUIConstants.MENU_EXTRA, true);
        this.getMenu().enableAction(false, ACTION_NEW_ASPECT);
        this.getMenu().addAction(Strings.MENU_NEW_IMPACT_ROOT, Icons.ICON_MENU_NEW_ROOT_GOAL, ACTION_NEW_IMPACT_MODEL,
                this, GUIConstants.MENU_EXTRA, true);
        this.getMenu().addAction(Strings.MENU_WEAVE_ALL, Icons.ICON_MENU_WEAVE, ACTION_WEAVE, 
                this, GUIConstants.MENU_EXTRA, true);
        // Expand
        this.getMenu().addAction(Strings.MENU_EXPAND_ALL, Icons.ICON_MENU_EXPAND_ALL_FEATURES, ACTION_EXPAND_FEATURES,
                this, SUBMENU_REUSES, true);
        this.getMenu().enableAction(false, ACTION_EXPAND_FEATURES);

    }

    @Override
    public boolean destroy() {
        EMFEditUtil.removeListenerFor(concern.getFeatureModel(), this);
        EMFEditUtil.removeListenerFor(concern, this);
        COREImpactModel im = concern.getImpactModel();
        if (im != null) {
            EMFEditUtil.removeListenerFor(im, this);
        }
        boolean ret = super.destroy();

        return ret;
    }

    /**
     * Shows a confirm popup for the given aspect to ask the user whether the aspect should be saved.
     *
     * @param parent the scene where the popup should be displayed, usually the current scene
     * @param listener the listener to inform which option the user selected
     */
    public void showCloseConfirmPopup(RamAbstractScene<?> parent, ConfirmPopup.SelectionListener listener) {
        String message = Strings.MODEL_CONCERN + " " + concern.getName() + Strings.POPUP_MODIFIED_SAVE;
        ConfirmPopup saveConfirmPopup = new ConfirmPopup(message, ConfirmPopup.OptionType.YES_NO_CANCEL);
        saveConfirmPopup.setListener(listener);

        parent.displayPopup(saveConfirmPopup);
    }

    @Override
    public void onEnter() {
        if (currentSelectedReuse != null) {
            destroyReuseDiagram();
            drawReuseDiagram(currentSelectedReuse, currentSelectedModelReuse);
        }
        GenericFileBrowser.setInitialFolder(filePath.getAbsolutePath());
        GenericFileBrowser.setRootFolder(filePath.getAbsolutePath());
    }

    /*
     * --------------------- DISPLAY -------------------
     */
    /**
     * Redraw only the feature diagram.
     */
    @Override
    public void drawFeatureDiagram(boolean repopulate) {
        // Keep the same position and scaling as before
        if (featureDiagramView == null) {
            if (currentSelectedReuse == null) {
                // full screen
                featureDiagramView = new FeatureDiagramEditView(getWidth(), getHeight(), this);
            } else {
                // split view with the selected reuse
                featureDiagramView = new FeatureDiagramEditView(getWidth(), getHeight() / 2, this);
            }
            containerLayer.addChild(0, featureDiagramView);
            featureDiagramView.setHandler(HandlerFactory.INSTANCE.getFeatureDiagramEditHandler());
        } else {
            featureDiagramView.updateFeaturesDisplay(repopulate);
        }
    }

    /**
     * Draws the selected Reuse.
     * Highlights the fill color of which feature is using the reuse.
     * Highlight the border of the feature modelReuse configuration we are seeing.
     * 
     * @param selectedReuse by the User
     * @param modelReuse The modelReuse we want to display (selection configuration is in there), null if default
     */
    @Override
    public void drawReuseDiagram(COREReuse selectedReuse, COREModelReuse modelReuse) {
        // resets the array of selected features, simplify the selection logic when changing a reuse
        selectedFeatures.removeAll(selectedFeatures);
        
        currentSelectedModelReuse = modelReuse;
        
        reuseDiagramView = new ReuseDiagramView(getWidth(), getHeight() / 2, this, selectedReuse, modelReuse);
        containerLayer.addChild(0, reuseDiagramView);
        reuseDiagramView.setHandler(HandlerFactory.INSTANCE.getFeatureDiagramSelectHandler());
        containerLayer.addChild(splitLine);
        COREFeatureModel fm = concern.getFeatureModel();
        
        boolean firstOne = true;
        
        for (COREFeature f : fm.getFeatures()) {
            if (COREPerspectiveUtil.INSTANCE.getReuses(f, null).contains(selectedReuse)) {
                if (modelReuse == null && firstOne) {
                    featureDiagramView.getFeatureView(f)
                        .highlightBorder(Colors.FEATURE_SELECTED_MODEL_CONFIGURATION);
                    firstOne = false;
                }
                featureDiagramView.getFeatureView(f).setFillColor(Colors.FEATURE_VIEW_IS_REUSING);
            } else if (f.getRealizedBy().isEmpty()) { 
                featureDiagramView.getFeatureView(f).setFillColor(Colors.FEATURE_UNASSIGNED_FILL_COLOR);
            } else {
                featureDiagramView.getFeatureView(f).setFillColor(Colors.DEFAULT_ELEMENT_FILL_COLOR);
            }
        }
        
        reuseDiagramView.updateFeatureColors();
    }
  
    /*
     * --------------------- BEHAVIOR ---------------------
     */
    @Override
    public void actionPerformed(ActionEvent event) {
        String actionCommand = event.getActionCommand();
        if (ACTION_MENU.equals(actionCommand)) {
            handler.switchToHome(this);
        } else if (ACTION_NEW_ASPECT.equals(actionCommand)) {
            handler.createAspect(this);
        } else if (ACTION_NEW_IMPACT_MODEL.equals(actionCommand)) {
            handler.createImpactModel(this);
        } else if (ACTION_EXPAND_FEATURES.equals(actionCommand)) {
            featureDiagramView.expandAllFeatures();
        } else if (ACTION_WEAVE.equals(actionCommand)) {
            for (FeatureView selected : selectedFeatures) {
                System.out.println(selected.getFeature().getName());
            }
        } else {
            super.actionPerformed(event);
        }
    }

    @Override
    public void handleNotification(Notification notification) {
        if (notification.getFeature() == CorePackage.Literals.CORE_CONCERN__IMPACT_MODEL) {
            COREImpactModel im;
            switch (notification.getEventType()) {
                case Notification.SET:
                    im = (COREImpactModel) notification.getNewValue();
                    if (im != null) {
                        EMFEditUtil.addListenerFor(im, this);
                        if (impactEditContainer != null) {
                            impactEditContainer.setImpactModel(im);
                        }
                        break;
                    }
                case Notification.UNSET:
                    im = (COREImpactModel) notification.getOldValue();
                    EMFEditUtil.removeListenerFor(im, this);
                    break;
            }
        } else if (notification.getFeature() == CorePackage.Literals.CORE_IMPACT_MODEL__LAYOUTS) {
            if (this == RamApp.getActiveScene()) {
                final LayoutContainerMapImpl containerMapImpl;
                switch (notification.getEventType()) {
                    case Notification.ADD:
                        containerMapImpl = (LayoutContainerMapImpl) notification.getNewValue();

                        containerMapImpl.eAdapters().add(new EContentAdapter() {
                            @Override
                            public void notifyChanged(final Notification notif) {
                                if (notif.getFeature() == CorePackage.Literals.LAYOUT_CONTAINER_MAP__VALUE) {
                                    final Entry<?, ?> entry;
                                    switch (notif.getEventType()) {
                                        case Notification.ADD:
                                            containerMapImpl.eAdapters().remove(this);
                                            entry = (Entry<?, ?>) notif.getNewValue();
                                            final COREImpactModel im = concern.getImpactModel();

                                            getApplication().displayEditImpactModel(concern, im,
                                                    concern.getName(), (COREImpactNode) entry.getKey());
                                            break;
                                    }
                                }
                            }
                        });
                        break;
                }
            }
        } else if (notification.getFeature() == CorePackage.Literals.CORE_FEATURE__EXCLUDES
                || notification.getFeature() == CorePackage.Literals.CORE_FEATURE__REQUIRES) {
            ConstraintType type = (notification.getFeature() == CorePackage.Literals.CORE_FEATURE__EXCLUDES)
                    ? ConstraintType.EXCLUDES
                    : ConstraintType.REQUIRES;
            SelectionFeature root = featureDiagramView.getRootFeature().getSelectionFeature();
            SelectionFeature owner =
                    SelectionFeature.findSelectionFeature((COREFeature) notification.getNotifier(), root);
            SelectionFeature target;
            switch (notification.getEventType()) {
                case Notification.ADD:
                    target = SelectionFeature.findSelectionFeature((COREFeature) notification.getNewValue(), root);
                    constraintContainer.addElement(new Constraint(owner, target, type));
                    break;
                case Notification.REMOVE:
                    target = SelectionFeature.findSelectionFeature((COREFeature) notification.getOldValue(), root);
                    constraintContainer.removeElement(new Constraint(owner, target, type));
                    break;
            }
        } else if (notification.getFeature() == CorePackage.Literals.CORE_CONCERN__REUSES) {
            switch (notification.getEventType()) {
                case Notification.ADD:
                    COREReuse newReuse = (COREReuse) notification.getNewValue();
                    List<COREReuse> listt = reusePanel.getReuses();
                    listt.add(newReuse);
                    reusePanel.setElements(listt);
                    reusePanel.setSelectedReuse(currentSelectedReuse);
                    if (reuseDiagramView != null) {
                        destroyReuseDiagram();
                        drawReuseDiagram(currentSelectedReuse, currentSelectedModelReuse);
                    }
                    break;
                case Notification.REMOVE:
                    COREReuse oldReuse = (COREReuse) notification.getOldValue();
                    reusePanel.removeElement(oldReuse);
                    if (oldReuse == currentSelectedReuse) {
                        destroyReuseDiagram();
                        currentSelectedReuse = null;
                    }
                    break;
            }
        }
    }

    /*
     * ---------------------- COLLAPSE -----------------------------
     */
    /**
     * Update the status of the 'expand all' button.
     * Check if features have to be removed from the collapsed feature list and if so remove them.
     */
    public void updateExpandButton() {
        // Remove features that can no longer be collapsed
        featureDiagramView.checkCollapseValidity();
        menu.enableAction(featureDiagramView.hasCollapsedElements(true), ACTION_EXPAND_FEATURES);
    }

    /**
     * Hide features in the view.
     * Children feature of the given feature are collapsed and will not be displayed
     *
     * @param view - The {@link FeatureView} to hide
     */
    public void switchCollapse(FeatureView view) {
        featureDiagramView.switchCollapse(view);
    }

    @Override
    protected EObject getElementToSave() {
        return concern;
    }
    
    /**
     * Handles everything when a feature is clicked.
     * @param featureView the clicked featureView
     */
    public void handleClickedFeature(FeatureView featureView) {
        // do not enable selection if a reuse is selected (not good for the overall UI logic)
        // If we decide to change, more colors and a bit more complex UI logic should be implemented
        if (currentSelectedReuse == null) {
            if (selectedFeatures.contains(featureView)) {
                selectedFeatures.remove(featureView);
                featureView.highlightBorder(null);
            } else {
                selectedFeatures.add(featureView);
                featureView.highlightBorder(Colors.FEATURE_VIEW_USER_SELECTED_STROKE_COLOR); 
            }
        } else {
            // change the displayed reuse configuration if the user clicked on a feature that is reusing
            // the current selected reuse
            COREFeature feature = featureView.getFeature();
            if (COREPerspectiveUtil.INSTANCE.getReuses(feature, null).contains(currentSelectedReuse)) {
       
                COREModelReuse modelReuse = getModelReuseFromFeature(feature);
                destroyReuseDiagram();
                drawReuseDiagram(currentSelectedReuse, modelReuse);
                for (FeatureView fv : featureDiagramView.getFeatureViews()) {
                    fv.highlightBorder(Colors.DEFAULT_ELEMENT_STROKE_COLOR);
                }
               
                featureView.highlightBorder(Colors.FEATURE_SELECTED_MODEL_CONFIGURATION);
            }
        }
    }
    
    /**
     * This operation deselects any reuses that might be selected and removes the ReuseDiagramView, if needed.
     */
    public void removeReuseSelectionIfNeeded() {
        if (reuseDiagramView != null) {
            destroyReuseDiagram();
            reuseDiagramView = null;
            currentSelectedReuse = null;
            
            // Dehighlight any selected reuse in the reuse panel
            reusePanel.clearSelections();
            
            // Un-colour any features that might have been highlighted
            COREFeatureModel fm = concern.getFeatureModel();
            for (COREFeature f : fm.getFeatures()) {
                featureDiagramView.getFeatureView(f).highlightBorder(Colors.DEFAULT_ELEMENT_STROKE_COLOR);
                if (f.getRealizedBy().isEmpty()) { 
                    featureDiagramView.getFeatureView(f).setFillColor(Colors.FEATURE_UNASSIGNED_FILL_COLOR);
                } else {
                    featureDiagramView.getFeatureView(f).setFillColor(Colors.DEFAULT_ELEMENT_FILL_COLOR);
                }
            }
        }
    }
}
