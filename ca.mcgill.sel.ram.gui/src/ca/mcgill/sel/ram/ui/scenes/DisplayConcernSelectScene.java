package ca.mcgill.sel.ram.ui.scenes;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.mt4j.components.clipping.Clip;
import org.mt4j.components.visibleComponents.shapes.MTRectangle;
import org.mt4j.input.inputProcessors.componentProcessors.AbstractComponentProcessor;
import org.mt4j.input.inputProcessors.componentProcessors.panProcessor.PanProcessorTwoFingers;
import org.mt4j.util.MTColor;

import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREConfiguration;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.COREFeatureModel;
import ca.mcgill.sel.core.COREImpactModel;
import ca.mcgill.sel.core.COREModelReuse;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.core.perspective.COREPerspectiveUtil;
import ca.mcgill.sel.core.util.COREConfigurationUtil;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamLineComponent;
import ca.mcgill.sel.ram.ui.components.RamListComponent;
import ca.mcgill.sel.ram.ui.components.RamPanelComponent.HorizontalStick;
import ca.mcgill.sel.ram.ui.components.RamPanelComponent.VerticalStick;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.listeners.RamListListener;
import ca.mcgill.sel.ram.ui.components.navigationbar.NavigationBar;
import ca.mcgill.sel.ram.ui.events.RightClickDragProcessor;
import ca.mcgill.sel.ram.ui.events.listeners.ActionListener;
import ca.mcgill.sel.ram.ui.scenes.handler.IConcernSelectSceneHandler;
import ca.mcgill.sel.ram.ui.scenes.handler.impl.ConcernSelectSceneHandler;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.GUIConstants;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.SelectionsSingleton;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.containers.COREConstraintSelectContainer;
import ca.mcgill.sel.ram.ui.views.containers.COREFeatureModelLegendPanel;
import ca.mcgill.sel.ram.ui.views.containers.COREImpactConcernSelectContainer;
import ca.mcgill.sel.ram.ui.views.containers.COREReusePanel;
import ca.mcgill.sel.ram.ui.views.feature.FeatureDiagramSelectView;
import ca.mcgill.sel.ram.ui.views.feature.FeatureView;
import ca.mcgill.sel.ram.ui.views.feature.ReuseDiagramView;
import ca.mcgill.sel.ram.ui.views.feature.handler.impl.FeatureSelectModeHandler;
import ca.mcgill.sel.ram.ui.views.feature.helpers.Constraint;
import ca.mcgill.sel.ram.ui.views.feature.helpers.SelectionFeature;
import ca.mcgill.sel.ram.ui.views.feature.helpers.SelectionFeature.FeatureSelectionStatus;
import ca.mcgill.sel.ram.ui.views.handler.HandlerFactory;

/**
 * Scene class used to represent the Selection mode of a concern.
 *
 * @author Nishanth
 * @author oalam
 * @author joerg
 */
public class DisplayConcernSelectScene
        extends AbstractConcernScene<FeatureDiagramSelectView, IConcernSelectSceneHandler>
        implements ActionListener {

    /**
     * Private Class that defines the behaviour of the COREReusePanel selector.
     * 
     * Description of the behaviour
     */
    private class ReusePanelSelectorListener implements RamListListener<COREReuse> {

        @Override
        public void elementSelected(RamListComponent<COREReuse> list, COREReuse reuse) {
            COREReuse currentReuse = getCurrentSelectedReuse();
            if (currentReuse != null && list.getElementMap().containsKey(currentReuse)) {
                list.getElementMap().get(currentReuse).setFillColor(Colors.DEFAULT_ELEMENT_FILL_COLOR);
            }
            if (reuseDiagramView != null) {
                destroyReuseDiagram();
            }
            // If the reuse clicked is different from the last one
            if (!reuse.equals(currentReuse)) {
                setCurrentSelectedReuse(reuse);
                handleNewSelectedReuse();

                list.getElementMap().get(reuse).setFillColor(Colors.FEATURE_VIEW_IS_REUSING);
            } else {
                setCurrentSelectedReuse(null);
            }
        }

        @Override
        public void elementDoubleClicked(RamListComponent<COREReuse> list, COREReuse reuse) {
        }

        @Override
        public void elementHeld(RamListComponent<COREReuse> list, COREReuse reuse) {
        }
    }

    /**
     * The enum indicating the different modes of selection.
     *
     * @author Nishanth.
     */
    public enum DisplayMode {
        /**
         * The Full_Mode selection in Feature Selection stage.
         * Show all features, even those that were selected in reuses.
         */
        FULL,

        /**
         * The Next_Mode selection in Feature Selection stage.
         * Shows only the next level of selections to make.
         */
        NEXT
    }

    private static final String ACTION_WEAVE = "weave.all";
    private static final String ACTION_MENU = "display.menu";
    private static final String ACTION_SWITCH_MODE = "switch.mode";
    private static final String ACTION_CLEAR_ALL = "clear.all";

    /**
     * Temporary fix until the weaver is fixed to disable/ enable reusing.
     */
    private boolean canReuse = true;

    private COREConstraintSelectContainer constraintSelectContainer;

    // private COREImpactConcernSelectContainer impactSelectContainer;

    private COREExternalArtefact artefact;

    private DisplayMode currentMode;

    private COREReuse extendingReuse;

    /**
     * Creates a new select scene for the given Concern.
     *
     * @param app - The current application
     * @param concern - The concern to allow selections to be made from
     * @param artefact - The artefact that is making the reuse.
     * @param extendingReuse - If we are extending a reuse. null if not
     */
    public DisplayConcernSelectScene(RamApp app, COREConcern concern, COREExternalArtefact artefact,
            COREReuse extendingReuse) {
        super(app, concern.getName().concat(" " + Strings.SCENE_NAME_CONCERN_SELECT_MODE), false);
        this.artefact = artefact;
        this.concern = concern;
        this.extendingReuse = extendingReuse;

        concernRectangle = new MTRectangle(app, getWidth(), getHeight());
        concernRectangle.setFillColor(Colors.BACKGROUND_COLOR);
        concernRectangle.setNoFill(false);
        concernRectangle.unregisterAllInputProcessors();
        concernRectangle.setChildClip(new Clip(RamApp.getApplication(), 0, 0, getWidth(), getHeight()));

        build();
    }

    /**
     * Re-create the whole Feature model view.
     */
    @Override
    protected void build() {
        // Add the background Rectangle
        addBackgroundLayer();
        COREFeatureModel featureModel = concern.getFeatureModel();
        root = featureModel.getRoot();
        // Draw feature diagram
        drawFeatureDiagram(true);

        // Draw reuse oriented elements
        ReusePanelSelectorListener listener = new ReusePanelSelectorListener();

        reusePanel = new COREReusePanel(HorizontalStick.LEFT, VerticalStick.CENTER,
                getReexposedReuses(), listener);
        containerLayer.addChild(reusePanel);

        // Line between the concern and the reuse
        splitLine = new RamLineComponent(MTColor.BLACK,
                reusePanel.getWidth(), getHeight() / 2, getWidth(), getHeight() / 2);

        List<Constraint> constraintsList = getConstraints(true);
        if (!constraintsList.isEmpty()) {
            constraintSelectContainer = new COREConstraintSelectContainer(HorizontalStick.LEFT, VerticalStick.TOP);
            constraintSelectContainer.setElements(constraintsList);
            containerLayer.addChild(constraintSelectContainer);
        }

        navbar.concernSelectMode();
        COREFeatureModelLegendPanel legend =
                new COREFeatureModelLegendPanel(HorizontalStick.RIGHT, VerticalStick.BOTTOM);
        containerLayer.addChild(legend);
    }

    /**
     * Get the selected features of the Feature Diagram Select View.
     * 
     * @return list of selected COREFeature
     */
    public Set<COREFeature> getSelectedFeatures() {
        FeatureView rootFeatureView = featureDiagramView.getRootFeature();
        return SelectionsSingleton.getInstance().collectSelected(rootFeatureView.getSelectionFeature());
    }

    /**
     * Getter for the current display mode.
     *
     * @return - The current mode
     */
    public DisplayMode getCurrentMode() {
        return currentMode;
    }

    /**
     * Setter for the current display mode.
     *
     * @param currentMode - The new mode
     */
    public void setCurrentMode(DisplayMode currentMode) {
        this.currentMode = currentMode;
        this.featureDiagramView.setCurrentMode(currentMode);
    }

    /**
     * Function to add the Background layer action of creating an Aspect.
     */
    public void addBackgroundLayer() {
        RamRectangleComponent rectangle = new RamRectangleComponent(0, 0, RamApp.getActiveScene().getWidth(), RamApp
                .getActiveScene().getHeight());
        rectangle.setVisible(true);
        rectangle.setNoFill(true);
        rectangle.setFillColor(Colors.COLOR_BUTTON_ENABLED);

        AbstractComponentProcessor rightClick = new RightClickDragProcessor(RamApp.getApplication());
        rightClick.setBubbledEventsEnabled(true);

        AbstractComponentProcessor twoPanFinger = new PanProcessorTwoFingers(RamApp.getApplication());
        twoPanFinger.setBubbledEventsEnabled(true);

        FeatureSelectModeHandler tapHandler = HandlerFactory.INSTANCE.getFeatureSelectModeHandler();

        containerLayer.registerInputProcessor(rightClick);
        containerLayer.addGestureListener(RightClickDragProcessor.class, tapHandler);

        containerLayer.registerInputProcessor(twoPanFinger);
        containerLayer.addGestureListener(PanProcessorTwoFingers.class, tapHandler);

        containerLayer.addChild(rectangle);
    }

    @Override
    public void actionPerformed(final ActionEvent event) {
        String actionCommand = event.getActionCommand();
        if (ACTION_MENU.equals(actionCommand)) {
            NavigationBar.getInstance().concernSelectMode();
            handler.switchToPreviousScene(this);
        } else if (ACTION_WEAVE.equals(actionCommand)) {
            NavigationBar.getInstance().concernSelectMode();
            handler.reuse(this);
        } else if (ACTION_CLEAR_ALL.equals(actionCommand)) {
            handler.clear(this);
        } else if (ACTION_SWITCH_MODE.equals(actionCommand)) {
            handler.switchMode(this);
        }
    }

    /**
     * Function used to set the handler for the scene.
     *
     * @param handler - The concern select scene handler.
     */
    public void setHandler(ConcernSelectSceneHandler handler) {
        this.handler = handler;
    }

    /**
     * Returns the artefact which is reusing the concern.
     *
     * @return COREExternalArtefact - The artefact which is reusing the concern.
     */
    public COREExternalArtefact getReusingArtefact() {
        return artefact;
    }

    /**
     * Temporary fix made to return whether the concern can be woven or not.
     *
     * @return - Boolean value set in the scene, whether a reuse can be made or not.
     */
    public boolean getCanReuse() {
        return canReuse;
    }

    /**
     * Temporary fix made to set whether the concern can be woven or not.
     *
     * @param canReuse - can we reuse the concern or not
     */
    private void setCanReuse(boolean canReuse) {
        this.canReuse = canReuse;
    }

    /**
     * Method used to change the buttons when different mode is selected.
     */
    public void updateModeButtons() {
        boolean tog = currentMode == DisplayMode.NEXT;
        getMenu().toggleAction(tog, ACTION_SWITCH_MODE);
    }

    /**
     * Enables or disables the reuse buttons according to the current selection validity.
     */
    public void updateReuseButton() {
        boolean constraints = constraintSelectContainer == null
                || constraintSelectContainer.validate(featureDiagramView);
        setCanReuse(constraints && featureDiagramView.checkForClashes());
        menu.enableAction(canReuse, ACTION_WEAVE);
    }

    @Override
    public void drawFeatureDiagram(boolean repopulate) {
        if (featureDiagramView == null) {
            featureDiagramView = new FeatureDiagramSelectView(getWidth(), getHeight(), concern.getFeatureModel());
            containerLayer.addChild(featureDiagramView);
            featureDiagramView.setHandler(HandlerFactory.INSTANCE.getFeatureDiagramSelectHandler());
            SelectionFeature rootFeature = featureDiagramView.getRootFeature().getSelectionFeature();
            SelectionsSingleton.getInstance().setRootFeature(rootFeature, true);
            setCurrentMode(DisplayMode.FULL);
            selectionChanged(true);
        } else {
            SelectionFeature rootFeature = featureDiagramView.getRootFeature().getSelectionFeature();
            SelectionsSingleton.getInstance().setRootFeature(rootFeature);
            selectionChanged(false);
            featureDiagramView.updateFeaturesDisplay(repopulate);
            featureDiagramView.setHandler(HandlerFactory.INSTANCE.getFeatureDiagramSelectHandler());
        }
    }

    @Override
    public void drawReuseDiagram(COREReuse selectedReuse, COREModelReuse modelReuse) {
        // Sets the diagram just above the feature diagram view
        reuseDiagramView = new ReuseDiagramView(getWidth(), getHeight() / 2, this, selectedReuse, modelReuse);
        containerLayer.addChild(containerLayer.getChildIndexOf(featureDiagramView) + 1, reuseDiagramView);
        containerLayer.addChild(splitLine);
        reuseDiagramView.setHandler(HandlerFactory.INSTANCE.getFeatureDiagramSelectHandler());
        reuseDiagramView.updateFeatureColors();
    }

    /**
     * Function called when a feature is selected / tapped.
     *
     * @param updateDisplay - Whether we want to place the icons and update the colors at the end of this method or not
     */
    public void selectionChanged(boolean updateDisplay) {
        if (concern.getImpactModel() != null && concern.getImpactModel().getContributions().size() > 0) {
            refreshImpactSelectContainer();
        }
        if (updateDisplay) {
            featureDiagramView.placeIcons();
            featureDiagramView.updateFeatureColors();
        }
        updateReuseButton();
    }

    /**
     * Check if there are clashes in the selections made.
     *
     * @return true if there are clashes, false otherwise
     */
    public boolean hasClashes() {
        return !featureDiagramView.checkForClashes();
    }

    /**
     * Create a new {@link COREImpactConcernSelectContainer} with the new goal Impact Map and the new selected feature.
     */
    public void refreshImpactSelectContainer() {
        COREImpactModel impactModel = concern.getImpactModel();

        if (impactModel == null) {
            return;
        }
        System.err.println("IMPLEMENTATION IS WRONG: DisplayConcernEditScene l.342");
        // List<COREImpactNode> rootGoals = new
        // ArrayList<COREImpactNode>(COREImpactModelUtil.getRootGoals(impactModel));
        //
        // PropagationResult propagationResult = ForwardPropagationAlgorithm.propagate(impactModel,
        // SelectionsSingleton.getInstance().getSelectedReuseConfiguration(), rootGoals);
        //
        // if (impactSelectContainer == null) {
        // impactSelectContainer = new COREImpactConcernSelectContainer(concern, this, propagationResult);
        // containerLayer.addChild(impactSelectContainer);
        // } else {
        // impactSelectContainer.setPropagationResult(propagationResult);
        // }
    }

    @Override
    protected void initMenu() {
        this.getMenu().addAction(Strings.MENU_WEAVE_ALL, Icons.ICON_MENU_VALIDATE, ACTION_WEAVE, this, true);

        this.getMenu().addSubMenu(2, ACTION_MENU);
        this.getMenu().addAction(Strings.MENU_BACK, Icons.ICON_MENU_CLOSE, ACTION_MENU, this, ACTION_MENU, true);

        this.getMenu().addSubMenu(1, GUIConstants.MENU_EXTRA);
        this.getMenu().addAction(Strings.MENU_SWITCH_NEXT, Strings.MENU_SWITCH_FULL, Icons.ICON_MENU_SHOW_NORMAL_MODE,
                Icons.ICON_MENU_SHOW_FULL_MODE, ACTION_SWITCH_MODE, this, GUIConstants.MENU_EXTRA, true, false);
        this.getMenu().addAction(Strings.MENU_CLEAR_SELECTION, Icons.ICON_MENU_CLEAR_SELECTION, ACTION_CLEAR_ALL, this,
                GUIConstants.MENU_EXTRA, true);
    }

    @Override
    protected EObject getElementToSave() {
        return null;
    }

    /**
     * Getter for extendingReuse.
     * 
     * @return value of extendingReuse
     */
    public COREReuse getExtendingReuse() {
        return extendingReuse;
    }

    /**
     * Returns the list of reexposed reuses within the given list of selectedFeatures.
     * 
     * @return reuses - list of reuses that have reexposed features
     */
    public List<COREReuse> getReexposedReuses() {
        // Set<COREFeature> selectedFeatures = getSelectedFeatures();
        List<COREReuse> reexposedReuses = new ArrayList<COREReuse>();
        //TODO: make this work again
//        for (COREFeature feature : selectedFeatures) {
//            if (feature.getReuses().size() > 0) {
//                for (COREReuse reuse : feature.getReuses()) {
//                    for (COREModelReuse modelReuse : reuse.getModelReuses()) {
//                        if (!modelReuse.getConfiguration().getReexposed().isEmpty()) {
//                            if (!reexposedReuses.contains(modelReuse.getReuse())) {
//                                reexposedReuses.add(modelReuse.getReuse());
//                            }
//                        }
//                    }
//                }
//            }
//        }

        return reexposedReuses;
    }

    /**
     * Handle a new selected Reuse.
     */
    public void handleNewSelectedReuse() {
        // Get and map the reuse configuration (merged or not)
        COREConfiguration configuration = mergeReuseConfiguration(currentSelectedReuse);
        SelectionsSingleton.getInstance().mapReuseToDefaultConfiguration(currentSelectedReuse, configuration);

        // Draw the diagram (sets the feature handlers on reexposed features)
        drawReuseDiagram(currentSelectedReuse, (COREModelReuse) configuration.eContainer());

        // If the user has made some choices on this reuse, re-apply them
        // We do it after the draw, since the feature handlers are on reexposed feature
        // this allow us to have a selected feature (saved user choice) but still have a handler on the feature
        if (SelectionsSingleton.getInstance().getReuseUserConfiguration().containsKey(currentSelectedReuse)) {
            applyUserSelection(currentSelectedReuse);
            reuseDiagramView.updateFeatureColors();
        }
    }

    /**
     * Apply the saved user selections to the default configuration (that will be used to display the diagram).
     * We will update the diagram colors after this so the user sees his previous selections.
     * 
     * @param reuse we are working on
     */
    private void applyUserSelection(COREReuse reuse) {
        Map<COREFeature, FeatureSelectionStatus> selections =
                SelectionsSingleton.getInstance().getReuseUserConfiguration().get(reuse);

        // for each feature that has a saved user selection, apply the change on the configuration
        for (Map.Entry<COREFeature, FeatureSelectionStatus> entry : selections.entrySet()) {
            reuseDiagramView.getFeatureView(entry.getKey()).setSelectionStatus(entry.getValue());
        }
    }

    /**
     * Merges all the configuration of the selected reuse.
     * For every selected features in the FeatureDiagramSelectView, we check if it has the reuse.
     * If yes, we merge the configurations
     * 
     * @param selectedReuse 
     * @return list
     */
    private COREConfiguration mergeReuseConfiguration(COREReuse selectedReuse) {

        Set<COREFeature> features = SelectionsSingleton.getInstance().collectSelected();
        COREConfiguration configuration = null;

        for (COREFeature feature : features) {
            if (COREPerspectiveUtil.INSTANCE.getReuses(feature, null).contains(selectedReuse)) {
                COREConfiguration toMerge = getModelReuseFromFeature(feature).getConfiguration();
                if (configuration == null) {
                    configuration = toMerge;
                } else {
                    COREConfigurationUtil.mergeConfigurations(configuration, toMerge);
                }
            }
        }

        return configuration;
    }

}
