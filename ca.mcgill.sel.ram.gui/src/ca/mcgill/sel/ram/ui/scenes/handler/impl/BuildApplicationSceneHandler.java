package ca.mcgill.sel.ram.ui.scenes.handler.impl;

import org.mt4j.sceneManagement.transition.SlideTransition;

import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREConfiguration;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.util.COREConfigurationUtil;
import ca.mcgill.sel.core.weaver.COREWeaver;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamPopup.PopupType;
import ca.mcgill.sel.ram.ui.scenes.DisplayConcernSelectScene;
import ca.mcgill.sel.ram.ui.utils.SelectionsSingleton;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.handler.HandlerFactory;

/**
 * Handler that handles events occurring in the feature select scene when reusing a concern.
 *
 * @author CCamillieri
 */
public class BuildApplicationSceneHandler extends ConcernSelectSceneHandler {

    @Override
    public void reuse(DisplayConcernSelectScene scene) {
        if (!scene.hasClashes() && scene.getCanReuse()) {
            try {
                COREConfiguration cfg = SelectionsSingleton.getInstance().getSelectedConfiguration();
                if (!COREConfigurationUtil.isConfigurationComplete(cfg)) {
                    scene.displayPopup(Strings.POPUP_ERROR_INCOMPLETE_SELECTION, PopupType.ERROR);
                    return;
                }
    
                scene.displayPopup(Strings.POPUP_REUSING);
    
                COREConcern reusedConcern = scene.getConcern();
    
                // Weave the features and rename the woven aspect
                COREExternalArtefact wovenArtefact = COREWeaver.getInstance().weaveConcern(reusedConcern, cfg);
                Aspect wovenAspect = (Aspect) wovenArtefact.getRootModelElement();
                wovenAspect.setName(reusedConcern.getName() + "Application");
    
                // Load the aspect and destroy the scene
                scene.setTransition(new SlideTransition(RamApp.getApplication(), 700, true));
                RamApp.getApplication().loadScene(wovenArtefact, wovenAspect,
                        HandlerFactory.INSTANCE.getDisplayWovenAspectSceneHandler());
                RamApp.getApplication().destroySceneAfterTransition(scene);
                
                // CHECKSTYLE:IGNORE IllegalCatch: Many exceptions can occur and we don't want to crash the application.
            } catch (Exception e) {
                scene.displayPopup(Strings.POPUP_ERROR_REUSE, PopupType.ERROR);
                e.printStackTrace();
            }
        }
    }
}
