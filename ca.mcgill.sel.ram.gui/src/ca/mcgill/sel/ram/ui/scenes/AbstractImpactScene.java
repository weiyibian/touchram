package ca.mcgill.sel.ram.ui.scenes;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEMap.Entry;
import org.mt4j.sceneManagement.transition.SlideTransition;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.COREImpactModel;
import ca.mcgill.sel.core.COREImpactNode;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.impl.LayoutContainerMapImpl;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.listeners.UINotifyChangedListener;
import ca.mcgill.sel.ram.ui.components.navigationbar.NavigationBar;
import ca.mcgill.sel.ram.ui.scenes.handler.IRamAbstractSceneHandler;

/**
 * Class used to represent the Impact Model Scene.
 *
 * @author Romain
 */
public abstract class AbstractImpactScene extends RamAbstractScene<IRamAbstractSceneHandler>
        implements UINotifyChangedListener {

    private static final String ACTION_BACK = "display.back";

    /**
     * The {@link COREImpactModel} of this concern.
     */
    protected COREImpactModel impactModel;
    /**
     * The root element of this scene.
     */
    protected COREImpactNode rootElement;

    private LayoutContainerMapImpl mapLayoutEntry;

    /**
     * Constructor called from the RamApp to display the Impact Model.
     *
     * @param application - The RamApp application.
     * @param name - The name of the scene.
     * @param impactModel - The {@link COREImpactModel} of this scene
     * @param rootNode the root node of this scene
     * @param defaultActions - Whether we want the default actions or not
     */
    public AbstractImpactScene(RamApp application, String name, COREImpactModel impactModel,
            COREImpactNode rootNode, boolean defaultActions) {
        super(application, name, defaultActions);

        this.impactModel = impactModel;
        this.rootElement = rootNode;

        mapLayoutEntry =
                EMFModelUtil.getEntryFromMap(this.impactModel.getLayouts(), rootElement);

        // register to the ContainerMap to receive adds/removes of ElementMaps
        EMFEditUtil.addListenerFor(mapLayoutEntry, this);
        
    }

   
    @Override
    public void actionPerformed(ActionEvent event) {
        String actionCommand = event.getActionCommand();
        if (ACTION_BACK.equals(actionCommand)) {
            switchToPreviousScene();
        } else {
            super.actionPerformed(event);
        }
    }

    @Override
    public void handleNotification(Notification notification) {
        if (notification.getFeature() == CorePackage.Literals.LAYOUT_CONTAINER_MAP__VALUE
                && notification.getNotifier() == this.mapLayoutEntry) {
            Entry<?, ?> entry;
            switch (notification.getEventType()) {
                case Notification.REMOVE:
                    entry = (Entry<?, ?>) notification.getOldValue();

                    COREImpactNode impactModelElement = (COREImpactNode) entry.getKey();

                    if (impactModelElement == rootElement) {
                        // Temporary workaround until navigation properly takes care of all navigation.
                        NavigationBar.getInstance().popSection();
                        switchToPreviousScene();
                    }

                    break;
            }
        }
    }

    /**
     * Switch to the previous scene.
     */
    public void switchToPreviousScene() {
        this.setTransition(new SlideTransition(RamApp.getApplication(), 700, false));
        this.getPreviousScene().getCanvas().addChild(NavigationBar.getInstance());
        this.getApplication().changeScene(this.getPreviousScene());
        this.getApplication().destroySceneAfterTransition(this);
    }

    @Override
    public boolean destroy() {
        EMFEditUtil.removeListenerFor(mapLayoutEntry, this);
        return super.destroy();
    }

    @Override
    protected void initMenu() {
    }
    
  
}
