package ca.mcgill.sel.ram.ui.utils;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.ecore.EStructuralFeature;

import ca.mcgill.sel.core.COREVisibilityType;
import ca.mcgill.sel.ram.RAMClassVisibilityType;
import ca.mcgill.sel.ram.RAMVisibilityType;
import ca.mcgill.sel.ram.RamPackage;

/**
 * A util class for correct creation of RAM metamodel elements. Also provide getters for these elements and makes sure
 * they are well formed.
 *
 * @author vbonnet
 * @author eyildirim
 * @author mschoettle
 * @author oalam
 */
public final class PrettyPrinterUtil {

    private static PrettyPrinterUtil instance;

    private Map<EStructuralFeature, Map<Object, String>> prettyPrinters;

    /**
     * Creates a new instance with default pretty printers.
     */
    private PrettyPrinterUtil() {
        prettyPrinters = new HashMap<EStructuralFeature, Map<Object, String>>();

        initializePrettyPrinters();
    }

    /**
     * Returns the singleton instance.
     *
     * @return the singleton instance
     */
    private static PrettyPrinterUtil getInstance() {
        if (instance == null) {
            instance = new PrettyPrinterUtil();
        }
        return instance;
    }

    /**
     * Initializes the pretty printers.
     */
    private void initializePrettyPrinters() {
        // pretty printer for Visibility
        Map<Object, String> prettyPrinter = new HashMap<Object, String>();

        prettyPrinter.put(COREVisibilityType.PUBLIC, Strings.SYMBOL_PUBLIC);
        prettyPrinter.put(COREVisibilityType.CONCERN, Strings.SYMBOL_PACKAGE);
        prettyPrinter.put(RAMVisibilityType.PUBLIC, Strings.SYMBOL_PUBLIC);
        prettyPrinter.put(RAMVisibilityType.PACKAGE, Strings.SYMBOL_PACKAGE);
        prettyPrinter.put(RAMVisibilityType.PRIVATE, Strings.SYMBOL_PRIVATE);
        prettyPrinter.put(RAMVisibilityType.PROTECTED, Strings.SYMBOL_PROTECTED);
        
        prettyPrinter.put(RAMClassVisibilityType.PUBLIC, Strings.SYMBOL_PUBLIC);
        prettyPrinter.put(RAMClassVisibilityType.PACKAGE, Strings.SYMBOL_PACKAGE);
        

        prettyPrinters.put(RamPackage.Literals.CLASSIFIER__VISIBILITY, prettyPrinter);
        prettyPrinters.put(RamPackage.Literals.OPERATION__VISIBILITY, prettyPrinter);
    }

    /**
     * Returns a map for pretty printing the given {@link EStructuralFeature}. Returns null if it doesn't exist.
     *
     * @param feature
     *            the feature a pretty printer is looked for
     * @return the map for pretty printing, null if not existent
     */
    public static Map<Object, String> getPrettyPrinter(EStructuralFeature feature) {
        return getInstance().prettyPrinters.get(feature);
    }
    
}
