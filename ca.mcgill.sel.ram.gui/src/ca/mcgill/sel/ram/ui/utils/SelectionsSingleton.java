package ca.mcgill.sel.ram.ui.utils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ca.mcgill.sel.core.COREConcernConfiguration;
import ca.mcgill.sel.core.COREConfiguration;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.COREFeatureModel;
import ca.mcgill.sel.core.COREFeatureRelationshipType;
import ca.mcgill.sel.core.COREModelReuse;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.core.CoreFactory;
import ca.mcgill.sel.core.util.COREConfigurationUtil;
import ca.mcgill.sel.ram.ui.views.feature.helpers.SelectionFeature;
import ca.mcgill.sel.ram.ui.views.feature.helpers.SelectionFeature.FeatureSelectionStatus;

/**
 * A singleton class used to store all the selections.
 * The selections can be composed of selections and reexposed features.
 * Every time a Feature Model Select Scene is displayed, it clear all its entries.
 * List of re-exposed feature is in fact list of features that have been de-reexposed,
 * because by default all features are reexposed.
 * 
 * @author Nishanth, CCamillieri
 */
public final class SelectionsSingleton {

    private static SelectionsSingleton instance;

    /** List for selected features from the reused concern. */
    private Set<SelectionFeature> selectedList = new HashSet<SelectionFeature>();
    /** List for selected features from the reused concern. */
    private Set<SelectionFeature> reexposedList = new HashSet<SelectionFeature>();

    /** Root feature of the reused concern. */
    private SelectionFeature rootFeature;

    /** Default configuration to display. */
    private COREConfiguration baseConfiguration;
    
    /** 
     * Map a reuse with his default configuration. 
     * The configuration can be a single configuration or a merged one.
     */ 
    private Map<COREReuse, COREConfiguration> reuseDefaultConfiguration = new HashMap<COREReuse, COREConfiguration>();
        
    /**
     * Map a reuse with the choices the user has made on the (reexposed) features.
     */
    private Map<COREReuse, Map<COREFeature, FeatureSelectionStatus>> reuseUserConfiguration
                = new HashMap<COREReuse, Map<COREFeature, FeatureSelectionStatus>>();
        
    /**
     * Prevent to use constructor.
     */
    private SelectionsSingleton() {
    }

    /**
     * Function used to return one instance of the class (Singleton).
     * 
     * @return - Returns the instance (Singleton) of the class.
     */
    public static SelectionsSingleton getInstance() {
        if (instance == null) {
            instance = new SelectionsSingleton();
        }
        return instance;
    }

    /**
     * Function called when both the List have to be cleared.
     */
    public void resetSelection() {
        selectedList.clear();
        reexposedList.clear();
        initializeSelection(baseConfiguration);
        updateFeaturesStatus();
        
        reuseDefaultConfiguration = new HashMap<COREReuse, COREConfiguration>();
        reuseUserConfiguration = new HashMap<COREReuse, Map<COREFeature, FeatureSelectionStatus>>();
    }

    /**
     * Function called when both the List have to be cleared.
     * Called when a new Feature Model Select Scene is loaded.
     */
    public void clearAll() {
        baseConfiguration = null;
        selectedList.clear();
        reexposedList.clear();
        rootFeature = null;
        
        reuseDefaultConfiguration = new HashMap<COREReuse, COREConfiguration>();
        reuseUserConfiguration = new HashMap<COREReuse, Map<COREFeature, FeatureSelectionStatus>>();
    }

    /**
     * Setter for the rootFeature of the current reuse. Must be called with the correct hierarchy of features before
     * any attempt to select feature is done.
     *
     * @param rootFeature - The rootFeature to set.
     */
    public void setRootFeature(SelectionFeature rootFeature) {
        setRootFeature(rootFeature, false);
    }

    /**
     * Setter for the rootFeature of the current reuse. Must be called with the correct hierarchy of features before
     * any attempt to select feature is done.
     *
     * @param root - The rootFeature to set.
     * @param resetState - Whether to clear selection (and re-initialize from base configuration)
     */
    public void setRootFeature(SelectionFeature root, boolean resetState) {
        this.rootFeature = root;
        if (resetState) {
            resetSelection();
        }
    }

    /**
     * Setter for a base configuration to use by default for display. Will build selection from it.
     *
     * @param configuration - The configuration to build selection from.
     */
    public void setBaseConfig(COREConfiguration configuration) {
        this.baseConfiguration = configuration;
        resetSelection();
    }

    /**
     * Used to add a new selection to the list.
     * 
     * @param feature - The {@link COREFeature} that was selected
     */
    public void addSelection(SelectionFeature feature) {
        selectedList.add(feature);
        // Update status of features
        updateFeaturesStatus();
    }

    /**
     * Used to initialize selections from the given configuration.
     * TODO Temp does not take in account default selections that might have made.
     * TODO Temp implementation: only looks at base selection. Would need to look at extended configurations and
     * everything and use this to initialize view with {@link COREConcernConfiguration}.
     * 
     * @param configuration - The {@link COREConfiguration} we want to use
     */
    private void initializeSelection(COREConfiguration configuration) {
        if (rootFeature == null || configuration == null) {
            return;
        }
        // Add selected features (from leaves only)
        for (COREFeature feature : COREConfigurationUtil.getSelectedLeaves(configuration)) {
            SelectionFeature f = SelectionFeature.findSelectionFeature(feature, rootFeature);
            if (f != null) {
                addSelection(f);
            }
        }

        // Re-exposed features
        for (COREFeature feature : ((COREFeatureModel) configuration.getSource()).getFeatures()) {
            SelectionFeature f = SelectionFeature.findSelectionFeature(feature, rootFeature);
            // Features that are not selected and not reexposed are de-reexposed
            if (f != null && !configuration.getSelected().contains(feature)
                    && !configuration.getReexposed().contains(feature)) {
                removeReexposition(f);
            }
        }

        // Update status of features
        updateFeaturesStatus();
    }

    /**
     * Used to remove a selection from the list.
     * 
     * @param feature - The {@link SelectionFeature} that was unselected
     */
    public void removeSelection(SelectionFeature feature) {
        selectedList.remove(feature);
        // Update status of features
        updateFeaturesStatus();
    }

    /**
     * Used to de-reexpose a feature (ie, add it to the list).
     * 
     * @param feature - The {@link SelectionFeature} that was re-exposed
     */
    public void removeReexposition(SelectionFeature feature) {
        reexposedList.add(feature);
        // Update status of features
        updateFeaturesStatus();
    }

    /**
     * Used to re-expose a feature (ie, remove it from the list).
     * 
     * @param feature - The {@link SelectionFeature} that we want to be removed
     */
    public void addReexposition(SelectionFeature feature) {
        reexposedList.remove(feature);
        // Update status of features
        updateFeaturesStatus();
    }

    /**
     * Updates the status of all features based on the selection.
     */
    private void updateFeaturesStatus() {
        if (rootFeature != null) {
            clearFeature(rootFeature);
            rootFeature.setStatus(FeatureSelectionStatus.AUTO_SELECTED);
            autoSelectFeature(rootFeature);
        }
    }

    /**
     * Checks if a given feature has been selected.
     * If recursive is false, the method will return true only if the feature has been manually selected.
     * Otherwise, checks if any of the children feature has been selected, and return true if it's the case.
     * 
     * @param selectionFeature - The {@link SelectionFeature} to check.
     * @param recursive - Whether we want to check children features or not.
     * @return true if the feature is contained in the list of selected features.
     */
    public boolean containsSelectedFeature(SelectionFeature selectionFeature, boolean recursive) {
        if (selectionFeature != null) {
            if (selectedList.contains(selectionFeature)) {
                return true;
            } else if (recursive) {
                for (SelectionFeature child : selectionFeature.getChildrenFeatures()) {
                    if (containsSelectedFeature(child, recursive)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Checks if a given feature has been de-reexposed.
     * If recursive is false, the method will return true only if the feature has been manually de-reexposed.
     * Otherwise, checks if any of the parent feature has been de-reexposed, and return true if it's the case.
     * 
     * @param selectionFeature - The {@link SelectionFeature} to check
     * @param recursive - Whether we want to check parent features as well.
     * @return true if the feature is contained in the list of de-reexposed features
     */
    public boolean containsDeReexposedFeature(SelectionFeature selectionFeature, boolean recursive) {
        if (selectionFeature == null) {
            return false;
        } else if (reexposedList.contains(selectionFeature)) {
            return true;
        }
        return recursive && containsDeReexposedFeature(selectionFeature.getParentFeature(), recursive);
    }

    /**
     * Returns a set of re-exposed features coming from the same model as the given {@link SelectionFeature}.
     * Only consider descendants with the same reuse as the given feature.
     * 
     * @param feature - The current {@link SelectionFeature} considered
     * @return the set of re-exposed features
     */
    public Set<COREFeature> collectReexposed(SelectionFeature feature) {
        return collectReexposed(feature.getCoreModelReuse(), feature.getRootParent());
    }

    /**
     * Returns a set of re-exposed features coming from the same model as the given {@link SelectionFeature}.
     * Only consider descendants with the given reuse.
     * 
     * @param reuse - The {@link COREReuse} re-exposed features should come from
     * @param feature - The current {@link SelectionFeature} considered
     * @return the set of re-exposed features
     */
    private Set<COREFeature> collectReexposed(COREModelReuse reuse, SelectionFeature feature) {
        Set<COREFeature> result = new HashSet<COREFeature>();

        // We don't want features that come from reuses, we can stop looking.
        if (reuse == null && feature.getCoreModelReuse() != null) {
            return result;
        } else if (reuse == feature.getCoreModelReuse()) {
            if (feature.getSelectionStatus() == FeatureSelectionStatus.RE_EXPOSED) {
                result.add(feature.getCoreFeature());
            }
        }

        // Look for children
        for (SelectionFeature child : feature.getChildrenFeatures()) {
            if (child.getCoreModelReuse() != reuse) {
                continue;
            }
            result.addAll(collectReexposed(reuse, child));
        }

        return result;
    }
    
    /**
     * Returns a set of selected features in the diagram.

     * @return the set of selected features
     */
    public Set<COREFeature> collectSelected() {
        return collectSelected(rootFeature);
    }

    /**
     * Returns a set of selected features coming from the same model as the given {@link SelectionFeature}.
     * Only consider descendants with the same reuse as the given feature.
     * 
     * @param feature - The current {@link SelectionFeature} considered
     * @return the set of selected features
     */
    public Set<COREFeature> collectSelected(SelectionFeature feature) {
        return collectSelected(feature.getCoreModelReuse(), feature.getRootParent());
    }

    /**
     * Returns a set of selected features. Returns a set of selected features,
     * descendant of the given {@link SelectionFeature}, only considers descendants coming from the given reuse.
     * 
     * @param reuse - The {@link COREReuse} selected features should come from
     * @param feature - The current {@link SelectionFeature} considered
     * @return the set of selected features
     */
    private Set<COREFeature> collectSelected(COREModelReuse reuse, SelectionFeature feature) {
        Set<COREFeature> result = new HashSet<COREFeature>();

        // We don't want features that come from reuses, we can stop looking.
        if (reuse == null && feature.getCoreModelReuse() != null) {
            return result;
        } else if (reuse == feature.getCoreModelReuse()) {
            if (feature.isSelected()) {
                result.add(feature.getCoreFeature());
            }
        }

        // Look for children
        for (SelectionFeature child : feature.getChildrenFeatures()) {
            if (child.getCoreModelReuse() != reuse) {
                continue;
            }
            result.addAll(collectSelected(reuse, child));
        }

        return result;
    }

    /**
     * Update a feature status and its children statuses recursively.
     * 
     * @param feature - the {@link SelectionFeature} to consider
     */
    private void autoSelectFeature(SelectionFeature feature) {
        // Check if the feature or one of its children has been selected
        boolean featureSelected = containsSelectedFeature(feature, false);
        boolean childSelected = featureSelected
                || containsSelectedFeature(feature, true);

        // A child has been selected => Automatically select the feature
        if (!featureSelected && childSelected) {
            feature.setStatus(FeatureSelectionStatus.AUTO_SELECTED);
            for (SelectionFeature child : feature.getChildrenFeatures()) {
                if (child.getCoreFeature().getParentRelationship() == COREFeatureRelationshipType.MANDATORY) {
                    child.setStatus(FeatureSelectionStatus.AUTO_SELECTED);
                }
                autoSelectFeature(child);
            }
        } else if (featureSelected) {
            // The feature is selected => Select the feature
            feature.setStatus(FeatureSelectionStatus.SELECTED);
            for (SelectionFeature child : feature.getChildrenFeatures()) {
                if (child.getParentRelationship() == COREFeatureRelationshipType.MANDATORY) {
                    child.setStatus(FeatureSelectionStatus.AUTO_SELECTED);
                }
                autoSelectFeature(child);
            }
        }

        // The feature is Auto-Selected => Select children if they are mandatory
        if (feature.getSelectionStatus() == FeatureSelectionStatus.AUTO_SELECTED) {
            for (SelectionFeature child : feature.getChildrenFeatures()) {
                if (child.getParentRelationship() == COREFeatureRelationshipType.MANDATORY) {
                    child.setStatus(FeatureSelectionStatus.AUTO_SELECTED);
                }
                autoSelectFeature(child);
            }
        }

        // Call recursively for children to be able to check for clashes
        for (SelectionFeature child : feature.getChildrenFeatures()) {
            autoSelectFeature(child);
        }

        // For 'XOR' or 'OR' relationship, check potential clashes
        if (feature.getChildrenRelationship() == COREFeatureRelationshipType.XOR
                || feature.getChildrenRelationship() == COREFeatureRelationshipType.OR) {
            // get list of direct children that are selected or auto-selected (or in warning status)
            Set<SelectionFeature> directSelectedChildren = getSelectedChildrenOneLevel(feature);
            boolean selected = feature.isSelected();
            boolean reExposed = feature.getSelectionStatus() == FeatureSelectionStatus.RE_EXPOSED;

            // There are more than one selected child and it's a 'XOR' => put selected ones in 'Warning' status
            if (selected && directSelectedChildren.size() > 1
                    && feature.getChildrenRelationship() == COREFeatureRelationshipType.XOR) {
                for (SelectionFeature child : directSelectedChildren) {
                    if (child.getSelectionStatus() == FeatureSelectionStatus.AUTO_SELECTED) {
                        child.setStatus(FeatureSelectionStatus.WARNING_AUTO_SELECTED);
                    } else if (child.getSelectionStatus() == FeatureSelectionStatus.SELECTED) {
                        child.setStatus(FeatureSelectionStatus.WARNING_SELECTED);
                    } else if (child.getSelectionStatus() != FeatureSelectionStatus.WARNING_SELECTED
                            && child.getSelectionStatus() != FeatureSelectionStatus.WARNING_AUTO_SELECTED) {
                        clearReexpose(child);
                    }
                }
                // There is only one selected child and it's an XOR => clear re-expose from children
            } else if (directSelectedChildren.size() == 1
                    && feature.getChildrenRelationship() == COREFeatureRelationshipType.XOR) {
                for (SelectionFeature child : feature.getChildrenFeatures()) {
                    if (!child.isSelected()) {
                        clearReexpose(child);
                    }
                }
                // There are no selected child
            } else if (selected || reExposed) {
                List<SelectionFeature> reexposed = new LinkedList<SelectionFeature>();
                // Get all re-exposed children
                for (SelectionFeature child : feature.getChildrenFeatures()) {
                    if (child.getSelectionStatus() != FeatureSelectionStatus.NOT_SELECTED
                            && child.getSelectionStatus() != FeatureSelectionStatus.WARNING_NOT_SELECTED
                            && !child.isReuse()) {
                        reexposed.add(child);
                    }
                }
                // Only one is re-exposed : it can be automatically selected
                if (selected && reexposed.size() == 1) {
                    reexposed.get(0).setStatus(FeatureSelectionStatus.AUTO_SELECTED);
                    autoSelectFeature(reexposed.get(0));
                }

                if (!reexposed.isEmpty()) {
                    return;
                }
                // Nothing is re-exposed : warning.
                for (SelectionFeature child : feature.getChildrenFeatures()) {
                    child.setStatus(FeatureSelectionStatus.WARNING_NOT_SELECTED);
                }
            }
        }
        // If a mandatory feature is de-reExposed and its parent is reExposed : invalid
        if (feature.getParentRelationship() == COREFeatureRelationshipType.MANDATORY
                && feature.getSelectionStatus() == FeatureSelectionStatus.NOT_SELECTED
                && feature.getParentFeature().getSelectionStatus() == FeatureSelectionStatus.RE_EXPOSED) {
            feature.setStatus(FeatureSelectionStatus.WARNING_NOT_SELECTED);
        }
    }

    /**
     * Function used to clear a feature and its children back to their default state (ie re-exposed).
     * 
     * @param feature - the feature to clear
     */
    private void clearFeature(SelectionFeature feature) {
        feature.setStatus(FeatureSelectionStatus.RE_EXPOSED);

        for (SelectionFeature child : feature.getChildrenFeatures()) {
            if (containsDeReexposedFeature(child, false)) {
                clearReexpose(child);
            } else {
                clearFeature(child);
            }
        }
    }

    /**
     * Function used to clear a feature and its children back to unselected state (not re-exposed).
     * 
     * @param feature - the feature to clear
     */
    private void clearReexpose(SelectionFeature feature) {
        feature.setStatus(FeatureSelectionStatus.NOT_SELECTED);

        for (SelectionFeature child : feature.getChildrenFeatures()) {
            clearReexpose(child);
        }
    }

    /**
     * Function used to check if any children are present in the immediate one level down.
     * 
     * @param feature - The feature which is used to check.
     * @return collectedChildren - The set of all collected children.
     */
    private static Set<SelectionFeature> getSelectedChildrenOneLevel(SelectionFeature feature) {
        Set<SelectionFeature> collectedChildren = new HashSet<SelectionFeature>();

        for (SelectionFeature childFeature : feature.getChildrenFeatures()) {
            // TODO temp? isReuse is needed in case a reuse is placed alongside an XOR relationship
            // We may want to add a mandatory dummy feature as parent of the XOR relationship instead
            if (!childFeature.isReuse() && childFeature.isSelected()) {
                collectedChildren.add(childFeature);
            }
        }
        return collectedChildren;
    }

//    /**
//     * Create a {@link COREConcernConfiguration} from the current selection of the container.
//     * 
//     * @return The created {@link COREConcernConfiguration}.
//     */
//    public COREConcernConfiguration getSelectedConcernConfiguration() {
//        return (COREConcernConfiguration) getSelectedConfiguration(false);
//    }
//
//    /**
//     * Create a {@link COREReuseConfiguration} from the current selection of the container.
//     * 
//     * @return The created {@link COREReuseConfiguration}.
//     */
//    public COREReuseConfiguration getSelectedReuseConfiguration() {
//        return (COREReuseConfiguration) getSelectedConfiguration(true);
//    }

    /**
     * Create a {@link COREConfiguration} from the current selection of the container.
     * 
     * @return The created {@link COREConfiguration}.
     */
    public COREConfiguration getSelectedConfiguration() {
        return getSelectedConfiguration(rootFeature);
    }

    /**
     * Create a {@link COREConfiguration} from the current selection of the container.
     * 
     * @param selectionFeature - Current {@link SelectionFeature} to consider.
     * @return The created {@link COREConfiguration}.
     */
    private COREConfiguration getSelectedConfiguration(SelectionFeature selectionFeature) {
        COREConfiguration configuration = CoreFactory.eINSTANCE.createCOREConfiguration();

        configuration.setSource((COREFeatureModel) selectionFeature.getCoreFeature().eContainer());
        // Base selection
        configuration.getSelected().addAll(collectSelected(selectionFeature));
        configuration.getReexposed().addAll(collectReexposed(selectionFeature));

        return configuration;
    }

    /**
     * D.
     * @return d
     */
    public Map<COREReuse, COREConfiguration> getReuseDefaultConfiguration() {
        return reuseDefaultConfiguration;
    }  
    
    /**
     * D.
     * @param reuse 
     * @param configuration 
     */
    public void mapReuseToDefaultConfiguration(COREReuse reuse, COREConfiguration configuration) {
        reuseDefaultConfiguration.put(reuse, configuration);
    }

    /**
     * Get the reuseUserConfiguration Map.
     * @return reuseUserConfiguration 
     */
    public Map<COREReuse, Map<COREFeature, FeatureSelectionStatus>> getReuseUserConfiguration() {
        return reuseUserConfiguration;
    }
    
    /**
     * Maps a reuse to an empty user configuration.
     * @param reuse 
     */
    public void mapReuseToUserConfiguration(COREReuse reuse) {
        if (!reuseUserConfiguration.containsKey(reuse)) {
            reuseUserConfiguration.put(reuse, new HashMap<COREFeature, FeatureSelectionStatus>());
        }
    }
    /**
     * Add a user selection choice (of a previously reexposed feature) to the user configuration map.
     * @param reuse 
     * @param feature 
     * @param status 
     */
    public void addSelectionToUserConfiguration(COREReuse reuse, COREFeature feature, FeatureSelectionStatus status) {
        reuseUserConfiguration.get(reuse).put(feature, status);
    }
        
}
