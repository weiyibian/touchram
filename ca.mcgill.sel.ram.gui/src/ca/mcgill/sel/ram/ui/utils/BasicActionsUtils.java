package ca.mcgill.sel.ram.ui.utils;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.emf.common.command.BasicCommandStack;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature.Setting;
import org.eclipse.emf.ecore.resource.Resource;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREExternalLanguage;
import ca.mcgill.sel.core.evaluator.im.ScaleFactorAndOffsetCalculator;
import ca.mcgill.sel.core.language.registry.CORELanguageRegistry;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.ram.Operation;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.provider.util.RAMEditUtil;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamPopup.PopupType;
import ca.mcgill.sel.ram.ui.components.browser.GenericFileBrowser;
import ca.mcgill.sel.ram.ui.components.browser.interfaces.FileBrowserListener;

/**
 * Implementation of stateless basic action.
 * 
 * @author g.Nicolas
 *
 */
public final class BasicActionsUtils {

    /**
     * Creates a new instance.
     */
    private BasicActionsUtils() {

    }

    /**
     * Call the undo command on the stack.
     * 
     * @param element - the aspect/concern to save
     */
    public static void undo(EObject element) {
        if (element != null) {
            EMFEditUtil.getCommandStack(element).undo();
        }
    }

    /**
     * Call the redo command on the stack.
     * 
     * @param element - the aspect/concern to save
     */
    public static void redo(EObject element) {
        if (element != null) {
            EMFEditUtil.getCommandStack(element).redo();
        }
    }

    /**
     * Saves a concern with all its models.
     *
     * @param concern the concern to save
     */
    public static void saveConcern(COREConcern concern) {
        BasicCommandStack commandStack = EMFEditUtil.getCommandStack(concern);

        Map<Object, Object> saveOptions = new HashMap<Object, Object>();
        saveOptions.put(Resource.OPTION_SAVE_ONLY_IF_CHANGED, Resource.OPTION_SAVE_ONLY_IF_CHANGED_MEMORY_BUFFER);

        try {
            // Calculate scale factor for the impact model
            if (concern.getImpactModel() != null && concern.getImpactModel().getImpactModelElements().size() > 0) {
                ScaleFactorAndOffsetCalculator.calculateScaleFactorsAndOffsets(concern.getImpactModel(),
                        concern.getFeatureModel());
            }

            // Save the concern
            concern.eResource().save(saveOptions);
            commandStack.saveIsDone();

            // TODO: Discuss whether we want to save the external artefacts when we save a concern or not
            // Build a list of concern models and realized by of the feature.
//            Set<COREArtefact> models = new HashSet<COREArtefact>(concern.getModels());
//            for (COREArtefact model : models) {
//                // Save only if changed.
//                try {
//                    model.eResource().save(saveOptions);
//                    BasicCommandStack modelStack = EMFEditUtil.getCommandStack(model);
//                    modelStack.saveIsDone();
//                } catch (NullPointerException npe) {
//                    // TODO temp fix to prevent errors during save if models can't be saved.
//                    // Should handle it by removing all references to this model in the concern.
//                    LoggerUtils.error("Model '" + model + "' couldn't be saved. The file may not have been found.");
//                }
//            }
            // CHECKSTYLE:IGNORE IllegalCatch: Many exceptions can occur and we don't want to crash the application.
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Saves a model. Presents a file browser to allow to specify the file if it has not been saved before already.
     * Otherwise just override the file. Optionally, a listener can be passed in order to be informed when the aspect
     * was saved. If the listener doesn't get called, the file browser was cancelled.
     *
     * @param model the model to save
     * @param listener the {@link ca.mcgill.sel.ram.ui.components.browser.interfaces.RamFileBrowserListener} that should
     *            be informed when the aspect was saved, null if not interested
     */
    public static void saveModel(final EObject model, final FileBrowserListener listener) {
        // Check for uncontained cross references before allowing to save.
        Map<EObject, Collection<Setting>> uncontainedCrossReferences =
                EMFModelUtil.findUncontainedCrossReferences(model.eResource());
        
        if (!uncontainedCrossReferences.isEmpty()) {
            String errorString = buildErrorMessage(uncontainedCrossReferences);
            
            RamApp.getActiveScene().displayPopup(errorString, PopupType.ERROR);
        } else {
            // Already saved
            if (model.eResource() != null) {
                try {
                    File file = new File(model.eResource().getURI().toFileString());
    
                    final Map<Object, Object> saveOptions = new HashMap<Object, Object>();
                    saveOptions.put(
                            Resource.OPTION_SAVE_ONLY_IF_CHANGED,
                            Resource.OPTION_SAVE_ONLY_IF_CHANGED_MEMORY_BUFFER);
    
                    model.eResource().save(saveOptions);
    
                    BasicCommandStack commandStack = EMFEditUtil.getCommandStack(model);
                    commandStack.saveIsDone();
                    
                    // Save the concern
                    COREConcern concern = COREArtefactUtil.getReferencingExternalArtefact(model).getCoreConcern();
                    if (concern != null) {
                        concern.eResource().save(saveOptions);
                        BasicCommandStack concernCommandStack = EMFEditUtil.getCommandStack(concern);
                        concernCommandStack.saveIsDone();
                    }
    
                    // Inform the listener to be able to do some additional tasks.
                    if (listener != null) {
                        listener.modelSaved(file);
                    }
                } catch (IOException e) {
                    RamApp.getActiveScene()
                            .displayPopup(Strings.POPUP_ERROR_FILE_SAVE + e.getMessage(), PopupType.ERROR);
                }
            } else {
                saveModelAs(model, listener);
            }
        }
    }

    /**
     * Builds the error message for all uncontained cross references.
     * 
     * @param uncontainedCrossReferences the map of uncontained cross references
     * @return the complete error message
     */
    private static String buildErrorMessage(Map<EObject, Collection<Setting>> uncontainedCrossReferences) {
        StringBuilder builder = new StringBuilder(String.format("%s:\n", Strings.POPUP_SAVE_UNCONTAINED));
        
        for (Entry<EObject, Collection<Setting>> entry : uncontainedCrossReferences.entrySet()) {
            EObject eObject = entry.getKey();
            builder.append(EMFEditUtil.getTypeName(eObject));
            builder.append(" ");
            builder.append(EMFEditUtil.getText(eObject));
            builder.append(String.format(" %s: \n", Strings.POPUP_SAVE_UNCONTAINED_REFERENCED));
            
            for (Setting setting : entry.getValue()) {
                EObject referencedBy = setting.getEObject();
                builder.append(" - ");
                AdapterFactory adapterFactory = EMFEditUtil.getAdapterFactory(referencedBy);
                if (referencedBy instanceof Operation) {
                    Operation operation = (Operation) referencedBy;
                    
                    builder.append(RAMEditUtil.getOperationSignature(adapterFactory, operation, false, false));
                } else {
                    builder.append(EMFEditUtil.getTypeName(referencedBy));
                    builder.append(" ");
                    builder.append(EMFEditUtil.getTextFor(adapterFactory, referencedBy));
                }
                
                EObject container = getContainer(referencedBy);
                if (container != null) {
                    builder.append(String.format(" %s ", Strings.POPUP_SAVE_UNCONTAINED_CONTAINER));
                    builder.append(EMFEditUtil.getTypeName(container));
                    builder.append(" ");
                    builder.append(EMFEditUtil.getText(container));
                }
                
                builder.append("\n");
            }
            
            builder.append("\n");
        }
        return builder.toString();
    }
    
    /**
     * Returns a container of interest for the given object.
     * A container of interest is one whose name can be shown to the user to help locating an element inside.
     * 
     * @param object the object whose container to find
     * @return a direct or indirect container for the given object
     */
    private static EObject getContainer(EObject object) {
        EObject container = null;
        
        // Try whether the object is contained in one of the known "super-containers" that we are interested in.
        // Abstract Message View
        container = EMFModelUtil.getRootContainerOfType(object, RamPackage.Literals.ABSTRACT_MESSAGE_VIEW);
        
        // Classifier
        if (container == null) {
            container = EMFModelUtil.getRootContainerOfType(object, RamPackage.Literals.CLASSIFIER);
        }
        
        // State View
        if (container == null) {
            container = EMFModelUtil.getRootContainerOfType(object, RamPackage.Literals.STATE_VIEW);
        }
        
        return container;
    }

    /**
     * Saves a model. Presents a file browser to allow to specify the file. Optionally, a listener can be passed in
     * order to be informed when the aspect was saved. If the listener doesn't get called, the file browser was
     * cancelled.
     *
     * @param model the model to save
     * @param listener the {@link ca.mcgill.sel.ram.ui.components.browser.interfaces.RamFileBrowserListener} that should
     *            be informed when the aspect was saved, null if not interested
     */
    public static void saveModelAs(final EObject model, final FileBrowserListener listener) {
        COREExternalArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(model);
        COREExternalLanguage language = CORELanguageRegistry.getRegistry().getLanguage(artefact.getLanguageName());
        
        GenericFileBrowser.saveModel(model, language.getFileExtension(), artefact.getName(), new FileBrowserListener() {
            @Override
            public void modelSaved(File file) {
                try {
                    // Tell the command stack that the aspect was saved.
                    BasicCommandStack commandStack = EMFEditUtil.getCommandStack(model);
                    commandStack.saveIsDone();
                    // Save the concern
                    COREConcern concern = COREArtefactUtil.getReferencingExternalArtefact(model).getCoreConcern();
                    if (concern != null) {
                        Map<Object, Object> saveOptions = new HashMap<Object, Object>();
                        saveOptions.put(Resource.OPTION_SAVE_ONLY_IF_CHANGED,
                                Resource.OPTION_SAVE_ONLY_IF_CHANGED_MEMORY_BUFFER);
                        concern.eResource().save(saveOptions);
                        BasicCommandStack concernCommandStack = EMFEditUtil.getCommandStack(concern);
                        concernCommandStack.saveIsDone();
                    }
                    // CHECKSTYLE:IGNORE IllegalCatch: Many exceptions can occur and we don't want to crash the app
                } catch (Exception e) {
                    e.printStackTrace();
                }
                // Inform the listener to be able to do some additional tasks.
                if (listener != null) {
                    listener.modelSaved(file);
                }
            }
            @Override
            public void modelLoaded(EObject model) {
            }
        });
    }

}
