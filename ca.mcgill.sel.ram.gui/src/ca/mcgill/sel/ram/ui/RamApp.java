package ca.mcgill.sel.ram.ui;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.swing.JFrame;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEMap.Entry;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.mt4j.MTApplication;
import org.mt4j.components.MTCanvas;
import org.mt4j.input.DesktopInputManager;
import org.mt4j.input.InputManager;
import org.mt4j.input.inputSources.MouseInputSource;
import org.mt4j.input.inputSources.Tuio2DCursorInputSource;
import org.mt4j.input.inputSources.Tuio2dObjectInputSource;
import org.mt4j.input.inputSources.Win7NativeTouchSource;
import org.mt4j.sceneManagement.AbstractScene;
import org.mt4j.sceneManagement.Iscene;
import org.mt4j.sceneManagement.transition.SlideTransition;
import org.mt4j.util.MT4jSettings;

import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.classdiagram.ui.scenes.DisplayClassDiagramScene;
import ca.mcgill.sel.classdiagram.ui.views.handler.ClassDiagramHandlerFactory;
import ca.mcgill.sel.commons.LoggerUtils;
import ca.mcgill.sel.commons.ResourceUtil;
import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREImpactModel;
import ca.mcgill.sel.core.COREImpactNode;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.controller.COREControllerFactory;
import ca.mcgill.sel.core.controller.ImpactModelElementController;
import ca.mcgill.sel.core.evaluator.im.PropagationResult;
import ca.mcgill.sel.core.impl.LayoutContainerMapImpl;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.ui.components.ConfirmPopup;
import ca.mcgill.sel.ram.ui.components.RamPopup.PopupType;
import ca.mcgill.sel.ram.ui.components.browser.interfaces.FileBrowserListener;
import ca.mcgill.sel.ram.ui.components.navigationbar.NavigationBar;
import ca.mcgill.sel.ram.ui.events.RamKeyboardInputSource;
import ca.mcgill.sel.ram.ui.scenes.DisplayAspectScene;
import ca.mcgill.sel.ram.ui.scenes.DisplayConcernEditScene;
import ca.mcgill.sel.ram.ui.scenes.DisplayConcernSelectScene;
import ca.mcgill.sel.ram.ui.scenes.DisplayImpactModelEditScene;
import ca.mcgill.sel.ram.ui.scenes.DisplayImpactModelSelectScene;
import ca.mcgill.sel.ram.ui.scenes.RamAbstractScene;
import ca.mcgill.sel.ram.ui.scenes.TouchCOREStartupScene;
import ca.mcgill.sel.ram.ui.scenes.handler.IConcernEditSceneHandler;
import ca.mcgill.sel.ram.ui.scenes.handler.IConcernSelectSceneHandler;
import ca.mcgill.sel.ram.ui.utils.BasicActionsUtils;
import ca.mcgill.sel.ram.ui.utils.GUIConstants;
import ca.mcgill.sel.ram.ui.utils.GraphicalUpdater;
import ca.mcgill.sel.ram.ui.utils.ImpactModelUtil;
import ca.mcgill.sel.ram.ui.utils.OSXAdapter;
import ca.mcgill.sel.ram.ui.utils.ResourceUtils;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.utils.ResourceUtils.OperatingSystem;
import ca.mcgill.sel.ram.ui.views.handler.HandlerFactory;
import ca.mcgill.sel.ram.ui.views.handler.IDisplaySceneHandler;
import ca.mcgill.sel.ram.ui.views.structural.StructuralDiagramView;
import ca.mcgill.sel.usecases.UseCaseModel;
import ca.mcgill.sel.usecases.ui.scenes.DisplayUseCaseDiagramScene;
import ca.mcgill.sel.usecases.ui.utils.UseCaseDiagramHandlerFactory;
import ca.mcgill.sel.restif.RestIF;
import ca.mcgill.sel.restif.ui.scenes.DisplayRestTreeScene;
import ca.mcgill.sel.restif.ui.views.handler.RestTreeHandlerFactory;
import processing.event.KeyEvent;

/**
 * Contains the current {@link MTApplication} instance. This can be retrieved using the getApplication() method for easy
 * of use. Since creating an application is asynchronous there is no way to create a new application and return it on
 * demand, so it is possible for the current application to be null.
 *
 * @author vbonnet
 * @author eyildirim
 * @author mschoettle
 * @author nishanth
 * @author omar
 */
public class RamApp extends MTApplication {

    /**
     * The different drawing modes available for RAM applications.
     *
     * @author vbonnet
     */
    public enum DisplayMode {
        /**
         * Pretty mode, take some extra time to make the display prettier.
         */
        PRETTY,
        /**
         * Be as efficient as possible, don't bother rearranging things.
         */
        FAST
    }

    /**
     * Default id for default purposes.
     */
    private static final long serialVersionUID = 1L;

    private static RamApp instance;

    private static Runnable onLoad;

    private final Set<EObject> loadedModels;

    private final Map<EObject, GraphicalUpdater> graphicalUpdaters;

    private DisplayMode displayMode;

    private AbstractScene backgroundScene;

    /**
     * Creates a new RAM application. Would be private except that MTApplication requires it to be public.
     */
    public RamApp() {
        displayMode = DisplayMode.PRETTY;
        loadedModels = new HashSet<EObject>();
        graphicalUpdaters = new HashMap<EObject, GraphicalUpdater>();
        // create our own desktop manager
        final DesktopInputManager desktopManager = new DesktopInputManager(this, false);
        // setting it now will stop a new one from being created
        setInputManager(desktopManager);
    }

    /**
     * Returns the current active {@link DisplayAspectScene} if there is an active one.
     *
     * @return the current active {@link DisplayAspectScene}, null if there is none active
     */
    // TODO Modify this to support more than one scene. NOTE: Will cause MAJOR cross module changes.
    public static DisplayAspectScene getActiveAspectScene() {
        final Iscene scene = getApplication().getCurrentScene();
        if (scene instanceof DisplayAspectScene) {
            return (DisplayAspectScene) scene;
        } else {
            return null;
        }
    }

    /**
     * Returns the current active {@link DisplayConcernEditScene} if there is an active one.
     *
     * @return the current active {@link DisplayConcernEditScene}, null if there is none active
     */
    public static DisplayConcernEditScene getActiveConcernEditScene() {
        final Iscene scene = getApplication().getCurrentScene();
        if (scene instanceof DisplayConcernEditScene) {
            return (DisplayConcernEditScene) scene;
        } else {
            return null;
        }
    }

    /**
     * Returns the current active {@link DisplayConcernSelectScene} if there is an active one.
     *
     * @return the current active {@link DisplayConcernSelectScene}, null if there is none active
     */
    public static DisplayConcernSelectScene getActiveConcernSelectScene() {
        final Iscene scene = getApplication().getCurrentScene();
        if (scene instanceof DisplayConcernSelectScene) {
            return (DisplayConcernSelectScene) scene;
        } else {
            return null;
        }
    }

    /**
     * Returns the current active {@link RamAbstractScene}.
     *
     * @return the current active {@link RamAbstractScene}
     */
    public static RamAbstractScene<?> getActiveScene() {
        final RamAbstractScene<?> scene = (RamAbstractScene<?>) getApplication().getCurrentScene();
        return scene;
    }

    /**
     * Returns the instance of RamApp running.
     *
     * @return the current MTApplication instance. CAN be null if no application has finished initializing.
     */
    public static RamApp getApplication() {
        return RamApp.instance;
    }

    /**
     * Returns the display mode of the instance.
     *
     * @return The display mode for the current application.
     */
    public static DisplayMode getDisplayMode() {
        return instance.displayMode;
    }

    /**
     * Initializes the application.
     */
    public static void initialize() {
        MTApplication.initialize();
    }

    /**
     * Initializes the application. This should be the FIRST mt4j method called and should only be called once.
     *
     * @param callback The callback to invoke when the application is done loading.
     */
    public static void initialize(final Runnable callback) {
        onLoad = callback;
        // This needs to be the FIRST thing called because mt4j decrees it so
        // Also it should be called from MTApplication, so redirect.
        MTApplication.initialize();
    }

    /**
     * Sets the display mode for the currently running application.
     *
     * @param mode The mode to set.
     */
    public static void setDisplayMode(final DisplayMode mode) {
        instance.displayMode = mode;
    }

    /**
     * Returns the active structural view of the RamApp instance. Returns null if aspect scene is not the active scene.
     *
     * @return The structural view of the active {@link DisplayAspectScene}, null if the current scene is of another
     *         type.
     */
    public static StructuralDiagramView getActiveStructuralView() {
        return getApplication().getStructuralView();
    }

    /**
     * Function called to close the aspect scene. Destroys the scene, unregisters the scene, and unloads the aspect.
     *
     * @param scene - The DisplayAspectScene which is passed to the function to be closed.
     */
    public void closeAspectScene(final DisplayAspectScene scene) {
        synchronized (loadedModels) {
            if (scene == getCurrentScene()) {
                changeScene(backgroundScene);
            }

            Aspect aspect = scene.getAspect();
            // unregister the aspect from the the main ram app
            unregisterAspect(aspect);
            // destroy the scene manually (call to destroySceneAfterTransition has no effect since there is no
            // transition)
            scene.destroy();
            destroySceneAfterTransition(scene);
            // remove it
            removeScene(scene);
        }
    }

    /**
     * Returns the canvas of the current scene.
     *
     * @return The canvas of the currently loaded scene.
     */
    public MTCanvas getCanvas() {
        return getCurrentScene().getCanvas();
    }

    /**
     * The function loads the concern passed and registers the scene and changes it accordingly.
     *
     * @param concern - the concern to display
     */
    public void displayConcernFeatureModelEditScene(final COREConcern concern) {
        invokeLater(new Runnable() {
            @Override
            public void run() {
                IConcernEditSceneHandler handler = HandlerFactory.INSTANCE.getDisplayConcernFMSceneHandler();
                DisplayConcernEditScene scene = null;
                scene = new DisplayConcernEditScene(RamApp.this, concern);
                scene.setHandler(handler);
                addScene(scene);
                changeScene(scene);
            }
        });
    }

    /**
     * This function loads the module of the Feature Model Select.
     *
     * @param concern - The concern which is being reused.
     * @param handler - The handler to use.
     * @param artefact - The artefact that is making the reuse
     * @param extendingReuse - If we are extending a reuse. null if not
     */
    public void displayFeatureModelSelectScene(final COREConcern concern,
            final IConcernSelectSceneHandler handler, final COREArtefact artefact, COREReuse extendingReuse) {
        invokeLater(new Runnable() {
            @Override
            public void run() {
                DisplayConcernSelectScene scene = new DisplayConcernSelectScene(RamApp.this, concern,
                        (COREExternalArtefact) artefact, extendingReuse);

                scene.setHandler(handler);
                addScene(scene);
                changeScene(scene);
            }
        });
    }

    /**
     * Display the impact model.
     *
     * @param concern - The concern which is being reused.
     * @param rootNode the root node to display
     */
    public void showImpactModel(final COREConcern concern, COREImpactNode rootNode) {

        final COREImpactModel impactModel = concern.getImpactModel();
        final String name = concern.getName();

        LayoutContainerMapImpl mapLayout = EMFModelUtil.getEntryFromMap(impactModel.getLayouts(), rootNode);
        if (mapLayout == null) {
            ImpactModelElementController controller = COREControllerFactory.INSTANCE.getImpactModelElementController();

            if (rootNode == null) {
                controller.createRootImpactModelElement(
                        impactModel,
                        ImpactModelUtil.getUniqueGoalName(impactModel), getWidth() / 2,
                        GUIConstants.ROOT_GOAL_INITIAL_HEIGHT);
            } else {
                controller.createLayoutContainer(rootNode,
                        getWidth() / 2, GUIConstants.ROOT_GOAL_INITIAL_HEIGHT);
            }
        } else {
            displayEditImpactModel(concern, impactModel, name, rootNode);
        }
    }

    /**
     * Operation called to display the Impact Model.
     *
     * @param name - The name of the scene.
     * @param concern - The concern name.
     * @param impactModel - The Impact Model.
     * @param rootNode the root node of this scene
     */
    public void displayEditImpactModel(COREConcern concern, COREImpactModel impactModel, String name,
            COREImpactNode rootNode) {
        DisplayImpactModelEditScene scene =
                new DisplayImpactModelEditScene(getApplication(), name, concern, impactModel, rootNode);

        scene.setTransition(new SlideTransition(RamApp.getApplication(), 700, true));
        // TODO HandlerFactory
        scene.setHandler(HandlerFactory.INSTANCE.getDefaultRamSceneHandler());

        addScene(scene);
        changeScene(scene);

    }

    /**
     * Display the impact model.
     *
     * @param concern - The concern which is being reused.
     * @param rootNode the root node to display
     * @param propagationResult The result of the evaluation. It associates impact model elements with their weight
     */
    public void showImpactModelSelectMode(final COREConcern concern, COREImpactNode rootNode,
            final PropagationResult propagationResult) {

        final COREImpactModel impactModel = concern.getImpactModel();
        final String name = concern.getName();

        LayoutContainerMapImpl mapLayout = EMFModelUtil.getEntryFromMap(impactModel.getLayouts(), rootNode);
        if (mapLayout == null) {
            // Register a listener so that the goal can be displayed
            // when it was successfully added.
            impactModel.eAdapters().add(new EContentAdapter() {
                @Override
                public void notifyChanged(Notification notification) {
                    if (notification.getFeature() == CorePackage.Literals.CORE_IMPACT_MODEL__LAYOUTS) {
                        final LayoutContainerMapImpl containerMapImpl;
                        switch (notification.getEventType()) {
                            case Notification.ADD:
                                impactModel.eAdapters().remove(this);
                                containerMapImpl = (LayoutContainerMapImpl) notification.getNewValue();

                                containerMapImpl.eAdapters().add(new EContentAdapter() {
                                    @Override
                                    public void notifyChanged(Notification notif) {
                                        if (notif.getFeature() == CorePackage.Literals.LAYOUT_CONTAINER_MAP__VALUE) {
                                            switch (notif.getEventType()) {
                                                case Notification.ADD:
                                                    containerMapImpl.eAdapters().remove(this);
                                                    Entry<?, ?> entry = (Entry<?, ?>) notif.getNewValue();

                                                    displaySelectImpactModel(concern, impactModel, name,
                                                            (COREImpactNode) entry.getKey(), propagationResult);
                                            }
                                        }
                                    }
                                });
                        }
                    }
                }
            });

            COREControllerFactory.INSTANCE.getImpactModelElementController().createLayoutContainer(rootNode,
                    getWidth() / 2, GUIConstants.ROOT_GOAL_INITIAL_HEIGHT);
        } else {
            displaySelectImpactModel(concern, impactModel, name, rootNode, propagationResult);
        }
    }

    /**
     * Operation called to display the Impact Model.
     *
     * @param name - The name of the scene.
     * @param concern - The concern name.
     * @param impactModel - The Impact Model.
     * @param rootNode the root node of this scene
     * @param propagationResult The result of the evaluation. It associates impact model elements with their weight
     */
    public void displaySelectImpactModel(COREConcern concern, COREImpactModel impactModel, String name,
            COREImpactNode rootNode, PropagationResult propagationResult) {
        DisplayImpactModelSelectScene scene =
                new DisplayImpactModelSelectScene(getApplication(), name, impactModel, rootNode,
                        propagationResult);

        scene.setTransition(new SlideTransition(RamApp.getApplication(), 700, true));

        addScene(scene);
        changeScene(scene);
    }

    /**
     * This function gets the scene of an aspect from loaded aspects but if that aspect has not been loaded before it
     * creates one scene with that aspect. If there is an error during initialization, null is returned, which the
     * caller should check for.
     *
     * @param model the model to load a scene for
     * @return the loaded {@link DisplayAspectScene}, null if there was an error during initialization
     */
    public RamAbstractScene<IDisplaySceneHandler> getExistingOrCreateModelScene(EObject model) {
        return getExistingOrCreateModelScene(COREArtefactUtil.getReferencingExternalArtefact(model), model);
    }

    /**
     * This function gets the scene of an aspect from loaded aspects but if that aspect has not been loaded before it
     * creates one scene with that aspect. If there is an error during initialization, null is returned, which the
     * caller should check for.
     *
     * @param artefact the artefact to load the scene for
     * @param model the aspect to load a scene for
     * @return the loaded {@link DisplayAspectScene}, null if there was an error during initialization
     */
    public RamAbstractScene<IDisplaySceneHandler> getExistingOrCreateModelScene(COREExternalArtefact artefact,
            EObject model) {
        synchronized (loadedModels) {
            registerModel(model);
            String modelSceneName = getSceneName(model);

            @SuppressWarnings("unchecked")
            RamAbstractScene<IDisplaySceneHandler> scene =
                    (RamAbstractScene<IDisplaySceneHandler>) getScene(modelSceneName);

            if (scene == null) {
                IDisplaySceneHandler handler;
                try {

                    if (model instanceof Aspect) {
                        handler = HandlerFactory.INSTANCE.getDisplayAspectSceneHandler();
                        scene = new DisplayAspectScene(RamApp.this, artefact, (Aspect) model, modelSceneName);
                        ((DisplayAspectScene) scene).setHandler(handler);
                    } else if (model instanceof ClassDiagram) {
                        handler = ClassDiagramHandlerFactory.INSTANCE.getDisplayClassDiagramSceneHandler();
                        scene = new DisplayClassDiagramScene(RamApp.this, artefact,
                                (ClassDiagram) model, modelSceneName);
                    } else if (model instanceof UseCaseModel) {
                        handler = UseCaseDiagramHandlerFactory.INSTANCE.getUseCaseDiagramDisplaySceneHandler();
                        scene = new DisplayUseCaseDiagramScene(RamApp.this, artefact,
                          (UseCaseModel) model, modelSceneName);
                    } else if (model instanceof RestIF) {
                        handler = RestTreeHandlerFactory.INSTANCE.getDisplayRestTreeSceneHandler();
                        scene = new DisplayRestTreeScene(RamApp.this, artefact, (RestIF) model, modelSceneName);
                    }
                    addScene(scene);

                    // CHECKSTYLE:IGNORE IllegalCatch: There could be many different exceptions during initialization.
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return scene;
        }
    }

    /**
     * Creates a unique name for a scene which displays an aspect.
     *
     * @param model the aspect that the scene should display
     * @return the unique name of the aspect scene
     */
    private static String getSceneName(EObject model) {
        String modelString = model.toString();

        // The string contains the name of the aspect. Since an aspect could be renamed,
        // the scene name has to be independent of the name of an aspect.
        // Just strip that part of and use the identify of the object.
        modelString = modelString.substring(0, modelString.indexOf(' '));

        return modelString;
    }

    /**
     * Returns a set of all loaded aspects.
     *
     * @return a {@link Set} of loaded {@link Aspect}s
     */
    public Set<EObject> getLoadedModels() {
        return loadedModels;
    }

    /**
     * Gets the GraphicalUpdater linked to the aspect parameter.
     *
     * @param model The aspect
     * @return The GraphicalUpdater linked to the aspect.
     */
    public GraphicalUpdater getGraphicalUpdaterForModel(EObject model) {
        return graphicalUpdaters.get(model);
    }

    /**
     * Function used to get the structural view from the current scene. Will return null if the current scene is not a
     * DisplayAspectScene
     *
     * @return StructuralDiagramView - The structural diagram view of the current aspect scene.
     */
    private StructuralDiagramView getStructuralView() {
        final Iscene scene = getCurrentScene();
        if (scene != null && scene instanceof DisplayAspectScene) {
            final DisplayAspectScene aspectscene = (DisplayAspectScene) scene;
            return aspectscene.getStructuralDiagramView();
        }
        return null;
    }

    @Override
    protected void handleKeyEvent(KeyEvent event) {
        // handle the ESC key specially
        // to show a confirm popup
        if (event.getAction() == KeyEvent.PRESS && event.getKey() == java.awt.event.KeyEvent.VK_ESCAPE) {
            handleQuitRequest();
            return;
        }

        super.handleKeyEvent(event);
    }

    /**
     * Loads the scene of a given model. If a scene for the given model does not exist yet it
     * will be created.
     *
     * @param artefact the artefact of the model to be loaded
     * @param model the model to display
     */
    public void loadScene(COREExternalArtefact artefact, final EObject model) {
        loadScene(artefact, model, null);
    }

    /**
     * Loads the scene of a given model. If a scene for the given model does not exist yet it
     * will be created.
     *
     * @param artefact the artefact of the model to be loaded
     * @param model - the aspect to display
     * @param handler - the handler to set for the scene. If null, handler of the scene will not be modified.
     */
    public void loadScene(final COREExternalArtefact artefact, final EObject model,
            final IDisplaySceneHandler handler) {

        invokeLater(new Runnable() {
            @Override
            public void run() {
                RamAbstractScene<IDisplaySceneHandler> scene = getExistingOrCreateModelScene(artefact, model);

                if (handler != null) {
                    scene.setHandler(handler);
                }
                if (scene != null) {
                    changeScene(scene);
                } else {
                    getActiveScene().displayPopup(Strings.popupAspectMalformed(model.toString()), PopupType.ERROR);
                }
            }
        });
    }

    /**
     * Creates a scene (but doesn't add it to the application) so that it can be called upon at a later time when
     * someone tries to load that scene.
     *
     * @param model The aspect to register
     */
    public void registerModel(final EObject model) {
        synchronized (loadedModels) {
            loadedModels.add(model);
            if (!graphicalUpdaters.containsKey(model)) {
                graphicalUpdaters.put(model, new GraphicalUpdater());
            }
        }
    }

    /**
     * Function default to register the default input sources.
     *
     * @param inputManager - The inputManager object.
     */
    private void registerDefaultInputSources(final InputManager inputManager) {
        /*
         * This code is pretty much the same as DesktopInputManger#registerDefaultInputSources() except with some
         * comments or useless code removed. It is copied here because the keyboard input is bad and this is sadly the
         * easiest way I found to fix it.
         */
        // boolean enableMultiMouse = false;
        final Properties properties = new Properties();

        try {
            final FileInputStream fi =
                    new FileInputStream(MT4jSettings.getInstance().getDefaultSettingsPath() + "Settings.txt");
            properties.load(fi);
            // enableMultiMouse = Boolean.parseBoolean(properties.getProperty("MultiMiceEnabled", "false").trim());
            // CHECKSTYLE:IGNORE IllegalCatch: Many exceptions can occur and we don't want to crash the application.
        } catch (final Exception e) {
            logger.debug("Failed to load Settings.txt from the File system. Trying to load it from classpath..");
            try {
                final InputStream in =
                        Thread.currentThread().getContextClassLoader().getResourceAsStream("Settings.txt");
                if (in != null) {
                    properties.load(in);
                    // enableMultiMouse = Boolean.parseBoolean(properties.getProperty("MultiMiceEnabled",
                    // "false").trim());
                } else {
                    logger.debug("Couldnt load Settings.txt as a resource. Using defaults.");
                }
            } catch (final IOException e1) {
                logger.error("Couldnt load Settings.txt. Using defaults.");
                e1.printStackTrace();
            }
        }
        final MouseInputSource mouseInput = new MouseInputSource(this);
        inputManager.registerInputSource(mouseInput);

        // Check if we run windows 7
        if (System.getProperty("os.name").toLowerCase().contains("windows 7")) {
            final Win7NativeTouchSource win7NativeInput = new Win7NativeTouchSource(this);
            if (win7NativeInput.isSuccessfullySetup()) {
                inputManager.registerInputSource(win7NativeInput);
            }
        }

        final MTApplication desktopApp = this;
        inputManager.registerInputSource(new Tuio2DCursorInputSource(desktopApp));
        inputManager.registerInputSource(new Tuio2dObjectInputSource(desktopApp));

        /*
         * This is the bit that is significantly different from the mt4j coe.
         */
        final RamKeyboardInputSource keyInput = new RamKeyboardInputSource(this);
        inputManager.registerInputSource(keyInput);

    }

    @Override
    public void startUp() {
        // Processing registers a listener in PApplet#setupExternalMessages()
        // which exits the application by default.
        // Remove it so we can use our own.
        for (WindowListener listener : frame.getWindowListeners()) {
            frame.removeWindowListener(listener);
        }

        // Registers listener for closing window requests.
        // This will not work on Mac OS X (Command+Q and selecting Quit from the menu bar).
        // Adding a hook for handling quit events on Mac OS X is done in registerForMacOSXEvents().
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                handleQuitRequest();
            }
        });

        // By default, the window will hide on close.
        ((JFrame) frame).setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

        // Load app icons. Do it here, because we need the frame.
        loadAppIcons();

        // Workaround for Mac OS X. The dock icon is usually set through an app bundle.
        // Until TouchCORE is bundled properly for Mac OS X, set the dock icon explicitly.
        // Using modified OSXAdapter from Apple, which should work also on Java 7.
        if (ResourceUtils.getOperatingSystem() == OperatingSystem.OSX) {
            registerForMacOSXEvents(frame.getIconImages().get(frame.getIconImages().size() - 1));
        }

        instance = this;

        // Add the first scene.
        backgroundScene = new TouchCOREStartupScene(this);
        ((TouchCOREStartupScene) backgroundScene).setHandler(HandlerFactory.INSTANCE.getSelectSceneHandler());
        addScene(backgroundScene);

        registerDefaultInputSources(getInputManager());

        if (onLoad != null) {
            onLoad.run();
        }
    }

    /**
     * Function used to load the application icons.
     */
    private void loadAppIcons() {
        List<String> icons = ResourceUtil.getResourceListing("icons/", ".png");

        // Sort the images according to their image size.
        Collections.sort(icons, new Comparator<String>() {
            @Override
            public int compare(String icon1, String icon2) {
                int sizeIndex1 = icon1.lastIndexOf('_') + 1;
                int sizeIndex2 = icon2.lastIndexOf('_') + 1;

                // Get the image size. Icon filenames follow the pattern of "icon_<size>x<size>"
                // and may also have "@2x" appended (for retina resolutions).
                String iconSize1 = icon1.substring(sizeIndex1, icon1.indexOf('x', sizeIndex1));
                String iconSize2 = icon2.substring(sizeIndex2, icon2.indexOf('x', sizeIndex2));

                return Integer.valueOf(iconSize1).compareTo(Integer.valueOf(iconSize2));
            }
        });

        Toolkit toolkit = Toolkit.getDefaultToolkit();
        List<Image> images = new ArrayList<Image>();

        // Create images for all icons.
        for (String icon : icons) {
            try {
                URL iconURL = new URL(icon);
                images.add(toolkit.createImage(iconURL));
            } catch (MalformedURLException e) {
                LoggerUtils.warn("Can't load app icon: " + icon);
            }
        }

        frame.setIconImages(images);
    }

    /**
     * Function called when the Operating System is MacOS. A docking icon image is registered accordingly.
     *
     * @param dockIcon - The docking Icon icon which needs to be set.
     */
    private void registerForMacOSXEvents(Image dockIcon) {
        if (OSXAdapter.isMacOSX()) {
            try {
                OSXAdapter.setQuitHandler(this,
                        getClass().getDeclaredMethod("handleQuitRequest", (java.lang.Class[]) null));
                OSXAdapter.setDockIconImage(dockIcon);
                // CHECKSTYLE:IGNORE IllegalCatch: Many exceptions can occur and we don't want to crash the application.
            } catch (Exception e) {
                System.err.println("Error while loading the OSXAdapter:");
                e.printStackTrace();
            }
        }
    }

    /**
     * Switches the app back to the background scene.
     *
     * @param scene the current scene
     */
    public void switchToBackground(Iscene scene) {
        changeScene(backgroundScene);
        NavigationBar.getInstance().wipeNavigationBar();
    }

    /**
     * Deletes a scene.
     *
     * @param aspect the aspect to unregister
     */
    private void unregisterAspect(final Aspect aspect) {
        synchronized (loadedModels) {
            loadedModels.remove(aspect);
            graphicalUpdaters.remove(aspect);
        }
    }

    @Override
    public void exit() {
        LoggerUtils.info("Shutting down TouchCORE...");

        super.exit();

        // Unload application handlers.
        if (ResourceUtils.getOperatingSystem() == OperatingSystem.OSX) {
            OSXAdapter.clearApplicationHandlers();
        }
    }

    /**
     * Should be called whenever the user wants to quit the application. Takes care of making sure this is what the user
     * wants.
     */
    public void handleQuitRequest() {
        // Get a list of all scenes that have unsaved changes.
        final List<RamAbstractScene<?>> unsavedScenes = new ArrayList<RamAbstractScene<?>>();

        for (Iscene scene : getScenes()) {
            EObject model = null;
            RamAbstractScene<?> currentScene = null;

            if (scene instanceof DisplayAspectScene) {
                DisplayAspectScene ramScene = (DisplayAspectScene) scene;
                model = ramScene.getAspect();
                currentScene = ramScene;
            } else if (scene instanceof DisplayConcernEditScene) {
                DisplayConcernEditScene concernScene = (DisplayConcernEditScene) scene;
                model = concernScene.getConcern();
                currentScene = concernScene;
            } else if (scene instanceof DisplayClassDiagramScene) {
                DisplayClassDiagramScene cdScene = (DisplayClassDiagramScene) scene;
                model = cdScene.getClassDiagram();
                currentScene = cdScene;
            } else if (scene instanceof DisplayUseCaseDiagramScene) {
                DisplayUseCaseDiagramScene ucdScene = (DisplayUseCaseDiagramScene) scene;
                model = ucdScene.getUseCaseDiagram();
                currentScene = ucdScene;
            } else if (scene instanceof DisplayRestTreeScene) {
                DisplayRestTreeScene restTreeScene = (DisplayRestTreeScene) scene;
                model = restTreeScene.getRestTree();
                currentScene = restTreeScene;
            }

            if (model != null) {
                boolean isSaveNeeded = EMFEditUtil.getCommandStack(model).isSaveNeeded();
                if (isSaveNeeded) {
                    unsavedScenes.add(currentScene);
                }

            }
        }

        invokeLater(new Runnable() {
            @Override
            public void run() {
                showExitConfirm(unsavedScenes);
            }
        });
    }

    /**
     * Function called to show exit confirmation pop up to all the scenes who are not saved yet.
     *
     * @param unsavedScenes - The list of unsaved scenes.
     */
    private void showExitConfirm(final List<RamAbstractScene<?>> unsavedScenes) {
        RamAbstractScene<?> currentScene = (RamAbstractScene<?>) getCurrentScene();

        if (!unsavedScenes.isEmpty()) {
            final RamAbstractScene<?> scene = unsavedScenes.iterator().next();

            if (scene instanceof DisplayAspectScene) {
                final DisplayAspectScene aspectScene = (DisplayAspectScene) scene;

                aspectScene.showCloseConfirmPopup(currentScene, new ConfirmPopup.SelectionListener() {

                    @Override
                    public void optionSelected(int selectedOption) {
                        if (selectedOption == ConfirmPopup.YES_OPTION) {
                            BasicActionsUtils.saveModel(aspectScene.getAspect(), new FileBrowserListener() {

                                @Override
                                public void modelSaved(File file) {
                                    unsavedScenes.remove(scene);
                                    showExitConfirm(unsavedScenes);
                                }

                                @Override
                                public void modelLoaded(EObject model) {
                                }
                            });
                        } else if (selectedOption == ConfirmPopup.NO_OPTION) {
                            unsavedScenes.remove(scene);
                            showExitConfirm(unsavedScenes);
                        }
                    }
                });
            } else if (scene instanceof DisplayClassDiagramScene) {
                final DisplayClassDiagramScene cdScene = (DisplayClassDiagramScene) scene;

                cdScene.showCloseConfirmPopup(currentScene, new ConfirmPopup.SelectionListener() {

                    @Override
                    public void optionSelected(int selectedOption) {
                        if (selectedOption == ConfirmPopup.YES_OPTION) {
                            BasicActionsUtils.saveModel(cdScene.getClassDiagram(), new FileBrowserListener() {

                                @Override
                                public void modelSaved(File file) {
                                    unsavedScenes.remove(scene);
                                    showExitConfirm(unsavedScenes);
                                }

                                @Override
                                public void modelLoaded(EObject model) {
                                }
                            });
                        } else if (selectedOption == ConfirmPopup.NO_OPTION) {
                            unsavedScenes.remove(scene);
                            showExitConfirm(unsavedScenes);
                        }
                    }
                });
            } else if (scene instanceof DisplayUseCaseDiagramScene) {
                final DisplayUseCaseDiagramScene ucdScene = (DisplayUseCaseDiagramScene) scene;

                ucdScene.showCloseConfirmPopup(currentScene, new ConfirmPopup.SelectionListener() {

                    @Override
                    public void optionSelected(int selectedOption) {
                        if (selectedOption == ConfirmPopup.YES_OPTION) {
                            BasicActionsUtils.saveModel(ucdScene.getUseCaseDiagram(), new FileBrowserListener() {

                                @Override
                                public void modelSaved(File file) {
                                    unsavedScenes.remove(scene);
                                    showExitConfirm(unsavedScenes);
                                }

                                @Override
                                public void modelLoaded(EObject model) {
                                }
                            });
                        } else if (selectedOption == ConfirmPopup.NO_OPTION) {
                            unsavedScenes.remove(scene);
                            showExitConfirm(unsavedScenes);
                        }
                    }
                });
            } else if (scene instanceof DisplayRestTreeScene) {
                final DisplayRestTreeScene restTreeScene = (DisplayRestTreeScene) scene;

                restTreeScene.showCloseConfirmPopup(currentScene, new ConfirmPopup.SelectionListener() {

                    @Override
                    public void optionSelected(int selectedOption) {
                        if (selectedOption == ConfirmPopup.YES_OPTION) {
                            BasicActionsUtils.saveModel(restTreeScene.getRestTree(), new FileBrowserListener() {

                                @Override
                                public void modelSaved(File file) {
                                    unsavedScenes.remove(scene);
                                    showExitConfirm(unsavedScenes);
                                }

                                @Override
                                public void modelLoaded(EObject model) {
                                }
                            });
                        } else if (selectedOption == ConfirmPopup.NO_OPTION) {
                            unsavedScenes.remove(scene);
                            showExitConfirm(unsavedScenes);
                        }
                    }
                });

            } else {
                final DisplayConcernEditScene sceneCopy = (DisplayConcernEditScene) scene;

                sceneCopy.showCloseConfirmPopup(currentScene, new ConfirmPopup.SelectionListener() {

                    @Override
                    public void optionSelected(int selectedOption) {
                        if (selectedOption == ConfirmPopup.YES_OPTION) {
                            IConcernEditSceneHandler handler = sceneCopy.getHandler();
                            handler.save(sceneCopy.getConcern());
                            unsavedScenes.remove(scene);
                            showExitConfirm(unsavedScenes);
                        } else if (selectedOption == ConfirmPopup.NO_OPTION) {
                            unsavedScenes.remove(scene);
                            showExitConfirm(unsavedScenes);
                        }
                    }
                });

            }

        } else {
            ConfirmPopup popup =
                    new ConfirmPopup(Strings.POPUP_QUIT_CONFIRM, ConfirmPopup.OptionType.YES_NO);
            popup.setListener(new ConfirmPopup.SelectionListener() {

                @Override
                public void optionSelected(int selectedOption) {
                    if (selectedOption == ConfirmPopup.YES_OPTION) {
                        exit();
                    }
                }
            });

            currentScene.displayPopup(popup);
        }

    }

}
