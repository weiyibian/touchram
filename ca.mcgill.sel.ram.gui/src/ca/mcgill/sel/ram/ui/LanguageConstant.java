package ca.mcgill.sel.ram.ui;

/**
 * A utility class that provides constants related to language.
 * @author Hyacinth
 *
 */
public final class LanguageConstant {

    /**
     * The resource factory of ram.
     */
    public static final String RAM_LANGUAGE_NAME = "Reusable Aspect Models";

    /**
     * The file extension for aspects.
     */
    public static final String RAM_FILE_EXTENSION = "ram";
    
    /**
     * The resource factory of ram.
     */
    public static final String RAM_RESOURCE_FACTORY = "ca.mcgill.sel.ram.util.RamResourceFactoryImpl";
    
    /**
     * The adapter factory of ram.
     */
    public static final String RAM_ADAPTER_FACTORY = "ca.mcgill.sel.ram.provider.RamItemProviderAdapterFactory";
    
    /**
     * The ram weaver.
     */
    public static final String RAM_WEAVER_CLASS = "ca.mcgill.sel.ram.weaver.RAMWeaver";
    
    // Class Diagrams
    
    /**
     * The language name for class diagrams.
     */
    public static final String CLASS_DIAGRAM_LANGUAGE_NAME = "Class Diagram";
   
    /**
     * The file extension for class diagrams.
     */
    public static final String CLASS_DIAGRAM_FILE_EXTENSION = "cdm";
    
    /**
     * The resource factory of class diagrams.
     */
    public static final String CLASS_DIAGRAM_RESOURCE_FACTORY = 
            "ca.mcgill.sel.classdiagram.util.CdmResourceFactoryImpl";
    
    /**
     * The adapter factory of class diagrams.
     */
    public static final String CLASS_DIAGRAM_ADAPTER_FACTORY = 
            "ca.mcgill.sel.classdiagram.provider.CdmItemProviderAdapterFactory";
    
    /**
     * The class diagrams weaver. TODO Doesn't exist yet, using RAM weaver
     */
    public static final String CLASS_DIAGRAM_WEAVER_CLASS = RAM_WEAVER_CLASS;
      
    // Domain Models
  
    /**
     * The file extension for class diagrams.
     */
    public static final String DOMAIN_MODEL_LANGUAGE_NAME = "Domain Model";
    
    /**
     * The file extension for class diagrams.
     */
    public static final String DOMAIN_MODEL_FILE_EXTENSION = "dm";
    
    
    /**
     * The resource factory of ram.
     */
    public static final String CD_RESOURCE_FACTORY = "ca.mcgill.sel.ram.util.RamResourceFactoryImpl";
    
    /**
     * The adapter factory of ram.
     */
    public static final String CD_ADAPTER_FACTORY = "ca.mcgill.sel.ram.provider.RamItemProviderAdapterFactory";
    
    /**
     * The ram weaver.
     */
    public static final String CD_WEAVER_CLASS = RAM_WEAVER_CLASS;
    
    // Use case diagrams
    /**
     * The use case language name.
     */
    public static final String UC_LANGUAGE_NAME = "Use Cases";

    // Rest Trees    
    /**
     * The language name for Rest Trees.
     */
    public static final String REST_TREE_LANGUAGE_NAME = "Rest Tree";
    
    /**
     * Creates a new instance.
     */
    private LanguageConstant() {
    }
}
