package ca.mcgill.sel.ram.ui;

import java.util.List;

import org.eclipse.emf.common.util.URI;

import ca.mcgill.sel.commons.LoggerUtils;
import ca.mcgill.sel.commons.ResourceUtil;
import ca.mcgill.sel.commons.emf.util.AdapterFactoryRegistry;
import ca.mcgill.sel.commons.emf.util.ResourceManager;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREExternalLanguage;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.language.registry.CORELanguageRegistry;
import ca.mcgill.sel.core.perspective.COREPerspectiveUtil;
import ca.mcgill.sel.core.provider.CoreItemProviderAdapterFactory;
import ca.mcgill.sel.core.util.CoreResourceFactoryImpl;
import ca.mcgill.sel.ram.expressions.ExpressionDslStandaloneSetup;
import ca.mcgill.sel.ram.expressions.RamStandaloneSetup;
import ca.mcgill.sel.ram.loaders.RamClassLoader;
import ca.mcgill.sel.ram.ui.utils.ResourceUtils;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.utils.ResourceUtils.OperatingSystem;
import ca.mcgill.sel.ram.util.Constants;
import ca.mcgill.sel.ram.validator.ValidationRulesParser;

/**
 * This class is just here to start a new instance of the TouchCORE application.
 * This is an empty facade that should hopefully
 * make refactoring easier should it ever be needed. NOTE - this MUST be run as an application (and not an applet)
 * otherwise Settings.txt will not be read.
 *
 * @author vbonnet
 * @author mschoettle
 */
public final class TouchCORE {

    /**
     * Creates a new instance of TouchCORE.
     */
    private TouchCORE() {

    }

    /**
     * Starts the application.
     *
     * @param args arguments, not used
     */
    public static void main(final String[] args) {

        // Temporary workaround to rename the dock icon name in Mac OS X.
        // Once the application is bundled as an .app this is not required anymore.
        // Might only work in Java 6 from Apple.
        if (ResourceUtils.getOperatingSystem() == OperatingSystem.OSX) {
            System.setProperty("com.apple.mrj.application.apple.menu.about.name", Strings.APP_NAME);
        }

        // Initialize ResourceManager (will initialize OCL).
        // See issue #84.
        ResourceManager.initialize();

        // Initialize CORE packages.
        CorePackage.eINSTANCE.eClass();

        // Register resource factories
        ResourceManager.registerExtensionFactory(Constants.CORE_FILE_EXTENSION, new CoreResourceFactoryImpl());

        // Initialize adapter factories
        AdapterFactoryRegistry.INSTANCE.addAdapterFactory(CoreItemProviderAdapterFactory.class);

        // Load UI relevant JARS from ca.mcgill.sel.ram.gui/lib folder.
        ResourceUtils.loadLibraries();

        // Start the application, the rest will happen by itself.
        RamApp.initialize();

        RamClassLoader.INSTANCE.initializeWithJavaClasses();

        // Register resource factories for expressions parser
        ExpressionDslStandaloneSetup.doSetup();
        RamStandaloneSetup.doSetup();

        // Check for existing languages and load them.
        loadLanguages();

        // Load existing perspectives
        COREPerspectiveUtil.INSTANCE.loadPerspectives();
        
        // associate domain model perspective language
        // COREPerspectiveUtil.INSTANCE.associateDomainPerspective();
        
        // associate design model perspective language
        // COREPerspectiveUtil.INSTANCE.associateDesignPerspective();

        // To parse OCL files and initialize rules.
        Thread tParser = new Thread(new Runnable() {
            @Override
            public void run() {
                ValidationRulesParser.getInstance();
                LoggerUtils.info("OCL files parsing terminated.");
            }
        }, "Rules Parser Initializer");
        tParser.start();

    }

    /**
     * TODO: refactor this method somewhere where it can be reused be e.g. a ui-less launcher.
     * (tricky because the method depends on code from two further modules: core, corelanguage)
     * Loads pre-defined languages and registers them in the {@link CORELanguageRegistry}.
     * Currently Ram.core, DomainModel.core, ClassDiagram.core
     */
    public static void loadLanguages() {
        List<String> languages = ResourceUtil.getResourceListing("models/languages/", ".core");

        if (languages != null) {
            for (String l : languages) {

                // load existing languages
                URI fileURI = URI.createURI(l);
                COREConcern languageConcern = (COREConcern) ResourceManager.loadModel(fileURI);
                // register any languages contained in the loaded concern
                for (COREArtefact a : languageConcern.getArtefacts()) {
                    if (a instanceof COREExternalLanguage) {
                        COREExternalLanguage existingLanguage = (COREExternalLanguage) a;
                        CORELanguageRegistry.getRegistry().registerLanguage(existingLanguage);
                    }
                }
            }
        }
    }
}
