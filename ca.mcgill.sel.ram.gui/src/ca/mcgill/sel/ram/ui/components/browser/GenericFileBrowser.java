package ca.mcgill.sel.ram.ui.components.browser;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;

import ca.mcgill.sel.commons.emf.util.ResourceManager;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamPopup.PopupType;
import ca.mcgill.sel.ram.ui.components.browser.RamFileBrowser.RamFileBrowserType;
import ca.mcgill.sel.ram.ui.components.browser.interfaces.FileBrowserListener;
import ca.mcgill.sel.ram.ui.components.browser.interfaces.RamFileBrowserListener;
import ca.mcgill.sel.ram.ui.utils.GUIConstants;
import ca.mcgill.sel.ram.ui.utils.Strings;


/**
 * Utility class to load/save aspect with a RamFileBrowser.
 *
 * @author tdimeco
 */
public final class GenericFileBrowser {

    private static File lastSharedFolder = new File(GUIConstants.DIRECTORY_MODELS).getAbsoluteFile();
    private static File rootConcernFolder;

    /**
     * Private constructor.
     */
    private GenericFileBrowser() {
    }

    /**
     * Load an aspect with the file browser.
     *
     * @param extension The extension type of the model we wish to load
     * @param listener The callback to handle the loaded aspect
     */
    public static void loadModel(String extension, final FileBrowserListener listener) {
        RamFileBrowser browser = new RamFileBrowser(RamFileBrowserType.LOAD, extension,
                lastSharedFolder, rootConcernFolder);
        // TODO: Threaded loading can cause MT4J to crash during drawAndUpdate
        // browser.setCallbackThreaded(true);
        browser.addFileBrowserListener(new RamFileBrowserListener() {

            @Override
            public void fileSelected(File file, RamFileBrowser fileBrowser) {
                try {
                    if (file.canRead()) {
                        lastSharedFolder = file.getParentFile();
                        Aspect aspect = (Aspect) ResourceManager.loadModel(file);
                        listener.modelLoaded(aspect);
                    }
                    // CHECKSTYLE:IGNORE IllegalCatch FOR 1 LINES: Need to catch RuntimeException
                } catch (Exception e) {
                    RamApp.getActiveScene().displayPopup(Strings.POPUP_ERROR_FILE_LOAD_SYNTAX, PopupType.ERROR);
                    e.printStackTrace();
                }
            }
        });
        browser.display();
    }

    /**
     * Save an aspect with the file browser.
     *
     * @param model The model the user wants to save
     * @param extension The extension to use for the saved file
     * @param name The name to use for the saved file
     * @param listener The callback to handle the saved file
     */
    public static void saveModel(final EObject model, String extension, final String name,
            final FileBrowserListener listener) {
        File suggestedFile;

        // Set the selected file to suggest a name.
        // If the aspect is already contained in a resource, choose that file.
        if (model.eResource() != null) {
            suggestedFile = new File(model.eResource().getURI().toFileString());
        } else {
            suggestedFile = new File(lastSharedFolder, name);
        }

        RamFileBrowser browser = new RamFileBrowser(RamFileBrowserType.SAVE, extension,
                suggestedFile, rootConcernFolder);

        // TODO: Threaded loading can cause MT4J to crash during drawAndUpdate
        // browser.setCallbackThreaded(true);

        // CHECKSTYLE:IGNORE AnonInnerLength FOR 1 LINES: Okay here
        browser.addFileBrowserListener(new RamFileBrowserListener() {

            @Override
            public void fileSelected(File file, RamFileBrowser fileBrowser) {
                if (!file.exists()) {
                    try {
                        if (!file.getParentFile().exists()) {
                            // create parent directory if someone's trying to be tricky.
                            file.getParentFile().mkdirs();
                        }
                        file.createNewFile();
                    } catch (final IOException e) {
                        // TODO display something to user
                        e.printStackTrace();
                    }
                }
                try {
                    // Check permission
                    if (!file.canWrite()) {
                        throw new IOException("Permission denied");
                    }

                    EObject modelToSave = model;
                    // if the file/aspect is already saved in a resource it should be copied
                    // but only if the file name differs
                    // otherwise the old resource will be empty
                    // if the aspect was created new it means it has no eResource because
                    // it has never been saved
                    // if we are overwriting a file then dont create a new resource
                    if (model.eResource() != null
                            && model.eResource().getURI().equals(URI.createFileURI(file.getAbsolutePath()))) {
                        // Save only resources that have actually changed.
                        final Map<Object, Object> saveOptions = new HashMap<Object, Object>();
                        saveOptions.put(
                                Resource.OPTION_SAVE_ONLY_IF_CHANGED,
                                Resource.OPTION_SAVE_ONLY_IF_CHANGED_MEMORY_BUFFER);
                        model.eResource().save(saveOptions);
                        listener.modelSaved(file);
                    } else {
                        if (model.eResource() != null) {
                            modelToSave = EcoreUtil.copy(model);
                        }

                        ResourceManager.saveModel(modelToSave, file.getAbsolutePath());
                        if (listener != null) {
                            lastSharedFolder = file.getParentFile();
                            listener.modelSaved(file);
                        }
                    }
                } catch (IOException e) {
                    RamApp.getActiveScene()
                            .displayPopup(Strings.POPUP_ERROR_FILE_SAVE + e.getMessage(), PopupType.ERROR);
                }
            }
        });
        browser.display();
    }

    /**
     * Sets the initial folder that is displayed first by the file browser.
     * The path needs to be a directory and readable.
     *
     * @param path the new path
     */
    public static void setInitialFolder(String path) {
        File newPath = new File(path);
        if (newPath.isDirectory() && newPath.canRead()) {
            lastSharedFolder = newPath;
        }
    }

    /**
     * Sets the root custom folder for browsing. The user will not be able to go higher
     * than this folder in the hierarchy.
     * @param path the new path
     */
    public static void setRootFolder(String path) {
        File newPath = new File(path);
        if (newPath.isDirectory() && newPath.canRead()) {
            rootConcernFolder = newPath;
        }
    }
}
