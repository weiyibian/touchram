package ca.mcgill.sel.ram.ui.components.listeners;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.edit.provider.INotifyChangedListener;

import ca.mcgill.sel.ram.ui.RamApp;

/**
 * Helper interface that several classes spanning across the package implements. 
 * The notifications are to be handled only by the main thread. Trying to do otherwise breaks the client. 
 * Implementing INotifyChangedListener calls for handing over the functionality to the main thread on several classes.
 * To avoid repetitions, a default interface was created that extends the existing INotifyChangedListener. 
 * The abstract method, 'handleNotification(Notification notification) is overridden by the classes in the package.
 *
 * @author arthurls
 */
public interface UINotifyChangedListener extends INotifyChangedListener {
    
    @Override
    default void notifyChanged(Notification notification) {
        RamApp.getApplication().invokeLater(new Runnable() {
            @Override
            public void run() {
                handleNotification(notification);
            }
        });
    }
    
    /**
     * Abstract method which are implemented by several other classes. 
     * Each one of them tells the main thread on how the notifications are to be handled.
     * 
     * @param notification that is to be handled.
     */
    void handleNotification(Notification notification);

}