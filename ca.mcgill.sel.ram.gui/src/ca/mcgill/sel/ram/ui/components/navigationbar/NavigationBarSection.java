package ca.mcgill.sel.ram.ui.components.navigationbar;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.mt4j.components.TransformSpace;
import org.mt4j.util.MTColor;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.classdiagram.CdmPackage;
import ca.mcgill.sel.classdiagram.impl.ClassDiagramImpl;
import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.COREImpactNode;
import ca.mcgill.sel.core.impl.COREConcernImpl;
import ca.mcgill.sel.core.impl.COREFeatureImpl;
import ca.mcgill.sel.core.impl.COREImpactNodeImpl;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.AspectMessageView;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.impl.AspectImpl;
import ca.mcgill.sel.ram.impl.AspectMessageViewImpl;
import ca.mcgill.sel.ram.impl.MessageViewImpl;
import ca.mcgill.sel.ram.impl.MessageViewReferenceImpl;
import ca.mcgill.sel.ram.impl.OperationImpl;
import ca.mcgill.sel.ram.impl.StateViewImpl;
import ca.mcgill.sel.ram.impl.StructuralViewImpl;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamButton;
import ca.mcgill.sel.ram.ui.components.RamImageComponent;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamSpacerComponent;
import ca.mcgill.sel.ram.ui.components.navigationbar.NavigationBarMenu.NavigatonBarNamer;
import ca.mcgill.sel.ram.ui.components.navigationbar.NavigationBarMenu.NavigatonBarNamerRM;
import ca.mcgill.sel.ram.ui.events.listeners.ActionListener;
import ca.mcgill.sel.ram.ui.layouts.HorizontalLayoutHorizontallyCentered;
import ca.mcgill.sel.ram.ui.layouts.VerticalLayout;
import ca.mcgill.sel.ram.ui.scenes.DisplayAspectScene;
import ca.mcgill.sel.ram.ui.scenes.handler.impl.DisplayAspectSceneHandler;
import ca.mcgill.sel.ram.ui.utils.Fonts;
import ca.mcgill.sel.ram.ui.utils.GraphicalUpdater;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.HandlerFactory;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.impl.UseCaseModelImpl;
import processing.core.PImage;

/**
 * A section in the {@link NavigationBar}. It represents an eObject in the path of the {@link NavigationBar}. When it's
 * shown in the {@link NavigationBar} it means that the related eObject is linked to the previous and next sections.
 * It can contain an icon and a {@link NavigationBarMenu} which allows to navigate to next sections not shown.
 * When a menu is defined, an arrow will be added on the right of the section in order to show/hide it.
 *
 * @author Andrea
 */
public class NavigationBarSection extends RamRectangleComponent {


    private RamButton expandButton;
    private RamButton filteredSwitchUpButton;
    private RamButton label;
    private RamRectangleComponent headerContainer;
    private RamRectangleComponent labelContainer;
    private RamRectangleComponent topLabelContainer;
    private RamRectangleComponent bottomLabelContainer;
    private Object typeOfSection;
    private boolean menuShown;
    private GraphicalUpdater graphicalUpdater;

    /**
     * Constructs a section with or without icon or menu in function of their value.
     * 
     * @param icon - icon in the section. Null to not add an icon.
     * @param label - label in the section.
     * @param namer - namer which constructs the menu. Null to not add a menu.
     * @param eObject - related eObject of the section.
     * @param <T> - type of the related eObject.
     */
    <T> NavigationBarSection(PImage icon, String label, final NavigatonBarNamer<T> namer, final T eObject) {
        super(new VerticalLayout());

        this.menuShown = false;
        
        if (eObject instanceof COREImpactNode) {
            this.label = new RamButton((COREImpactNode) eObject);
        } else {
            this.label = new RamButton(label);
        }
        this.label.setAutoMaximizes(false);
        this.label.setAutoMinimizes(false);

        setPickable(false);
        this.typeOfSection = eObject.getClass();
        headerContainer = new RamRectangleComponent();
        headerContainer.setPickable(false);
        headerContainer.setLayout(new VerticalLayout(-9.0f));
        topLabelContainer = new RamRectangleComponent();
        topLabelContainer.setPickable(false);
        topLabelContainer.setLayout(new HorizontalLayoutHorizontallyCentered());

        labelContainer = new RamRectangleComponent();
        labelContainer.setPickable(false);
        labelContainer.setLayout(new HorizontalLayoutHorizontallyCentered());

        bottomLabelContainer = new RamRectangleComponent();
        bottomLabelContainer.setPickable(false);
        bottomLabelContainer.setLayout(new HorizontalLayoutHorizontallyCentered());

        addChild(headerContainer);

        if (eObject.getClass().equals(COREConcernImpl.class)) {
            createConcernSection(namer, eObject);
        } else if (eObject.getClass().equals(COREFeatureImpl.class)) {
            this.label = new RamButton("<" + label + ">");
            createFeatureSection(namer, eObject);
        } else if (eObject.getClass().equals(StructuralViewImpl.class)) {
            createStructuralViewSection(namer, eObject);
        } else if (eObject.getClass().equals(MessageViewImpl.class)
                || eObject.getClass().equals(AspectMessageViewImpl.class)
                || eObject.getClass().equals(OperationImpl.class)
                || eObject.getClass().equals(MessageViewReferenceImpl.class)) {
            createMessageViewSection(namer, eObject);
        } else if (eObject.getClass().equals(StateViewImpl.class)) {
            createStateViewSection(namer, eObject);
        } else if (eObject.getClass().equals(COREImpactNodeImpl.class)) {
            createImpactNodeSection(namer, eObject);
        }
    }

    /**
     * Normal mode constructor for creating a canonical RM NavigationBarSection.
     * 
     * @param icon the icon for the section
     * @param label the name of the section
     * @param namer the content
     * @param eObject the type of content
     * @param <T> type
     */
    <T> NavigationBarSection(PImage icon, String label, final NavigatonBarNamerRM<T> namer, final T eObject) {
        super(new VerticalLayout());

        this.menuShown = false;
        this.label = new RamButton(label);
        this.label.setAutoMaximizes(false);
        this.label.setAutoMinimizes(false);
        // this.label.setFillColor(MTColor.GREY);
        this.typeOfSection = eObject.getClass();
        headerContainer = new RamRectangleComponent();
        headerContainer.setLayout(new VerticalLayout(-9.0f));
        topLabelContainer = new RamRectangleComponent();
        topLabelContainer.setLayout(new HorizontalLayoutHorizontallyCentered());

        labelContainer = new RamRectangleComponent();
        labelContainer.setLayout(new HorizontalLayoutHorizontallyCentered());

        bottomLabelContainer = new RamRectangleComponent();
        bottomLabelContainer.setLayout(new HorizontalLayoutHorizontallyCentered());

        addChild(headerContainer);
        
        if (eObject.getClass().equals(AspectImpl.class) 
                || eObject.getClass().equals(ClassDiagramImpl.class)
                || eObject.getClass().equals(UseCaseModelImpl.class)) {
            createRMSection(namer, eObject);
        }
    }

    /**
     * Mode for creating a special section for the special Build Application navigation bar.
     * 
     * @param icon the icon for the section
     * @param label the name of the section
     * @param namer the content
     * @param eObject the type of content
     * @param flag used to toggle the constructor for the creation of the special navbar
     * @param <T> type
     */
    <T> NavigationBarSection(PImage icon, String label, final NavigatonBarNamerRM<T> namer,
            final T eObject, boolean flag) {
        super(new VerticalLayout());
        // BUILD MODE
        if (flag) {
            this.menuShown = false;
            this.label = new RamButton(label);
            this.label.setAutoMaximizes(false);
            this.label.setAutoMinimizes(false);
            // this.label.setFillColor(MTColor.GREY);
            this.typeOfSection = eObject.getClass();
            headerContainer = new RamRectangleComponent();
            headerContainer.setLayout(new VerticalLayout(-9.0f));
            topLabelContainer = new RamRectangleComponent();
            topLabelContainer.setLayout(new HorizontalLayoutHorizontallyCentered());

            labelContainer = new RamRectangleComponent();
            labelContainer.setLayout(new HorizontalLayoutHorizontallyCentered());

            bottomLabelContainer = new RamRectangleComponent();
            bottomLabelContainer.setLayout(new HorizontalLayoutHorizontallyCentered());

            addChild(headerContainer);

            if (eObject.getClass().equals(AspectImpl.class)) {
                createBuildRMSection(namer, eObject);
            }
        } else {
            // NO FEATURE MODE
            this.menuShown = false;
            String newLabel = new String();

            if (label.contains("<")) {
                // Concern Reuse Mode
                for (int i = 0; i < label.length(); i++) {
                    if (label.charAt(i) == '<') {
                        newLabel = label.substring(0, i--);
                        break;
                    }
                }

                this.label = new RamButton(newLabel);
                this.label.setAutoMaximizes(false);
                this.label.setAutoMinimizes(false);
                // this.label.setFillColor(MTColor.GREY);
                this.typeOfSection = eObject.getClass();
                headerContainer = new RamRectangleComponent();
                headerContainer.setLayout(new VerticalLayout(-9.0f));
                topLabelContainer = new RamRectangleComponent();
                topLabelContainer.setLayout(new HorizontalLayoutHorizontallyCentered());

                labelContainer = new RamRectangleComponent();
                labelContainer.setLayout(new HorizontalLayoutHorizontallyCentered());

                bottomLabelContainer = new RamRectangleComponent();
                bottomLabelContainer.setLayout(new HorizontalLayoutHorizontallyCentered());

                addChild(headerContainer);

                if (eObject.getClass().equals(AspectImpl.class)) {
                    createRMNoFeatureSection(namer, eObject);
                }
            } else if (label.contains("_")) {
                this.label = new RamButton(newLabel);
                this.label.setAutoMaximizes(false);
                this.label.setAutoMinimizes(false);
                // this.label.setFillColor(MTColor.GREY);
                this.typeOfSection = eObject.getClass();
                headerContainer = new RamRectangleComponent();
                headerContainer.setLayout(new VerticalLayout(-9.0f));
                topLabelContainer = new RamRectangleComponent();
                topLabelContainer.setLayout(new HorizontalLayoutHorizontallyCentered());

                labelContainer = new RamRectangleComponent();
                labelContainer.setLayout(new HorizontalLayoutHorizontallyCentered());

                bottomLabelContainer = new RamRectangleComponent();
                bottomLabelContainer.setLayout(new HorizontalLayoutHorizontallyCentered());

                addChild(headerContainer);

                if (eObject.getClass().equals(AspectImpl.class)) {
                    createRMWeavingSection(namer, eObject);
                }
            } else {
                // new aspect mode
                newLabel = label;

                this.label = new RamButton(newLabel);
                this.label.setAutoMaximizes(false);
                this.label.setAutoMinimizes(false);
                // this.label.setFillColor(MTColor.GREY);
                this.typeOfSection = eObject.getClass();
                headerContainer = new RamRectangleComponent();
                headerContainer.setLayout(new VerticalLayout(-9.0f));
                topLabelContainer = new RamRectangleComponent();
                topLabelContainer.setLayout(new HorizontalLayoutHorizontallyCentered());

                labelContainer = new RamRectangleComponent();
                labelContainer.setLayout(new HorizontalLayoutHorizontallyCentered());

                bottomLabelContainer = new RamRectangleComponent();
                bottomLabelContainer.setLayout(new HorizontalLayoutHorizontallyCentered());

                addChild(headerContainer);

                if (eObject.getClass().equals(AspectImpl.class)) {
                    createRMNoFeatureSectionNewAspect(namer, eObject);
                }
            }
        }
    }

    /**
     * Creates a Design Model detached from a Feature section.
     * 
     * @param namer namer of the menu
     * @param eObject specific element
     * @param <T> type
     */
    private <T> void createRMNoFeatureSection(NavigatonBarNamerRM<T> namer, final T eObject) {
        RamSpacerComponent fakeUpArrow = new RamSpacerComponent(0.0f, 22.0f);
        topLabelContainer.addChild(fakeUpArrow);

        headerContainer.addChild(topLabelContainer);
        this.label.setFillColor(new MTColor(128, 128, 128, MTColor.ALPHA_HALF_TRANSPARENCY));
        this.label.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent event) {
                // Action used to return immediately to the Concern no matter where the user is
                getParentOfType(NavigationBar.class).popSection(typeOfSection);
            }

        });
        labelContainer.addChild(label);

        headerContainer.addChild(labelContainer);

        if (namer != null) {
            addExpandableButtonRM(namer, eObject);
            headerContainer.addChild(bottomLabelContainer);
        } else {
            bottomLabelContainer.addChild(fakeUpArrow);
        }
    }
    
    /**
     * Creates a realization model weaving section.
     * 
     * @param namer the namer
     * @param eObject the model
     * @param <T> the actual type
     */
    private <T> void createRMWeavingSection(NavigatonBarNamerRM<T> namer, final T eObject) {
        this.label.removeAllChildren();
        RamSpacerComponent fakeUpArrow = new RamSpacerComponent(0.0f, 22.0f);
        topLabelContainer.addChild(fakeUpArrow);

        headerContainer.addChild(topLabelContainer);

        Vector3D p = this.label.getPosition(TransformSpace.GLOBAL);
        p.y += 25;
        this.label.setPositionGlobal(p);
        this.label.setFillColor(new MTColor(128, 128, 128, MTColor.ALPHA_HALF_TRANSPARENCY));

        Aspect aspect = (Aspect) eObject;

        TextView aspectName = new TextView(aspect, RamPackage.Literals.NAMED_ELEMENT__NAME);

        aspectName.setFont(Fonts.DEFAULT_FONT_MENU);
        aspectName.setMaximumHeight(38.0f);
        aspectName.registerTapProcessor(HandlerFactory.INSTANCE.getTextViewHandler());
        graphicalUpdater = RamApp.getApplication().getGraphicalUpdaterForModel(aspect);
        graphicalUpdater.addGUListener(aspect, aspectName);

        this.label.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent event) {
                getParentOfType(NavigationBar.class).popSection(typeOfSection);
            }

        });

        this.label.addChild(aspectName);

        labelContainer.addChild(label);

        headerContainer.addChild(labelContainer);
        //addFilteredSwitchButton(namer, eObject);
        addExpandableButtonRM(namer, eObject);

        headerContainer.addChild(bottomLabelContainer);
    }

    /**
     * Creates a section specific for when a new aspect is being created (A+) from RamMenu.
     * 
     * @param namer namer containing menu information
     * @param eObject the new Untitled Aspect just created
     * @param <T> the Aspect
     */
    private <T> void createRMNoFeatureSectionNewAspect(NavigatonBarNamerRM<T> namer, final T eObject) {
        this.label.removeAllChildren();
        RamSpacerComponent fakeUpArrow = new RamSpacerComponent(0.0f, 22.0f);
        topLabelContainer.addChild(fakeUpArrow);

        headerContainer.addChild(topLabelContainer);

        Vector3D p = this.label.getPosition(TransformSpace.GLOBAL);
        p.y += 25;
        this.label.setPositionGlobal(p);
        this.label.setFillColor(new MTColor(128, 128, 128, MTColor.ALPHA_HALF_TRANSPARENCY));

        Aspect aspect = (Aspect) eObject;

        TextView aspectName = new TextView(aspect, RamPackage.Literals.NAMED_ELEMENT__NAME);

        aspectName.setFont(Fonts.DEFAULT_FONT_MENU);
        aspectName.setMaximumHeight(38.0f);
        aspectName.registerTapProcessor(HandlerFactory.INSTANCE.getTextViewHandler());
        graphicalUpdater = RamApp.getApplication().getGraphicalUpdaterForModel(aspect);
        graphicalUpdater.addGUListener(aspect, aspectName);

        this.label.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent event) {
                getParentOfType(NavigationBar.class).popSection(typeOfSection);
            }

        });

        this.label.addChild(aspectName);

        labelContainer.addChild(label);

        headerContainer.addChild(labelContainer);
        addFilteredSwitchButton(namer, eObject);
        addExpandableButtonRM(namer, eObject);

        headerContainer.addChild(bottomLabelContainer);
    }

    /**
     * Creates a section in the navigation bar specifically for a Concern element.
     * 
     * @param namer for initializing the menu
     * @param eObject The actual Object of the section
     * @param <T> is the TYPE of the object, which is a COREConcernImpl
     */
    private <T> void createConcernSection(final NavigatonBarNamer<T> namer, final T eObject) {
        RamSpacerComponent fakeUpArrow = new RamSpacerComponent(0.0f, 22.0f);
        topLabelContainer.addChild(fakeUpArrow);
        headerContainer.addChild(topLabelContainer);
        
        this.label.setFillColor(new MTColor(128, 128, 128, MTColor.ALPHA_HALF_TRANSPARENCY));

        this.label.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent event) {
                // Action used to return immediately to the Concern no matter where the user is
                if (RamApp.getApplication().getCurrentScene() instanceof DisplayAspectScene) {

                    boolean isSaveNeeded = EMFEditUtil
                            .getCommandStack(
                                    ((DisplayAspectScene) RamApp.getApplication().getCurrentScene()).getAspect())
                            .isSaveNeeded();
                    if (isSaveNeeded) {
                        DisplayAspectSceneHandler d = (DisplayAspectSceneHandler) ((DisplayAspectScene) RamApp
                                .getApplication().getCurrentScene()).getHandler();
                        d.showCloseConfirmBackToConcern((DisplayAspectScene) RamApp.getApplication().getCurrentScene());

                    } else {

                        getParentOfType(NavigationBar.class).popSection(typeOfSection, (EObject) eObject);
                    }
                } else {
                    getParentOfType(NavigationBar.class).popSection(typeOfSection, (EObject) eObject);
                }
            }

        });
        labelContainer.addChild(label);
        headerContainer.addChild(labelContainer);
        addExpandableButton(namer, eObject);
        headerContainer.addChild(bottomLabelContainer);
    }

    /**
     * Creates a Goal Impact Node section.
     * 
     * @param namer for initializing the menu
     * @param eObject The actual Object of the section
     * @param <T> is the TYPE of the object, which is a COREConcernImpl
     */
    private <T> void createImpactNodeSection(NavigatonBarNamer<T> namer, T eObject) {
        RamSpacerComponent fakeUpArrow = new RamSpacerComponent(0.0f, 22.0f);
        topLabelContainer.addChild(fakeUpArrow);
        headerContainer.addChild(topLabelContainer);

        this.label.setFillColor(new MTColor(128, 128, 128, MTColor.ALPHA_HALF_TRANSPARENCY));

        labelContainer.addChild(label);

        headerContainer.addChild(labelContainer);

        addExpandableButton(namer, eObject);
        headerContainer.addChild(bottomLabelContainer);
    }

    /**
     * Creates a section in the navigation bar specifically for a Feature element.
     * 
     * @param namer for initializing the menu
     * @param eObject The actual Object of the section
     * @param <T> is the TYPE of the object, which is a COREFeatureImpl
     */
    private <T> void createFeatureSection(final NavigatonBarNamer<T> namer, final T eObject) {
        RamSpacerComponent fakeUpArrow = new RamSpacerComponent(0.0f, 22.0f);
        topLabelContainer.addChild(fakeUpArrow);
        headerContainer.addChild(topLabelContainer);

        Vector3D p = this.label.getPosition(TransformSpace.GLOBAL);
        p.y += 30;
        this.label.setPositionGlobal(p);
        this.label.setFillColor(new MTColor(128, 128, 128, MTColor.ALPHA_HALF_TRANSPARENCY));
        
        labelContainer.addChild(this.label);
        
        Vector3D pos = labelContainer.getPosition(TransformSpace.GLOBAL);
        pos.y += 20;
        labelContainer.setPositionGlobal(pos);
        headerContainer.addChild(labelContainer);

        addExpandableButton(namer, eObject);
        headerContainer.addChild(bottomLabelContainer);
    }

    /**
     * Creates a section in the navigation bar specifically for a Realization Model element.
     * 
     * @param namer for initializing the menu
     * @param eObject The actual Object of the section
     * @param <T> is the TYPE of the object, which is a COREAspectImpl
     */
    private <T> void createRMSection(final NavigatonBarNamerRM<T> namer, final T eObject) {
        this.label.removeAllChildren();
        RamSpacerComponent fakeUpArrow = new RamSpacerComponent(0.0f, 22.0f);
        topLabelContainer.addChild(fakeUpArrow);

        headerContainer.addChild(topLabelContainer);

        Vector3D p = this.label.getPosition(TransformSpace.GLOBAL);
        p.y += 25;
        this.label.setPositionGlobal(p);
        this.label.setFillColor(new MTColor(128, 128, 128, MTColor.ALPHA_HALF_TRANSPARENCY));

        EObject aspect = (EObject) eObject;
        
        EStructuralFeature feature = null;
        if (eObject.getClass().equals(ClassDiagramImpl.class)) {
            feature = CdmPackage.Literals.NAMED_ELEMENT__NAME;
        } else if (eObject.getClass().equals(UseCaseModelImpl.class)) {
            feature = UcPackage.Literals.NAMED_ELEMENT__NAME;
        } else {
            feature = RamPackage.Literals.NAMED_ELEMENT__NAME;
        }
        
        TextView aspectName = new TextView(aspect, feature);

        aspectName.setFont(Fonts.DEFAULT_FONT_MENU);
        aspectName.setMaximumHeight(38.0f);
        aspectName.registerTapProcessor(HandlerFactory.INSTANCE.getTextViewHandler());
        graphicalUpdater = RamApp.getApplication().getGraphicalUpdaterForModel(aspect);
        graphicalUpdater.addGUListener(aspect, aspectName);

        this.label.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent event) {
                getParentOfType(NavigationBar.class).popSection(typeOfSection);
            }

        });

        this.label.addChild(aspectName);

        labelContainer.addChild(label);

        headerContainer.addChild(labelContainer);

        addFilteredSwitchButton(namer, eObject);
        addExpandableButtonRM(namer, eObject);
        headerContainer.addChild(bottomLabelContainer);

    }

    /**
     * Method to create a "Build Application" section.
     * 
     * @param namer for initializing the menu
     * @param eObject The actual Object of the section
     * @param <T> is the TYPE of the object
     */
    private <T> void createBuildRMSection(final NavigatonBarNamerRM<T> namer, final T eObject) {
        RamSpacerComponent fakeUpArrow = new RamSpacerComponent(0.0f, 22.0f);
        topLabelContainer.addChild(fakeUpArrow);

        headerContainer.addChild(topLabelContainer);

        this.label.setFillColor(new MTColor(51, 255, 153, MTColor.ALPHA_HALF_TRANSPARENCY));
        this.label.setNoStroke(false);
        this.label.setStrokeColor(new MTColor(0, 204, 102));

        labelContainer.addChild(label);

        headerContainer.addChild(labelContainer);

        addExpandableButtonRM(namer, eObject);
        headerContainer.addChild(bottomLabelContainer);
    }

    /**
     * Creates a section in the navigation bar specifically for a Structural View element.
     * 
     * @param namer for initializing the menu
     * @param eObject the actual Object of the section
     * @param <T> is the TYPE of the object, which is a StructuralViewImpl
     */
    private <T> void createStructuralViewSection(final NavigatonBarNamer<T> namer, final T eObject) {
        RamSpacerComponent fakeUpArrow = new RamSpacerComponent(0.0f, 22.0f);
        topLabelContainer.addChild(fakeUpArrow);
        headerContainer.addChild(topLabelContainer);
        this.label.setFillColor(new MTColor(128, 128, 128, MTColor.ALPHA_HALF_TRANSPARENCY));

        labelContainer.addChild(label);

        headerContainer.addChild(labelContainer);

        addExpandableButton(namer, eObject);
        headerContainer.addChild(bottomLabelContainer);
    }

    /**
     * Creates a section in the navigation bar specifically for a Message View element.
     * 
     * @param namer for initializing the menu
     * @param eObject the actual Object of the section
     * @param <T> is the TYPE of the object, which is a MessageViewImpl
     */
    private <T> void createMessageViewSection(final NavigatonBarNamer<T> namer, final T eObject) {
        RamSpacerComponent fakeUpArrow = new RamSpacerComponent(0.0f, 22.0f);
        topLabelContainer.addChild(fakeUpArrow);
        headerContainer.addChild(topLabelContainer);

        if (eObject instanceof AspectMessageViewImpl) {
            this.label.removeAllChildren();

            Vector3D p = this.label.getPosition(TransformSpace.GLOBAL);
            p.y += 25;
            this.label.setPositionGlobal(p);
            this.label.setFillColor(new MTColor(128, 128, 128, MTColor.ALPHA_HALF_TRANSPARENCY));

            AspectMessageView mA = (AspectMessageView) eObject;

            TextView aspectMessageName = new TextView(mA, RamPackage.Literals.NAMED_ELEMENT__NAME);

            aspectMessageName.setFont(Fonts.DEFAULT_FONT_MENU);
            aspectMessageName.setMaximumHeight(38.0f);
            aspectMessageName.registerTapProcessor(HandlerFactory.INSTANCE.getTextViewHandler());
            this.label.addChild(aspectMessageName);

            labelContainer.addChild(label);
            headerContainer.addChild(labelContainer);

        } else {
            this.label.setFillColor(new MTColor(128, 128, 128, MTColor.ALPHA_HALF_TRANSPARENCY));

            labelContainer.addChild(label);

            headerContainer.addChild(labelContainer);
        }

        addExpandableButtonMessage(namer, eObject);
        headerContainer.addChild(bottomLabelContainer);
    }

    /**
     * Creates a section in the navigation bar specifically for a State View element.
     * 
     * @param namer for initializing the menu
     * @param eObject the actual Object if the section
     * @param <T> is the TYPE of the object, that is StateViewImpl
     */
    private <T> void createStateViewSection(final NavigatonBarNamer<T> namer, final T eObject) {
        RamSpacerComponent fakeUpArrow = new RamSpacerComponent(0.0f, 22.0f);
        topLabelContainer.addChild(fakeUpArrow);
        headerContainer.addChild(topLabelContainer);
        this.label.setFillColor(new MTColor(128, 128, 128, MTColor.ALPHA_HALF_TRANSPARENCY));

        labelContainer.addChild(label);

        headerContainer.addChild(labelContainer);

        addExpandableButton(namer, eObject);
        headerContainer.addChild(bottomLabelContainer);
    }

    /**
     * Creates the menu, call the Namer to initialize it and call the menu manager in {@link NavigationBar} to position
     * it.
     * 
     * @param namer - namer which initialize the menu.
     * @param eObject - related eObject of the section.
     * @param <T> - type of the related eObject.
     */
    private <T> void initMenu(NavigatonBarNamer<T> namer, T eObject) {
        NavigationBarMenu menu = new NavigationBarMenu();
        namer.initMenu(menu, eObject);
        Vector3D menuPosition = expandButton.getPosition(TransformSpace.GLOBAL);
        menuPosition.x += expandButton.getWidthXY(TransformSpace.GLOBAL);
        getParentOfType(NavigationBar.class).openMenu(this, menu, menuPosition);
    }

    /**
     * Creates the menu for the Realization Model section handling the two arrows.
     * 
     * @param namer the namer initializing the menu
     * @param eObject object of the section
     * @param <T> type of eObject
     */
    private <T> void initMenu(NavigatonBarNamerRM<T> namer, T eObject) {
        NavigationBarMenu menu = new NavigationBarMenu();
        namer.initMenu(menu, eObject);
        Vector3D menuPosition = expandButton.getPosition(TransformSpace.GLOBAL);
        menuPosition.x += expandButton.getWidthXY(TransformSpace.GLOBAL);
        getParentOfType(NavigationBar.class).openMenu(this, menu, menuPosition);
    }

    /**
     * Creates the menu for the filtered switch choices of the UP arrow for the Realization Model.
     * 
     * @param menu menu where to add the elements
     * @param namer - namer to initialize the menu
     * @param eObject - the Object to which the section references to
     * @param <T> - type of the related eObject (= AspectImpl)
     */
    private <T> void initFanOutMenu(NavigationBarMenu menu, NavigatonBarNamerRM<T> namer, T eObject) {
        filteredSwitchUpButton.setVisible(true);
        PImage expandIcon = Icons.ICON_FILTERED_SWITCH_EXPANDED;
        filteredSwitchUpButton.getIconImage().setTexture(expandIcon);

        Vector3D menuPosition = filteredSwitchUpButton.getPosition(TransformSpace.GLOBAL);
        menuPosition.x += filteredSwitchUpButton.getWidthXY(TransformSpace.GLOBAL);
        getParentOfType(NavigationBar.class).openMenuFanOut(this, menu, menuPosition);
    }

    /**
     * Updates the expand button icon in function of the given boolean. When the parameter is true the icon will be the
     * expand one. Otherwise it will be the collapse one. It also updated the class state to menu shown.
     * 
     * @param expand - true if you want the expand icon, false for the collapse icon.
     */
    public void updateExpandButton(boolean expand) {
        if (typeOfSection.equals(OperationImpl.class)) {
            PImage expandIcon = expand ? Icons.ICON_FILTERED_SWITCH_EXPANDED : Icons.ICON_FILTERED_SWITCH_COLLAPSE;
            expandButton.getIconImage().setTexture(expandIcon);
            menuShown = expand;
        } else {
            PImage expandIcon = expand ? Icons.ICON_NAVIGATION_EXPAND : Icons.ICON_NAVIGATION_COLLAPSE;
            expandButton.getIconImage().setTexture(expandIcon);
            menuShown = expand;
        }
    }

    /**
     * Updates the filteredSwitchUpButton whenever from a different section and expand button is clicked/tapped so that
     * the UP arrow returns to where it has to be and the menu closes.
     * 
     * @param expand toggle the second menu of a RM section
     */
    public void updateUpArrowSwitch(boolean expand) {
        PImage expandIcon = expand ? Icons.ICON_FILTERED_SWITCH_EXPANDED : Icons.ICON_FILTERED_SWITCH_COLLAPSE;
        
        if (filteredSwitchUpButton != null) {
            filteredSwitchUpButton.setNoStroke(true);
            filteredSwitchUpButton.getIconImage().setTexture(expandIcon);
        }
        
        menuShown = expand;
    }

    /**
     * Add an expandable button in the header.
     * 
     * @param namer - namer which constructs the menu. Null to not add a menu.
     * @param eObject - related eObject of the section.
     * @param <T> - type of the related eObject.
     */
    public <T> void addExpandableButton(final NavigatonBarNamer<T> namer, final T eObject) {
        if (this.expandButton == null) {
            RamImageComponent expandImage = new RamImageComponent(Icons.ICON_NAVIGATION_COLLAPSE, MTColor.WHITE);
            expandImage.setSizeLocal(32, 32);
            this.expandButton = new RamButton(expandImage);
            this.expandButton.setAutoMaximizes(false);
            this.expandButton.setAutoMinimizes(false);

            // EXPAND BUTTON WILL ALWAYS BE IN THE MIDDLE POINT OF THE SECTION OF THE MENU
            expandButton.setPositionRelativeToOther(labelContainer, labelContainer.getCenterPointGlobal());

            if (this.filteredSwitchUpButton != null) {
                filteredSwitchUpButton
                        .setPositionRelativeToOther(labelContainer, labelContainer.getCenterPointGlobal());
            }

            if (namer != null) {
                this.expandButton.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent event) {
                        if (!menuShown) {
                            initMenu(namer, eObject);
                        } else {
                            getParentOfType(NavigationBar.class).closeMenu();
                        }

                    }

                });
            }

            bottomLabelContainer.addChild(this.expandButton);
        }
    }

    /**
     * Add the expandable button for a Message View: The arrow points backward to the other possible Operations.
     * 
     * @param namer to initialize the menu
     * @param eObject of section
     * @param <T> type of eObject treated (= MessageViewImpl)
     */
    public <T> void addExpandableButtonMessage(final NavigatonBarNamer<T> namer, final T eObject) {
        if (this.expandButton == null) {
            RamImageComponent expandImage = new RamImageComponent(Icons.ICON_FILTERED_SWITCH_COLLAPSE, MTColor.WHITE);
            expandImage.setSizeLocal(32, 32);
            this.expandButton = new RamButton(expandImage);
            this.expandButton.setAutoMaximizes(false);
            this.expandButton.setAutoMinimizes(false);

            // EXPAND BUTTON WILL ALWAYS BE IN THE MIDDLE POINT OF THE SECTION OF THE MENU
            expandButton.setPositionRelativeToOther(labelContainer, labelContainer.getCenterPointGlobal());

            if (this.filteredSwitchUpButton != null) {
                filteredSwitchUpButton
                        .setPositionRelativeToOther(labelContainer, labelContainer.getCenterPointGlobal());
            }

            if (namer != null) {
                this.expandButton.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent event) {
                        if (!menuShown) {
                            initMenu(namer, eObject);
                        } else {
                            getParentOfType(NavigationBar.class).closeMenu();
                        }
                    }
                });
            }

            bottomLabelContainer.addChild(this.expandButton);
        }
    }

    /**
     * Method handling the arrows to be added to a Realization Model section.
     * 
     * @param namer the namer for the menu
     * @param eObject of the section data
     * @param <T> type of eObject
     */
    public <T> void addExpandableButtonRM(final NavigatonBarNamerRM<T> namer, final T eObject) {
        if (this.expandButton == null) {
            RamImageComponent expandImage = new RamImageComponent(Icons.ICON_NAVIGATION_COLLAPSE, MTColor.WHITE);
            expandImage.setSizeLocal(32, 32);
            this.expandButton = new RamButton(expandImage);
            this.expandButton.setAutoMaximizes(false);
            this.expandButton.setAutoMinimizes(false);

            // EXPAND BUTTON WILL ALWAYS BE IN THE MIDDLE POINT OF THE SECTION OF THE MENU
            expandButton.setPositionRelativeToOther(labelContainer, labelContainer.getCenterPointGlobal());

            if (this.filteredSwitchUpButton != null) {
                filteredSwitchUpButton
                        .setPositionRelativeToOther(labelContainer, labelContainer.getCenterPointGlobal());
            }

            if (namer != null) {
                this.expandButton.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent event) {
                        if (!menuShown) {
                            initMenu(namer, eObject);
                        } else {
                            getParentOfType(NavigationBar.class).closeMenu();
                        }
                    }

                });
            } else {
                RamSpacerComponent buffer = new RamSpacerComponent(32.0f, 32.0f);
                bottomLabelContainer.addChild(buffer);
            }

            bottomLabelContainer.addChild(this.expandButton);
        }
    }

    /**
     * Add the filtered switch UP arrow in case of a Realization Model section in the bar.
     * 
     * @param namer - namer which constructs the menu. Null to not add a menu.
     * @param eObject - related eObject of the section.
     * @param <T> - type of the related eObject.
     */
    public <T> void addFilteredSwitchButton(final NavigatonBarNamerRM<T> namer, final T eObject) {
        if (this.filteredSwitchUpButton == null) {
            RamImageComponent switchImage = new RamImageComponent(Icons.ICON_FILTERED_SWITCH_COLLAPSE, MTColor.WHITE);
            switchImage.setSizeLocal(32, 32);
            this.filteredSwitchUpButton = new RamButton(switchImage);
            this.filteredSwitchUpButton.setAutoMaximizes(false);
            this.filteredSwitchUpButton.setAutoMinimizes(false);
            
            if (namer != null) {
                final NavigationBarMenu menu = new NavigationBarMenu();
                namer.initFanOutMenu(menu, eObject);
                if (menu.containsElements()) {

                    this.filteredSwitchUpButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent event) {
                            if (!menuShown) {
                                initFanOutMenu(menu, namer, eObject);
                            } else {
                                // filteredSwitchUpButton.setPositionGlobal(back);
                                getParentOfType(NavigationBar.class).closeMenuFanOut();
                                filteredSwitchUpButton.setNoStroke(true);
                                PImage switchback = Icons.ICON_FILTERED_SWITCH_COLLAPSE;
                                filteredSwitchUpButton.getIconImage().setTexture(switchback);
                                // expandButton.setVisible(true);
                                bottomLabelContainer.addChild(filteredSwitchUpButton);
                                bottomLabelContainer.addChild(expandButton);
                            }
                        }

                    });

                    bottomLabelContainer.addChild(this.filteredSwitchUpButton);
                } else {
                    filteredSwitchUpButton.setVisible(false);
                }
            }
        }
    }

    /**
     * Removes the expand button if it exists and if there is no menu to show.
     * 
     * @return needed for removing the within notation fullstop
     */
    public boolean removeUnusedExpandButton() {
        boolean value;
        
        if (this.expandButton != null && !this.expandButton.hasListeners()) {
            headerContainer.removeChild(this.expandButton);
            
            if (filteredSwitchUpButton != null) {
                headerContainer.removeChild(this.filteredSwitchUpButton);
            }
            this.expandButton.destroy();
            this.expandButton = null;
            if (filteredSwitchUpButton != null) {
                this.filteredSwitchUpButton.destroy();
                this.filteredSwitchUpButton = null;
            }
            value = true;
        } else if (this.filteredSwitchUpButton != null) {
            if (!this.filteredSwitchUpButton.hasListeners()) {
                value = true;
            }
            value = false;
        } else {
            value = false;
        }
        return value;
    }

    /**
     * Method needed for retrieving the type of section inside the NavigationBar class.
     * 
     * @return type of section
     */
    public Object retrieveTypeOfSection() {
        return typeOfSection;
    }

    /**
     * Method to toggle the removal of arrow from a section that does not have any successor.
     */
    public void hideArrows() {
        bottomLabelContainer.removeAllChildren();
        RamSpacerComponent fakeArrow = new RamSpacerComponent(0.0f, 32.0f);
        bottomLabelContainer.addChild(fakeArrow);
    }

}
