package ca.mcgill.sel.usecases.ui.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.mt4j.sceneManagement.Iscene;

import ca.mcgill.sel.classdiagram.ui.views.ClassDiagramView;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.util.MetamodelRegex;
import ca.mcgill.sel.usecases.Actor;
import ca.mcgill.sel.usecases.ui.scenes.DisplayUseCaseDiagramScene;
import ca.mcgill.sel.usecases.ui.views.UseCaseDiagramView;
import ca.mcgill.sel.usecases.util.Multiplicity;

/**
 * Utility methods for use case diagrams in the UI.
 * @author rlanguay
 *
 */
public final class UcdModelUtils {
    private static final int UPPER_BOUND_INDEX = 3;
        
    /**
     * Creates a new instance with default pretty printers.
     */
    private UcdModelUtils() {
    }

    /**
     * Retrieves the class diagram view from the RamApp if viewing a class diagram.
     * @param app currently running RamApp
     * @return class diagram view currently displayed {@link ClassDiagramView}
     */
    public static UseCaseDiagramView getUseCaseDiagramViewFromApp(RamApp app) {
        final Iscene scene = RamApp.getActiveScene();
        if (scene != null && scene instanceof DisplayUseCaseDiagramScene) {
            final DisplayUseCaseDiagramScene ucdScene = (DisplayUseCaseDiagramScene) scene;
            return ucdScene.getUseCaseDiagramView();
        }
        return null;
    }
    
    /**
     * Gets the multiplicity of an actor, as displayed in the UI.
     * @param actor The actor
     * @return The multiplicity string to display
     */
    public static String getActorMultiplicity(Actor actor) {
        String upperBound;
        
        if (actor.getUpperBound() == -1) {
            upperBound = "*";
        } else {
            upperBound = String.valueOf(actor.getUpperBound());
        }

        if (actor.getLowerBound() == actor.getUpperBound()) {
            return upperBound;
        } else {
            StringBuilder builder = new StringBuilder();

            builder.append(actor.getLowerBound());
            builder.append("..");
            builder.append(upperBound);

            return builder.toString();
        }
    }
    
    public static Multiplicity parseMultiplicity(String text) {
        // For example: 0..* --> group(0) = 0..*, group(1) = 0, group(2)= .. ,group(3) = *
        Matcher matcher = Pattern.compile(MetamodelRegex.REGEX_GROUP_MULTIPLICITY).matcher(text);
        if (!matcher.matches()) {
            return null;
        }

        // it may be null because allowed inputs can be : number OR number..number OR number..*
        String upperBoundString = matcher.group(UPPER_BOUND_INDEX);
        int upperBound;
        int lowerBound = Integer.parseInt(matcher.group(1));
        if (upperBoundString == null) {
            upperBound = lowerBound;
        } else {
            if ("*".equals(upperBoundString)) {
                upperBound = -1;
            } else {
                upperBound = Integer.parseInt(upperBoundString);
                if (upperBound < lowerBound) {
                    return null;
                }
            }
        }
        
        return new Multiplicity(lowerBound, upperBound);
    }
}
