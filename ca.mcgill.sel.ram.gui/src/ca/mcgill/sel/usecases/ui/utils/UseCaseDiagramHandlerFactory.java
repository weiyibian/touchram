package ca.mcgill.sel.usecases.ui.utils;

import ca.mcgill.sel.ram.ui.views.handler.IDisplaySceneHandler;
import ca.mcgill.sel.ram.ui.views.handler.IRelationshipViewHandler;
import ca.mcgill.sel.ram.ui.views.handler.ITextViewHandler;
import ca.mcgill.sel.ram.ui.views.handler.impl.TextViewHandler;
import ca.mcgill.sel.usecases.ui.scenes.handler.impl.DisplayUseCaseDiagramSceneHandler;
import ca.mcgill.sel.usecases.ui.views.handler.IActorViewHandler;
import ca.mcgill.sel.usecases.ui.views.handler.INoteViewHandler;
import ca.mcgill.sel.usecases.ui.views.handler.IUseCaseDiagramViewHandler;
import ca.mcgill.sel.usecases.ui.views.handler.IUseCaseViewHandler;
import ca.mcgill.sel.usecases.ui.views.handler.impl.ActorMultiplicityHandler;
import ca.mcgill.sel.usecases.ui.views.handler.impl.ActorViewHandler;
import ca.mcgill.sel.usecases.ui.views.handler.impl.AssociationViewHandler;
import ca.mcgill.sel.usecases.ui.views.handler.impl.NoteViewHandler;
import ca.mcgill.sel.usecases.ui.views.handler.impl.UseCaseDiagramViewHandler;
import ca.mcgill.sel.usecases.ui.views.handler.impl.UseCaseViewHandler;

public final class UseCaseDiagramHandlerFactory {
    /**
     * The singleton instance of the factory.
     */
    public static final UseCaseDiagramHandlerFactory INSTANCE = new UseCaseDiagramHandlerFactory();
    
    private IDisplaySceneHandler useCaseDiagramDisplaySceneHandler;
    private IUseCaseDiagramViewHandler useCaseDiagramViewHandler;
    private IUseCaseViewHandler useCaseViewHandler;
    private IActorViewHandler actorViewHandler;
    private INoteViewHandler noteViewHandler;
    private IRelationshipViewHandler associationViewHandler;
    private ITextViewHandler noteContentHandler;
    private ITextViewHandler textViewHandler;
    private ITextViewHandler actorMultiplicityViewHandler;
    
    private UseCaseDiagramHandlerFactory() {      
    }
    
    /**
     * Gets the handler for the use case diagram scene.
     * @return The handler for the scene
     */
    public IDisplaySceneHandler getUseCaseDiagramDisplaySceneHandler() {
        if (this.useCaseDiagramDisplaySceneHandler == null) {
            this.useCaseDiagramDisplaySceneHandler = new DisplayUseCaseDiagramSceneHandler();
        }
        
        return this.useCaseDiagramDisplaySceneHandler;
    }
    
    /**
     * Gets the handler for the use case diagram view.
     * @return The handler for the view
     */
    public IUseCaseDiagramViewHandler getUseCaseDiagramViewHandler() {
        if (this.useCaseDiagramViewHandler == null) {
            this.useCaseDiagramViewHandler = new UseCaseDiagramViewHandler();
        }
        
        return this.useCaseDiagramViewHandler;
    }
    
    /**
     * Gets the handler for the note view.
     * @return The handler for the view
     */
    public INoteViewHandler getNoteViewHandler() {
        if (this.noteViewHandler == null) {
            this.noteViewHandler = new NoteViewHandler();
        }
        
        return this.noteViewHandler;
    }
    
    /**
     * Gets the handler for the use case view.
     * @return The handler for the view
     */
    public IUseCaseViewHandler getUseCaseViewHandler() {
        if (this.useCaseViewHandler == null) {
            this.useCaseViewHandler = new UseCaseViewHandler();
        }
        
        return this.useCaseViewHandler;
    }
    
    /**
     * Gets the handler for the actor view.
     * @return The handler for the view
     */
    public IActorViewHandler getActorViewHandler() {
        if (this.actorViewHandler == null) {
            this.actorViewHandler = new ActorViewHandler();
        }
        
        return this.actorViewHandler;
    }
    
    /**
     * Gets the handler for the note content.
     * @return the handler
     */
    public ITextViewHandler getNoteContentHandler() {
        if (this.noteContentHandler == null) {
            this.noteContentHandler = new TextViewHandler();
        }
        
        return this.noteContentHandler;
    }
    
    /**
     * Gets a handler for generic text views.
     * @return the handler
     */
    public ITextViewHandler getTextViewHandler() {
        if (this.textViewHandler == null) {
            this.textViewHandler = new TextViewHandler();
        }
        
        return this.textViewHandler;
    }
    
    /**
     * Gets the actor multiplicity text view handler.
     * @return the handler
     */
    public ITextViewHandler getActorMultiplicityViewHandler() {
        if (this.actorMultiplicityViewHandler == null) {
            this.actorMultiplicityViewHandler = new ActorMultiplicityHandler();
        }
        
        return this.actorMultiplicityViewHandler;
    }
    
    /**
     * Gets the handler for association views.
     * @return the handler
     */
    public IRelationshipViewHandler getAssociationViewHandler() {
        if (this.associationViewHandler == null) {
            this.associationViewHandler = new AssociationViewHandler();
        }
        
        return this.associationViewHandler;
    }
}
