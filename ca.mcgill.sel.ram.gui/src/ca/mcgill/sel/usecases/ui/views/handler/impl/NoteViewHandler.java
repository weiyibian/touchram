package ca.mcgill.sel.usecases.ui.views.handler.impl;

import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.menu.RamLinkedMenu;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.usecases.Note;
import ca.mcgill.sel.usecases.language.controller.ControllerFactory;
import ca.mcgill.sel.usecases.language.controller.UseCaseModelController;
import ca.mcgill.sel.usecases.ui.views.BaseView;
import ca.mcgill.sel.usecases.ui.views.NoteView;
import ca.mcgill.sel.usecases.ui.views.handler.INoteViewHandler;

public class NoteViewHandler extends BaseViewHandler implements INoteViewHandler {

    private static final String ACTION_DELETE_ANNOTATIONS = "view.note.annotations.delete";
    
    @Override
    public void removeRepresented(BaseView<?> baseView) {
        ControllerFactory.INSTANCE.getUseCaseDiagramController().removeNote(
                (Note) baseView.getRepresented());                
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        String actionCommand = event.getActionCommand();
        RamRectangleComponent pressedButton = (RamRectangleComponent) event.getTarget();
        RamLinkedMenu linkedMenu = (RamLinkedMenu) pressedButton.getParentOfType(RamLinkedMenu.class);
        if (linkedMenu != null) {

            NoteView noteView = (NoteView) linkedMenu.getLinkedView();

            if (ACTION_DELETE_ANNOTATIONS.equals(actionCommand)) {
                UseCaseModelController controller = ControllerFactory.INSTANCE.getUseCaseDiagramController();
                Note note = noteView.getNote();
                
                controller.removeAnnotations(note);
            }
        }
        super.actionPerformed(event);
    }
    
    @Override
    public void initMenu(RamLinkedMenu menu) {
        super.initMenu(menu);
        menu.addAction(Strings.MENU_DELETE_ANNOTATIONS, Icons.ICON_MENU_CLEAR_SELECTION,
                ACTION_DELETE_ANNOTATIONS, this, SUBMENU_ADD, true);
    }
}
