package ca.mcgill.sel.usecases.ui.views;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.provider.INotifyChangedListener;
import org.mt4j.input.gestureAction.TapAndHoldVisualizer;
import org.mt4j.input.inputProcessors.IGestureEventListener;
import org.mt4j.input.inputProcessors.componentProcessors.panProcessor.PanProcessorTwoFingers;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldProcessor;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapProcessor;
import org.mt4j.input.inputProcessors.componentProcessors.unistrokeProcessor.UnistrokeUtils.Direction;
import org.mt4j.input.inputProcessors.componentProcessors.unistrokeProcessor.UnistrokeUtils.UnistrokeGesture;
import org.mt4j.input.inputProcessors.componentProcessors.zoomProcessor.ZoomProcessor;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.events.MouseWheelProcessor;
import ca.mcgill.sel.ram.ui.events.RightClickDragProcessor;
import ca.mcgill.sel.ram.ui.events.UnistrokeProcessorLeftClick;
import ca.mcgill.sel.ram.ui.utils.GUIConstants;
import ca.mcgill.sel.ram.ui.views.AbstractView;
import ca.mcgill.sel.ram.ui.views.RelationshipView;
import ca.mcgill.sel.usecases.Actor;
import ca.mcgill.sel.usecases.LayoutElement;
import ca.mcgill.sel.usecases.NamedElement;
import ca.mcgill.sel.usecases.Note;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.UseCase;
import ca.mcgill.sel.usecases.UseCaseModel;
import ca.mcgill.sel.usecases.impl.ContainerMapImpl;
import ca.mcgill.sel.usecases.impl.ElementMapImpl;
import ca.mcgill.sel.usecases.ui.utils.UseCaseDiagramHandlerFactory;
import ca.mcgill.sel.usecases.ui.views.AssociationView.AssociationType;
import ca.mcgill.sel.usecases.ui.views.handler.IUseCaseDiagramViewHandler;

public class UseCaseDiagramView extends AbstractView<IUseCaseDiagramViewHandler>
        implements INotifyChangedListener {

    private UseCaseModel useCaseModel;
    private ContainerMapImpl layout;
    
    private Set<BaseView<?>> selectedElements;
    
    private HashMap<Actor, ActorView> actorToViewMap;
    private HashMap<UseCase, UseCaseView> useCaseToViewMap;
    private HashMap<Note, NoteView> noteToViewMap;
    private List<AssociationView> associationViewList;
    private List<AnnotationView> annotationViewList;
    
    public UseCaseDiagramView(UseCaseModel ucd, ContainerMapImpl layout, float width, float height) {
        super(width, height);
        
        this.useCaseModel = ucd;
        this.layout = layout;
        
        this.actorToViewMap = new HashMap<Actor, ActorView>();
        this.useCaseToViewMap = new HashMap<UseCase, UseCaseView>();
        this.noteToViewMap = new HashMap<Note, NoteView>();
        this.associationViewList = new ArrayList<AssociationView>();
        this.annotationViewList = new ArrayList<AnnotationView>();
        
        buildAndLayout(width, height);
        
        EMFEditUtil.addListenerFor(ucd, this);
        // register to the ContainerMap to receive adds/removes of ElementMaps
        EMFEditUtil.addListenerFor(this.layout, this);
        
        this.selectedElements = new HashSet<BaseView<?>>();
    }

    @Override
    public void notifyChanged(Notification notification) {
        // The map as a whole can be changed
        if (notification.getFeature() == UcPackage.Literals.CONTAINER_MAP__VALUE) {
            if (notification.getEventType() == Notification.ADD) {
                ElementMapImpl elementMap = (ElementMapImpl) notification.getNewValue();
                if (elementMap.getKey() instanceof Actor) {
                    actorToViewMap.get(elementMap.getKey()).setLayoutElement(elementMap.getValue());
                } else if (elementMap.getKey() instanceof UseCase) {
                    useCaseToViewMap.get(elementMap.getKey()).setLayoutElement(elementMap.getValue());
                } else if (elementMap.getKey() instanceof Note) {
                    noteToViewMap.get(elementMap.getKey()).setLayoutElement(elementMap.getValue());
                }
            }
        } else if (notification.getFeature() == UcPackage.Literals.USE_CASE_MODEL__ACTORS) {
            Actor actor = null;
            switch (notification.getEventType()) {
                case Notification.ADD:
                    actor = (Actor) notification.getNewValue();
                    addActor(actor, layout.getValue().get(actor));                    
                    break;
                case Notification.REMOVE:
                    actor = (Actor) notification.getOldValue();
                    deleteActor(actor);
                    break;
            }
        } else if (notification.getFeature() == UcPackage.Literals.USE_CASE_MODEL__USE_CASES) {
            UseCase useCase = null;
            switch (notification.getEventType()) {
                case Notification.ADD:
                    useCase = (UseCase) notification.getNewValue();
                    addUseCase(useCase, layout.getValue().get(useCase));                    
                    break;
                case Notification.REMOVE:
                    useCase = (UseCase) notification.getOldValue();
                    deleteUseCase(useCase);
                    break;
            }
        } else if (notification.getFeature() == UcPackage.Literals.USE_CASE_MODEL__NOTES) {
            Note note = null;
            switch (notification.getEventType()) {
                case Notification.ADD:
                    note = (Note) notification.getNewValue();
                    addNote(note, layout.getValue().get(note));
                    for (NamedElement annotated : note.getNotedElement()) {
                        addAnnotationView(note, annotated);
                    }
                    break;
                case Notification.REMOVE:
                    note = (Note) notification.getOldValue();
                    deleteNote(note);
                    break;
            }
        }
    }
    
    @Override
    protected void registerGestureListeners(IGestureEventListener listener) {
        super.registerGestureListeners(listener);

        addGestureListener(TapProcessor.class, listener);
        addGestureListener(TapAndHoldProcessor.class, listener);
        addGestureListener(TapAndHoldProcessor.class, new TapAndHoldVisualizer(RamApp.getApplication(), getParent()));
    }

    @Override
    protected void registerInputProcessors() {
        registerInputProcessor(new TapProcessor(RamApp.getApplication(), GUIConstants.TAP_MAX_FINGER_UP, false,
                GUIConstants.TAP_DOUBLE_TAP_TIME));
        registerInputProcessor(new TapAndHoldProcessor(RamApp.getApplication(), GUIConstants.TAP_AND_HOLD_DURATION));
        registerInputProcessor(new PanProcessorTwoFingers(RamApp.getApplication()));
        registerInputProcessor(new RightClickDragProcessor(RamApp.getApplication()));
        registerInputProcessor(new ZoomProcessor(RamApp.getApplication()));
        registerInputProcessor(new MouseWheelProcessor(RamApp.getApplication()));

        UnistrokeProcessorLeftClick up = new UnistrokeProcessorLeftClick(RamApp.getApplication());
        up.addTemplate(UnistrokeGesture.CIRCLE, Direction.CLOCKWISE);
        up.addTemplate(UnistrokeGesture.CIRCLE, Direction.COUNTERCLOCKWISE);

        registerInputProcessor(up);
    }
    
    /**
     * Gets the use case diagram associated with the view.
     * @return The use case diagram
     */
    public UseCaseModel getUseCaseModel() {
        return this.useCaseModel;
    }
    
    /**
     * Returns a collection of all base views.
     * This includes {@link ClassView}, {@link NoteView}, {@link ImplementationClassView} and {@link EnumView}.
     * 
     * @return a collection of all base views
     */
    public Collection<BaseView<?>> getBaseViews() {
        Collection<BaseView<?>> baseViews = new HashSet<BaseView<?>>();

        baseViews.addAll(actorToViewMap.values());
        baseViews.addAll(useCaseToViewMap.values());
        baseViews.addAll(noteToViewMap.values());

        return baseViews;
    }
    
    /**
     * Removes the given {@link BaseView} from the list of selected views.
     * 
     * @param baseView the {@link BaseView} to remove
     */
    protected void elementDeselected(BaseView<?> baseView) {
        selectedElements.remove(baseView);
    }

    /**
     * Adds the given {@link BaseView} to the list of selected views.
     * 
     * @param baseView the baseView to add to the selection
     */
    protected void elementSelected(BaseView<?> baseView) {
        selectedElements.add(baseView);
    }
    
    public Set<BaseView<?>> getSelectedElements() {
        return this.selectedElements;
    }
    
    public ActorView getActorView(Actor actor) {
        return this.actorToViewMap.get(actor);
    }
    
    public UseCaseView getUseCaseView(UseCase useCase) {
        return this.useCaseToViewMap.get(useCase);
    }
    
    public NoteView getNoteView(Note note) {
        return this.noteToViewMap.get(note);
    }
    
    /**
     * Deselects all currently selected classifiers.
     */
    public void deselect() {
        // Use separate set here, otherwise a concurrent modification occurs,
        // because the view notifies us that it was deselected, which triggers
        // the removal of the view from our set.
        for (BaseView<?> baseView : new HashSet<BaseView<?>>(selectedElements)) {
            baseView.setSelect(false);
        }

        selectedElements.clear();
    }
    
    @Override
    public void destroy() {
        // remove listeners
        EMFEditUtil.removeListenerFor(layout, this);
        EMFEditUtil.removeListenerFor(this.useCaseModel, this);

        for (AssociationView view : associationViewList) {
            view.destroy();
        }

        // destroy basic views
        for (BaseView<?> view : getBaseViews()) {
            view.destroy();
        }
        
        // do rest
        super.destroy();
    }
    
    private void buildAndLayout(float width, float height) {
        Vector3D position = new Vector3D(100, 100);

        float maxX = width;
        float maxY = height;
        
        for (Actor actor : useCaseModel.getActors()) {
            LayoutElement layoutElement = layout == null ? null : layout.getValue().get(actor);
            addActor(actor, layoutElement);
            ActorView actorView = actorToViewMap.get(actor);

            position.setX(Math.max(position.getX(), layoutElement.getX() + actorView.getWidth()));
            position.setY(Math.max(position.getY(), layoutElement.getY() + actorView.getHeight()));

            maxX = Math.max(maxX, actorView.getX() + actorView.getWidth());
            maxY = Math.max(maxY, actorView.getY() + actorView.getHeight());
        }
        
        for (UseCase useCase : useCaseModel.getUseCases()) {
            LayoutElement layoutElement = layout == null ? null : layout.getValue().get(useCase);
            addUseCase(useCase, layoutElement);
            UseCaseView useCaseView = useCaseToViewMap.get(useCase);

            position.setX(Math.max(position.getX(), layoutElement.getX() + useCaseView.getWidth()));
            position.setY(Math.max(position.getY(), layoutElement.getY() + useCaseView.getHeight()));

            maxX = Math.max(maxX, useCaseView.getX() + useCaseView.getWidth());
            maxY = Math.max(maxY, useCaseView.getY() + useCaseView.getHeight());
        }
        
        for (Note note : useCaseModel.getNotes()) {
            LayoutElement layoutElement = layout == null ? null : layout.getValue().get(note);
            addNote(note, layoutElement);
            NoteView noteView = noteToViewMap.get(note);

            position.setX(Math.max(position.getX(), layoutElement.getX() + noteView.getWidth()));
            position.setY(Math.max(position.getY(), layoutElement.getY() + noteView.getHeight()));

            maxX = Math.max(maxX, noteView.getX() + noteView.getWidth());
            maxY = Math.max(maxY, noteView.getY() + noteView.getHeight());
        }
        
        for (UseCase useCase : useCaseModel.getUseCases()) {
            addAssociationViews(useCase);
        }
        
     // Create annotation links.
        for (Note note : useCaseModel.getNotes()) {
            for (NamedElement annotatedElement : note.getNotedElement()) {
                addAnnotationView(note, annotatedElement);
            }
        }
    }
    
    private void addActor(Actor actor, LayoutElement layoutElement) {
        ActorView view = new ActorView(this, actor, layoutElement);
        actorToViewMap.put(actor, view);
        addChild(view);
        view.setHandler(UseCaseDiagramHandlerFactory.INSTANCE.getActorViewHandler());
    }
    
    private void deleteActor(Actor actor) {
        ActorView actorView = this.actorToViewMap.remove(actor);
        
        removeChild(actorView);
        actorView.destroy();        
    }    
    
    private void addUseCase(UseCase useCase, LayoutElement layoutElement) {
        UseCaseView view = new UseCaseView(this, useCase, layoutElement);
        useCaseToViewMap.put(useCase, view);
        addChild(view);
        view.setHandler(UseCaseDiagramHandlerFactory.INSTANCE.getUseCaseViewHandler());
    }
    
    private void deleteUseCase(UseCase useCase) {
        UseCaseView useCaseView = this.useCaseToViewMap.remove(useCase);
        
        removeChild(useCaseView);
        useCaseView.destroy();        
    }    
    
    private void addNote(Note note, LayoutElement layoutElement) {
        NoteView noteView = new NoteView(this, note, layoutElement);
        noteToViewMap.put(note, noteView);
        addChild(noteView);
        noteView.setHandler(UseCaseDiagramHandlerFactory.INSTANCE.getNoteViewHandler());
    }
    
    private void deleteNote(Note note) {
        NoteView noteView = this.noteToViewMap.remove(note);
        for (RelationshipView<?, ? extends RamRectangleComponent> view : noteView.getAllRelationshipViews()) {
            if (view instanceof AnnotationView) {
                ((AnnotationView) view).destroyOppositeEnd();
            }
        }
        
        removeChild(noteView);
        noteView.destroy();
    }
    
    private void addAssociationViews(UseCase useCase) {
        UseCaseView useCaseView = useCaseToViewMap.get(useCase);
        
        Actor primaryActor = useCase.getPrimaryActor();
        if (primaryActor != null) {
            ActorView actorView = actorToViewMap.get(primaryActor);
            AssociationView associationView = new AssociationView(
                    useCase, useCaseView, primaryActor, actorView, AssociationType.PRIMARY);
            associationView.setBackgroundColor(this.getFillColor());
            associationView.updateLines();
            associationView.setHandler(UseCaseDiagramHandlerFactory.INSTANCE.getAssociationViewHandler());
            addChild(associationView);
            associationViewList.add(associationView);
        }
        
        Collection<Actor> secondaryActors = useCase.getSecondaryActors();
        if (!secondaryActors.isEmpty()) {
            for (Actor secondaryActor : secondaryActors) {
                ActorView actorView = actorToViewMap.get(secondaryActor);
                AssociationView associationView = new AssociationView(
                        useCase, useCaseView, secondaryActor, actorView, AssociationType.SECONDARY);
                associationView.setBackgroundColor(this.getFillColor());
                associationView.updateLines();
                associationView.setHandler(UseCaseDiagramHandlerFactory.INSTANCE.getAssociationViewHandler());
                addChild(associationView);
                associationViewList.add(associationView);
            }
        }
    }
    
    public void addAnnotationView(Note note, NamedElement annotatedElement) {
        BaseView<?> annotatedElementView = null;
        NoteView noteView = noteToViewMap.get(note);

        if (actorToViewMap.containsKey(annotatedElement)) {
            annotatedElementView = actorToViewMap.get(annotatedElement);
        } else if (useCaseToViewMap.containsKey(annotatedElement)) {
            annotatedElementView = useCaseToViewMap.get(annotatedElement);
        } else {
            return;
        }

        AnnotationView annotationView = new AnnotationView(noteView, annotatedElementView);

        annotationViewList.add(annotationView);

        addChild(annotationView);
        annotationView.updateLines();
    }
    
    public void removeAnnotationView(Note note, NamedElement annotatedElement) {
        AnnotationView annotViewToBeDeleted = null;

        for (AnnotationView annotView : annotationViewList) {

            Note noteFrom = annotView.getNoteView().getNote();
            NamedElement annotatedElementTo = (NamedElement) annotView.getAnnotatedElementView().getRepresented();

            if (noteFrom == note && annotatedElementTo == annotatedElement) {
                annotViewToBeDeleted = annotView;
                break;
            }
        }

        if (annotViewToBeDeleted != null) {
            annotationViewList.remove(annotViewToBeDeleted);
            removeChild(annotViewToBeDeleted);

            annotViewToBeDeleted.destroy();
        }
    }
    
    public void addAssociationView(UseCase useCase, Actor actor, AssociationType type) {
        UseCaseView useCaseView = useCaseToViewMap.get(useCase);
        ActorView actorView = actorToViewMap.get(actor);
        AssociationView associationView = new AssociationView(
                useCase, useCaseView, actor, actorView, type);
        associationView.setBackgroundColor(this.getFillColor());
        associationView.updateLines();
        associationView.setHandler(UseCaseDiagramHandlerFactory.INSTANCE.getAssociationViewHandler());
        associationViewList.add(associationView);
        addChild(associationView);
    }
    
    public void removeAssociationView(EObject end1, EObject end2) {
        AssociationView viewToDelete = null;
        
        for (AssociationView view : associationViewList) {
            EObject e1 = view.getToEnd().getModel();
            EObject e2 = view.getFromEnd().getModel();

            if (end1 == e1 && end2 == e2 || end1 == e2 && end2 == e1) {
                viewToDelete = view;
                break;
            }
        }

        if (viewToDelete != null) {
            associationViewList.remove(viewToDelete);
            removeChild(viewToDelete);

            viewToDelete.destroy();
        }
    }
    
    /**
     * Toggles the visibility of all NoteViews and their associated AnnotationViews.
     * 
     * @return the new visibility status of notes
     */
    public boolean toggleNotes() {
        boolean newVisibility = true;
        for (NoteView noteView : noteToViewMap.values()) {
            newVisibility = !noteView.isVisible();
            noteView.setVisible(newVisibility);
            for (RelationshipView<?, ? extends RamRectangleComponent> annotView : noteView.getAllRelationshipViews()) {
                annotView.setVisible(newVisibility);
            }
            noteView.updateRelationships();
        }
        return newVisibility;
    }
}
