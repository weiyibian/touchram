package ca.mcgill.sel.usecases.ui.views.handler.impl;

import org.eclipse.emf.ecore.util.EcoreUtil;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;

import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.impl.TextViewHandler;
import ca.mcgill.sel.ram.util.MetamodelRegex;
import ca.mcgill.sel.usecases.Actor;
import ca.mcgill.sel.usecases.UseCaseModel;
import ca.mcgill.sel.usecases.language.controller.ActorController;
import ca.mcgill.sel.usecases.language.controller.ControllerFactory;
import ca.mcgill.sel.usecases.ui.utils.UcdModelUtils;
import ca.mcgill.sel.usecases.util.Multiplicity;

public class ActorMultiplicityHandler extends TextViewHandler {

    @Override
    public void keyboardOpened(TextView textView) {
        // do nothing
    }

    @Override
    public boolean processTapEvent(TapEvent tapEvent) {
        if (tapEvent.isDoubleTap()) {
            TextView target = (TextView) tapEvent.getTarget();

            target.showKeyboard();
            return true;
        }

        return true;
    }
    
    @Override
    public boolean shouldDismissKeyboard(TextView textView) {
        String text = textView.getText();

        if (!text.matches(MetamodelRegex.REGEX_MULTIPLICITY)) {
            return false;
        }

        return setMultiplicity(text, (Actor) textView.getData());
    }
    
    @SuppressWarnings("static-method")
    private boolean setMultiplicity(String text, Actor actor) {
        Multiplicity multiplicity = UcdModelUtils.parseMultiplicity(text);
        ActorController controller = ControllerFactory.INSTANCE.getActorController();
        UseCaseModel ucd = (UseCaseModel) EcoreUtil.getRootContainer(actor);
        if (multiplicity != null) {
            controller.setMultiplicity(ucd, actor, multiplicity);
        }
        return true;
    }
}
