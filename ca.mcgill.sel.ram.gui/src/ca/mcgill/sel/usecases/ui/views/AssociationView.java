package ca.mcgill.sel.usecases.ui.views;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.provider.INotifyChangedListener;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.utils.GraphicalUpdater;
import ca.mcgill.sel.ram.ui.views.IRelationshipEndView;
import ca.mcgill.sel.ram.ui.views.RamEnd.Position;
import ca.mcgill.sel.ram.ui.views.RelationshipView;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.UseCaseModel;
import processing.core.PGraphics;

public class AssociationView extends RelationshipView<EObject, BaseView<?>> 
        implements INotifyChangedListener {
    
    /**
     * The possible types of associations.
     * @author rlanguay
     *
     */
    public enum AssociationType {
        /**
         * A primary actor - use case relationship.
         */
        PRIMARY,
        
        /**
         * A secondary actor - use case relationship.
         */
        SECONDARY,
        
        /**
         * One use case includes another in its steps.
         */
        INCLUDES,
        
        /**
         * One use case extends another in its steps.
         */
        EXTENDS
    }

    private GraphicalUpdater graphicalUpdater;
    private boolean shouldUpdate;
    private AssociationType type;
    
    public AssociationView(EObject from, BaseView<?> fromView, EObject to, BaseView<?> toView,
            AssociationType type) {
        super(from, fromView, to, toView);
        this.shouldUpdate = true;
        this.type = type;
        
        if (this.type == AssociationType.PRIMARY) {
            this.lineStyle = LineStyle.SOLID;
        } else if (this.type == AssociationType.SECONDARY) {
            this.lineStyle = LineStyle.DOTTED;
        }
        
        // We do not need to add a listener for the "to" side, this listener already listens on the whole class
        EMFEditUtil.addListenerFor(from, this);

        UseCaseModel ucd = EMFModelUtil.getRootContainerOfType(from, UcPackage.Literals.USE_CASE_MODEL);
        graphicalUpdater = RamApp.getApplication().getGraphicalUpdaterForModel(ucd);
        graphicalUpdater.addGUListener(from, this);
    }

    @Override
    protected void update() {
        // TODO Auto-generated method stub
        drawAllLines();
    }

    @Override
    public void notifyChanged(Notification notification) {
        // TODO Auto-generated method stub
        
    }
    
    /**
     * Draws the line between the center of the note and the center of the annotated element (not using the CDEnds).
     */
    @Override
    protected void drawAllLines() {
        //
        Vector3D fromCenter = fromEnd.getComponentView().getCenterPointRelativeToParent();
        Vector3D toCenter = toEnd.getComponentView().getCenterPointRelativeToParent();
        
        drawLine(fromCenter.getX(), fromCenter.getY(), null, toCenter.getX(), toCenter.getY());
        
        //force linked views to redraw, avoiding line appearing on top of them
        fromEnd.getComponentView().sendToFront();
        toEnd.getComponentView().sendToFront();
        
    }
    
    @Override
    public void drawComponent(PGraphics g) {
        if (this.shouldUpdate) {
            prepareUpdate();
            this.shouldUpdate = false;
        }
        super.drawComponent(g);
    }
    
    @Override
    protected void updateRelationshipEnds(RamRectangleComponent viewFrom, RamRectangleComponent viewTo,
            Position fromEndPosition, Position toEndPosition) {
     // now we set the positions.
        if (viewFrom instanceof LinkableView) {
            // process from end
            LinkableView<?> classViewFrom = (LinkableView<?>) viewFrom;
            LinkableView<?> classViewTo = (LinkableView<?>) viewTo;

            // if previous and current positions are different
            // also if fromEnd.getPosition() is null
            if (fromEndPosition != fromEnd.getPosition()) {
                classViewFrom.moveRelationshipEnd(fromEnd, fromEndPosition);
                classViewFrom.updateLayout();
            } else {
                // if position is the same reset the positions
                // for all ends on the same edge of this view.
                classViewFrom.setCorrectPosition(fromEnd);
            }

            // process to end
            if (toEndPosition != toEnd.getPosition()) {
                classViewTo.moveRelationshipEnd(toEnd, toEndPosition);
                classViewFrom.moveRelationshipEnd(fromEnd, fromEndPosition);
                classViewTo.updateLayout();
            } else {
                classViewTo.setCorrectPosition(toEnd);
            }
        } else if (viewFrom instanceof RamRectangleComponent) {
            // TODO: This is what everyone should do (see issue #109).
            IRelationshipEndView fromRelationshipView = null;
            IRelationshipEndView toRelationshipView = null;
            if (fromEnd.getComponentView() instanceof IRelationshipEndView) {
                fromRelationshipView = (IRelationshipEndView) fromEnd.getComponentView();
            }
            if (toEnd.getComponentView() instanceof IRelationshipEndView) {
                toRelationshipView = (IRelationshipEndView) toEnd.getComponentView();
            }
            // End has to be moved somewhere else.
            if (fromEndPosition != fromEnd.getPosition()) {
                Position oldPosition = fromEnd.getPosition();
                fromEnd.setPosition(fromEndPosition);
                if (fromRelationshipView != null) {
                    fromRelationshipView.moveRelationshipEnd(fromEnd, oldPosition, fromEndPosition);
                }
            } else {
                if (fromRelationshipView != null) {
                    fromRelationshipView.updateRelationshipEnd(fromEnd);
                }
            }

            if (toEndPosition != toEnd.getPosition()) {
                Position oldPosition = toEnd.getPosition();
                toEnd.setPosition(toEndPosition);
                if (toRelationshipView != null) {
                    toRelationshipView.moveRelationshipEnd(toEnd, oldPosition, toEndPosition);
                }
                // The from end might have to be fixed depending on what happened to the to end.
                if (fromRelationshipView != null) {
                    fromRelationshipView.updateRelationshipEnd(fromEnd);
                }
            } else {
                if (toRelationshipView != null) {
                    toRelationshipView.updateRelationshipEnd(toEnd);
                }
            }
        }
    }
    
    public AssociationType getType() {
        return this.type;
    }
    
    /**
     * Prepares the view for an upcoming update.
     * The update is skipped, if the ends are outside the screen.
     */
    private void prepareUpdate() {
        if (toEnd.getPosition() != Position.OFFSCREEN && fromEnd.getPosition() != Position.OFFSCREEN) {
            update();
        }
    }

}
