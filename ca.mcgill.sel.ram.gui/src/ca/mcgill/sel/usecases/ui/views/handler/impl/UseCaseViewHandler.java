package ca.mcgill.sel.usecases.ui.views.handler.impl;

import ca.mcgill.sel.usecases.UseCase;
import ca.mcgill.sel.usecases.language.controller.ControllerFactory;
import ca.mcgill.sel.usecases.ui.views.BaseView;
import ca.mcgill.sel.usecases.ui.views.handler.IUseCaseViewHandler;

public class UseCaseViewHandler extends BaseViewHandler implements IUseCaseViewHandler {

    @Override
    public void removeRepresented(BaseView<?> baseView) {
        UseCase useCase = (UseCase) baseView.getRepresented();
        ControllerFactory.INSTANCE.getUseCaseDiagramController().removeUseCase(useCase);
    }

}
