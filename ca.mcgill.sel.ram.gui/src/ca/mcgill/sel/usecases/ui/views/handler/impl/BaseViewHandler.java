package ca.mcgill.sel.usecases.ui.views.handler.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.mt4j.components.MTComponent;
import org.mt4j.components.TransformSpace;
import org.mt4j.input.gestureAction.InertiaDragAction;
import org.mt4j.input.inputProcessors.MTGestureEvent;
import org.mt4j.input.inputProcessors.componentProcessors.dragProcessor.DragEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;
import org.mt4j.util.MTColor;
import org.mt4j.util.animation.Animation;
import org.mt4j.util.animation.AnimationEvent;
import org.mt4j.util.animation.IAnimationListener;
import org.mt4j.util.animation.MultiPurposeInterpolator;
import org.mt4j.util.math.Vector3D;
import org.mt4j.util.math.Vertex;

import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.AlignmentLineComponent;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.menu.RamLinkedMenu;
import ca.mcgill.sel.ram.ui.events.DelayedDrag;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.handler.BaseHandler;
import ca.mcgill.sel.ram.ui.views.handler.ILinkedMenuListener;
import ca.mcgill.sel.usecases.LayoutElement;
import ca.mcgill.sel.usecases.NamedElement;
import ca.mcgill.sel.usecases.UcFactory;
import ca.mcgill.sel.usecases.UseCaseModel;
import ca.mcgill.sel.usecases.language.controller.ControllerFactory;
import ca.mcgill.sel.usecases.language.controller.UseCaseModelController;
import ca.mcgill.sel.usecases.ui.utils.UcdModelUtils;
import ca.mcgill.sel.usecases.ui.utils.UseCaseDiagramHandlerFactory;
import ca.mcgill.sel.usecases.ui.views.BaseView;
import ca.mcgill.sel.usecases.ui.views.UseCaseDiagramView;
import ca.mcgill.sel.usecases.ui.views.handler.IBaseViewHandler;


/**
 * The handler for the BaseView.
 *
 * @author Franz
 * @author yhattab
 */
public abstract class BaseViewHandler extends BaseHandler implements IBaseViewHandler, ILinkedMenuListener {

    /**
     * Name of the sub-menu with add actions.
     */
    protected static final String SUBMENU_ADD = "sub.add";

    /**
     * The minimum distance an object has to be dragged before it actually is dragged.
     */
    private static final float MIN_DRAG_DISTANCE = 11.5f;

    /**
     * The amount of time (in milliseconds) that the last events of the drag should be taken into account for inertia.
     */
    private static final int INERTIA_INTEGRATION_TIME = 125;

    /**
     * The factor the velocity of the inertia is scaled by. MT4js default is 0.85, but this causes a longer inertia
     * effect.
     */
    private static final float INERTIA_DAMPING = 0.75f;

    /**
     * The maximum velocity length of the velocity for inertia.
     */
    private static final float INERTIA_MAX_VELOCITY = 25;

    private static final String ACTION_REPRESENTED_REMOVE = "view.represented.remove";
    /**
     * The minimum distance to check for in-line BaseViews.
     */
    private static final int MIN_DISTANCE = 40;
    
    private static final float WIDTH = RamApp.getApplication().getWidth();
    private static final float HEIGHT = RamApp.getApplication().getHeight();
    private static final AlignmentLineComponent H_LINE = new AlignmentLineComponent(0, 0, 0, 0);
    private static final AlignmentLineComponent V_LINE = new AlignmentLineComponent(0, 0, 0, 0);
    private static Collection<BaseView<?>> baseViews = new ArrayList<BaseView<?>>();
    /**
     * Maps to store classifiers and non classifiers layout positions.
     */
    private static Map<NamedElement, LayoutElement> positionMap = new HashMap<NamedElement, LayoutElement>();
    private static Map<EObject, LayoutElement> nonClassifierPositionMap = new HashMap<EObject, LayoutElement>();
    private DelayedDrag dragAction = new DelayedDrag(MIN_DRAG_DISTANCE);
    
    
    @SuppressWarnings("unused")
    private InertiaDragAction inertiaAction = new InertiaDragAction(INERTIA_INTEGRATION_TIME, INERTIA_DAMPING,
            INERTIA_MAX_VELOCITY);
    
    @Override
    public boolean processDragEvent(DragEvent dragEvent) {
        BaseView<?> target = (BaseView<?>) dragEvent.getTarget();
        dragAction.processGestureEvent(dragEvent);
        
        float x = target.getCenterPointRelativeToParent().x;
        float y = target.getCenterPointRelativeToParent().y;
        float newX = x;
        float newY = y;
        float vOffset = target.getHeight() / 2;
        float hOffset = target.getWidth() / 2;
        UseCaseDiagramView useCaseDiagramView = target.getUseCaseDiagramView();
        UseCaseModel ucd = useCaseDiagramView.getUseCaseModel();
        
        if (dragEvent.getId() == MTGestureEvent.GESTURE_STARTED) {
            useCaseDiagramView.getContainerLayer().addChild(H_LINE);
            useCaseDiagramView.getContainerLayer().addChild(V_LINE);
            Vertex[] vertsX = { new Vertex(0, 0, 0), new Vertex(3 * WIDTH / target.getScale().x, 0, 0) };
            Vertex[] vertsY = { new Vertex(0, 0, 0), new Vertex(0, 3 * HEIGHT / target.getScale().x, 0) };
            V_LINE.setVertices(vertsY);
            H_LINE.setVertices(vertsX);
            V_LINE.sendToFront();
            H_LINE.sendToFront();
            buildBaseViews();
            baseViews.remove(target);
            target.sendToFront();
        } else if (dragEvent.getId() == MTGestureEvent.GESTURE_UPDATED) {
            if (target.getIsSelected() && dragAction.wasDragPerformed()) {
                target.translateGlobal(dragEvent.getTranslationVect().getInverted());
                UseCaseDiagramHandlerFactory.INSTANCE.getUseCaseDiagramViewHandler().dragAllSelected(
                        target.getUseCaseDiagramView(), dragEvent.getTranslationVect());
            }
            removeHighlightInLine();
            highlightInLine(target, x, y);
        } else if (dragEvent.getId() == MTGestureEvent.GESTURE_CANCELED) {
            useCaseDiagramView.getContainerLayer().removeChild(H_LINE);
            useCaseDiagramView.getContainerLayer().removeChild(V_LINE);
            removeHighlightInLine();
            baseViews.clear();
        } else if (dragEvent.getId() == MTGestureEvent.GESTURE_ENDED) {
            if (dragAction.wasDragPerformed()) {
                removeHighlightInLine();
                H_LINE.setVisible(false);
                V_LINE.setVisible(false);
                UseCaseModelController controller = ControllerFactory.INSTANCE.getUseCaseDiagramController();
                
                
                if (!isOverlapping(target)) {
                    newY = getYLine(y);
                    newX = getXLine(x);
                } else {
                    prohibitOverlaps(target, x, y);
                    controller.moveNamedElements(ucd, 
                            positionMap);
                    useCaseDiagramView.getContainerLayer().removeChild(H_LINE);
                    useCaseDiagramView.getContainerLayer().removeChild(V_LINE);
                    removeHighlightInLine();
                    positionMap.clear();
                    baseViews.clear();
                    return true;
                }
                slideTranslate(newX - target.getPosition(TransformSpace.RELATIVE_TO_PARENT).x - hOffset, 
                            newY - target.getPosition(TransformSpace.RELATIVE_TO_PARENT).y - vOffset, target);
                target.setPositionRelativeToParent(new Vector3D(newX - hOffset, newY - vOffset, 0));
                controller.moveNamedElements(ucd, positionMap);
                controller.moveNonNamedElements(ucd, nonClassifierPositionMap);
                baseViews.clear();
                positionMap.clear();
                nonClassifierPositionMap.clear();
                useCaseDiagramView.getContainerLayer().removeChild(H_LINE);
                useCaseDiagramView.getContainerLayer().removeChild(V_LINE);
            } else {
                removeHighlightInLine();
                baseViews.clear();
                positionMap.clear();
                nonClassifierPositionMap.clear();
                useCaseDiagramView.getContainerLayer().removeChild(H_LINE);
                useCaseDiagramView.getContainerLayer().removeChild(V_LINE);
            }
        }
        
        return true;
    }
    
    /**
     * Builds the baseViews array from the structuralView.
     */
    private static void buildBaseViews() {
        UseCaseDiagramView useCaseDiagramView = 
                UcdModelUtils.getUseCaseDiagramViewFromApp(RamApp.getApplication());
        if (useCaseDiagramView != null) {
            baseViews = useCaseDiagramView.getBaseViews();
        }
        
        for (MTComponent c : baseViews) {
            c.sendToFront();
        }
    }
    
    /**
     * Adds a BaseView to the positionMap to update the model.
     * @param baseView the BaseView to add to the map
     * @param deltaX the amount that the BaseView has moved on the x-axis
     * @param deltaY the amount that the BaseView has moved on the y-axis
     */
    private static void addToPositionMap(BaseView<?> baseView, float deltaX, float deltaY) {
        Vector3D position = baseView.getPosition(TransformSpace.RELATIVE_TO_PARENT);
        LayoutElement layoutElement = UcFactory.eINSTANCE.createLayoutElement();
        layoutElement.setX(position.getX() + deltaX);
        layoutElement.setY(position.getY() + deltaY);
        if (baseView.getRepresented() instanceof NamedElement) {
            positionMap.put((NamedElement) baseView.getRepresented(), layoutElement);
        } else {
            nonClassifierPositionMap.put(baseView.getRepresented(), layoutElement);
        }
        
    }
    
    /**
     * Moves necessary BaseViews in the diagram to prevent overlapping.
     * @param target the BaseView that is overlapping others
     * @param x the x position that results in the overlap
     * @param y the y position that results in the overlap
     */
    private static void prohibitOverlaps(BaseView<?> target, float x, float y) {
        ArrayList<BaseView<?>> overlaps = getOverlapping(target);
        float x1 = target.getCenterPointRelativeToParent().x;
        float y1 = target.getCenterPointRelativeToParent().y;
        float width1 = target.getWidth() / 2;
        float height1 = target.getHeight() / 2;
        BaseView<?> class2 = overlaps.get(0);
        float x2 = class2.getCenterPointRelativeToParent().x;
        float y2 = class2.getCenterPointRelativeToParent().y;
        float width2 = class2.getWidth() / 2;
        float height2 = class2.getHeight() / 2;
        float newX = x2;
        float newY = y2;
        float newX1 = x2;
        float newY1 = y2;
        if (isInLineY(target, overlaps.get(0))) {
            if (overlaps.size() == 2 && isInLineY(target, overlaps.get(0)) && isInLineY(target, overlaps.get(1))) {
                BaseView<?> topView;
                BaseView<?> bottomView;
                if (overlaps.get(0).getPosition(TransformSpace.RELATIVE_TO_PARENT).y 
                       < overlaps.get(1).getPosition(TransformSpace.RELATIVE_TO_PARENT).y) {
                    topView = overlaps.get(0);
                    bottomView = overlaps.get(1);
                } else {
                    topView = overlaps.get(1);
                    bottomView = overlaps.get(0);
                }
                overlapPush(0.0f, target.getPosition(TransformSpace.RELATIVE_TO_PARENT).y + (target.getHeight())
                         - bottomView.getPosition(TransformSpace.RELATIVE_TO_PARENT).y 
                         + MIN_DISTANCE, bottomView);
                overlapPush(0.0f, target.getPosition(TransformSpace.RELATIVE_TO_PARENT).y - topView.getHeight()
                        - topView.getPosition(TransformSpace.RELATIVE_TO_PARENT).y - MIN_DISTANCE, topView);
                newY = y1;
                newX = getXLine(x);
            } else {
                newX = getXLine(x);
                if (y1 > y2) {
                    newY = y2 + (height1 + height2) + MIN_DISTANCE;
                } else {
                    newY = y2 - (height1 + height2) - MIN_DISTANCE;
                }
            }
        } else if (isInLineX(target, overlaps.get(0))) { 
            if (overlaps.size() == 2 && isInLineX(target, overlaps.get(0)) && isInLineX(target, overlaps.get(1))) {
                BaseView<?> rightView;
                BaseView<?> leftView;
                if (overlaps.get(0).getPosition(TransformSpace.RELATIVE_TO_PARENT).x 
                        > overlaps.get(1).getPosition(TransformSpace.RELATIVE_TO_PARENT).x) {
                    rightView = overlaps.get(0);
                    leftView = overlaps.get(1);
                } else {
                    rightView = overlaps.get(1);
                    leftView = overlaps.get(0);
                }
                overlapPush(target.getPosition(TransformSpace.RELATIVE_TO_PARENT).x + (target.getWidth())
                         - rightView.getPosition(TransformSpace.RELATIVE_TO_PARENT).x 
                         + MIN_DISTANCE,
                        0.0f, rightView);
                overlapPush(target.getPosition(TransformSpace.RELATIVE_TO_PARENT).x - leftView.getWidth()
                        - leftView.getPosition(TransformSpace.RELATIVE_TO_PARENT).x - MIN_DISTANCE, 
                       0.0f, leftView);
                
                newY = getYLine(y);
                newX = x1;
            } else {
                newY = getYLine(y);
                if (x1 > x2) {
                    newX = x2 + (width1 + width2) + MIN_DISTANCE;
                } else {
                    newX = x2 - (width1 + width2) - MIN_DISTANCE;
                }
            }
        } else {
            while (isOverlapping(newX - width1, newX + width1, 
                    newY - height1, newY + height1)) {
                if (y1 > y2) {
                    newY = newY + (height1 + height2) + MIN_DISTANCE;
                } else {
                    newY = newY - (height1 + height2) - MIN_DISTANCE;
                }
                newX = (((newY - y1) * (x2 - x1)) / (y2 - y1)) + x1;
            }
            while (isOverlapping(newX1 - width1, newX1 + width1, 
                    newY1 - height1, newY1 + height1)) {
                if (x1 > x2) {
                    newX1 = newX1 + (width1 + width2) + MIN_DISTANCE;
                } else {
                    newX1 = newX1 - (width1 + width2) - MIN_DISTANCE;
                }
                newY1 = (((newX1 - x1) * (y2 - y1)) / (x2 - x1)) + y1;
            }
            if (Math.sqrt(Math.pow(newY1 - y1, 2) + Math.pow(newX1 - x1, 2)) 
                < Math.sqrt(Math.pow(newY - y1, 2) + Math.pow(newX - x1, 2))) {
                newX = newX1;
                newY = newY1;
            } 
        }
        slideTranslate(newX - target.getPosition(TransformSpace.RELATIVE_TO_PARENT).x - (target.getWidth() / 2), 
                newY - target.getPosition(TransformSpace.RELATIVE_TO_PARENT).y 
                - (target.getHeight() / 2), target);
        return;
    }
    
    /**
     * Pushes all in line BaseViews if there is an overlap from the first movement.
     * @param deltaX x displacement to move the BaseView
     * @param deltaY y displacement to move the BaseView
     * @param baseView BaseView to move
     */
    private static void overlapPush(float deltaX, float deltaY, final BaseView<?> baseView) {
        slideTranslate(deltaX, deltaY, baseView);
        for (BaseView<?> i : getInLineComponents(baseView.getCenterPointRelativeToParent().x, 
                baseView.getCenterPointRelativeToParent().y)) {
            if (isOverlapping(i, baseView.getPosition(TransformSpace.RELATIVE_TO_PARENT).x + deltaX,
                    baseView.getPosition(TransformSpace.RELATIVE_TO_PARENT).x + baseView.getWidth() + deltaX,
                    baseView.getPosition(TransformSpace.RELATIVE_TO_PARENT).y + deltaY,
                    baseView.getPosition(TransformSpace.RELATIVE_TO_PARENT).y + baseView.getHeight() + deltaY) 
                    && i != baseView) {
                overlapPush(deltaX, deltaY, i);
            }
        }
    }
    
    /**
     * Translates the BaseView by given displacement in a slide animation and updates model.
     * @param deltaX displacement of BaseView on x-axis
     * @param deltaY displacement of BaseView on y_axis
     * @param baseView the BaseView to translate
     */
    private static void slideTranslate(float deltaX, float deltaY, final BaseView<?> baseView) {
        if (baseView.getIsSelected()) {
            slideAllSelectedClasses(deltaX, deltaY, baseView.getUseCaseDiagramView());
        } else {
            snapTranslate(deltaX, deltaY, baseView);
        }
    }
    
    /**
     * Animates the movement of a BaseView from its current position, to the new one defined by deltaX and deltaY.
     * @param deltaX x displacement to move the BaseView
     * @param deltaY y displacement to move the BaseView
     * @param baseView BaseView to move
     */
    private static void snapTranslate(float deltaX, float deltaY, final BaseView<?> baseView) {
        final float startX = baseView.getPosition(TransformSpace.RELATIVE_TO_PARENT).x;
        final float endX = startX + deltaX;
        final float startY = baseView.getPosition(TransformSpace.RELATIVE_TO_PARENT).y;
        final float endY = startY + deltaY;
        addToPositionMap(baseView, deltaX, deltaY);
        MultiPurposeInterpolator slideInterpolator = new MultiPurposeInterpolator(0, 1.0f, 300, 0.0f, 1.0f, 1);
        final Animation forceSlideAnimation = new Animation("slideTranslate anim", slideInterpolator, baseView, 0);
        forceSlideAnimation.addAnimationListener(new IAnimationListener() {
            private float deltaX = endX - startX;
            private float deltaXTotal;
            private float deltaY = endY - startY;
            private float deltaYTotal;
            @Override
            public void processAnimationEvent(AnimationEvent ae) {
                switch (ae.getId()) {
                    case AnimationEvent.ANIMATION_STARTED:
                    case AnimationEvent.ANIMATION_UPDATED:
                        deltaX = deltaX / 2;
                        deltaXTotal = deltaXTotal + deltaX;
                        deltaY = deltaY / 2;
                        deltaYTotal = deltaYTotal + deltaY;
                        float newY = startY + deltaYTotal;
                        float newX = startX + deltaXTotal;
                        baseView.setPositionRelativeToParent(new Vector3D(newX, newY, 0));
                    case AnimationEvent.ANIMATION_ENDED:
                }
            }
        });
        forceSlideAnimation.start();
    }
    
    /**
     * Slides all selected BaseViews by displacement values.
     * @param deltaX x displacement to move the BaseView
     * @param deltaY y displacement to move the BaseView
     * @param v UseCaseDiagramView containing BaseViews
     */
    private static void slideAllSelectedClasses(float deltaX, float deltaY, UseCaseDiagramView v) {
        for (BaseView<?> baseView : v.getSelectedElements()) {
            snapTranslate(deltaX, deltaY, baseView);
        }
    }

    /**
     * Checks which rectangles are overlapping.
     * 
     * @param rectangle the rectangle to check the overlap
     * @return Returns the rectangle that is being overlapped
     */
    private static ArrayList<BaseView<?>> getOverlapping(BaseView<?> rectangle) {
        ArrayList<BaseView<?>> overlap = new ArrayList<BaseView<?>>();
        for (BaseView<?> i : baseViews) {
            if (isOverlapping(rectangle, i)) {
                overlap.add(i);
            }
        }
        return overlap;
    }
    
    /**
     * Checks if two rectangles are overlapping.
     * @param class1 First rectangle
     * @param class2 Second rectangle
     * @return true if the rectangles overlap
     */
    private static boolean isOverlapping(BaseView<?> class1, BaseView<?> class2) {
        float r1x1 = class1.getPosition(TransformSpace.RELATIVE_TO_PARENT).x;
        float r2x1 = class2.getPosition(TransformSpace.RELATIVE_TO_PARENT).x;
        float r1y1 = class1.getPosition(TransformSpace.RELATIVE_TO_PARENT).y;
        float r2y1 = class2.getPosition(TransformSpace.RELATIVE_TO_PARENT).y;
        float r1x2 = r1x1 + class1.getWidth();
        float r2x2 = r2x1 + class2.getWidth();
        float r1y2 = r1y1 + class1.getHeight();
        float r2y2 = r2y1 + class2.getHeight();
        return r1x2 > r2x1 && r1x1 < r2x2 && r1y2 > r2y1 && r1y1 < r2y2;
    }
    
    /**
     * Checks if a BaseView overlaps a rectangle of the given dimensions.
     * @param baseView the BaseView to check if it is overlapping
     * @param r2x1 the left bound of the rectangle
     * @param r2x2 the right bound of the rectangle
     * @param r2y1 the upper bound of the rectangle
     * @param r2y2 the lower bound of the rectangle
     * @return returns true if the BaseView is overlapping
     */
    private static boolean isOverlapping(BaseView<?> baseView, float r2x1, float r2x2, float r2y1, float r2y2) {
        float r1x1 = baseView.getPosition(TransformSpace.RELATIVE_TO_PARENT).x;
        float r1x2 = baseView.getPosition(TransformSpace.RELATIVE_TO_PARENT).x + baseView.getWidth();
        float r1y1 = baseView.getPosition(TransformSpace.RELATIVE_TO_PARENT).y; 
        float r1y2 = baseView.getPosition(TransformSpace.RELATIVE_TO_PARENT).y + baseView.getHeight();
        return r1x2 > r2x1 && r1x1 < r2x2 && r1y2 > r2y1 && r1y1 < r2y2;
    }
    
    /**
     * Checks if the current rectangle overlaps any other on the screen.
     * @param baseView the baseView to check for overlap
     * @return true if the baseView overlaps
     */
    private static boolean isOverlapping(BaseView<?> baseView) {
        for (BaseView<?> i : baseViews) {
            if (isOverlapping(i, baseView)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Checks if a given dimension of rectangle overlaps any BaseView in the diagram.
     * @param x1 the left bound of the rectangle
     * @param x2 the right bound of the rectangle
     * @param y1 the upper bound of the rectangle
     * @param y2 the lower bound of the rectangle
     * @return returns true if the dimensions overlap any BaseViews in the diagram
     */
    private static boolean isOverlapping(float x1, float x2, float y1, float y2) {
        for (BaseView<?> i : baseViews) {
            if (isOverlapping(i, x1, x2, y1, y2)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Draws alignment lines and highlights all BaseViews in Line with given coordinates.
     * @param target the BaseView that is in line with other views.
     * @param x the x coordinate to align to
     * @param y the y coordinate to align to
     */
    private static void highlightInLine(BaseView<?> target, float x, float y) {
        ArrayList<BaseView<?>> overlaps = getOverlapping(target);
        float newX = x;
        float newY = y;
        if (isInLineY(y)) {
            newY = getYLine(y);
            H_LINE.setPositionRelativeToParent(new Vector3D(x, newY, 0));
            H_LINE.setVisible(true);
        } else {
            H_LINE.setVisible(false);
        }
        if (isInLineX(x)) {
            newX = getXLine(x);
            V_LINE.setPositionRelativeToParent(new Vector3D(newX, y, 0));
            V_LINE.setVisible(true);
        } else {
            V_LINE.setVisible(false);
        }
        for (BaseView<?> i : getInLineComponents(x, y)) {
            if (!i.getIsSelected() && (Math.abs(i.getCenterPointRelativeToParent().x - newX) <= MIN_DISTANCE 
                    || Math.abs(i.getCenterPointRelativeToParent().y - newY) <= MIN_DISTANCE)) {
                if (overlaps.size() == 2 && ((isInLineY(target, overlaps.get(0)) && isInLineY(target, overlaps.get(1))) 
                        || (isInLineX(target, overlaps.get(0)) && isInLineX(target, overlaps.get(1))))) {
                    overlaps.get(0).setStrokeColor(MTColor.RED);
                    overlaps.get(1).setStrokeColor(MTColor.RED);
//                } 
//                else if (overlaps.size() == 2 && isInLineX(target, overlaps.get(0)) 
//                        && isInLineX(target, overlaps.get(1))) {
//                    overlaps.get(0).setStrokeColor(MTColor.RED);
//                    overlaps.get(1).setStrokeColor(MTColor.RED);
//                    
                } else {
                    i.setStrokeColor(MTColor.LIME);
                }
            }
        }
    }
    
    /**
     * Removes the highlight of all inLine views.
     */
    private static void removeHighlightInLine() {
        for (BaseView<?> i : baseViews) {
            if (Objects.equals(i.getStrokeColor().toString(), MTColor.LIME.toString()) 
                    || Objects.equals(i.getStrokeColor().toString(), MTColor.RED.toString())) {
                i.setStrokeColor(MTColor.BLACK);
            }
        }
    }
    

    /**
     * Returns a list of views in line with the provided coordinates.
     * @param x X coordinates to check for in Line components
     * @param y Y coordinates to check for in Line components
     * @return Views in Line with coordinates
     */
    private static ArrayList<BaseView<?>> getInLineComponents(float x, float y) {
        ArrayList<BaseView<?>> list = new ArrayList<BaseView<?>>();
        for (BaseView<?> i : baseViews) {
            if (!baseViews.isEmpty()) {
                if (Math.abs(i.getCenterPointRelativeToParent().x - x) < MIN_DISTANCE && !i.getIsSelected()
                        && !list.contains(i)) {
                    list.add(i);
                }
                if (Math.abs(i.getCenterPointRelativeToParent().y - y) < MIN_DISTANCE && !i.getIsSelected() 
                        && !list.contains(i)) {
                    list.add(i);
                }
            }
        }
        return list;
    }
    
    /**
     * Returns the vertical coordinate of the horizontal alignment line.
     * @param x x coordinate to check for line
     * @return coordinate of alignment line
     */
    private static float getXLine(float x) {
        float line = x;
        for (BaseView<?> i : baseViews) {
            if (!baseViews.isEmpty()) {
                if (Math.abs(i.getCenterPointRelativeToParent().x - x) < MIN_DISTANCE && !i.getIsSelected()) {
                    line = i.getCenterPointRelativeToParent().x;
                }
            }
        }
        return line;
    }
    
    /**
     * Returns the horizontal coordinate of the vertical alignment line.
     * @param y y coordinate to check for line
     * @return coordinate of alignment line
     */
    private static float getYLine(float y) {
        for (BaseView<?> i : baseViews) {
            if (!baseViews.isEmpty()) {
                if (Math.abs(i.getCenterPointRelativeToParent().y - y) < MIN_DISTANCE && !i.getIsSelected()) {
                    return i.getCenterPointRelativeToParent().y;
                }
            }
        }
        return y;
    }
    
    /**
     * Checks if there are any other BaseViews in line with x.
     * @param x horizontal coordinate to check for views
     * @return true if there are any views in line with x
     */
    private static boolean isInLineX(float x) {
        for (BaseView<?> i : baseViews) {
            if (!baseViews.isEmpty()) {
                if (Math.abs(i.getCenterPointRelativeToParent().x - x) < MIN_DISTANCE && !i.getIsSelected()) {
                    return true;
                }
            }
        }
        return false;
    }
    
    /**
     * Checks if there are any other BaseViews in line with y.
     * @param y vertical coordinate to check for views
     * @return true if there are any views in line with y
     */
    private static boolean isInLineY(float y) {
        for (BaseView<?> i : baseViews) {
            if (!baseViews.isEmpty()) {
                if (Math.abs(i.getCenterPointRelativeToParent().y - y) < MIN_DISTANCE && !i.getIsSelected()) {
                    return true;
                }
            }
        }
        return false;
    }
    
    /**
     * Checks if two BaseViews are in line with each other along the x-axis.
     * @param class1 the first BaseView to check
     * @param class2 the second BaseView to check
     * @return true if the two BaseViews are in-line
     */
    private static boolean isInLineX(BaseView<?> class1, BaseView<?> class2) {
        return Math.abs(class1.getCenterPointRelativeToParent().y - class2.getCenterPointRelativeToParent().y) 
                < MIN_DISTANCE && !class2.getIsSelected() && !class1.getIsSelected();
    }
    
    /**
     * Checks if two BaseViews are in line with each other along the y-axis.
     * @param class1 the first BaseView to check
     * @param class2 the second BaseView to check
     * @return true if the two BaseViews are in-line
     */
    private static boolean isInLineY(BaseView<?> class1, BaseView<?> class2) {
        return Math.abs(class1.getCenterPointRelativeToParent().x - class2.getCenterPointRelativeToParent().x) 
                < MIN_DISTANCE && !class2.getIsSelected() && !class1.getIsSelected();
    }
    
    @Override
    public boolean processTapAndHoldEvent(TapAndHoldEvent tapAndHoldEvent) {
        if (tapAndHoldEvent.isHoldComplete()) {
            BaseView<?> target = (BaseView<?>) tapAndHoldEvent.getTarget();
            return target.getUseCaseDiagramView().getHandler()
                    .handleTapAndHold(target.getUseCaseDiagramView(), target);
        }
        return false;
    }

    @Override
    public boolean processTapEvent(TapEvent tapEvent) {
        // Always flip the selection of the view.
        if (tapEvent.isTapped() || tapEvent.isDoubleTap()) {
            BaseView<?> target = (BaseView<?>) tapEvent.getTarget();
            target.setSelect(!target.getIsSelected());

            // Pass it to the structural view handler to handle more advanced gestures.
            if (tapEvent.isDoubleTap()) {
                UseCaseDiagramView structuralDiagramView = target.getUseCaseDiagramView();
                return structuralDiagramView.getHandler().handleDoubleTap(structuralDiagramView, target);
            }
        }

        return false;
    }

    @Override
    public abstract void removeRepresented(BaseView<?> baseView);

    @Override
    public void actionPerformed(ActionEvent event) {
        String actionCommand = event.getActionCommand();
        RamRectangleComponent pressedButton = (RamRectangleComponent) event.getTarget();
        RamLinkedMenu linkedMenu = (RamLinkedMenu) pressedButton.getParentOfType(RamLinkedMenu.class);
        if (linkedMenu != null) {

            BaseView<?> represented = (BaseView<?>) linkedMenu.getLinkedView();

            if (ACTION_REPRESENTED_REMOVE.equals(actionCommand)) {
                removeRepresented(represented);
            }
        }
    }

    @Override
    public EObject getEobject(RamRectangleComponent rectangle) {
        return ((BaseView<?>) rectangle).getRepresented();
    }

    @Override
    public void initMenu(RamLinkedMenu menu) {
        menu.addSubMenu(1, SUBMENU_ADD);
        menu.addAction(Strings.MENU_DELETE, Icons.ICON_MENU_TRASH, ACTION_REPRESENTED_REMOVE, this, true);
    }

    @Override
    public List<EObject> getEObjectToListenForUpdateMenu(RamRectangleComponent rectangle) {
        BaseView<?> baseView = (BaseView<?>) rectangle;
        ArrayList<EObject> ret = new ArrayList<EObject>();
        ret.add(baseView.getRepresented());
        return ret;
    }

    @Override
    public void updateMenu(RamLinkedMenu menu, Notification notification) {
    }

    @Override
    public RamRectangleComponent getVisualLinkedComponent(RamRectangleComponent link) {
        return link;
    }

}
