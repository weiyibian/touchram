package ca.mcgill.sel.usecases.ui.views.handler.impl;

import java.util.ArrayList;

import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;

import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamImageComponent;
import ca.mcgill.sel.ram.ui.components.RamSelectorComponent;
import ca.mcgill.sel.ram.ui.components.listeners.AbstractDefaultRamSelectorListener;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.views.OptionSelectorView;
import ca.mcgill.sel.ram.ui.views.OptionSelectorView.Iconified;
import ca.mcgill.sel.ram.ui.views.RamEnd;
import ca.mcgill.sel.ram.ui.views.handler.IRelationshipViewHandler;
import ca.mcgill.sel.usecases.Actor;
import ca.mcgill.sel.usecases.UseCase;
import ca.mcgill.sel.usecases.language.controller.ControllerFactory;
import ca.mcgill.sel.usecases.language.controller.UseCaseController;
import ca.mcgill.sel.usecases.ui.views.AssociationView;
import ca.mcgill.sel.usecases.ui.views.BaseView;

public class AssociationViewHandler extends BaseViewHandler implements IRelationshipViewHandler {

    /**
     * The options to display for an association end.
     */
    private enum AssociationOptions implements Iconified {
        PRIMARY(new RamImageComponent(Icons.ICON_LINE, Colors.ICON_ADD_DEFAULT_COLOR)),
        SECONDARY(new RamImageComponent(Icons.ICON_LINE, Colors.ICON_ADD_DEFAULT_COLOR)),
        DELETE(new RamImageComponent(Icons.ICON_DELETE, Colors.ICON_DELETE_COLOR));

        private RamImageComponent icon;

        /**
         * Creates a new option literal with the given icon.
         *
         * @param icon the icon to use for this option
         */
        AssociationOptions(RamImageComponent icon) {
            this.icon = icon;
        }

        @Override
        public RamImageComponent getIcon() {
            return icon;
        }

    }
    
    @Override
    public void removeRepresented(BaseView<?> baseView) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public boolean processDoubleTap(TapEvent tapEvent, RamEnd<?, ?> end) {
        AssociationView view = (AssociationView) end.getRelationshipView();
        UseCase targetUseCase;
        Actor actor;
        
        ArrayList<AssociationOptions> availableOptions = 
                new ArrayList<AssociationOptions>();
        switch (view.getType()) {
            case PRIMARY:
                availableOptions.add(AssociationOptions.SECONDARY);
                targetUseCase = (UseCase) view.getFromEnd().getModel();
                actor = (Actor) view.getToEnd().getModel();
                break;
            case SECONDARY:
                availableOptions.add(AssociationOptions.PRIMARY);
                targetUseCase = (UseCase) view.getFromEnd().getModel();
                actor = (Actor) view.getToEnd().getModel();
                break;
            default:
                targetUseCase = null;
                actor = null;
                break;
        }
        
        availableOptions.add(AssociationOptions.DELETE);
        
        OptionSelectorView<AssociationOptions> selector =
                new OptionSelectorView<AssociationViewHandler.AssociationOptions>(availableOptions);

        RamApp.getActiveScene().addComponent(selector, tapEvent.getLocationOnScreen());

        selector.registerListener(new AbstractDefaultRamSelectorListener<AssociationViewHandler.AssociationOptions>() {
            @Override
            public void elementSelected(RamSelectorComponent<AssociationOptions> selector, AssociationOptions element) {
                UseCaseController useCaseController = ControllerFactory.INSTANCE.getUseCaseController();

                switch (element) { 
                    case PRIMARY:
                        useCaseController.setPrimaryActor(actor, targetUseCase);
                        break;
                    case SECONDARY:
                        useCaseController.addSecondaryActor(actor, targetUseCase);
                        break;
                    case DELETE:
                        switch (view.getType()) {
                            case PRIMARY:
                                useCaseController.clearPrimaryActor(targetUseCase);
                                break;
                            case SECONDARY:
                                useCaseController.removeSecondaryActor(actor, targetUseCase);
                                break;
                        }
                }
            }

        });
        
        return true;
    }

    @Override
    public boolean processTapAndHold(TapAndHoldEvent tapAndHoldEvent, RamEnd<?, ?> end) {
        // TODO Auto-generated method stub
        return false;
    }

}
