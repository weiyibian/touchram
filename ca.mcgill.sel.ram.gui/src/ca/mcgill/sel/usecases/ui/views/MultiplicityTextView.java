package ca.mcgill.sel.usecases.ui.views;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.usecases.Actor;
import ca.mcgill.sel.usecases.ui.utils.UcdModelUtils;

public class MultiplicityTextView extends TextView {

    public MultiplicityTextView(EObject data) {
        super(data, null, false);
    }
    
    @Override
    protected String getModelText() {
        EObject data = getData();
        if (data instanceof Actor) {
            return UcdModelUtils.getActorMultiplicity((Actor) data);
        } else {
            return super.getModelText();
        }
    }

    @Override
    public void notifyChanged(Notification notification) {
        if (notification.getNotifier() == getData()) {
            updateText();
        }
    }

    @Override
    public void showKeyboard() {
        super.showKeyboard(this);
        getKeyboard().setSymbolsState(true);
    }

}
