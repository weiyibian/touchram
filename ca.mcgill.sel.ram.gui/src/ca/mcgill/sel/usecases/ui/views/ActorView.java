package ca.mcgill.sel.usecases.ui.views;

import org.eclipse.emf.ecore.EObject;
import org.mt4j.components.visibleComponents.shapes.MTEllipse;
import org.mt4j.util.MTColor;
import org.mt4j.util.math.Vector3D;
import org.mt4j.util.math.Vertex;

import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamLineComponent;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent.Alignment;
import ca.mcgill.sel.ram.ui.layouts.Layout;
import ca.mcgill.sel.ram.ui.layouts.VerticalLayout;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Fonts;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.ITextViewHandler;
import ca.mcgill.sel.usecases.Actor;
import ca.mcgill.sel.usecases.LayoutElement;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.ui.utils.UseCaseDiagramHandlerFactory;
import ca.mcgill.sel.usecases.ui.views.handler.IActorViewHandler;

public class ActorView extends LinkableView<IActorViewHandler> {

    private static final float MINIMUM_WIDTH = 75f;
    private static final float ACTOR_BODY_LENGTH = 50f;
    private static final float ACTOR_ARM_LENGTH = 50f;
    private static final int ACTOR_HEAD_RADIUS = 15;
    private static final float ACTOR_LEG_LENGTH = 25f;
    private static final float ACTOR_STROKE_WEIGHT = 3f;
    
    private RamRectangleComponent actorContainer;
    private MTEllipse actorHead;
    private RamLineComponent actorBody;
    private RamLineComponent actorArms;
    private RamLineComponent actorLeftLeg;
    private RamLineComponent actorRightLeg;
    private MultiplicityTextView multiplicityView;
    
    protected ActorView(UseCaseDiagramView useCaseDiagramView, EObject represented, LayoutElement layoutElement) {
        super(useCaseDiagramView, represented, layoutElement);
        
        setMinimumWidth(MINIMUM_WIDTH);
        setNoStroke(true);
        setNoFill(true);
        setLayout(new VerticalLayout(Layout.HorizontalAlignment.CENTER));
        
        addMultiplicityField();
        buildNameField();
        buildActor(layoutElement);
        addChild(nameField);
        nameField.setBufferSize(Cardinal.SOUTH, 5);
        
        // translate the class based on the meta-model
        if (layoutElement != null) {
            setLayoutElement(layoutElement);
        }
    }
    
    public Actor getActor() {
        return (Actor) this.represented;
    }
    
    @Override
    public void destroy() {       
        destroyRelationships();
        this.destroyAllChildren();
        super.destroy();
    }
    
    @Override
    public void setStrokeColor(MTColor color) {
        if (actorContainer != null) {
            actorHead.setStrokeColor(color);
            actorBody.setStrokeColor(color);
            actorArms.setStrokeColor(color);
            actorLeftLeg.setStrokeColor(color);
            actorRightLeg.setStrokeColor(color);
        }        
    }
    
    @Override
    public MTColor getStrokeColor() {
        if (actorBody != null) {
            return actorBody.getStrokeColor();
        } else {
            return super.getStrokeColor();
        }
    }
    
    private void buildNameField() {
        // Add the name field to base view
        nameField = new TextView(getActor(), UcPackage.Literals.NAMED_ELEMENT__NAME);
        nameField.setFont(Fonts.DEFAULT_FONT_MEDIUM);
        nameField.setAlignment(Alignment.CENTER_ALIGN);
        nameField.setPlaceholderText(Strings.PH_ENTER_ACTOR_NAME);
        nameField.setNewlineDisabled(true);
        ((TextView) nameField).setUniqueName(true);        
        ((TextView) nameField).setHandler(UseCaseDiagramHandlerFactory.INSTANCE.getTextViewHandler());
    }
    
    private void addMultiplicityField() {
        // Add the multiplicity field to base view
        multiplicityView = new MultiplicityTextView(getActor());
        multiplicityView.setFont(Fonts.DEFAULT_FONT_MEDIUM);
        multiplicityView.setAlignment(Alignment.CENTER_ALIGN);
        multiplicityView.setNewlineDisabled(true);
        multiplicityView.setAlignment(Alignment.RIGHT_ALIGN);
        addChild(multiplicityView);
        
        ITextViewHandler handler = UseCaseDiagramHandlerFactory.INSTANCE.getActorMultiplicityViewHandler();
        multiplicityView.registerTapProcessor(handler);
    }
    
    private void buildActor(LayoutElement layoutElement) {       
        actorContainer = new RamRectangleComponent();
        actorContainer.setMinimumHeight(2 * ACTOR_BODY_LENGTH);
        actorContainer.setNoFill(true);
        
        // Head
        // We use the name field to determine the width of the view because it is always going to be the widest
        // child control in this view
        actorHead = new MTEllipse(
                RamApp.getApplication(), 
                new Vector3D(nameField.getWidth() / 2, ACTOR_HEAD_RADIUS + ACTOR_STROKE_WEIGHT), 
                ACTOR_HEAD_RADIUS, 
                ACTOR_HEAD_RADIUS);
        actorHead.setStrokeColor(Colors.DEFAULT_ELEMENT_STROKE_COLOR);
        actorHead.setStrokeWeight(ACTOR_STROKE_WEIGHT);
        actorContainer.addChild(actorHead);        
        
        // Body
        Vertex bodyTop = new Vertex(
                actorHead.getCenterPointGlobal().getX(), actorHead.getCenterPointGlobal().getY() + ACTOR_HEAD_RADIUS);
        Vertex bodyBottom = new Vertex(bodyTop.getX(), bodyTop.getY() + ACTOR_BODY_LENGTH);
        actorBody = new RamLineComponent(bodyTop, bodyBottom);
        actorBody.setStrokeColor(Colors.DEFAULT_ELEMENT_STROKE_COLOR);
        actorContainer.addChild(actorBody);
        
       
        // Arms
        Vertex centerPoint = new Vertex(
                bodyTop.getX(), bodyTop.getY() + ((bodyBottom.getY() - bodyTop.getY()) / 2));
        Vertex armLeft = new Vertex(centerPoint.getX() - ACTOR_ARM_LENGTH / 2, centerPoint.getY());
        Vertex armRight = new Vertex(centerPoint.getX() + ACTOR_ARM_LENGTH / 2, centerPoint.getY());
        actorArms = new RamLineComponent(armLeft, armRight);
        actorContainer.addChild(actorArms);
        
        
        // Legs
        float leftLegBottomX = bodyBottom.getX() - ((float) Math.sqrt(ACTOR_LEG_LENGTH * ACTOR_LEG_LENGTH / 2));
        float leftLegBottomY = bodyBottom.getY() + ((float) Math.sqrt(ACTOR_LEG_LENGTH * ACTOR_LEG_LENGTH / 2));
        float rightLegBottomX = bodyBottom.getX() + ((float) Math.sqrt(ACTOR_LEG_LENGTH * ACTOR_LEG_LENGTH / 2));
        float rightLegBottomY = bodyBottom.getY() + ((float) Math.sqrt(ACTOR_LEG_LENGTH * ACTOR_LEG_LENGTH / 2));
        
        actorLeftLeg = new RamLineComponent(bodyBottom, new Vertex(leftLegBottomX, leftLegBottomY));
        actorContainer.addChild(actorLeftLeg);
        actorRightLeg = new RamLineComponent(bodyBottom, new Vertex(rightLegBottomX, rightLegBottomY));
        actorContainer.addChild(actorRightLeg);
                
        addChild(actorContainer);
    }
}
