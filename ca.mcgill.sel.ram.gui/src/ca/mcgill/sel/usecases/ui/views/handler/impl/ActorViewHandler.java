package ca.mcgill.sel.usecases.ui.views.handler.impl;

import ca.mcgill.sel.usecases.Actor;
import ca.mcgill.sel.usecases.language.controller.ControllerFactory;
import ca.mcgill.sel.usecases.ui.views.BaseView;
import ca.mcgill.sel.usecases.ui.views.handler.IActorViewHandler;

public class ActorViewHandler extends BaseViewHandler implements IActorViewHandler {

    @Override
    public void removeRepresented(BaseView<?> baseView) {
        ControllerFactory.INSTANCE.getUseCaseDiagramController().removeActor(
                (Actor) baseView.getRepresented());                
    }
}
