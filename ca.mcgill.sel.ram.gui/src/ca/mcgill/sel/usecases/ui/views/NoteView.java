package ca.mcgill.sel.usecases.ui.views;

import org.eclipse.emf.common.notify.Notification;
import org.mt4j.components.MTComponent;
import org.mt4j.components.TransformSpace;
import org.mt4j.components.visibleComponents.shapes.MTPolygon;
import org.mt4j.util.math.Vector3D;
import org.mt4j.util.math.Vertex;

import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent.Alignment;
import ca.mcgill.sel.ram.ui.layouts.DefaultLayout;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Fonts;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.usecases.LayoutElement;
import ca.mcgill.sel.usecases.NamedElement;
import ca.mcgill.sel.usecases.Note;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.ui.utils.UseCaseDiagramHandlerFactory;
import ca.mcgill.sel.usecases.ui.views.handler.INoteViewHandler;

public class NoteView extends LinkableView<INoteViewHandler> {

    /**
     * The minimum width of the view.
     */
    protected static final float MINIMUM_WIDTH = 50f;
    
    private static final int CONTENT_FIELD_BUFFER = 20;
    
    private static final float FOLD_WIDTH_RATIO = 0.05f;
    
    /**
     * Reference to the represented {@link Note}.
     */
    protected Note note;
    
    /**
     * The view used for displaying the note's content.
     */
    protected TextView contentField;

    private MTPolygon lowerLeftFoldTriangle;

    private MTPolygon outline;

    
    protected NoteView(UseCaseDiagramView useCaseDiagramView, Note note, LayoutElement layoutElement) {
        super(useCaseDiagramView, note, layoutElement);
        
        this.note = note;
        
        setMinimumWidth(MINIMUM_WIDTH);
        setNoStroke(true);
        setNoFill(true);
        setLayout(new DefaultLayout());
        
        addContentField();
        
        buildNoteFold();
        
        // translate the class based on the meta-model
        if (layoutElement != null) {
            setLayoutElement(layoutElement);
        }
    }
    
    /**
     * Builds the note outline and the fold on the upper right corner.
     */
    private void buildNoteFold() {
        if (contentField == null) {
            return;
        }
        
        float foldSize = getWidth() * FOLD_WIDTH_RATIO;
        
        Vertex[] currentVertices = getVerticesGlobal();
        
        Vertex upperLeftVertex = new Vertex(currentVertices[0].getX(), currentVertices[0].getY());
        float x = upperLeftVertex.getX() + getWidth();
        float y = upperLeftVertex.getY();
        
        Vertex lowerLeftVertex = new Vertex(x - getWidth(),
                y + getHeight());
        Vertex lowerRightVertex = new Vertex(x, y + getHeight());
        
        Vertex leftFoldTop = new Vertex(x - foldSize, y);
        Vertex leftFoldBottom = new Vertex(x - foldSize, y + foldSize);
        Vertex rightFoldBottom = new Vertex(x, y + foldSize);
        
        outline = new RamRectangleComponent();
        outline.setVertices(new Vertex[] {upperLeftVertex, leftFoldTop,
            rightFoldBottom, lowerRightVertex, lowerLeftVertex, upperLeftVertex});
        outline.setNoStroke(false);
        outline.setNoFill(false);
        outline.setFillColor(Colors.NOTE_VIEW_FILL_COLOR);
        outline.setStrokeColor(Colors.CLASS_VIEW_DEFAULT_STROKE_COLOR);
        outline.setEnabled(false);
        outline.setPickable(false);
        
        addChild(outline);
        contentField.sendToFront();
        
        lowerLeftFoldTriangle = new MTPolygon(RamApp.getApplication(), new Vertex[] {leftFoldTop,
            rightFoldBottom, leftFoldBottom, leftFoldTop});
        lowerLeftFoldTriangle.setNoStroke(false);
        lowerLeftFoldTriangle.setNoFill(false);
        lowerLeftFoldTriangle.setFillColor(Colors.NOTE_VIEW_FILL_COLOR);
        lowerLeftFoldTriangle.setStrokeColor(Colors.CLASS_VIEW_DEFAULT_STROKE_COLOR);
        lowerLeftFoldTriangle.setEnabled(false);
        lowerLeftFoldTriangle.setPickable(false);
        
        outline.addChild(lowerLeftFoldTriangle);
    }

    /**
     * Resizes the note fold.
     */
    private void resizeNoteFold() { 
        if (outline != null) {
            outline.setSizeXYGlobal(contentField.getWidth(), contentField.getHeight());
            outline.setPositionGlobal(this.getPosition(TransformSpace.GLOBAL));
            setMinimumSize(contentField.getWidth(), contentField.getHeight());
            setMaximumSize(contentField.getWidth(), contentField.getHeight());
        }
    }
    
    @Override
    public boolean containsPointGlobal(Vector3D testPoint) {
        return contentField.containsPointGlobal(testPoint);
    }
    
    /**
     * Getter for the note represented by this view.
     * @return the represented note
     */
    public Note getNote() {
        return note;
    }
    
    /**
     * Show keyboard for content field of this view.
     * 
     */
    @Override
    public void showKeyboard() {
        contentField.showKeyboard();
    }

    /**
     * Adds text field to show the note content.
     */
    protected void addContentField() {
        // Add the name field to base view
        contentField = new TextView(note, UcPackage.Literals.NOTE__CONTENT);
        contentField.setFont(Fonts.DEFAULT_FONT_MEDIUM);
        contentField.setUniqueName(true);
        contentField.setAlignment(Alignment.CENTER_ALIGN);
        contentField.setPlaceholderText(Strings.PH_ENTER_NOTE_TEXT);
        contentField.setBufferSize(Cardinal.EAST, CONTENT_FIELD_BUFFER);
        contentField.setNewlineDisabled(false);
        
        contentField.setHandler(UseCaseDiagramHandlerFactory.INSTANCE.getNoteContentHandler());
        
        addChild(contentField);
    }
    
    @Override
    protected void handleChildResized(MTComponent component) {
        super.handleChildResized(component);
        resizeNoteFold();
    }
  
    @Override
    public void notifyChanged(Notification notification) {
        super.notifyChanged(notification);
        NamedElement annotated = null;
        if (notification.getNotifier() == note) {
            if (notification.getFeature() == UcPackage.Literals.NOTE__NOTED_ELEMENT) {
                switch (notification.getEventType()) {
                    case Notification.REMOVE:
                        annotated = (NamedElement) notification.getOldValue();
                        useCaseDiagramView.removeAnnotationView(note, annotated);
                        break;
                    case Notification.ADD:
                        annotated = (NamedElement) notification.getNewValue();
                        useCaseDiagramView.addAnnotationView(note, annotated);
                        break;
                } 
            }
        }
    }

    @Override
    public void destroy() {        
        destroyRelationships();
        
        this.destroyAllChildren();
        super.destroy();
    }
}
