package ca.mcgill.sel.usecases.ui.views;

import org.eclipse.emf.common.notify.Notification;
import org.mt4j.util.MTColor;

import ca.mcgill.sel.ram.ui.components.RamRoundedRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent.Alignment;
import ca.mcgill.sel.ram.ui.layouts.VerticalLayout;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Fonts;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.usecases.Actor;
import ca.mcgill.sel.usecases.LayoutElement;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.UseCase;
import ca.mcgill.sel.usecases.ui.utils.UseCaseDiagramHandlerFactory;
import ca.mcgill.sel.usecases.ui.views.AssociationView.AssociationType;
import ca.mcgill.sel.usecases.ui.views.handler.IUseCaseViewHandler;

public class UseCaseView extends LinkableView<IUseCaseViewHandler> {

    private static final float MINIMUM_WIDTH = 80.0f;
    private static final float MAXIMUM_WIDTH = 120.0f;
    private static final int ELLIPSE_ARC_RADIUS = 30;
    private static final float ELLIPSE_VERTICAL_BUFFER = 10.0f;
    private static final float ELLIPSE_HORIZONTAL_BUFFER = 5.0f;
    
    private RamRoundedRectangleComponent ellipse;
    
    protected UseCaseView(UseCaseDiagramView useCaseDiagramView, UseCase represented, LayoutElement layoutElement) {
        super(useCaseDiagramView, represented, layoutElement);

        setMinimumWidth(MINIMUM_WIDTH);
        setNoFill(true);
        setNoStroke(true);
        setLayout(new VerticalLayout());
        
        ellipse = new RamRoundedRectangleComponent(ELLIPSE_ARC_RADIUS);
        ellipse.setMinimumWidth(MINIMUM_WIDTH);
        ellipse.setMaximumWidth(MAXIMUM_WIDTH);
        ellipse.setNoStroke(false);
        ellipse.setNoFill(false);
        ellipse.setFillColor(Colors.CLASS_VIEW_DEFAULT_FILL_COLOR);
        ellipse.setStrokeColor(Colors.DEFAULT_ELEMENT_STROKE_COLOR);
        ellipse.setEnabled(true);
        ellipse.setPickable(true);
        ellipse.setLayout(new VerticalLayout());
        ellipse.setBufferSize(Cardinal.NORTH, ELLIPSE_VERTICAL_BUFFER);
        ellipse.setBufferSize(Cardinal.SOUTH, ELLIPSE_VERTICAL_BUFFER);
        ellipse.setBufferSize(Cardinal.EAST, ELLIPSE_HORIZONTAL_BUFFER);
        ellipse.setBufferSize(Cardinal.WEST, ELLIPSE_HORIZONTAL_BUFFER);
        addChild(ellipse);
        
        addNameField();
        
        // translate the class based on the meta-model
        if (layoutElement != null) {
            setLayoutElement(layoutElement);
        }       
    }
    
    public UseCase getUseCase() {
        return (UseCase) this.represented;
    }

    @Override
    public void destroy() {        
        destroyRelationships();
        this.destroyAllChildren();
        super.destroy();
    }
    
    @Override
    public void notifyChanged(Notification notification) {
        super.notifyChanged(notification);
        UseCase useCase = this.getUseCase();
        
        if (notification.getNotifier() == useCase) {
            if (notification.getFeature() == UcPackage.Literals.USE_CASE__PRIMARY_ACTOR
                    || notification.getFeature() == UcPackage.Literals.USE_CASE__SECONDARY_ACTORS) {
                AssociationType associationType;
                if (notification.getFeature() == UcPackage.Literals.USE_CASE__PRIMARY_ACTOR) {
                    associationType = AssociationType.PRIMARY; 
                } else {
                    associationType = AssociationType.SECONDARY;
                }
                
                switch (notification.getEventType()) {
                    case Notification.ADD:
                        Actor newActor = (Actor) notification.getNewValue();
                        useCaseDiagramView.addAssociationView(useCase, newActor, associationType);
                        break;
                    
                    case Notification.SET:
                        Actor setActor = (Actor) notification.getNewValue();
                        if (setActor == null) {
                            Actor unsetActor = (Actor) notification.getOldValue();
                            useCaseDiagramView.removeAssociationView(useCase, unsetActor);
                        } else {
                            useCaseDiagramView.addAssociationView(useCase, setActor, associationType);
                        }
                    case Notification.REMOVE:
                        Actor oldActor = (Actor) notification.getOldValue();
                        useCaseDiagramView.removeAssociationView(useCase, oldActor);
                        break;
                        
                }
            }
        }        
    }
    
    @Override
    public void setStrokeColor(MTColor color) {
        if (ellipse != null) {
            ellipse.setStrokeColor(color);    
        }        
    }
    
    @Override
    public MTColor getStrokeColor() {
        if (ellipse != null) {
            return ellipse.getStrokeColor();
        } else {
            return super.getStrokeColor();
        }
    }
    
    private void addNameField() {
        // Add the name field to base view
        nameField = new TextView(getUseCase(), UcPackage.Literals.NAMED_ELEMENT__NAME);
        nameField.setFont(Fonts.DEFAULT_FONT_MEDIUM);
        nameField.setAlignment(Alignment.CENTER_ALIGN);
        nameField.setPlaceholderText(Strings.PH_ENTER_USE_CASE_NAME);
        nameField.setNewlineDisabled(true);
        nameField.setNoFill(true);
        ((TextView) nameField).setUniqueName(true);
        
        ellipse.addChild(nameField);
        
        ((TextView) nameField).setHandler(UseCaseDiagramHandlerFactory.INSTANCE.getTextViewHandler());
    }
}
