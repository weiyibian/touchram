package ca.mcgill.sel.usecases.ui.views;

import org.eclipse.emf.ecore.EObject;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.views.IRelationshipEndView;
import ca.mcgill.sel.ram.ui.views.RamEnd;
import ca.mcgill.sel.ram.ui.views.RamEnd.Position;
import ca.mcgill.sel.ram.ui.views.RelationshipView;
import ca.mcgill.sel.usecases.NamedElement;

/**
 * Represents a relationship between a {@link Note} and a class.
 * 
 * @author yhattab
 */
public class AnnotationView extends RelationshipView<EObject, BaseView<?>> {
    
    /**
     * Distance from the end of the note to the center, the point where the next line is drawn (orthogonal).
     */
    private NoteView noteView;
    private BaseView<?> annotatedElementView;
    
    /**
     * Creates a new {@link AnnotationView} for the given note and class.
     * 
     * @param noteView
     *            the {@link NoteView} for the note
     * @param annotatedElementView
     *            the {@link BaseView} for the annotated element
     */
    public AnnotationView(NoteView noteView, BaseView<?> annotatedElementView) {
        super(noteView.getNote(), noteView, annotatedElementView.getRepresented(), annotatedElementView);
        setLineStyle(LineStyle.DOTTED);
        this.drawColor = Colors.ANNOTATION_STROKE_COLOR;
        
        
        this.noteView = noteView;
        this.annotatedElementView = annotatedElementView;
    }
    
    /**
     * Draws the line between the center of the note and the center of the annotated element (not using the CDEnds).
     */
    @Override
    protected void drawAllLines() {
        //
        Vector3D noteCenter = noteView.getCenterPointRelativeToParent();
        Vector3D elementCenter = annotatedElementView.getCenterPointRelativeToParent();
        
        if (noteView.isVisible()) {
            drawLine(noteCenter.getX(), noteCenter.getY(), null, elementCenter.getX(), elementCenter.getY());
        }
        
        //force linked views to redraw, avoiding line appearing on top of them
        noteView.sendToFront();
        annotatedElementView.sendToFront();
        
    }
      
    /**
     * Returns the view of the annotated element.
     * 
     * @return the {@link BaseView} of the annotated element
     */
    public BaseView<?> getAnnotatedElementView() {
        return annotatedElementView;
    }
    
    /**
     * Returns the view of the note.
     * 
     * @return the {@link NoteView} of the annotation origin
     */
    public NoteView getNoteView() {
        return noteView;
    }

    @Override
    public void update() {
        drawAllLines();
    }
    
    @Override
    protected void updateRelationshipEnds(RamRectangleComponent viewFrom, RamRectangleComponent viewTo,
            Position fromEndPosition, Position toEndPosition) {
     // now we set the positions.
        if (viewFrom instanceof LinkableView) {
            // process from end
            LinkableView<?> classViewFrom = (LinkableView<?>) viewFrom;
            LinkableView<?> classViewTo = (LinkableView<?>) viewTo;

            // if previous and current positions are different
            // also if fromEnd.getPosition() is null
            if (fromEndPosition != fromEnd.getPosition()) {
                classViewFrom.moveRelationshipEnd(fromEnd, fromEndPosition);
                classViewFrom.updateLayout();
            } else {
                // if position is the same reset the positions
                // for all ends on the same edge of this view.
                classViewFrom.setCorrectPosition(fromEnd);
            }

            // process to end
            if (toEndPosition != toEnd.getPosition()) {
                classViewTo.moveRelationshipEnd(toEnd, toEndPosition);
                classViewFrom.moveRelationshipEnd(fromEnd, fromEndPosition);
                classViewTo.updateLayout();
            } else {
                classViewTo.setCorrectPosition(toEnd);
            }
        } else if (viewFrom instanceof RamRectangleComponent) {
            // TODO: This is what everyone should do (see issue #109).
            IRelationshipEndView fromRelationshipView = null;
            IRelationshipEndView toRelationshipView = null;
            if (fromEnd.getComponentView() instanceof IRelationshipEndView) {
                fromRelationshipView = (IRelationshipEndView) fromEnd.getComponentView();
            }
            if (toEnd.getComponentView() instanceof IRelationshipEndView) {
                toRelationshipView = (IRelationshipEndView) toEnd.getComponentView();
            }
            // End has to be moved somewhere else.
            if (fromEndPosition != fromEnd.getPosition()) {
                Position oldPosition = fromEnd.getPosition();
                fromEnd.setPosition(fromEndPosition);
                if (fromRelationshipView != null) {
                    fromRelationshipView.moveRelationshipEnd(fromEnd, oldPosition, fromEndPosition);
                }
            } else {
                if (fromRelationshipView != null) {
                    fromRelationshipView.updateRelationshipEnd(fromEnd);
                }
            }

            if (toEndPosition != toEnd.getPosition()) {
                Position oldPosition = toEnd.getPosition();
                toEnd.setPosition(toEndPosition);
                if (toRelationshipView != null) {
                    toRelationshipView.moveRelationshipEnd(toEnd, oldPosition, toEndPosition);
                }
                // The from end might have to be fixed depending on what happened to the to end.
                if (fromRelationshipView != null) {
                    fromRelationshipView.updateRelationshipEnd(fromEnd);
                }
            } else {
                if (toRelationshipView != null) {
                    toRelationshipView.updateRelationshipEnd(toEnd);
                }
            }
        }
    }
    
    /**
     * Destroys the link to this relationship view on the other end.
     */
    public void destroyOppositeEnd() {
        // Annotations also need to remove the link from the other end
        BaseView<?> annotatedView = getAnnotatedElementView();
        if (annotatedView instanceof LinkableView) {
            LinkableView<?> classViewFrom = (LinkableView<?>) annotatedView;
            @SuppressWarnings("unchecked")
            RamEnd<? extends NamedElement, ? extends RamRectangleComponent> toView = 
                    (RamEnd<? extends NamedElement, ? extends RamRectangleComponent>) this.getToEnd();
            classViewFrom.removeRelationshipEnd(toView);
        }
        
        super.destroy();
    }
}
