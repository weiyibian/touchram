package ca.mcgill.sel.usecases.ui.views.handler.impl;

import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.mt4j.input.inputProcessors.MTGestureEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;
import org.mt4j.input.inputProcessors.componentProcessors.unistrokeProcessor.UnistrokeEvent;
import org.mt4j.input.inputProcessors.componentProcessors.unistrokeProcessor.UnistrokeUtils.UnistrokeGesture;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.commons.StringUtil;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamSelectorComponent;
import ca.mcgill.sel.ram.ui.components.listeners.AbstractDefaultRamSelectorListener;
import ca.mcgill.sel.ram.ui.utils.GUIConstants;
import ca.mcgill.sel.ram.ui.utils.MathUtils;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.AbstractView;
import ca.mcgill.sel.ram.ui.views.OptionSelectorView;
import ca.mcgill.sel.ram.ui.views.handler.impl.AbstractViewHandler;
import ca.mcgill.sel.usecases.Actor;
import ca.mcgill.sel.usecases.NamedElement;
import ca.mcgill.sel.usecases.Note;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.UseCase;
import ca.mcgill.sel.usecases.UseCaseModel;
import ca.mcgill.sel.usecases.language.controller.ControllerFactory;
import ca.mcgill.sel.usecases.language.controller.UseCaseController;
import ca.mcgill.sel.usecases.language.controller.UseCaseModelController;
import ca.mcgill.sel.usecases.ui.views.BaseView;
import ca.mcgill.sel.usecases.ui.views.UseCaseDiagramView;
import ca.mcgill.sel.usecases.ui.views.handler.IUseCaseDiagramViewHandler;

public class UseCaseDiagramViewHandler extends AbstractViewHandler 
        implements IUseCaseDiagramViewHandler {

    private enum CreateFeature {
        CREATE_ACTOR, CREATE_USE_CASE, CREATE_NOTE
    }
    
    private enum CreateRelationship {
        PRIMARY,
        SECONDARY,
        ANNOTATION
        // INCLUDES,
        // EXTENDS
    }

    @Override
    public boolean processTapAndHoldEvent(TapAndHoldEvent tapAndHoldEvent) {
        if (tapAndHoldEvent.isHoldComplete()) {
            final UseCaseDiagramView target = (UseCaseDiagramView) tapAndHoldEvent.getTarget();
            final Vector3D position = tapAndHoldEvent.getLocationOnScreen();
            
            OptionSelectorView<CreateFeature> selector = 
                        new OptionSelectorView<CreateFeature>(CreateFeature.values());
            
            RamApp.getActiveScene().addComponent(selector, tapAndHoldEvent.getLocationOnScreen());
            // CHECKSTYLE:IGNORE AnonInnerLength: Okay here.
            selector.registerListener(new AbstractDefaultRamSelectorListener<CreateFeature>() {

                @Override
                public void elementSelected(RamSelectorComponent<CreateFeature> selector, CreateFeature element) {
                    switch (element) {
                        case CREATE_ACTOR: 
                            createNewActor(target, position);
                            break;
                        case CREATE_USE_CASE:
                            createNewUseCase(target, position);
                            break;
                        case CREATE_NOTE:
                            createNewNote(target, position);
                            break;
                    }
                }
            });
        }
        return true;
    }

    @Override
    public void handleUnistrokeGesture(AbstractView<?> target, UnistrokeGesture gesture, Vector3D startPosition,
            UnistrokeEvent event) {
        UseCaseDiagramView diagramView = (UseCaseDiagramView) target;

        switch (event.getId()) {
            case MTGestureEvent.GESTURE_ENDED:
                Vector3D startGesturePoint = event.getCursor().getStartPosition();
                Vector3D endGesturePoint = event.getCursor().getPosition();
                BaseView<?> startView = null;
                BaseView<?> endView = null;

                for (BaseView<?> view : diagramView.getBaseViews()) {
                    if (MathUtils.pointIsInRectangle(startGesturePoint, view, GUIConstants.MARGIN_ELEMENT_DETECTION)) {
                        startView = view;
                    }
                    if (view.containsPointGlobal(endGesturePoint)) {
                        endView = view;
                    }
                    
                    if (startView != null & endView != null) {
                        handleRelationshipCreation(diagramView, startView, endView, endGesturePoint);
                        return;
                    }
                }
                break;
        }
        
        // TODO: shapes
    }

    @Override
    public boolean processTapEvent(TapEvent tapEvent) {
        if (tapEvent.isTapped()) {

            UseCaseDiagramView target = (UseCaseDiagramView) tapEvent.getTarget();
            target.deselect();
        }

        return true;
    }
    
    /**
     * Handles the creation of a note in the class diagram.
     *
     * @param view the view representing the current class diagram
     * @param position the position where the note should be located
     */
    @SuppressWarnings("static-method")
    private void createNewNote(final UseCaseDiagramView view, final Vector3D position) {
        final UseCaseModel ucd = view.getUseCaseModel();
        
        ucd.eAdapters().add(new EContentAdapter() {
            
            private Note note;
            
            @Override
            public void notifyChanged(Notification notification) {
                if (notification.getFeature() == UcPackage.Literals.USE_CASE_MODEL__NOTES) {
                    if (notification.getEventType() == Notification.ADD) {
                        note = (Note) notification.getNewValue();
                    }
                } else if (notification.getFeature() == UcPackage.Literals.CONTAINER_MAP__VALUE) {
                    if (notification.getEventType() == Notification.ADD) {
                        view.getNoteView(note).showKeyboard();
                        ucd.eAdapters().remove(this);
                    }
                }
                
            }
        });
       
        ControllerFactory.INSTANCE.getUseCaseDiagramController().createNewNote(
                ucd, position.getX(), position.getY());
    }
    
    /**
     * Handles the creation of a use case in the diagram.
     *
     * @param view the view representing the current diagram
     * @param position the position where the note should be located
     */
    @SuppressWarnings("static-method")
    private void createNewUseCase(final UseCaseDiagramView view, final Vector3D position) {
        final UseCaseModel ucd = view.getUseCaseModel();
        
        ucd.eAdapters().add(new EContentAdapter() {
            
            private UseCase useCase;
            
            @Override
            public void notifyChanged(Notification notification) {
                if (notification.getFeature() == UcPackage.Literals.USE_CASE_MODEL__USE_CASES) {
                    if (notification.getEventType() == Notification.ADD) {
                        useCase = (UseCase) notification.getNewValue();
                    }
                } else if (notification.getFeature() == UcPackage.Literals.CONTAINER_MAP__VALUE) {
                    if (notification.getEventType() == Notification.ADD) {
                        view.getUseCaseView(useCase).showKeyboard();
                        view.getUseCaseView(useCase).clearNameField();
                        ucd.eAdapters().remove(this);
                    }
                }
                
            }
        });
       
        Set<String> names = new HashSet<String>();

        for (UseCase u : view.getUseCaseModel().getUseCases()) {
            names.add(u.getName());
        }
        
        String useCaseName = StringUtil.createUniqueName(Strings.DEFAULT_USE_CASE_NAME, names);
        
        ControllerFactory.INSTANCE.getUseCaseDiagramController().createNewUseCase(
                ucd, useCaseName, position.getX(), position.getY());
    }
    
    /**
     * Handles the creation of a use case in the diagram.
     *
     * @param view the view representing the current diagram
     * @param position the position where the note should be located
     */
    @SuppressWarnings("static-method")
    private void createNewActor(final UseCaseDiagramView view, final Vector3D position) {
        final UseCaseModel ucd = view.getUseCaseModel();
        
        ucd.eAdapters().add(new EContentAdapter() {
            
            private Actor actor;
            
            @Override
            public void notifyChanged(Notification notification) {
                if (notification.getFeature() == UcPackage.Literals.USE_CASE_MODEL__ACTORS) {
                    if (notification.getEventType() == Notification.ADD) {
                        actor = (Actor) notification.getNewValue();
                    }
                } else if (notification.getFeature() == UcPackage.Literals.CONTAINER_MAP__VALUE) {
                    if (notification.getEventType() == Notification.ADD) {
                        view.getActorView(actor).showKeyboard();
                        view.getActorView(actor).clearNameField();
                        ucd.eAdapters().remove(this);
                    }
                }
                
            }
        });
       
        Set<String> names = new HashSet<String>();

        for (Actor a : view.getUseCaseModel().getActors()) {
            names.add(a.getName());
        }        
        String actorName = StringUtil.createUniqueName(Strings.DEFAULT_ACTOR_NAME, names);
        
        ControllerFactory.INSTANCE.getUseCaseDiagramController().createNewActor(
                ucd, actorName, position.getX(), position.getY());
    }
    
    /**
     * Handles creation of relationships for the given start (from) and end view (to).
     * 
     * @param useCaseDiagramView the {@link UseCaseDiagramView} this gesture was performed in
     * @param startView the {@link BaseView} from which the gesture was performed
     * @param endView the {@link BaseView} to which the gesture was performed
     * @param endGesturePoint the position gesture finished at
     */
    private static void handleRelationshipCreation(UseCaseDiagramView useCaseDiagramView,
            BaseView<?> startView, BaseView<?> endView,
            Vector3D endGesturePoint) {
        UseCaseModel ucd = useCaseDiagramView.getUseCaseModel();
        EnumSet<CreateRelationship> availableOptions = EnumSet.allOf(CreateRelationship.class);
        
        EObject fromEntity = startView.getRepresented();
        EObject toEntity = endView.getRepresented();
        
        UseCase useCase;
        Actor actor;
        NamedElement targetElement;
        Note note;
        if (fromEntity instanceof UseCase && toEntity instanceof UseCase) {
            // TODO: includes, extends
            actor = null;
            useCase = null;
            targetElement = null;
            note = null;
        } else if (fromEntity instanceof Note || toEntity instanceof Note) {
            if (fromEntity instanceof Note && toEntity instanceof NamedElement) {
                note = (Note) fromEntity;
                targetElement = (NamedElement) toEntity;
            } else if (toEntity instanceof Note && fromEntity instanceof NamedElement) {
                note = (Note) toEntity;
                targetElement = (NamedElement) fromEntity;
            } else {
                targetElement = null;
                note = null;
            }
            
            availableOptions.remove(CreateRelationship.PRIMARY);
            availableOptions.remove(CreateRelationship.SECONDARY);
            actor = null;
            useCase = null;
        } else if (fromEntity instanceof Actor || toEntity instanceof Actor) {
            targetElement = null;
            note = null;
            
            if (fromEntity instanceof Actor && toEntity instanceof UseCase) {
                actor = (Actor) fromEntity;
                useCase = (UseCase) toEntity;
            } else if (toEntity instanceof Actor && fromEntity instanceof UseCase) {
                actor = (Actor) toEntity;
                useCase = (UseCase) fromEntity;
            } else {
                actor = null;
                useCase = null;
            }
        } else {
            actor = null;
            useCase = null;
            targetElement = null;
            note = null;
        }
        
        if (useCase == null || actor == null) {
            availableOptions.remove(CreateRelationship.PRIMARY);
            availableOptions.remove(CreateRelationship.SECONDARY);
        } else {
            if (useCase.getPrimaryActor() == actor) {
                availableOptions.remove(CreateRelationship.PRIMARY);
            } else if (useCase.getSecondaryActors().contains(actor)) {
                availableOptions.remove(CreateRelationship.SECONDARY);
            }
        }
        
        if (targetElement == null || note == null) {
            availableOptions.remove(CreateRelationship.ANNOTATION);
        }
        
        if (!availableOptions.isEmpty()) {
            OptionSelectorView<CreateRelationship> selector = 
                    new OptionSelectorView<CreateRelationship>(availableOptions.toArray(new CreateRelationship[] { }));
            
            RamApp.getActiveScene().addComponent(selector, endGesturePoint);

            // CHECKSTYLE:IGNORE AnonInnerLength: Okay here.
            selector.registerListener(new AbstractDefaultRamSelectorListener<CreateRelationship>() {

                @Override
                public void elementSelected(RamSelectorComponent<CreateRelationship> selector,
                        CreateRelationship element) {                    
                    
                    UseCaseController controller = ControllerFactory.INSTANCE.getUseCaseController();
                    UseCaseModelController ucdController = ControllerFactory.INSTANCE.getUseCaseDiagramController();
                    switch (element) {
                        case PRIMARY:
                            controller.setPrimaryActor(actor, useCase);
                            break;
                        case SECONDARY:
                            controller.addSecondaryActor(actor, useCase);
                            break;
                        case ANNOTATION:
                            ucdController.createAnnotation(ucd, note, targetElement);
                            break;
                    }
                }
            });
        }
    }

    @Override
    public boolean handleTapAndHold(UseCaseDiagramView useCaseDiagramView, BaseView<?> target) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean handleDoubleTap(UseCaseDiagramView structuralDiagramView, BaseView<?> target) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void dragAllSelected(UseCaseDiagramView udv, Vector3D directionVector) {
        for (BaseView<?> baseView : udv.getSelectedElements()) {
            // create a copy of the translation vector, because translateGlobal modifies it
            baseView.translateGlobal(new Vector3D(directionVector));
        }        
    }
}
