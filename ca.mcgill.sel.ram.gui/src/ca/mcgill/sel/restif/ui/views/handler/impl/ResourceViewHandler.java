package ca.mcgill.sel.restif.ui.views.handler.impl;

import org.mt4j.input.inputProcessors.MTGestureEvent;
import org.mt4j.input.inputProcessors.componentProcessors.dragProcessor.DragEvent;

import ca.mcgill.sel.restif.ui.views.BaseView;
import ca.mcgill.sel.restif.ui.views.handler.IResourceViewHandler;

public class ResourceViewHandler extends BaseViewHandler implements IResourceViewHandler {
    
    // overwrite this method in order to prevent dragging the resourceView
    @Override
    public boolean processDragEvent(DragEvent dragEvent) {
        return true;
    }

    @Override
    public void removeRepresented(BaseView<?> baseView) {
        // TODO Auto-generated method stub

    }
    
    // disable the pop up menu
    @Override
    public boolean processGestureEvent(MTGestureEvent gestureEvent) {
        return false;
    }
}
