package ca.mcgill.sel.restif.ui.views.handler.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;
import org.mt4j.input.inputProcessors.componentProcessors.unistrokeProcessor.UnistrokeEvent;
import org.mt4j.input.inputProcessors.componentProcessors.unistrokeProcessor.UnistrokeUtils.UnistrokeGesture;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamSelectorComponent;
import ca.mcgill.sel.ram.ui.components.listeners.AbstractDefaultRamSelectorListener;
import ca.mcgill.sel.ram.ui.views.AbstractView;
import ca.mcgill.sel.ram.ui.views.OptionSelectorView;
import ca.mcgill.sel.ram.ui.views.handler.impl.AbstractViewHandler;
import ca.mcgill.sel.restif.PathFragment;
import ca.mcgill.sel.restif.RestIF;
import ca.mcgill.sel.restif.RestifPackage;
import ca.mcgill.sel.restif.language.controller.ControllerFactory;
import ca.mcgill.sel.restif.ui.scenes.DisplayRestTreeScene;
import ca.mcgill.sel.restif.ui.views.BaseView;
import ca.mcgill.sel.restif.ui.views.PathFragmentView;
import ca.mcgill.sel.restif.ui.views.RestTreeView;
import ca.mcgill.sel.restif.ui.views.handler.IRestTreeViewHandler;

public class RestTreeViewHandler extends AbstractViewHandler implements IRestTreeViewHandler {

    private enum CreatePathFragment {
        CREATE_STATIC_FRAGMENT,
        CREATE_DYNAMIC_FRAGMENT
    }

    @Override
    public boolean processTapEvent(TapEvent tapEvent) {
        if (tapEvent.isTapped()) {

            RestTreeView target = (RestTreeView) tapEvent.getTarget();
            target.deselect();
        }

        return true;
    }

    @Override
    public boolean processTapAndHoldEvent(TapAndHoldEvent tapAndHoldEvent) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void handleUnistrokeGesture(AbstractView<?> target, UnistrokeGesture gesture, Vector3D startPosition,
            UnistrokeEvent event) {
        DisplayRestTreeScene scene = (DisplayRestTreeScene) RamApp.getActiveScene();
        if (scene != null) {
            PathFragmentView startPathFragmentView = scene.liesAround(
                    event.getCursor().getStartPosition());
            Vector3D childPosition = event.getCursor().getPosition();

            // TODO check if childPosition is an elligible position for the child (no conflicts with existing path
            // fragments)

            if (startPathFragmentView != null) {
                OptionSelectorView<CreatePathFragment> selector =
                        new OptionSelectorView<CreatePathFragment>(CreatePathFragment.class.getEnumConstants());

                RamApp.getActiveScene().addComponent(selector, childPosition);

                // check if there are any path fragments at the same "height" of the new path fragment
                // if there is set the y of the new path fragment as that to maintain equal height
                RestTreeView restTreeView = startPathFragmentView.getParentOfType(RestTreeView.class);
                int height = restTreeView.findHeight(startPathFragmentView.getPathFragment()) + 1;                
                float yChildAtSameHeight = restTreeView.findYForChildAtSameHeight(height);
                
                if (yChildAtSameHeight != -1) {
                    childPosition.setY(yChildAtSameHeight);
                }
                
                // CHECKSTYLE:IGNORE AnonInnerLength: Okay here.
                selector.registerListener(new AbstractDefaultRamSelectorListener<CreatePathFragment>() {
                    @Override
                    public void elementSelected(RamSelectorComponent<CreatePathFragment> selector,
                            CreatePathFragment element) {
                        switch (element) {
                            case CREATE_STATIC_FRAGMENT:
                                createStaticFragment((RestTreeView) target,
                                        childPosition, startPathFragmentView.getPathFragment());
                                break;
                            case CREATE_DYNAMIC_FRAGMENT:
                                createDynamicFragment((RestTreeView) target,
                                        childPosition, startPathFragmentView.getPathFragment());
                                break;
                        }
                    }
                });
            }
        }
    }

    public void createStaticFragment(final RestTreeView view, final Vector3D position, PathFragment parent) {
        final RestIF restIF = (RestIF) view.getRestIF();

        restIF.eAdapters().add(new EContentAdapter() {
            private PathFragment pathFragment;

            @Override
            public void notifyChanged(Notification notification) {
                if (notification.getFeature() == RestifPackage.Literals.PATH_FRAGMENT__CHILD) {
                    if (notification.getEventType() == Notification.ADD) {
                        pathFragment = (PathFragment) notification.getNewValue();
                    }
                } else if (notification.getFeature() == RestifPackage.Literals.CONTAINER_MAP__VALUE) {
                    if (notification.getEventType() == Notification.ADD) {
                        view.getPathFragmentViewOf(pathFragment).showKeyboard();
                        view.getPathFragmentViewOf(pathFragment).clearNameField();
                        restIF.eAdapters().remove(this);
                    }
                }
            }
        });

        String childName = "/child";
        ControllerFactory.INSTANCE.getRestTreeController().addChildStaticFragment(
                view.getRestIF(), parent, childName, position.getX(), position.getY());
    }

    public void createDynamicFragment(final RestTreeView view, final Vector3D position, PathFragment parent) {
        final RestIF restIF = (RestIF) view.getRestIF();

        restIF.eAdapters().add(new EContentAdapter() {
            private PathFragment pathFragment;

            @Override
            public void notifyChanged(Notification notification) {
                if (notification.getFeature() == RestifPackage.Literals.PATH_FRAGMENT__CHILD) {
                    if (notification.getEventType() == Notification.ADD) {
                        pathFragment = (PathFragment) notification.getNewValue();
                    }
                } else if (notification.getFeature() == RestifPackage.Literals.CONTAINER_MAP__VALUE) {
                    if (notification.getEventType() == Notification.ADD) {
                        view.getPathFragmentViewOf(pathFragment).showKeyboard();
                        view.getPathFragmentViewOf(pathFragment).clearNameField();
                        restIF.eAdapters().remove(this);
                    }
                }
            }
        });

        String childName = "/{child}";
        ControllerFactory.INSTANCE.getRestTreeController().addChildDynamicFragment(
                view.getRestIF(), parent, childName, position.getX(), position.getY());
    }

    @Override
    public boolean handleDoubleTap(RestTreeView restTreeView, BaseView<?> target) {
        return false;
    }

    @Override
    public boolean handleTapAndHold(RestTreeView restTreeView, BaseView<?> target) {
        return false;
    }

    @Override
    public void dragAllSelected(RestTreeView restTreeView, Vector3D directionVector) {
        for (BaseView<?> baseView : restTreeView.getSelectedElements()) {
            // create a copy of the translation vector, because translateGlobal modifies it
            baseView.translateGlobal(new Vector3D(directionVector));
        }
    }
}
