package ca.mcgill.sel.restif.ui.views;

import java.util.ArrayList;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.edit.provider.INotifyChangedListener;

import ca.mcgill.sel.ram.ui.components.RamLineComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent.Alignment;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Fonts;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.restif.LayoutElement;
import ca.mcgill.sel.restif.PathFragment;
import ca.mcgill.sel.restif.RestifPackage;
import ca.mcgill.sel.restif.ui.views.handler.IPathFragmentViewHandler;
import ca.mcgill.sel.restif.ui.views.handler.RestTreeHandlerFactory;

public class PathFragmentView extends LinkableView<IPathFragmentViewHandler> implements INotifyChangedListener {

    private static final float MINIMUM_WIDTH = 136.0f;

    private PathFragment pathFragment;
    private ArrayList<PathFragmentView> childrenView;
    private RamLineComponent lineToParent;

    protected PathFragmentView(RestTreeView restTreeView, PathFragment pathFragment, LayoutElement layoutElement) {
        super(restTreeView, pathFragment, layoutElement);

        this.pathFragment = pathFragment;
        childrenView = new ArrayList<>();

        setMinimumWidth(MINIMUM_WIDTH);
        setAnchor(PositionAnchor.UPPER_LEFT);
        setStrokeColor(Colors.DEFAULT_ELEMENT_STROKE_COLOR);
        setNoStroke(false);

        addNameField();
    }

    private void addNameField() {
        TextView pathFragmentNameField = new TextView(pathFragment, RestifPackage.Literals.NAMED_ELEMENT__NAME);

        pathFragmentNameField.setFont(Fonts.DEFAULT_FONT_MEDIUM);
        pathFragmentNameField.setAlignment(Alignment.CENTER_ALIGN);
        pathFragmentNameField.setNewlineDisabled(true);
        pathFragmentNameField.setNoFill(true);
        pathFragmentNameField.setUniqueName(false);
        pathFragmentNameField.setPlaceholderText("Enter path fragment name:");
        pathFragmentNameField.setHandler(RestTreeHandlerFactory.INSTANCE.getPathFragmentNameHandler());

        setNameField(pathFragmentNameField);
    }
    
    public void addChildPathFragmentView(PathFragmentView child) {
        childrenView.add(child);
    }
    
    public void removeChildPathFragmentView(PathFragmentView child) {
        childrenView.remove(child);
    }

    public void addChildView(ResourceView resourceView) {
        addChild(resourceView);
    }

    public PathFragment getPathFragment() {
        return (PathFragment) this.represented;
    }

    /**
     * Displays the keyboard.
     */
    public void showKeyboard() {
        nameField.showKeyboard();
    }

    /**
     * Clear the name field of this view.
     *
     */
    public void clearNameField() {
        ((TextView) nameField).clearText();
    }

    @Override
    public void notifyChanged(Notification notification) {
        // when a child is added or removed a notification is sent on all path fragments
        // thus we redirect the notification to RestTreeView to be handled
        if (notification.getFeature() == RestifPackage.Literals.PATH_FRAGMENT__CHILD) {
            
            // uncomment this when testing!!!
            //this.getParentOfType(RestTreeView.class).outputRestIFModel();
            
            switch (notification.getEventType()) {                
                case Notification.ADD:                    
                    PathFragment parent = this.getParentOfType(RestTreeView.class).getParent(
                            (PathFragment) notification.getNewValue());
                    
                    if (parent != null) {                    
                        if (parent.equals(pathFragment)) {                            
                            this.getParentOfType(RestTreeView.class).notifyChanged(notification);
                        }
                    }
                    break;
                case Notification.REMOVE:                    
                    RestTreeView parentView = this.getParentOfType(RestTreeView.class);
                     
                    if (parentView.getPathFragmentViewOf(pathFragment).getChildrenView().contains(
                            parentView.getPathFragmentViewOf((
                                    PathFragment) notification.getOldValue()))) {                        
                        this.getParentOfType(RestTreeView.class).notifyChanged(notification);
                    }
                    
                    break;
            }
        }
    }

    @Override
    public void destroy() {
        this.destroyAllChildren();
        super.destroy();
    }
    
    public ArrayList<PathFragmentView> getChildrenView() {
        return childrenView;
    }

    public RamLineComponent getLineToParent() {
        return lineToParent;
    }

    public void setLineToParent(RamLineComponent lineToParent) {
        this.lineToParent = lineToParent;
    }
}
