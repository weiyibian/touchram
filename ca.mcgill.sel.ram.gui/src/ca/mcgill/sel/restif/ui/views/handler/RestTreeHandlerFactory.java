package ca.mcgill.sel.restif.ui.views.handler;

import ca.mcgill.sel.ram.ui.events.listeners.ActionListener;
import ca.mcgill.sel.ram.ui.views.handler.IDisplaySceneHandler;
import ca.mcgill.sel.ram.ui.views.handler.ITextViewHandler;
import ca.mcgill.sel.restif.ui.scenes.handler.impl.DisplayRestTreeSceneHandler;
import ca.mcgill.sel.restif.ui.views.handler.impl.PathFragmentViewHandler;
import ca.mcgill.sel.restif.ui.views.handler.impl.ResourceButtonHandler;
import ca.mcgill.sel.restif.ui.views.handler.impl.ResourceViewHandler;
import ca.mcgill.sel.restif.ui.views.handler.impl.RestTreeViewHandler;
import ca.mcgill.sel.restif.ui.views.handler.impl.TextViewHandler;

/**
 * A Rest Tree specific factory to obtain the handlers.
 *
 * @author Bowen
 */
public final class RestTreeHandlerFactory {
    /**
     * The singleton instance of the factory.
     */
    public static final RestTreeHandlerFactory INSTANCE = new RestTreeHandlerFactory();
    
    private IDisplaySceneHandler displayRestTreeSceneHandler;
    private IRestTreeViewHandler restTreeViewHandler;
    private IPathFragmentViewHandler pathFragmentViewHandler;
    private IResourceViewHandler resourceViewHandler;
    private ActionListener resourceButtonHandler;
    private ITextViewHandler pathFragmentNameHandler;
        
    /**
     * Create a new instance.
     */
    private RestTreeHandlerFactory() {
    }
    
    /**
     * Returns the default handler for a display class diagram scene.
     * 
     * @return the default {@link IDisplaySceneHandler}
     */
    public IDisplaySceneHandler getDisplayRestTreeSceneHandler() {
        if (displayRestTreeSceneHandler == null) {
            displayRestTreeSceneHandler = new DisplayRestTreeSceneHandler();
        }

        return displayRestTreeSceneHandler;
    }

    /**
     * Returns the default handler for a path fragment view.
     * 
     * @return the default {@link IPathFragmentViewHandler}
     */
    public IPathFragmentViewHandler getPathFragmentViewHandler() {
        if (pathFragmentViewHandler == null) {
            pathFragmentViewHandler = new PathFragmentViewHandler();
        }

        return pathFragmentViewHandler;
    }
    
    /**
     * Returns the default handler for a rest tree view.
     * 
     * @return the default {@link IRestTreeView}
     */
    public IRestTreeViewHandler getRestTreeViewHandler() {
        if (restTreeViewHandler == null) {
            restTreeViewHandler = new RestTreeViewHandler();
        }

        return restTreeViewHandler;    
    }

    public IResourceViewHandler getResourceViewHandler() {
        if (resourceViewHandler == null) {
            resourceViewHandler = new ResourceViewHandler();
        }

        return resourceViewHandler;  
    }
    
    public ActionListener getResourceButtonHandler() {
        if (resourceButtonHandler == null) {
            resourceButtonHandler = new ResourceButtonHandler();
        }
        
        return resourceButtonHandler;
    }

    public ITextViewHandler getPathFragmentNameHandler() {
        if (pathFragmentNameHandler == null) {
            pathFragmentNameHandler = new TextViewHandler();
        }
        
        return pathFragmentNameHandler;
    }
}
