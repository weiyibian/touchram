package ca.mcgill.sel.restif.ui.views;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.mt4j.components.TransformSpace;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.RamApp.DisplayMode;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.views.RamEnd;
import ca.mcgill.sel.ram.ui.views.RamEnd.Position;
import ca.mcgill.sel.ram.ui.views.RelationshipView;
import ca.mcgill.sel.restif.LayoutElement;
import ca.mcgill.sel.restif.NamedElement;
import ca.mcgill.sel.restif.ui.views.handler.IBaseViewHandler;

/**
 * An abstract view that represents a view which can link or be linked to with relationships.
 * Any "uml box" view should extend this base view.
 * 
 * @param <H> handler for this component
 * @author hattab
 *
 */
public abstract class LinkableView<H extends IBaseViewHandler> extends BaseView<H> {

    /**
     * RelationshipEnds of this view, keyed by the position.
     */
    protected Map<Position, List<RamEnd<?, ? extends RamRectangleComponent>>> relationshipEndByPosition;

    /**
     * Initialize the relationshipEnds of the LinkableView.
     * 
     * @param restTreeView the {@link RestTreeView} that is the parent of this view
     * @param represented the {@link EObject} this view represents
     * @param layoutElement of the represented element
     */
    protected LinkableView(RestTreeView restTreeView, EObject represented, LayoutElement layoutElement) {
        super(restTreeView, represented, layoutElement);

        // initialize the position map
        relationshipEndByPosition = new HashMap<Position, List<RamEnd<?, ? extends RamRectangleComponent>>>();
        for (Position position : Position.values()) {
            relationshipEndByPosition.put(position, new ArrayList<RamEnd<?, ? extends RamRectangleComponent>>());
        }
    }

    /**
     * NOTE Will add the end if it's position is null. If the end position is null will remove it from the list and set
     * the end's position as null.
     *
     * @param end The end to move.
     * @param newPosition The new position to set.
     * @postcondition The end's position will be set to the newPosition.
     */
    public void moveRelationshipEnd(RamEnd<? extends EObject, ? extends RamRectangleComponent> end,
            Position newPosition) {
        if (end.getPosition() == null) {
            // it updates the position and calls
            // getRelationshipView().shouldUpdate();
            end.setPosition(newPosition);
            addRelationshipEndAtPosition(end);
            // setCorrectPosition(end);
        } else {
            moveRelationshipEnd(end, end.getPosition(), newPosition);
        }
    }

    /**
     * Adds the given end to this view at the position it has stored.
     *
     * @param end the end to add to this view.
     * @precondition This view is that end's associated class view.
     */
    public void addRelationshipEndAtPosition(RamEnd<? extends EObject, ? extends RamRectangleComponent> end) {
        List<RamEnd<?, ? extends RamRectangleComponent>> list = relationshipEndByPosition.get(end.getPosition());
        if (list.size() == 0) {
            end.setIsAlone(true);
        } else {
            end.setIsAlone(false);
            for (RamEnd<?, ? extends RamRectangleComponent> ramEnd : list) {
                ramEnd.setIsAlone(false);
            }
        }

        list.add(end);
        setCorrectPosition(list, end.getPosition());
    }

    /**
     * Moves the relationship end to the correct position. This needs to be done when the view itself changed either
     * size or position.
     *
     * @param end The end to move.
     * @param oldPosition The old position of this end in the class.
     * @param newPosition The new position of this end.
     * @precondition The end's position is the same position as it is in the class view's list.
     * @postcondition The end's position will be set to newPosition.
     */
    public void moveRelationshipEnd(RamEnd<?, ? extends RamRectangleComponent> end, Position oldPosition,
            Position newPosition) {
        List<RamEnd<?, ? extends RamRectangleComponent>> oldList = relationshipEndByPosition.get(oldPosition);

        oldList.remove(end);
        // update isAlone property
        if (oldList.size() == 1) {
            oldList.get(0).setIsAlone(true);
        }
        end.setIsAlone(false);

        setCorrectPosition(oldList, oldPosition);

        List<RamEnd<?, ? extends RamRectangleComponent>> newList = relationshipEndByPosition.get(newPosition);
        newList.add(end);

        // update isAlone property
        if (newList.size() == 1) {
            end.setIsAlone(true);
        } else {
            for (RamEnd<?, ? extends RamRectangleComponent> r : newList) {
                r.setIsAlone(false);
            }
        }
        end.setPosition(newPosition);
        setCorrectPosition(newList, newPosition);
    }

    /**
     * Changes the position of the given end to a specific position depending on the positioning.
     *
     * @param end the end to set the location for
     * @param classLocation the location of the class
     * @param number the number (in the index sense) of this end on this side of the view
     * @param space the space between two relationships
     */
    protected void changePosition(RamEnd<?, ?> end, Vector3D classLocation, float number, float space) {
        switch (end.getPosition()) {
            case TOP:
                end.setLocation(new Vector3D(classLocation.getX() + number * space,
                        classLocation.getY()));
                break;
            case BOTTOM:
                end.setLocation(new Vector3D(classLocation.getX() + number * space,
                        classLocation.getY() + getHeightXY(TransformSpace.RELATIVE_TO_PARENT)));
                break;
            case RIGHT:
                end.setLocation(new Vector3D(classLocation.getX() + getWidthXY(TransformSpace.RELATIVE_TO_PARENT),
                        classLocation.getY() + number * space));
                break;
            case LEFT:
                end.setLocation(new Vector3D(classLocation.getX(), classLocation.getY() + number * space));
                break;
        }
    }

    /**
     * Calculates the number of slots required on the given position.
     * 
     * @param list the list of ends on the given position
     * @param position the position to calculate the number of slots for
     * @return the number of slots required on the given position
     */
    protected int calculateNumberOfSlots(List<RamEnd<?, ? extends RamRectangleComponent>> list, Position position) {
        // First, figure out the number of inheritance relationship kinds.
        // Multiple kinds on the same edge are grouped together.
        // Then, these numbers are used to calculate the divider amount between all ends.
        // In the last step, the correct position is set for each end,
        // depending on at what position (index) in the list it occurs.
        // There might be a slight flicker sometimes. This seems to happen when the relationship position changes
        // and the line is made straight instead of edged (because it jumps all of a sudden).
        int numberOfSlots = 0;

        for (int index = 0; index < list.size(); index++) {
            numberOfSlots++;
        }

        // The actual number of elements on one side of the class,
        // which share the available space.
        int actualNumOfSlots = numberOfSlots;

        return actualNumOfSlots;
    }

    /**
     * Updates the view for all of this class' associations.
     */
    protected void updateRelationships() {
        if (relationshipEndByPosition != null) {
            for (RelationshipView<?, ? extends RamRectangleComponent> view : getAllRelationshipViews()) {
                view.updateLines();
            }
        }
    }

    /**
     * Returns all relationship views associated with this view.
     *
     * @return all relationship for this view.
     */
    public Set<RelationshipView<?, ? extends RamRectangleComponent>> getAllRelationshipViews() {
        Set<RelationshipView<?, ? extends RamRectangleComponent>> allRelationships =
                new HashSet<RelationshipView<?, ? extends RamRectangleComponent>>();
        for (List<RamEnd<?, ? extends RamRectangleComponent>> list : relationshipEndByPosition.values()) {
            for (RamEnd<?, ? extends RamRectangleComponent> end : list) {
                allRelationships.add(end.getRelationshipView());
            }
        }
        return allRelationships;
    }

    /**
     * Resets the positions for all ends on the same edge of this view.
     *
     * @param end the end that moved.
     * @precondition The end's position is the same position as it is in the class view's list.
     */
    public void setCorrectPosition(RamEnd<?, ? extends RamRectangleComponent> end) {
        List<RamEnd<?, ? extends RamRectangleComponent>> list = relationshipEndByPosition.get(end.getPosition());
        setCorrectPosition(list, end.getPosition());
    }

    /**
     * Removes the association end from the class view.
     *
     * @param end The end to remove.
     * @precondition The end's position is the same position as it is in the class view's list.
     */
    public void removeRelationshipEnd(RamEnd<? extends NamedElement, ? extends RamRectangleComponent> end) {
        List<RamEnd<?, ? extends RamRectangleComponent>> list = relationshipEndByPosition.get(end.getPosition());
        list.remove(end);

        if (list.size() == 1) {
            list.get(0).setIsAlone(true);
        }

        setCorrectPosition(list, end.getPosition());
    }

    @Override
    public void destroy() {
        // destroy relationships
        for (RelationshipView<?, ?> view : getAllRelationshipViews()) {
            view.destroy();
        }

        relationshipEndByPosition.clear();

        // destroy rest
        super.destroy();
    }

    /**
     * Sets the correct position of all ends of the given list. The list is for the given position. The ends are
     * positioned such that a "pretty drawing" is achieved. More documentation is necessary.
     *
     * @param list the list of ends for a specific position
     * @param position the position the list of ends is for
     */
    // TODO: Needs refactoring!
    // CHECKSTYLE:IGNORE MethodLength FOR 2 LINES: Temporary fix. Needs refactoring.
    // CHECKSTYLE:IGNORE ReturnCount: Unfortunately, it is very difficult to reduce the return count. Needs refactoring.
    protected void setCorrectPosition(List<RamEnd<?, ? extends RamRectangleComponent>> list, Position position) {
        // to speed things up
        if (list.size() == 0) {
            return;
        }

        // the top left position is needed to determine the new ends position along the side of the
        // class
        Vector3D classLocation = getLocalVecToParentRelativeSpace(this, getBounds().getVectorsLocal()[0]);

        float availableSpace = 0;
        switch (position) {
            case TOP:
            case BOTTOM:
                availableSpace = getWidthXY(TransformSpace.RELATIVE_TO_PARENT);
                break;
            case LEFT:
            case RIGHT:
                availableSpace = getHeightXY(TransformSpace.RELATIVE_TO_PARENT);
                break;
            default:
        }

        // if this end is the only one on this position, calculate its coordinates accordingly
        if (list.size() == 1) {
            RamEnd<?, ? extends RamRectangleComponent> thisEnd = list.get(0);
            RamEnd<?, ? extends RamRectangleComponent> otherEnd = thisEnd.getOpposite();
            if (thisEnd.isAlone() && otherEnd.isAlone()) {
                // the other end is also along on its position, so we can align them in the middle
                // of the overlap
                Vector3D otherClassLocation =
                        getLocalVecToParentRelativeSpace(otherEnd.getComponentView(), otherEnd.getComponentView()
                                .getBounds().getVectorsLocal()[0]);
                final float thisXLeft = classLocation.getX();
                final float thisXRight = thisXLeft + getWidthXY(TransformSpace.RELATIVE_TO_PARENT);
                final float thisYTop = classLocation.getY();
                final float thisYBottom = thisYTop + getHeightXY(TransformSpace.RELATIVE_TO_PARENT);
                final float otherXLeft = otherClassLocation.getX();
                final float otherXRight =
                        otherXLeft + otherEnd.getComponentView().getWidthXY(TransformSpace.RELATIVE_TO_PARENT);
                final float otherYTop = otherClassLocation.getY();
                final float otherYBottom =
                        otherYTop + otherEnd.getComponentView().getHeightXY(TransformSpace.RELATIVE_TO_PARENT);

                if (thisXLeft > otherXLeft && thisXLeft < otherXRight || thisXRight > otherXLeft
                        && thisXRight < otherXRight || thisYTop > otherYTop && thisYTop < otherYBottom
                        || thisYBottom > otherYTop && thisYBottom < otherYBottom || otherXLeft > thisXLeft
                                && otherXLeft < thisXRight
                        || otherXRight > thisXLeft && otherXRight < thisXRight
                        || otherYBottom > thisYTop && otherYBottom < thisYBottom || otherYTop > thisYTop
                                && otherYTop < thisYBottom) {
                    switch (position) {
                        case TOP:
                            if (thisXLeft > otherXLeft && thisXRight < otherXRight) {
                                // this class' edge is included in the other class'edge -> put the
                                // end in the middle
                                thisEnd.setLocation(new Vector3D((thisXLeft + thisXRight) / 2, thisYTop));
                            } else if (otherXLeft > thisXLeft && otherXRight < thisXRight) {
                                // the other class' edge is included in this edge -> put the end in
                                // the middle of the other one
                                thisEnd.setLocation(new Vector3D((otherXLeft + otherXRight) / 2, thisYTop));
                            } else {
                                // they overlap -> put the end in the center of the overlap
                                final float left = thisXLeft > otherXLeft ? thisXLeft : otherXLeft;
                                final float right = thisXRight < otherXRight ? thisXRight : otherXRight;
                                thisEnd.setLocation(new Vector3D((left + right) / 2, thisYTop));
                            }
                            break;
                        case BOTTOM:
                            if (thisXLeft > otherXLeft && thisXRight < otherXRight) {
                                // this class' edge is included in the other class'edge -> put the
                                // end in the middle
                                thisEnd.setLocation(new Vector3D((thisXLeft + thisXRight) / 2, thisYBottom));
                            } else if (otherXLeft > thisXLeft && otherXRight < thisXRight) {
                                // the other class' edge is included in this edge -> put the end in
                                // the middle of the other one
                                thisEnd.setLocation(new Vector3D((otherXLeft + otherXRight) / 2, thisYBottom));
                            } else {
                                // they overlap -> put the end in the center of the overlap
                                final float left = thisXLeft > otherXLeft ? thisXLeft : otherXLeft;
                                final float right = thisXRight < otherXRight ? thisXRight : otherXRight;
                                thisEnd.setLocation(new Vector3D((left + right) / 2, thisYBottom));
                            }
                            break;
                        case RIGHT:
                            if (thisYTop > otherYTop && thisYBottom < otherYBottom) {
                                // this class' edge is included in the other class'edge -> put the
                                // end in the middle
                                thisEnd.setLocation(new Vector3D(thisXRight, (thisYTop + thisYBottom) / 2));
                            } else if (otherYTop > thisYTop && otherYBottom < thisYBottom) {
                                // the other class' edge is included in this edge -> put the end in
                                // the middle of the other one
                                thisEnd.setLocation(new Vector3D(thisXRight, (otherYTop + otherYBottom) / 2));
                            } else {
                                final float top = thisYTop > otherYTop ? thisYTop : otherYTop;
                                final float bottom = thisYBottom < otherYBottom ? thisYBottom : otherYBottom;
                                thisEnd.setLocation(new Vector3D(thisXRight, (top + bottom) / 2));
                            }
                            break;
                        case LEFT:
                            if (thisYTop > otherYTop && thisYBottom < otherYBottom) {
                                // this class' edge is included in the other class'edge -> put the
                                // end in the middle
                                thisEnd.setLocation(new Vector3D(thisXLeft, (thisYTop + thisYBottom) / 2));
                            } else if (otherYTop > thisYTop && otherYBottom < thisYBottom) {
                                // the other class' edge is included in this edge -> put the end in
                                // the middle of the other one
                                thisEnd.setLocation(new Vector3D(thisXLeft, (otherYTop + otherYBottom) / 2));
                            } else {
                                final float top = thisYTop > otherYTop ? thisYTop : otherYTop;
                                final float bottom = thisYBottom < otherYBottom ? thisYBottom : otherYBottom;
                                thisEnd.setLocation(new Vector3D(thisXLeft, (top + bottom) / 2));
                            }
                            break;
                    }
                    return;
                }
            } else if (thisEnd.isAlone()) {
                // this end is the only one on this position, but the other one has a fixed position
                // align this ends coordinates with the other ones
                final float thisXLeft = classLocation.getX();
                final float thisXRight = thisXLeft + getWidthXY(TransformSpace.RELATIVE_TO_PARENT);
                final float thisYTop = classLocation.getY();
                final float thisYBottom = thisYTop + getHeightXY(TransformSpace.RELATIVE_TO_PARENT);
                final Vector3D otherEndLocation = otherEnd.getLocation();
                final float otherX = otherEndLocation.getX();
                final float otherY = otherEndLocation.getY();

                switch (position) {
                    case TOP:
                        if (thisXLeft < otherX && thisXRight > otherX) {
                            thisEnd.setLocation(new Vector3D(otherX, thisYTop));
                            return;
                        }
                        break;
                    case BOTTOM:
                        if (thisXLeft < otherX && thisXRight > otherX) {
                            thisEnd.setLocation(new Vector3D(otherX, thisYBottom));
                            return;
                        }
                        break;
                    case RIGHT:
                        if (thisYTop < otherY && thisYBottom > otherY) {
                            thisEnd.setLocation(new Vector3D(thisXRight, otherY));
                            return;
                        }
                        break;
                    case LEFT:
                        if (thisYTop < otherY && thisYBottom > otherY) {
                            thisEnd.setLocation(new Vector3D(thisXLeft, otherY));
                            return;
                        }
                        break;
                }
            }
        }

        if (RamApp.getDisplayMode() == DisplayMode.PRETTY) {
            // we want a pretty drawing, reorder the ends so they're less likely to overlap
            Comparator<RamEnd<?, ?>> comparator;
            switch (position) {
                case TOP:
                case BOTTOM:
                    comparator = RamEnd.HORIZONTAL_COMPARATOR;
                    break;
                case LEFT:
                case RIGHT:
                    comparator = RamEnd.VERTICAL_COMPARATOR;
                    break;
                case OFFSCREEN:
                    // set as null
                default:
                    comparator = null;
            }
            if (comparator != null) {
                Collections.sort(list, comparator);
            }
        }

        int actualNumOfSlots = calculateNumberOfSlots(list, position);
        float dividerDistance = availableSpace / (actualNumOfSlots + 1);

        int currentSlot = 0;
        for (int index = 0; index < list.size(); index++) {
            RamEnd<?, ?> associationEnd = list.get(index);

            currentSlot++;
            changePosition(associationEnd, classLocation, currentSlot, dividerDistance);
        }
    }
}
