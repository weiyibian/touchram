package ca.mcgill.sel.restif.ui.views.handler;

import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.ram.ui.events.listeners.ITapAndHoldListener;
import ca.mcgill.sel.ram.ui.events.listeners.ITapListener;
import ca.mcgill.sel.ram.ui.views.handler.IAbstractViewHandler;
import ca.mcgill.sel.restif.ui.views.BaseView;
import ca.mcgill.sel.restif.ui.views.RestTreeView;


public interface IRestTreeViewHandler extends IAbstractViewHandler, ITapListener, ITapAndHoldListener {    
    boolean handleDoubleTap(RestTreeView restTreeView, BaseView<?> target);
    
    boolean handleTapAndHold(RestTreeView restTreeView, BaseView<?> target);
    
    void dragAllSelected(RestTreeView restTreeView, Vector3D translationVect);
}