package ca.mcgill.sel.restif.ui.views;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.edit.provider.INotifyChangedListener;
import org.mt4j.input.gestureAction.TapAndHoldVisualizer;
import org.mt4j.input.inputProcessors.IGestureEventListener;
import org.mt4j.input.inputProcessors.componentProcessors.panProcessor.PanProcessorTwoFingers;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldProcessor;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapProcessor;
import org.mt4j.input.inputProcessors.componentProcessors.zoomProcessor.ZoomProcessor;
import org.mt4j.util.math.Vector3D;
import org.mt4j.util.math.Vertex;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamLineComponent;
import ca.mcgill.sel.ram.ui.events.MouseWheelProcessor;
import ca.mcgill.sel.ram.ui.events.RightClickDragProcessor;
import ca.mcgill.sel.ram.ui.events.UnistrokeProcessorLeftClick;
import ca.mcgill.sel.ram.ui.utils.GUIConstants;
import ca.mcgill.sel.ram.ui.utils.MathUtils;
import ca.mcgill.sel.ram.ui.views.AbstractView;
import ca.mcgill.sel.restif.LayoutElement;
import ca.mcgill.sel.restif.PathFragment;
import ca.mcgill.sel.restif.Resource;
import ca.mcgill.sel.restif.RestIF;
import ca.mcgill.sel.restif.RestifFactory;
import ca.mcgill.sel.restif.RestifPackage;
import ca.mcgill.sel.restif.impl.ContainerMapImpl;
import ca.mcgill.sel.restif.impl.ElementMapImpl;
import ca.mcgill.sel.restif.impl.RestifFactoryImpl;
import ca.mcgill.sel.restif.language.controller.ControllerFactory;
import ca.mcgill.sel.restif.ui.views.handler.IRestTreeViewHandler;
import ca.mcgill.sel.restif.ui.views.handler.RestTreeHandlerFactory;

public class RestTreeView extends AbstractView<IRestTreeViewHandler> implements INotifyChangedListener {

    private ContainerMapImpl layout;
    private RestIF restIF;

    private HashMap<PathFragment, PathFragmentView> pathFragmentToViewMap;
    private HashMap<Resource, ResourceView> resourceToViewMap;

    private Set<BaseView<?>> selectedElements;

    /**
     * Creates a new RestTreeView for the given rest tree. For every class inside the rest tree its own
     * view is created as well as for every association. IMPORTANT: All the view elements related to the
     * RestTreeView are added to the containerLayer.
     * 
     * @param restIF the rest tree
     * @param layout some layout.
     * @param width The width over which the elements can be displayed
     * @param height The height over which the elements can be displayed.
     */
    public RestTreeView(RestIF restIF, ContainerMapImpl layout, float width, float height) {
        super(width, height);

        this.layout = layout;
        this.restIF = restIF;

        this.pathFragmentToViewMap = new HashMap<>();
        this.resourceToViewMap = new HashMap<>();

        buildAndLayout(width, height);

        EMFEditUtil.addListenerFor(restIF, this);
        // register to the ContainerMap to receive adds/removes of ElementMaps
        EMFEditUtil.addListenerFor(this.layout, this);

        this.selectedElements = new HashSet<BaseView<?>>();
    }

    /**
     * Builds this view and layouts all classes within the dimensions given.
     * 
     * @param width the width of this view
     * @param height the height of this view
     */
    private void buildAndLayout(float width, float height) {
        // Now add them to the structural view and layout the ones that don't have a layout yet.
        Vector3D position = new Vector3D(100, 100);

        float maxX = width;
        float maxY = height;

        PathFragment root = restIF.getRoot();

        // if the root exists, display the rest tree model
        // if it does not, create a root and display the created path fragment
        if (root != null) {
            // start from the root and add the path fragment
            LayoutElement rootLayoutElement = layout == null ? null : layout.getValue().get(root);
            addPathFragment(root, rootLayoutElement);
            PathFragmentView rootPathFragmentView = pathFragmentToViewMap.get(root);

            position.setX(Math.max(position.getX(), rootLayoutElement.getX() + rootPathFragmentView.getWidth()));
            position.setY(Math.max(position.getY(), rootLayoutElement.getY() + rootPathFragmentView.getHeight()));

            maxX = Math.max(maxX, rootPathFragmentView.getX() + rootPathFragmentView.getWidth());
            maxY = Math.max(maxY, rootPathFragmentView.getY() + rootPathFragmentView.getHeight());

            // start from the root and add the resource
            Resource rootResource = getResource(root);
            boolean rootResourceExistInRestIFModel = isExistInRestIFModel(rootResource);

            // create a new layout element and set the x and y values right below the path fragment view
            LayoutElement rootResourceLayoutElement = RestifFactoryImpl.init().createLayoutElement();
            rootResourceLayoutElement.setX(rootPathFragmentView.getX());
            rootResourceLayoutElement.setY(rootPathFragmentView.getY() + rootPathFragmentView.getHeight());

            addResource(rootResource, rootResourceLayoutElement, rootPathFragmentView, rootResourceExistInRestIFModel);
            ResourceView rootResourceView = resourceToViewMap.get(rootResource);

            position.setX(Math.max(position.getX(), rootResourceLayoutElement.getX() + rootResourceView.getWidth()));
            position.setY(Math.max(position.getY(), rootResourceLayoutElement.getY() + rootResourceView.getHeight()));

            maxX = Math.max(maxX, rootResourceView.getX() + rootResourceView.getWidth());
            maxY = Math.max(maxY, rootResourceView.getY() + rootResourceView.getHeight());

            // set the parents as the root for the next tree traversal algorithm
            ArrayList<PathFragment> parents = new ArrayList<>(Arrays.asList(root));
            ArrayList<PathFragment> newParents;

            // traverse through the tree to add every path fragment and resource
            do {
                newParents = new ArrayList<>();
                for (PathFragment parent : parents) {
                    EList<PathFragment> children = parent.getChild();

                    if (children != null) {
                        newParents.addAll(children);

                        for (PathFragment child : children) {
                            // add a path fragment from the child
                            LayoutElement layoutElement = layout == null ? null : layout.getValue().get(child);
                            addPathFragment(child, layoutElement);
                            PathFragmentView pathFragmentView = pathFragmentToViewMap.get(child);

                            // add the child to the parent's children arraylist
                            pathFragmentToViewMap.get(parent).addChildPathFragmentView(pathFragmentView);

                            position.setX(Math.max(position.getX(),
                                    layoutElement.getX() + pathFragmentView.getWidth()));
                            position.setY(Math.max(position.getY(),
                                    layoutElement.getY() + pathFragmentView.getHeight()));

                            maxX = Math.max(maxX, pathFragmentView.getX() + pathFragmentView.getWidth());
                            maxY = Math.max(maxY, pathFragmentView.getY() + pathFragmentView.getHeight());

                            // start from the root and add the resource
                            Resource resource = getResource(child);
                            boolean existInRestIFModel = isExistInRestIFModel(rootResource);

                            // create a new layout element and set the x and y values right below the path fragment view
                            LayoutElement resourceLayoutElement = RestifFactoryImpl.init().createLayoutElement();
                            rootResourceLayoutElement.setX(pathFragmentView.getX());
                            rootResourceLayoutElement.setY(pathFragmentView.getY() + pathFragmentView.getHeight());
                            addResource(resource, resourceLayoutElement, pathFragmentView, existInRestIFModel);
                            ResourceView resourceView = resourceToViewMap.get(resource);

                            position.setX(
                                    Math.max(position.getX(), resourceLayoutElement.getX() + resourceView.getWidth()));
                            position.setY(
                                    Math.max(position.getY(), resourceLayoutElement.getY() + resourceView.getHeight()));

                            maxX = Math.max(maxX, resourceView.getX() + resourceView.getWidth());
                            maxY = Math.max(maxY, resourceView.getY() + resourceView.getHeight());

                            drawLine(pathFragmentToViewMap.get(child), pathFragmentToViewMap.get(parent));
                        }
                    }
                }
                parents.clear();
                parents.addAll(newParents);
            }
            while (parents.size() > 0);
        } else {
            // add a StaticFragment to the RestIF model named root and set its layout
            ControllerFactory.INSTANCE.getRestTreeController().createRoot(restIF, "/root", 532.0f, 30.0f);

            // draw root
            addRoot();
        }
    }

    // this method displays the generated root of a new RestIF model
    private void addRoot() {
        PathFragment root = restIF.getRoot();

        LayoutElement rootLayoutElement = layout == null ? null : layout.getValue().get(root);
        addPathFragment(root, rootLayoutElement);
        PathFragmentView rootPathFragmentView = pathFragmentToViewMap.get(root);

        Resource rootResource = getResource(root);

        // create a new layout element and set the x and y values right below the path fragment view
        LayoutElement rootResourceLayoutElement = RestifFactoryImpl.init().createLayoutElement();
        rootResourceLayoutElement.setX(rootPathFragmentView.getX());
        rootResourceLayoutElement.setY(rootPathFragmentView.getY() + rootPathFragmentView.getHeight());

        addResource(rootResource, rootResourceLayoutElement, rootPathFragmentView, false);
    }

    private boolean isExistInRestIFModel(Resource checkedResource) {
        EList<Resource> resources = restIF.getResource();
        if (resources != null) {
            for (Resource resource : resources) {
                if (resource.equals(checkedResource)) {
                    return true;
                }
            }
        }
        return false;
    }

    // this helper method returns the resource linked to the path fragment and returns a dummy resource if there is no
    // resource linked to a path fragment
    public Resource getResource(PathFragment pathFragment) {
        EList<Resource> resources = restIF.getResource();
        if (resources != null) {
            for (Resource resource : resources) {
                if (pathFragment.equals(resource.getEndpoint())) {
                    return resource;
                }
            }
        }
        Resource resource = RestifFactoryImpl.eINSTANCE.createResource();
        resource.setEndpoint(pathFragment);

        return resource;
    }

    @Override
    public void notifyChanged(Notification notification) {
        if (notification.getFeature() == RestifPackage.Literals.PATH_FRAGMENT__CHILD) {
            PathFragment pathFragment = null;

            switch (notification.getEventType()) {
                case Notification.ADD:
                    pathFragment = (PathFragment) notification.getNewValue();
                    addPathFragment(pathFragment, layout.getValue().get(pathFragment));

                    PathFragmentView pathFragmentView = pathFragmentToViewMap.get(pathFragment);

                    // add the child to the parent's children arraylist
                    pathFragmentToViewMap.get(getParent(pathFragment)).addChildPathFragmentView(pathFragmentView);

                    Resource resource = getResource(pathFragment);

                    // create a new layout element and set the x and y values right below the path fragment view
                    LayoutElement resourceLayoutElement = RestifFactoryImpl.init().createLayoutElement();
                    resourceLayoutElement.setX(pathFragmentView.getX());
                    resourceLayoutElement.setY(pathFragmentView.getY() + pathFragmentView.getHeight());

                    addResource(resource, resourceLayoutElement, pathFragmentView, false);

                    PathFragmentView parent = pathFragmentToViewMap.get(getParent(pathFragment));
                    if (parent != null) {
                        drawLine(pathFragmentView, parent);
                    }

                    break;
                case Notification.REMOVE:
                    pathFragment = (PathFragment) notification.getOldValue();
                    // deleteChildren(pathFragment);
                    deletePathFragment(pathFragment);
                    break;
            }
        } else if (notification.getFeature() == RestifPackage.Literals.CONTAINER_MAP__VALUE) {
            if (notification.getEventType() == Notification.ADD) {
                ElementMapImpl elementMap = (ElementMapImpl) notification.getNewValue();

                if (elementMap.getKey() instanceof PathFragment) {
                    pathFragmentToViewMap.get(elementMap.getKey()).setLayoutElement(elementMap.getValue());

                    // draw the line connecting the parent and the child
                    PathFragmentView child = pathFragmentToViewMap.get(elementMap.getKey());
                    PathFragmentView parent =
                            pathFragmentToViewMap.get(getParent((PathFragment) elementMap.getKey()));

                    if (parent != null) {
                        updateLine(child, parent);
                    }
                }
            }
        } else if (notification.getFeature() == RestifPackage.Literals.REST_IF__ROOT) {
            switch (notification.getEventType()) {
                case Notification.SET:
                    if (notification.getOldValue() != null) {
                        PathFragment root = (PathFragment) notification.getOldValue();
                        deletePathFragment(root);
                    } else {
                        addRoot();
                    }
                    break;
            }
        }
    }

    // private void deleteChildren(PathFragment pathFragment) {
    // ArrayList<PathFragment> parents = new ArrayList<>(Arrays.asList(pathFragment));
    // ArrayList<PathFragment> newParents;
    //
    // do {
    // newParents = new ArrayList<>();
    // for (PathFragment parent : parents) {
    // EList<PathFragment> children = parent.getChild();
    //
    // if (children != null) {
    // newParents.addAll(children);
    //
    // for (PathFragment child : parent.getChild()) {
    // deletePathFragment(child);
    // }
    // }
    // }
    // parents.clear();
    // parents.addAll(newParents);
    // }
    // while (parents.size() > 0);
    // }

    private void deletePathFragment(PathFragment pathFragment) {
        if (restIF.getRoot() != null) {
            pathFragmentToViewMap.get(getDeletedParentOf(pathFragment)).removeChildPathFragmentView(
                    pathFragmentToViewMap.get(pathFragment));
        }
        PathFragmentView pathFragmentView = pathFragmentToViewMap.remove(pathFragment);
        selectedElements.remove(pathFragmentView);
        if (pathFragmentView.getLineToParent() != null) {
            containerLayer.removeChild(pathFragmentView.getLineToParent());
        }
        removeChild(pathFragmentView);
        pathFragmentView.destroy();
    }

    public PathFragment getDeletedParentOf(PathFragment deletedPathFragment) {
        ArrayList<PathFragment> parents = new ArrayList<>(Arrays.asList(restIF.getRoot()));
        ArrayList<PathFragment> newParents;

        do {
            newParents = new ArrayList<>();
            for (PathFragment parent : parents) {
                ArrayList<PathFragment> children = new ArrayList<>();

                for (PathFragmentView view : pathFragmentToViewMap.get(parent).getChildrenView()) {
                    children.add(view.getPathFragment());

                    if (view.getPathFragment().equals(deletedPathFragment)) {
                        return parent;
                    }
                }

                newParents.addAll(children);
            }
            parents.clear();
            parents.addAll(newParents);
        }
        while (parents.size() > 0);

        return null;
    }

    private void drawLine(PathFragmentView child, PathFragmentView parent) {
        // set the x and y of the parent section of the line as the middle button point of the rectangle
        float xParent = parent.getX() + parent.getWidth() / 2;
        float yParent = parent.getY() + parent.getHeight();

        // set the x and y of the child section of the line as the middle top point of the rectangle
        float xChild = child.getX() + child.getWidth() / 2;
        float yChild = child.getY();

        RamLineComponent line = new RamLineComponent(xParent, yParent, xChild, yChild);

        child.setLineToParent(line);
        containerLayer.addChild(line);
    }

    @SuppressWarnings("static-method")
    private void updateLine(PathFragmentView child, PathFragmentView parent) {
        // set the x and y of the parent section of the line as the middle button point of the rectangle
        float xParent = parent.getX() + parent.getWidth() / 2;
        float yParent = parent.getY() + parent.getHeight();

        // set the x and y of the child section of the line as the middle top point of the rectangle
        float xChild = child.getX() + child.getWidth() / 2;
        float yChild = child.getY();

        Vertex[] vertices = { new Vertex(xParent, yParent), new Vertex(xChild, yChild) };

        child.getLineToParent().setVertices(vertices);
    }

    public PathFragment getParent(PathFragment pathFragment) {
        ArrayList<PathFragment> parents = new ArrayList<>(Arrays.asList(restIF.getRoot()));
        ArrayList<PathFragment> newParents;

        do {
            newParents = new ArrayList<>();
            for (PathFragment parent : parents) {
                EList<PathFragment> children = parent.getChild();

                if (children != null) {
                    newParents.addAll(children);

                    for (PathFragment child : parent.getChild()) {
                        if (child.equals(pathFragment)) {
                            return parent;
                        }
                    }
                }
            }
            parents.clear();
            parents.addAll(newParents);
        }
        while (parents.size() > 0);

        return null;
    }

    public LinkedHashMap<PathFragment, LayoutElement> getChildrenToLayoutElementMap(PathFragment pathFragment) {
        ArrayList<PathFragment> parents = new ArrayList<>(Arrays.asList(pathFragment));
        ArrayList<PathFragment> newParents;
        LinkedHashMap<PathFragment, LayoutElement> childrenMap = new LinkedHashMap<>();

        do {
            newParents = new ArrayList<>();
            for (PathFragment parent : parents) {
                EList<PathFragment> children = parent.getChild();

                if (children != null) {
                    newParents.addAll(children);

                    for (PathFragment child : parent.getChild()) {
                        LayoutElement layoutElement = RestifFactory.eINSTANCE.createLayoutElement();

                        layoutElement.setX(getPathFragmentViewOf(child).getX());
                        layoutElement.setY(getPathFragmentViewOf(child).getY());

                        childrenMap.put(child, layoutElement);
                    }
                }
            }
            parents.clear();
            parents.addAll(newParents);
        }
        while (parents.size() > 0);

        return childrenMap;
    }

    // this method returns a hashmap of of all children of pathFragment
    // and their updated parents after the parent is removed
    public LinkedHashMap<PathFragment, PathFragment> getChildrenToParentEditedMap(PathFragment pathFragment) {
        ArrayList<PathFragment> parents = new ArrayList<>(Arrays.asList(pathFragment));
        ArrayList<PathFragment> newParents;
        LinkedHashMap<PathFragment, PathFragment> childrenMap = new LinkedHashMap<>();

        do {
            newParents = new ArrayList<>();
            for (PathFragment parent : parents) {
                EList<PathFragment> children = parent.getChild();

                if (children != null) {
                    newParents.addAll(children);

                    for (PathFragment child : parent.getChild()) {
                        if (parent.equals(pathFragment)) {
                            childrenMap.put(child, getParent(pathFragment));
                        } else {
                            childrenMap.put(child, parent);
                        }
                    }
                }
            }
            parents.clear();
            parents.addAll(newParents);
        }
        while (parents.size() > 0);

        return childrenMap;
    }

    // this method returns a hashmap of of all children of pathFragment and their parents
    public LinkedHashMap<PathFragment, PathFragment> getChildrenToParentMap(PathFragment pathFragment) {
        ArrayList<PathFragment> parents = new ArrayList<>(Arrays.asList(pathFragment));
        ArrayList<PathFragment> newParents;
        LinkedHashMap<PathFragment, PathFragment> childrenMap = new LinkedHashMap<>();

        do {
            newParents = new ArrayList<>();
            for (PathFragment parent : parents) {
                EList<PathFragment> children = parent.getChild();

                if (children != null) {
                    newParents.addAll(children);

                    for (PathFragment child : parent.getChild()) {
                        childrenMap.put(child, parent);
                    }
                }
            }
            parents.clear();
            parents.addAll(newParents);
        }
        while (parents.size() > 0);

        return childrenMap;
    }

    private void addResource(Resource resource, LayoutElement layoutElement,
            PathFragmentView parentView, boolean existInRestIFModel) {
        ResourceView resourceView = new ResourceView(this, resource, layoutElement, parentView, existInRestIFModel);
        resourceToViewMap.put(resource, resourceView);
        // add the resource view as a child to the path fragment view
        parentView.addChildView(resourceView);
        resourceView.setHandler(RestTreeHandlerFactory.INSTANCE.getResourceViewHandler());
    }

    private void addPathFragment(PathFragment pathFragment, LayoutElement layoutElement) {
        PathFragmentView pathFragmentView = new PathFragmentView(this, pathFragment, layoutElement);
        pathFragmentToViewMap.put(pathFragment, pathFragmentView);
        addChild(pathFragmentView);
        pathFragmentView.setHandler(RestTreeHandlerFactory.INSTANCE.getPathFragmentViewHandler());
    }

    /**
     * Removes the given {@link BaseView} from the list of selected views.
     * 
     * @param baseView the {@link BaseView} to remove
     */
    protected void elementDeselected(BaseView<?> baseView) {
        selectedElements.remove(baseView);
    }

    /**
     * Adds the given {@link BaseView} to the list of selected views.
     * 
     * @param baseView the baseView to add to the selection
     */
    protected void elementSelected(BaseView<?> baseView) {
        selectedElements.add(baseView);
    }

    @Override
    protected void registerGestureListeners(IGestureEventListener listener) {
        super.registerGestureListeners(listener);

        addGestureListener(TapProcessor.class, listener);
        addGestureListener(TapAndHoldProcessor.class, listener);
        addGestureListener(TapAndHoldProcessor.class, new TapAndHoldVisualizer(RamApp.getApplication(), getParent()));
    }

    @Override
    protected void registerInputProcessors() {
        registerInputProcessor(new TapProcessor(RamApp.getApplication(), GUIConstants.TAP_MAX_FINGER_UP, false,
                GUIConstants.TAP_DOUBLE_TAP_TIME));
        registerInputProcessor(new TapAndHoldProcessor(RamApp.getApplication(), GUIConstants.TAP_AND_HOLD_DURATION));
        registerInputProcessor(new PanProcessorTwoFingers(RamApp.getApplication()));
        registerInputProcessor(new RightClickDragProcessor(RamApp.getApplication()));
        registerInputProcessor(new ZoomProcessor(RamApp.getApplication()));
        registerInputProcessor(new MouseWheelProcessor(RamApp.getApplication()));

        UnistrokeProcessorLeftClick up = new UnistrokeProcessorLeftClick(RamApp.getApplication());

        registerInputProcessor(up);
    }

    public RestIF getRestIF() {
        return restIF;
    }

    public Collection<BaseView<?>> getBaseViews() {
        Collection<BaseView<?>> baseViews = new HashSet<BaseView<?>>();

        baseViews.addAll(pathFragmentToViewMap.values());

        return baseViews;
    }

    public Set<BaseView<?>> getSelectedElements() {
        return selectedElements;
    }

    /**
     * Deselects all currently selected classifiers.
     */
    public void deselect() {
        // Use separate set here, otherwise a concurrent modification occurs,
        // because the view notifies us that it was deselected, which triggers
        // the removal of the view from our set.
        for (BaseView<?> baseView : new HashSet<BaseView<?>>(selectedElements)) {
            baseView.setSelect(false);
        }

        selectedElements.clear();
    }

    public PathFragmentView liesAround(Vector3D position) {
        for (PathFragmentView pathFragmentView : pathFragmentToViewMap.values()) {
            if (MathUtils.pointIsInRectangle(position, pathFragmentView, GUIConstants.MARGIN_ELEMENT_DETECTION)) {
                return pathFragmentView;
            }
        }
        return null;
    }

    /**
     * Gets the {@link PathFragmentView} of the specified PathFragment.
     * 
     * @param pathFragment the PathFragment element for which we want to get the path fragment view
     * @return {@link PathFragmentView}
     */
    public PathFragmentView getPathFragmentViewOf(PathFragment pathFragment) {
        return pathFragmentToViewMap.get(pathFragment);
    }

    // this method returns the height of a specific path fragment in the restIF model
    public int findHeight(PathFragment pathFragment) {
        int height = 1;

        ArrayList<PathFragment> parents = new ArrayList<>(Arrays.asList(restIF.getRoot()));
        ArrayList<PathFragment> newParents;

        do {
            newParents = new ArrayList<>();
            for (PathFragment parent : parents) {
                EList<PathFragment> children = parent.getChild();

                if (children != null) {
                    newParents.addAll(children);

                    for (PathFragment child : parent.getChild()) {
                        if (child.equals(pathFragment)) {
                            return height;
                        }
                    }
                }
            }
            height++;
            parents.clear();
            parents.addAll(newParents);
        }
        while (parents.size() > 0);

        // it will return 0 when pathFragment is the root
        return 0;
    }

    // this method returns the y coordinate of a child of a specific height, if it DNE, it returns -1
    public float findYForChildAtSameHeight(int height) {
        int currentHeight = 1;

        ArrayList<PathFragment> parents = new ArrayList<>(Arrays.asList(restIF.getRoot()));
        ArrayList<PathFragment> newParents;

        do {
            newParents = new ArrayList<>();
            for (PathFragment parent : parents) {
                EList<PathFragment> children = parent.getChild();

                if (children != null) {
                    newParents.addAll(children);

                    for (PathFragment child : parent.getChild()) {
                        if (currentHeight == height) {
                            return getPathFragmentViewOf(child).getY();
                        }
                    }
                }
            }
            currentHeight++;
            parents.clear();
            parents.addAll(newParents);
        }
        while (parents.size() > 0); 
        return -1;
    }

    // this method is to help output restif model during development
    // TODO: delete this method when the rest tree is complete
    void outputRestIFModel() {
        System.out.println("displaying restIF model...");
        System.out.println("root: " + restIF.getRoot());

        ArrayList<PathFragment> parents = new ArrayList<>(Arrays.asList(restIF.getRoot()));
        ArrayList<PathFragment> newParents;

        do {
            newParents = new ArrayList<>();
            for (PathFragment parent : parents) {
                EList<PathFragment> children = parent.getChild();

                if (children != null) {
                    newParents.addAll(children);

                    for (PathFragment child : parent.getChild()) {
                        System.out.println("child: " + child + " parent: " + parent);
                    }
                }
            }
            parents.clear();
            parents.addAll(newParents);
        }
        while (parents.size() > 0);
    }
}
