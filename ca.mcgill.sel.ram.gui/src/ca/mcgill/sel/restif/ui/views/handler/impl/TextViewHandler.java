package ca.mcgill.sel.restif.ui.views.handler.impl;

import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;

import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.BaseHandler;
import ca.mcgill.sel.ram.ui.views.handler.ITextViewHandler;
import ca.mcgill.sel.restif.DynamicFragment;
import ca.mcgill.sel.restif.PathFragment;
import ca.mcgill.sel.restif.StaticFragment;
import ca.mcgill.sel.restif.language.controller.ControllerFactory;

public class TextViewHandler extends BaseHandler implements ITextViewHandler {
    @Override
    public void keyboardCancelled(TextView textView) {
        textView.updateText();        
    }

    @Override
    public void keyboardOpened(TextView textView) {
    }
    
    @Override
    public boolean processTapEvent(TapEvent tapEvent) {
        return false;
    }

    @Override
    public boolean shouldDismissKeyboard(TextView textView) {
        String name = textView.getText();
        PathFragment pathFragment = null;
        
        if (textView.getData() instanceof StaticFragment) {
            pathFragment = (StaticFragment) textView.getData();
        } else if (textView.getData() instanceof DynamicFragment) {
            pathFragment = (DynamicFragment) textView.getData();
        }
        
        ControllerFactory.INSTANCE.getRestTreeController().setPathFragmentName(pathFragment, name);
                        
        return true;
    }
}
