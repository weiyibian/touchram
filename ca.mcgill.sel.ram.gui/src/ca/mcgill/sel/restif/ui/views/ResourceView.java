package ca.mcgill.sel.restif.ui.views;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;

import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamImageComponent;
import ca.mcgill.sel.ram.ui.layouts.HorizontalLayout;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.restif.AccessMethod;
import ca.mcgill.sel.restif.LayoutElement;
import ca.mcgill.sel.restif.MethodType;
import ca.mcgill.sel.restif.Resource;
import ca.mcgill.sel.restif.ui.components.ResourceRamButton;
import ca.mcgill.sel.restif.ui.views.handler.IResourceViewHandler;
import ca.mcgill.sel.restif.ui.views.handler.RestTreeHandlerFactory;
import ca.mcgill.sel.restif.ui.views.handler.impl.ResourceButtonHandler;

public class ResourceView extends LinkableView<IResourceViewHandler> {

    private ResourceButtonHandler resourceButtonHandler;

    private ResourceRamButton getButton;
    private ResourceRamButton putButton;
    private ResourceRamButton postButton;
    private ResourceRamButton deleteButton;
    
    // this variable represents whether or not the represented Resource is in the RestIF model or not
    private boolean existInRestIFModel;

    protected ResourceView(RestTreeView restTreeView, Resource resource, LayoutElement layoutElement,
            PathFragmentView parentView, boolean existInRestIFModel) {
        super(restTreeView, resource, layoutElement);

        this.existInRestIFModel = existInRestIFModel;
        
        setMinimumHeight(parentView.getHeight());
        setMinimumWidth(parentView.getWidth());
        setAnchor(PositionAnchor.UPPER_LEFT);

        setLayout(new HorizontalLayout());

        setStrokeColor(Colors.DEFAULT_ELEMENT_STROKE_COLOR);
        setNoStroke(false);

        addAccessMethods();
    }

    // TODO: shift buttons to the left, there is a buffer for some reason
    @SuppressWarnings("static-access")
    private void addAccessMethods() {
        EList<AccessMethod> accessMethods = ((Resource) this.represented).getAccessmethod();
        boolean getFound = false;
        boolean putFound = false;
        boolean postFound = false;
        boolean deleteFound = false;
        resourceButtonHandler = (ResourceButtonHandler) RestTreeHandlerFactory.INSTANCE.getResourceButtonHandler();

        if (accessMethods != null) {
            for (AccessMethod accessMethod : accessMethods) {
                if (accessMethod.getType() == MethodType.GET) {
                    getFound = true;
                } else if (accessMethod.getType() == MethodType.PUT) {
                    putFound = true;
                } else if (accessMethod.getType() == MethodType.POST) {
                    postFound = true;
                } else {
                    deleteFound = true;
                }
            }
        }

        if (getFound) {
            getButton = new ResourceRamButton(new RamImageComponent(Icons.ICON_MENU_GET,
                    Colors.DEFAULT_ELEMENT_FILL_COLOR), (int) this.getHeight());
            getButton.setActionCommand(resourceButtonHandler.getActionResourceGetRemove());
        } else {
            getButton = new ResourceRamButton(new RamImageComponent(Icons.ICON_MENU_ADD_RESOURCE,
                    Colors.DEFAULT_ELEMENT_FILL_COLOR), (int) this.getHeight());
            getButton.setActionCommand(resourceButtonHandler.getActionResourceGetAdd());
        }
        if (putFound) {
            putButton = new ResourceRamButton(new RamImageComponent(Icons.ICON_MENU_PUT,
                    Colors.DEFAULT_ELEMENT_FILL_COLOR), (int) this.getHeight());
            putButton.setActionCommand(resourceButtonHandler.getActionResourcePutRemove());
        } else {
            putButton = new ResourceRamButton(new RamImageComponent(Icons.ICON_MENU_ADD_RESOURCE,
                    Colors.DEFAULT_ELEMENT_FILL_COLOR), (int) this.getHeight());
            putButton.setActionCommand(resourceButtonHandler.getActionResourcePutAdd());
        }
        if (postFound) {
            postButton = new ResourceRamButton(new RamImageComponent(Icons.ICON_MENU_POST,
                    Colors.DEFAULT_ELEMENT_FILL_COLOR), (int) this.getHeight());
            postButton.setActionCommand(resourceButtonHandler.getActionResourcePostRemove());
        } else {
            postButton = new ResourceRamButton(new RamImageComponent(Icons.ICON_MENU_ADD_RESOURCE,
                    Colors.DEFAULT_ELEMENT_FILL_COLOR), (int) this.getHeight());
            postButton.setActionCommand(resourceButtonHandler.getActionResourcePostAdd());
        }
        if (deleteFound) {
            deleteButton = new ResourceRamButton(new RamImageComponent(Icons.ICON_MENU_DELETE,
                    Colors.DEFAULT_ELEMENT_FILL_COLOR), (int) this.getHeight());
            deleteButton.setActionCommand(resourceButtonHandler.getActionResourceDeleteRemove());
        } else {
            deleteButton = new ResourceRamButton(new RamImageComponent(Icons.ICON_MENU_ADD_RESOURCE,
                    Colors.DEFAULT_ELEMENT_FILL_COLOR), (int) this.getHeight());
            deleteButton.setActionCommand(resourceButtonHandler.getActionResourceDeleteAdd());
        }
        
        getButton.addActionListener(resourceButtonHandler);
        putButton.addActionListener(resourceButtonHandler);
        postButton.addActionListener(resourceButtonHandler);
        deleteButton.addActionListener(resourceButtonHandler);

        addChild(getButton);
        addChild(putButton);
        addChild(postButton);
        addChild(deleteButton);
    }

    @Override
    public void notifyChanged(Notification notification) {
        if (notification.getNotifier() == this.getResource()) {
            AccessMethod accessMethod = null;
            switch (notification.getEventType()) {
                case Notification.ADD:
                    accessMethod = (AccessMethod) notification.getNewValue();

                    if (accessMethod.getType() == MethodType.GET) {
                        addGetResource();
                    } else if (accessMethod.getType() == MethodType.PUT) {
                        addPutResource();
                    } else if (accessMethod.getType() == MethodType.POST) {
                        addPostResource();
                    } else if (accessMethod.getType() == MethodType.DELETE) {
                        addDeleteResource();
                    }
                    break;
                case Notification.REMOVE:
                    accessMethod = (AccessMethod) notification.getOldValue();

                    if (accessMethod.getType() == MethodType.GET) {
                        removeGetResource();
                    } else if (accessMethod.getType() == MethodType.PUT) {
                        removePutResource();
                    } else if (accessMethod.getType() == MethodType.POST) {
                        removePostResource();
                    } else if (accessMethod.getType() == MethodType.DELETE) {
                        removeDeleteResource();
                    }
                    break;
            }
        }
    }

    private void addGetResource() {
        RamApp.getApplication().invokeLater(new Runnable() {

            @SuppressWarnings("static-access")
            @Override
            public void run() {
                getButton.setIcon(new RamImageComponent(Icons.ICON_MENU_GET,
                        Colors.DEFAULT_ELEMENT_FILL_COLOR));
                getButton.setActionCommand(resourceButtonHandler.getActionResourceGetRemove());
            }
        });
    }

    private void removeGetResource() {
        RamApp.getApplication().invokeLater(new Runnable() {

            @SuppressWarnings("static-access")
            @Override
            public void run() {
                getButton.setIcon(new RamImageComponent(Icons.ICON_MENU_ADD_RESOURCE,
                        Colors.DEFAULT_ELEMENT_FILL_COLOR));
                getButton.setActionCommand(resourceButtonHandler.getActionResourceGetAdd());
            }
        });
    }

    private void addPutResource() {
        RamApp.getApplication().invokeLater(new Runnable() {

            @SuppressWarnings("static-access")
            @Override
            public void run() {
                putButton.setIcon(new RamImageComponent(Icons.ICON_MENU_PUT,
                        Colors.DEFAULT_ELEMENT_FILL_COLOR));
                putButton.setActionCommand(resourceButtonHandler.getActionResourcePutRemove());
            }
        });
    }

    private void removePutResource() {
        RamApp.getApplication().invokeLater(new Runnable() {

            @SuppressWarnings("static-access")
            @Override
            public void run() {
                putButton.setIcon(new RamImageComponent(Icons.ICON_MENU_ADD_RESOURCE,
                        Colors.DEFAULT_ELEMENT_FILL_COLOR));
                putButton.setActionCommand(resourceButtonHandler.getActionResourcePutAdd());
            }
        });
    }

    private void addPostResource() {
        RamApp.getApplication().invokeLater(new Runnable() {

            @SuppressWarnings("static-access")
            @Override
            public void run() {
                postButton.setIcon(new RamImageComponent(Icons.ICON_MENU_POST,
                        Colors.DEFAULT_ELEMENT_FILL_COLOR));
                postButton.setActionCommand(resourceButtonHandler.getActionResourcePostRemove());
            }
        });
    }

    private void removePostResource() {
        RamApp.getApplication().invokeLater(new Runnable() {

            @SuppressWarnings("static-access")
            @Override
            public void run() {
                postButton.setIcon(new RamImageComponent(Icons.ICON_MENU_ADD_RESOURCE,
                        Colors.DEFAULT_ELEMENT_FILL_COLOR));
                postButton.setActionCommand(resourceButtonHandler.getActionResourcePostAdd());
            }
        });
    }

    private void addDeleteResource() {
        RamApp.getApplication().invokeLater(new Runnable() {

            @SuppressWarnings("static-access")
            @Override
            public void run() {
                deleteButton.setIcon(new RamImageComponent(Icons.ICON_MENU_DELETE,
                        Colors.DEFAULT_ELEMENT_FILL_COLOR));
                deleteButton.setActionCommand(resourceButtonHandler.getActionResourceDeleteRemove());
            }
        });
    }

    private void removeDeleteResource() {
        RamApp.getApplication().invokeLater(new Runnable() {

            @SuppressWarnings("static-access")
            @Override
            public void run() {
                deleteButton.setIcon(new RamImageComponent(Icons.ICON_MENU_ADD_RESOURCE,
                        Colors.DEFAULT_ELEMENT_FILL_COLOR));
                deleteButton.setActionCommand(resourceButtonHandler.getActionResourceDeleteAdd());
            }
        });
    }

    public Resource getResource() {
        return (Resource) this.represented;
    }
    
    public boolean isExistInRestIFModel() {
        return existInRestIFModel;
    }

    public void setExistInRestIFModel(boolean existInRestIFModel) {
        this.existInRestIFModel = existInRestIFModel;
    }

    @Override
    public void destroy() {
        this.destroyAllChildren();
        super.destroy();
    }
}
