package ca.mcgill.sel.restif.ui.views.handler.impl;

import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.events.listeners.ActionListener;
import ca.mcgill.sel.restif.MethodType;
import ca.mcgill.sel.restif.language.controller.ControllerFactory;
import ca.mcgill.sel.restif.ui.views.ResourceView;
import ca.mcgill.sel.restif.ui.views.RestTreeView;

public class ResourceButtonHandler implements ActionListener {

    private static final String ACTION_RESOURCE_GET_ADD = "view.resource.get.add";
    private static final String ACTION_RESOURCE_GET_REMOVE = "view.resource.get.remove";
    private static final String ACTION_RESOURCE_PUT_ADD = "view.resource.put.add";
    private static final String ACTION_RESOURCE_PUT_REMOVE = "view.resource.put.remove";
    private static final String ACTION_RESOURCE_POST_ADD = "view.resource.post.add";
    private static final String ACTION_RESOURCE_POST_REMOVE = "view.resource.post.remove";
    private static final String ACTION_RESOURCE_DELETE_ADD = "view.resource.delete.add";
    private static final String ACTION_RESOURCE_DELETE_REMOVE = "view.resource.delete.remove";

    public static String getActionResourceGetAdd() {
        return ACTION_RESOURCE_GET_ADD;
    }

    public static String getActionResourceGetRemove() {
        return ACTION_RESOURCE_GET_REMOVE;
    }

    public static String getActionResourcePutAdd() {
        return ACTION_RESOURCE_PUT_ADD;
    }

    public static String getActionResourcePutRemove() {
        return ACTION_RESOURCE_PUT_REMOVE;
    }

    public static String getActionResourcePostAdd() {
        return ACTION_RESOURCE_POST_ADD;
    }

    public static String getActionResourcePostRemove() {
        return ACTION_RESOURCE_POST_REMOVE;
    }

    public static String getActionResourceDeleteAdd() {
        return ACTION_RESOURCE_DELETE_ADD;
    }

    public static String getActionResourceDeleteRemove() {
        return ACTION_RESOURCE_DELETE_REMOVE;
    }
    
    @Override
    public void actionPerformed(ActionEvent event) {
        String actionCommand = event.getActionCommand();
        
        RamRectangleComponent pressedButton = (RamRectangleComponent) event.getTarget();
        
        ResourceView resourceView = pressedButton.getParentOfType(ResourceView.class);
                
        // if the Resource does not exist in Restif model, add it to Restif model to perform commands on Resource
        if (!resourceView.isExistInRestIFModel()) {
            ControllerFactory.INSTANCE.getRestTreeController().addResource(
                    resourceView.getParentOfType(RestTreeView.class).getRestIF(), resourceView.getResource());
            
            resourceView.setExistInRestIFModel(true);
        }
        
        if (ACTION_RESOURCE_GET_ADD.equals(actionCommand)) {
            ControllerFactory.INSTANCE.getRestTreeController().addAccessMethodToResource(
                    resourceView.getResource(), MethodType.GET);
        } else if (ACTION_RESOURCE_GET_REMOVE.equals(actionCommand)) {
            ControllerFactory.INSTANCE.getRestTreeController().removeAccessMethodFromResource(
                    resourceView.getResource(), MethodType.GET);
        } else if (ACTION_RESOURCE_PUT_ADD.equals(actionCommand)) {
            ControllerFactory.INSTANCE.getRestTreeController().addAccessMethodToResource(
                    resourceView.getResource(), MethodType.PUT);
        } else if (ACTION_RESOURCE_PUT_REMOVE.equals(actionCommand)) {
            ControllerFactory.INSTANCE.getRestTreeController().removeAccessMethodFromResource(
                    resourceView.getResource(), MethodType.PUT);
        } else if (ACTION_RESOURCE_POST_ADD.equals(actionCommand)) {
            ControllerFactory.INSTANCE.getRestTreeController().addAccessMethodToResource(
                    resourceView.getResource(), MethodType.POST);
        } else if (ACTION_RESOURCE_POST_REMOVE.equals(actionCommand)) {
            ControllerFactory.INSTANCE.getRestTreeController().removeAccessMethodFromResource(
                    resourceView.getResource(), MethodType.POST);
        } else if (ACTION_RESOURCE_DELETE_ADD.equals(actionCommand)) {
            ControllerFactory.INSTANCE.getRestTreeController().addAccessMethodToResource(
                    resourceView.getResource(), MethodType.DELETE);
        } else if (ACTION_RESOURCE_DELETE_REMOVE.equals(actionCommand)) {
            ControllerFactory.INSTANCE.getRestTreeController().removeAccessMethodFromResource(
                    resourceView.getResource(), MethodType.DELETE);
        }
    }
}
