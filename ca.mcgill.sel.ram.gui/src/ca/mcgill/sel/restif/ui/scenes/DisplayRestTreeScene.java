package ca.mcgill.sel.restif.ui.scenes;

import java.util.Collection;
import java.util.Stack;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.emf.edit.provider.INotifyChangedListener;
import org.mt4j.components.MTComponent;
import org.mt4j.components.visibleComponents.shapes.MTRectangle.PositionAnchor;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.commons.LoggerUtils;
import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.ConfirmPopup;
import ca.mcgill.sel.ram.ui.components.ConfirmPopup.OptionType;
import ca.mcgill.sel.ram.ui.components.RamPanelComponent;
import ca.mcgill.sel.ram.ui.components.listeners.RamPanelListener;
import ca.mcgill.sel.ram.ui.scenes.RamAbstractScene;
import ca.mcgill.sel.ram.ui.utils.GraphicalUpdater;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.handler.IDisplaySceneHandler;
import ca.mcgill.sel.restif.RestIF;
import ca.mcgill.sel.restif.RestifPackage;
import ca.mcgill.sel.restif.impl.ContainerMapImpl;
import ca.mcgill.sel.restif.ui.views.PathFragmentView;
import ca.mcgill.sel.restif.ui.views.RestTreeView;
import ca.mcgill.sel.restif.ui.views.handler.RestTreeHandlerFactory;
import ca.mcgill.sel.restif.util.RifModelUtil;

public class DisplayRestTreeScene extends RamAbstractScene<IDisplaySceneHandler>
        implements INotifyChangedListener, RamPanelListener {

    private RestIF restIF;
    private COREArtefact artefact;
    private RestTreeView restTreeView;

    private GraphicalUpdater graphicalUpdater;

    private MTComponent viewContainer;
    private Stack<MTComponent> previousViews;
    private MTComponent currentView;

    public DisplayRestTreeScene(RamApp app, COREExternalArtefact artefact, RestIF restIF, String sceneName) {
        // Calling the constructor of the Abstract scene with the name of the concern
        super(app, sceneName);
        this.restIF = restIF;
        this.artefact = artefact;

        this.handler = RestTreeHandlerFactory.INSTANCE.getDisplayRestTreeSceneHandler();

        // views are added into this layer
        viewContainer = new MTComponent(app, "view container");
        getContainerLayer().addChild(viewContainer);

        // Graphical Updater linked to this aspect
        graphicalUpdater = RamApp.getApplication().getGraphicalUpdaterForModel(restIF);

        buildViews();

        EMFEditUtil.addListenerFor(restIF, this);

        // the following registers the artefact to listen to all changes to it's content, which
        // is all the changes to contained COREModelExtensions, COREModelReuses, and CORELinks
        // whenever a new mapping is created, deleted, or when the "to" element of a mapping changes,
        // the graphical updated is sent a referenceEvent so it can trigger updateStyle changes on all
        // registered views so that they can change to their "ghost" background color, if needed
        this.artefact.eAdapters().add(new EContentAdapter() {
            @Override
            public void notifyChanged(Notification notification) {
                System.out.println(notification);
                super.notifyChanged(notification);
                if (notification.getFeature() == CorePackage.Literals.CORE_LINK__TO) {
                    // the model element of a "to" mapping was changed
                    // we need to trigger the updateStyle in all views representing the old element and the new
                    // element
                    if (notification.getOldValue() != null) {
                        graphicalUpdater.referenceEvent((EObject) notification.getOldValue());
                    }
                    if (notification.getNewValue() != null) {
                        graphicalUpdater.referenceEvent((EObject) notification.getNewValue());
                    }
                }
            }
        });

        // The manager of the validation thread responsible for its launch (TODO currently unavailable for classDiagram)
        // validatorLauncher = new ValidationManager(cd, new Validator(), graphicalUpdater);
        // showValidation(true);

        setCommandStackListener(restIF);

        repushSections();
    }

    /**
     * Used to refresh the sections in the navigation bar whenever the scene is called.
     */
    public void repushSections() {
        if (artefact.getScene() != null && artefact.getScene().getRealizes().size() > 0) {
            COREFeature feature = artefact.getScene().getRealizes().get(0);

            Collection<RestIF> restTrees = EMFModelUtil.collectElementsOfType(feature,
                    CorePackage.Literals.CORE_FEATURE__REALIZED_BY,
                    RestifPackage.Literals.REST_IF);
            restTrees.remove(this.restIF);
            if (restTrees.isEmpty()) {
                navbar.pushSectionJumpFeature(Icons.ICON_NAVIGATION_FEATURE,
                        feature.getName(), null, feature);
            } else {
                navbar.pushSectionJumpFeature(Icons.ICON_NAVIGATION_FEATURE,
                        feature.getName(), navbar.getFeatureAndConflict(), feature);
            }

            navbar.pushSectionJumpRM(Icons.ICON_NAVIGATION_ASPECT, this.restIF.getName(),
                    null, this.restIF);
        }
    }

    /**
     * Builds the structural, state and initial message views.
     */
    private void buildViews() {
        // create layout if it doesn't exist yet
        if (restIF.getLayout() == null) {
            RifModelUtil.createLayout(restIF);
        }

        ContainerMapImpl layout = EMFModelUtil.getEntryFromMap(restIF.getLayout().getContainers(),
                restIF);
        restTreeView = new RestTreeView(restIF, layout, getWidth(), getHeight());

        // we will get position of the upper left corner of the view instead of center.
        restTreeView.setAnchor(PositionAnchor.UPPER_LEFT);

        previousViews = new Stack<MTComponent>();
        switchToView(restTreeView);

        restTreeView.setHandler(RestTreeHandlerFactory.INSTANCE.getRestTreeViewHandler());
    }

    /**
     * Replaces the current view with the given view. Remembers the old current view in case the user wants to go back
     * to it.
     *
     * @param view
     *            the view to display
     * @see #switchToPreviousView()
     */
    public void switchToView(MTComponent view) {
        this.switchToView(view, true);
    }

    /**
     * Replaces the current view with the given view. Remembers the old current view in case the user wants to go back
     * to it.
     *
     * @param view
     *            the view to display
     * @param saveCurrent - whether we want to remember the current view as the previous one.
     * @see #switchToPreviousView()
     */
    public void switchToView(MTComponent view, boolean saveCurrent) {
        if (saveCurrent) {
            previousViews.push(currentView);
        }

        currentView = view;
        viewContainer.removeAllChildren();
        viewContainer.addChild(view);
        showMenu();
        clearTemporaryComponents();
    }

    private void showMenu() {
        // TODO Auto-generated method stub

    }

    public RestIF getRestTree() {
        return this.restIF;
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        if (handler != null) {
            // String actionCommand = event.getActionCommand();
            super.actionPerformed(event);
        } else {
            LoggerUtils.warn("No handler set for " + this.getClass().getName());
        }
    }

    @Override
    public void notifyChanged(Notification notification) {
        if (notification.getFeature() == CorePackage.Literals.CORE_ARTEFACT__CORE_CONCERN
                && notification.getEventType() == Notification.SET
                && notification.getNewValue() == null) {
            // Go back to the concern if the link with it is undone.
            handler.switchToConcern(this);
        }
    }

    @Override
    protected void initMenu() {
        // TODO Auto-generated method stub

    }

    @Override
    protected EObject getElementToSave() {
        // TODO Auto-generated method stub
        return restIF;
    }

    @Override
    public void panelClosed(RamPanelComponent panel) {
        // TODO Auto-generated method stub

    }

    public COREArtefact getArtefact() {
        return artefact;
    }

    /**
     * Shows a confirm popup for the given aspect to ask the user whether the aspect should be saved.
     *
     * @param parent
     *            the scene where the popup should be displayed, usually the current scene
     * @param listener
     *            the listener to inform which option the user selected
     */
    public void showCloseConfirmPopup(RamAbstractScene<?> parent, ConfirmPopup.SelectionListener listener) {
        showCloseConfirmPopup(parent, listener, OptionType.YES_NO_CANCEL);
    }

    /**
     * Shows a confirm popup for the given class diagram to ask the user whether it should be saved.
     *
     * @param parent the scene where the popup should be displayed, usually the current scene
     * @param listener the listener to inform which option the user selected
     * @param options the buttons to display in the popup
     */
    public void showCloseConfirmPopup(RamAbstractScene<?> parent, ConfirmPopup.SelectionListener listener,
            OptionType options) {
        String message = Strings.MODEL_ASPECT + " " + restIF.getName() + Strings.POPUP_MODIFIED_SAVE;
        ConfirmPopup saveConfirmPopup = new ConfirmPopup(message, options);
        saveConfirmPopup.setListener(listener);

        parent.displayPopup(saveConfirmPopup);
    }

    public void switchToPreviousView() {
        if (!previousViews.isEmpty()) {
            switchToView(previousViews.pop(), false);
        }
        navbar.popSection();
    }
    
    /**
     * Get the PathFragmentView closest to the position in the diagram, if there is one.
     *
     * @param position - The position to check
     * @return the closest {@link PathFragmentView} or null if there is no feature at this position
     */
    public PathFragmentView liesAround(Vector3D position) {
        return restTreeView.liesAround(position);
    }
}
