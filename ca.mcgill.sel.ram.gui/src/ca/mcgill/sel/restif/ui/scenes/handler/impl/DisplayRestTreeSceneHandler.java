package ca.mcgill.sel.restif.ui.scenes.handler.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.mt4j.sceneManagement.transition.SlideTransition;

import ca.mcgill.sel.commons.LoggerUtils;
import ca.mcgill.sel.commons.emf.util.CORECommandStack;
import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.core.util.COREModelUtil;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.ConfirmPopup;
import ca.mcgill.sel.ram.ui.components.RamPopup.PopupType;
import ca.mcgill.sel.ram.ui.components.browser.GenericFileBrowser;
import ca.mcgill.sel.ram.ui.components.browser.interfaces.FileBrowserListener;
import ca.mcgill.sel.ram.ui.components.navigationbar.NavigationBar;
import ca.mcgill.sel.ram.ui.components.navigationbar.SlideUpDownTransition;
import ca.mcgill.sel.ram.ui.layouts.AutomaticLayout;
import ca.mcgill.sel.ram.ui.layouts.Layout;
import ca.mcgill.sel.ram.ui.scenes.DisplayAspectScene;
import ca.mcgill.sel.ram.ui.scenes.DisplayConcernEditScene;
import ca.mcgill.sel.ram.ui.scenes.DisplayConcernSelectScene;
import ca.mcgill.sel.ram.ui.scenes.RamAbstractScene;
import ca.mcgill.sel.ram.ui.scenes.handler.impl.ConcernSelectSceneHandler;
import ca.mcgill.sel.ram.ui.utils.BasicActionsUtils;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.handler.IDisplaySceneHandler;
import ca.mcgill.sel.ram.ui.views.structural.CompositionView;
import ca.mcgill.sel.restif.RestIF;
import ca.mcgill.sel.restif.ui.scenes.DisplayRestTreeScene;
import ca.mcgill.sel.restif.ui.utils.RifModelUtils;

public class DisplayRestTreeSceneHandler extends DefaultRestTreeSceneHandler implements IDisplaySceneHandler {

    /**
     * A listener for a confirm popup.
     * Handles the user response on whether to save in case modifications occurred.
     */
    private final class SaveConfirmListener implements ConfirmPopup.SelectionListener {
        private final DisplayRestTreeScene scene;

        /**
         * Creates a new instance for the given scene.
         * 
         * @param scene the scene for this listener
         */
        private SaveConfirmListener(DisplayRestTreeScene scene) {
            this.scene = scene;
        }

        @Override
        public void optionSelected(int selectedOption) {
            if (selectedOption == ConfirmPopup.YES_OPTION) {
                BasicActionsUtils.saveModel(scene.getRestTree(), new FileBrowserListener() {
                    @Override
                    public void modelSaved(File file) {
                        NavigationBar.getInstance().popSection();
                        NavigationBar.popHistory();
                        if (scene.getPreviousScene() instanceof DisplayConcernSelectScene) {
                            DisplayConcernSelectScene prev = (DisplayConcernSelectScene) scene.getPreviousScene();
                            ConcernSelectSceneHandler prevH = (ConcernSelectSceneHandler) prev.getHandler();
                            prevH.switchToPreviousScene(prev);
                            NavigationBar.getInstance().wipeNavigationBar();

                        } else {
                            doSwitchToPreviousScene(scene);
                        }
                    }

                    @Override
                    public void modelLoaded(EObject model) {
                    }
                });
            } else if (selectedOption == ConfirmPopup.NO_OPTION) {

                if (EMFEditUtil.getCommandStack(scene.getRestTree()) instanceof CORECommandStack) {
                    CORECommandStack stack = (CORECommandStack) EMFEditUtil.getCommandStack(scene.getRestTree());

                    if (stack.canRevertToLastSave()) {
                        stack.goToLastSave();
                    } else {
                        scene.displayPopup(Strings.POPUP_REVERT_IMPOSSIBLE, PopupType.ERROR);
                        return;
                    }
                } else {
                    LoggerUtils.warn(Strings.invalidCommandStackInstance(scene.getArtefact()));
                }

                NavigationBar.getInstance().popSection();
                NavigationBar.popHistory();
                checkForModelRemoval(scene);

                if (scene.getPreviousScene() instanceof DisplayConcernSelectScene) {
                    DisplayConcernSelectScene prev = (DisplayConcernSelectScene) scene.getPreviousScene();
                    ConcernSelectSceneHandler prevH = (ConcernSelectSceneHandler) prev.getHandler();
                    prevH.switchToPreviousScene(prev);
                    NavigationBar.getInstance().wipeNavigationBar();

                } else {
                    doSwitchToPreviousScene(scene);
                }
            }

        }
    }

    @Override
    public void generate(RamAbstractScene<?> scene) {
    }

    @Override
    public void loadScene(RamAbstractScene<?> scene) {
     // Transition to the right

        // Ask the user to load a model
        GenericFileBrowser.loadModel("restif", new FileBrowserListener() {

            @Override
            public void modelLoaded(EObject model) {
                RamApp.getApplication().loadScene(COREArtefactUtil.getReferencingExternalArtefact(model), model);
            }

            @Override
            public void modelSaved(File file) {
            }
        });
    }

    @Override
    public void showValidation(RamAbstractScene<?> scene) {
    }

    @Override
    public void showTracing(RamAbstractScene<?> scene) {
    }

    @Override
    public void back(RamAbstractScene<?> scene) {
        ((DisplayRestTreeScene) scene).switchToPreviousView();
    }

    @Override
    public void switchToCompositionEditMode(RamAbstractScene<?> scene, CompositionView compositionView) {
        // TODO Auto-generated method stub

    }

    @Override
    public void switchToMenu(RamAbstractScene<?> scene) {
     // to the left!
        scene.setTransition(new SlideTransition(RamApp.getApplication(), 700, false));
        // go to SelectAspectScene
        RamApp.getApplication().switchToBackground((DisplayRestTreeScene) scene);
    }

    @Override
    public void weaveAll(RamAbstractScene<?> scene) {
    }

    @Override
    public void weaveStateMachines(RamAbstractScene<?> scene) {
    }

    @Override
    public void weaveAllNoCSPForStateViews(RamAbstractScene<?> scene) {
    }

    @Override
    public void switchToConcern(final RamAbstractScene<?> scene) {
        boolean isSaveNeeded = EMFEditUtil.getCommandStack(((DisplayRestTreeScene) scene).getRestTree())
                .isSaveNeeded();
        if (isSaveNeeded) {
            showCloseConfirmPopup((DisplayRestTreeScene) scene);
        } else {
            checkForModelRemoval((DisplayRestTreeScene) scene);
            doSwitchToPreviousScene((DisplayRestTreeScene) scene);
        }
    }

    @Override
    public void closeSplitView(RamAbstractScene<?> displayAspectScene) {
    }

    @Override
    public void layout(RamAbstractScene<?> scene) {
        Layout newLayout = new AutomaticLayout(70);
        newLayout.layout(RifModelUtils.getRestTreeViewFromApp(RamApp.getApplication()).getContainerLayer(), 
                Layout.LayoutUpdatePhase.FROM_PARENT);
    }

    /**
     * Performs the switching to the concern scene.
     * Unloads the resource and triggers the scene change to the previous scene.
     *
     * @param scene the current aspect scene
     */
    protected void doSwitchToPreviousScene(DisplayRestTreeScene scene) {
        // Unload WovenAspect of this aspect when we leave
        COREArtefact artefact = scene.getArtefact();
        COREModelUtil.unloadExternalResources(artefact);
        if (scene.getPreviousScene() instanceof DisplayRestTreeScene) {
            scene.setTransition(new SlideUpDownTransition(RamApp.getApplication(), 500, true));

            scene.getApplication().changeScene(scene.getPreviousScene());
            if (scene.getPreviousScene() instanceof DisplayRestTreeScene) {
                ((DisplayRestTreeScene) scene.getPreviousScene()).repushSections();
            }
            scene.getApplication().destroySceneAfterTransition(scene);
        } else if (scene.getPreviousScene() instanceof DisplayConcernEditScene) {
            // Temporary workaround until navigation properly takes care of all navigation.
            NavigationBar.getInstance().popSection();
            scene.getPreviousScene().getCanvas().addChild(NavigationBar.getInstance());
            scene.setTransition(new SlideTransition(RamApp.getApplication(), 500, false));

            scene.getApplication().changeScene(scene.getPreviousScene());
            scene.getApplication().destroySceneAfterTransition(scene);
        }
    }

    /**
     * Checks whether the artefact needs to be removed from the concern.
     * The artefact is removed, if it was not saved and therefore not contained anywhere.
     *
     * @param scene the current use case diagram scene
     */
    protected static void checkForModelRemoval(DisplayRestTreeScene scene) {
        RestIF aspect = scene.getRestTree();
        COREArtefact artefact = scene.getArtefact();
        // That is the aspect is not saved at all.
        if (aspect.eResource() == null) {

            // Get all the features realizing it
            List<COREFeature> listOfFeatures = artefact.getScene().getRealizes();
            List<COREFeature> copyOfListOfFeature = new ArrayList<COREFeature>(listOfFeatures);

            // Loop through all the features and remove the realization
            for (COREFeature feature : copyOfListOfFeature) {
                // Do a pre-mature check to see if the feature realizes the scene,
                // Can exist scenario, where the bi directional link might not be true
                if (feature.getRealizedBy().contains(artefact.getScene())) {
                    feature.getRealizedBy().remove(artefact.getScene());
                }
            }

            COREConcern concern = artefact.getCoreConcern();

            // Remove from the list of artefacts
            if (concern != null) {
                concern.getArtefacts().remove(artefact);
            }
        }
    }

    /**
     * Display a popup for the user to decided whether he wants to save the Use Case Diagram or leave the scene.
     *
     * @param scene - The {@link DisplayAspectScene} to consider
     */
    private void showCloseConfirmPopup(final DisplayRestTreeScene scene) {
        scene.showCloseConfirmPopup(scene, new SaveConfirmListener(scene));
    }
}
