package ca.mcgill.sel.restif.ui.components;

import ca.mcgill.sel.ram.ui.components.RamButton;
import ca.mcgill.sel.ram.ui.components.RamImageComponent;

public class ResourceRamButton extends RamButton {

    @SuppressWarnings("unused")
    private RamImageComponent icon;
    private boolean usesImage;

    public ResourceRamButton(RamImageComponent image, int arcRadius) {
        super(image, arcRadius);
        this.icon = image;
        this.usesImage = true;
    }
    
    @Override
    public void setIcon(RamImageComponent img) {
        if (img != null) {
            this.icon = img;
            if (usesImage) {
                removeAllChildren();
                setBuffers(0);
                this.addChild(img);
            }
        }
    }
}
