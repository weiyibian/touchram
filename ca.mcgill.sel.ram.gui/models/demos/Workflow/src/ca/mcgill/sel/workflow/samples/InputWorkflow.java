package ca.mcgill.sel.workflow.samples;

import ca.mcgill.sel.workflow.CustomizableNode;
import ca.mcgill.sel.workflow.EndNode;
import ca.mcgill.sel.workflow.InputData;
import ca.mcgill.sel.workflow.InputNode;
import ca.mcgill.sel.workflow.LocalContext;
import ca.mcgill.sel.workflow.StartupNode;

public class InputWorkflow extends AbstractWorkflowDemo {
    
    private class SomeInputData extends InputData {

        private String data;
        
        public SomeInputData(String senderName, String queueName, String data) {
            super(senderName, queueName);
            
            this.data = data;
        }
        
        public String getData() {
            return data;
        }
        
    }
    
    private class ProcessInputNode extends CustomizableNode {
        
        @Override
        public void execute(LocalContext c) {
            InputData inputData = c.getMyInput();
            SomeInputData concreteInputData = (SomeInputData) inputData;
            System.out.println("Input received: " + concreteInputData.data);
        }
        
    }

    public InputWorkflow(LocalContext context) {
        super(context);
    }

    @Override
    public void initNodes() {
        StartupNode startupNode = new StartupNode();
        EndNode endNode = new EndNode();
        
        InputNode inputNode = new InputNode();
        ProcessInputNode processNode = new ProcessInputNode();
        
        workflow.addNode(startupNode);
        workflow.addNode(inputNode);
        workflow.addNode(processNode);
        workflow.addNode(endNode);
        
        startupNode.setNextNode(inputNode);
        inputNode.setNextNode(processNode);
        processNode.setNextNode(endNode);
        
        workflow.addStartupNode(startupNode);
    }

    public static void main(String[] args) throws InterruptedException {
        LocalContext context = new LocalContext();
        InputWorkflow workflow = new InputWorkflow(context);
        workflow.executeWorkflow();
        
        Thread.sleep(2000);
        
        SomeInputData inputData = workflow.new SomeInputData("unsure", "unsure", "teh data");
        inputData.execute();
    }

}
