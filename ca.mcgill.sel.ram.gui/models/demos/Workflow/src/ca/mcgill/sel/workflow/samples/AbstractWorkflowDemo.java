package ca.mcgill.sel.workflow.samples;

import ca.mcgill.sel.workflow.LocalContext;
import ca.mcgill.sel.workflow.Workflow;

public abstract class AbstractWorkflowDemo {
    
    protected Workflow workflow;
    protected LocalContext context;
    
    public AbstractWorkflowDemo(LocalContext context) {
        this.workflow = initWorkflow();
        this.context = context;
        
        initNodes();
    }
    
    protected Workflow initWorkflow() {
        return new Workflow();
    }
    
    protected abstract void initNodes();
    
    public void executeWorkflow() {
        workflow.initialize();
        workflow.launch(context);
    }
    

}
