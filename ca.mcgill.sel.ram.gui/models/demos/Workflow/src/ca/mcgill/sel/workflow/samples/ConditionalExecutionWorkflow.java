package ca.mcgill.sel.workflow.samples;

import ca.mcgill.sel.workflow.ConditionalExecutionNode;
import ca.mcgill.sel.workflow.CustomizableNode;
import ca.mcgill.sel.workflow.EndNode;
import ca.mcgill.sel.workflow.LocalContext;
import ca.mcgill.sel.workflow.StartupNode;
import ca.mcgill.sel.workflow.WorkflowUtility;

public class ConditionalExecutionWorkflow extends AbstractWorkflowDemo {

    public ConditionalExecutionWorkflow(LocalContext context) {
        super(context);
    }
    
    private class ChoosePathNode extends CustomizableNode {

        @Override
        public void execute(LocalContext c) {
            WorkflowUtility.getWorkflowUtility().setCurrentChosenPath("OrForkNodeName", "if");
        }
        
    }

    @Override
    public void initNodes() {
        StartupNode startupNode = new StartupNode();
        EndNode endNode = new EndNode();
        EndNode endNode2 = new EndNode();
        
        ConditionalExecutionNode conditionalNode = new ConditionalExecutionNode("OrForkNodeName");
        ChoosePathNode choosePathNode = new ChoosePathNode();
        
        startupNode.setNextNode(choosePathNode);
        choosePathNode.setNextNode(conditionalNode);
        conditionalNode.addNextNode("if", endNode);
        conditionalNode.addNextNode("else", endNode2);
        
        workflow.addNode(startupNode);
        workflow.addNode(choosePathNode);
        workflow.addNode(conditionalNode);
        workflow.addNode(endNode);
        workflow.addNode(endNode2);
        
        workflow.addStartupNode(startupNode);
    }

    public static void main(String[] args) {
        LocalContext context = new LocalContext();
        ConditionalExecutionWorkflow workflow = new ConditionalExecutionWorkflow(context);
        workflow.executeWorkflow();
    }

}
