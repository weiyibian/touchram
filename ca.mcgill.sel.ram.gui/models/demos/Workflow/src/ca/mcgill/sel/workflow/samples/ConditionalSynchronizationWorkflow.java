package ca.mcgill.sel.workflow.samples;

import ca.mcgill.sel.workflow.ConditionalSynchronizationNode;
import ca.mcgill.sel.workflow.CustomizableNode;
import ca.mcgill.sel.workflow.EndNode;
import ca.mcgill.sel.workflow.LocalContext;
import ca.mcgill.sel.workflow.StartupNode;
import ca.mcgill.sel.workflow.WorkflowUtility;

public class ConditionalSynchronizationWorkflow extends AbstractWorkflowDemo {
    
    private class Node extends CustomizableNode {

        @Override
        public void execute(LocalContext c) {
            WorkflowUtility.getWorkflowUtility().setCurrentSyncCondition("CSNode", true);
        }
        
    }

    public ConditionalSynchronizationWorkflow(LocalContext context) {
        super(context);
    }

    @Override
    public void initNodes() {
        StartupNode startupNode = new StartupNode();
        StartupNode startupNode2 = new StartupNode();
        EndNode endNode = new EndNode();
        
        ConditionalSynchronizationNode synchronizedNode = new ConditionalSynchronizationNode(false, "AndJoinNodeName");
        
        HelloNode hello = new HelloNode("hello from the other side");
        
        startupNode.setNextNode("waiting", synchronizedNode);
        startupNode2.setNextNode("trigger", synchronizedNode);
        synchronizedNode.setNextNode(hello);
        hello.setNextNode(endNode);
        
        workflow.addNode(startupNode);
        workflow.addNode(startupNode2);
        workflow.addNode(hello);
        workflow.addNode(synchronizedNode);
        workflow.addNode(endNode);
        
        workflow.addStartupNode(startupNode);
        workflow.addStartupNode(startupNode2);
    }

    public static void main(String[] args) {
        LocalContext context = new LocalContext();
        ConditionalSynchronizationWorkflow workflow = new ConditionalSynchronizationWorkflow(context);
        workflow.executeWorkflow();
    }

}
