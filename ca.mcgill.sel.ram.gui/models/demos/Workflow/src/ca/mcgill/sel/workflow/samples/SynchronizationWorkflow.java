package ca.mcgill.sel.workflow.samples;

import ca.mcgill.sel.workflow.EndNode;
import ca.mcgill.sel.workflow.LocalContext;
import ca.mcgill.sel.workflow.StartupNode;
import ca.mcgill.sel.workflow.SynchronizationNode;

public class SynchronizationWorkflow extends AbstractWorkflowDemo {

    public SynchronizationWorkflow(LocalContext context) {
        super(context);
    }

    @Override
    public void initNodes() {
        StartupNode startupNode = new StartupNode();
        StartupNode startupNode2 = new StartupNode();
        EndNode endNode = new EndNode();
        
        SynchronizationNode synchronizedNode = new SynchronizationNode();
        
        HelloNode hello1 = new HelloNode("Hello from path 1");
        HelloNode hello2 = new HelloNode("Hello from path 2");
        
        startupNode.setNextNode(hello1);
        startupNode2.setNextNode(hello2);
        
        hello1.setNextNode("path1", synchronizedNode);
        hello2.setNextNode("path2", synchronizedNode);
        
        synchronizedNode.setNextNode(endNode);
        
        workflow.addNode(startupNode);
        workflow.addNode(startupNode2);
        workflow.addNode(hello1);
        workflow.addNode(hello2);
        workflow.addNode(synchronizedNode);
        workflow.addNode(endNode);
        
        workflow.addStartupNode(startupNode);
        workflow.addStartupNode(startupNode2);
    }

    public static void main(String[] args) {
        LocalContext context = new LocalContext();
        SynchronizationWorkflow workflow = new SynchronizationWorkflow(context);
        workflow.executeWorkflow();
    }

}
