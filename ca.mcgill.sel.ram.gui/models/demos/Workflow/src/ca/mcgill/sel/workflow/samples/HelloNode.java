package ca.mcgill.sel.workflow.samples;

import ca.mcgill.sel.workflow.CustomizableNode;
import ca.mcgill.sel.workflow.LocalContext;

public class HelloNode extends CustomizableNode {
    
    private String message;
    
    public HelloNode(String message) {
        this.message = message;
    }
    
    public void execute(LocalContext context) {
        System.out.println(message);
    }
    
}