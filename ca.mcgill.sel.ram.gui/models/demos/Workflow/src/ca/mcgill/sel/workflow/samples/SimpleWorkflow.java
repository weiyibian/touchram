package ca.mcgill.sel.workflow.samples;

import ca.mcgill.sel.workflow.EndNode;
import ca.mcgill.sel.workflow.LocalContext;
import ca.mcgill.sel.workflow.StartupNode;

public class SimpleWorkflow extends AbstractWorkflowDemo {

    public SimpleWorkflow(LocalContext context) {
        super(context);
    }

    @Override
    public void initNodes() {
        StartupNode startupNode = new StartupNode();
        EndNode endNode = new EndNode();
        
        HelloNode hello = new HelloNode("Hello from the other side");
        
        workflow.addNode(startupNode);
        workflow.addNode(hello);
        workflow.addNode(endNode);
        
        startupNode.setNextNode(hello);
        hello.setNextNode(endNode);
        
        workflow.addStartupNode(startupNode);
    }

    public static void main(String[] args) {
        LocalContext context = new LocalContext();
        SimpleWorkflow workflow = new SimpleWorkflow(context);
        workflow.executeWorkflow();
    }

}
