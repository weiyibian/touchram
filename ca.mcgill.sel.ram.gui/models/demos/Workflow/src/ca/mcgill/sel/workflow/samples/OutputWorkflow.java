package ca.mcgill.sel.workflow.samples;

import ca.mcgill.sel.workflow.CustomizableNode;
import ca.mcgill.sel.workflow.CustomizableOutputData;
import ca.mcgill.sel.workflow.EndNode;
import ca.mcgill.sel.workflow.LocalContext;
import ca.mcgill.sel.workflow.StartupNode;

public class OutputWorkflow extends AbstractWorkflowDemo {
    
    private class AskForInformationOutput extends CustomizableOutputData {
        
        private String message;
        
        public AskForInformationOutput(String message) {
            this.message = message;
        }
        
        public String getMessage() {
            return message;
        }
        
    }
    
    private class AskForInformation extends CustomizableNode {

        @Override
        public void execute(LocalContext c) {
            AskForInformationOutput output = new AskForInformationOutput("Gimme some information pls!");
            // TODO: simply output it on the console.
            System.out.println("Send over network: " + output.message);
            output.send("unknownHost");
        }
        
    }

    public OutputWorkflow(LocalContext context) {
        super(context);
    }

    @Override
    public void initNodes() {
        StartupNode startupNode = new StartupNode();
        EndNode endNode = new EndNode();
        
        AskForInformation requestNode = new AskForInformation();
        
        workflow.addNode(startupNode);
        workflow.addNode(requestNode);
        workflow.addNode(endNode);
        
        startupNode.setNextNode(requestNode);
        requestNode.setNextNode(endNode);
        
        workflow.addStartupNode(startupNode);
    }

    public static void main(String[] args) {
        LocalContext context = new LocalContext();
        OutputWorkflow workflow = new OutputWorkflow(context);
        workflow.executeWorkflow();
    }

}
