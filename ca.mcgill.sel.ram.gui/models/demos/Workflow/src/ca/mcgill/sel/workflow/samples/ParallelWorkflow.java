package ca.mcgill.sel.workflow.samples;

import ca.mcgill.sel.workflow.EndNode;
import ca.mcgill.sel.workflow.LocalContext;
import ca.mcgill.sel.workflow.ParallelExecutionNode;
import ca.mcgill.sel.workflow.StartupNode;

public class ParallelWorkflow extends AbstractWorkflowDemo {

    public ParallelWorkflow(LocalContext context) {
        super(context);
    }

    @Override
    public void initNodes() {
        StartupNode startupNode = new StartupNode();
        EndNode endNode = new EndNode();
        EndNode endNode2 = new EndNode();
        
        ParallelExecutionNode parallelNode = new ParallelExecutionNode();
        HelloNode hello1 = new HelloNode("Hello from parallel path 1");
        HelloNode hello2 = new HelloNode("Hello from parallel path 2");
        
        startupNode.setNextNode(parallelNode);
        parallelNode.addNextNode("path1", hello1);
        parallelNode.addNextNode("path2", hello2);
        hello1.setNextNode(endNode);
        hello2.setNextNode(endNode2);
        
        workflow.addNode(startupNode);
        workflow.addNode(parallelNode);
        workflow.addNode(hello1);
        workflow.addNode(hello2);
        workflow.addNode(endNode);
        workflow.addNode(endNode2);
        
        workflow.addStartupNode(startupNode);
    }

    public static void main(String[] args) {
        LocalContext context = new LocalContext();
        ParallelWorkflow workflow = new ParallelWorkflow(context);
        workflow.executeWorkflow();
    }

}
