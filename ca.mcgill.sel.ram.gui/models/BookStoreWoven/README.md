# BookStore - Woven

## About

This is a hand crafted model of the Bookstore. We hope that one day soon we will be able to automatically create a corresponding model by weaving a RIF-derifed annotated CDM with a functionality rich CDM.

## Why

We crafted this model by hand to explore how controllers should be arranged and how they should interact with JAR imported functionality. Also we crafted this model to verify that the code generation works. (it does)

## Usage

Generating the code:
 * Open the realizing design model (classdiagram).
 * You would see a class diagram. Note that there are model element s that you can not see, because they are not visually displayed. (Annotations)
 * Then click the "gears" symbol in the top right corner.
 * Select "Generate java/maven" code. Then select a target directory.

This will generate a new folder in your target directory:
 * *generated-project*
 * This generated folder houses a maven project. So inside you will see two things:
   * A pom.xml file (maven configuration, declared dependencies and specifies how to build the project)
   * a src folder with the actual java sources, generated from your java 
 * The project depends on a code that is not available in the official maven repository, the "Bookstore Internals".
   * clone this repository: [BookstoreInternals](https://github.com/kartoffelquadrat/BookStoreInternals)
   * ```cd``` inside, compile it as a maven project, **and add it to your local .m2 repository**, with  
```mvn clean install```
 ( Now you can compile an ron the generated sources:
   * cd into your target folder
   * compile the sources with:  
```mvn clean package```
   * Run the Bookstore, with ```java -jar target/bookstore-0.0.1.jar```
 * You can now access the bookstore's REST interface

## Bookstore REST interface

The bookstore offers the following [rest interface](bookstore.arc). (Open the arc file with the "Advanced Rest Client")






