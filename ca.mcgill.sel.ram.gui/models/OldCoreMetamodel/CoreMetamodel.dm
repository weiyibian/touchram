<?xml version="1.0" encoding="ASCII"?>
<classdiagram:ClassDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:classdiagram="http://cs.mcgill.ca/sel/cdm/1.0" name="CoreMetamodel">
  <classes xsi:type="classdiagram:Class" name="Concern">
    <associationEnds name="artefacts" assoc="//@associations.0" upperBound="-1" referenceType="Composition"/>
  </classes>
  <classes xsi:type="classdiagram:Class" name="Model">
    <associationEnds name="myConcern" navigable="false" assoc="//@associations.0" lowerBound="1"/>
    <associationEnds name="modelReuses" assoc="//@associations.2" upperBound="-1"/>
    <associationEnds name="modelExtensions" assoc="//@associations.3" upperBound="-1"/>
    <associationEnds name="modelElements" assoc="//@associations.4" upperBound="-1" referenceType="Composition"/>
    <associationEnds name="realizes" assoc="//@associations.5" upperBound="-1"/>
  </classes>
  <classes xsi:type="classdiagram:Class" name="FeatureModel" superTypes="//@classes.1">
    <associationEnds name="features" assoc="//@associations.1" upperBound="-1" referenceType="Composition"/>
  </classes>
  <classes xsi:type="classdiagram:Class" name="Feature">
    <associationEnds name="myFeatureModel" navigable="false" assoc="//@associations.1" lowerBound="1"/>
    <associationEnds name="realizedBy" assoc="//@associations.5" upperBound="-1"/>
  </classes>
  <classes xsi:type="classdiagram:Class" name="EObject" dataType="true"/>
  <classes xsi:type="classdiagram:Class" name="ModelReuse" superTypes="//@classes.7">
    <associationEnds name="myArtefact" navigable="false" assoc="//@associations.2" lowerBound="1"/>
  </classes>
  <classes xsi:type="classdiagram:Class" name="ModelExtension" superTypes="//@classes.7">
    <associationEnds name="myArtefact" navigable="false" assoc="//@associations.3" lowerBound="1"/>
  </classes>
  <classes xsi:type="classdiagram:Class" name="ModelComposition"/>
  <classes xsi:type="classdiagram:Class" name="ModelElement">
    <associationEnds name="myArtefact" navigable="false" assoc="//@associations.4" lowerBound="1"/>
    <attributes name="visibility" type="//@types.11"/>
    <attributes name="partiality" type="//@types.10"/>
  </classes>
  <types xsi:type="classdiagram:CDVoid"/>
  <types xsi:type="classdiagram:CDAny"/>
  <types xsi:type="classdiagram:CDBoolean"/>
  <types xsi:type="classdiagram:CDDouble"/>
  <types xsi:type="classdiagram:CDInt"/>
  <types xsi:type="classdiagram:CDLong"/>
  <types xsi:type="classdiagram:CDString"/>
  <types xsi:type="classdiagram:CDByte"/>
  <types xsi:type="classdiagram:CDFloat"/>
  <types xsi:type="classdiagram:CDChar"/>
  <types xsi:type="classdiagram:CDEnum" name="PartialityType">
    <literals name="None"/>
    <literals name="Concern"/>
    <literals name="Public"/>
  </types>
  <types xsi:type="classdiagram:CDEnum" name="VisibilityType"/>
  <associations name="Concern_Artefact" ends="//@classes.0/@associationEnds.0 //@classes.1/@associationEnds.0"/>
  <associations name="FeatureModel_Feature" ends="//@classes.2/@associationEnds.0 //@classes.3/@associationEnds.0"/>
  <associations name="Artefact_ModelReuse" ends="//@classes.1/@associationEnds.1 //@classes.5/@associationEnds.0"/>
  <associations name="Artefact_ModelExtension" ends="//@classes.1/@associationEnds.2 //@classes.6/@associationEnds.0"/>
  <associations name="Artefact_UIElement" ends="//@classes.1/@associationEnds.3 //@classes.8/@associationEnds.0"/>
  <associations name="Feature_Model" ends="//@classes.3/@associationEnds.1 //@classes.1/@associationEnds.4"/>
  <layout>
    <containers key="/">
      <value key="//@classes.0">
        <value x="87.931" y="396.03326"/>
      </value>
      <value key="//@classes.1">
        <value x="525.1863" y="396.03326"/>
      </value>
      <value key="//@classes.2">
        <value x="336.98322" y="569.9519"/>
      </value>
      <value key="//@classes.3">
        <value x="569.5248" y="671.8027"/>
      </value>
      <value key="//@classes.4">
        <value x="1335.908" y="-2.4329472"/>
      </value>
      <value key="//@classes.5">
        <value x="431.12146" y="153.25417"/>
      </value>
      <value key="//@classes.6">
        <value x="612.12177" y="153.25813"/>
      </value>
      <value key="//@classes.7">
        <value x="507.18652" y="9.067053"/>
      </value>
      <value key="//@classes.8">
        <value x="923.8063" y="388.03326"/>
      </value>
      <value key="//@types.10">
        <value x="1335.908" y="348.52744"/>
      </value>
      <value key="//@types.11">
        <value x="1390.0" y="266.0"/>
      </value>
    </containers>
  </layout>
</classdiagram:ClassDiagram>
