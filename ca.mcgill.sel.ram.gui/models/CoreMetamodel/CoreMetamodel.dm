<?xml version="1.0" encoding="ASCII"?>
<classdiagram:ClassDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:classdiagram="http://cs.mcgill.ca/sel/cdm/1.0" name="CoreMetamodel">
  <classes xsi:type="classdiagram:Class" name="Concern">
    <associationEnds name="artefacts" assoc="//@associations.0" upperBound="-1" referenceType="Composition"/>
  </classes>
  <classes xsi:type="classdiagram:Class" name="Artefact">
    <associationEnds name="myConcern" navigable="false" assoc="//@associations.0" lowerBound="1"/>
    <associationEnds name="myScenes" assoc="//@associations.4" lowerBound="1" upperBound="-1"/>
    <associationEnds name="modelReuses" assoc="//@associations.5" upperBound="-1"/>
    <associationEnds name="modelExtensions" assoc="//@associations.6" upperBound="-1"/>
    <associationEnds name="myCIElements" assoc="//@associations.9" upperBound="-1" referenceType="Composition"/>
    <associationEnds name="myUIElements" assoc="//@associations.10" upperBound="-1" referenceType="Composition"/>
  </classes>
  <classes xsi:type="classdiagram:Class" name="FeatureModel" superTypes="//@classes.1">
    <associationEnds name="features" assoc="//@associations.1" upperBound="-1" referenceType="Composition"/>
  </classes>
  <classes xsi:type="classdiagram:Class" name="Feature">
    <associationEnds name="myFeatureModel" navigable="false" assoc="//@associations.1" lowerBound="1"/>
    <associationEnds name="realizedBy" assoc="//@associations.2" upperBound="-1"/>
  </classes>
  <classes xsi:type="classdiagram:Class" name="ExternalArtefact" superTypes="//@classes.1">
    <attributes name="language" type="//@types.6"/>
    <attributes name="rootModelElement" type="//@classes.5"/>
  </classes>
  <classes xsi:type="classdiagram:Class" name="EObject" dataType="true"/>
  <classes xsi:type="classdiagram:Class" name="Perspective">
    <associationEnds name="types" assoc="//@associations.3" upperBound="-1"/>
  </classes>
  <classes xsi:type="classdiagram:Class" name="Scene">
    <associationEnds name="realizes" assoc="//@associations.2"/>
    <associationEnds name="typedBy" assoc="//@associations.3" lowerBound="1"/>
    <associationEnds name="artefacts" assoc="//@associations.4" upperBound="-1"/>
  </classes>
  <classes xsi:type="classdiagram:Class" name="ModelReuse" superTypes="//@classes.10">
    <associationEnds name="myArtefact" navigable="false" assoc="//@associations.5" lowerBound="1"/>
  </classes>
  <classes xsi:type="classdiagram:Class" name="ModelExtension" superTypes="//@classes.10">
    <associationEnds name="myArtefact" navigable="false" assoc="//@associations.6" lowerBound="1"/>
  </classes>
  <classes xsi:type="classdiagram:Class" name="ModelComposition"/>
  <classes xsi:type="classdiagram:Class" name="UIElement">
    <associationEnds name="myArtefact" assoc="//@associations.10" lowerBound="1"/>
    <attributes name="modelElement" type="//@classes.5"/>
  </classes>
  <classes xsi:type="classdiagram:Class" name="CIElement">
    <associationEnds name="myMappingCardinality" assoc="//@associations.7"/>
    <associationEnds name="referenceCardinality" assoc="//@associations.8" upperBound="-1" referenceType="Composition"/>
    <associationEnds name="myArtefact" assoc="//@associations.9" lowerBound="1"/>
    <attributes name="partiality" type="//@types.10"/>
    <attributes name="modelElement" type="//@classes.5"/>
  </classes>
  <classes xsi:type="classdiagram:Class" name="MappingCardinality">
    <associationEnds name="myCIElement" navigable="false" assoc="//@associations.7" lowerBound="1"/>
    <associationEnds name="myCIElement1" navigable="false" assoc="//@associations.8" lowerBound="1"/>
    <attributes name="lowerBound" type="//@types.4"/>
    <attributes name="upperBound" type="//@types.4"/>
    <attributes name="name" type="//@types.6"/>
  </classes>
  <types xsi:type="classdiagram:CDVoid"/>
  <types xsi:type="classdiagram:CDAny"/>
  <types xsi:type="classdiagram:CDBoolean"/>
  <types xsi:type="classdiagram:CDDouble"/>
  <types xsi:type="classdiagram:CDInt"/>
  <types xsi:type="classdiagram:CDLong"/>
  <types xsi:type="classdiagram:CDString"/>
  <types xsi:type="classdiagram:CDByte"/>
  <types xsi:type="classdiagram:CDFloat"/>
  <types xsi:type="classdiagram:CDChar"/>
  <types xsi:type="classdiagram:CDEnum" name="PartialityType">
    <literals name="None"/>
    <literals name="Concern"/>
    <literals name="Public"/>
  </types>
  <associations name="Concern_Artefact" ends="//@classes.0/@associationEnds.0 //@classes.1/@associationEnds.0"/>
  <associations name="FeatureModel_Feature" ends="//@classes.2/@associationEnds.0 //@classes.3/@associationEnds.0"/>
  <associations name="Feature_Scene" ends="//@classes.3/@associationEnds.1 //@classes.7/@associationEnds.0"/>
  <associations name="Scene_Perspective" ends="//@classes.7/@associationEnds.1 //@classes.6/@associationEnds.0"/>
  <associations name="Scene_Artefact" ends="//@classes.7/@associationEnds.2 //@classes.1/@associationEnds.1"/>
  <associations name="Artefact_ModelReuse" ends="//@classes.1/@associationEnds.2 //@classes.8/@associationEnds.0"/>
  <associations name="Artefact_ModelExtension" ends="//@classes.1/@associationEnds.3 //@classes.9/@associationEnds.0"/>
  <associations name="CIElement_MappingCardinality" ends="//@classes.12/@associationEnds.0 //@classes.13/@associationEnds.0"/>
  <associations name="CIElement_MappingCardinality" ends="//@classes.12/@associationEnds.1 //@classes.13/@associationEnds.1"/>
  <associations name="Artefact_CIElement" ends="//@classes.1/@associationEnds.4 //@classes.12/@associationEnds.2"/>
  <associations name="Artefact_UIElement" ends="//@classes.1/@associationEnds.5 //@classes.11/@associationEnds.0"/>
  <layout>
    <containers key="/">
      <value key="//@classes.0">
        <value x="87.931" y="396.03326"/>
      </value>
      <value key="//@classes.1">
        <value x="525.1863" y="396.03326"/>
      </value>
      <value key="//@classes.2">
        <value x="172.68144" y="569.92896"/>
      </value>
      <value key="//@classes.3">
        <value x="172.68144" y="671.47485"/>
      </value>
      <value key="//@classes.4">
        <value x="767.71246" y="643.47485"/>
      </value>
      <value key="//@classes.5">
        <value x="1335.908" y="-2.4329472"/>
      </value>
      <value key="//@classes.6">
        <value x="525.1863" y="856.50995"/>
      </value>
      <value key="//@classes.7">
        <value x="525.1863" y="671.47485"/>
      </value>
      <value key="//@classes.8">
        <value x="475.45987" y="153.31027"/>
      </value>
      <value key="//@classes.9">
        <value x="656.46014" y="153.31422"/>
      </value>
      <value key="//@classes.10">
        <value x="551.5248" y="9.12311"/>
      </value>
      <value key="//@classes.11">
        <value x="1100.9363" y="381.03693"/>
      </value>
      <value key="//@classes.12">
        <value x="1100.9318" y="506.70746"/>
      </value>
      <value key="//@classes.13">
        <value x="1503.1537" y="493.71655"/>
      </value>
      <value key="//@types.10">
        <value x="1335.8679" y="115.27275"/>
      </value>
    </containers>
  </layout>
</classdiagram:ClassDiagram>
