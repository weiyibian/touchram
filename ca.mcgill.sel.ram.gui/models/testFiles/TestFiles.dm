<?xml version="1.0" encoding="ASCII"?>
<classdiagram:ClassDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:classdiagram="http://cs.mcgill.ca/sel/cdm/1.0" name="TestFiles">
  <classes xsi:type="classdiagram:Class" name="A">
    <associationEnds name="myB" assoc="//@associations.0" lowerBound="1"/>
  </classes>
  <classes xsi:type="classdiagram:Class" name="B">
    <associationEnds name="myA" assoc="//@associations.0" lowerBound="1"/>
  </classes>
  <classes xsi:type="classdiagram:Class" name="C"/>
  <classes xsi:type="classdiagram:Class" name="D"/>
  <types xsi:type="classdiagram:CDVoid"/>
  <types xsi:type="classdiagram:CDAny"/>
  <types xsi:type="classdiagram:CDBoolean"/>
  <types xsi:type="classdiagram:CDDouble"/>
  <types xsi:type="classdiagram:CDInt"/>
  <types xsi:type="classdiagram:CDLong"/>
  <types xsi:type="classdiagram:CDString"/>
  <types xsi:type="classdiagram:CDByte"/>
  <types xsi:type="classdiagram:CDFloat"/>
  <types xsi:type="classdiagram:CDChar"/>
  <associations name="A_B" ends="//@classes.0/@associationEnds.0 //@classes.1/@associationEnds.0"/>
  <layout>
    <containers key="/">
      <value key="//@classes.0">
        <value x="514.0" y="196.0"/>
      </value>
      <value key="//@classes.1">
        <value x="852.9997" y="196.0"/>
      </value>
      <value key="//@classes.2">
        <value x="500.0" y="297.0"/>
      </value>
      <value key="//@classes.3">
        <value x="807.00006" y="297.0"/>
      </value>
    </containers>
  </layout>
</classdiagram:ClassDiagram>
