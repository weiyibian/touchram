<?xml version="1.0" encoding="ASCII"?>
<classdiagram:ClassDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:classdiagram="http://cs.mcgill.ca/sel/cdm/1.0" name="Testp1">
  <classes xsi:type="classdiagram:Class" name="A">
    <associationEnds name="myB" assoc="//@associations.0" lowerBound="1"/>
    <attributes name="x" type="//@types.4"/>
    <attributes name="y" type="//@types.8"/>
  </classes>
  <classes xsi:type="classdiagram:Class" name="B">
    <associationEnds name="myA" assoc="//@associations.0" lowerBound="1"/>
    <attributes name="b" type="//@types.4"/>
  </classes>
  <types xsi:type="classdiagram:CDVoid"/>
  <types xsi:type="classdiagram:CDAny"/>
  <types xsi:type="classdiagram:CDBoolean"/>
  <types xsi:type="classdiagram:CDDouble"/>
  <types xsi:type="classdiagram:CDInt"/>
  <types xsi:type="classdiagram:CDLong"/>
  <types xsi:type="classdiagram:CDString"/>
  <types xsi:type="classdiagram:CDByte"/>
  <types xsi:type="classdiagram:CDFloat"/>
  <types xsi:type="classdiagram:CDChar"/>
  <associations name="A_B" ends="//@classes.0/@associationEnds.0 //@classes.1/@associationEnds.0"/>
  <layout>
    <containers key="/">
      <value key="//@classes.0">
        <value x="489.0" y="149.0"/>
      </value>
      <value key="//@classes.1">
        <value x="831.0" y="125.0"/>
      </value>
    </containers>
  </layout>
</classdiagram:ClassDiagram>
