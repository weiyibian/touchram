package ca.mcgill.sel.ram.controller;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.MoveCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.CORECIElement;
import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREModelElementComposition;
import ca.mcgill.sel.core.COREPartialityType;
import ca.mcgill.sel.core.COREVisibilityType;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.AssignmentStatement;
import ca.mcgill.sel.ram.Attribute;
import ca.mcgill.sel.ram.Class;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.ClassifierMapping;
import ca.mcgill.sel.ram.Gate;
import ca.mcgill.sel.ram.Interaction;
import ca.mcgill.sel.ram.Lifeline;
import ca.mcgill.sel.ram.MappableElement;
import ca.mcgill.sel.ram.Message;
import ca.mcgill.sel.ram.MessageSort;
import ca.mcgill.sel.ram.MessageView;
import ca.mcgill.sel.ram.ObjectType;
import ca.mcgill.sel.ram.OpaqueExpression;
import ca.mcgill.sel.ram.Operation;
import ca.mcgill.sel.ram.OperationMapping;
import ca.mcgill.sel.ram.OperationType;
import ca.mcgill.sel.ram.Parameter;
import ca.mcgill.sel.ram.ParameterValueMapping;
import ca.mcgill.sel.ram.RAMClassVisibilityType;
import ca.mcgill.sel.ram.RAMPartialityType;
import ca.mcgill.sel.ram.RAMVisibilityType;
import ca.mcgill.sel.ram.RCollection;
import ca.mcgill.sel.ram.RVoid;
import ca.mcgill.sel.ram.RamFactory;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.StructuralFeature;
import ca.mcgill.sel.ram.StructuralFeatureValue;
import ca.mcgill.sel.ram.StructuralView;
import ca.mcgill.sel.ram.Type;
import ca.mcgill.sel.ram.controller.util.RAMReferenceUtil;
import ca.mcgill.sel.ram.impl.AspectImpl;
import ca.mcgill.sel.ram.util.MessageViewUtil;
import ca.mcgill.sel.ram.util.RAMModelUtil;
import ca.mcgill.sel.ram.util.StructuralViewUtil;

/**
 * The controller for {@link Class}es. Additionally contains operations for attributes, operations and parameters.
 *
 * @author mschoettle
 */
public class ClassController extends BaseController {

    /**
     * Creates a new instance of {@link ClassController}.
     */
    protected ClassController() {
        // Prevent anyone outside this package to instantiate.
    }

    /**
     * Moves the given classifier to a new position.
     *
     * @param classifier the classifier to move
     * @param x the new x position
     * @param y the new y position
     */
    public void moveClassifier(Classifier classifier, float x, float y) {
        Aspect aspect = EMFModelUtil.getRootContainerOfType(classifier, RamPackage.Literals.ASPECT);
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(aspect);

        Command moveClassCommand = createMoveCommand(editingDomain, aspect.getStructuralView(), classifier, x, y);

        doExecute(editingDomain, moveClassCommand);
    }
    
    /**
     * Changes the visibility of the classifier.
     *
     * @param clazz the classifier
     * @param visibility the new class visibility
     * @throws IllegalArgumentException when the visibility cannot be changed
     */
    public void changeClassVisibility(Classifier clazz, COREVisibilityType visibility) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(clazz);
        CompoundCommand command = new CompoundCommand();
        
        // if the class is abstract, it can be private or public
        if (visibility != COREVisibilityType.PUBLIC && !clazz.isAbstract()) {
            for (Operation op : clazz.getOperations()) {
                if (op.getVisibility() == RAMVisibilityType.PUBLIC) {
                    throw new IllegalArgumentException("The class has one or more public operations.");
                }
            }
        }
        
        Aspect aspect = EMFModelUtil.getRootContainerOfType(clazz, RamPackage.Literals.ASPECT);
        command.append(changeCoreVisibilityCommand(editingDomain, clazz, aspect, visibility));
        
        doExecute(editingDomain, command);
    }

    /**
     * Removes the given attribute.
     *
     * @param attribute the attribute to be removed
     */
    public void removeAttribute(Attribute attribute) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(attribute);
        CompoundCommand compoundCommand = new CompoundCommand();

        CORECIElement ci = COREArtefactUtil.getCIElementFor(attribute);
        if (ci != null) {
            compoundCommand.append(deleteCORECIElementCommand(ci));
        }
        
        // Create remove Command.
        compoundCommand.append(RemoveCommand.create(editingDomain, attribute));

        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Creates a new attribute.
     *
     * @param owner the class the attribute should be added to
     * @param index the index where the attribute should be added at
     * @param attributeString the user input for the attribute
     * @throws IllegalArgumentException if the attribute already exists or the given attribute string is invalid
     */
    public void createAttribute(Class owner, int index, String attributeString) {
        // Create the attribute with the given information.
        Attribute attribute = StructuralViewUtil.createAttribute(owner, attributeString);
        
        ObjectType type = attribute.getType();
        EditingDomain domain = EMFEditUtil.getEditingDomain(owner);
        CompoundCommand compoundCommand = new CompoundCommand();
        
        ObjectType newType = RAMReferenceUtil.localizeElement(domain, compoundCommand, owner, type);
        
        attribute.setType(newType);
        
        Command addCommand = AddCommand.create(domain, owner, 
                                RamPackage.Literals.CLASSIFIER__ATTRIBUTES, attribute, index);
        compoundCommand.append(addCommand);
        
        doExecute(domain, compoundCommand);
    }

    /**
     * Removes the given operation. Also removes the corresponding {@link MessageView} if it exists.
     *
     * @param operation the operation to be removed
     */
    public void removeOperation(Operation operation) {
        Aspect aspect = EMFModelUtil.getRootContainerOfType(operation, RamPackage.Literals.ASPECT);
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(aspect);
        CompoundCommand compoundCommand = new CompoundCommand();

        MessageView messageView = MessageViewUtil.getMessageViewFor(aspect, operation);

        if (messageView != null) {
            compoundCommand.append(AspectController.createRemoveMessageViewCommand(messageView));
        }
        CORECIElement ci = COREArtefactUtil.getCIElementFor(operation);
        if (ci != null) {
            compoundCommand.append(deleteCORECIElementCommand(ci));
        }
        compoundCommand.append(RemoveCommand.create(editingDomain, operation));

        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Creates a new operation.
     *
     * @param owner the class the operation should be added to
     * @param index the index the operation should be added at
     * @param operationString the user input for the new operation
     * @throws IllegalArgumentException if the operation already exists
     */
    public void createOperation(Class owner, int index, String operationString) {
        // Create the operation with the given information.
        Operation operation = StructuralViewUtil.createOperation(owner, operationString);
        
        if (operation != null) {
            Type type = operation.getReturnType();
            CompoundCommand compoundCommand = new CompoundCommand();
            EditingDomain domain = EMFEditUtil.getEditingDomain(owner);
            
            Type newType = RAMReferenceUtil.localizeElement(domain, compoundCommand, owner, type);
            
            operation.setReturnType(newType);

            Command command = 
                    createAddOperationCommand(domain, owner, operation, operation.getVisibility(), index);
            compoundCommand.append(command);
            
            for (int i = 0; i < operation.getParameters().size(); i++) {
                Parameter parameter = operation.getParameters().get(i);
                Type paramType = 
                        RAMReferenceUtil.localizeElement(domain, compoundCommand, owner, parameter.getType());
                parameter.setType(paramType);
            }
            
            doExecute(domain, compoundCommand);
        } else {
            throw new IllegalArgumentException("An operation with the same signature already exists.");
        }
    }
    
    /**
     * Sets an operation to a different index.
     *
     * @param operation the operation to be set
     * @param index the index the operation should be set to
     */
    public void setOperationPosition(Operation operation, int index) {
        AspectImpl ai = (AspectImpl) EcoreUtil.getRootContainer(operation);
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(ai);

        Command command = new MoveCommand(editingDomain, operation.eContainer(),
                RamPackage.Literals.CLASSIFIER__OPERATIONS, operation, index);

        doExecute(editingDomain, command);
    }
    
    /**
     * Sets an attribute to a different index.
     *
     * @param attribute the attribute to be set
     * @param index the index the attribute should be set to
     */
    public void setAttributePosition(Attribute attribute, int index) {
        AspectImpl ai = (AspectImpl) EcoreUtil.getRootContainer(attribute);
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(ai);

        Command command = new MoveCommand(editingDomain, attribute.eContainer(),
                RamPackage.Literals.CLASSIFIER__ATTRIBUTES, attribute, index);

        doExecute(editingDomain, command);
    }
    
    /**
     * Creates a new ghost (local duplicate) of an external operation.
     * The operation is mapped to its duplicate and the parameter mappings are done as well.
     * {@link RAMReferenceUtil} localizeElement is used here.
     * 
     * @param aspect Aspect root of the model
     * @param externalOperation the operation we have to duplicate and map with its local copy
     * @param newOperationName the local duplicate operation name (can be the same ad the externalOperation)
     */
    public void createGhostMappedOperation(Aspect aspect, Operation externalOperation, 
            String newOperationName) {
        EditingDomain domain = EMFEditUtil.getEditingDomain(aspect);
        CompoundCommand command = new CompoundCommand();
        Operation newOperation = RAMReferenceUtil.localizeElement(domain, command, aspect, externalOperation);
        
        newOperation.setName(newOperationName);
        
        doExecute(domain, command);
    }

    /**
     * Create a command that add a new operation.
     *
     * @param domain the {@link EditingDomain}
     * @param owner the class the operation should be added to
     * @param operation the operation to add
     * @param ramVisibility the {@link RAMVisibilityType} to set for the operation
     * @param index the index the operation should be added at
     * @throws IllegalArgumentException if the operation already exists
     *
     * @return The {@link Command} created
     */
    private Command createAddOperationCommand(EditingDomain domain, Classifier owner, Operation operation,
            RAMVisibilityType ramVisibility, int index) {
        CompoundCommand compoundCommand = new CompoundCommand();
        compoundCommand.append(
                changeOperationAndClassVisibilityCommand(domain, owner, operation, ramVisibility));
        compoundCommand.append(AddCommand.create(domain, owner, RamPackage.Literals.CLASSIFIER__OPERATIONS, operation,
                index));

        return compoundCommand;
    }

    /**
     * Changes the partiality of a {@link MappableElement}.
     * Usually a {@link Classifier}, a {@link Attribute} or a {@link Operation}
     * 
     * @param owner - The classifier we want the partiality to be changed
     * @param ramPartiality - The new value of the classifier partiality
     */
    public void changeMappableElementPartiality(MappableElement owner, RAMPartialityType ramPartiality) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);
        Command command = changeMappableElementPartialityCommand(editingDomain, owner, ramPartiality);
        doExecute(editingDomain, command);
    }
    
    /**
     * Returns the command that changes the partiality of a {@link MappableElement}.
     * Usually a {@link Classifier}, a {@link Attribute} or a {@link Operation}
     * 
     * @param editingDomain - The {@link EditingDomain} for the command
     * @param owner - The classifier we want the partiality to be changed
     * @param ramPartiality - The new value of the classifier partiality
     * @return {@link Command} which is created.
     */
    private Command changeMappableElementPartialityCommand(EditingDomain editingDomain, MappableElement owner,
            RAMPartialityType ramPartiality) {
        CompoundCommand compoundCommand = new CompoundCommand();

        // Change CORE Partiality
        compoundCommand.append(setCOREPartialityType(owner, fromRAMToCOREPartialityType(ramPartiality)));
        
        // Change RAM Partiality
        compoundCommand.append(SetCommand.create(editingDomain, owner, 
                RamPackage.Literals.MAPPABLE_ELEMENT__PARTIALITY, ramPartiality));
        
        return compoundCommand;
    }
    
    /**
     * Changes the visibility of the classifier accordingly when the visibility of an operation is changed to public.
     *
     * @param owner the classifier the operation belongs to
     * @param operation which visibility is changed
     * @param visibility the value of operation's {@link RAMVisibilityType}
     */
    public void changeOperationAndClassVisibility(Classifier owner, Operation operation, RAMVisibilityType visibility) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);
        Command command = changeOperationAndClassVisibilityCommand(editingDomain, owner, operation, visibility);
        doExecute(editingDomain, command);
    }

    /**
     * Returns the command that changes the visibility of the classifier accordingly when the visibility of an operation
     * is changed.
     *
     * @param editingDomain - The {@link EditingDomain} for the command
     * @param owner - The {@link Classifier} the operation belongs to
     * @param operation - The {@link Operation} which visibility is changed
     * @param ramVisibility - The value of operation's {@link RAMVisibilityType}
     * @return {@link Command} which is created.
     */
    Command changeOperationAndClassVisibilityCommand(EditingDomain editingDomain, Classifier owner,
            Operation operation,
            RAMVisibilityType ramVisibility) {
        CompoundCommand compoundCommand = new CompoundCommand();
        // If adding a public operation to a non public class (unless it is abstract)
        if (ramVisibility == RAMVisibilityType.PUBLIC && owner.getVisibility() != RAMClassVisibilityType.PUBLIC 
                && !owner.isAbstract()) {
            // Update the classifier visibility
            // For CORE
            compoundCommand.append(changeCoreVisibilityCommand(editingDomain, 
                    owner, owner, COREVisibilityType.PUBLIC));
            // For RAM
            compoundCommand.append(SetCommand.create(editingDomain, 
                    owner, RamPackage.Literals.CLASSIFIER__VISIBILITY, RAMClassVisibilityType.PUBLIC));
        }
        // Update the operation visibility
        compoundCommand.append(changeOperationVisibilityCommand(editingDomain, operation, owner, ramVisibility));
        return compoundCommand;
    }

    /**
     * Get the command used to change the {@link RAMVisibilityType} of an {@link Operation}.
     * This does update the {@link COREVisibilityType} of the operation if necessary as well.
     *
     * @param editingDomain - the editing domain
     * @param operation - the operation
     * @param owner - the operation owner
     * @param visibilityType - the new visibility to set
     * @return the created command
     */
    private Command changeOperationVisibilityCommand(EditingDomain editingDomain, Operation operation, Classifier owner,
            RAMVisibilityType visibilityType) {
        CompoundCommand command = new CompoundCommand();
        // Change the CORE visibility if necessary
        if (visibilityType == RAMVisibilityType.PUBLIC) {
            Command tempCommand = changeCoreVisibilityCommand(editingDomain, 
                    operation, owner, COREVisibilityType.PUBLIC);
            if (tempCommand != null) {
                command.append(tempCommand);
            }
        } else {
            Command tempCommand = changeCoreVisibilityCommand(editingDomain, 
                    operation, owner, COREVisibilityType.CONCERN);
            if (tempCommand != null) {
                command.append(tempCommand);
            }
        }
        // Change only the RAM visibility
        command.append(SetCommand.create(editingDomain, operation, RamPackage.Literals.OPERATION__VISIBILITY,
                visibilityType));
        return command;
    }

    /**
     * Creates a copy of the given operation with the same signature and with a desired name. Adds it to the proper
     * class as well.
     *
     * @param operationMapping the operation mapping which the operation we copy belongs to
     * @param newName the name of the operation that is going to be created from the copy.
     * @param structuralView the structural view of the current aspect
     * @return {@link Operation} which is created as a clone of the given operation with the parameters that are
     *         accordingly changed depending on the signature
     */
    public Operation createOperationCopyWithoutCommand(OperationMapping operationMapping, String newName,
            StructuralView structuralView) {
        ClassifierMapping classifierMapping = (ClassifierMapping) operationMapping.eContainer();
        COREModelComposition composition = (COREModelComposition) classifierMapping.eContainer();

        Operation oldOperation = operationMapping.getFrom();

        // First we copy the entire operation then we'll change the types of the parameters depending on the signature.
        Operation copyOperation = EcoreUtil.copy(oldOperation);

        // Then we change the name of the operation
        copyOperation.setName(newName);
        copyOperation.setPartiality(RAMPartialityType.NONE);

        copyOperation.setReturnType(getType(copyOperation.getReturnType(), composition, structuralView));

        // These parameters are exact same copy of the old operation. So we need to change them. We chose this way
        // because we didn't want to create all the parameters one at a time.
        EList<Parameter> parametersOfNewOperation = copyOperation.getParameters();

        // For each original parameter check if it is mapped as a classifier mapping's from element
        // so that we can get the signature of to element if it is mapped to anything.
        for (Parameter parameter : parametersOfNewOperation) {
            parameter.setType(getType(parameter.getType(), composition, structuralView));
        }

        return copyOperation;
    }

    /**
     * Creates an operation which gets the name of an attribute.
     *
     * @param attribute the attribute.
     */
    public void createGetterOperation(StructuralFeature attribute) {
        Aspect aspect = EMFModelUtil.getRootContainerOfType(attribute, RamPackage.Literals.ASPECT);

        EditingDomain domain = EMFEditUtil.getEditingDomain(aspect);
        Command command = this.createGetterOperationCommand(domain, aspect, attribute, RAMVisibilityType.PUBLIC);
        if (command != null) {
            doExecute(domain, command);
        }
    }

    /**
     * Returns a command that creates an operation which gets the name of an attribute.
     *
     * @param domain the {@link EditingDomain} to use for executing commands
     * @param aspect The current aspect
     * @param attribute the attribute.
     * @param visibilityType The visibility type
     *
     * @return the created {@link Command}
     */
    private Command createGetterOperationCommand(EditingDomain domain, Aspect aspect, StructuralFeature attribute,
            RAMVisibilityType visibilityType) {
        Class owner = (Class) attribute.eContainer();
        int index = owner.getOperations().size();

        Operation operation = RAMModelUtil.createGetter(attribute);

        if (operation == null) {
            return null;
        }
        
        CompoundCommand compoundCommand = new CompoundCommand();

        compoundCommand.appendAndExecute(createAddOperationCommand(domain, owner, operation, visibilityType, index));

        MessageView messageView = MessageViewUtil.createMessageView(operation);
        for (Message message : messageView.getSpecification().getMessages()) {
            if (message.getSignature() == operation && message.getMessageSort() == MessageSort.REPLY) {
                StructuralFeatureValue structuralFeatureValue = RamFactory.eINSTANCE.createStructuralFeatureValue();
                structuralFeatureValue.setValue(attribute);

                message.setReturns(structuralFeatureValue);
            }
        }

        compoundCommand.appendAndExecute(createAddMessageViewCommand(domain, aspect, messageView,
                messageView.getSpecification()));

        return compoundCommand;
    }

    /**
     * Creates an operation which sets the name of an attribute.
     *
     * @param attribute the attribute.
     */
    public void createSetterOperation(StructuralFeature attribute) {
        Aspect aspect = EMFModelUtil.getRootContainerOfType(attribute, RamPackage.Literals.ASPECT);

        EditingDomain domain = EMFEditUtil.getEditingDomain(aspect);

        Command command = this.createSetterOperationCommand(domain, aspect, attribute, attribute.getName(),
                RAMVisibilityType.PUBLIC);

        if (command != null) {
            doExecute(domain, command);
        }
    }

    /**
     * Creates an operation which sets the name of an attribute.
     *
     * @param domain the {@link EditingDomain} to use for executing commands
     * @param aspect The current aspect
     * @param attribute the attribute.
     * @param parameterName The name of the parameter
     * @param visibilityType The visibility type
     *
     * @return The created {@link Command}
     */
    private Command createSetterOperationCommand(EditingDomain domain, Aspect aspect, StructuralFeature attribute,
            String parameterName, RAMVisibilityType visibilityType) {

        Class owner = (Class) attribute.eContainer();
        int index = owner.getOperations().size();

        Operation operation = RAMModelUtil.createSetter(attribute, parameterName);
        if (operation == null) {
            return null;
        }

        CompoundCommand compoundCommand = new CompoundCommand();

        // we need to execute the command because we will need the result of this command in the
        // CreateAddMessageViewCommand operation.
        compoundCommand.appendAndExecute(createAddOperationCommand(domain, owner, operation, visibilityType, index));

        // Create Message view
        MessageView messageView = MessageViewUtil.createMessageView(operation);
        Interaction interaction = messageView.getSpecification();

        // We just create the messageView, so it contains only one lifeline.
        Lifeline lifeline = interaction.getLifelines().get(0);

        // Create Assignment statement
        AssignmentStatement assignmentStatement = RamFactory.eINSTANCE.createAssignmentStatement();
        OpaqueExpression specification = MessageViewUtil.createOpaqueExpression();
        specification.setBody(attribute.getName());
        assignmentStatement.setValue(specification);
        assignmentStatement.setAssignTo(attribute);
        assignmentStatement.getCovered().add(lifeline);

        // getFragments already contains two fragments (the two initials messages).
        // So we add at the position one the assignment fragment.
        interaction.getFragments().add(1, assignmentStatement);

        compoundCommand.appendAndExecute(createAddMessageViewCommand(domain, aspect, messageView,
                messageView.getSpecification()));

        return compoundCommand;
    }

    /**
     * Creates an operation which gets and sets the name of an attribute.
     *
     * @param attribute the attribute.
     */
    public void createGetterAndSetterOperation(StructuralFeature attribute) {
        Aspect aspect = EMFModelUtil.getRootContainerOfType(attribute, RamPackage.Literals.ASPECT);

        EditingDomain domain = EMFEditUtil.getEditingDomain(aspect);

        CompoundCommand compoundCommand = new CompoundCommand();

        Command getterCommand = this.createGetterOperationCommand(domain, aspect, attribute, RAMVisibilityType.PUBLIC);
        if (getterCommand != null) {
            compoundCommand.append(getterCommand);
        }

        Command setterCommand = this.createSetterOperationCommand(domain, aspect, attribute, attribute.getName(),
                RAMVisibilityType.PUBLIC);
        if (setterCommand != null) {
            compoundCommand.append(setterCommand);
        }

        doExecute(domain, compoundCommand);
    }

    /**
     * Creates a new constructor for the given class.
     *
     * @param owner the class to add a constructor to
     */
    public void createConstructor(Class owner) {

        Operation operation = RAMModelUtil.createOperation(owner, RAMModelUtil.CONSTRUCTOR_NAME,
                owner, RAMVisibilityType.PUBLIC, null, false);
        if (operation != null) {
            operation.setOperationType(OperationType.CONSTRUCTOR);
            int index = RAMModelUtil.findConstructorIndexFor(owner, OperationType.CONSTRUCTOR);
            EditingDomain domain = EMFEditUtil.getEditingDomain(owner);
            Command command =
                    changeOperationAndClassVisibilityCommand(domain, owner, operation, RAMVisibilityType.PUBLIC);
            CompoundCommand compundCommand = new CompoundCommand();
            compundCommand.append(command);
            compundCommand.append(AddCommand.create(domain, owner, RamPackage.Literals.CLASSIFIER__OPERATIONS,
                    operation,
                    index));
            doExecute(domain, compundCommand);
        }

    }

    /**
     * Creates a new destructor for the given class.
     *
     * @param owner the class to add a destructor to
     */
    public void createDestructor(Class owner) {
        RVoid voidType = RAMModelUtil.getVoidType((StructuralView) owner.eContainer());
        Operation operation = RAMModelUtil.createOperation(owner, RAMModelUtil.DESTRUCTOR_NAME, voidType, 
                RAMVisibilityType.PUBLIC,  null, false);
        if (operation != null) {
            operation.setOperationType(OperationType.DESTRUCTOR);
            int index = RAMModelUtil.findConstructorIndexFor(owner, OperationType.DESTRUCTOR);

            EditingDomain domain = EMFEditUtil.getEditingDomain(owner);
            Command command =
                    changeOperationAndClassVisibilityCommand(domain, owner, operation, RAMVisibilityType.PUBLIC);
            CompoundCommand compundCommand = new CompoundCommand();
            compundCommand.append(command);
            compundCommand.append(AddCommand.create(domain, owner, RamPackage.Literals.CLASSIFIER__OPERATIONS,
                    operation,
                    index));
            doExecute(domain, compundCommand);
        }
    }

    /**
     * Creates a copy of the given attribute with the same signature and with a desired name. Adds it to the proper
     * class as well.
     *
     * @param aspect Aspect root of the model
     * @param attribute the attribute to copy
     * @param newName the name of the attribute that is going to be created from the copy.
     */
    public void createAttributeCopy(Aspect aspect, Attribute attribute, String newName) {       
        EditingDomain domain = EMFEditUtil.getEditingDomain(aspect);
        CompoundCommand command = new CompoundCommand();
        
        Attribute localAttribute = RAMReferenceUtil.localizeElement(domain, command, aspect, attribute);
        localAttribute.setName(newName);
        
        doExecute(domain, command);
    }

    /**
     * Creates a new parameter. In case the affected operation is used in message views, all messages are updated by
     * adding a corresponding parameter value mapping.
     *
     * @param owner the operation the parameter should be added to
     * @param index the index the parameter should be added at
     * @param parameterString the user input for the new parameter
     */
    public void createParameter(Operation owner, int index, String parameterString) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);
        CompoundCommand compoundCommand = new CompoundCommand();

        Parameter parameter = StructuralViewUtil.createParameter(owner, parameterString);

        Type type = parameter.getType();
        EditingDomain domain = EMFEditUtil.getEditingDomain(owner);
        
        Type newType = RAMReferenceUtil.localizeElement(domain, compoundCommand, owner, type);
        
        parameter.setType(newType);

        compoundCommand.append(AddCommand.create(editingDomain, owner, RamPackage.Literals.OPERATION__PARAMETERS,
                parameter, index));

        // Find all messages where the operation is used as a signature.
        Collection<Message> affectedMessages = getAffectedMessages(owner);

        for (Message message : affectedMessages) {
            ParameterValueMapping parameterMapping = RamFactory.eINSTANCE.createParameterValueMapping();
            parameterMapping.setParameter(parameter);
            compoundCommand.append(AddCommand.create(editingDomain, message, RamPackage.Literals.MESSAGE__ARGUMENTS,
                    parameterMapping, index));
        }

        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Removes the given parameter. In case the affected operation is used in message views, all messages are updated by
     * removing the corresponding parameter value mapping.
     *
     * @param parameter the parameter to be removed
     */
    public void removeParameter(Parameter parameter) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(parameter.eContainer());
        CompoundCommand compoundCommand = new CompoundCommand();

        compoundCommand.append(RemoveCommand.create(editingDomain, parameter));

        Collection<Message> affectedMessages = getAffectedMessages(parameter.eContainer());

        for (Message message : affectedMessages) {
            for (ParameterValueMapping parameterMapping : message.getArguments()) {
                if (parameterMapping.getParameter() == parameter) {
                    compoundCommand.append(RemoveCommand.create(editingDomain, parameterMapping));
                }
            }
        }

        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Returns a collection of messages that have the given object set as the message's signature.
     *
     * @param objectOfInterest the object to search for
     * @return a collection of messages with the object as the signature
     */
    private Collection<Message> getAffectedMessages(EObject objectOfInterest) {
        Collection<Message> affectedMessages = new ArrayList<Message>();
        Collection<Message> crossReferences = EMFModelUtil.findCrossReferencesOfType(objectOfInterest, 
                    RamPackage.Literals.MESSAGE__SIGNATURE, RamPackage.Literals.MESSAGE);
        
        for (Message message : crossReferences) {
            /**
             * Don't consider those messages that are the first message of a message view or a reply.
             */
            if (message.getMessageSort() != MessageSort.REPLY
                    && !(message.getSendEvent() instanceof Gate)) {
                affectedMessages.add(message);
            }
        }

        return affectedMessages;
    }

    /**
     * Adds the given classifier as a super type to the given class. Makes sure that the given super type can actually
     * be added, i.e., there is no existing inheritance between the two classes in either direction.
     *
     * @param subType the sub class the super type should be added to
     * @param superType the super type of the sub class
     */
    public void addSuperType(Class subType, Classifier superType) {
        // Avoid if same inheritance already exists.
        if (!RAMModelUtil.hasSuperClass(subType, superType)
                // and inheritance on the same class
                && (superType != subType)
                // and inheritance in the opposite direction
                && !((superType instanceof Class) && RAMModelUtil.hasSuperClass((Class) superType, subType))) {
            doAdd(subType, RamPackage.Literals.CLASSIFIER__SUPER_TYPES, superType);
        }
    }

    /**
     * Removes the given classifier as the super type of the given class. Determines which of the given classes is the
     * sub-type and the super-type, i.e., it is not necessary to determine this beforehand.
     *
     * @param classifier1 the first classifier
     * @param classifier2 the second classifier
     */
    public void removeSuperType(Classifier classifier1, Classifier classifier2) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(classifier1);

        Class subType;
        Classifier superType;

        // figure out which is the super type
        if (classifier1 instanceof Class && RAMModelUtil.hasSuperClass((Class) classifier1, classifier2)) {
            subType = (Class) classifier1;
            superType = classifier2;
        } else {
            subType = (Class) classifier2;
            superType = classifier1;
        }

        // Create remove command.
        Command removeCommand =
                RemoveCommand.create(editingDomain, subType, RamPackage.Literals.CLASSIFIER__SUPER_TYPES, superType);
        doExecute(editingDomain, removeCommand);
    }

    /**
     * Switched the abstract property of the given operation, and if its class is not abstract, set it abstract.
     *
     * @param operation the operation
     */
    public void switchOperationAbstract(Operation operation) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(operation);

        CompoundCommand compoundCommand = new CompoundCommand();

        boolean isOperationAbstract = operation.isAbstract();
        compoundCommand.append(SetCommand.create(editingDomain, operation, RamPackage.Literals.OPERATION__ABSTRACT,
                !isOperationAbstract));

        if (!isOperationAbstract) {
            EObject clazz = operation.eContainer();
            boolean isClassAbstract = (Boolean) clazz.eGet(RamPackage.Literals.CLASSIFIER__ABSTRACT);
            if (!isClassAbstract) {
                compoundCommand.append(SetCommand.create(editingDomain, clazz, RamPackage.Literals.CLASSIFIER__ABSTRACT,
                        !isClassAbstract));
            }
        }

        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Switches the abstract property of the given class.
     *
     * @param clazz the class
     * @return true if the class has been switched. False otherwise.
     */
    public boolean switchAbstract(Class clazz) {
        if (!RAMModelUtil.classCanBeNotAbstract(clazz)) {
            return false;
        }
        doSwitch(clazz, RamPackage.Literals.CLASSIFIER__ABSTRACT);
        return true;
    }

    /**
     * Switches the static property of the given operation.
     *
     * @param operation the operation
     */
    public void switchOperationStatic(Operation operation) {
        doSwitch(operation, RamPackage.Literals.OPERATION__STATIC);
    }

    /**
     * Switches the static property of the given attribute.
     *
     * @param attribute the attribute
     */
    public void switchAttributeStatic(Attribute attribute) {
        doSwitch(attribute, RamPackage.Literals.STRUCTURAL_FEATURE__STATIC);
    }
    
    private COREPartialityType fromRAMToCOREPartialityType(RAMPartialityType ramPartiality) {
        if (ramPartiality == RAMPartialityType.PROTECTED) {
            return COREPartialityType.CONCERN;
        } else if (ramPartiality == RAMPartialityType.PUBLIC) {
            return COREPartialityType.PUBLIC;
        } else {
            return COREPartialityType.NONE;
        }
    }

    /**
     * Finds the corresponding type of the given type for the given {@link COREModelComposition}.
     *
     * @param oldType the type from the instantiated aspect
     * @param composition the composition
     * @param structuralView the structural view of the current aspect
     * @return the type from the higher-level aspect
     */
    // TODO: mschoettle: Move to RAMModelUtil. This method is messy, should be improved (maybe splitted).
    private Type getType(Type oldType, COREModelComposition composition, StructuralView structuralView) {
        Type newType = oldType;

        // handle collections
        if (oldType instanceof RCollection) {
            RCollection oldCollection = (RCollection) oldType;
            boolean found = false;

            // see if the collection already exists
            for (Type type : structuralView.getTypes()) {
                if (type.eClass().isInstance(oldType)) {
                    RCollection collection = (RCollection) type;
                    // check if there exists an appropriate type in the higher-level aspect
                    // e.g., if the type was mapped
                    Type collectionType = getType(oldCollection.getType(), composition, structuralView);

                    if (collectionType == collection.getType()) {
                        newType = type;
                        found = true;
                        break;
                    }
                }
            }

            // if no collection is found, create a new one
            if (!found) {
                RCollection newCollection = EcoreUtil.copy(oldCollection);
                Type oldCollectionType = newCollection.getType();
                newCollection.setType((ObjectType) getType(oldCollectionType, composition, structuralView));

                // TODO: Should use a command here
                // (but might cause confusion because the user doesn't see anything visual)
                structuralView.getTypes().add(newCollection);
                newType = newCollection;
            }
        } else if (!(oldType instanceof Class)) {
            // handle everything else, except classes (e.g., primitive types and special types)
            for (Type type : structuralView.getTypes()) {
                if (type.getName().equals(oldType.getName())) {
                    newType = type;
                }
            }
        }
        // if no types have been found yet, check the mappings
        if (oldType instanceof Classifier && newType == oldType) {
            for (COREModelElementComposition<?> modelElementComposition : composition.getCompositions()) {
                @SuppressWarnings("unchecked")
                COREMapping<Classifier> classifierMapping = (COREMapping<Classifier>) modelElementComposition;
                
                Classifier resolvedFrom = classifierMapping.getFrom();

                if (resolvedFrom == oldType && classifierMapping.getTo() != null) {
                    newType = classifierMapping.getTo();
                    // TODO: if a class is mapped to 2 or more classes we are now using the first one as the signature.
                    // you can inform the user about this.
                    break;
                }
            }
        }

        return newType;
    }

}
