package ca.mcgill.sel.ram.controller;

import java.util.Collections;
import java.util.List;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import ca.mcgill.sel.commons.StringUtil;
import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.EnumLiteralValue;
import ca.mcgill.sel.ram.InteractionFragment;
import ca.mcgill.sel.ram.Lifeline;
import ca.mcgill.sel.ram.Message;
import ca.mcgill.sel.ram.MessageSort;
import ca.mcgill.sel.ram.ObjectType;
import ca.mcgill.sel.ram.Parameter;
import ca.mcgill.sel.ram.ParameterValue;
import ca.mcgill.sel.ram.REnumLiteral;
import ca.mcgill.sel.ram.RamFactory;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.StructuralFeature;
import ca.mcgill.sel.ram.StructuralFeatureValue;
import ca.mcgill.sel.ram.Type;
import ca.mcgill.sel.ram.ValueSpecification;
import ca.mcgill.sel.ram.controller.util.RAMReferenceUtil;
import ca.mcgill.sel.ram.util.MessageViewUtil;
import ca.mcgill.sel.ram.util.RAMModelUtil;

/**
 * The controller for {@link Message}s.
 * 
 * @author mschoettle
 */
public class MessageController extends BaseController {

    /**
     * Creates a new instance of {@link MessageController}.
     */
    protected MessageController() {
        // Prevent anyone outside this package to instantiate.
    }

    /**
     * Creates a new specific value specification and adds it as the value of the given owner.
     * The value specification intended to be added are of type {@link ca.mcgill.sel.ram.LiteralSpecification}.
     * In case a literal specification is already set as the value specification, it is deleted first.
     * 
     * @param owner the object to add the value to
     * @param containingFeature the feature containing the value specification
     * @param eClass the {@link EClass} of the specific value specification subclass
     */
    public void addValueSpecification(EObject owner, EStructuralFeature containingFeature, EClass eClass) {
        EObject valueSpecification = RamFactory.eINSTANCE.create(eClass);
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);
        CompoundCommand compoundCommand = new CompoundCommand();
        
        executeAddValueSpecification(editingDomain, compoundCommand, owner, containingFeature, valueSpecification);
    }

    /**
     * Adds a value specification as the value of the given owner.
     * In case a value specification is already set, it is deleted first.
     * 
     * @param owner the object to add the value to
     * @param containingFeature the feature containing the value specification
     * @param valueSpecification the value specification to be added
     */
    public void addExpressionValueSpecification(EObject owner, 
            EStructuralFeature containingFeature, ValueSpecification valueSpecification) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);
        CompoundCommand compoundCommand = new CompoundCommand();

        executeAddValueSpecification(editingDomain, compoundCommand, owner, containingFeature, valueSpecification);
    }
    
    /**
     * Creates and adds a specific value specification ({@link StructuralFeatureValue}, {@link ParameterValue})
     * or {@link EnumLiteralValue} to the given owner and sets the given value as the value of the value specification.
     * The specific type to create depends on the actual type of the given value.
     * In case a literal specification is already set as the value specification, it is removed first.
     * 
     * @param owner the object to add the value to
     * @param containingFeature the feature containing the value specification
     * @param value the {@link EObject} to set as the value of the value specification to create
     */
    public void addReferenceValueSpecification(EObject owner, EStructuralFeature containingFeature,
            EObject value) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);
        CompoundCommand compoundCommand = new CompoundCommand();
        ValueSpecification valueSpecification = null;

        if (value instanceof Parameter) {
            ParameterValue parameterValue = RamFactory.eINSTANCE.createParameterValue();
            parameterValue.setParameter((Parameter) value);

            valueSpecification = parameterValue;
        } else if (value instanceof StructuralFeature) {
            StructuralFeature localFeature = (StructuralFeature) RAMReferenceUtil.localizeElement(
                    editingDomain, compoundCommand, owner, value);

            StructuralFeatureValue structuralFeatureValue = RamFactory.eINSTANCE.createStructuralFeatureValue();
            structuralFeatureValue.setValue(localFeature);
            
            valueSpecification = structuralFeatureValue;
        } else if (value instanceof REnumLiteral) {
            REnumLiteral localFeature = (REnumLiteral) RAMReferenceUtil.localizeElement(
                    editingDomain, compoundCommand, owner, value);

            EnumLiteralValue enumLiteralValue = RamFactory.eINSTANCE.createEnumLiteralValue();
            enumLiteralValue.setValue(localFeature);

            valueSpecification = enumLiteralValue;
        }
        
        executeAddValueSpecification(editingDomain, compoundCommand, owner, containingFeature, valueSpecification);
    }

    /**
     * Adds the given value specification to the given owner and executes the command.
     * In case a value exists already, it is removed first.
     * 
     * @param editingDomain the editing domain
     * @param command the command
     * @param owner the object to add the value to
     * @param containingFeature the feature containing the value specification
     * @param valueSpecification the specific {@link ValueSpecification} to add
     */
    private void executeAddValueSpecification(EditingDomain editingDomain, CompoundCommand command,
            EObject owner, EStructuralFeature containingFeature, EObject valueSpecification) {
        // Check if there is an existing value.
        Object currentValue = owner.eGet(containingFeature);

        // Delete existing value specifications
        // Note: This is not totally necessary, because the set command just overwrites.
        if (currentValue != null) {
            command.append(RemoveCommand.create(editingDomain, currentValue));
        }

        command.append(SetCommand.create(editingDomain, owner, containingFeature, valueSpecification));
        
        doExecute(editingDomain, command);
    }

    /**
     * Creates a new temporary property and sets it as the "assignTo" of the message.
     * Also adds the temporary property to the initial message.
     * If there is an existing temporary property, it is deleted in addition.
     * 
     * @param message the message for which the "assignTo" to set for
     */
    public void createTemporaryAssignment(Message message) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(message);

        InteractionFragment sendEvent = (InteractionFragment) message.getSendEvent();
        Message initialMessage = MessageViewUtil.findInitialMessage(sendEvent);

        Type returnType = message.getSignature().getReturnType();
        if (returnType instanceof Classifier) {
            Aspect aspect = EMFModelUtil.getRootContainerOfType(message, RamPackage.Literals.ASPECT);
            returnType = RAMModelUtil.resolveClassifier(aspect, (Classifier) returnType);
        }

        // Determine proper initial name of property.
        String name = message.getSignature().getName().replaceFirst("^get", "");
        // If the message is called "get" the name will be empty.
        if (name.isEmpty()) {
            name = returnType.getName();
        }

        // Make sure the property is lower case
        name = StringUtil.toLowerCaseFirst(name);

        // Create temporary property.
        ObjectType objectType = (ObjectType) returnType;
        StructuralFeature property = FragmentsController.createTemporaryProperty(initialMessage, name, objectType);

        CompoundCommand compoundCommand = new CompoundCommand();

        // Unused temporary properties (after changing it) need to be removed first.
        StructuralFeature structuralFeature = message.getAssignTo();
        
        List<InteractionFragment> affectedFragments =
                Collections.<InteractionFragment>singletonList(sendEvent);
        FragmentsController.appendRemoveTemporaryPropertyCommand(editingDomain, compoundCommand, sendEvent, 
                structuralFeature, affectedFragments);

        compoundCommand.append(AddCommand.create(editingDomain, initialMessage,
                RamPackage.Literals.MESSAGE__LOCAL_PROPERTIES, property));
        compoundCommand.append(SetCommand.create(editingDomain, message,
                RamPackage.Literals.MESSAGE__ASSIGN_TO, property));

        // Create messages require that the lifelines represent is replaced with the new assignment,
        // because the lifeline is represented with what the new object is assigned to.
        // E.g., foo := create() [on lifeline foo : Foo]
        if (message.getMessageSort() == MessageSort.CREATE_MESSAGE) {
            InteractionFragment receiveEvent = (InteractionFragment) message.getReceiveEvent();
            Lifeline lifeline = receiveEvent.getCovered().get(0);

            compoundCommand.append(SetCommand.create(editingDomain, lifeline,
                    RamPackage.Literals.LIFELINE__REPRESENTS, property));
        }

        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Changes the "assignTo" of the message to the given.
     * It might not be set yet, if it is and it is a temporary property, the existing one is deleted.
     * In case of a create message the represents of the lifeline is updated to the new value.
     * 
     * @param message the message the "assignTo" to change of
     * @param assignTo the new "assignTo" value to change to
     */
    public void changeAssignTo(Message message, StructuralFeature assignTo) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(message);
                
        CompoundCommand compoundCommand = new CompoundCommand();
        
        // Unused temporary properties (after changing it) need to be removed first.
        InteractionFragment sendEvent = (InteractionFragment) message.getSendEvent();
        StructuralFeature structuralFeature = message.getAssignTo();
        
        List<InteractionFragment> affectedFragments =
                Collections.<InteractionFragment>singletonList(sendEvent);
        FragmentsController.appendRemoveTemporaryPropertyCommand(editingDomain, compoundCommand, sendEvent, 
                structuralFeature, affectedFragments);
        
        /**
         * Ensure that the structural feature to which we assigned, if from another model, is "localized" 
         * as a reference.
         */
        StructuralFeature localFeature = RAMReferenceUtil.localizeElement(editingDomain, compoundCommand,
                message, assignTo);
        
        compoundCommand.append(SetCommand.create(editingDomain, message,
                RamPackage.Literals.MESSAGE__ASSIGN_TO, localFeature));
        
        // Create messages require that the lifelines represent is replaced with the new assignment,
        // because the lifeline is represented with what the new object is assigned to.
        // E.g., foo := create() [on lifeline foo : Foo]
        if (message.getMessageSort() == MessageSort.CREATE_MESSAGE) {
            InteractionFragment receiveEvent = (InteractionFragment) message.getReceiveEvent();
            Lifeline lifeline = receiveEvent.getCovered().get(0);
        
            compoundCommand.append(SetCommand.create(editingDomain, lifeline,
                    RamPackage.Literals.LIFELINE__REPRESENTS, assignTo));
        }
        
        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Changes the "returns" value of the given message.
     * 
     * @param message the message
     * @param value the value
     */
    public void changeReturnValue(Message message, ValueSpecification value) {
        doSet(message, RamPackage.Literals.MESSAGE__RETURNS, value);
    }

}
