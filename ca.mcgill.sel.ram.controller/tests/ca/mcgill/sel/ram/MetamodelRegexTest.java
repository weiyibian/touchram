package ca.mcgill.sel.ram;

//CHECKSTYLE:OFF Temporary location in controller project (therefore this package is not added to the exclusions).
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.hasItem;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Assume;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;

import ca.mcgill.sel.ram.util.MetamodelRegex;

@RunWith(Theories.class)
public class MetamodelRegexTest {
    
    @DataPoints
    public static final String[] classRegex_trueValues = new String[] { "Test", "CamelCase", "Test1", "Test<T>", "Test<int>" };
    
    @DataPoints
    public static final String[] classRegex_falseValues = new String[] { "", "1", "1Test", "test", "Test-_.", "A<", "A<X", "A<0>" };
    
    @DataPoints
    public static final String[] typeRegex_trueValues = new String[] { "a", "A", "AAA", "A1", "Test<T>", "Test<test>" };
    
    @DataPoints
    public static final String[] typeRegex_falseValues = new String[] { "", "1", "1Test", "Test-_.", "A,B", "A<", "A<X", "A<0>" };
    
    @DataPoints
    public static final String[] typeNameRegex_trueValues = new String[] { "a", "A", "AAA", "A1" };
    
    @DataPoints
    public static final String[] typeNameRegex_falseValues = new String[] { "", "1", "1Test", "Test-_.", "Test<T>", "Test<test>", "A<0>" };
    
    @DataPoints
    public static final String[] arrayRegex_trueValues = new String[] { "int[]", "A[]", "Foo[]", "int[][]", "int[5]", "int[5][]", "String[5][5]" };
    
    @DataPoints
    public static final String[] arrayRegex_falseValues = new String[] { "", "1", "a", "int", "int[", "int[0", "int[a]", "int[][][][b]", "1int[]", "Foo.Bar[]" };
    
    @DataPoints
    public static final String[] visibilityRegex_trueValues = new String[] { "+", "-", "~", "#" };
    
    @DataPoints
    public static final String[] visibilityRegex_falseValues = new String[] { "", "1", "a", "Test", ".", "$", "*" };
    
    @DataPoints
    public static final String[] enumLiteralRegex_trueValues = new String[] { "a", "A", "AAA", "A1", "A_B" };
    
    @DataPoints
    public static final String[] enumLiteralRegex_falseValues = new String[] { "", "1", ".", "A_", "_A", "A-B", "A,B", "A.B", "$", "*", " A" };
    
    @DataPoints
    public static final String[] enumLiteralsRegex_trueValues = new String[] { "a,b", "A, B", "A,    B    ,        C", "a", "A", "A1", "A_B", "A_B,A_B" };
    
    @DataPoints
    public static final String[] enumLiteralsRegex_falseValues = new String[] { "A,", ",b", "a,b,", ",", ",,b", "A_", " A, B" };
    
    @Theory
    public void testClassNameRegex_true(String input) {
        Assume.assumeThat(Arrays.asList(classRegex_trueValues), hasItem(input));
        
        Matcher matcher = Pattern.compile(MetamodelRegex.REGEX_CLASS_NAME).matcher(input);
        assertThat(matcher.matches()).isTrue();
    }
    
    @Theory
    public void testClassNameRegex_false(String input) {
        Assume.assumeThat(Arrays.asList(classRegex_falseValues), hasItem(input));
        
        Matcher matcher = Pattern.compile(MetamodelRegex.REGEX_CLASS_NAME).matcher(input);
        assertThat(matcher.matches()).isFalse();
    }
    
    @Theory
    public void testTypeRegex_true(String input) {
        Assume.assumeThat(Arrays.asList(typeRegex_trueValues), hasItem(input));

        Matcher matcher = Pattern.compile(MetamodelRegex.REGEX_TYPE).matcher(input);
        assertThat(matcher.matches()).isTrue();
    }
    
    @Theory
    public void testTypeRegex_false(String input) {
        Assume.assumeThat(Arrays.asList(typeRegex_falseValues), hasItem(input));
        
        Matcher matcher = Pattern.compile(MetamodelRegex.REGEX_TYPE).matcher(input);
        assertThat(matcher.matches()).isFalse();
    }
    
    @Theory
    public void testTypeNameRegex_true(String input) {
        Assume.assumeThat(Arrays.asList(typeNameRegex_trueValues), hasItem(input));

        Matcher matcher = Pattern.compile(MetamodelRegex.REGEX_TYPE_NAME).matcher(input);
        assertThat(matcher.matches()).isTrue();
    }
    
    @Theory
    public void testTypeNameRegex_false(String input) {
        Assume.assumeThat(Arrays.asList(typeNameRegex_falseValues), hasItem(input));
        
        Matcher matcher = Pattern.compile(MetamodelRegex.REGEX_TYPE_NAME).matcher(input);
        assertThat(matcher.matches()).isFalse();
    }
    
    @Theory
    public void testArrayTypeRegex_true(String input) {
        Assume.assumeThat(Arrays.asList(arrayRegex_trueValues), hasItem(input));

        Matcher matcher = Pattern.compile(MetamodelRegex.REGEX_ARRAY_TYPE).matcher(input);
        assertThat(matcher.matches()).isTrue();
    }
    
    @Theory
    public void testArrayTypeRegex_false(String input) {
        Assume.assumeThat(Arrays.asList(arrayRegex_falseValues), hasItem(input));
        
        Matcher matcher = Pattern.compile(MetamodelRegex.REGEX_ARRAY_TYPE).matcher(input);
        assertThat(matcher.matches()).isFalse();
    }
    
    @Theory
    public void testVisibilityRegex_true(String input) {
        Assume.assumeThat(Arrays.asList(visibilityRegex_trueValues), hasItem(input));

        Matcher matcher = Pattern.compile(MetamodelRegex.REGEX_VISIBILITY).matcher(input);
        assertThat(matcher.matches()).isTrue();
    }
    
    @Theory
    public void testVisibilityRegex_false(String input) {
        Assume.assumeThat(Arrays.asList(visibilityRegex_falseValues), hasItem(input));
        
        Matcher matcher = Pattern.compile(MetamodelRegex.REGEX_VISIBILITY).matcher(input);
        assertThat(matcher.matches()).isFalse();
    }
    
    @Theory
    public void testEnumLiteralRegex_true(String input) {
        Assume.assumeThat(Arrays.asList(enumLiteralRegex_trueValues), hasItem(input));

        Matcher matcher = Pattern.compile(MetamodelRegex.REGEX_ENUM_LITERAL).matcher(input);
        assertThat(matcher.matches()).isTrue();
    }
    
    @Theory
    public void testEnumLiteralRegex_false(String input) {
        Assume.assumeThat(Arrays.asList(enumLiteralRegex_falseValues), hasItem(input));
        
        Matcher matcher = Pattern.compile(MetamodelRegex.REGEX_ENUM_LITERAL).matcher(input);
        assertThat(matcher.matches()).isFalse();
    }
    
    @Theory
    public void testEnumLiteralsRegex_true(String input) {
        Assume.assumeThat(Arrays.asList(enumLiteralsRegex_trueValues), hasItem(input));

        Matcher matcher = Pattern.compile(MetamodelRegex.REGEX_ENUM_LITERALS).matcher(input);
        assertThat(matcher.matches()).isTrue();
    }
    
    @Theory
    public void testEnumLiteralsRegex_false(String input) {
        Assume.assumeThat(Arrays.asList(enumLiteralsRegex_falseValues), hasItem(input));
        
        Matcher matcher = Pattern.compile(MetamodelRegex.REGEX_ENUM_LITERALS).matcher(input);
        assertThat(matcher.matches()).isFalse();
    }
    
}
