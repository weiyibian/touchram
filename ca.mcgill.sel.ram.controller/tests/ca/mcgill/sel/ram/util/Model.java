package ca.mcgill.sel.ram.util;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Indicates that a method declaration of a test case should use the given model.
 * 
 * @author mschoettle
 */
@Retention(value = RetentionPolicy.RUNTIME)
@Target(value = ElementType.METHOD)
public @interface Model {
    
    /**
     * Returns the file name of the intended model.
     * 
     * @return the model file name
     */
    String fileName();
    
}
