/**
 */
package ca.mcgill.sel.ram;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>RByte</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.mcgill.sel.ram.RamPackage#getRByte()
 * @model
 * @generated
 */
public interface RByte extends PrimitiveType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot body='\'byte\''"
	 * @generated
	 */
	String getName();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot body='\'java.lang.Byte\''"
	 * @generated
	 */
	String getInstanceClassName();

} // RByte
