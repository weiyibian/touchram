/**
 */
package ca.mcgill.sel.ram;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Woven Aspect</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.ram.WovenAspect#getComesFrom <em>Comes From</em>}</li>
 *   <li>{@link ca.mcgill.sel.ram.WovenAspect#getChildren <em>Children</em>}</li>
 *   <li>{@link ca.mcgill.sel.ram.WovenAspect#getWovenElements <em>Woven Elements</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.ram.RamPackage#getWovenAspect()
 * @model
 * @generated
 */
public interface WovenAspect extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Comes From</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comes From</em>' reference.
	 * @see #setComesFrom(Aspect)
	 * @see ca.mcgill.sel.ram.RamPackage#getWovenAspect_ComesFrom()
	 * @model required="true"
	 * @generated
	 */
	Aspect getComesFrom();

	/**
	 * Sets the value of the '{@link ca.mcgill.sel.ram.WovenAspect#getComesFrom <em>Comes From</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Comes From</em>' reference.
	 * @see #getComesFrom()
	 * @generated
	 */
	void setComesFrom(Aspect value);

	/**
	 * Returns the value of the '<em><b>Children</b></em>' containment reference list.
	 * The list contents are of type {@link ca.mcgill.sel.ram.WovenAspect}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Children</em>' containment reference list.
	 * @see ca.mcgill.sel.ram.RamPackage#getWovenAspect_Children()
	 * @model containment="true"
	 * @generated
	 */
	EList<WovenAspect> getChildren();

	/**
	 * Returns the value of the '<em><b>Woven Elements</b></em>' map.
	 * The key is of type {@link ca.mcgill.sel.ram.Traceable},
	 * and the value is of type {@link ca.mcgill.sel.ram.Traceable},
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Woven Elements</em>' map.
	 * @see ca.mcgill.sel.ram.RamPackage#getWovenAspect_WovenElements()
	 * @model mapType="ca.mcgill.sel.ram.TracingMap&lt;ca.mcgill.sel.ram.Traceable, ca.mcgill.sel.ram.Traceable&gt;"
	 * @generated
	 */
	EMap<Traceable, Traceable> getWovenElements();

} // WovenAspect
