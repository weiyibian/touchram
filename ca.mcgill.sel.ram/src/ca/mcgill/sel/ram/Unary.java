/**
 */
package ca.mcgill.sel.ram;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Unary</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.ram.Unary#getExpression <em>Expression</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.ram.RamPackage#getUnary()
 * @model abstract="true"
 * @generated
 */
public interface Unary extends Operator {
	/**
	 * Returns the value of the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expression</em>' containment reference.
	 * @see #setExpression(ValueSpecification)
	 * @see ca.mcgill.sel.ram.RamPackage#getUnary_Expression()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ValueSpecification getExpression();

	/**
	 * Sets the value of the '{@link ca.mcgill.sel.ram.Unary#getExpression <em>Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Expression</em>' containment reference.
	 * @see #getExpression()
	 * @generated
	 */
	void setExpression(ValueSpecification value);

} // Unary
