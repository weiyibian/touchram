/**
 */
package ca.mcgill.sel.ram;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Conditional</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.mcgill.sel.ram.RamPackage#getConditional()
 * @model
 * @generated
 */
public interface Conditional extends Ternary {
} // Conditional
