/**
 */
package ca.mcgill.sel.ram;

import ca.mcgill.sel.core.COREMapping;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Classifier Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.mcgill.sel.ram.RamPackage#getClassifierMapping()
 * @model
 * @generated
 */
public interface ClassifierMapping extends COREMapping<Classifier> {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	EList<AttributeMapping> getAttributeMappings();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	EList<OperationMapping> getOperationMappings();

} // ClassifierMapping
