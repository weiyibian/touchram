/**
 */
package ca.mcgill.sel.ram;

import ca.mcgill.sel.core.COREMapping;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Enum Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.mcgill.sel.ram.RamPackage#getEnumMapping()
 * @model
 * @generated
 */
public interface EnumMapping extends COREMapping<REnum> {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	EList<EnumLiteralMapping> getEnumLiteralMappings();

} // EnumMapping
