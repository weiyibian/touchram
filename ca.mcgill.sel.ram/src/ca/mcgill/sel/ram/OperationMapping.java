/**
 */
package ca.mcgill.sel.ram;

import ca.mcgill.sel.core.COREMapping;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operation Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.mcgill.sel.ram.RamPackage#getOperationMapping()
 * @model
 * @generated
 */
public interface OperationMapping extends COREMapping<Operation> {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	EList<ParameterMapping> getParameterMappings();

} // OperationMapping
