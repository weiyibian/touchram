/**
 */
package ca.mcgill.sel.ram.util;

import java.util.Map;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.resource.Resource;

import org.eclipse.emf.ecore.resource.impl.ResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource Factory</b> associated with the package.
 * <!-- end-user-doc -->
 * @see ca.mcgill.sel.ram.util.RamResourceImpl
 * @generated
 */
public class RamResourceFactoryImpl extends ResourceFactoryImpl {
    /**
	 * Creates an instance of the resource factory.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public RamResourceFactoryImpl() {
		super();
	}

    /**
     * Creates an instance of the resource.
     * 
     * @generated NOT
     */
    @Override
    public Resource createResource(URI uri) {
        Resource result = new RamResourceImpl(uri);

        Map<Object, Object> defaultLoadOptions = ((XMLResourceImpl) result).getDefaultLoadOptions();
        defaultLoadOptions.put(XMLResource.OPTION_RESOURCE_HANDLER, new RamResourceHandler());

        return result;
    }

} // RamResourceFactoryImpl
