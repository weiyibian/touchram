package ca.mcgill.sel.ram.util;

import java.io.InputStream;
import java.util.Map;

import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.BasicResourceHandler;

import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.RamFactory;
import ca.mcgill.sel.ram.StructuralView;

/**
 * Allows methods to be executed relative to loading a new resource.
 * 
 * @see ca.mcgill.sel.ram.util.RamResourceFactoryImpl.createResource
 * @author Alec Harmon Blumenfeld
 */
public class RamResourceHandler extends BasicResourceHandler {
    
    /**
     * After loading the resource make sure that the StructuralView has all of the available types.
     * 
     * @param resource The Ram resource
     * @param inputStream the input
     */
    @Override
    public void postLoad(XMLResource resource, InputStream inputStream, Map<?, ?> options) {
        super.postLoad(resource, inputStream, options);
        
        Aspect aspect = (Aspect) resource.getContents().get(0);
        
        StructuralView structuralView = aspect.getStructuralView();
        
        // Create the structural view if it doesn't exist yet.
        if (structuralView == null) {
            structuralView = RamFactory.eINSTANCE.createStructuralView();
            aspect.setStructuralView(structuralView);
        }
        
        RAMModelUtil.createDefaultTypes(structuralView);
    }
}
