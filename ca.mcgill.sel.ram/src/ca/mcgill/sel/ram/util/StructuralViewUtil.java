package ca.mcgill.sel.ram.util;

import java.lang.reflect.GenericArrayType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.WildcardType;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;

import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.ram.Attribute;
import ca.mcgill.sel.ram.Class;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.ImplementationClass;
import ca.mcgill.sel.ram.ObjectType;
import ca.mcgill.sel.ram.Operation;
import ca.mcgill.sel.ram.Parameter;
import ca.mcgill.sel.ram.PrimitiveType;
import ca.mcgill.sel.ram.RAMVisibilityType;
import ca.mcgill.sel.ram.RArray;
import ca.mcgill.sel.ram.RCollection;
import ca.mcgill.sel.ram.REnum;
import ca.mcgill.sel.ram.RamFactory;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.StructuralView;
import ca.mcgill.sel.ram.Type;
import ca.mcgill.sel.ram.TypeParameter;

/**
 * Helper class with convenient static methods for working with structural view model objects.
 * 
 * @author arthurls
 */
public final class StructuralViewUtil {
       
    /**
     * Symbol used to display public elements.
     */
    public static final String SYMBOL_PUBLIC = "+";
    /**
     * Symbol used to display private elements.
     */
    public static final String SYMBOL_PRIVATE = "-";
    /**
     * Symbol used to display protected elements.
     */
    public static final String SYMBOL_PROTECTED = "#";
    /**
     * Symbol used to display package visiblity elements.
     */
    public static final String SYMBOL_PACKAGE = "~";

    /**
     * Delimiter of a parameter (provided by the user) can be "," and optionally be followed by a whitespace.
     */
    private static final String PARAMETER_DELIMITER_PATTERN = ",\\s?";
    

    private static final String GENERIC_DELIMITER_BEGIN = "<";

    private static final String GENERIC_DELIMITER_END = ">";

    private static final String ARRAY_DELIMITER_EMPTY = "[]";
    /**
     * Supress Default Contructor.
     */
    private StructuralViewUtil() {
    }
    
    /**
     * Creates a new attribute based on the given attribute string (<code>"[typeName] [attributeName]"</code>).
     * The attribute is added to the class at the given index.
     *
     * @param owner the {@link Class} the attribute should be added to
     * @param attributeString the textual description of the attribute
     * 
     * @return the newly created attribute
     * @throws IllegalArgumentException if the given string does not match the expected pattern
     */
    public static Attribute createAttribute(Class owner, String attributeString) {
        
        Matcher matcher = matchAttributeByGroup(attributeString);

        StructuralView structuralView = (StructuralView) owner.eContainer();
        
        String name = matcher.group(3);
        ObjectType type = getTypeByNameIdentifier(matcher.group(1), structuralView);
        
        if (!RAMModelUtil.isUniqueName(owner, RamPackage.Literals.CLASSIFIER__ATTRIBUTES, name)) {
            throw new IllegalArgumentException("The " + name + " attribute is not unique for attributes");
        }
        
        Attribute attribute = RamFactory.eINSTANCE.createAttribute();
        attribute.setName(name);
        attribute.setType(type);
        
        return attribute;
    }

    /**
     * Creates a new operation based on the given operation string.
     * The string should match the following pattern:
     * <code>"[visibility] [returnType] [operationName]([optional parameters])"</code>.
     * The parameters need to be separated by comma and have to match the following pattern:
     * <code>"[parameterType] [parameterName]"</code>.
     * The operation is added to the class at the given index.
     *
     * @param owner the {@link Class} the operation should be added to
     * @param operationString the textual description of the operation
     * @return the newly created operation. null if duplicate
     * @throws IllegalArgumentException if the given string does not match the expected pattern
     */
    public static Operation createOperation(Class owner, String operationString) {
        Matcher matcher = matchOperationByGroup(operationString);
        RAMVisibilityType ramVisibility = getRamVisibilityFromStringRepresentation(matcher.group(1));
        if (ramVisibility == null) {
            throw new IllegalArgumentException("The visibility is not valid");
        }
        StructuralView structuralView = (StructuralView) owner.eContainer();
        Type returnType = getType(matcher.group(2), structuralView);
        String name = matcher.group(4);
        List<Parameter> parameters = getParameters(matcher.group(5), structuralView);

        return RAMModelUtil.createOperation(owner, name, returnType, ramVisibility, parameters);
    }
    
    /**
     * Creates a new parameter in the model.
     *
     * @param owner the operation the parameter should be added to
     * @param parameterString the string representation of the parameter (e.g., "&lt;type> &lt;parameterName>)
     * 
     * @return the newly created parameter
     */
    public static Parameter createParameter(Operation owner, String parameterString) {
        Matcher matcher =
                Pattern.compile("^" + MetamodelRegex.REGEX_PARAMETER_DECLARATION + "$").matcher(parameterString);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("String " + parameterString
                    + " does not conform to parameter syntax");
        }
        
        StructuralView structuralView = EMFModelUtil.getRootContainerOfType(owner, RamPackage.Literals.STRUCTURAL_VIEW);
        Type type = StructuralViewUtil.getTypeByName(matcher.group(1), structuralView);
        String nameString = matcher.group(3);
        if (type == null) {
            throw new IllegalArgumentException("No type with that name exists");
        }

        if (nameString == null) {
            throw new IllegalArgumentException("Parameter name did not match naming syntax");
        }
        
        EStructuralFeature containingFeature = RamPackage.Literals.OPERATION__PARAMETERS;
        if (!RAMModelUtil.isUnique(owner, containingFeature, nameString, type)) {
            throw new IllegalArgumentException("Parameter names duplicate");
        }

        Parameter parameter = RamFactory.eINSTANCE.createParameter();
        parameter.setName(nameString);
        parameter.setType(type);
        
        return parameter;
    }
    
    /**
     * Creates and returns a parameter based on the given parameter string.
     * The parameter needs to match following pattern: <code>"[parameterType] [parameterName]"</code>.
     *
     * @param parameterString the textual description of the parameter
     * @param structuralView to get the list of types
     * @return the {@link Parameter} based on the given string
     * @throws IllegalArgumentException if the string does not match the expected pattern
     */
    private static Parameter getParameter(String parameterString, StructuralView structuralView) {
        
        Matcher matcher = Pattern.compile(MetamodelRegex.REGEX_ATTRIBUTE_DECLARATION).matcher(parameterString);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("The type " + parameterString + " does not exist in the model");
        }
        
        Parameter parameter = RamFactory.eINSTANCE.createParameter();
        
        Type type = getType(matcher.group(1), structuralView);
        parameter.setType(type);
        parameter.setName(matcher.group(3));

        return parameter;
    }

    /**
     * Returns a list of parameters based on the given string.
     * The parameters need to be separated by comma and have to match the following pattern:
     * <code>"[parameterType] [parameterName]"</code>.
     *
     * @param parametersString the comma separated list of parameter strings
     * @param structuralView to get the list of types needed in getParameter()
     * @return the list of {@link Parameter}s
     * @see #getParameter(String)
     */
    private static List<Parameter> getParameters(String parametersString, StructuralView structuralView) {
        List<Parameter> parameters = new ArrayList<Parameter>();

        if (parametersString != null && !parametersString.isEmpty()) {
            String[] parametersList = parametersString.split(PARAMETER_DELIMITER_PATTERN);

            for (String parameterString : parametersList) {
                Parameter parameter = getParameter(parameterString, structuralView);
                parameters.add(parameter);
            }
        }

        return parameters;
    }

    /**
     * Returns the primitive type that matches the given type name.
     *
     * @param typeString the type name of the type
     * @param structuralView the current {@link StructuralView}
     * @return the {@link PrimitiveType} that matches the name of the type
     * @throws IllegalArgumentException if the primitive type does not exist
     */
    private static ObjectType getTypeByNameIdentifier(String typeString, StructuralView structuralView) {
        ObjectType type = getAttributeTypeByName(typeString, structuralView);

        if (type == null) {
            throw new IllegalArgumentException("The structural view does not contain a primitive type " + type);
        }

        return type;
    }

    /**
     * Returns the type that matches the given type name.
     *
     * @param typeString the type name
     * @param structuralView the current {@link StructuralView}
     * @return the {@link Type} that matches the name of the type
     * @throws IllegalArgumentException if the type does not exist
     */
    private static Type getType(String typeString, StructuralView structuralView) {
        Type returnType = getTypeByName(typeString, structuralView);
        
        if (returnType == null) {
            throw new IllegalArgumentException("The type " + typeString + " does not exist in the model");
        }

        return returnType;
    }
    
    /**
     * Returns a matcher that divided the operation string into groups.
     * 
     * @param operationString the textual description of the operation
     * @return the matcher that holds the groups within the string 
     */
    private static Matcher matchOperationByGroup(String operationString) {
        Matcher matcher =
                Pattern.compile("^" + MetamodelRegex.REGEX_OPERATION_DECLARATION + "$").matcher(operationString);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("The string " + operationString
                    + " does not conform to operation syntax");
        }
        
        return matcher;
    }
    
    /**
     * Returns a matcher that divided the operation string into groups.
     * 
     * @param attributeString the textual description of the attribute
     * @return the matcher that holds the groups within the string 
     */
    private static Matcher matchAttributeByGroup(String attributeString) {
        Matcher matcher = Pattern.compile(MetamodelRegex.REGEX_ATTRIBUTE_DECLARATION).matcher(attributeString);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("The string " + attributeString + " is not valid syntax for attributes");
        }
        
        return matcher;
    }

    /**
     * Returns the {@link ObjectType} object with the given name that is contained in the given
     * {@link StructuralView}.
     *
     * @param name
     *            the name of the {@link ObjectType} that is looked for
     * @param structuralView
     *            the {@link StructuralView} containing the types
     * @return the {@link ObjectType} object of the given {@link StructuralView} that corresponds to the given name
     */
    public static ObjectType getAttributeTypeByName(String name, StructuralView structuralView) {
        
        if (name.matches(MetamodelRegex.REGEX_ARRAY_TYPE)) {
            return getArrayByName(name, true, structuralView);
        }
        
        for (ObjectType type : RAMInterfaceUtil.getAvailableAttributeTypes(structuralView)) {
            if (type.getName() != null && type.getName().toLowerCase().equals(name.toLowerCase())) {
                return type;
            }
        }

        return null;
    }

    /**
     * Returns the type for the given name, if it exists, of the given structural view.
     *
     * @param name
     *            The name of the type to get.
     * @param structuralView
     *            The structural view of interest
     * @return The type associated with that name, null if no such type exists.
     */
    // CHECKSTYLE:IGNORE ReturnCount: This is definitively the best way to write this piece of code.
    public static Type getTypeByName(String name, StructuralView structuralView) {
        if ("void".equals(name)) {
            return RAMModelUtil.getVoidType(structuralView);
        }
        
        if ("*".equals(name)) {
            return RAMModelUtil.getAnyType(structuralView);
        }

        if (name.matches(MetamodelRegex.REGEX_ARRAY_TYPE)) {
            return getArrayByName(name, false, structuralView);
        }

        // if instance class name
        if (!name.contains(".")) {
            // TODO: find a better way
            // handle collections special for now
            if (name.contains(GENERIC_DELIMITER_BEGIN)) {
                // this will result in the type at index 0 and the typed parameter in index 1
                String[] parameterizedType = name.split("<|>");

                // get the type
                RCollection type = null;
                for (Type currentType : structuralView.getTypes()) {
                    // TODO: once the name of collections is not set by OCL in the way of Set<String>
                    // instead of name just the type name has to be used (parameterizedType[0])
                    if (currentType.getName().equals(name)) {
                        type = (RCollection) currentType;
                    }
                }

                // if the type hasn't been found we have to create it
                if (type == null) {
                    EClass typeClass = (EClass) RamPackage.eINSTANCE.getEClassifier("R" + parameterizedType[0]);

                    // the requested type doesn't exist
                    if (typeClass == null) {
                        return null;
                    }

                    // create a new instance of the type
                    type = (RCollection) RamFactory.eINSTANCE.create(typeClass);

                    // this ensures that we also find primitive types
                    ObjectType collectionType = (ObjectType) getTypeByName(parameterizedType[1], structuralView);

                    // if the class doesn't exist we cannot create the type
                    if (collectionType == null) {
                        return null;
                    }

                    type.setType(collectionType);
                    structuralView.getTypes().add(type);
                }

                return type;
            }
            for (PrimitiveType primType : RAMInterfaceUtil.getAvailablePrimitiveTypes(structuralView)) {
                if (primType.getName().equals(name)) {
                    return primType;
                }
            }
            for (Classifier clazz : RAMInterfaceUtil.getAvailableClasses(structuralView)) {
                if (clazz.getName() != null && clazz.getName().equals(name)) {
                    return clazz;
                }
            }
        } else {
            if ("java.lang.String".equals(name)) {
                return getTypeByName("String", structuralView);
            }
            for (Classifier clazz : structuralView.getClasses()) {
                if (clazz instanceof ImplementationClass) {
                    if (name.equals(getClassNameWithGenerics(clazz))) {
                        return clazz;
                    }
                }
            }
            for (Type type : structuralView.getTypes()) {
                if (type instanceof REnum) {
                    if (((REnum) type).getInstanceClassName() != null) {
                        if (name.equals(((REnum) type).getInstanceClassName())) {
                            return type;
                        }
                    }
                }
            }
        }

        return null;
    }

    /**
     * Returns the existing array of the structural view if it exists.
     * Otherwise it creates a new array with the appropriate properties,
     * adds it to the structural view and returns it.
     * The array size may also be specified, but is optional.
     *
     * @param name the fully qualified name of the array (e.g., int[])
     * @param simpleTypesOnly whether only primitive or data types as array type are allowed
     * @param structuralView the {@link StructuralView} of interest
     * @return the {@link RArray} corresponding to the given name,
     *         null if the primitive types only constraint is violated
     */
    private static RArray getArrayByName(String name, boolean simpleTypesOnly, StructuralView structuralView) {
        RArray type = null;

        for (Type currentType : structuralView.getTypes()) {
            if (currentType.getName().equals(name)) {
                type = (RArray) currentType;
            }
        }

        if (type == null) {
            int arrayIndex = name.lastIndexOf('[');
            String arrayTypeName = name.substring(0, arrayIndex);
            String sizeString = name.substring(arrayIndex).replaceAll("\\[|\\]", "");

            ObjectType arrayType = (ObjectType) getTypeByName(arrayTypeName, structuralView);
            
            /**
             * In case of simple types only, the type has to either a primitive type
             * or a data type.
             */
            if (!simpleTypesOnly
                    || simpleTypesOnly
                            && (arrayType instanceof PrimitiveType
                                    || arrayType instanceof Classifier && ((Classifier) arrayType).isDataType())) {
                type = RamFactory.eINSTANCE.createRArray();
                type.setType(arrayType);
                
                if (!sizeString.isEmpty()) {
                    type.setSize(Integer.parseInt(sizeString));
                }
                
                structuralView.getTypes().add(type);
            }
        }

        return type;
    }

    /**
     * Extract the class name with generic parameters of an ObjectType.
     *
     * @param otype some ObjectType
     * @return the class name with generics
     */
    private static String getClassNameWithGenerics(ObjectType otype) {
        String name = otype.getName();
        // if implementation class but not a primitive type
        if (otype instanceof ImplementationClass && (!(otype instanceof PrimitiveType) || otype instanceof REnum)) {
            if (((ImplementationClass) otype).getInstanceClassName() != null) {
                name = ((ImplementationClass) otype).getInstanceClassName();
            }
        }
        if (otype instanceof Classifier) {
            // if has type parameters
            EList<TypeParameter> typeParams = ((Classifier) otype).getTypeParameters();
            if (typeParams.size() != 0) {
                name += GENERIC_DELIMITER_BEGIN;
                // go through every type parameter
                for (int i = 0; i < typeParams.size(); i++) {
                    TypeParameter tp = typeParams.get(i);
                    String tpName = tp.getName();
                    ObjectType genericType = tp.getGenericType();

                    // if implementation class or implementation enum extract name
                    if (genericType instanceof ImplementationClass || genericType instanceof REnum) {
                        if (((ImplementationClass) genericType).getInstanceClassName() != null) {
                            tpName = getClassNameWithGenerics(genericType);
                        }
                    } else if (genericType instanceof RArray) {
                        tpName = getClassNameWithGenerics(((RArray) genericType).getType()) + ARRAY_DELIMITER_EMPTY;
                    } else if (genericType instanceof PrimitiveType
                            || genericType instanceof ca.mcgill.sel.ram.Class) {
                        tpName = genericType.getName();
                    }
                    // add to name
                    name += tpName;
                    // add comma if not last one
                    if (i != typeParams.size() - 1) {
                        name += ",";
                    }
                }
                name += GENERIC_DELIMITER_END;
            }
        }

        return name;
    }

    /**
     * Returns the {@link RAMVisibilityType} from the given string.
     *
     * @param stringRepresentation
     *            The string representation of the visibility type.
     * @return The visibility type associated with the representation, null if none exists.
     */
    public static RAMVisibilityType getRamVisibilityFromStringRepresentation(String stringRepresentation) {
        RAMVisibilityType result = null;

        if (SYMBOL_PUBLIC.equals(stringRepresentation)) {
            result = RAMVisibilityType.PUBLIC;
        } else if (SYMBOL_PACKAGE.equals(stringRepresentation)) {
            result = RAMVisibilityType.PACKAGE;
        } else if (SYMBOL_PRIVATE.equals(stringRepresentation)) {
            result = RAMVisibilityType.PRIVATE;
        } else if (SYMBOL_PROTECTED.equals(stringRepresentation)) {
            result = RAMVisibilityType.PROTECTED;
        }

        return result;
    }

    /**
     * Get the name of a specified type. Replaces all type parameters by the type parameters of the owner.
     *
     * @param type some type
     * @param owner ImplementationClass that uses this type as return type or parameter.
     * @return name of the type
     */
    public static String getNameOfType(java.lang.reflect.Type type, ImplementationClass owner) {
        // get name
        String name = type.toString();

        // if it is a parameterized type get all type parameters
        if (type instanceof ParameterizedType) {
            // get raw name
            ParameterizedType pType = (ParameterizedType) type;
            name = pType.getRawType().toString().replaceAll("(interface|class|enum) ", "");

            // get all generics
            java.lang.reflect.Type[] generics = pType.getActualTypeArguments();
            if (generics.length != 0) {
                name += GENERIC_DELIMITER_BEGIN;
                for (int i = 0; i < generics.length; i++) {
                    // if also a parameterized type recursively get the name
                    if (generics[i] instanceof WildcardType) {
                        name += "?";
                    } else {
                        name += getNameOfType(generics[i], owner);
                    }

                    if (i != generics.length - 1) {
                        name += ",";
                    }
                }
                name += GENERIC_DELIMITER_END;
            }
        } else if (type instanceof GenericArrayType) {
            name = getNameOfType(((GenericArrayType) type).getGenericComponentType(), owner) + ARRAY_DELIMITER_EMPTY;
        } else if (isGenericOf(owner, name)) {
            name = getClassNameWithGenerics(getGenericOf(owner, name).getGenericType());
        } else if (type instanceof java.lang.Class<?>) {
            if (((java.lang.Class<?>) type).isArray()) {
                name = getNameOfType(((java.lang.Class<?>) type).getComponentType(), owner) + ARRAY_DELIMITER_EMPTY;
            } else {
                name = ((java.lang.Class<?>) type).getCanonicalName();
            }
        }
        // Replace java.lang.String to String
        return name.replaceAll("java.lang.String", "String");
    }

    /**
     * Gets the TypeParameter of some ImplementationClass given the type parameter name.
     *
     * @param owner of the type parameter
     * @param typeName name of type parameter
     * @return TypeParameter with the specified name
     */
    public static TypeParameter getGenericOf(ImplementationClass owner, String typeName) {
        for (TypeParameter tp : owner.getTypeParameters()) {
            if (typeName.equals(tp.getName())) {
                return tp;
            }
        }
        return null;
    }

    /**
     * Checks if ImplementationClass has a generic with the specified type parameter name.
     *
     * @param owner Some ImplementationClass.
     * @param typeName Type parameter name
     * @return true if owner contains the typeName, false otherwise
     */
    public static boolean isGenericOf(ImplementationClass owner, String typeName) {
        return getGenericOf(owner, typeName) != null;
    }

}
