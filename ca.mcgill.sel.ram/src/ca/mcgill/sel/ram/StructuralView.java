/**
 */
package ca.mcgill.sel.ram;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Structural View</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.ram.StructuralView#getClasses <em>Classes</em>}</li>
 *   <li>{@link ca.mcgill.sel.ram.StructuralView#getAssociations <em>Associations</em>}</li>
 *   <li>{@link ca.mcgill.sel.ram.StructuralView#getTypes <em>Types</em>}</li>
 *   <li>{@link ca.mcgill.sel.ram.StructuralView#getDependencies <em>Dependencies</em>}</li>
 *   <li>{@link ca.mcgill.sel.ram.StructuralView#getImportstubs <em>Importstubs</em>}</li>
 *   <li>{@link ca.mcgill.sel.ram.StructuralView#getSignature <em>Signature</em>}</li>
 *   <li>{@link ca.mcgill.sel.ram.StructuralView#getLauncherclass <em>Launcherclass</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.ram.RamPackage#getStructuralView()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='uniqueTypes noTwoClassesWithSameName'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot uniqueTypes='Tuple {\n\tmessage : String = \'There may be only one type of the same type\',\n\tstatus : Boolean = self.types-&gt;isUnique(name)\n}.status' noTwoClassesWithSameName='Tuple {\n\tmessage : String = \'Name of a class has to be unique\',\n\tstatus : Boolean = self.classes-&gt;isUnique(name)\n}.status'"
 * @generated
 */
public interface StructuralView extends EObject {
	/**
	 * Returns the value of the '<em><b>Classes</b></em>' containment reference list.
	 * The list contents are of type {@link ca.mcgill.sel.ram.Classifier}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Classes</em>' containment reference list.
	 * @see ca.mcgill.sel.ram.RamPackage#getStructuralView_Classes()
	 * @model containment="true"
	 * @generated
	 */
	EList<Classifier> getClasses();

	/**
	 * Returns the value of the '<em><b>Associations</b></em>' containment reference list.
	 * The list contents are of type {@link ca.mcgill.sel.ram.Association}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Associations</em>' containment reference list.
	 * @see ca.mcgill.sel.ram.RamPackage#getStructuralView_Associations()
	 * @model containment="true"
	 * @generated
	 */
	EList<Association> getAssociations();

	/**
	 * Returns the value of the '<em><b>Types</b></em>' containment reference list.
	 * The list contents are of type {@link ca.mcgill.sel.ram.Type}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Types</em>' containment reference list.
	 * @see ca.mcgill.sel.ram.RamPackage#getStructuralView_Types()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<Type> getTypes();

	/**
	 * Returns the value of the '<em><b>Dependencies</b></em>' containment reference list.
	 * The list contents are of type {@link ca.mcgill.sel.ram.ArtifactSignature}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dependencies</em>' containment reference list.
	 * @see ca.mcgill.sel.ram.RamPackage#getStructuralView_Dependencies()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<ArtifactSignature> getDependencies();

	/**
	 * Returns the value of the '<em><b>Importstubs</b></em>' containment reference list.
	 * The list contents are of type {@link ca.mcgill.sel.ram.Import}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Importstubs</em>' containment reference list.
	 * @see ca.mcgill.sel.ram.RamPackage#getStructuralView_Importstubs()
	 * @model containment="true"
	 * @generated
	 */
	EList<Import> getImportstubs();

	/**
	 * Returns the value of the '<em><b>Signature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Signature</em>' containment reference.
	 * @see #setSignature(ArtifactSignature)
	 * @see ca.mcgill.sel.ram.RamPackage#getStructuralView_Signature()
	 * @model containment="true"
	 * @generated
	 */
	ArtifactSignature getSignature();

	/**
	 * Sets the value of the '{@link ca.mcgill.sel.ram.StructuralView#getSignature <em>Signature</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Signature</em>' containment reference.
	 * @see #getSignature()
	 * @generated
	 */
	void setSignature(ArtifactSignature value);

	/**
	 * Returns the value of the '<em><b>Launcherclass</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Launcherclass</em>' reference.
	 * @see #setLauncherclass(Classifier)
	 * @see ca.mcgill.sel.ram.RamPackage#getStructuralView_Launcherclass()
	 * @model
	 * @generated
	 */
	Classifier getLauncherclass();

	/**
	 * Sets the value of the '{@link ca.mcgill.sel.ram.StructuralView#getLauncherclass <em>Launcherclass</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Launcherclass</em>' reference.
	 * @see #getLauncherclass()
	 * @generated
	 */
	void setLauncherclass(Classifier value);

} // StructuralView
