/**
 */
package ca.mcgill.sel.ram;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Binary</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.ram.Binary#getLeft <em>Left</em>}</li>
 *   <li>{@link ca.mcgill.sel.ram.Binary#getRight <em>Right</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.ram.RamPackage#getBinary()
 * @model abstract="true"
 * @generated
 */
public interface Binary extends Operator {
	/**
	 * Returns the value of the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Left</em>' containment reference.
	 * @see #setLeft(ValueSpecification)
	 * @see ca.mcgill.sel.ram.RamPackage#getBinary_Left()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ValueSpecification getLeft();

	/**
	 * Sets the value of the '{@link ca.mcgill.sel.ram.Binary#getLeft <em>Left</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Left</em>' containment reference.
	 * @see #getLeft()
	 * @generated
	 */
	void setLeft(ValueSpecification value);

	/**
	 * Returns the value of the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Right</em>' containment reference.
	 * @see #setRight(ValueSpecification)
	 * @see ca.mcgill.sel.ram.RamPackage#getBinary_Right()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ValueSpecification getRight();

	/**
	 * Sets the value of the '{@link ca.mcgill.sel.ram.Binary#getRight <em>Right</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Right</em>' containment reference.
	 * @see #getRight()
	 * @generated
	 */
	void setRight(ValueSpecification value);

} // Binary
