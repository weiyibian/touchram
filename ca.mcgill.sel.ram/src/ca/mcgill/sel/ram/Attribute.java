/**
 */
package ca.mcgill.sel.ram;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.ram.Attribute#getType <em>Type</em>}</li>
 *   <li>{@link ca.mcgill.sel.ram.Attribute#getVisibility <em>Visibility</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.ram.RamPackage#getAttribute()
 * @model
 * @generated
 */
public interface Attribute extends StructuralFeature, TemporaryProperty, MappableElement, Traceable, Annotatable {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(ObjectType)
	 * @see ca.mcgill.sel.ram.RamPackage#getAttribute_Type()
	 * @model required="true"
	 * @generated
	 */
	ObjectType getType();

	/**
	 * Sets the value of the '{@link ca.mcgill.sel.ram.Attribute#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(ObjectType value);

	/**
	 * Returns the value of the '<em><b>Visibility</b></em>' attribute.
	 * The default value is <code>"protected"</code>.
	 * The literals are from the enumeration {@link ca.mcgill.sel.ram.RAMVisibilityType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Visibility</em>' attribute.
	 * @see ca.mcgill.sel.ram.RAMVisibilityType
	 * @see #setVisibility(RAMVisibilityType)
	 * @see ca.mcgill.sel.ram.RamPackage#getAttribute_Visibility()
	 * @model default="protected"
	 * @generated
	 */
	RAMVisibilityType getVisibility();

	/**
	 * Sets the value of the '{@link ca.mcgill.sel.ram.Attribute#getVisibility <em>Visibility</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Visibility</em>' attribute.
	 * @see ca.mcgill.sel.ram.RAMVisibilityType
	 * @see #getVisibility()
	 * @generated
	 */
	void setVisibility(RAMVisibilityType value);

} // Attribute
