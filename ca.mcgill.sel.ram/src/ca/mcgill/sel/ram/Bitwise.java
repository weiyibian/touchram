/**
 */
package ca.mcgill.sel.ram;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bitwise</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.mcgill.sel.ram.RamPackage#getBitwise()
 * @model abstract="true"
 * @generated
 */
public interface Bitwise extends Binary {
} // Bitwise
