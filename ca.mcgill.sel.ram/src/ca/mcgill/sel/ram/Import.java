/**
 */
package ca.mcgill.sel.ram;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Import</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.ram.Import#getStub <em>Stub</em>}</li>
 *   <li>{@link ca.mcgill.sel.ram.Import#getExternalartifact <em>Externalartifact</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.ram.RamPackage#getImport()
 * @model
 * @generated
 */
public interface Import extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Stub</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Stub</em>' attribute.
	 * @see #setStub(String)
	 * @see ca.mcgill.sel.ram.RamPackage#getImport_Stub()
	 * @model required="true"
	 * @generated
	 */
	String getStub();

	/**
	 * Sets the value of the '{@link ca.mcgill.sel.ram.Import#getStub <em>Stub</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Stub</em>' attribute.
	 * @see #getStub()
	 * @generated
	 */
	void setStub(String value);

	/**
	 * Returns the value of the '<em><b>Externalartifact</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Externalartifact</em>' reference.
	 * @see #setExternalartifact(ArtifactSignature)
	 * @see ca.mcgill.sel.ram.RamPackage#getImport_Externalartifact()
	 * @model
	 * @generated
	 */
	ArtifactSignature getExternalartifact();

	/**
	 * Sets the value of the '{@link ca.mcgill.sel.ram.Import#getExternalartifact <em>Externalartifact</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Externalartifact</em>' reference.
	 * @see #getExternalartifact()
	 * @generated
	 */
	void setExternalartifact(ArtifactSignature value);

} // Import
