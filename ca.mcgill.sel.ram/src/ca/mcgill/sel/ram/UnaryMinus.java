/**
 */
package ca.mcgill.sel.ram;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Unary Minus</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.mcgill.sel.ram.RamPackage#getUnaryMinus()
 * @model
 * @generated
 */
public interface UnaryMinus extends Unary {
} // UnaryMinus
