/**
 */
package ca.mcgill.sel.ram;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Literal Char</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.ram.LiteralChar#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.ram.RamPackage#getLiteralChar()
 * @model
 * @generated
 */
public interface LiteralChar extends LiteralSpecification {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(char)
	 * @see ca.mcgill.sel.ram.RamPackage#getLiteralChar_Value()
	 * @model required="true"
	 * @generated
	 */
	char getValue();

	/**
	 * Sets the value of the '{@link ca.mcgill.sel.ram.LiteralChar#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(char value);

} // LiteralChar
