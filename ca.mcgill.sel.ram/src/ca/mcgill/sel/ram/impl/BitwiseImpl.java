/**
 */
package ca.mcgill.sel.ram.impl;

import ca.mcgill.sel.ram.Bitwise;
import ca.mcgill.sel.ram.RamPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Bitwise</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class BitwiseImpl extends BinaryImpl implements Bitwise {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BitwiseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RamPackage.Literals.BITWISE;
	}

} //BitwiseImpl
