/**
 */
package ca.mcgill.sel.ram.impl;

import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.impl.COREMappingImpl;

import ca.mcgill.sel.ram.AttributeMapping;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.ClassifierMapping;
import ca.mcgill.sel.ram.OperationMapping;
import ca.mcgill.sel.ram.RamPackage;

import java.util.Collection;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Classifier Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ClassifierMappingImpl extends COREMappingImpl<Classifier> implements ClassifierMapping {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClassifierMappingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RamPackage.Literals.CLASSIFIER_MAPPING;
	}

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated NOT
     */
    @Override
    public EList<AttributeMapping> getAttributeMappings() {
        Collection<AttributeMapping> collection = EMFModelUtil.collectElementsOfType(this, 
                CorePackage.Literals.CORE_MAPPING__MAPPINGS, RamPackage.Literals.ATTRIBUTE_MAPPING);
         
        return new BasicEList<AttributeMapping>(collection);
    }

    /**
     * <!-- begin-user-doc -->.
     * <!-- end-user-doc -->
     * @generated NOT
     */
    @Override
    public EList<OperationMapping> getOperationMappings() {
        Collection<OperationMapping> collection = EMFModelUtil.collectElementsOfType(this, 
                CorePackage.Literals.CORE_MAPPING__MAPPINGS, RamPackage.Literals.OPERATION_MAPPING);
         
        return new BasicEList<OperationMapping>(collection);
    }

} //ClassifierMappingImpl
