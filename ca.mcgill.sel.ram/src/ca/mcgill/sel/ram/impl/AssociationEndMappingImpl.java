/**
 */
package ca.mcgill.sel.ram.impl;

import ca.mcgill.sel.core.impl.COREMappingImpl;

import ca.mcgill.sel.ram.AssociationEnd;
import ca.mcgill.sel.ram.AssociationEndMapping;
import ca.mcgill.sel.ram.RamPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Association End Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AssociationEndMappingImpl extends COREMappingImpl<AssociationEnd> implements AssociationEndMapping {
    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    protected AssociationEndMappingImpl() {
		super();
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    protected EClass eStaticClass() {
		return RamPackage.Literals.ASSOCIATION_END_MAPPING;
	}

} //AssociationEndMappingImpl
