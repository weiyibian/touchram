/**
 */
package ca.mcgill.sel.ram.impl;

import ca.mcgill.sel.ram.Annotatable;
import ca.mcgill.sel.ram.Annotation;
import ca.mcgill.sel.ram.Attribute;
import ca.mcgill.sel.ram.MappableElement;
import ca.mcgill.sel.ram.ObjectType;
import ca.mcgill.sel.ram.RAMPartialityType;
import ca.mcgill.sel.ram.RAMVisibilityType;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.TemporaryProperty;
import ca.mcgill.sel.ram.Traceable;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Attribute</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.ram.impl.AttributeImpl#getPartiality <em>Partiality</em>}</li>
 *   <li>{@link ca.mcgill.sel.ram.impl.AttributeImpl#getAnnotation <em>Annotation</em>}</li>
 *   <li>{@link ca.mcgill.sel.ram.impl.AttributeImpl#getType <em>Type</em>}</li>
 *   <li>{@link ca.mcgill.sel.ram.impl.AttributeImpl#getVisibility <em>Visibility</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AttributeImpl extends StructuralFeatureImpl implements Attribute {
	/**
	 * The default value of the '{@link #getPartiality() <em>Partiality</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPartiality()
	 * @generated
	 * @ordered
	 */
	protected static final RAMPartialityType PARTIALITY_EDEFAULT = RAMPartialityType.NONE;

	/**
	 * The cached value of the '{@link #getPartiality() <em>Partiality</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPartiality()
	 * @generated
	 * @ordered
	 */
	protected RAMPartialityType partiality = PARTIALITY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAnnotation() <em>Annotation</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnnotation()
	 * @generated
	 * @ordered
	 */
	protected EList<Annotation> annotation;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected ObjectType type;

	/**
	 * The default value of the '{@link #getVisibility() <em>Visibility</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVisibility()
	 * @generated
	 * @ordered
	 */
	protected static final RAMVisibilityType VISIBILITY_EDEFAULT = RAMVisibilityType.PROTECTED;

	/**
	 * The cached value of the '{@link #getVisibility() <em>Visibility</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVisibility()
	 * @generated
	 * @ordered
	 */
	protected RAMVisibilityType visibility = VISIBILITY_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AttributeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RamPackage.Literals.ATTRIBUTE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RAMPartialityType getPartiality() {
		return partiality;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPartiality(RAMPartialityType newPartiality) {
		RAMPartialityType oldPartiality = partiality;
		partiality = newPartiality == null ? PARTIALITY_EDEFAULT : newPartiality;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RamPackage.ATTRIBUTE__PARTIALITY, oldPartiality, partiality));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Annotation> getAnnotation() {
		if (annotation == null) {
			annotation = new EObjectContainmentEList<Annotation>(Annotation.class, this, RamPackage.ATTRIBUTE__ANNOTATION);
		}
		return annotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ObjectType getType() {
		if (type != null && type.eIsProxy()) {
			InternalEObject oldType = (InternalEObject)type;
			type = (ObjectType)eResolveProxy(oldType);
			if (type != oldType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RamPackage.ATTRIBUTE__TYPE, oldType, type));
			}
		}
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectType basicGetType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setType(ObjectType newType) {
		ObjectType oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RamPackage.ATTRIBUTE__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RAMVisibilityType getVisibility() {
		return visibility;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVisibility(RAMVisibilityType newVisibility) {
		RAMVisibilityType oldVisibility = visibility;
		visibility = newVisibility == null ? VISIBILITY_EDEFAULT : newVisibility;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RamPackage.ATTRIBUTE__VISIBILITY, oldVisibility, visibility));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RamPackage.ATTRIBUTE__ANNOTATION:
				return ((InternalEList<?>)getAnnotation()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RamPackage.ATTRIBUTE__PARTIALITY:
				return getPartiality();
			case RamPackage.ATTRIBUTE__ANNOTATION:
				return getAnnotation();
			case RamPackage.ATTRIBUTE__TYPE:
				if (resolve) return getType();
				return basicGetType();
			case RamPackage.ATTRIBUTE__VISIBILITY:
				return getVisibility();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RamPackage.ATTRIBUTE__PARTIALITY:
				setPartiality((RAMPartialityType)newValue);
				return;
			case RamPackage.ATTRIBUTE__ANNOTATION:
				getAnnotation().clear();
				getAnnotation().addAll((Collection<? extends Annotation>)newValue);
				return;
			case RamPackage.ATTRIBUTE__TYPE:
				setType((ObjectType)newValue);
				return;
			case RamPackage.ATTRIBUTE__VISIBILITY:
				setVisibility((RAMVisibilityType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RamPackage.ATTRIBUTE__PARTIALITY:
				setPartiality(PARTIALITY_EDEFAULT);
				return;
			case RamPackage.ATTRIBUTE__ANNOTATION:
				getAnnotation().clear();
				return;
			case RamPackage.ATTRIBUTE__TYPE:
				setType((ObjectType)null);
				return;
			case RamPackage.ATTRIBUTE__VISIBILITY:
				setVisibility(VISIBILITY_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RamPackage.ATTRIBUTE__PARTIALITY:
				return partiality != PARTIALITY_EDEFAULT;
			case RamPackage.ATTRIBUTE__ANNOTATION:
				return annotation != null && !annotation.isEmpty();
			case RamPackage.ATTRIBUTE__TYPE:
				return type != null;
			case RamPackage.ATTRIBUTE__VISIBILITY:
				return visibility != VISIBILITY_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == TemporaryProperty.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == MappableElement.class) {
			switch (derivedFeatureID) {
				case RamPackage.ATTRIBUTE__PARTIALITY: return RamPackage.MAPPABLE_ELEMENT__PARTIALITY;
				default: return -1;
			}
		}
		if (baseClass == Traceable.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == Annotatable.class) {
			switch (derivedFeatureID) {
				case RamPackage.ATTRIBUTE__ANNOTATION: return RamPackage.ANNOTATABLE__ANNOTATION;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == TemporaryProperty.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == MappableElement.class) {
			switch (baseFeatureID) {
				case RamPackage.MAPPABLE_ELEMENT__PARTIALITY: return RamPackage.ATTRIBUTE__PARTIALITY;
				default: return -1;
			}
		}
		if (baseClass == Traceable.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == Annotatable.class) {
			switch (baseFeatureID) {
				case RamPackage.ANNOTATABLE__ANNOTATION: return RamPackage.ATTRIBUTE__ANNOTATION;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (partiality: ");
		result.append(partiality);
		result.append(", visibility: ");
		result.append(visibility);
		result.append(')');
		return result.toString();
	}

} //AttributeImpl
