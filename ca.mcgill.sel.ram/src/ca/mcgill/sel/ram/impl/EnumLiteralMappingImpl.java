/**
 */
package ca.mcgill.sel.ram.impl;

import ca.mcgill.sel.core.impl.COREMappingImpl;

import ca.mcgill.sel.ram.EnumLiteralMapping;
import ca.mcgill.sel.ram.REnumLiteral;
import ca.mcgill.sel.ram.RamPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Enum Literal Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class EnumLiteralMappingImpl extends COREMappingImpl<REnumLiteral> implements EnumLiteralMapping {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EnumLiteralMappingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RamPackage.Literals.ENUM_LITERAL_MAPPING;
	}

} //EnumLiteralMappingImpl
