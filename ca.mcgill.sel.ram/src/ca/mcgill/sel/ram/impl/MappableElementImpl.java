/**
 */
package ca.mcgill.sel.ram.impl;

import ca.mcgill.sel.ram.MappableElement;
import ca.mcgill.sel.ram.RAMPartialityType;
import ca.mcgill.sel.ram.RamPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Mappable Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.ram.impl.MappableElementImpl#getPartiality <em>Partiality</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class MappableElementImpl extends NamedElementImpl implements MappableElement {
	/**
	 * The default value of the '{@link #getPartiality() <em>Partiality</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPartiality()
	 * @generated
	 * @ordered
	 */
	protected static final RAMPartialityType PARTIALITY_EDEFAULT = RAMPartialityType.NONE;

	/**
	 * The cached value of the '{@link #getPartiality() <em>Partiality</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPartiality()
	 * @generated
	 * @ordered
	 */
	protected RAMPartialityType partiality = PARTIALITY_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MappableElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RamPackage.Literals.MAPPABLE_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RAMPartialityType getPartiality() {
		return partiality;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPartiality(RAMPartialityType newPartiality) {
		RAMPartialityType oldPartiality = partiality;
		partiality = newPartiality == null ? PARTIALITY_EDEFAULT : newPartiality;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RamPackage.MAPPABLE_ELEMENT__PARTIALITY, oldPartiality, partiality));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RamPackage.MAPPABLE_ELEMENT__PARTIALITY:
				return getPartiality();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RamPackage.MAPPABLE_ELEMENT__PARTIALITY:
				setPartiality((RAMPartialityType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RamPackage.MAPPABLE_ELEMENT__PARTIALITY:
				setPartiality(PARTIALITY_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RamPackage.MAPPABLE_ELEMENT__PARTIALITY:
				return partiality != PARTIALITY_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (partiality: ");
		result.append(partiality);
		result.append(')');
		return result.toString();
	}

} //MappableElementImpl
