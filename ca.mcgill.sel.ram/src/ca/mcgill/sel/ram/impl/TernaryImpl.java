/**
 */
package ca.mcgill.sel.ram.impl;

import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.Ternary;
import ca.mcgill.sel.ram.ValueSpecification;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ternary</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.ram.impl.TernaryImpl#getCondition <em>Condition</em>}</li>
 *   <li>{@link ca.mcgill.sel.ram.impl.TernaryImpl#getExpressionT <em>Expression T</em>}</li>
 *   <li>{@link ca.mcgill.sel.ram.impl.TernaryImpl#getExpressionF <em>Expression F</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class TernaryImpl extends OperatorImpl implements Ternary {
	/**
	 * The cached value of the '{@link #getCondition() <em>Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCondition()
	 * @generated
	 * @ordered
	 */
	protected ValueSpecification condition;

	/**
	 * The cached value of the '{@link #getExpressionT() <em>Expression T</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpressionT()
	 * @generated
	 * @ordered
	 */
	protected ValueSpecification expressionT;

	/**
	 * The cached value of the '{@link #getExpressionF() <em>Expression F</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpressionF()
	 * @generated
	 * @ordered
	 */
	protected ValueSpecification expressionF;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TernaryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RamPackage.Literals.TERNARY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ValueSpecification getCondition() {
		return condition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCondition(ValueSpecification newCondition, NotificationChain msgs) {
		ValueSpecification oldCondition = condition;
		condition = newCondition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, RamPackage.TERNARY__CONDITION, oldCondition, newCondition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCondition(ValueSpecification newCondition) {
		if (newCondition != condition) {
			NotificationChain msgs = null;
			if (condition != null)
				msgs = ((InternalEObject)condition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - RamPackage.TERNARY__CONDITION, null, msgs);
			if (newCondition != null)
				msgs = ((InternalEObject)newCondition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - RamPackage.TERNARY__CONDITION, null, msgs);
			msgs = basicSetCondition(newCondition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RamPackage.TERNARY__CONDITION, newCondition, newCondition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ValueSpecification getExpressionT() {
		return expressionT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExpressionT(ValueSpecification newExpressionT, NotificationChain msgs) {
		ValueSpecification oldExpressionT = expressionT;
		expressionT = newExpressionT;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, RamPackage.TERNARY__EXPRESSION_T, oldExpressionT, newExpressionT);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setExpressionT(ValueSpecification newExpressionT) {
		if (newExpressionT != expressionT) {
			NotificationChain msgs = null;
			if (expressionT != null)
				msgs = ((InternalEObject)expressionT).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - RamPackage.TERNARY__EXPRESSION_T, null, msgs);
			if (newExpressionT != null)
				msgs = ((InternalEObject)newExpressionT).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - RamPackage.TERNARY__EXPRESSION_T, null, msgs);
			msgs = basicSetExpressionT(newExpressionT, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RamPackage.TERNARY__EXPRESSION_T, newExpressionT, newExpressionT));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ValueSpecification getExpressionF() {
		return expressionF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExpressionF(ValueSpecification newExpressionF, NotificationChain msgs) {
		ValueSpecification oldExpressionF = expressionF;
		expressionF = newExpressionF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, RamPackage.TERNARY__EXPRESSION_F, oldExpressionF, newExpressionF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setExpressionF(ValueSpecification newExpressionF) {
		if (newExpressionF != expressionF) {
			NotificationChain msgs = null;
			if (expressionF != null)
				msgs = ((InternalEObject)expressionF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - RamPackage.TERNARY__EXPRESSION_F, null, msgs);
			if (newExpressionF != null)
				msgs = ((InternalEObject)newExpressionF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - RamPackage.TERNARY__EXPRESSION_F, null, msgs);
			msgs = basicSetExpressionF(newExpressionF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RamPackage.TERNARY__EXPRESSION_F, newExpressionF, newExpressionF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RamPackage.TERNARY__CONDITION:
				return basicSetCondition(null, msgs);
			case RamPackage.TERNARY__EXPRESSION_T:
				return basicSetExpressionT(null, msgs);
			case RamPackage.TERNARY__EXPRESSION_F:
				return basicSetExpressionF(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RamPackage.TERNARY__CONDITION:
				return getCondition();
			case RamPackage.TERNARY__EXPRESSION_T:
				return getExpressionT();
			case RamPackage.TERNARY__EXPRESSION_F:
				return getExpressionF();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RamPackage.TERNARY__CONDITION:
				setCondition((ValueSpecification)newValue);
				return;
			case RamPackage.TERNARY__EXPRESSION_T:
				setExpressionT((ValueSpecification)newValue);
				return;
			case RamPackage.TERNARY__EXPRESSION_F:
				setExpressionF((ValueSpecification)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RamPackage.TERNARY__CONDITION:
				setCondition((ValueSpecification)null);
				return;
			case RamPackage.TERNARY__EXPRESSION_T:
				setExpressionT((ValueSpecification)null);
				return;
			case RamPackage.TERNARY__EXPRESSION_F:
				setExpressionF((ValueSpecification)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RamPackage.TERNARY__CONDITION:
				return condition != null;
			case RamPackage.TERNARY__EXPRESSION_T:
				return expressionT != null;
			case RamPackage.TERNARY__EXPRESSION_F:
				return expressionF != null;
		}
		return super.eIsSet(featureID);
	}

} //TernaryImpl
