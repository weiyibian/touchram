/**
 */
package ca.mcgill.sel.ram.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import ca.mcgill.sel.ram.ArtifactSignature;
import ca.mcgill.sel.ram.Association;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.Import;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.StructuralView;
import ca.mcgill.sel.ram.Type;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Structural View</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.ram.impl.StructuralViewImpl#getClasses <em>Classes</em>}</li>
 *   <li>{@link ca.mcgill.sel.ram.impl.StructuralViewImpl#getAssociations <em>Associations</em>}</li>
 *   <li>{@link ca.mcgill.sel.ram.impl.StructuralViewImpl#getTypes <em>Types</em>}</li>
 *   <li>{@link ca.mcgill.sel.ram.impl.StructuralViewImpl#getDependencies <em>Dependencies</em>}</li>
 *   <li>{@link ca.mcgill.sel.ram.impl.StructuralViewImpl#getImportstubs <em>Importstubs</em>}</li>
 *   <li>{@link ca.mcgill.sel.ram.impl.StructuralViewImpl#getSignature <em>Signature</em>}</li>
 *   <li>{@link ca.mcgill.sel.ram.impl.StructuralViewImpl#getLauncherclass <em>Launcherclass</em>}</li>
 * </ul>
 *
 * @generated
 */
public class StructuralViewImpl extends EObjectImpl implements StructuralView {
	/**
	 * The cached value of the '{@link #getClasses() <em>Classes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClasses()
	 * @generated
	 * @ordered
	 */
	protected EList<Classifier> classes;

	/**
	 * The cached value of the '{@link #getAssociations() <em>Associations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssociations()
	 * @generated
	 * @ordered
	 */
	protected EList<Association> associations;

	/**
	 * The cached value of the '{@link #getTypes() <em>Types</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypes()
	 * @generated
	 * @ordered
	 */
	protected EList<Type> types;

	/**
	 * The cached value of the '{@link #getDependencies() <em>Dependencies</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDependencies()
	 * @generated
	 * @ordered
	 */
	protected EList<ArtifactSignature> dependencies;

	/**
	 * The cached value of the '{@link #getImportstubs() <em>Importstubs</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImportstubs()
	 * @generated
	 * @ordered
	 */
	protected EList<Import> importstubs;

	/**
	 * The cached value of the '{@link #getSignature() <em>Signature</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSignature()
	 * @generated
	 * @ordered
	 */
	protected ArtifactSignature signature;

	/**
	 * The cached value of the '{@link #getLauncherclass() <em>Launcherclass</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLauncherclass()
	 * @generated
	 * @ordered
	 */
	protected Classifier launcherclass;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StructuralViewImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RamPackage.Literals.STRUCTURAL_VIEW;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Classifier> getClasses() {
		if (classes == null) {
			classes = new EObjectContainmentEList<Classifier>(Classifier.class, this, RamPackage.STRUCTURAL_VIEW__CLASSES);
		}
		return classes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Association> getAssociations() {
		if (associations == null) {
			associations = new EObjectContainmentEList<Association>(Association.class, this, RamPackage.STRUCTURAL_VIEW__ASSOCIATIONS);
		}
		return associations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Type> getTypes() {
		if (types == null) {
			types = new EObjectContainmentEList<Type>(Type.class, this, RamPackage.STRUCTURAL_VIEW__TYPES);
		}
		return types;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ArtifactSignature> getDependencies() {
		if (dependencies == null) {
			dependencies = new EObjectContainmentEList<ArtifactSignature>(ArtifactSignature.class, this, RamPackage.STRUCTURAL_VIEW__DEPENDENCIES);
		}
		return dependencies;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Import> getImportstubs() {
		if (importstubs == null) {
			importstubs = new EObjectContainmentEList<Import>(Import.class, this, RamPackage.STRUCTURAL_VIEW__IMPORTSTUBS);
		}
		return importstubs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ArtifactSignature getSignature() {
		return signature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSignature(ArtifactSignature newSignature, NotificationChain msgs) {
		ArtifactSignature oldSignature = signature;
		signature = newSignature;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, RamPackage.STRUCTURAL_VIEW__SIGNATURE, oldSignature, newSignature);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSignature(ArtifactSignature newSignature) {
		if (newSignature != signature) {
			NotificationChain msgs = null;
			if (signature != null)
				msgs = ((InternalEObject)signature).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - RamPackage.STRUCTURAL_VIEW__SIGNATURE, null, msgs);
			if (newSignature != null)
				msgs = ((InternalEObject)newSignature).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - RamPackage.STRUCTURAL_VIEW__SIGNATURE, null, msgs);
			msgs = basicSetSignature(newSignature, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RamPackage.STRUCTURAL_VIEW__SIGNATURE, newSignature, newSignature));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Classifier getLauncherclass() {
		if (launcherclass != null && launcherclass.eIsProxy()) {
			InternalEObject oldLauncherclass = (InternalEObject)launcherclass;
			launcherclass = (Classifier)eResolveProxy(oldLauncherclass);
			if (launcherclass != oldLauncherclass) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RamPackage.STRUCTURAL_VIEW__LAUNCHERCLASS, oldLauncherclass, launcherclass));
			}
		}
		return launcherclass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Classifier basicGetLauncherclass() {
		return launcherclass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLauncherclass(Classifier newLauncherclass) {
		Classifier oldLauncherclass = launcherclass;
		launcherclass = newLauncherclass;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RamPackage.STRUCTURAL_VIEW__LAUNCHERCLASS, oldLauncherclass, launcherclass));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RamPackage.STRUCTURAL_VIEW__CLASSES:
				return ((InternalEList<?>)getClasses()).basicRemove(otherEnd, msgs);
			case RamPackage.STRUCTURAL_VIEW__ASSOCIATIONS:
				return ((InternalEList<?>)getAssociations()).basicRemove(otherEnd, msgs);
			case RamPackage.STRUCTURAL_VIEW__TYPES:
				return ((InternalEList<?>)getTypes()).basicRemove(otherEnd, msgs);
			case RamPackage.STRUCTURAL_VIEW__DEPENDENCIES:
				return ((InternalEList<?>)getDependencies()).basicRemove(otherEnd, msgs);
			case RamPackage.STRUCTURAL_VIEW__IMPORTSTUBS:
				return ((InternalEList<?>)getImportstubs()).basicRemove(otherEnd, msgs);
			case RamPackage.STRUCTURAL_VIEW__SIGNATURE:
				return basicSetSignature(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RamPackage.STRUCTURAL_VIEW__CLASSES:
				return getClasses();
			case RamPackage.STRUCTURAL_VIEW__ASSOCIATIONS:
				return getAssociations();
			case RamPackage.STRUCTURAL_VIEW__TYPES:
				return getTypes();
			case RamPackage.STRUCTURAL_VIEW__DEPENDENCIES:
				return getDependencies();
			case RamPackage.STRUCTURAL_VIEW__IMPORTSTUBS:
				return getImportstubs();
			case RamPackage.STRUCTURAL_VIEW__SIGNATURE:
				return getSignature();
			case RamPackage.STRUCTURAL_VIEW__LAUNCHERCLASS:
				if (resolve) return getLauncherclass();
				return basicGetLauncherclass();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RamPackage.STRUCTURAL_VIEW__CLASSES:
				getClasses().clear();
				getClasses().addAll((Collection<? extends Classifier>)newValue);
				return;
			case RamPackage.STRUCTURAL_VIEW__ASSOCIATIONS:
				getAssociations().clear();
				getAssociations().addAll((Collection<? extends Association>)newValue);
				return;
			case RamPackage.STRUCTURAL_VIEW__TYPES:
				getTypes().clear();
				getTypes().addAll((Collection<? extends Type>)newValue);
				return;
			case RamPackage.STRUCTURAL_VIEW__DEPENDENCIES:
				getDependencies().clear();
				getDependencies().addAll((Collection<? extends ArtifactSignature>)newValue);
				return;
			case RamPackage.STRUCTURAL_VIEW__IMPORTSTUBS:
				getImportstubs().clear();
				getImportstubs().addAll((Collection<? extends Import>)newValue);
				return;
			case RamPackage.STRUCTURAL_VIEW__SIGNATURE:
				setSignature((ArtifactSignature)newValue);
				return;
			case RamPackage.STRUCTURAL_VIEW__LAUNCHERCLASS:
				setLauncherclass((Classifier)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RamPackage.STRUCTURAL_VIEW__CLASSES:
				getClasses().clear();
				return;
			case RamPackage.STRUCTURAL_VIEW__ASSOCIATIONS:
				getAssociations().clear();
				return;
			case RamPackage.STRUCTURAL_VIEW__TYPES:
				getTypes().clear();
				return;
			case RamPackage.STRUCTURAL_VIEW__DEPENDENCIES:
				getDependencies().clear();
				return;
			case RamPackage.STRUCTURAL_VIEW__IMPORTSTUBS:
				getImportstubs().clear();
				return;
			case RamPackage.STRUCTURAL_VIEW__SIGNATURE:
				setSignature((ArtifactSignature)null);
				return;
			case RamPackage.STRUCTURAL_VIEW__LAUNCHERCLASS:
				setLauncherclass((Classifier)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RamPackage.STRUCTURAL_VIEW__CLASSES:
				return classes != null && !classes.isEmpty();
			case RamPackage.STRUCTURAL_VIEW__ASSOCIATIONS:
				return associations != null && !associations.isEmpty();
			case RamPackage.STRUCTURAL_VIEW__TYPES:
				return types != null && !types.isEmpty();
			case RamPackage.STRUCTURAL_VIEW__DEPENDENCIES:
				return dependencies != null && !dependencies.isEmpty();
			case RamPackage.STRUCTURAL_VIEW__IMPORTSTUBS:
				return importstubs != null && !importstubs.isEmpty();
			case RamPackage.STRUCTURAL_VIEW__SIGNATURE:
				return signature != null;
			case RamPackage.STRUCTURAL_VIEW__LAUNCHERCLASS:
				return launcherclass != null;
		}
		return super.eIsSet(featureID);
	}

} //StructuralViewImpl
