/**
 */
package ca.mcgill.sel.ram.impl;

import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.UnaryMinus;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Unary Minus</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class UnaryMinusImpl extends UnaryImpl implements UnaryMinus {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UnaryMinusImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RamPackage.Literals.UNARY_MINUS;
	}

} //UnaryMinusImpl
